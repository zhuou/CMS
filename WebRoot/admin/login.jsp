<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>登录</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta content="IE=EmulateIE7" http-equiv="X-UA-Compatible" />
    <meta content="IE=7" http-equiv="X-UA-Compatible" />
    <link href="<%=request.getContextPath()%>/admin/css/blue/login.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
    <script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
    <script src="<%=request.getContextPath()%>/admin/gpm/js/login.js" type="text/javascript"></script>
    <script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    <script type="text/javascript">
		var contextPath = '<%=request.getContextPath()%>';
		if (parent.document.URL!=document.URL) {
			window.parent.location = contextPath+"/login.html";
		};
		$(document).ready(function(){
			$('#img_Validate').attr('src',contextPath+'/validate');
		});
    </script>
</head>
<body onload="loginfouce();">
    <table style="width:100%;height:100%;" cellspacing="0" cellpadding="0" border="0" class="login_bg">
        <tr>
            <td valign="top" align="center" style="padding-top: 73px;">
                <table width="489" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td valign="bottom" style="height:247px" class="login_pic01">
                            <table width="360" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td style="width:10px;height:51px;">&nbsp;</td>
                                    <td style="width:390px"></td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" border="0" align="center">
                                <tr>
                                    <td style="width:93px; height:28px;" align="right">用户名：</td>
                                    <td style="width:227px;">
                                        <input id="txt_UserName" type="text" class="input_login" maxlength="50"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:28px;" align="right">密码：</td>
                                    <td>
                                        <input id="txt_Password" type="password" class="input_login" maxlength="50"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:28px;" align="right">验证码：</td>
                                    <td>
                                        <input id="txt_Code" type="text" size="14" maxlength="8" class="input_text"/>
                                        <img id="img_Validate" alt="" style="width:70px;height:25px;cursor: pointer;border:0;" align="absmiddle" onclick="this.src=this.src+'?'"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:35px;">&nbsp;</td>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" border="0" class="fm_table_left">
                                            <tr>
                                                <td>
                                                    <input id="bnt_login" type="button" class="button_normal" value="登 录" name="Submit"/>
                                                </td>
                                                <td>
                                                    <input id="bnt_reset" type="button" class="button_normal" value="重 置" name="Submit2"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr>
                                        <td style="width:120px;"></td>
                                        <td style="height:22px;color: red;"></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="height:22px;">
                                            <span style="color: #3986B5; font-size: 10px">版权所有 oSwift-易网工作室。保留一切权利。</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="height:137px;" class="login_pic02">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
