var beautifyTopButton = function(options){
	if (typeof options != "undefined" && options != null) {
		$.each(options, function(i, id){
			var eleObj = $("#" + id);
			eleObj.bind({
				mouseover: function(){
					eleObj.addClass("topinput_hover");
				},
				mouseout: function(){
					eleObj.removeClass("topinput_hover");
				}
			});
		});
	}
};
var beautifyBottomButton = function(options){
	if (typeof options != "undefined" && options != null) {
		$.each(options, function(i, id){
			var eleObj = $("#" + id);
			eleObj.bind({
				mouseover: function(){
					eleObj.removeClass();
					eleObj.addClass("inputbutton_hover");
				},
				mouseout: function(){
					eleObj.removeClass();
					eleObj.addClass("inputbutton");
				}
			});
		});
	}
};
var beautifyInputText = function(options){
	if (typeof options != "undefined" && options != null) {
		$.each(options, function(i, id){
			var defaultTxt = $("#" + id).attr('alt');
			$("#" + id).val(defaultTxt).bind({
				focus: function(){
					var txt = $.trim($(this).val());
					if(defaultTxt == txt){
						$(this).val('');
					}
					$(this).css('color','#000');
				},
				blur: function(){
					if('' == $.trim($(this).val())){
						$(this).val(defaultTxt)
						$(this).css('color','#999999');
					}
				}
			});
		});
	}
};
var beautifyTable = function(tbodyId){
	if (typeof tbodyId != "undefined" && tbodyId != null && tbodyId != "") {
		var trObj = $("#" + tbodyId);
		$.each($(trObj).find("tr[class='tdbg']"), function(i, eleObj){
		$(eleObj).bind({
				mouseout: function(){
					$(eleObj).removeClass();
					$(eleObj).addClass("tdbg");
				},
				mouseover: function(){
					$(eleObj).removeClass();
					$(eleObj).addClass("tdbgmouseover");
				}
			});
		});
	}
};
var beautifySingleSelectedTable = function(tbodyId){
	var _checkObj;
	if (typeof tbodyId != "undefined" && tbodyId != null && tbodyId != "") {
		var trObj = $("#" + tbodyId);
		$.each($(trObj).find("tr[class='tdbg']"), function(i, eleObj){
			if(i==0){
				_checkObj = eleObj;
				$(eleObj).css('background-color','#3875D7');
			}
			$(eleObj).bind({
				mouseout: function(){
					$(eleObj).removeClass();
					$(eleObj).addClass("tdbg");
				},
				mouseover: function(){
					$(eleObj).removeClass();
					$(eleObj).addClass("tdbgmouseover");
				},
				click:function(){
					if(null!=_checkObj&&typeof(_checkObj)!='undefined'){
						$(_checkObj).removeAttr("style");
					}
					_checkObj = eleObj;
					$(eleObj).css('background-color','#3875D7');
				}
			});
		});
	}
	this.getValue = function(){
		if(null==_checkObj&&typeof(_checkObj)=='undefined'){
			return '';
		}
		var _valueTemp = $(_checkObj).attr('value');
		if(null==_valueTemp||typeof(_valueTemp)=='undefined'){
			return '';
		}
		return _valueTemp;
	}
};
var checkBoxFunction = {
	init:function(cbId,containId){
		var checkedNum = 0;
		var cbs = this.cbs = $('#'+containId).find(':checkbox');
		var totalNum = cbs.length;
		$.each(cbs,function(i,obj){
			$(obj).change(function(){
				 if(this.checked){
				 	checkedNum = checkedNum+1;
				 	if(checkedNum==totalNum){
				 		$('#'+cbId).attr('checked','checked');
				 	}
				 }
				 else{
				 	checkedNum = checkedNum-1;
				 	$('#'+cbId).removeAttr('checked');
				 }
			});
		});
		$('#'+cbId).change(function(){
			if(this.checked){
				$.each(cbs,function(i,obj){
					this.checked = true;
				});
			}
			else{
				$.each(cbs,function(i,obj){
					this.checked = false;
				});
			}
		});
	},
	getId:function(){
		var array = new Array();
		$.each(this.cbs,function(i,obj){
			if(this.checked){
				array.push($(this).attr('value'));
			}
		});
		return array;
	}
};
var parseUri = {
  	init:function(){
  		var parser = /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
  		key = ['source', 'protocol', 'authority', 'userInfo', 'user', 'password', 'host', 'port', 'relative', 'path', 'directory', 'file', 'query', 'fragment'],
  		url = window.location.href;
  		var str = decodeURI( url ),
		res   = parser.exec( str ),
		uri = { attr : {}, param : {}, seg : {} },
		i   = 14;

		while ( i-- ) {
			uri.attr[ key[i] ] = res[i] || '';
		}
		var array = String(uri.attr['query']).split(/&|;/);
		for(var i=0; i<array.length; i++){
			if(array[i].indexOf('=') != -1){
				var eql = array[i].split('=');
				uri.param[eql[0]] = eql[1]||'';
			}
		}
		uri.seg['path'] = uri.attr.path.replace(/^\/+|\/+$/g,'').split('/');
		uri.seg['fragment'] = uri.attr.fragment.replace(/^\/+|\/+$/g,'').split('/');
		uri.attr['base'] = uri.attr.host ? (uri.attr.protocol ?  uri.attr.protocol+'://'+uri.attr.host : uri.attr.host) + (uri.attr.port ? ':'+uri.attr.port : '') : '';

		this.uri = uri;
  	},
  	setParam:function(key,value){
  		if('undefined' == typeof(this.uri)){
  			this.init();
  		}
  		if('undefined' != typeof(key)&&''!=key){
  			this.uri.param[key] = encodeURIComponent(value);
  		}
  		return this.uri.attr['base']+this.uri.attr['path']+'?'+jQuery.param(this.uri.param);
  	},
  	getParam:function(key){
  		if('undefined' == typeof(this.uri)){
  			this.init();
  		}
  		if('undefined' != typeof(key)&&''!=key){
  			var _v = this.uri.param[key];
  			if('undefined' == typeof(_v)){
  				return '';
  			}
  			return _v;
  		}
  		return '';
  	},
  	clear:function(){
  		for (var key in this.uri.param){
  			delete this.uri.param[key];
  		}
  	},
  	getUrl:function(){
  		if('undefined' == typeof(this.uri)){
  			this.init();
  		}
  		return this.uri.attr['base']+this.uri.attr['path'];
  	},
  	getAllUrl:function(){
  		if('undefined' == typeof(this.uri)){
  			this.init();
  		}
  		return this.uri.attr['base']+this.uri.attr['path']+'?'+jQuery.param(this.uri.param);
  	}
};
var pageTab = function(options){
	var tabId,pannelId;
	if (typeof options != 'undefined' && options != null) {
		for (var key in options){
			if(typeof(tabId)=='undefined'){
				tabId = key;
				pannelId = options[key];
			}
			$('#'+key).click(function(){
				var tdId = $(this).attr('id');
				var tableId = options[tdId];
				if(tabId!=tdId){
					$('#'+tableId).show();
					$('#'+pannelId).hide();
					$('#'+tdId).attr('class', 'titlemouseover');
					$('#'+tabId).attr('class', 'tabtitle');
					tabId = tdId;
					pannelId = tableId;
				}
			});
		}
	}
};
var alertMessage = function(msg){
	art.dialog({id:'alertMessage',title:'提示',content:msg,lock:true,background:'#DEECF7',icon:'alert-message',ok:function(){return true;}});
};
var alertWaiting = function(msg){
	var src = "/admin/common/dialog/skins/icons/loading.gif";
	if(typeof(contextPath)!='undefined'&&contextPath!=null){
		src = contextPath+src;
	}
	var waitWialog = art.dialog({id:'alertWait',title: false,cancel:false,lock:true,background:'#DEECF7'}).content('<div style="height:20px;line-height:20px;"><img style="vertical-align:middle;" src="'+src+'" alt="waiting"/>&nbsp;' + msg + '</div>');
	return waitWialog;
};