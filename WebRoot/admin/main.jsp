<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>后台首页</title>
    <script type="text/javascript">
		var contextPath = '<%=request.getContextPath()%>';
    </script>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
    <link href="<%=request.getContextPath()%>/admin/css/blue/main.css" rel="stylesheet" type="text/css" />
    <script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
    <script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    <script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
    <script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
    <script src="<%=request.getContextPath()%>/admin/gpm/js/admin.js" type="text/javascript"></script>
</head>
<body>
    <div id="head">
        <div id="headLogo">
            <div style="height:23px;"></div>
            <table border="0" cellpadding="0" cellspacing="0" style="display:block;">
                <tr>
                    <td style="width: 230px; height: 30px;">
                        &nbsp;</td>
                    <td>
                        <ul id="tabMenu">
							<c:forEach var="list" items="${bean}" varStatus="status">
							 <c:choose>
	        					<c:when test="${status.count==1}">
							<script type="text/javascript">
							var checkedId = "topMenu_${list.opsId}";
							var firstOpsId = '${list.opsId}';
    						</script>
							<li id="topMenu_${list.opsId}" class="tabMenuOpen" onclick="openTopTab('${list.opsId}');">
                            	<a href="#"><span>${list.opsName}</span></a>
                            </li>
								</c:when>
								<c:otherwise>
                            <li id="topMenu_${list.opsId}" class="tabMenuClose" onclick="openTopTab('${list.opsId}');">
                            	<a href="#"><span>${list.opsName}</span></a>
                            </li>
                            	</c:otherwise>
                            </c:choose>
							</c:forEach>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <ul id="userInfo">
                            <li>管理员：${adminName}，欢迎进入"${webSiteName}"网站后台管理系统</li>
                            <li>今天：${nowDate}</li>
                            <li><a id="bnt_IndexRefresh" title="刷新工作区页面">刷新页面</a></li>
                            <li><a id="bnt_UpdatePassWord" title="修改密码">修改密码</a></li>
                            <li><a id="bnt_IndexExit" title="安全退出">安全退出</a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="wrapper">
        <table border="0" class="tableBox">
            <tr>
                <td id="leftWrapper" valign="top">
                    <div style="height: 57px;">
                        <div style="width: 130px; padding: 30px 0px 0px 50px; color: #416AA3; font-weight: bold;
                            text-align: center; font-size: 14px;">
                            我的工作台</div>
                    </div>
                    <div class="leftMain">
                        <div id="menuContent">
                        </div>
                    </div>
                </td>
                <td id="sideBar" class="button">
                    <a id="switchButton" class="hand" onclick="changeSideBarState();"><img alt="关闭左栏" src="<%=request.getContextPath()%>/admin/imgs/blue/but_close.gif" /></a></td>
                <td id="content" style="height: 100%;">
                    <table class="tableBox">
                        <tr>
                            <td>
                                <div id="contentTop" style="overflow-x:hidden; overflow-y:hidden; overflow:hidden;">
                                    <div id="tab_left" onclick="leftClick();" class="tab-left" onmouseover="this.className='tab-left tab-left-over'" onmouseout="this.className='tab-left'"></div>
                                    <div id="tab_right" onclick="rightClick();" class="tab-right" onmouseover="this.className='tab-right tab-right-over'" onmouseout="this.className='tab-right'"></div>
                                    <div id="tab_content" style="width: 8000px;" class="tab-strip-wrap">
                                        <ul id="menuListUL" style="float: left; margin-left: 0px;">

                                            <li id="iframeTab_li_0" class="tabOpen"><a id="iframeTab_a1_0" onclick="openContentTab('0','<%=request.getContextPath()%>/workSpace.htm');" class="tabOpen_a1">工作台首页</a>
	                                            <a id="iframeTab_a2_0" onclick="closeContentTab('0');" class="tabOpen_a2"><img src="<%=request.getContextPath()%>/admin/imgs/blue/tab-close.gif" alt=""/></a></li>

                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr style="height:100%;background-color:#D4E4F6;">
                            <td>
                                <iframe id="iframeMenuContent" name="iframeMenuContent" frameborder="0" src="<%=request.getContextPath()%>/workSpace.html" style="border: 0; width: 100%; height: 500px; margin: 0;padding: 0;">
                                </iframe>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>