<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>修改密码</title>
    <script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
    <link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
    <script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
    <script type="text/javascript">
    var contextPath = '<%=request.getContextPath()%>';
    </script>
  </head>
  <body>
      <form id="updatePwdForm" name="userForm" action="#">
      <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        <tr class="tdbg" style="height:35px;">
            <td class="tdbgleft" width="130px" align="right"><strong>原密码：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="txt_OldPwd" type="password" style="width:150px;"/></td>
                  <td><span class="cautiontitleRed">*</span></td>
                  <td><div id="txt_OldPwdTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg" style="height:35px;">
            <td class="tdbgleft" align="right"><strong>新密码：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="txt_NewPwd1" type="password" style="width:150px;"/></td>
                  <td><span class="cautiontitleRed">*</span></td>
                  <td><div id="txt_NewPwd1Tip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg" style="height:35px;">
            <td class="tdbgleft" align="right"><strong>再次输入新密码：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="txt_NewPwd2" type="password" style="width:150px;"/></td>
                  <td><span class="cautiontitleRed">*</span></td>
                  <td><div id="txt_NewPwd2Tip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td colspan="2" align="center" style="height:40px;">
            	<input id="bnt_Update" type="button" class="inputbutton" value=" 修 改 "/>&nbsp;&nbsp;
        		<input id="bnt_Close" type="button" class="inputbutton" value=" 关 闭 "/>
            </td>
        </tr>
     </table>
     </form>
  </body>
<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$.formValidator.initConfig({formID:"updatePwdForm",theme:"Small",
		onError:function(msg,obj,errorlist){
		}
	});
	beautifyBottomButton(['bnt_Update','bnt_Close']);
	var regExp = '^[\@A-Za-z0-9\!\#\$\%\^\&\*\.\~\_]{6,20}$';
	$("#txt_OldPwd").formValidator({onShowText:"",onFocus:"请输入原密码"}).inputValidator({min:1,max:20,onErrorMin:'请输入原密码'});
	$("#txt_NewPwd1").formValidator({onShowText:"",onFocus:"为6-20个英文、数字和\"~_!@#$%^&*.\"组合"}).inputValidator({min:6,max:20,onErrorMin:'密码小于6位',onErrorMax:'密码大于20位'}).regexValidator({regExp:regExp,onError:"输入的密码不合法"});
	$("#txt_NewPwd2").formValidator({onShowText:"",onFocus:"请再次输入新密码"}).inputValidator({min:6,max:12,onErrorMin:'请再次输入新密码'});
	$('#bnt_Update').click(function(){
		var isOk = $.formValidator.pageIsValid('1');
		if(!isOk){
			return;
		}
		var waiting = window.parent.alertWaiting('正在提交数据，请等待...');
		$.ajax({
			type:"POST",
			url:contextPath+'/updatePwd.html',
			data:{oldPwd:$('#txt_OldPwd').val(),newPwd1:$('#txt_NewPwd1').val(),newPwd2:$('#txt_NewPwd2').val()},
			dataType:'json',
			contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			success: function(data){
				if(data.code=='200'){
					alert(data.content+"，请重新登陆！");
					waiting.content('正在退出系统，请稍等...');
					$.getJSON(contextPath+'/hLogout.html',function(data1){
						if(data1.code=='200'){
							window.location.href = contextPath+'/login.html';
						}
						else{
							waiting.close();
							alert(data1.content);
						}
					});
				}
				else{
					waiting.close();
					alert(data.content);
				}
			},
			error:function(){
				waiting.close();
			}
		});
	});
	$('#bnt_Close').click(function(){
		window.parent.dialogUpdatePWDClose();
	});
});
</script>
</html>