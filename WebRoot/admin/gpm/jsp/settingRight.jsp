<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>用户权限设置</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<link href="<%=request.getContextPath()%>/admin/common/zTree/css/zTreeStyle.css" rel="stylesheet" type="text/css"/>
		<script src="<%=request.getContextPath()%>/admin/common/zTree/js/jquery.ztree.core-3.5.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/zTree/js/jquery.ztree.excheck-3.5.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/gpm/js/settingRight.js" type="text/javascript"></script>
		<style type="text/css">
		.divRole{
		    display: block;
			width:375px;
		}
		.divRole ul{
			list-style-type:none;
			margin:0px;
			padding:0px;
		}
		.divRole li{
			padding:5px;
			float:left;
		}
		.divRole input{
			cursor:pointer;
		}
		</style>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
	</head>
	<body>
	  <form id="systemForm" name="systemForm" action="#">
      <table id="table_tab1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
      	<tr class="tdbg">
      		<td colspan="2" align="center">
			 <div style="color:#FF0000;">提示：可以选择角色或者组/部门之一为用户设置权限，也可以两者都选择！</div>
      		</td>
      	</tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="135px" align="right">
            	<strong>选择系统：</strong>
            </td>
            <td>
            	<select id="sysSelect" style="cursor:pointer">
					<c:forEach var="list" items="${sysBean}">
					<option id="sys_${list.systemId}" value="${list.systemId}">${list.systemName}</option>
					</c:forEach>
            	</select>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>角色选择：</strong></td>
            <td align="left">
				<div id="div_role" class="divRole">
				</div>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>组/部门选择：</strong></td>
            <td id="td_groups">
				<div id="div_Tree" class="zTreeDemoBackground left">
					<ul id="ul_GroupTree" class="ztree"></ul>
				</div>
            </td>
        </tr>
     </table>
     <table border="0" width="100%" cellpadding="2" cellspacing="1" style="margin-top: 20px;">
     	<tr class="height25">
        	<td colspan="2" align="center">
        		<input id="bnt_Submit" type="button" class="inputbutton" value=" 设置权限 "/>&nbsp;&nbsp;
        		<input id="bnt_Close" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
        	</td>
        </tr>
     </table>
     </form>
	</body>
</html>