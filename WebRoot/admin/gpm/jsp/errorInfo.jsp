<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>错误信息提示</title>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
	</head>
	<body class="mainContent">
        <table width="400" cellspacing="1" cellpadding="2" border="0" align="center" class="tableWrap">
                <tr align="center" class="title">
                    <td>
                        <strong>提示信息</strong></td>
                </tr>
                <tr class="tdbg">
                    <td valign="middle" align="center" height="100">
                        <span style="color:#FF0000;font-size:20px;">${bean.messInfo}！</span>
                    </td>
                </tr>
         </table>
	</body>
</html>