<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>添加用户信息</title>
    <script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
    <script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
    <script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
    <link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
    <script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
    <script src="<%=request.getContextPath()%>/admin/gpm/js/user.js" type="text/javascript"></script>
    <script src="<%=request.getContextPath()%>/admin/common/date/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
      var contextPath = '<%=request.getContextPath()%>';
      var method = '${method}';
      </script>
      <style type="text/css">
      #td_sysList span{border: 1px solid #76A3DA;display:inline-table;margin:2px 4px 2px 0;}
      </style>
  </head>
  <body>
      <form id="userForm" name="userForm" action="#">
    <table border="0" cellpadding="0" cellspacing="0" style="width:244px;">
            <tr align="center">
                <td id="td_tab1" class="titlemouseover" style="width:80px;">基本信息</td>
                <td style="width:2px"></td>
                <td id="td_tab2" class="tabtitle" style="width:80px;">扩展信息</td>
                <td style="width:2px"></td>
                <td id="td_tab3" class="tabtitle" style="width:80px;">系统设置</td>
             </tr>
        </table>
        <c:choose>
      <c:when test="${method eq 'add'}">
      <table id="table_tab1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>用户名称：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="userName" type="text" /></td>
                  <td><span class="cautiontitleRed">*</span></td>
                  <td><div id="userNameTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>密码：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="password" type="text" /></td>
                  <td><span class="cautiontitleRed">*</span></td>
                  <td><div id="passwordTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>真实姓名：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="trueName" type="text" /></td>
                  <td><span class="cautiontitleRed">*</span></td>
                  <td><div id="trueNameTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>是否允许多人同时使用此帐号登录：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><label>是<input id="enableMultiLogin_Yes" type="radio" name="enableMultiLogin" checked="checked"/></label>&nbsp;<label>否<input id="enableMultiLogin_No" type="radio" name="enableMultiLogin"/></label></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right">
            <strong>容许登录错误次数：</strong>
      <div style="color:Red;text-align:left;">当登陆次数超过设置，则账号被锁定。0为不限制</div>
            </td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="loginErrorTime" type="text" value="0" style="width:50px;"/></td>
                  <td><span class="cautiontitleRed">*</span></td>
                  <td><div id="loginErrorTimeTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right">
            <strong>锁定时间长度：</strong>
            <div style="color:Red;text-align:left;">单位:分钟。当为0时，为无限长时间，有管理员解锁</div>
            </td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="lockTime" type="text" value="5" style="width:50px;"/></td>
                  <td><span class="cautiontitleRed">*</span></td>
                  <td><div id="lockTimeTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>用户状态：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td>
                  <label>正常<input id="status_0" type="radio" name="status" checked="checked"/></label>
                  <label>锁定<input id="status_1" type="radio" name="status"/></label>
                  </td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>是否允许修改密码：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td>
                  <label>是<input id="enableModifyPassword_Yes" type="radio" name="enableModifyPassword" checked="checked"/></label>
                  <label>否<input id="enableModifyPassword_No" type="radio" name="enableModifyPassword" /></label>
                  </td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right">
            <strong>账户有效期：</strong><br/>
            <div style="color:Red;text-align:left;">如果不填则无限期</div>
            </td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="accountExpires" type="text" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'%y-%M-%d'})"/></td>
                  <td><div id="accountExpiresTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
     </table>
     <table id="table_tab2" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" style="display:none">
    <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>性别：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td>
                  <select id="sex">
                    <option value="男"> 男 </option>
                    <option value="女"> 女 </option>
                  </select>
                  </td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>职务：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="position" type="text" /></td>
                  <td><div id="positionTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
     <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>Email：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="email" type="text" /></td>
                  <td><div id="emailTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>QQ：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="qq" type="text" /></td>
                  <td><div id="qqTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>办公电话：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="officePhone" type="text" /></td>
                  <td><div id="officePhoneTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>住宅电话：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="homePhone" type="text" /></td>
                  <td><div id="homePhoneTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>移动电话：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="mobile" type="text" /></td>
                  <td><div id="mobileTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>联系地址：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="address" type="text" /></td>
                  <td><div id="addressTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>邮政编码：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="zipCode" type="text" /></td>
                  <td><div id="zipCodeTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>出生日期：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="birthday" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'%y-%M-%d'})"/></td>
                  <td><div id="birthdayTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>证件号：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="idCard" type="text" /></td>
                  <td><div id="idCardTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
     </table>
    <div id="table_tab3" style="border: 1px solid #76A3DA;display:none">
       <div style="margin:0;padding:0">
      <table border="0" cellpadding="0" cellspacing="0" style="width:100%;height:100%">
        <tr class="tdbg">
          <td style="width:120px;"><strong>请给用户选择系统：</strong></td>
          <td id="td_sysList">
          </td>
        </tr>
      </table>
       </div>
       <div style="margin:0;padding:0">
       <iframe id="iframeSysList" frameborder="0" src="<%=request.getContextPath()%>/security/getCheckSysList.html" style="border: 0; width: 100%;height:245px"></iframe>
       </div>
     </div>
     <table border="0" width="100%" cellpadding="2" cellspacing="1" style="margin-top: 20px;">
       <tr class="height25">
          <td colspan="2" align="center">
            <input id="bnt_Save" type="button" class="inputbutton" value=" 保 存 "/>&nbsp;&nbsp;
            <input id="bnt_Reset" type="button" class="inputbutton" value=" 重 置 "/>&nbsp;&nbsp;
            <input id="bnt_Close" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
          </td>
        </tr>
     </table>
     </c:when>
     <c:otherwise>
   <table id="table_tab1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>用户名称：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="userName" type="text" value="${bean.userName}"/></td>
                  <td><span class="cautiontitleRed">*</span></td>
                  <td><div id="userNameTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>真实姓名：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="trueName" type="text" value="${bean.trueName}"/></td>
                  <td><span class="cautiontitleRed">*</span></td>
                  <td><div id="trueNameTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>是否允许多人同时使用此帐号登录：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><label>是<input id="enableMultiLogin_Yes" type="radio" name="enableMultiLogin" <c:if test="${bean.enableMultiLogin}">checked="checked"</c:if>/></label>&nbsp;<label>否<input id="enableMultiLogin_No" type="radio" name="enableMultiLogin" <c:if test="${!bean.enableMultiLogin}">checked="checked"</c:if>/></label></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right">
            <strong>容许登录错误次数：</strong>
      <div style="color:Red;text-align:left;">当登陆次数超过设置，则账号被锁定。0为不限制</div>
            </td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="loginErrorTime" type="text" value="${bean.loginErrorTime}" style="width:50px;"/></td>
                  <td><span class="cautiontitleRed">*</span></td>
                  <td><div id="loginErrorTimeTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right">
            <strong>锁定时间长度：</strong>
            <div style="color:Red;text-align:left;">单位:分钟。当为0时，为无限长时间，有管理员解锁</div>
            </td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="lockTime" type="text" value="5" style="width:50px;" value="${bean.lockTime}"/></td>
                  <td><span class="cautiontitleRed">*</span></td>
                  <td><div id="lockTimeTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>用户状态：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td>
                  <label>正常<input id="status_0" type="radio" name="status" <c:if test="${bean.status==0 }">checked="checked"</c:if>/></label>
                  <label>锁定<input id="status_1" type="radio" name="status" <c:if test="${bean.status==1||bean.status==2 }">checked="checked"</c:if>/></label>
                  </td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>是否允许修改密码：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td>
                  <label>是<input id="enableModifyPassword_Yes" type="radio" name="enableModifyPassword" <c:if test="${enableModifyPassword}">checked="checked"</c:if>/></label>
                  <label>否<input id="enableModifyPassword_No" type="radio" name="enableModifyPassword" <c:if test="${!enableModifyPassword}">checked="checked"</c:if>/></label>
                  </td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right">
            <strong>账户有效期：</strong><br/>
            <div style="color:Red;text-align:left;">如果不填则无限期</div>
            </td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="accountExpires" type="text" class="Wdate" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'%y-%M-%d'})" value="${bean.accountExpires }"/></td>
                  <td><div id="accountExpiresTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
     </table>
     <table id="table_tab2" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" style="display:none">
    <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>性别：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td>
                  <select id="sex">
                    <option value="男" <c:if test="${bean.sex eq '男'}">selected="selected"</c:if>> 男 </option>
                    <option value="女" <c:if test="${bean.sex eq '女'}">selected="selected"</c:if>> 女 </option>
                  </select>
                  </td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>职务：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="position" type="text" value="${bean.position}"/></td>
                  <td><div id="positionTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
     <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>Email：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="email" type="text" value="${bean.email}"/></td>
                  <td><div id="emailTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>QQ：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="qq" type="text" value="${bean.qq}"/></td>
                  <td><div id="qqTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>办公电话：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="officePhone" type="text" value="${bean.officePhone}"/></td>
                  <td><div id="officePhoneTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>住宅电话：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="homePhone" type="text" value="${bean.homePhone}"/></td>
                  <td><div id="homePhoneTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>移动电话：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="mobile" type="text" value="${bean.mobile}"/></td>
                  <td><div id="mobileTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>联系地址：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="address" type="text" value="${bean.address}"/></td>
                  <td><div id="addressTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>邮政编码：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="zipCode" type="text" value="${bean.zipCode}"/></td>
                  <td><div id="zipCodeTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>出生日期：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="birthday" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd',maxDate:'%y-%M-%d'})" value="${bean.birthday}"/></td>
                  <td><div id="birthdayTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>证件号：</strong></td>
            <td>
              <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td><input id="idCard" type="text" value="${bean.idCard}"/></td>
                  <td><div id="idCardTip"></div></td>
                </tr>
              </table>
            </td>
        </tr>
     </table>
    <div id="table_tab3" style="border: 1px solid #76A3DA;display:none">
       <div style="margin:0;padding:0">
      <table border="0" cellpadding="0" cellspacing="0" style="width:100%;height:100%">
        <tr class="tdbg">
          <td style="width:120px;"><strong>请给用户选择系统：</strong></td>
          <td id="td_sysList">
          </td>
        </tr>
      </table>
       </div>
       <div style="margin:0;padding:0">
       <iframe id="iframeSysList" frameborder="0" src="<%=request.getContextPath()%>/security/getCheckSysList.html" style="border: 0; width: 100%;height:245px"></iframe>
       </div>
     </div>
     <table border="0" width="100%" cellpadding="2" cellspacing="1" style="margin-top: 20px;">
       <tr class="height25">
          <td colspan="2" align="center">
            <input id="bnt_Update" type="button" class="inputbutton" value=" 修 改 "/>&nbsp;&nbsp;
            <input id="bnt_Close" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
          </td>
        </tr>
     </table>
     </c:otherwise>
     </c:choose>

     </form>
  </body>
</html>