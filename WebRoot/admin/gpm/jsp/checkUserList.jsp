<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>选择用户</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
    	<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
    	<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	function checkedUser(_uId,_uName){
    		window.parent.checkedUserList(_uId,_uName);
    	}
    	</script>
	</head>
	<body>
	   	<table class="tableWrap" border="0" width="100%" cellpadding="0" cellspacing="0" style="border:0;">
	        <c:choose>
	        <c:when test="${pageBean.totalRecordNum>0}">
	        <tr class="title">
	        	<th>操作</th>
	            <th>用户名</th>
	            <th>真实姓名</th>
	            <th>状态</th>
	        </tr>
	    	<tbody id="tbody_DataList">
		        <c:forEach var="list" items="${pageBean.dataList}">
		        <tr class="tdbg" style="height:25px;">
		        	<td align="center"><a onclick="checkedUser(${list.userId},'${list.userName}');">选择</a></td>
		        	<td align="center">${list.userName}</td>
		        	<td align="center">${list.trueName}</td>
		        	<td align="center"><c:choose><c:when test="${list.status==0}"><div style="color:#00A600">正常</div></c:when><c:when test="${list.status==1||list.status==2}"><div style="color:Red;">锁定</div></c:when></c:choose></td>
		        </tr>
		        </c:forEach>
	        </tbody>
	        <tr align="right" class="tdbg" style="height:28px;">
                <td colspan="4">
                   <c:import url="pageCurrent.jsp">
			        <c:param name="totalRecordNum" value="${pageBean.totalRecordNum}"></c:param>
			        <c:param name="pageRecordNum" value="${pageBean.pageRecordNum}"></c:param>
				   </c:import>
                </td>
            </tr>
	        </c:when>
	        <c:otherwise>
            	<tr class="tdbg" style="height:80px;">
			        <td colspan="4" align="center">
			             <strong class="valignMiddle">没有任何数据！</strong>
			        </td>
		        </tr>
            </c:otherwise>
            </c:choose>
	     </table>
	</body>
</html>