<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>组/部门详情</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<link href="<%=request.getContextPath()%>/admin/common/zTree/css/zTreeStyle.css" rel="stylesheet" type="text/css"/>
		<script src="<%=request.getContextPath()%>/admin/common/zTree/js/jquery.ztree.core-3.5.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	$(document).ready(function(){
    		beautifyBottomButton(['bnt_Close']);
    		$('#bnt_Close').click(function(){
				window.top.frames['iframeMenuContent'].closeDialog(0);
    		});

    		parseUri.init();
    		var _id = parseUri.getParam('id');
    		var url1 = contextPath+'/security/getRolesByGroupId.html?id='+_id;
			$.get(url1,function(data){
				if(data.code=='200'){
					var _array = new Array();
					$.each(data.objContent,function(i,item){
						_array.push(item.roleName);
					});
					if(_array.length>0){
						$('#td_Roles').html(_array.join(','));
					}
					else{
						$('#td_Roles').html('无数据');
					}
				}
				else{
					alert(data.content);
				}
			},'json');

			var url2 = contextPath+'/security/getOpsByGroupId.html?id='+_id;
			var nodes = [];
			var setting = {
				data: {
					simpleData: {
						enable: true
					}
				}
			};
			$.get(url2,function(data){
				if(data.code=='200'){
					if(data.objContent.length>0){
						$.each(data.objContent,function(i,item){
							var itemNode = {};
						    itemNode.id = item.opsId;
							itemNode.name = item.opsName;
							itemNode.pId = item.parentId;
							itemNode.open = true;
							if(item.opsType==1){
								itemNode.icon = contextPath+'/admin/imgs/blue/opt_menu.png';
							}
							else if(item.opsType==2){
								itemNode.icon = contextPath+'/admin/imgs/blue/opt_page.png';
							}
							else if(item.opsType==3){
								itemNode.icon = contextPath+'/admin/imgs/blue/opt_operate.png';
							}
							else{
							}
							nodes[i] = itemNode;
						});
						$.fn.zTree.init($("#ul_Tree"), setting, nodes);
					}
					else{
						$('#div_Tree').html('无数据');
					}
				}
				else{
					alert(data.content);
				}
			},'json');
    	});
    	</script>
	</head>
	<body>
	    <table id="table_tab1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        <tr class="tdbg">
            <td class="tdbgleft" width="135px" align="right"><strong>所属系统：</strong></td>
            <td>${bean.systemName }</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="135px" align="right"><strong>父节点：</strong></td>
            <td>${bean.parentName }</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>组名称：</strong></td>
            <td>${bean.groupName }</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>是否继承父节点权限：</strong></td>
            <td>
			<c:choose><c:when test="${bean.inherit}"><div style="color:#00A600">是</div></c:when><c:otherwise><div style="color:Red;">否</div></c:otherwise></c:choose>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>拥有的角色：</strong></td>
            <td id="td_Roles"></td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>拥有的操作：</strong></td>
            <td>
            	<div id="div_Tree" class="zTreeDemoBackground left">
					<ul id="ul_Tree" class="ztree"></ul>
				</div>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>创建时间：</strong></td>
            <td>${bean.createTime }</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>创建人：</strong></td>
            <td>${bean.createUser }</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>修改时间：</strong></td>
            <td>${bean.lastEditTime }</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>修改人：</strong></td>
            <td>${bean.lastEditUser }</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>组描述：</strong>
            </td>
            <td>${bean.groupDesc }</td>
        </tr>
     </table>
     <table border="0" width="100%" cellpadding="2" cellspacing="1" style="margin-top: 20px;">
     	<tr class="height25">
        	<td colspan="2" align="center">
        		<input id="bnt_Close" type="button" class="inputbutton" value=" 关 闭 "/>
        	</td>
        </tr>
     </table>
	</body>
</html>