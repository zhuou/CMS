<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>添加系统信息</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/gpm/js/system.js" type="text/javascript"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	var method = '${method}';
    	</script>
    	<style type="text/css">
    	#td_userList span{border: 1px solid #76A3DA;display:inline-table;margin:2px 4px 2px 0;}
    	</style>
	</head>
	<body>
	    <form id="systemForm" name="systemForm" action="#">
		<table border="0" cellpadding="0" cellspacing="0" style="width:162px;">
            <tr align="center">
                <td id="td_tab1" class="titlemouseover" style="width:80px;">基本信息</td>
                <td style="width:2px"></td>
                <td id="td_tab2" class="tabtitle" style="width:80px;">用户设置</td>
             </tr>
        </table>
	    <c:choose>
	    <c:when test="${method eq 'add'}">
	    <table id="table_tab1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        <tr class="tdbg">
            <td class="tdbgleft" width="135px" align="right"><strong>系统名称：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="systemName" type="text" /></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="systemNameTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>系统IP：</strong>
            <div style="color:Red;text-align:left;">用于系统鉴权验证使用</div>
            </td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="systemIP" type="text" /></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="systemIPTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>域名或系统IP+端口：</strong>
            <div style="color:Red;text-align:left;">用于拼接请求地址使用</div>
            </td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="hostAddr" type="text" /></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="hostAddrTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>密钥：</strong><br/>
            <span style="color:Red;">系统通信签证信息</span>
            </td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="secretKey" type="text" style="width:105px;"/></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="secretKeyTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>系统描述：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0" style="height:105px;">
	            	<tr>
	            		<td><textarea id="systemDesc" style="height:80px;width:250px;" cols="20" rows="2" name="txt_Intr"></textarea></td>
	            	</tr>
	            	<tr>
	            		<td><div id="systemDescTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
     </table>
     <div id="table_tab2" style="border: 1px solid #76A3DA;display:none">
	     <div style="margin:0;padding:0">
			<table border="0" cellpadding="0" cellspacing="0" style="width:100%;height:100%">
				<tr class="tdbg">
					<td style="width:120px;"><strong>请给系统选择用户：</strong></td>
					<td id="td_userList">
					</td>
				</tr>
			</table>
	     </div>
	     <div style="margin:0;padding:0">
	     <iframe id="iframeUserList" frameborder="0" src="<%=request.getContextPath()%>/security/getCheckUserList.html" style="border: 0; width: 100%;height:245px"></iframe>
	     </div>
     </div>
     <table border="0" width="100%" cellpadding="2" cellspacing="1" style="margin-top: 20px;">
     	<tr class="height25">
        	<td colspan="2" align="center">
        		<input id="bnt_Save" type="button" class="inputbutton" value=" 保 存 "/>&nbsp;&nbsp;
        		<input id="bnt_Reset" type="button" class="inputbutton" value=" 重 置 "/>&nbsp;&nbsp;
        		<input id="bnt_Close" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
        	</td>
        </tr>
     </table>
     </c:when>
     <c:otherwise>
	 <table id="table_tab1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        <tr class="tdbg">
            <td class="tdbgleft" width="135px" align="right"><strong>系统名称：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="systemName" type="text" value="${bean.systemName}"/></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="systemNameTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>系统IP：</strong>
            <div style="color:Red;text-align:left;">用于系统鉴权验证使用</div>
            </td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="systemIP" type="text" value="${bean.systemIP}"/></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="systemIPTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>域名或系统IP+端口：</strong>
            <div style="color:Red;text-align:left;">用于拼接请求地址使用</div>
            </td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="hostAddr" type="text" value="${bean.hostAddr}"/></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="hostAddrTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>密钥：</strong><br/>
            <span style="color:Red;">系统通信签证信息</span>
            </td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="secretKey" type="text" style="width:105px;" value="${bean.secretKey}"/></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="secretKeyTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>系统描述：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0" style="height:105px;">
	            	<tr>
	            		<td><textarea id="systemDesc" style="height:80px;width:250px;" cols="20" rows="2" name="txt_Intr">${bean.systemDesc}</textarea></td>
	            	</tr>
	            	<tr>
	            		<td><div id="systemDescTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
     </table>
     <div id="table_tab2" style="border: 1px solid #76A3DA;display:none">
	     <div style="margin:0;padding:0">
			<table border="0" cellpadding="0" cellspacing="0" style="width:100%;height:100%">
				<tr class="tdbg">
					<td style="width:120px;"><strong>请给系统选择用户：</strong></td>
					<td id="td_userList">
					</td>
				</tr>
			</table>
	     </div>
	     <div style="margin:0;padding:0">
	     <iframe id="iframeUserList" frameborder="0" src="<%=request.getContextPath()%>/security/getCheckUserList.html" style="border: 0; width: 100%;height:245px"></iframe>
	     </div>
     </div>
     <table border="0" width="100%" cellpadding="2" cellspacing="1" style="margin-top: 20px;">
     	<tr class="height25">
        	<td colspan="2" align="center">
        		<input id="bnt_Update" type="button" class="inputbutton" value=" 修 改 "/>&nbsp;&nbsp;
        		<input id="bnt_Close" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
        	</td>
        </tr>
     </table>
     </c:otherwise>
     </c:choose>
     </form>
	</body>
</html>