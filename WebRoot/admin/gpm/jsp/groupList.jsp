<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>组信息管理</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/common/zTree/css/zTreeStyle.css" rel="stylesheet" type="text/css"/>
		<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/zTree/js/jquery.ztree.core-3.5.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/gpm/js/groupList.js" type="text/javascript"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
		var zNodes =[
			{id:-1, pId:0, name:"请选择您要操作的系统", open:true, icon:contextPath+'/admin/imgs/blue/choose.png'}
			<c:forEach var="list" items="${sysList}" varStatus="status">
			,{id:${list.systemId}, pId:-1, name:'${list.systemName}', open:true, icon:contextPath+'/admin/imgs/blue/sys_icon.png'}
			</c:forEach>
		];
		var setting = {
			data: {
				simpleData: {
					enable: true
				}
			},
			callback: {
				onClick:sysTreeClick
			}
		};
		function sysTreeClick(event, treeId, treeNode){
			if(treeNode.level==0){
				return;
			}
			if(_sysId!=treeNode.id){
				$.fn.zTree.getZTreeObj("treeOpsList").destroy();
				_sysId = treeNode.id;
				_sysName = treeNode.name;
				loadTree();
			}
		};
    	</script>
	</head>
	<body class="mainContent">
	    <span class="localSpan">组/部门管理</span>
	   	<table class="tableWrap" border="0" width="100%" cellpadding="1" cellspacing="1">
	        <tr>
	        	<td align="center" class="tdbg" style="height:25px;width:320px">
	        		<div style="font-weight:bolder;">系统选择</div>
	        	</td>
	        	<td align="left" class="tdbg topbottom">
					<input id="group_Add" class="topinput topinput_add" type="button" value="新建"/>
	            	<input id="group_Update" class="topinput topinput_edit" type="button" value="修改"/>
	            	<input id="group_Del" class="topinput topinput_del" type="button" value="删除选定选"/>
	            	<input id="group_Detail" class="topinput topinput_detail" type="button" value="查看详情"/>
	        	</td>
	        </tr>
	         <tr>
	        	<td align="center" class="tdbg" valign="top">
	        		<div class="zTreeDemoBackground left">
						<ul id="treeSysList" class="ztree"></ul>
					</div>
	        	</td>
	        	<td id="td_Tree" align="center" class="tdbg">
					<div id="div_treeOps" class="zTreeDemoBackground left">
						<ul id="treeOpsList" class="ztree"></ul>
					</div>
					<div id="div_Message"></div>
	        	</td>
	        </tr>
	     </table>
	</body>
</html>