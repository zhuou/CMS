<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>添加功能操作信息</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/gpm/js/optInfo.js" type="text/javascript"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	var _method = '${method}';
    	var _sysId = '${bean.sysBean.systemId}';
    	var _parentId = '${bean.parentId}';
    	var _sysName = '${bean.sysBean.systemName}';
    	var _parentName = '${bean.parentName}';
    	var _opsType = '${bean.opsType}';
    	var _status = '${bean.status}';
    	var _opsId = '${bean.opsId}';
    	</script>
    	<style>
    	.spanBnt{vertical-align:bottom;font-weight:bold;}
    	.spanBnt a{color:#0000FF;text-decoration:underline;}
    	</style>
	</head>
	<body>
	    <form id="opsInfoForm" name="systemForm" action="#">
	    <c:choose>
	    <c:when test="${method eq 'add'}">
	    <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        <tr class="tdbg">
            <td class="tdbgleft" width="135px" align="right"><strong>所属系统：</strong></td>
            <td><span id="systemName" style="font-weight:bold;"></span></td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>父节点：</strong></td>
            <td>
            	<input id="parentName" type="text" class="input_public" disabled="disabled"/>&nbsp;<span class="spanBnt">&lt;=<a>无父节点</a></span>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>操作名称：</strong>
            </td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
            		<tr>
            			<td><input id="opsName" type="text" /></td>
            			<td><span class="cautiontitleRed">*</span></td>
            			<td><div id="opsNameTip"></div></td>
            		</tr>
            	</table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>操作类型：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td>
	            			<select id="opsType">
								<option value="1">菜单</option>
								<option value="2">页面</option>
								<option value="3">按钮操作</option>
							</select>
	            		</td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>排序：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="sort" type="text" value="0" style="width:50px;"/></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="sortTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>操作URL地址：</strong>
            </td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="opsUrl" type="text" /></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="opsUrlTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>操作icon图标路径：</strong>
            </td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="opsIcon" type="text" /></td>
	            		<td><div id="opsIconTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>操作状态：</strong>
            </td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td>
	            			<span><label>启用</label><input id="status_open" type="radio" checked="checked" name="ops_status" /></span>
	            			<span><label>停用</label><input id="status_close" type="radio" name="ops_status" /></span>
	            		</td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg" style="height:115px;">
            <td class="tdbgleft" width="105px" align="right"><strong>操作描述：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><textarea id="opsDesc" style="height:80px;width:250px;" cols="20" rows="2"></textarea></td>
	            	</tr>
	            	<tr>
	            		<td><div id="opsDescTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
     </table>
     <table border="0" width="100%" cellpadding="2" cellspacing="1" style="margin-top: 20px;">
     	<tr class="height25">
        	<td colspan="2" align="center">
        		<input id="bnt_Save" type="button" class="inputbutton" value=" 保 存 "/>&nbsp;&nbsp;
        		<input id="bnt_Reset" type="button" class="inputbutton" value=" 重 置 "/>&nbsp;&nbsp;
        		<input id="bnt_Close" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
        	</td>
        </tr>
     </table>
      </c:when>
     <c:otherwise>
      <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        <tr class="tdbg">
            <td class="tdbgleft" width="135px" align="right"><strong>所属系统：</strong></td>
            <td><span id="systemName" style="font-weight:bold;">${bean.sysBean.systemName}</span></td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>父节点：</strong></td>
            <td>
            	<input id="parentName" type="text" class="input_public" value="${bean.parentName}" disabled="disabled"/>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>操作名称：</strong>
            </td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
            		<tr>
            			<td><input id="opsName" type="text" value="${bean.opsName}"/></td>
            			<td><span class="cautiontitleRed">*</span></td>
            			<td><div id="opsNameTip"></div></td>
            		</tr>
            	</table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>操作类型：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td>
	            			<select id="opsType">
								<option value="1">菜单</option>
								<option value="2">页面</option>
								<option value="3">按钮操作</option>
							</select>
	            		</td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>排序：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="sort" type="text" style="width:50px;" value="${bean.sort}"/></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="sortTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>操作URL地址：</strong>
            </td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="opsUrl" type="text" value="${bean.opsUrl}"/></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="opsUrlTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>操作icon图标路径：</strong>
            </td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="opsIcon" type="text" value="${bean.opsIcon}"/></td>
	            		<td><div id="opsIconTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>操作状态：</strong>
            </td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td>
	            			<span><label>启用</label><input id="status_open" type="radio" name="ops_status" <c:if test="${bean.status==1 }">checked="checked"</c:if>/></span>
	            			<span><label>停用</label><input id="status_close" type="radio" name="ops_status" <c:if test="${bean.status==0 }">checked="checked"</c:if>/></span>
	            		</td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg" style="height:115px;">
            <td class="tdbgleft" width="105px" align="right"><strong>操作描述：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><textarea id="opsDesc" style="height:80px;width:250px;" cols="20" rows="2">${bean.opsDesc}</textarea></td>
	            	</tr>
	            	<tr>
	            		<td><div id="opsDescTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
     </table>
     <table border="0" width="100%" cellpadding="2" cellspacing="1" style="margin-top: 20px;">
     	<tr class="height25">
        	<td colspan="2" align="center">
        		<input id="bnt_Update" type="button" class="inputbutton" value=" 修 改 "/>&nbsp;&nbsp;
        		<input id="bnt_Close" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
        	</td>
        </tr>
     </table>
     </c:otherwise>
     </c:choose>
     </form>
	</body>
</html>