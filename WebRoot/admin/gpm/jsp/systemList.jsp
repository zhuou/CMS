<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>系统信息管理</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/gpm/js/systemList.js" type="text/javascript"></script>
    	<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
	</head>
	<body class="mainContent">
	    <form id="systemForm" name="systemForm" action="#">
	    <span class="localSpan">系统信息管理</span>
	   	<table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
	        <tr class="topbottom">
	            <td colspan="10">
	            	<input id="system_Add" class="topinput topinput_add" type="button" value="新建"/>
	            	<input id="system_Update" class="topinput topinput_edit" type="button" value="修改"/>
	            	<input id="system_Del" class="topinput topinput_del" type="button" value="删除选定选"/>
	            	<input id="system_Detail" class="topinput topinput_detail" type="button" value="查看详情"/>
	            </td>
	        </tr>
	        <c:choose>
	        <c:when test="${pageBean.totalRecordNum>0}">
	        <tr class="title">
	            <th style="width:8%;">系统ID</th>
	            <th style="width:18%;">系统名称</th>
	            <th style="width:10%;">系统IP</th>
	            <th style="width:12%;">域名或系统IP+端口</th>
	            <th style="width:12%;">密钥</th>
	            <th style="width:12%;">创建时间</th>
	            <th style="width:12%;">创建人</th>
	            <th style="width:20%;">系统描述</th>
	        </tr>
	        <tbody id="tbody_DataList">
	        <c:forEach var="list" items="${pageBean.dataList}">
	        <tr class="tdbg" value="${list.systemId}">
	        	<td align="center"><a onclick="sysDetail('${list.systemId}');">${list.systemId}</a></td>
	        	<td align="center">${list.systemName}</td>
	        	<td align="center">${list.systemIP}</td>
	        	<td align="center">${list.hostAddr}</td>
	        	<td align="center">${list.secretKey}</td>
	        	<td align="center">${list.createTime}</td>
	        	<td align="center">${list.createUser}</td>
	        	<td align="center">${list.systemDesc}</td>
	        </tr>
	        </c:forEach>
	        </tbody>
	        <tr align="right" class="tdbg" style="height:28px;">
                <td colspan="8">
                   <c:import url="pageCurrent.jsp">
			        <c:param name="totalRecordNum" value="${pageBean.totalRecordNum}"></c:param>
			        <c:param name="pageRecordNum" value="${pageBean.pageRecordNum}"></c:param>
				   </c:import>
                </td>
            </tr>
	        </c:when>
	        <c:otherwise>
            	<tr class="tdbg" style="height:80px;">
			        <td colspan="8" align="center">
			             <strong class="valignMiddle">没有任何数据！</strong>
			        </td>
		        </tr>
            </c:otherwise>
            </c:choose>
	     </table>
     </form>
	</body>
</html>