<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>系统选择</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
    	<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
    	<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	function checkedSys(_sId,_sName){
    		window.parent.checkedSysList(_sId,_sName);
    	}
    	</script>
	</head>
	<body>
	   	<table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" style="border:0">
	        <c:choose>
	        <c:when test="${pageBean.totalRecordNum>0}">
	        <tr class="title">
	        	<th>操作</th>
	            <th>系统ID</th>
	            <th>系统名称</th>
	            <th>创建人</th>
	        </tr>
	        <tbody id="tbody_DataList">
	        <c:forEach var="list" items="${pageBean.dataList}">
	        <tr class="tdbg">
	        	<td align="center"><a onclick="checkedSys(${list.systemId},'${list.systemName}');">选择</a></td>
	        	<td align="center">${list.systemId}</td>
	        	<td align="center">${list.systemName}</td>
	        	<td align="center">${list.createUser}</td>
	        </tr>
	        </c:forEach>
	        </tbody>
	        <tr align="right" class="tdbg" style="height:28px;">
                <td colspan="4">
                   <c:import url="pageCurrent.jsp">
			        <c:param name="totalRecordNum" value="${pageBean.totalRecordNum}"></c:param>
			        <c:param name="pageRecordNum" value="${pageBean.pageRecordNum}"></c:param>
				   </c:import>
                </td>
            </tr>
	        </c:when>
	        <c:otherwise>
            	<tr class="tdbg" style="height:80px;">
			        <td colspan="4" align="center">
			             <strong class="valignMiddle">没有任何数据！</strong>
			        </td>
		        </tr>
            </c:otherwise>
            </c:choose>
	     </table>
	</body>
</html>