<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>用户管理</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/gpm/js/userList.js" type="text/javascript"></script>
    	<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
	</head>
	<body class="mainContent">
	    <form id="systemForm" name="systemForm" action="#">
	    <span class="localSpan">用户管理</span>
	   	<table class="tableWrap" border="0" width="100%" cellpadding="1" cellspacing="1">
	       <tr class="topbottom">
	            <td colspan="10">
	            	<input id="user_Add" class="topinput topinput_add" type="button" value="新建"/>
	            	<input id="user_Update" class="topinput topinput_edit" type="button" value="修改"/>
	            	<input id="user_Del" class="topinput topinput_del" type="button" value="删除选定选"/>
	            	<input id="user_Settings" class="topinput topinput_settings" type="button" value="权限设置"/>
	            	<input id="user_lock" class="topinput topinput_lock" type="button" value="锁定用户"/>
	            	<input id="user_unlock" class="topinput topinput_unlock" type="button" value="解除锁定"/>
	            	<input id="user_Detail" class="topinput topinput_detail" type="button" value="查看详情"/>
	            </td>
	        </tr>
	        <c:choose>
	        <c:when test="${pageBean.totalRecordNum>0}">
	        <tr class="title">
	            <th>用户名</th>
	            <th>真实姓名</th>
	            <th>账户有效期</th>
	            <th>允许多人同时使用此帐号登录</th>
	            <th>是否允许修改密码</th>
	            <th>状态</th>
	            <th>登录次数</th>
	            <th>最近登录IP</th>
	            <th>最近登录时间</th>
	        </tr>
	    	<tbody id="tbody_DataList">
		        <c:forEach var="list" items="${pageBean.dataList}">
		        <tr class="tdbg" style="height:25px;" value="${list.userId}">
		        	<td align="center"><a onclick="userDetail('${list.userId}');">${list.userName}</a></td>
		        	<td align="center">${list.trueName}</td>
		        	<td align="center"><c:choose><c:when test="${list.accountExpires!=null}">${list.accountExpires}</c:when><c:otherwise>无限期</c:otherwise></c:choose></td>
		        	<td align="center"><c:choose><c:when test="${list.enableMultiLogin}"><div style="color:#00A600">是</div></c:when><c:otherwise><div style="color:Red;">否</div></c:otherwise></c:choose></td>
		        	<td align="center"><c:choose><c:when test="${list.enableModifyPassword}"><div style="color:#00A600">是</div></c:when><c:otherwise><div style="color:Red;">否</div></c:otherwise></c:choose></td>
		        	<td align="center"><c:choose><c:when test="${list.status==0}"><div style="color:#00A600">正常</div></c:when><c:when test="${list.status==1||list.status==2}"><div style="color:Red;">锁定</div></c:when></c:choose></td>
		        	<td align="center">${list.loginCount}</td>
		        	<td align="center">${list.lastLoginIP}</td>
		        	<td align="center">${list.lastLoginTime}</td>
		        </tr>
		        </c:forEach>
	        </tbody>
	        <tr align="right" class="tdbg" style="height:28px;">
                <td colspan="10">
                   <c:import url="pageCurrent.jsp">
			        <c:param name="totalRecordNum" value="${pageBean.totalRecordNum}"></c:param>
			        <c:param name="pageRecordNum" value="${pageBean.pageRecordNum}"></c:param>
				   </c:import>
                </td>
            </tr>
	        </c:when>
	        <c:otherwise>
            	<tr class="tdbg" style="height:80px;">
			        <td colspan="10" align="center">
			             <strong class="valignMiddle">没有任何数据！</strong>
			        </td>
		        </tr>
            </c:otherwise>
            </c:choose>
	     </table>
     </form>
	</body>
</html>