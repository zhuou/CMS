<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>系统信息详情</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	$(document).ready(function(){
	    	beautifyBottomButton(['bnt_Ok']);
	    	$("#bnt_Ok").click(function(){
				window.top.frames['iframeMenuContent'].closeDialog();
			});
			parseUri.init();
			var _sysId = parseUri.getParam('id');
			var _uArray = new Array();
			$.getJSON(contextPath+'/security/getUsersBySysId.html?id='+_sysId,function(data){
				if(data.code=='200'){
					$.each(data.objContent,function(i,item){
						_uArray.push(item.userName);
					});
					$('#td_userList').append(_uArray.join(','));
				}
				else{
					alert(data.content);
				}
			});
		});
    	</script>
	</head>
	<body>
	    <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
	     <tr class="tdbg">
            <td class="tdbgleft" width="135px" align="right"><strong>系统ID：</strong></td>
            <td>${bean.systemId}</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="135px" align="right"><strong>系统名称：</strong></td>
            <td>${bean.systemName}</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>系统IP：</strong></td>
            <td>${bean.systemIP}</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>域名或系统IP+端口：</strong></td>
            <td>${bean.hostAddr}</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>密钥：</strong><br/>
            </td>
            <td>${bean.secretKey}</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>通信签证密文：</strong><br/>
            </td>
            <td>${bean.signed}</td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>系统拥有用户：</strong><br/>
            </td>
            <td id="td_userList"></td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>系统创建者：</strong><br/>
            </td>
            <td>${bean.createUser}</td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>系统创建时间：</strong><br/>
            </td>
            <td>${bean.createTime}</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>系统最后修改者：</strong><br/>
            </td>
            <td>${bean.lastEditUser}</td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>系统最后修改时间：</strong><br/>
            </td>
            <td>${bean.lastEditTime}</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>系统描述：</strong></td>
            <td>${bean.systemDesc}</td>
        </tr>
     </table>
     <table border="0" width="100%" cellpadding="2" cellspacing="1" style="margin-top: 20px;">
     	<tr class="height25">
        	<td colspan="2" align="center">
        		<input id="bnt_Ok" type="button" class="inputbutton" value=" 确 定 "/>&nbsp;&nbsp;
        	</td>
        </tr>
     </table>
	</body>
</html>