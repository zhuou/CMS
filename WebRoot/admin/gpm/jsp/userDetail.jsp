<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>用户信息详情</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	$(document).ready(function(){
    		pageTab({'td_tab1':'table_tab1','td_tab2':'table_tab2','td_tab3':'table_tab3'});
			$("#bnt_Ok").click(function(){
				window.top.frames['iframeMenuContent'].closeDialog(0);
			});

			parseUri.init();
			var _uId = parseUri.getParam('id');
			var _sArray = new Array();
			$.getJSON(contextPath+'/security/getSysByUId.html?id='+_uId,function(data){
				if(data.code=='200'){
					$.each(data.objContent,function(i,item){
						_sArray.push(item.systemName);
					});
					$('#td_sysList').append(_sArray.join(','));
				}
				else{
					alert(data.content);
				}
			});

    	});
    	</script>
	</head>
	<body>
	    <form id="userForm" name="userForm" action="#">
	   <table border="0" cellpadding="0" cellspacing="0" style="width:244px;">
            <tr align="center">
                <td id="td_tab1" class="titlemouseover" style="width:80px;">基本信息</td>
                <td style="width:2px"></td>
                <td id="td_tab2" class="tabtitle" style="width:80px;">扩展信息</td>
                <td style="width:2px"></td>
                <td id="td_tab3" class="tabtitle" style="width:80px;">系统设置</td>
             </tr>
        </table>
	    <table id="table_tab1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
	     <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>用户ID：</strong></td>
            <td>
            ${bean.userId}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>用户名称：</strong></td>
            <td>
            ${bean.userName}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>真实姓名：</strong></td>
            <td>
            ${bean.trueName}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>是否允许多人同时使用此帐号登录：</strong></td>
            <td>
            <c:choose><c:when test="${bean.enableModifyPassword}"><div style="color:#00A600">是</div></c:when><c:otherwise><div style="color:Red;">否</div></c:otherwise></c:choose>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right">
            <strong>容许登录错误次数：</strong>
            </td>
            <td>
            ${bean.loginErrorTime}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right">
            <strong>锁定时间长度：</strong>
            </td>
            <td>
            ${bean.lockTime}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>用户状态：</strong></td>
            <td>
            <c:choose><c:when test="${bean.status==0}"><div style="color:#00A600">正常</div></c:when><c:when test="${bean.status==1||bean.status==2}"><div style="color:Red;">锁定</div></c:when></c:choose>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>是否允许修改密码：</strong></td>
            <td>
            <c:choose><c:when test="${bean.enableModifyPassword}"><div style="color:#00A600">是</div></c:when><c:otherwise><div style="color:Red;">否</div></c:otherwise></c:choose>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right">
            <strong>账户有效期：</strong>
            </td>
            <td>
            <c:choose><c:when test="${bean.accountExpires!=null}">${bean.accountExpires}</c:when><c:otherwise>无限期</c:otherwise></c:choose>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>登录次数：</strong></td>
            <td>
            ${bean.loginCount}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>最近登录IP：</strong></td>
            <td>
            ${bean.lastLoginIP}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>最近登录时间：</strong></td>
            <td>
            ${bean.lastLoginTime}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>最近退出时间：</strong></td>
            <td>
            ${bean.lastLogoutTime}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>最近修改密码时间：</strong></td>
            <td>
            ${bean.lastModifyPasswordTime}
            </td>
        </tr>
     </table>
     <table id="table_tab2" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" style="display:none">
		 <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>性别：</strong></td>
            <td>
            ${bean.sex}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>职务：</strong></td>
            <td>
            ${bean.position}
            </td>
        </tr>
		 <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>Email：</strong></td>
            <td>
            ${bean.email}
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>QQ：</strong></td>
            <td>
            ${bean.qq}
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>办公电话：</strong></td>
            <td>
            ${bean.officePhone}
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>住宅电话：</strong></td>
            <td>
            ${bean.homePhone}
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>移动电话：</strong></td>
            <td>
            ${bean.mobile}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>联系地址：</strong></td>
            <td>
            ${bean.address}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>邮政编码：</strong></td>
            <td>
            ${bean.zipCode}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>出生日期：</strong></td>
            <td>
            ${bean.birthday}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>证件号：</strong></td>
            <td>
            ${bean.idCard}
            </td>
        </tr>
     </table>
     <table id="table_tab3" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" style="display:none">
		 <tr class="tdbg">
            <td class="tdbgleft" width="150px" align="right"><strong>用户所属系统：</strong></td>
            <td id="td_sysList"></td>
        </tr>
      </table>
     <table border="0" width="100%" cellpadding="2" cellspacing="1" style="margin-top: 20px;">
     	<tr class="height25">
        	<td colspan="2" align="center">
        		<input id="bnt_Ok" type="button" class="inputbutton" value=" 确 定 "/>&nbsp;&nbsp;
        	</td>
        </tr>
     </table>

     </form>
	</body>
</html>