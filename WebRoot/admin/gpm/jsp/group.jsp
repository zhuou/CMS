<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>添加组信息</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/common/zTree/css/zTreeStyle.css" rel="stylesheet" type="text/css"/>
		<script src="<%=request.getContextPath()%>/admin/common/zTree/js/jquery.ztree.core-3.5.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/zTree/js/jquery.ztree.excheck-3.5.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/gpm/js/group.js" type="text/javascript"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
		var method = '${method}';
    	</script>
    	<style type="text/css">
    	.spanBnt{vertical-align:bottom;font-weight:bold;}
    	.spanBnt a{color:#0000FF;text-decoration:underline;}
    	</style>
	</head>
	<body>
	    <form id="systemForm" name="systemForm" action="#">
	    <table border="0" cellpadding="0" cellspacing="0" style="width:162px;">
            <tr align="center">
                <td id="td_tab1" class="titlemouseover" style="width:80px;">基本信息</td>
                <td style="width:2px"></td>
                <td id="td_tab2" class="tabtitle" style="width:80px;">权限设置</td>
             </tr>
        </table>
        <c:choose>
	    <c:when test="${method eq 'add'}">
	    <table id="table_tab1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        <tr class="tdbg">
            <td class="tdbgleft" width="135px" align="right"><strong>所属系统：</strong></td>
            <td>
	            <input id="systemName" type="text" class="input_public" disabled="disabled"/>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="135px" align="right"><strong>父节点：</strong></td>
            <td>
	            <input id="parentName" type="text" class="input_public" disabled="disabled"/>&nbsp;<span class="spanBnt">&lt;=<a>无父节点</a></span>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>组名称：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="groupName" type="text" /></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="groupNameTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>是否继承父节点权限：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td>
	            			<span><label>否</label><input id="status_no" type="radio" name="groupInherit" checked="checked"/></span>
	            			<span><label>是</label><input id="status_yes" type="radio" name="groupInherit" /></span>
	            		</td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>组描述：</strong>
            </td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><textarea id="groupDesc" style="height:80px;width:250px;" cols="20" rows="2"></textarea></td>
	            	</tr>
	            	<tr>
	            		<td><div id="groupDescTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
     </table>
     <table id="table_tab2" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" style="display:none;">
		 <tr class="tdbg">
			 <td class="tdbgleft" width="105px" align="right">
	            <strong>角色设置：</strong>
	         </td>
	         <td id="td_roleId">
	         	<c:forEach var="list" items="${roleList}" varStatus="status">
				<span><label>${list.roleName}</label><input id="role_${list.roleId}" type="checkbox" value="${list.roleId}"/></span>
				<c:if test="${status.count%3==0}">
				<br/>
				</c:if>
				</c:forEach>
	         </td>
	     </tr>
	     <tr class="tdbg">
			 <td class="tdbgleft" width="105px" align="right">
	            <strong>操作设置：</strong>
	         </td>
	         <td>
				<div id="div_Tree" class="zTreeDemoBackground left">
					<ul id="ul_Tree" class="ztree"></ul>
				</div>
				<div id="div_Message"></div>
	         </td>
	     </tr>
     </table>
     <table border="0" width="100%" cellpadding="2" cellspacing="1" style="margin-top: 20px;">
     	<tr class="height25">
        	<td colspan="2" align="center">
        		<input id="bnt_Save" type="button" class="inputbutton" value=" 保 存 "/>&nbsp;&nbsp;
        		<input id="bnt_Reset" type="button" class="inputbutton" value=" 重 置 "/>&nbsp;&nbsp;
        		<input id="bnt_Close" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
        	</td>
        </tr>
     </table>
     </c:when>
     <c:otherwise>
	  	    <table id="table_tab1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        <tr class="tdbg">
            <td class="tdbgleft" width="135px" align="right"><strong>所属系统：</strong></td>
            <td>
	            <input id="systemName" type="text" class="input_public" value="${bean.systemName }" disabled="disabled"/>
	            <input id="hidden_systemId" type="hidden" value="${bean.systemId }"/>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="135px" align="right"><strong>父节点：</strong></td>
            <td>
	            <input id="parentName" type="text" class="input_public" value="${bean.parentName }" disabled="disabled"/>&nbsp;<span class="spanBnt">&lt;=<a>无父节点</a></span>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>组名称：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="groupName" type="text" value="${bean.groupName }"/><input id="hidden_groupId" type="hidden" value="${bean.groupId }"/></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="groupNameTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>是否继承父节点权限：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td>
	            			<span><label>否</label><input id="status_no" type="radio" name="groupInherit" <c:if test="${!bean.inherit }">checked="checked"</c:if>/></span>
	            			<span><label>是</label><input id="status_yes" type="radio" name="groupInherit" <c:if test="${bean.inherit }">checked="checked"</c:if>/></span>
	            		</td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right">
            <strong>组描述：</strong>
            </td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><textarea id="groupDesc" style="height:80px;width:250px;" cols="20" rows="2">${bean.groupDesc }</textarea></td>
	            	</tr>
	            	<tr>
	            		<td><div id="groupDescTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
     </table>
     <table id="table_tab2" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" style="display:none;">
		 <tr class="tdbg">
			 <td class="tdbgleft" width="105px" align="right">
	            <strong>角色设置：</strong>
	         </td>
	         <td id="td_roleId">
	         	<c:forEach var="list" items="${roleList}" varStatus="status">
				<span><label>${list.roleName}</label><input id="role_${list.roleId}" type="checkbox" value="${list.roleId}" <c:if test="${list.checked}">checked="checked"</c:if>/></span>
				<c:if test="${status.count%3==0}">
				<br/>
				</c:if>
				</c:forEach>
	         </td>
	     </tr>
	     <tr class="tdbg">
			 <td class="tdbgleft" width="105px" align="right">
	            <strong>操作设置：</strong>
	         </td>
	         <td>
				<div id="div_Tree" class="zTreeDemoBackground left">
					<ul id="ul_Tree" class="ztree"></ul>
				</div>
				<div id="div_Message"></div>
	         </td>
	     </tr>
     </table>
     <table border="0" width="100%" cellpadding="2" cellspacing="1" style="margin-top: 20px;">
     	<tr class="height25">
        	<td colspan="2" align="center">
        		<input id="bnt_Update" type="button" class="inputbutton" value=" 修 改 "/>&nbsp;&nbsp;
        		<input id="bnt_Close" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
        	</td>
        </tr>
     </table>
     </c:otherwise>
     </c:choose>
     </form>
	</body>
</html>