<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>添加角色</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<link href="<%=request.getContextPath()%>/admin/common/zTree/css/zTreeStyle.css" rel="stylesheet" type="text/css"/>
		<script src="<%=request.getContextPath()%>/admin/common/zTree/js/jquery.ztree.core-3.5.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/zTree/js/jquery.ztree.excheck-3.5.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/gpm/js/role.js" type="text/javascript"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	var method = '${method}';
    	</script>
	</head>
	<body>
	    <form id="systemForm" name="systemForm" action="#">
	    <table border="0" cellpadding="0" cellspacing="0" style="width:162px;">
            <tr align="center">
                <td id="td_tab1" class="titlemouseover" style="width:80px;">基本信息</td>
                <td style="width:2px"></td>
                <td id="td_tab2" class="tabtitle" style="width:80px;">权限设置</td>
             </tr>
        </table>
	    <c:choose>
	    <c:when test="${method eq 'add'}">
	    <table id="table_tab1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        <tr class="tdbg">
            <td class="tdbgleft" width="135px" align="right"><strong>所属系统：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td>
	            			<select id="select_sysList" style="height:20px;cursor:pointer;">
	            				 <c:forEach var="list" items="${sysList}">
			                       <option value="${list.systemId}">${list.systemName}</option>
			                     </c:forEach>
	            			</select>
	            		</td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="select_sysListTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>角色名称：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="roleName" type="text" /></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="roleNameTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>角色描述：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0" style="height:145px;">
	            	<tr>
	            		<td><textarea id="roleDesc" style="height:120px;width:260px;" cols="20" rows="2" name="txt_Intr"></textarea></td>
	            	</tr>
	            	<tr>
	            		<td><div id="roleDescTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
     </table>
     <table id="table_tab2" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" style="display:none;">
     	<tr class="tdbg">
     		<td class="tdbgleft" width="135px" align="right"><strong>系统权限</strong></td>
     		<td>
     			<div id="div_Tree" class="zTreeDemoBackground left">
					<ul id="ul_Tree" class="ztree"></ul>
				</div>
				<div id="div_Message">
				</div>
     		</td>
     	</tr>
     </table>
     <table border="0" width="100%" cellpadding="2" cellspacing="1" style="margin-top: 20px;">
     	<tr class="height25">
        	<td colspan="2" align="center">
        		<input id="bnt_Save" type="button" class="inputbutton" value=" 保 存 "/>&nbsp;&nbsp;
        		<input id="bnt_Reset" type="button" class="inputbutton" value=" 重 置 "/>&nbsp;&nbsp;
        		<input id="bnt_Close" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
        	</td>
        </tr>
     </table>
     </c:when>
     <c:otherwise>
      <table id="table_tab1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        <tr class="tdbg">
            <td class="tdbgleft" width="135px" align="right"><strong>所属系统：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td>
							<input id="systemName" type="text" class="input_public" disabled="disabled" value="${bean.systemName }"/>
							<input id="systemId" type="hidden" value="${bean.systemId }"/>
							<input id="roleId" type="hidden" value="${bean.roleId }"/>
	            		</td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="select_sysListTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>角色名称：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="roleName" type="text" value="${bean.roleName }"/></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="roleNameTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>角色描述：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0" style="height:145px;">
	            	<tr>
	            		<td><textarea id="roleDesc" style="height:120px;width:260px;" cols="20" rows="2" name="txt_Intr">${bean.roleDesc }</textarea></td>
	            	</tr>
	            	<tr>
	            		<td><div id="roleDescTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
     </table>
     <table id="table_tab2" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" style="display:none;">
     	<tr class="tdbg">
     		<td class="tdbgleft" width="135px" align="right"><strong>系统权限</strong></td>
     		<td>
     			<div id="div_Tree" class="zTreeDemoBackground left">
					<ul id="ul_Tree" class="ztree"></ul>
				</div>
				<div id="div_Message">
				</div>
     		</td>
     	</tr>
     </table>
     <table border="0" width="100%" cellpadding="2" cellspacing="1" style="margin-top: 20px;">
     	<tr class="height25">
        	<td colspan="2" align="center">
        		<input id="bnt_Update" type="button" class="inputbutton" value=" 修 改 "/>&nbsp;&nbsp;
        		<input id="bnt_Close" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
        	</td>
        </tr>
     </table>
     </c:otherwise>
     </c:choose>
     </form>
	</body>
</html>