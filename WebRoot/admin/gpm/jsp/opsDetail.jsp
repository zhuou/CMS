<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>操作详情</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	$(document).ready(function(){
	    	beautifyBottomButton(['bnt_Ok']);
	    	$("#bnt_Ok").click(function(){
				window.top.frames['iframeMenuContent'].closeDialog();
			});
			parseUri.init();
			var _opsId = parseUri.getParam('id');
			$.getJSON(contextPath+'/security/getOpsInfo.html?id='+_opsId,function(data){
			if(data.code=='200'){
					$('#detail_opsId').html(data.objContent.opsId);
					$('#detail_systemName').html(data.objContent.sysBean.systemName);
					$('#detail_parentName').html(data.objContent.parentName);
					$('#detail_opsName').html(data.objContent.opsName);
					$('#detail_noteLevel').html(data.objContent.noteLevel);
					if(data.objContent.opsType=='1'){
						$('#detail_opsType').html('菜单类型');
					}
					else if(data.objContent.opsType=='2'){
						$('#detail_opsType').html('页面类型');
					}
					else{
						$('#detail_opsType').html('按钮操作类型');
					}
					$('#detail_opsUrl').html(data.objContent.opsUrl);
					$('#detail_opsIcon').html(data.objContent.opsIcon);
					$('#detail_sort').html(data.objContent.sort);
					if(data.objContent.status=='1'){
						$('#detail_status').html('启用');
					}
					else{
						$('#detail_status').html('停用');
					}
					$('#detail_createTime').html(data.objContent.createTime);
					$('#detail_createUser').html(data.objContent.createUser);
					$('#detail_lastEditTime').html(data.objContent.lastEditTime);
					$('#detail_lastEditUser').html(data.objContent.lastEditUser);
					$('#detail_opsDesc').html(data.objContent.opsDesc);
				}
				else{
					alert(data.content)
				}
			});
		});
    	</script>
	</head>
	<body>
       	    <table width="100%" cellspacing="1" cellpadding="2" border="0" class="tableWrap">
	    <tr class="tdbg">
            <td width="150px" align="right"><strong>操作ID：</strong></td>
            <td id="detail_opsId"></td>
        </tr>
         <tr class="tdbg">
            <td width="150px" align="right"><strong>所属系统：</strong></td>
            <td id="detail_systemName"></td>
        </tr>
         <tr class="tdbg">
            <td width="150px" align="right"><strong>父节点名称：</strong></td>
            <td id="detail_parentName"></td>
        </tr>
         <tr class="tdbg">
            <td width="150px" align="right"><strong>操作名称：</strong></td>
            <td id="detail_opsName"></td>
        </tr>
         <tr class="tdbg">
            <td width="150px" align="right"><strong>节点深度：</strong></td>
            <td id="detail_noteLevel"></td>
        </tr>
         <tr class="tdbg">
            <td width="150px" align="right"><strong>操作类型：</strong></td>
            <td id="detail_opsType"></td>
        </tr>
         <tr class="tdbg">
            <td width="150px" align="right"><strong>操作URL：</strong></td>
            <td id="detail_opsUrl"></td>
        </tr>
         <tr class="tdbg">
            <td width="150px" align="right"><strong>icon图片路径：</strong></td>
            <td id="detail_opsIcon"></td>
        </tr>
         <tr class="tdbg">
            <td width="150px" align="right"><strong>排序：</strong></td>
            <td id="detail_sort"></td>
        </tr>
         <tr class="tdbg">
            <td width="150px" align="right"><strong>状态：</strong></td>
            <td id="detail_status"></td>
        </tr>
         <tr class="tdbg">
            <td width="150px" align="right"><strong>创建时间：</strong></td>
            <td id="detail_createTime"></td>
        </tr>
         <tr class="tdbg">
            <td width="150px" align="right"><strong>创建人：</strong></td>
            <td id="detail_createUser"></td>
        </tr>
         <tr class="tdbg">
            <td width="150px" align="right"><strong>最后编辑时间：</strong></td>
            <td id="detail_lastEditTime"></td>
        </tr>
         <tr class="tdbg">
            <td width="150px" align="right"><strong>最后编辑人：</strong></td>
            <td id="detail_lastEditUser"></td>
        </tr>
         <tr class="tdbg">
            <td width="150px" align="right"><strong>操作描述：</strong></td>
            <td id="detail_opsDesc"></td>
        </tr>
		</table>
	     <table border="0" width="100%" cellpadding="2" cellspacing="1" style="margin-top: 20px;">
	     	<tr class="height25">
	        	<td colspan="2" align="center">
	        		<input id="bnt_Ok" type="button" class="inputbutton" value=" 确 定 "/>&nbsp;&nbsp;
	        	</td>
	        </tr>
	     </table>
	</body>
</html>