﻿var checkedMenuId = "iframeTab_li_0";
var iWidth = 0;//right部分的width
var tWidth = 0;//将每次tab菜单width相加
var mLeft = 0;//点击向左的偏移量
var wHeight = document.documentElement.clientHeight - 78;//78为头高
var wWidth = document.documentElement.clientWidth;
var _dialogUpdatePWD;
$(document).ready(function(){
    onLoad();
    loadMenu(firstOpsId);//初始化菜单
    tWidth = $("#"+checkedMenuId).width() + 55;//初始化tWidth
    $(window).resize(function(){
    	wHeight = document.documentElement.clientHeight - 78;
    	wWidth = document.documentElement.clientWidth;
        onLoad();
    });
    $('#bnt_IndexRefresh').click(function(){
		var _iframeSrc = $("#iframeMenuContent").attr('src');
		$("#iframeMenuContent").attr('src',_iframeSrc);
    });
	$('#bnt_UpdatePassWord').click(function(){
		_dialogUpdatePWD = art.dialog.open(contextPath+'/loadUpdatePwd.html',{height:'170px',width:'500px',title:'修改密码',padding: 0,lock:true},false);
	});
    $('#bnt_IndexExit').click(function(){
		var isOK = confirm('您确定要退出网站后台管理系统！');
		if(isOK){
			$.getJSON(contextPath+'/hLogout.html',function(data){
				if(data.code=='200'){
					window.location.href = contextPath+'/login.html';
				}
				else{
					alert(data.content);
				}
			});
		}
    });
});
function onLoad(){
    $("#wrapper").height(wHeight + "px").width(wWidth + "px");
    if(barState=="open"){
        iWidth = wWidth - 195 - 12;//195是leftWrapper宽 12是sideBar的宽
    }
    else{
        iWidth = wWidth - 12;
    }
    var sWidth = iWidth + "px";
    $("#content").width(sWidth);
    $("#contentTop").width(sWidth);
    $("#iframeMenuContent").height(wHeight-31);
}

var barState = "open";
function changeSideBarState(){
    if (barState == "open") {
        $("#switchButton").attr({
            src: contextPath+"/imgs/blue/but_open.gif",
            alt: "打开左栏"
        });
        $("#leftWrapper").css("display", "none");
        barState = "close";
        //重新计算menuListUL的margin-left值
        var iValue = getULMLeft();
        iValue = iValue+195;
        if(iValue>0){
            $("#menuListUL").css("margin-left","0px");
        }
        else{
            $("#menuListUL").css("margin-left",iValue+"px");
        }
    }
    else {
        $("#switchButton").attr({
            src: contextPath+"/admin/imgs/blue/but_close.gif",
            alt: "关闭左栏"
        });
        $("#leftWrapper").css("display", "block");
        barState = "open";
    }
    onLoad();
}

function changeLeftMenu(divId, ulId){
    var state = $("#" + ulId).css("display");
    if (state == "block") {
        $("#" + ulId).css("display", "none");
        $("#" + divId).attr("class", "leftHeadClose");
    }
    else {
        $("#" + ulId).css("display", "block");
        $("#" + divId).attr("class", "leftHeadOpen");
    }
}

function openTopTab(pId){
    var mId = "topMenu_" + pId;
    if (checkedId != mId) {
        $("#" + mId).attr("class", "tabMenuOpen");
        $("#" + checkedId).attr("class", "tabMenuClose");
        checkedId = mId;
        loadMenu(pId);
    }
}

function loadMenu(pId){
	var waiting = alertWaiting('正在获取数据，请等待...');
    $.ajax({
        type: "get",
        url: contextPath+"/security/menu.html?id="+pId,
        data: '',
        success: function(jsonData, status){
        	waiting.close();
            if (jsonData.code == "200") {
                $("#menuContent").html(jsonData.content);
            }
            else if(jsonData.code == "7600"){
                alert(jsonData.content);
                window.location.href=contextPath+"/login.html";
            }
            else {
                alert(jsonData.content);
            }
        },
        error: function(xhr, ajaxOptions, thrownError){
            alert("请求超时，请检查网络！" + thrownError);
        }
    });
}

function openMenu(liId, menuId, url){
    var flag = IsHaveMenuEle(menuId);
    var nowMenu = "iframeTab_li_" + menuId;
    if (flag) {
        if (checkedMenuId != nowMenu) {
            $("#" + nowMenu).attr("class", "tabOpen");
            var a1Id = nowMenu.replace("li", "a1");
            $("#" + a1Id).attr("class", "tabOpen_a1");
            var a2Id = nowMenu.replace("li", "a2");
            $("#" + a2Id).attr("class", "tabOpen_a2");
        }
    }
    else {
        var sb = new Array();
        sb.push("<li id=\"iframeTab_li_" + menuId + "\" class=\"tabOpen\">");
        sb.push("<a id=\"iframeTab_a1_" + menuId + "\" onclick=\"openContentTab('" + menuId + "','" + url + "');\" class=\"tabOpen_a1\">" + $("#" + liId).html() + "</a>");
        sb.push("<a id=\"iframeTab_a2_" + menuId + "\" onclick=\"closeContentTab('" + menuId + "');\" class=\"tabOpen_a2\"><img src=\""+contextPath+"/admin/imgs/blue/tab-close.gif\" /></a>");
        sb.push("</li>");
        $("#menuListUL").html(function(index, html){
            var newMenu = html + sb.join('');
            $("#menuListUL").html(newMenu);
        });

        //判断是否出现tab
        tWidth = tWidth + $("#"+nowMenu).width();
        if(tWidth>iWidth){
            $("#tab_left").show();
            $("#tab_right").show();
            $("#tab_content").addClass("tab-strip-margin");
            mLeft = tWidth - iWidth;
            $("#menuListUL").css("margin-left","-"+mLeft+"px");
        }
    }
    if (checkedMenuId != nowMenu) {
        $("#" + checkedMenuId).attr("class", "tabClose");
        var a1Id = checkedMenuId.replace("li", "a1");
        $("#" + a1Id).attr("class", "tabClose_a1");
        var a2Id = checkedMenuId.replace("li", "a2");
        $("#" + a2Id).attr("class", "tabClose_a2");
    }
    $("#iframeMenuContent").attr("src", url);
    checkedMenuId = nowMenu;
}

function IsHaveMenuEle(menuId){
    var menu = "iframeTab_li_" + menuId;
    var flag = false;
    $.each($("#menuListUL>li"), function(i, eleObj){
        if ($(eleObj).attr("id") == menu) {
            flag = true;
        }
    });
    return flag;
}

function leftClick(){
    var iValue = getULMLeft();
    if((50-iValue)>mLeft){
       $("#menuListUL").css("margin-left","-"+mLeft+"px");
    }
    else{
       $("#menuListUL").css("margin-left",(iValue-50)+"px");
    }
}

function rightClick(){
    var iValue = getULMLeft();
    if((iValue+50)>=0){
        $("#menuListUL").css("margin-left","0px");
    }
    else{
        $("#menuListUL").css("margin-left", (iValue+50)+"px");
    }
}

function getULMLeft(){
    var mValue = $("#menuListUL").css("margin-left");
    var iValue = parseInt(mValue.substring(0,mValue.indexOf('p')));
    return iValue;
}

function openContentTab(menuId, url){
    var nowMenu = "iframeTab_li_" + menuId;
    if (checkedMenuId != nowMenu) {
        $("#" + nowMenu).attr("class", "tabOpen");
        var a1Id = nowMenu.replace("li", "a1");
        $("#" + a1Id).attr("class", "tabOpen_a1");
        var a2Id = nowMenu.replace("li", "a2");
        $("#" + a2Id).attr("class", "tabOpen_a2");

        $("#" + checkedMenuId).attr("class", "tabClose");
        var a1Id = checkedMenuId.replace("li", "a1");
        $("#" + a1Id).attr("class", "tabClose_a1");
        var a2Id = checkedMenuId.replace("li", "a2");
        $("#" + a2Id).attr("class", "tabClose_a2");
        document.getElementById("iframeMenuContent").src = url;
        checkedMenuId = nowMenu;
    }
}

function closeContentTab(menuId){
    var nowMenu = "iframeTab_li_" + menuId;
    var num = 0;
    var indexNum = 0;
    var iMargin = $("#"+nowMenu).width();
    $.each($("#menuListUL>li"), function(i, eleObj){
        num++;
        if ($(eleObj).attr("id") == nowMenu) {
            indexNum = i;
        }
    });
    if (num > 0) {
        if (checkedMenuId == nowMenu) {
            if (indexNum - 1 < 0) {
                var liId = $("#menuListUL>li").get(1).id;
                $("#" + liId).attr("class", "tabOpen");
                var a1Id = liId.replace("li", "a1");
                $("#" + a1Id).attr("class", "tabOpen_a1");
                var a2Id = liId.replace("li", "a2");
                $("#" + a2Id).attr("class", "tabOpen_a2");
				checkedMenuId = liId;
            }
			else{
				var liId = $("#menuListUL>li").get(indexNum-1).id;
                $("#" + liId).attr("class", "tabOpen");
                var a1Id = liId.replace("li", "a1");
                $("#" + a1Id).attr("class", "tabOpen_a1");
                var a2Id = liId.replace("li", "a2");
                $("#" + a2Id).attr("class", "tabOpen_a2");
				checkedMenuId = liId;
			}
        }
        $("#" + nowMenu).detach();
    }
    //重新计算menuListUL的margin-left值
    tWidth = tWidth - iMargin; //重新计算
    var iValue = getULMLeft();
    if(iValue>=0){
        return;
    }
    iValue = iValue+iMargin;
    if(iValue>0){
        $("#menuListUL").css("margin-left","0px");
    }
    else{
        $("#menuListUL").css("margin-left",iValue+"px");
    }
};
function dialogUpdatePWDClose(){
	_dialogUpdatePWD.close();
};
