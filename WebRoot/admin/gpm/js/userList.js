var _dialogUser;
$(document).ready(function(){
	beautifyTopButton(['user_Add','user_Update','user_Del','user_Detail','user_lock','user_unlock','user_Settings']);
	var tableTemp = new beautifySingleSelectedTable('tbody_DataList');

	$('#user_Add').click(function(){
		_dialogUser = art.dialog.open(contextPath+'/security/loadAddUser.html',{height:'400px',width:'520px',title:'添加用户信息',padding: 0,lock:true},false);
	});
	$('#user_Update').click(function(){
		var uId = tableTemp.getValue();
		if(uId==''){
			alert('请选择要操作项！');
			return;
		}
		_dialogUser = art.dialog.open(contextPath+'/security/loadUpdateUser.html?id='+uId,{height:'400px',width:'520px',title:'修改用户信息',padding: 0,lock:true},false);
	});
	$('#user_Del').click(function(){
		var uId = tableTemp.getValue();
		if(uId==''){
			alert('请选择要操作项！');
			return;
		}
		var isOK = confirm('您确定要删除选定的用户吗？如果删除，则和其相关联的数据都将被删除。');
		if(isOK){
			$.getJSON(contextPath+'/security/delUser.html?id='+uId,function(data){
				if(data.code=='200'){
					window.location.reload();
				}
				else{
					alert(data.content);
				}
			});
		}
	});
	$('#user_Detail').click(function(){
		var uId = tableTemp.getValue();
		if(uId==''){
			alert('请选择要操作项！');
			return;
		}
		userDetail(uId);
	});
	$('#user_lock').click(function(){
		var uId = tableTemp.getValue();
		if(uId==''){
			alert('请选择要操作项！');
			return;
		}
		updateStatus(uId,'2');
	});
	$('#user_unlock').click(function(){
		var uId = tableTemp.getValue();
		if(uId==''){
			alert('请选择要操作项！');
			return;
		}
		updateStatus(uId,'0');
	});
	function updateStatus(userId,uStatus){
		var waiting = alertWaiting('请等待...');
		var data = {'userId':userId,'status':uStatus};
		var url = contextPath+'/security/updateUStatus.html';
		$.post(url,data,function(data){
			waiting.close();
			if(data.code=='200'){
				window.location.reload();
			}
			else{
				alert(data.content);
			}
		},'json');
	};
	$('#user_Settings').click(function(){
		var uId = tableTemp.getValue();
		if(uId==''){
			alert('请选择要操作项！');
			return;
		}
		_dialogUser = art.dialog.open(contextPath+'/security/loadSettingRight.html?id='+uId,{height:'400px',width:'520px',title:'用户权限设置',padding: 0,lock:true},false);
	});
});
var closeDialog = function(status){
	_dialogUser.close();
	if(status==1){
		window.location.reload();
	}
};
var userDetail = function(id){
	_dialogUser = art.dialog.open(contextPath+'/security/getUserDetail.html?id='+id,{height:'450px',width:'470px',title:'用户信息详情',padding: 0,lock:true},false);
};