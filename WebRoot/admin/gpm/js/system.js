var checkTable = 'td_tab1';
$(document).ready(function(){
	$.formValidator.initConfig({formID:"systemForm",theme:"Small",
		onError:function(msg,obj,errorlist){
			alertMessage(msg);
		}
	});
	$("#systemName").formValidator({onShowText:"请输入系统名称",onFocus:"公司名称为小1-25个字"}).inputValidator({min:1,max:30,onErrorMin:'请输入系统名称',onError:"系统名称长度不合法"});
	$("#systemIP").formValidator({onShowText:"请输入系统IP",onFocus:"如:192.168.1.101"}).regexValidator({regExp:regexEnum.ip4,onError:"系统IP不合法"});
	$("#secretKey").formValidator({onFocus:"密钥为6-20个数字字母组合"}).inputValidator({min:6,max:20,onError:"输入的密钥不合法"});
	$("#systemDesc").formValidator({onFocus:"系统描述不能超过200个字"}).inputValidator({min:0,max:200,onError:"系统描述不能超过200个字"});
	$("#hostAddr").formValidator({onShowText:"请输入域名或者系统IP+端口",onFocus:"如:www.163.com或192.168.1.101:8080"}).inputValidator({min:1,max:64,onError:"请输入域名或者系统IP+端口"});

	beautifyBottomButton(['bnt_Save','bnt_Reset','bnt_Close']);
	$('#td_tab1').click(function(){
		if(checkTable!='td_tab1'){
			$("#td_tab1").attr("class", "titlemouseover");
        	$("#td_tab2").attr("class", "tabtitle");
        	$("#table_tab1").show();
        	$("#table_tab2").hide();
        	checkTable = 'td_tab1';
		}
	});
	$('#td_tab2').click(function(){
		if(checkTable!='td_tab2'){
			$("#td_tab2").attr("class", "titlemouseover");
        	$("#td_tab1").attr("class", "tabtitle");
        	$("#table_tab2").show();
        	$("#table_tab1").hide();
        	checkTable = 'td_tab2';
		}
	});
	$("#bnt_Close").click(function(){
		window.top.frames['iframeMenuContent'].closeDialog(0);
	});
	if(method=='add'){
		$('#bnt_Save').click(function(){
			var isOk = $.formValidator.pageIsValid('1');
			if(!isOk){
				return;
			}
			var waiting = window.parent.alertWaiting('正在提交数据，请等待...');
			var url = contextPath+'/security/addSys.html';
			$.ajax({
				type:"POST",
				url:url,
				data:getData(),
				dataType:'json',
				contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				success: function(data){
					waiting.close();
					if(data.code=='200'){
						window.top.frames['iframeMenuContent'].closeDialog(1);
					}
					else{
						alert(data.content);
					}
				}
			});
		});
		$('#bnt_Reset').click(function(){
			$('#systemForm')[0].reset();
		});
	}
	else{
		parseUri.init();
		var _sysId = parseUri.getParam('id');
		$.getJSON(contextPath+'/security/getUsersBySysId.html?id='+_sysId,function(data){
			if(data.code=='200'){
				var _html = '';
				$.each(data.objContent,function(i,item){
					_html=_html+'<span id="spanUId_'+item.userId+'">'+item.userName+'<a onclick="delCheckedUser('+item.userId+')"><img src="'+contextPath+'/admin/imgs/blue/close.png"/></a></span>';
					_uArray.push(item.userId);
				});
				$('#td_userList').append(_html);
			}
			else{
				alert(data.content);
			}
		});

		$('#bnt_Update').click(function(){
			var isOk = $.formValidator.pageIsValid('1');
			if(!isOk){
				return;
			}
			var waiting = window.parent.alertWaiting('正在提交数据，请等待...');
			var url = contextPath+'/security/updateSys.html?id='+_sysId;
			$.ajax({
				type:"POST",
				url:url,
				data:getData(),
				dataType:'json',
				contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				success: function(data){
					waiting.close();
					if(data.code=='200'){
						window.top.frames['iframeMenuContent'].closeDialog(1);
					}
					else{
						alert(data.content);
					}
				}
			});
		});
	}

	var getData = function(){
		var data = {systemName:$('#systemName').val(),systemIP:$('#systemIP').val(),hostAddr:$('#hostAddr').val(),secretKey:$('#secretKey').val(),
		userId:_uArray.join(','),systemDesc:$('#systemDesc').val()};
		return data;
	};
});
var _uArray = new Array();
function checkedUserList(_uId,_uName){
	if(jQuery.inArray(_uId,_uArray)!=-1){
		alert('选择的用户已经存在');
		return;
	}
	_uArray.push(_uId);
	var _content = '<span id="spanUId_'+_uId+'">'+_uName+'<a onclick="delCheckedUser('+_uId+')"><img src="'+contextPath+'/admin/imgs/blue/close.png"/></a></span>';
	$('#td_userList').append(_content);
};
function delCheckedUser(_uId){
	$('#spanUId_'+_uId).detach();
	var _index = jQuery.inArray(_uId,_uArray);
	if(_index!=-1){
		_uArray.splice(_index,1);
	}
}