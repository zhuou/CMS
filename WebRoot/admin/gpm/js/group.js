var checkTable = 'td_tab1',_sysId,_sysName,_parentId,_parentName;
$(document).ready(function(){
	$.formValidator.initConfig({formID:"systemForm",theme:"Small",
		onError:function(msg,obj,errorlist){
			alertMessage(msg);
		}
	});

	$("#groupName").formValidator({onShowText:"请输入组/部门名称",onFocus:"组/部门名称为小1-25个字"}).inputValidator({min:1,max:30,onErrorMin:'请输入组/部门名称',onError:"组/部门名称长度不合法"});
	$("#groupDesc").formValidator({onFocus:"描述不能超过200个字"}).inputValidator({min:0,max:200,onError:"描述不能超过200个字"});

	if(method=='add'){
		beautifyBottomButton(['bnt_Save','bnt_Reset','bnt_Close']);
		_sysId = art.dialog.data('sysId');
		_sysName = art.dialog.data('sysName');
		_parentId = art.dialog.data('parentId');
		_parentName = art.dialog.data('parentName');

		$('#systemName').val(_sysName);
		if(typeof(_parentId)=='undefined' || _parentId==''){
			_parentId = "-1";
			$('#parentName').val('无父节点');
		}
		else{
			$('#parentName').val(_parentName);
		}

		$('#bnt_Reset').click(function(){
			$('#systemForm')[0].reset();
		});
		$('#bnt_Save').click(function(){
			var isOk = $.formValidator.pageIsValid('1');
			if(!isOk){
				return;
			}

			var waiting = window.parent.alertWaiting('正在提交数据，请等待...');
			var url = contextPath+'/security/addGroup.html';
			$.ajax({
				type:"POST",
				url:url,
				data:getData(),
				dataType:'json',
				contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				success: function(data){
					waiting.close();
					if(data.code=='200'){
						window.top.frames['iframeMenuContent'].closeDialog(1);
					}
					else{
						alert(data.content);
					}
				}
			});
		});

		loadTree('-1');
	}
	else{
		beautifyBottomButton(['bnt_Update','bnt_Close']);
		_sysId = $('#hidden_systemId').val();

		$('#bnt_Update').click(function(){
			var isOk = $.formValidator.pageIsValid('1');
			if(!isOk){
				return;
			}

			var waiting = window.parent.alertWaiting('正在提交数据，请等待...');
			var url = contextPath+'/security/updateGroup.html';
			var dateRole = getData();
			dateRole['groupId'] = $('#hidden_groupId').val();
			$.ajax({
				type:"POST",
				url:url,
				data:dateRole,
				dataType:'json',
				contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				success: function(data){
					waiting.close();
					if(data.code=='200'){
						window.top.frames['iframeMenuContent'].closeDialog(1);
					}
					else{
						alert(data.content);
					}
				}
			});
		});
		loadTree($('#hidden_groupId').val());
	}

	$("#bnt_Close").click(function(){
		window.top.frames['iframeMenuContent'].closeDialog(0);
	});
	$('#td_tab1').click(function(){
		if(checkTable!='td_tab1'){
			$("#td_tab1").attr("class", "titlemouseover");
        	$("#td_tab2").attr("class", "tabtitle");
        	$("#table_tab1").show();
        	$("#table_tab2").hide();
        	checkTable = 'td_tab1';
		}
	});
	$('#td_tab2').click(function(){
		if(checkTable!='td_tab2'){
			$("#td_tab2").attr("class", "titlemouseover");
        	$("#td_tab1").attr("class", "tabtitle");
        	$("#table_tab2").show();
        	$("#table_tab1").hide();
        	checkTable = 'td_tab2';
		}
	});
	var getData = function(){
		var _array = new Array();
		var _checkedNodes = $.fn.zTree.getZTreeObj("ul_Tree").getCheckedNodes(true);
		for(var i=0;i<_checkedNodes.length;i++){
			_array.push(_checkedNodes[i].id);
		}

		var _array1 = new Array();
		$.each($('#td_roleId').find(':checkbox'),function(i,obj){
			if(this.checked){
				_array1.push($(this).attr('value'));
			}
		});

		var _inherit = false;
		if($('#status_yes').attr('checked')=='checked'){
			_inherit = true;
		}
		var data = {systemId:_sysId,parentId:_parentId,groupName:$('#groupName').val(),groupDesc:$('#groupDesc').val(),
		inherit:_inherit,opsId:_array.join(','),roleIds:_array1.join(',')};
		return data;
	};
});
var loadTree = function(gId){
	if(typeof(_sysId)=='undefined' || _sysId==''){
		$('#div_Message').val('没有系统，请先新增系统信息');
		return;
	}
	var nodes = [];
	var setting = {
		check:{
			enable: true
		},
		data: {
			simpleData: {
				enable: true
			}
		}
	};
	$.getJSON(contextPath+'/security/getOpsList.html?sid='+_sysId+'&gid='+gId,function(data){
		if(data.code=='200'){
			if(data.objContent.length<=0){
				$('#div_Message').show().text('没有权限，请先新增系统权限');
				$('#div_Tree').hide();
			}
			else{
				$('#div_Tree').show();
				$.each(data.objContent,function(i,item){
					var itemNode = {};
				    itemNode.id = item.opsId;
					itemNode.name = item.opsName;
					itemNode.pId = item.parentId;
					itemNode.open = true;
					if(item.checked){
						itemNode.checked = true;
					}
					if(item.opsType==1){
						itemNode.icon = contextPath+'/admin/imgs/blue/opt_menu.png';
					}
					else if(item.opsType==2){
						itemNode.icon = contextPath+'/admin/imgs/blue/opt_page.png';
					}
					else if(item.opsType==3){
						itemNode.icon = contextPath+'/admin/imgs/blue/opt_operate.png';
					}
					else{
					}
					nodes[i] = itemNode;
				});
				$.fn.zTree.init($("#ul_Tree"), setting, nodes);
			}
		}
		else{
			$('#div_Message').show().text(data.content);
			$('#div_Tree').hide();
		}
	});
};