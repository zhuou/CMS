var _dialogRole;
$(document).ready(function(){
	beautifyTopButton(['role_Add','role_Update','role_Del','role_Detail']);
	var tableTemp = new beautifySingleSelectedTable('tbody_DataList');

	$('#role_Add').click(function(){
		_dialogRole = art.dialog.open(contextPath+'/security/loadAddRole.html',{height:'400px',width:'470px',title:'新增角色',padding: 0,lock:true},false);
	});
	$('#role_Update').click(function(){
		var roleId = tableTemp.getValue();
		if(roleId==''){
			alert('请选择要操作项！');
			return;
		}
		_dialogRole = art.dialog.open(contextPath+'/security/loadUpdateRole.html?id='+roleId,{height:'400px',width:'470px',title:'修改角色',padding: 0,lock:true},false);
	});
	$('#role_Detail').click(function(){
		var roleId = tableTemp.getValue();
		if(roleId==''){
			alert('请选择要操作项！');
			return;
		}
		roleDetail(roleId);
	});
	$('#role_Del').click(function(){
		var roleId = tableTemp.getValue();
		if(roleId==''){
			alert('请选择要操作项！');
			return;
		}
		var isOK = confirm('您确定要删除此角色吗？如果删除，则和其相关联的数据都将被删除。');
		if(isOK){
			$.getJSON(contextPath+'/security/delRole.html?id='+roleId,function(data){
				if(data.code=='200'){
					window.location.reload();
				}
				else{
					alert(data.content);
				}
			});
		}
	});
});
var closeDialog = function(status){
	_dialogRole.close();
	if(status==1){
		window.location.reload();
	}
};
var roleDetail = function(id){
	_dialogRole = art.dialog.open(contextPath+'/security/getRoleDetail.html?id='+id,{height:'350px',width:'470px',title:'角色详情',padding: 0,lock:true},false);
};