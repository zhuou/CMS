var checkTable = 'td_tab1';
var sysId = '';
$(document).ready(function(){
	$.formValidator.initConfig({formID:"systemForm",theme:"Small",
		onError:function(msg,obj,errorlist){
			alertMessage(msg);
		}
	});
	$("#roleName").formValidator({onShowText:"请输入角色名称",onFocus:"角色名称为小1-25个字"}).inputValidator({min:1,max:30,onErrorMin:'请输入角色名称',onError:"角色名称长度不合法"});
	$("#roleDesc").formValidator({onFocus:"角色描述不能超过200个字"}).inputValidator({min:0,max:200,onError:"角色描述不能超过200个字"});

	$("#bnt_Close").click(function(){
		window.top.frames['iframeMenuContent'].closeDialog();
	});
	if(method=='add'){
		beautifyBottomButton(['bnt_Save','bnt_Reset','bnt_Close']);
		$('#bnt_Reset').click(function(){
			$('#systemForm')[0].reset();
		});
		$('#bnt_Save').click(function(){
			var isOk = $.formValidator.pageIsValid('1');
			if(!isOk){
				return;
			}
			var checkedNodes = $.fn.zTree.getZTreeObj("ul_Tree");
			if(checkedNodes==null||checkedNodes.getCheckedNodes(true).length<=0){
				alert('权限设置不能为空，请设置权限！');
				return;
			}
			var waiting = window.parent.alertWaiting('正在提交数据，请等待...');
			var url = contextPath+'/security/addRole.html';
			$.ajax({
				type:"POST",
				url:url,
				data:getData(checkedNodes),
				dataType:'json',
				contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				success: function(data){
					waiting.close();
					if(data.code=='200'){
						window.top.frames['iframeMenuContent'].closeDialog(1);
					}
					else{
						alert(data.content);
					}
				}
			});
		});
	}
	else{
		beautifyBottomButton(['bnt_Update','bnt_Close']);
		$('#bnt_Update').click(function(){
			var isOk = $.formValidator.pageIsValid('1');
			if(!isOk){
				return;
			}
			var checkedNodes = $.fn.zTree.getZTreeObj("ul_Tree");
			if(checkedNodes==null||checkedNodes.getCheckedNodes(true).length<=0){
				alert('权限设置不能为空，请设置权限！');
				return;
			}
			var waiting = window.parent.alertWaiting('正在提交数据，请等待...');
			var url = contextPath+'/security/updateRole.html';
			var paramData = getData(checkedNodes);
			paramData['roleId'] = $('#roleId').val();
			$.ajax({
				type:"POST",
				url:url,
				data:paramData,
				dataType:'json',
				contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				success: function(data){
					waiting.close();
					if(data.code=='200'){
						window.top.frames['iframeMenuContent'].closeDialog(1);
					}
					else{
						alert(data.content);
					}
				}
			});
		});
	}
	$('#td_tab1').click(function(){
		if(checkTable!='td_tab1'){
			$("#td_tab1").attr("class", "titlemouseover");
        	$("#td_tab2").attr("class", "tabtitle");
        	$("#table_tab1").show();
        	$("#table_tab2").hide();
        	checkTable = 'td_tab1';
		}
	});
	$('#td_tab2').click(function(){
		if(checkTable!='td_tab2'){
			$("#td_tab2").attr("class", "titlemouseover");
        	$("#td_tab1").attr("class", "tabtitle");
        	$("#table_tab2").show();
        	$("#table_tab1").hide();
        	checkTable = 'td_tab2';

			if(method=='add'){
	        	var sysIdTemp = $('#select_sysList option:selected').val();
	        	if(sysId!=sysIdTemp){
	        	    // 销毁数
	        		var zTree = $.fn.zTree.getZTreeObj("ul_Tree");
	        		if(zTree!=null){
	        			zTree.destroy();
	        		}
	        		loadTree(sysIdTemp,'');
	        		sysId = sysIdTemp;
	        	}
        	}
        	else{
				var sysIdTemp = $('#systemId').val();
				var roleIdTemp = $('#roleId').val();
				if(sysId!=sysIdTemp){
					loadTree(sysIdTemp,roleIdTemp);
					sysId = sysIdTemp;
				}
        	}
		}
	});

	var getData = function(_objTree){
		var _array = new Array();
		var _checkedNodes = _objTree.getCheckedNodes(true);
		for(var i=0;i<_checkedNodes.length;i++){
			_array.push(_checkedNodes[i].id);
		}
		var data = {systemId:$('#select_sysList option:selected').val(),roleName:$('#roleName').val(),roleDesc:$('#roleDesc').val(),
		opsId:_array.join(',')};
		return data;
	};
});
var loadTree = function(sysId,roleId){
	if(typeof(sysId)=='undefined' || sysId==''){
		$('#div_Message').val('没有系统，请先新增系统信息');
		return;
	}
	var nodes = [];
	var setting = {
		check:{
			enable: true,
			chkboxType: {'Y':'p','N':'s'}
		},
		data: {
			simpleData: {
				enable: true
			}
		}
	};
	var waiting = window.parent.alertWaiting('正在获取数据，请稍等...');
	$.getJSON(contextPath+'/security/getOpsList.html?sid='+sysId+'&rid='+roleId,function(data){
		if(data.code=='200'){
			if(data.objContent.length<=0){
				$('#div_Message').show().text('没有权限，请先新增系统权限');
				$('#div_Tree').hide();
			}
			else{
				$('#div_Tree').show();
				$.each(data.objContent,function(i,item){
					var itemNode = {};
				    itemNode.id = item.opsId;
					itemNode.name = item.opsName;
					itemNode.pId = item.parentId;
					itemNode.open = true;
					if(item.checked){
						itemNode.checked = true;
					}
					if(item.opsType==1){
						itemNode.icon = contextPath+'/admin/imgs/blue/opt_menu.png';
					}
					else if(item.opsType==2){
						itemNode.icon = contextPath+'/admin/imgs/blue/opt_page.png';
					}
					else if(item.opsType==3){
						itemNode.icon = contextPath+'/admin/imgs/blue/opt_operate.png';
					}
					else{
					}
					nodes[i] = itemNode;
				});
				$.fn.zTree.init($("#ul_Tree"), setting, nodes);
			}
		}
		else{
			$('#div_Message').show().text(data.content);
			$('#div_Tree').hide();
		}
		waiting.close();
	});
};