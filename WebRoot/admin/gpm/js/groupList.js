var _dialogGroup,_sysId,_sysName,_parentId,_parentName;
$(document).ready(function(){
	beautifyTopButton(['group_Add','group_Update','group_Del','group_Detail']);
	var sysTree = $.fn.zTree.init($("#treeSysList"), setting, zNodes);
	var nodes = sysTree.getNodes()[0].children;
	if(nodes.length>0){
		sysTree.selectNode(nodes[0]);
		_sysId = nodes[0].id;
		_sysName = nodes[0].name;
		loadTree();
	}
	$('#group_Add').click(function(){
		getParam();
		_dialogGroup = art.dialog.open(contextPath+'/security/loadAddGroup.html?id='+_sysId,{height:'400px',width:'470px',title:'新增组/部门',padding: 0,lock:true},false);
	});
	$('#group_Update').click(function(){
		if(typeof(_parentId)=='undefined' || _parentId==''){
			alert('请选择要操作项！');
			return;
		}
		_dialogGroup = art.dialog.open(contextPath+'/security/loadUpdateGroup.html?id='+_parentId,{height:'400px',width:'470px',title:'修改组/部门',padding: 0,lock:true},false);
	});
	$('#group_Del').click(function(){
		if(typeof(_parentId)=='undefined' || _parentId==''){
			alert('请选择要操作项！');
			return;
		}
		var isOK = confirm('您确定要删除选定的组/部门吗？如果删除，则和其相关联的数据都将被删除。');
		if(isOK){
			$.getJSON(contextPath+'/security/delGroup.html?id='+_parentId,function(data){
				if(data.code=='200'){
					loadTree();
				}
				else{
					alert(data.content);
				}
			});
		}
	});

	$('#group_Detail').click(function(){
		if(typeof(_parentId)=='undefined' || _parentId==''){
			alert('请选择要查看的组/部门');
			return;
		}
		_dialogGroup = art.dialog.open(contextPath+'/security/getGroupDetail.html?id='+_parentId,{height:'400px',width:'470px',title:'组/部门详情',padding: 0,lock:true},false);
	});
	function getParam(){
		art.dialog.data('sysId',_sysId);
		art.dialog.data('sysName',_sysName);
		art.dialog.data('parentId',_parentId);
		art.dialog.data('parentName',_parentName);
	};
});
var closeDialog = function(status){
	_dialogGroup.close();
	if(status==1){
		loadTree();
	}
};
var roleDetail = function(id){
	_dialogGroup = art.dialog.open(contextPath+'/security/getRoleDetail.html?id='+id,{height:'350px',width:'470px',title:'角色详情',padding: 0,lock:true},false);
};
var loadTree = function(){
	if(typeof(_sysId)=='undefined' || _sysId==''){
		return;
	}
	var nodes = [{id:-1,name:_sysName+'(组/部门)',pId:-2,open:true,icon:contextPath+'/admin/imgs/blue/sys_icon.png'}];
	var setting = {
		view:{
			addHoverDom: addHoverDom,
			removeHoverDom: removeHoverDom,
			selectedMulti: false
		},
		data: {
			simpleData: {
				enable: true
			}
		},
		callback: {
			onClick:getGroupInfo
		}
	};
	$.getJSON(contextPath+'/security/getGroupList.html?id='+_sysId,function(data){
		if(data.code=='200'){
			if(data.objContent.length<=0){
				$('#td_Tree').removeAttr('valign');
			 	$('#div_Message').show().text('没有数据，请点击添加组/部门信息');
			 	$('#div_Tree').hide();
			}
			else{
				$.each(data.objContent,function(i,item){
					var itemNode = {};
				    itemNode.id = item.groupId;
					itemNode.name = item.groupName;
					itemNode.pId = item.parentId;
					itemNode.open = true;
					itemNode.icon = contextPath+'/admin/imgs/blue/group.png';
					nodes[i+1] = itemNode;
				});
				$.fn.zTree.init($("#treeOpsList"), setting, nodes);

				$('#td_Tree').attr('valign','top');
			 	$('#div_Message').hide();
			 	$('#div_Tree').show();
			}
		}
		else{
			$('#td_Tree').removeAttr('valign');
			$('#div_Message').show().text(data.content);
			$('#div_Tree').hide();
		}
	});
	function addHoverDom(treeId, treeNode){
		if ($("#addBtn_"+treeNode.id).length>0) return;
		var bntStr = "<span id='addBtn_" +treeNode.id+ "' title='add' onfocus='this.blur();'><a style='color:#0000FF;' href='#'>新增</a></span>";
		bntStr += "<span id='editBtn_" +treeNode.id+ "' title='edit' onfocus='this.blur();'><a style='color:#0000FF;' href='#'>修改</a></span>";
		bntStr += "<span id='delBtn_" +treeNode.id+ "' title='delete' onfocus='this.blur();'><a style='color:#0000FF;' href='#'>删除</a></span>";
		var sObj = $("#" + treeNode.tId + "_span");
		sObj.after(bntStr);
		var btn_Add = $("#addBtn_"+treeNode.id);
		var btn_Edit = $("#editBtn_"+treeNode.id);
		var btn_Del = $("#delBtn_"+treeNode.id);
		if (btn_Add){
			btn_Add.bind("click", function(){
				_parentId = treeNode.id;
				_parentName = treeNode.name;
				showAddPage();
			});
		}
		if (btn_Edit){
			btn_Edit.bind("click", function(){
				_parentId = treeNode.id;
				_parentName = treeNode.name;
				showUpdatePage();
			});
		}
		if (btn_Del){
			btn_Del.bind("click", function(){
				alert('del');
			});
		}
	};
	function removeHoverDom(treeId, treeNode){
		$("#addBtn_"+treeNode.id).unbind().remove();
		$("#editBtn_"+treeNode.id).unbind().remove();
		$("#delBtn_"+treeNode.id).unbind().remove();
	};
	function getGroupInfo(event, treeId, treeNode){
		if(treeNode.level==0){
			_parentId = '';
			_parentName = '';
		}
		else{
			_parentId = treeNode.id;
			_parentName = treeNode.name;
		}
	};
};