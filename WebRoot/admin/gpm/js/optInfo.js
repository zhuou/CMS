$(document).ready(function(){
	$.formValidator.initConfig({formID:"opsInfoForm",theme:"Small",
		onError:function(msg,obj,errorlist){
			alertMessage(msg);
		}
	});

	$("#opsName").formValidator({onShowText:"请输入操作名称",onFocus:"操作名称为小1-25个字"}).inputValidator({min:1,max:30,onErrorMin:'请输入操作名称',onError:"操作名称长度不合法"});
	$("#sort").formValidator({onFocus:"请输入正整数"}).inputValidator({min:0,onErrorMin:'不能为空，请输入'}).regexValidator({regExp:regexEnum.num1,onError:"输入的数字不合法"});
	$("#opsIcon").formValidator({empty:true,onFocus:"请输入icon路径"}).inputValidator({min:0,max:256,onMaxError:"icon路径长度过长"});
	$("#opsDesc").formValidator({empty:true,onFocus:"请输入操作描述"}).inputValidator({min:0,max:256,onMaxError:"描述长度过长"});

	if(_method=='add'){
		beautifyBottomButton(['bnt_Save','bnt_Reset','bnt_Close']);
		_sysId = art.dialog.data('sysId');
		_sysName = art.dialog.data('sysName');
		_parentId = art.dialog.data('parentId');
		_parentName = art.dialog.data('parentName');
		$('#systemName').text(_sysName);
		if(typeof(_parentId)=='undefined' || _parentId==''){
			_parentId = "-1";
			$('#parentName').val('无父节点');
		}
		else{
			$('#parentName').val(_parentName);
		}
		if(typeof(_sysId)=='undefined' || _sysId==''){
			alert('请选择系统！');
		}

		disabledUrl();

		$('#bnt_Save').click(function(){
			var isOk = $.formValidator.pageIsValid('1');
			if(!isOk){
				return;
			}
			var waiting = window.parent.alertWaiting('正在提交数据，请等待...');
			var url = contextPath+'/security/addOpt.html';
			$.ajax({
				type:"POST",
				url:url,
				data:getData(),
				dataType:'json',
				contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				success: function(data){
					waiting.close();
					if(data.code=='200'){
						window.top.frames['iframeMenuContent'].closeDialog(1);
					}
					else{
						alert(data.content);
					}
				}
			});
		});
	}
	else{
		beautifyBottomButton(['bnt_Update','bnt_Close']);
		$('#systemName').text(_sysName);
		$('#parentName').val(_parentName);
		$("#opsType option[value='"+_opsType+"']").attr("selected", true);
		if(_opsType=='1'){
			disabledUrl();
		}
		else{
			enableUrl();
		}

		$('#bnt_Update').click(function(){
			var isOk = $.formValidator.pageIsValid('1');
			if(!isOk){
				return;
			}
			var waiting = window.parent.alertWaiting('正在提交数据，请等待...');
			var url = contextPath+'/security/updateOpt.html';
			var pageDate = getData();
			pageDate['opsId'] = _opsId;
			$.ajax({
				type:"POST",
				url:url,
				data:pageDate,
				dataType:'json',
				contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				success: function(data){
					waiting.close();
					if(data.code=='200'){
						window.top.frames['iframeMenuContent'].closeDialog(1);
					}
					else{
						alert(data.content);
					}
				}
			});
		});
	}

	var getData = function(){
		var _status = "1";
		if($('#status_close').is(":checked")){
			_status = "0";
		}
		var data = {systemId:_sysId,parentId:_parentId,opsName:$('#opsName').val(),opsType:$('#opsType').val(),
		opsUrl:$('#opsUrl').val(),opsIcon:$('#opsIcon').val(),status:_status,sort:$('#sort').val(),opsDesc:$('#opsDesc').val()};
		return data;
	};

	$('#opsType').change(function(){
		if($(this).val()=='1'){
		    disabledUrl();
		}
		else{
			$('#opsUrl').val('');
			enableUrl();
		}
	});
	function disabledUrl(){
		$('#opsUrl').val('菜单类型,不需要URL');
		$('#opsUrl').attr('disabled','disabled');
		$("#opsUrl").formValidator({empty:true});
	};
	function enableUrl(){
		$('#opsUrl').removeAttr('disabled');
		$("#opsUrl").formValidator({onShowText:"请输入操作URL",onFocus:"请输入操作URL"}).inputValidator({min:1,max:256,onErrorMin:'请输入操作URL',onMaxError:"URL长度过长"});
	};
	$("#bnt_Close").click(function(){
		window.top.frames['iframeMenuContent'].closeDialog(0);
	});
});
