$(document).ready(function(){
	beautifyBottomButton(['bnt_Submit','bnt_Close']);
	var sysId = $('#sysSelect').val();
	var uId = parseUri.getParam('id');
	var sName = $('#sysSelect option:selected').text()+'(组/部门)';
	if(typeof(sysId)!='undefined' && sysId!=''){
		getRightData(uId,sysId,sName);
	}
	$('#sysSelect').change(function(){
		sysId = $('#sysSelect').val();
		sName = $('#sysSelect option:selected').text()+'(组/部门)';
		getRightData(uId,sysId,sName);
	});
	$('#bnt_Close').click(function(){
		window.top.frames['iframeMenuContent'].closeDialog(0);
	});
	function getRightData(uid,sid,sysName){
		var waiting = window.parent.alertWaiting('正在请求数据，请等待...');
		var data = {'uid':uid,'sid':sid};
		var url = contextPath+'/security/getRightData.html';
		$.post(url,data,function(data){
			waiting.close();
			if(data.code=='200'){
			    var aRole = new Array();
			    aRole.push('<ul>');
				$.each(data.objContent.role,function(i,item){
					aRole.push('<li>');
					aRole.push('<label>'+item.roleName+'<input id="role_'+item.roleId+'" type="checkbox" value="'+item.roleId+'"');
					if(item.checked){
						aRole.push(' checked="checked"');
					}
					aRole.push('/></label>');
					aRole.push('</li>');
				});
				aRole.push('</ul>');
				$('#div_role').html(aRole.join(''));

				var aGroup = new Array();
				var defaultNode = {};
				defaultNode.id = -1;
				defaultNode.name = sysName;
				defaultNode.pId = -999;
				defaultNode.open = true;
				defaultNode.nocheck = true
				defaultNode.icon = contextPath+'/admin/imgs/blue/sys_icon.png';
				aGroup.push(defaultNode);
				$.each(data.objContent.group,function(i,item){
					var itemNode = {};
					itemNode.id = item.groupId;
					itemNode.name = item.groupName;
					itemNode.pId = item.parentId;
					itemNode.open = true;
					itemNode.icon = contextPath+'/admin/imgs/blue/group.png';
					if(item.checked){
						itemNode.checked = true;
					}
					aGroup.push(itemNode);
				});
				loadGroupTree(aGroup);
			}
			else{
				alert(data.content);
			}
		},'json');
	};
	function loadGroupTree(nodes){
		var setting = {
			check:{
				enable: true,
				chkboxType: { "Y" : "", "N" : "" }
			},
			data: {
				simpleData: {
					enable: true
				}
			}
		};
		$.fn.zTree.init($("#ul_GroupTree"), setting, nodes);
	};
	$('#bnt_Submit').click(function(){
		sysId = $('#sysSelect').val();

		var aRId = new Array();
		$.each($('#div_role').find('input'),function(i,item){
			var obj = $(item);
			if(obj.attr('checked')=='checked'){
				aRId.push(obj.attr('value'));
			}
		});

		var aGId = new Array();
		var _checkedNodes = $.fn.zTree.getZTreeObj("ul_GroupTree").getCheckedNodes(true);
		for(var i=0;i<_checkedNodes.length;i++){
			var _vId = _checkedNodes[i].id;
			if(_vId != '-1'){
				aGId.push(_vId);
			}
		}

		var waiting = window.parent.alertWaiting('正在提交数据，请等待...');
		var data = {'uid':uId,'sid':sysId,'rid':aRId.join(','),'gid':aGId.join(',')};
		var url = contextPath+'/security/settingRight.html';
		$.post(url,data,function(data){
			waiting.close();
			if(data.code=='200'){
				alert(data.content);
				window.top.frames['iframeMenuContent'].closeDialog(0);
			}
			else{
				alert(data.content);
			}
		},'json');
	});
});
