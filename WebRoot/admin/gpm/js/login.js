$(document).ready(function(){
	$('#bnt_login').click(function(){
		var code = $("#txt_Code").val();
		var name = $("#txt_UserName").val();
		var password = $("#txt_Password").val();
		if($.trim(name)==''){
			alert('请输入用户名');
			return;
		}
		if($.trim(password)==''){
			alert('请输入密码');
			return;
		}
		if($.trim(code)==''){
			alert('请输入验证码');
			return;
		}

		var waiting = window.parent.alertWaiting('正在登录，请等待...');
		$.ajax({
	        type:"POST",
	        url:contextPath+"/login.html",
	        dataType:'json',
	        contentType:'application/x-www-form-urlencoded; charset=UTF-8',
	        data:"code="+code+"&password="+password+"&userName="+name,
	        success:function(jsonData,status){
	            if(jsonData.code=="200"){
					window.location.href = contextPath+'/security/index.html';
	            }
	            else if(jsonData.code=="7510"){
	            	waiting.close();
					getSys();
	            }
	            else{
	            	waiting.close();
	                alert(jsonData.content);
	            }
	        },
	        error: function(){
	            alert("请求超时，请检查网络！");
	        }
    	});
	});
	$('#bnt_reset').click(function(){
		$("#txt_Code").val("");
	    $("#txt_UserName").val("");
	    $("#txt_Password").val("");
	});
	function getSys(){
		var waiting = window.parent.alertWaiting('正在获取系统信息，请等待...');
		$.ajax({
        type:"GET",
        url:contextPath+"/getUserSys.html",
        dataType:'json',
        success:function(jsonData,status){
	       	   waiting.close();
	           if(jsonData.code=="200"){
				var content = new Array();
				content.push('<div class="divSys">');
				content.push('<ul>');
				$.each(jsonData.objContent,function(i,item){
					content.push('<li>');
					content.push('<a href="javascript:void(0);" onclick="goIndex('+item.systemId+');"><img src="'+contextPath+'/admin/imgs/blue/sys_bigicon.png" alt=""/></a>');
					content.push('<div><a href="javascript:void(0);" onclick="goIndex('+item.systemId+');">'+item.systemName+'</a></div>');
					content.push('</li>');
				});
				content.push('</li>');
				content.push('</ul>');
				art.dialog({padding: 0,title: '请选择要登录系统',content: content.join(''),lock: true,drag:false});
	         }
	         else{
	            alert(jsonData.content);
	         }
	       },
	       error: function(){
	           alert("请求超时，请检查网络！");
	       }
   		});
	}
});
function goIndex(id){
	window.location.href = contextPath + '/goIndex.html?id='+id;
};
function loginfouce(){
    if(this.location != top.location){
        top.location = this.location;
    }
    $("#txt_UserName").focus();
};