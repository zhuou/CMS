var _dialogSystem;
$(document).ready(function(){
	beautifyTopButton(['system_Add','system_Update','system_Del','system_Detail']);
	var tableTemp = new beautifySingleSelectedTable('tbody_DataList');

	$('#system_Add').click(function(){
		_dialogSystem = art.dialog.open(contextPath+'/security/loadSys.html',{height:'370px',width:'480px',title:'添加系统信息',padding: 0,lock:true},false);
	});
	$('#system_Update').click(function(){
		var sysId = tableTemp.getValue();
		if(sysId==''){
			alert('请选择要操作项！');
			return;
		}
		_dialogSystem = art.dialog.open(contextPath+'/security/loadUpdateSys.html?id='+sysId,{height:'350px',width:'470px',title:'修改系统信息',padding: 0,lock:true},false);
	});
	$('#system_Detail').click(function(){
		var sysId = tableTemp.getValue();
		if(sysId==''){
			alert('请选择要操作项！');
			return;
		}
		sysDetail(sysId);
	});
	$('#system_Del').click(function(){
		var sysId = tableTemp.getValue();
		if(sysId==''){
			alert('请选择要操作项！');
			return;
		}
		var bool = window.confirm('您确认要删除此产品信息吗？');
		if(!bool){
			return;
		}
		$.getJSON(contextPath+'/security/delSys.html?id='+sysId,function(data){
			if(data.code=='200'){
				window.location.reload();
			}
			else{
				alert(data.content);
			}
		});
	});
});
function closeDialog(status){
	_dialogSystem.close();
	if(status==1){
		window.location.reload();
	}
};
var sysDetail = function(id){
	_dialogSystem = art.dialog.open(contextPath+'/security/getSysDetail.html?id='+id,{height:'350px',width:'470px',title:'系统信息详情',padding: 0,lock:true},false);
};