var _dialogSystem;
var _sysId,_sysName,_parentId,_parentName;
$(document).ready(function(){
	beautifyTopButton(['optInfo_Add','optInfo_Update','optInfo_Del','optInfo_Detail']);
	var sysTree = $.fn.zTree.init($("#treeSysList"), setting, zNodes);
	var nodes = sysTree.getNodes()[0].children;
	if(nodes.length>0){
		sysTree.selectNode(nodes[0]);
		_sysId = nodes[0].id;
		_sysName = nodes[0].name;
	}

	$('#optInfo_Add').click(function(){showAddPage();});
	$('#optInfo_Update').click(function(){
		if(typeof(_parentId)=='undefined' || _parentId==''){
			alert('请选择要修改的节点！');
			return;
		}
		showUpdatePage();
	});
	$('#optInfo_Detail').click(function(){
		if(typeof(_parentId)=='undefined' || _parentId==''){
			alert('请选择要查看详情的节点！');
			return;
		}
		showDetail();
	});
	$('#optInfo_Del').click(function(){delOpsInfo();});

	loadTree(_sysId,_sysName);
});
var closeDialog = function(status){
	_dialogSystem.close();
	if(status==1){
		loadTree(_sysId,_sysName);
	}
};
var showAddPage = function(){
	initParam();
	_dialogSystem = art.dialog.open(contextPath+'/security/loadAddOpt.html',{height:'380px',width:'520px',title:'添加功能信息',padding: 0,lock:true},false);
};
var showUpdatePage = function(){
	initParam();
	_dialogSystem = art.dialog.open(contextPath+'/security/loadUpdateOpt.html?id='+_parentId,{height:'380px',width:'520px',title:'修改功能信息',padding: 0,lock:true},false);
};
var showDetail = function(){
	_dialogSystem = art.dialog.open(contextPath+'/admin/gpm/jsp/opsDetail.jsp?id='+_parentId,{height:'400px',width:'500px',title:'功能详情',padding: 0,lock:true},false);
};
var initParam =	function(){
	if(typeof(_sysId)=='undefined' || _sysId==''){
		alert('请选择系统，若没有系统，请先新建系统');
		return;
	}
	art.dialog.data('sysId',_sysId);
	art.dialog.data('sysName',_sysName);
	art.dialog.data('parentId',_parentId);
	art.dialog.data('parentName',_parentName);
};
var loadTree = function(sysId,sysTitle){
	if(typeof(sysId)=='undefined' || sysId==''){
		return;
	}
	var nodes = [];
	var setting = {
		view:{
			addHoverDom: addHoverDom,
			removeHoverDom: removeHoverDom,
			selectedMulti: false
		},
		data: {
			simpleData: {
				enable: true
			}
		},
		callback: {
			onClick:getOpsInfo
		}
	};
	function addHoverDom(treeId, treeNode){
		if ($("#addBtn_"+treeNode.id).length>0) return;
		var bntStr = "<span id='addBtn_" +treeNode.id+ "' title='add' onfocus='this.blur();'><a style='color:#0000FF;' href='#'>新增</a></span>";
		bntStr += "<span id='editBtn_" +treeNode.id+ "' title='edit' onfocus='this.blur();'><a style='color:#0000FF;' href='#'>修改</a></span>";
		bntStr += "<span id='delBtn_" +treeNode.id+ "' title='delete' onfocus='this.blur();'><a style='color:#0000FF;' href='#'>删除</a></span>";
		var sObj = $("#" + treeNode.tId + "_span");
		sObj.after(bntStr);
		var btn_Add = $("#addBtn_"+treeNode.id);
		var btn_Edit = $("#editBtn_"+treeNode.id);
		var btn_Del = $("#delBtn_"+treeNode.id);
		if (btn_Add){
			btn_Add.bind("click", function(){
				_parentId = treeNode.id;
				_parentName = treeNode.name;
				showAddPage();
			});
		}
		if (btn_Edit){
			btn_Edit.bind("click", function(){
				_parentId = treeNode.id;
				_parentName = treeNode.name;
				showUpdatePage();
			});
		}
		if (btn_Del){
			btn_Del.bind("click", function(){
				_parentId = treeNode.id;
				_parentName = treeNode.name;
				delOpsInfo();
			});
		}
	};
	function removeHoverDom(treeId, treeNode){
		$("#addBtn_"+treeNode.id).unbind().remove();
		$("#editBtn_"+treeNode.id).unbind().remove();
		$("#delBtn_"+treeNode.id).unbind().remove();
	};

	var waiting = window.parent.alertWaiting('正在获取数据，请稍等...');
	$.getJSON(contextPath+'/security/getOpsList.html?sid='+sysId,function(data){
		if(data.code=='200'){
			if(data.objContent.length<=0){
				$('#td_Tree').removeAttr('valign');
			 	$('#div_Message').show().text('没有数据，请点击添加系统操作信息');
			 	$('#div_treeOps').hide();
			}
			else{
			 	$('#td_Tree').attr('valign','top');
			 	$('#div_Message').hide();
			 	$('#div_treeOps').show();

				$.each(data.objContent,function(i,item){
					var itemNode = {};
				    itemNode.id = item.opsId;
					itemNode.name = item.opsName;
					itemNode.pId = item.parentId;
					itemNode.open = true;
					if(item.opsType==1){
						itemNode.icon = contextPath+'/admin/imgs/blue/opt_menu.png';
					}
					else if(item.opsType==2){
						itemNode.icon = contextPath+'/admin/imgs/blue/opt_page.png';
					}
					else if(item.opsType==3){
						itemNode.icon = contextPath+'/admin/imgs/blue/opt_operate.png';
					}
					else{
					}
					nodes[i] = itemNode;
				});
				$.fn.zTree.init($("#treeDemo"), setting, nodes);
			}
		}
		else{
			$('#td_Tree').removeAttr('valign');
			$('#div_Message').show().text(data.content);
			$('#div_treeOps').hide();
		}
		waiting.close();
	});
};
var getOpsInfo = function(event, treeId, treeNode){
	_parentId = treeNode.id;
	_parentName = treeNode.name;
};
var delOpsInfo = function(){
	var isOK = confirm('您确定要删除此操作项吗？如果删除，则和其相关联的数据都将被删除。');
	if(isOK){
		$.getJSON(contextPath+'/security/delOps.html?id='+_parentId,function(data){
			if(data.code=='200'){
				loadTree(_sysId,_sysName);
			}
			else{
				alert(data.content);
			}
		});
	}
};