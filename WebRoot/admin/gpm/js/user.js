$(document).ready(function(){
	$.formValidator.initConfig({formID:"systemForm",theme:"Small",
		onError:function(msg,obj,errorlist){
			alertMessage(msg);
		}
	});
	var regExp = '^[\@A-Za-z0-9\!\#\$\%\^\&\*\.\~\_]{6,20}$';
	$("#userName").formValidator({onShowText:"请输入用户名称",onFocus:"为6-12个英文、数字和\"_\""}).inputValidator({min:1,max:30,onErrorMin:'请输入用户名称'});
	$("#password").formValidator({onShowText:"",onFocus:"为6-20个英文、数字和\"~_!@#$%^&*.\"组合"}).inputValidator({min:6,max:20,onErrorMin:'密码小于6位',onErrorMax:'密码大于20位'}).regexValidator({regExp:regExp,onError:"输入的密码不合法"});
	$("#trueName").formValidator({onFocus:"姓名不超过10个字"}).inputValidator({min:0,max:10,onErrorMin:'请输入姓名',onError:"姓名不超过10个字"});
	$("#loginErrorTime").formValidator({onFocus:"请输入正整数"}).inputValidator({min:0,onErrorMin:'不能为空，请输入'}).regexValidator({regExp:regexEnum.num1,onError:"输入的数字不合法"});
	$("#lockTime").formValidator({onFocus:"请输入正整数"}).inputValidator({min:0,onErrorMin:'不能为空，请输入'}).regexValidator({regExp:regexEnum.num1,onError:"输入的数字不合法"});
	$("#position").formValidator({onFocus:"请输入职位"});
	$("#email").formValidator({empty:true,onFocus:"请输入Email"}).regexValidator({regExp:regexEnum.email,onError:"输入的Email不合法"});
	$("#qq").formValidator({empty:true,onFocus:"请输入QQ"}).regexValidator({regExp:regexEnum.qq,onError:"输入的QQ不合法"});
	$("#officePhone").formValidator({onFocus:"请输入办公电话"});
	$("#homePhone").formValidator({onFocus:"请输入住宅电话"});
	$("#mobile").formValidator({onFocus:"请输入移动电话"});
	$("#address").formValidator({onFocus:"请输入联系地址"});
	$("#zipCode").formValidator({onFocus:"请输入邮政编码"});
	$("#idCard").formValidator({onFocus:"请输入证件号"});

	beautifyBottomButton(['bnt_Save','bnt_Reset','bnt_Close']);
	pageTab({'td_tab1':'table_tab1','td_tab2':'table_tab2','td_tab3':'table_tab3'});

	if(method=='add'){
		$("#bnt_Close").click(function(){
			window.top.frames['iframeMenuContent'].closeDialog(0);
		});
		$('#bnt_Reset').click(function(){
			$('#systemForm')[0].reset();
		});
		$('#bnt_Save').click(function(){
			var isOk = $.formValidator.pageIsValid('1');
			if(!isOk){
				return;
			}
			var waiting = window.parent.alertWaiting('正在提交数据，请等待...');
			var url = contextPath+'/security/addUser.html';
			$.ajax({
				type:"POST",
				url:url,
				data:getData(),
				dataType:'json',
				contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				success: function(data){
					waiting.close();
					if(data.code=='200'){
						window.top.frames['iframeMenuContent'].closeDialog(1);
					}
					else{
						alert(data.content);
					}
				}
			});
		});
	}
	else{
		$("#bnt_Close").click(function(){
			window.top.frames['iframeMenuContent'].closeDialog(0);
		});
		var _uId = parseUri.getParam('id');
		$('#bnt_Update').click(function(){
			var isOk = $.formValidator.pageIsValid('1');
			if(!isOk){
				return;
			}
			var waiting = window.parent.alertWaiting('正在提交数据，请等待...');
			var url = contextPath+'/security/updateUser.html?id='+_uId;
			$.ajax({
				type:"POST",
				url:url,
				data:getData(),
				dataType:'json',
				contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				success: function(data){
					waiting.close();
					if(data.code=='200'){
						window.top.frames['iframeMenuContent'].closeDialog(1);
					}
					else{
						alert(data.content);
					}
				}
			});
		});
		$.getJSON(contextPath+'/security/getSysByUId.html?id='+_uId,function(data){
			if(data.code=='200'){
				var _content = "";
				$.each(data.objContent,function(i,item){
					_sArray.push(item.systemId);
					_content += '<span id="spanUId_'+item.systemId+'">'+item.systemName+'<a onclick="delCheckedSys('+item.systemId+')"><img src="'+contextPath+'/admin/imgs/blue/close.png"/></a></span>';
				});

				$('#td_sysList').append(_content);
			}
			else{
				alert(data.content);
			}
		});
	}

	var getData = function(){
		var enableMultiLogin = true;
		if($('#enableMultiLogin_No').prop('checked')){
			enableMultiLogin = false;
		}
		var status = 0;
		if($('#status_0').prop('checked')){
			status = 0;
		}
		else if($('#status_1').prop('checked')){
			status = 1;
		}
		var enableModifyPassword = true;
		if($('#enableModifyPassword_No').prop('checked')){
			enableModifyPassword = false;
		}
		var data = {"userName":$('#userName').val(),"password":$('#password').val(),
		"enableMultiLogin":enableMultiLogin,"loginErrorTime":$('#loginErrorTime').val(),"lockTime":$('#lockTime').val(),
		"status":status,"enableModifyPassword":enableModifyPassword,"accountExpires":$('#accountExpires').val(),"trueName":$('#trueName').val(),
		"sex":$('#sex').val(),"position":$('#position').val(),"email":$('#email').val(),"qq":$('#qq').val(),
		"officePhone":$('#officePhone').val(),"homePhone":$('#homePhone').val(),"mobile":$('#mobile').val(),"address":$('#address').val(),
		"zipCode":$('#zipCode').val(),"birthday":$('#birthday').val(),"idCard":$('#idCard').val(),"sysId":_sArray.join(',')};
		return data;
	};
});
var _sArray = new Array();
function checkedSysList(_sId,_sName){
	if(jQuery.inArray(_sId,_sArray)!=-1){
		alert('选择的系统已经存在');
		return;
	}
	_sArray.push(_sId);
	var _content = '<span id="spanUId_'+_sId+'">'+_sName+'<a onclick="delCheckedSys('+_sId+')"><img src="'+contextPath+'/admin/imgs/blue/close.png"/></a></span>';
	$('#td_sysList').append(_content);
};
function delCheckedSys(_sId){
	$('#spanUId_'+_sId).detach();
	var _index = jQuery.inArray(_sId,_sArray);
	if(_index!=-1){
		_sArray.splice(_index,1);
	}
}