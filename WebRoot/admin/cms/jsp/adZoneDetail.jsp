<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>广告位详情</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
	</head>
	<body class="mainContent">
     <table border="0" cellpadding="2" cellspacing="1" class="tableWrap" width="100%">
		<tr class="tdbg">
             <td class="tdbgleft" width="100px"><strong>广告位ID：&nbsp;</strong></td>
             <td>
             	${bean.zoneId}
             </td>
         </tr>
         <tr class="tdbg">
             <td class="tdbgleft" width="100px"><strong>广告位编码：&nbsp;</strong></td>
             <td>
             	${bean.zoneCode}
             </td>
         </tr>
		<tr class="tdbg">
             <td class="tdbgleft"><strong>广告位名称：&nbsp;</strong></td>
             <td>
             ${bean.zoneName}
             </td>
         </tr>
		<tr class="tdbg">
             <td class="tdbgleft"><strong>版位状态：&nbsp;</strong></td>
             <td>
             <c:choose>
            	<c:when test="${bean.status}"><span style="color:#00A600">启用</span></c:when>
            	<c:otherwise><span style="color:#FF0000">禁用</span></c:otherwise>
            </c:choose>
             </td>
         </tr>
         <tr class="tdbg">
             <td class="tdbgleft"><strong>广告位说明：&nbsp;</strong></td>
             <td >
             ${bean.zoneIntro}
             </td>
         </tr>
         <tr class="tdbg">
             <td class="tdbgleft"><strong>创建时间：&nbsp;</strong></td>
             <td >
             <fmt:formatDate value="${bean.createTime}" type="date" pattern="yyyy-MM-dd HH:mm"/>
             </td>
         </tr>
         <tr class="tdbg">
             <td class="tdbgleft"><strong>创建者：&nbsp;</strong></td>
             <td >
             ${bean.createUser}
             </td>
         </tr>
         <tr class="tdbg">
         	<td colspan="2" align="center"><input type="button" class="inputbutton" id="bnt_close" value="关闭" ></td>
         </tr>
    </table>
	</body>
<script type="text/javascript">
$(document).ready(function(){
	$('#bnt_close').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('0');
	});
});
</script>
</html>