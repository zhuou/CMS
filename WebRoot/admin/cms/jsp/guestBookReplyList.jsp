<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>留言回复列表</title>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/cms/js/guestBookReplyList.js" type="text/javascript"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
	</head>
	<body class="mainContent">
		<span class="localSpan">后台管理<span> &gt;&gt; </span><span> 内容管理 </span><span>&gt;&gt; </span><span>留言回复列表 </span></span>
        <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        	<tr class="topbottom">
        		<td colspan="7">
				   <select id="ddl_findField">
					<option value="title">按标题关键字搜索</option>
					<option value="replyContent">按回复内容关键字搜索</option>
                   </select>
        		    <input id="reply_SearchText" class="inputtext_public" type="text" alt="请输入搜索关键字" style="width:150px;">
	            	<input id="reply_Search" class="topinput topinput_search" type="button" value="搜索"/>
        		</td>
        	</tr>
	        <c:choose>
	    	<c:when test="${pageBean.totalRecordNum>0}">
            <tr class="title" style="height:25px;">
               <th scope="col" style="width: 4%; cursor: col-resize;">ID</th>
               <th scope="col" style="width: 16%; cursor: col-resize;">标题</th>
               <th scope="col" style="width: 10%; cursor: col-resize;">回复人</th>
               <th scope="col" style="width: 44%; cursor: col-resize;">回复内容</th>
               <th scope="col" style="width: 9%; cursor: col-resize;">回复时间</th>
               <th scope="col" style="width: 9%; cursor: col-resize;">创建时间</th>
               <th scope="col" style="width: 8%; cursor: col-resize;">操作</th>
            </tr>
             <tbody id="tbody_DataList">
             	<c:forEach var="list" items="${pageBean.dataList}">
                <tr class="tdbg" style="height:25px;">
                   <td align="center">${list.replyId}</td>
                   <td align="center"><a name="bnt_toDetail" value="${list.guestBookId}">${list.title}</a></td>
                   <td align="center">${list.replyName}</td>
                   <td>${list.replyContent}</td>
                   <td align="center">${list.replyTime}</td>
                   <td align="center">${list.createTime}</td>
                   <td align="center"><a name="bnt_updateReply" value="${list.replyId}">修改</a>&nbsp;&nbsp;<a name="bnt_delReply" value="${list.replyId}">删除</a></td>
                 </tr>
                 </c:forEach>
            </tbody>
            <tr align="right" class="tdbg" style="height:28px;">
                <td colspan="10">
                   <c:import url="pageCurrent.jsp">
			        <c:param name="totalRecordNum" value="${pageBean.totalRecordNum}"></c:param>
			        <c:param name="pageRecordNum" value="${pageBean.pageRecordNum}"></c:param>
				   </c:import>
                </td>
            </tr>
            </c:when>
            <c:otherwise>
            	<tr class="tdbg" style="height:80px;">
			        <td colspan="10" align="center">
			             <strong class="valignMiddle">没有任何数据！</strong>
			        </td>
		        </tr>
            </c:otherwise>
         	</c:choose>
        </table>
	</body>
</html>