<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>广告位新增修改</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
		var reqMethod = '${method}';
    	</script>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/cms/js/adZone.js" type="text/javascript"></script>
	</head>
	<body class="mainContent">
	<form id="adZoneForm" name="adZoneForm" action="#">
	<c:choose>
	 <c:when test="${method eq 'add'}">
     <table border="0" cellpadding="2" cellspacing="1" class="tableWrap" width="100%" style="border-bottom:0px;">
		<tr class="tdbg">
             <td class="tdbgleft" style="width:30%;">
             <strong>广告位编码：&nbsp;</strong><br />
             <span style="color:Red;">用于前台查询广告，系统会自动生产，也可以自己定义</span>
             </td>
             <td>
				<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_zoneCode" type="text" style="width:150px;" value="${zoneCode}"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_zoneCodeTip" style="width:280px"></div></td>
	                </tr>
	            </table>
             </td>
         </tr>
		<tr class="tdbg">
             <td class="tdbgleft"><strong>广告位名称：&nbsp;</strong></td>
             <td>
				<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_zoneName" type="text" style="width:200px;"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_zoneNameTip" style="width:280px"></div></td>
	                </tr>
	            </table>
             </td>
         </tr>
		<tr class="tdbg">
             <td class="tdbgleft"><strong>版位状态：&nbsp;</strong></td>
             <td>
                 <input id="cb_status" type="checkbox" checked="checked"/><span class="cautiontitle valignMiddle">设为活动的版位才能在前台显示。</span>
             </td>
         </tr>
         <tr class="tdbg">
             <td class="tdbgleft"><strong>广告位说明：&nbsp;</strong></td>
             <td >
				<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <textarea id="txt_zoneIntro" rows="5" cols="46" class="inputtext"></textarea>
		                </td>
		                <td><div id="txt_zoneIntroTip" style="width:280px"></div></td>
	                </tr>
	            </table>
             </td>
         </tr>
    </table>
      <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bnt_save" value="保 存">&nbsp;
	                <input type="button" class="inputbutton" id="bnt_close" value="关闭" >
	             </td>
	        </tr>
        </table>
	  </c:when>
      <c:otherwise>
		<table border="0" cellpadding="2" cellspacing="1" class="tableWrap" width="100%" style="border-bottom:0px;">
		<tr class="tdbg">
             <td class="tdbgleft" style="width:30%;">
             <strong>广告位编码：&nbsp;</strong><br />
             <span style="color:Red;">用于前台查询广告，系统会自动生产，也可以自己定义</span>
             </td>
             <td>
				<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_zoneCode" type="text" style="width:150px;" value="${bean.zoneCode}"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_zoneCodeTip" style="width:280px"></div></td>
	                </tr>
	            </table>
             </td>
         </tr>
		<tr class="tdbg">
             <td class="tdbgleft"><strong>广告位名称：&nbsp;</strong></td>
             <td>
				<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_zoneName" type="text" style="width:200px;" value="${bean.zoneName}"/><span style="color:#FF0000;">*</span>
		                <input id="txt_zoneId" type="hidden" value="${bean.zoneId}"/>
		                </td>
		                <td><div id="txt_zoneNameTip" style="width:280px"></div></td>
	                </tr>
	            </table>
             </td>
         </tr>
		<tr class="tdbg">
             <td class="tdbgleft"><strong>版位状态：&nbsp;</strong></td>
             <td>
                 <input id="cb_status" type="checkbox"  <c:if test="${bean.status}">checked="checked"</c:if>/><span class="cautiontitle valignMiddle">设为活动的版位才能在前台显示。</span>
             </td>
         </tr>
         <tr class="tdbg">
             <td class="tdbgleft"><strong>广告位说明：&nbsp;</strong></td>
             <td >
				<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <textarea id="txt_zoneIntro" rows="5" cols="46" class="inputtext">${bean.zoneIntro}</textarea>
		                </td>
		                <td><div id="txt_zoneIntroTip" style="width:280px"></div></td>
	                </tr>
	            </table>
             </td>
         </tr>
    </table>
      <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bnt_update" value="修改">&nbsp;
	                <input type="button" class="inputbutton" id="bnt_close" value="关闭" >
	             </td>
	        </tr>
        </table>
      </c:otherwise>
     </c:choose>
    </form>
	</body>
<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
</html>