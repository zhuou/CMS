<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>上传图片选择列表</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$.each($('#imageList').find('img'),function(i,eleObj){
					eleObj.onload = function () {
                        var w = this.width, h = this.height;
                        scale(this, 100, 120, 80);
                        this.title = '双击可选中图片，原图尺寸: ' + w + "X" + h;
                        this.onload = null;
                    };
				});
				function scale(img, max, oWidth, oHeight) {
			        var width = 0, height = 0, percent, ow = img.width || oWidth, oh = img.height || oHeight;
			        if (ow > max || oh > max) {
			            if (ow >= oh) {
			                if (width = ow - max) {
			                    percent = (width / ow).toFixed(2);
			                    img.height = oh - oh * percent;
			                    img.width = max;
			                }
			            } else {
			                if (height = oh - max) {
			                    percent = (height / oh).toFixed(2);
			                    img.width = ow - ow * percent;
			                    img.height = max;
			                }
			            }
			        }
			    };
				$('img[name="img_checked"]').dblclick(function(){
					var imgUrl = $(this).attr('src');
					var imgName = $(this).attr('original');
					window.top.frames['${iframe}'].${callback}(imgUrl,imgName);
				});
			});
		</script>
		<style type="text/css">
		#imageList {
		    margin-top: 10px;
		    overflow-x: hidden;
		    overflow-y: auto;
		    width: 620px;
		}
		 #imageList div {
		    float: left;
		    cursor: pointer;
		    height: 100px;
		    margin: 5px 10px;
		    width: 100px;
		}
		</style>
	</head>
	<body class="mainContent">
	    <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
	        <c:choose>
		    	<c:when test="${pageBean.totalRecordNum>0}">
			        <tbody id="tbody_DataList">
			        <tr class="tdbg">
						<td id="imageList">
							<c:forEach var="list" items="${pageBean.dataList}">
							<div>
							<img name="img_checked" width="100" src="${list.path }" original="${list.fileName}" />
							</div>
							</c:forEach>
						</td>
			        </tr>
			        </tbody>
			        <tr align="right" class="tdbg" style="height:28px;">
			        	<td>
			        		<c:import url="pageCurrent.jsp">
			        			<c:param name="totalRecordNum" value="${pageBean.totalRecordNum}"></c:param>
			        			<c:param name="pageRecordNum" value="${pageBean.pageRecordNum}"></c:param>
					        </c:import>
			        	</td>
			        </tr>
		        </c:when>
	     		<c:otherwise>
		     		<tr class="tdbg" style="height:80px;">
			             <td align="center">
			                  <strong class="valignMiddle">没有任何数据！</strong>
			             </td>
		            </tr>
	     		</c:otherwise>
      		</c:choose>
     </table>
	</body>
</html>