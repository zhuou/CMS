<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>栏目选择</title>
		<link href="<%=request.getContextPath()%>/admin/common/zTree/css/zTreeStyle.css" rel="stylesheet" type="text/css"/>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/zTree/js/jquery.ztree.core-3.5.min.js" type="text/javascript"></script>
		<script type="text/javascript">
			var contextPath = '<%=request.getContextPath()%>';
			var setting = {
			view: {
				//showIcon: showIconForTree,
				fontCss: getFont
			},
			data: {
				simpleData: {
					enable: true
				}
			},
			callback: {
				onDblClick: zTreeOnDblClick
			}
		};
		var zNodes =[
				{id:-1,pId:0,name:"无(所添加的栏目为根节点)",icon:contextPath+'/admin/imgs/blue/channel_root.png',font:{'font-weight':'bold'},open:true,type:0},
			<c:forEach var="list" items="${list}" varStatus="status">
			<c:choose>
				<c:when test="${list.channelType==0}">
				{id:${list.channelId},pId:${list.parentId},name:"${list.channelName}",open:true,font:{'font-weight':'bold'},type:${list.channelType},icon:contextPath+'/admin/imgs/blue/channel_contain.png'}
				</c:when>
				<c:when test="${list.channelType==2}">
				{id:${list.channelId},pId:${list.parentId},name:"${list.channelName}",open:true,font:{'font-weight':'bold'},type:${list.channelType},icon:contextPath+'/admin/imgs/blue/channel_single.png'}
				</c:when>
				<c:otherwise>
				{id:${list.channelId},pId:${list.parentId},name:"${list.channelName}",open:true,font:{'font-weight':'bold','color':'#ADADAD','cursor':'default'},type:${list.channelType},icon:contextPath+'/admin/imgs/blue/channel_external.png'}
				</c:otherwise>
			</c:choose>
				<c:if test="${!status.last}">,</c:if>
			</c:forEach>
		];
		function showIconForTree(treeId, treeNode){
			return !treeNode.isParent;
		};
		function getFont(treeId, node) {
			return node.font ? node.font : {};
		}
		$(document).ready(function(){
			$.fn.zTree.init($("#treeChannel"), setting, zNodes);
		});
		function zTreeOnDblClick(event, treeId, treeNode){
			window.top.frames['OpendialogWindow_AddChannel'].callBackChannel(treeNode.id,treeNode.name);
		}
		</script>
	</head>
	<body>
	<div style="font:12px '宋体',Tahoma,Helvetica,Arial,sans-serif;color:red;">请选择栏目，双击选中</div>
	<div class="zTreeDemoBackground left">
		<ul id="treeChannel" class="ztree"></ul>
	</div>
	</body>
</html>