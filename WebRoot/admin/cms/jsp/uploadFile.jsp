<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>上传文件</title>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				beautifyBottomButton(['bnt_UploadFile']);
			});
		</script>
	</head>
	<body class="tdbg">
	<form action="<%=request.getContextPath()%>/security/uploadFile.html" method="post" enctype="multipart/form-data">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<br><input name="fileData" id="fileData" width="300" type="file"/>
					<input id="bnt_UploadFile" type="submit" value=" 上传 " class="inputbutton"/>
					<input name="callback" value="${callback}" type="hidden"/>
					<input name="iframe" value="${iframe}" type="hidden"/>
					<input name="fileName" value="fileData" type="hidden"/>
					<c:choose>
						<c:when test="${!(empty bean)&&!(bean.code eq '200')}">
							<div style="color:#FF0000;">${bean.content}</div>
						</c:when>
						<c:when test="${!(empty bean)&&(bean.code eq '200')}">
						<script type="text/javascript">
						$(document).ready(function(){
							//拼接回调函数
							window.top.frames['${iframe}'].${callback}('${bean.content}');
						});
						</script>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</table>
    </form>
	</body>
</html>