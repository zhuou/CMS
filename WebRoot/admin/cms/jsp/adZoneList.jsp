<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>广告位列表</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/cms/js/adZoneList.js"></script>
	</head>
	<body class="mainContent">
		<span class="localSpan">后台管理<span> &gt;&gt; </span><span> 内容管理 </span><span>&gt;&gt; </span><span>广告位列表 </span></span>
	    <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
	        <tr class="topbottom">
	            <td colspan="10">
	            	<input id="adZone_Add" class="topinput topinput_detail" type="button" value="新增"/>
	            	<input id="adZone_Update" class="topinput topinput_edit" type="button" value="修改"/>
	            	<input id="adZone_Del" class="topinput topinput_del" type="button" value="删除选定选"/>
	            </td>
	        </tr>
	        <c:choose>
		    	<c:when test="${pageBean.totalRecordNum>0}">
			        <tr class="title">
			        	<th style="width:4%;"><input id="cb_CheckedAll" type="checkbox"/></th>
			        	<th style="width:6%;">广告位ID</th>
			        	<th style="width:8%;">广告位编码</th>
			            <th style="width:15%;">广告位名称</th>
			            <th style="width:12%;">公司名称</th>
			            <th style="width:7%;">广告位状态</th>
			            <th style="width:18%;">广告位描述</th>
			            <th style="width:10%;">创建时间</th>
			            <th style="width:8%;">创建者</th>
			            <th style="width:12%;">操作</th>
			        </tr>
			        <tbody id="tbody_DataList">
			        <c:forEach var="list" items="${pageBean.dataList}">
			        <tr class="tdbg">
			        	<td align="center"><input type="checkbox" value="${list.zoneId}"/></td>
			        	<td align="center">${list.zoneId}</td>
			        	<td align="center"><a href="#" name="detail_${list.zoneId}">${list.zoneCode}</a></td>
			            <td align="center"><a href="#" name="detail_${list.zoneId}">${list.zoneName}</a></td>
			            <td align="center">${list.companyName}</td>
			            <td align="center">
			            <c:choose>
			            	<c:when test="${list.status}"><span style="color:#00A600">启用</span></c:when>
			            	<c:otherwise><span style="color:#FF0000">禁用</span></c:otherwise>
			            </c:choose>
			            </td>
			            <td align="center">${list.zoneIntro}</td>
			            <td align="center"><fmt:formatDate value="${list.createTime}" type="date" pattern="yyyy-MM-dd HH:mm"/></td>
			            <td align="center">${list.createUser}</td>
			            <td align="center">
							<c:choose>
			            	<c:when test="${list.status}"><a href="#" name="adZoneStatus_${list.zoneId}_false">禁用广告位</a></c:when>
			            	<c:otherwise><a href="#" name="adZoneStatus_${list.zoneId}_true">启用广告位</a></c:otherwise>
			            	</c:choose>
							<a>新增广告</a>
			            </td>
			        </tr>
			        </c:forEach>
			        </tbody>
			        <tr align="right" class="tdbg" style="height:28px;">
			        	<td colspan="10">
			        		<c:import url="pageCurrent.jsp">
			        			<c:param name="totalRecordNum" value="${pageBean.totalRecordNum}"></c:param>
			        			<c:param name="pageRecordNum" value="${pageBean.pageRecordNum}"></c:param>
					        </c:import>
			        	</td>
			        </tr>
		        </c:when>
	     		<c:otherwise>
		     		<tr class="tdbg" style="height:80px;">
			             <td align="center">
			                  <strong class="valignMiddle">没有任何数据！</strong>
			             </td>
		            </tr>
	     		</c:otherwise>
      		</c:choose>
     </table>
	</body>
<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
</html>