<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>上传首页默认图页面片</title>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<style type="text/css">
		.input_public {border: 1px solid #7F9DB9;font-size: 12px;height: 16px;line-height: 20px;padding-left: 4px;}
		</style>
		<script type="text/javascript">
			$(document).ready(function(){
				beautifyBottomButton(['bnt_UploadDefaultImg']);
				$('#checkbox_isThumb').change(function(){
					if($(this).attr("checked")){
						$(this).val('true');
					}
					else{
						$(this).val('false');
					}
				});
			});
		</script>
	</head>
	<body class="tdbg">
	<form action="<%=request.getContextPath()%>/security/uploadDefaultImg.html" method="post" enctype="multipart/form-data">
		<table border="0" width="100%" cellpadding="0" cellspacing="0">
			<tr style="height:25px;">
				<td>
					<br><input name="imgFile" id="imgFile" width="300" type="file"/>
				</td>
			</tr>
			<tr>
				<td>
					宽:<input name="imgWidth" type="text" style="width:40px;" class="input_public" id="txt_imgWidth"/>
					高:<input name="imgHight" type="text" style="width:40px;" class="input_public" id="txt_imgHight"/>
					<input name="isThumb" id="checkbox_isThumb" class="valignMiddle" type="checkbox" /><span class="valignMiddle">等比例缩放</span>
					<input id="bnt_UploadDefaultImg" type="submit" value=" 上传 " class="inputbutton"/>
					<input name="callback" value="${callback}" type="hidden"/>
					<input name="iframe" value="${iframe}" type="hidden"/>
					<c:choose>
						<c:when test="${!(empty bean)&&!(bean.code eq '200')}">
							<span style="color:#FF0000;">${bean.content}</span>
						</c:when>
						<c:when test="${!(empty bean)&&(bean.code eq '200')}">
						<script type="text/javascript">
						$(document).ready(function(){
							//拼接回调函数
							window.top.frames['${iframe}'].${callback}('${bean.content}');
						});
						</script>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</table>
    </form>
	</body>
</html>