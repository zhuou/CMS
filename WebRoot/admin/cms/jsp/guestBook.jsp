<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>留言新增修改页面</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
		var reqMethod = '${method}';
		window.UEDITOR_HOME_URL = '<%=request.getContextPath()%>';
    	</script>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/date/WdatePicker.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/editor/ueditor.all.min.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/editor/ueditor.config1.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/cms/js/guestBook.js" type="text/javascript"></script>
	</head>
	<body class="mainContent">
		<form id="guestBookForm" name="guestBookForm" action="#">
		<c:choose>
	    <c:when test="${method eq 'add'}">
         <table border="0" cellpadding="0" cellspacing="0" width="161px">
            <tr align="center">
                <td id="td_tab1" class="titlemouseover" onclick="checkedTab(this,'tb_Channel1');">
                    基本信息</td>
                <td width="2px"></td>
                <td id="td_tab2" class="tabtitle" onclick="checkedTab(this,'tb_Channel2');">
                    属性选项</td>
            </tr>
        </table>
		<table id="tb_Pannel1" border="0" cellpadding="2" cellspacing="1" class="tableWrap dispalyTable" width="100%" style="border-bottom:0px;">
            <tr class="tdbg">
                <td class="tdbgleft" style="width:200px;" align="right">
                <strong>所属节点 ：&nbsp;</strong></td>
	            <td>
	            	<input type="text" style="width:250px;" class="inputtext" disabled="disabled" id="txt_CheckChannelName" value="${defaultChannel.channelName }"><input type="button" value="选择" class="inputbutton" id="bnt_checked">
	            	<input id="txt_channelId" type="hidden" value="${defaultChannel.channelId }"/>
	            </td>
            </tr>
            <tr class="tdbg">
               <td class="tdbgleft" style="width:200px;" align="right">
                <strong>标题 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
	                	<tr>
		                	<td>
			                <input id="txt_title" type="text" style="width:450px;"/><span style="color:#FF0000;">*</span>
			                </td>
			                <td><div id="txt_titleTip" style="width:280px"></div></td>
		                </tr>
		            </table>
	            </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right">
                    <strong>用户名 ：</strong>
                </td>
                <td>
                	<table cellpadding="0" cellspacing="0" border="0">
	                	<tr>
		                	<td>
			                <input id="txt_guestName" type="text" style="width:250px;"/><span style="color:#FF0000;">*</span>
			                </td>
			                <td><div id="txt_guestNameTip" style="width:280px"></div></td>
		                </tr>
		            </table>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>Email地址 ：</strong></td>
                <td>
                	<table cellpadding="0" cellspacing="0" border="0">
	                	<tr>
		                	<td>
			                <input id="txt_email" type="text" style="width:250px;"/><span style="color:#FF0000;">*</span>
			                </td>
			                <td><div id="txt_emailTip" style="width:280px"></div></td>
		                </tr>
		            </table>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>手机号码 ：</strong></td>
                <td>
                	<table cellpadding="0" cellspacing="0" border="0">
	                	<tr>
		                	<td>
			                <input id="txt_phoneNumber" type="text" style="width:250px;"/>
			                </td>
			                <td><div id="txt_phoneNumberTip" style="width:280px"></div></td>
		                </tr>
		            </table>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>QQ号码 ：</strong></td>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0">
	                	<tr>
		                	<td>
			                <input id="txt_guestOicq" type="text" style="width:250px;"/>
			                </td>
			                <td><div id="txt_guestOicqTip" style="width:280px"></div></td>
		                </tr>
		            </table>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>留言内容 ：&nbsp;</strong></td>
               <td>
               	<script id="editor" type="text/plain"></script>
               </td>
            </tr>
            <tr class="tdbg">
                <td  class="tdbgleft" align="right"><strong>是否隐藏 ：&nbsp;</strong></td>
                <td>
                    <input id="cb_isPrivate" type="checkbox" class="valignMiddle" />
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>状态 ：</strong></td>
               <td>
                  	<input type="radio" class="valignMiddle" id="rd_Status_1" name="Site_Status" value="rd_Status_1"><span class="valignMiddle">草稿</span>
                	<input type="radio" class="valignMiddle" id="rd_Status_2" name="Site_Status" value="rd_Status_2"><span class="valignMiddle">待审核</span>
               	 	<input type="radio" class="valignMiddle" checked="checked" id="rd_Status_3" name="Site_Status" value="rd_Status_3"><span class="valignMiddle">终审通过</span>
               </td>
            </tr>
       </table>
	   <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel2">
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>总点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_totlaHits" value="0" name="txt_TotlaHits"/>
		                </td>
		                <td><div id="txt_totlaHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>日点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_dayHits" value="0" name="txt_DayHits"/>
		                </td>
		                <td><div id="txt_dayHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>周点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_weekHits" value="0" name="txt_WeekHits"/>
		                </td>
		                <td><div id="txt_weekHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>月点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_monthHits" value="0" name="txt_MonthHits"/>
		                </td>
		                <td><div id="txt_monthHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>更新时间 ：&nbsp;</strong></td>
	            <td>
	                <input type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" id="txt_updataTime" value="${date }" />
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>推荐级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_eliteLevel" value="0" name="txt_EliteLevel"/>
		                </td>
		                <td><div id="txt_eliteLevelTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>优先级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_priority" value="0" name="txt_Priority">
		                </td>
		                <td><div id="txt_priorityTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
     	</table>
        <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bnt_save" value="保存">&nbsp;
	                <input type="button" class="inputbutton" id="bnt_back" value="关闭" >
	             </td>
	        </tr>
        </table>
      </c:when>
      <c:otherwise>
         <table border="0" cellpadding="0" cellspacing="0" width="161px">
            <tr align="center">
                <td id="td_tab1" class="titlemouseover" onclick="checkedTab(this,'tb_Channel1');">
                    基本信息</td>
                <td width="2px"></td>
                <td id="td_tab2" class="tabtitle" onclick="checkedTab(this,'tb_Channel2');">
                    属性选项</td>
            </tr>
        </table>
		<table id="tb_Pannel1" border="0" cellpadding="2" cellspacing="1" class="tableWrap dispalyTable" width="100%" style="border-bottom:0px;">
            <tr class="tdbg">
                <td class="tdbgleft" style="width:200px;" align="right">
                <strong>所属节点 ：&nbsp;</strong></td>
	            <td>
	            	<input type="text" style="width:250px;" class="inputtext" disabled="disabled" id="txt_CheckChannelName" value="${guestBookBean.channelName }"><input type="button" value="选择" class="inputbutton" id="bnt_checked">
	            	<input id="txt_channelId" type="hidden" value="${guestBookBean.channelId }"/>
	            	<input id="txt_commonModelId" type="hidden" value="${guestBookBean.commonModelId }"/>
	            </td>
            </tr>
            <tr class="tdbg">
               <td class="tdbgleft" style="width:200px;" align="right">
                <strong>标题 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
	                	<tr>
		                	<td>
			                <input id="txt_title" type="text" style="width:450px;" value="${guestBookBean.title }"/><span style="color:#FF0000;">*</span>
			                </td>
			                <td><div id="txt_titleTip" style="width:280px"></div></td>
		                </tr>
		            </table>
	            </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right">
                    <strong>用户名 ：</strong>
                </td>
                <td>
                	<table cellpadding="0" cellspacing="0" border="0">
	                	<tr>
		                	<td>
			                <input id="txt_guestName" type="text" style="width:250px;" value="${guestBookBean.userName }"/><span style="color:#FF0000;">*</span>
			                </td>
			                <td><div id="txt_guestNameTip" style="width:280px"></div></td>
		                </tr>
		            </table>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>Email地址 ：</strong></td>
                <td>
                	<table cellpadding="0" cellspacing="0" border="0">
	                	<tr>
		                	<td>
			                <input id="txt_email" type="text" style="width:250px;" value="${guestBookBean.guestEmail }"/><span style="color:#FF0000;">*</span>
			                </td>
			                <td><div id="txt_emailTip" style="width:280px"></div></td>
		                </tr>
		            </table>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>手机号码 ：</strong></td>
                <td>
                	<table cellpadding="0" cellspacing="0" border="0">
	                	<tr>
		                	<td>
			                <input id="txt_phoneNumber" type="text" style="width:250px;" value="${guestBookBean.phoneNumber }"/>
			                </td>
			                <td><div id="txt_phoneNumberTip" style="width:280px"></div></td>
		                </tr>
		            </table>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>QQ号码 ：</strong></td>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0">
	                	<tr>
		                	<td>
			                <input id="txt_guestOicq" type="text" style="width:250px;" value="${guestBookBean.guestOicq }"/>
			                </td>
			                <td><div id="txt_guestOicqTip" style="width:280px"></div></td>
		                </tr>
		            </table>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>留言内容 ：&nbsp;</strong></td>
               <td>
               	<script id="editor" type="text/plain">${guestBookBean.guestContent }</script>
               </td>
            </tr>
            <tr class="tdbg">
                <td  class="tdbgleft" align="right"><strong>是否隐藏 ：&nbsp;</strong></td>
                <td>
                    <input id="cb_isPrivate" type="checkbox" class="valignMiddle" <c:if test="${guestBookBean.guestIsPrivate}">checked="checked"</c:if>/>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>状态 ：</strong></td>
                <td>
                <input type="radio" class="valignMiddle" id="rd_Status_1" name="Site_Status" <c:if test="${guestBookBean.status==-1 }">checked="checked"</c:if>/><span class="valignMiddle">草稿</span>
                <input type="radio" class="valignMiddle" id="rd_Status_2" name="Site_Status" <c:if test="${guestBookBean.status==0 }">checked="checked"</c:if>/><span class="valignMiddle">待审核</span>
                <input type="radio" class="valignMiddle" id="rd_Status_3" name="Site_Status" <c:if test="${guestBookBean.status==99 }">checked="checked"</c:if>/><span class="valignMiddle">终审通过</span>
             	</td>
            </tr>
       </table>
	   <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel2">
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>总点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_totlaHits" value="${guestBookBean.hits }"/>
		                </td>
		                <td><div id="txt_totlaHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>日点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_dayHits" value="${guestBookBean.dayHits }"/>
		                </td>
		                <td><div id="txt_dayHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>周点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_weekHits" value="${guestBookBean.weekHits }"/>
		                </td>
		                <td><div id="txt_weekHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>月点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_monthHits" value="${guestBookBean.monthHits }"/>
		                </td>
		                <td><div id="txt_monthHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>更新时间 ：&nbsp;</strong></td>
	            <td>
	                <input type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" id="txt_updataTime" value="${guestBookBean.updateTime }" />
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>推荐级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_eliteLevel" value="${guestBookBean.eliteLevel }"/>
		                </td>
		                <td><div id="txt_eliteLevelTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>优先级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_priority" value="${guestBookBean.priority }"/>
		                </td>
		                <td><div id="txt_priorityTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
     	</table>
        <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bnt_update" value="修 改">&nbsp;
	                <input type="button" class="inputbutton" id="bnt_back" value="返 回" >
	             </td>
	        </tr>
        </table>
	  </c:otherwise>
     </c:choose>
        </form>
	</body>
</html>