<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>添加、修改公司信息</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
    	<script type="text/javascript" src="<%=request.getContextPath()%>/admin/cms/js/company.js"></script>
	</head>
	<body class="mainContent">
	    <form id="companyForm" name="companyForm" action="#">
	    <c:choose>
	    <c:when test="${method eq 'add'}">
	    <table id="tb_Pannel2" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        <tr class="tdbg">
            <td class="tdbgleft" style="width:150px;" align="right"><strong>公司名称 ：&nbsp;</strong></td>
            <td>
           		<table cellpadding="0" cellspacing="0" border="0">
               	<tr>
                	<td>
	                	<input id="companyName" type="text" style="width:250px;"/><span class="cautiontitleRed">*</span>
	                </td>
	                <td><div id="companyNameTip" style="width:280px"></div></td>
                </tr>
            	</table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司简称 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<input id="referred" type="text" style="width:250px;"/><span class="cautiontitleRed">*</span>
						</td>
						<td><div id="referredTip" style="width:280px"></div></td>
				    </tr>
				</table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司性质 ：&nbsp;</strong></td>
            <td>
               <select id="companyPropertieId" name="companyPropertieId">
               <c:forEach var="list" items="${map.properties}">
               		<option value="${list.companyPropertieId}">${list.companyPropertie}</option>
               </c:forEach>
				</select>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司行业 ：&nbsp;</strong></td>
            <td>
               <select id="companyIndustryId" name="companyIndustryId">
			   <c:forEach var="list" items="${map.industrys}">
               	 <option value="${list.companyIndustryId}">${list.companyIndustry}</option>
               </c:forEach>
				</select>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司规模 ：&nbsp;</strong></td>
            <td>
               <select id="companyScaleId" name="companyScaleId">
			   <c:forEach var="list" items="${map.scales}">
               	 <option value="${list.companyScaleId}">${list.companyScale}</option>
               </c:forEach>
			   </select>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>所在地区 ：&nbsp;</strong></td>
            <td>
               <select class="valignMiddle" id="ddl_FirstProvince">
                 <option>请选择...</option>
                 <c:forEach var="list" items="${map.areas}">
                 	<option value="${list.id}">${list.name}</option>
                 </c:forEach>
                </select>
                -
                <select class="valignMiddle" id="ddl_SecondCity">
                	<option value="0">请选择...</option>
                </select>
            	-
            	<select class="valignMiddle" id="ddl_ThirdDistrict">
            		<option value="0">请选择...</option>
				</select>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>详细地址 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<input id="address" type="text" style="width:250px;"/>
						</td>
						<td><div id="addressTip" style="width:280px"></div></td>
				    </tr>
				</table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>邮政编码 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<input id="zipCode" type="text" style="width:200px;"/>
						</td>
						<td><div id="zipCodeTip" style="width:280px"></div></td>
				    </tr>
				</table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>联系人 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<input id="contact" type="text" style="width:200px;"/>
						</td>
						<td><div id="contactTip" style="width:280px"></div></td>
				    </tr>
				</table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司联系电话 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<input id="telephone" type="text" style="width:200px;"/>
						</td>
						<td><div id="telephoneTip" style="width:280px"></div></td>
				    </tr>
				</table>

            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>Email ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<input id="email" type="text" style="width:200px;"/>
						</td>
						<td><div id="emailTip" style="width:280px"></div></td>
				    </tr>
				</table>
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司手机 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<input id="mobile" type="text" style="width:200px;"/>
						</td>
						<td><div id="mobileTip" style="width:280px"></div></td>
				    </tr>
				</table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司简介 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<textarea id="companyIntr" style="height:60px;width:400px;" name="txt_Intr"></textarea>
						</td>
						<td><div id="companyIntrTip" style="width:280px"></div></td>
				    </tr>
				</table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>备注 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<textarea id="remark" style="height:60px;width:400px;" name="txt_Intr"></textarea>
						</td>
						<td><div id="remarkTip" style="width:280px"></div></td>
				    </tr>
				</table>
            </td>
        </tr>
     </table>
      <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input id="saveCompany" type="button" class="inputbutton" value=" 保 存 "/>&nbsp;&nbsp;
	        		<input id="returnCompany" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
	             </td>
	        </tr>
        </table>
     	</c:when>
     	<c:otherwise>
	    <table id="tb_Pannel2" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        <tr class="tdbg">
            <td class="tdbgleft" style="width:150px;" align="right"><strong>公司名称 ：&nbsp;</strong></td>
            <td>
           		<table cellpadding="0" cellspacing="0" border="0">
               	<tr>
                	<td>
	                	<input id="companyName" type="text" style="width:250px;" value="${map.detail.companyName}"/><span class="cautiontitleRed">*</span>
	                	<input id="companyId" type="hidden" value="${map.detail.companyId}"/>
	                </td>
	                <td><div id="companyNameTip" style="width:280px"></div></td>
                </tr>
            	</table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司简称 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<input id="referred" type="text" style="width:250px;" value="${map.detail.referred}"/><span class="cautiontitleRed">*</span>
						</td>
						<td><div id="referredTip" style="width:280px"></div></td>
				    </tr>
				</table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司性质 ：&nbsp;</strong></td>
            <td>
               <select id="companyPropertieId" name="companyPropertieId">
               <c:forEach var="list" items="${map.properties}">
               		<option value="${list.companyPropertieId}">${list.companyPropertie}</option>
               </c:forEach>
				</select>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司行业 ：&nbsp;</strong></td>
            <td>
               <select id="companyIndustryId" name="companyIndustryId">
			   <c:forEach var="list" items="${map.industrys}">
               	 <option value="${list.companyIndustryId}">${list.companyIndustry}</option>
               </c:forEach>
				</select>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司规模 ：&nbsp;</strong></td>
            <td>
               <select id="companyScaleId" name="companyScaleId">
			   <c:forEach var="list" items="${map.scales}">
               	 <option value="${list.companyScaleId}">${list.companyScale}</option>
               </c:forEach>
			   </select>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>所在地区 ：&nbsp;</strong></td>
            <td>
               <select class="valignMiddle" id="ddl_FirstProvince">
                 <option>请选择...</option>
                 <c:forEach var="list" items="${map.areas}">
                 	<option value="${list.id}">${list.name}</option>
                 </c:forEach>
                </select>
                -
                <select class="valignMiddle" id="ddl_SecondCity">
                	<option value="0">请选择...</option>
                </select>
            	-
            	<select class="valignMiddle" id="ddl_ThirdDistrict">
            		<option value="0">请选择...</option>
				</select>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>详细地址 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<input id="address" type="text" style="width:250px;" value="${map.detail.address}"/>
						</td>
						<td><div id="addressTip" style="width:280px"></div></td>
				    </tr>
				</table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>邮政编码 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<input id="zipCode" type="text" style="width:200px;" value="${map.detail.zipCode}"/>
						</td>
						<td><div id="zipCodeTip" style="width:280px"></div></td>
				    </tr>
				</table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>联系人 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<input id="contact" type="text" style="width:200px;" value="${map.detail.contact}"/>
						</td>
						<td><div id="contactTip" style="width:280px"></div></td>
				    </tr>
				</table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司联系电话 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<input id="telephone" type="text" style="width:200px;" value="${map.detail.telephone}"/>
						</td>
						<td><div id="telephoneTip" style="width:280px"></div></td>
				    </tr>
				</table>

            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>Email ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<input id="email" type="text" style="width:200px;" value="${map.detail.email}"/>
						</td>
						<td><div id="emailTip" style="width:280px"></div></td>
				    </tr>
				</table>
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司手机 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<input id="mobile" type="text" style="width:200px;" value="${map.detail.mobile}"/>
						</td>
						<td><div id="mobileTip" style="width:280px"></div></td>
				    </tr>
				</table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司简介 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<textarea id="companyIntr" style="height:60px;width:400px;" name="txt_Intr">${map.detail.companyIntr}</textarea>
						</td>
						<td><div id="companyIntrTip" style="width:280px"></div></td>
				    </tr>
				</table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>备注 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
			        <tr>
						<td>
							<textarea id="remark" style="height:60px;width:400px;" name="txt_Intr">${map.detail.remark}</textarea>
						</td>
						<td><div id="remarkTip" style="width:280px"></div></td>
				    </tr>
				</table>
            </td>
        </tr>
     </table>
      <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input id="updateCompany" type="button" class="inputbutton" value=" 修 改 "/>&nbsp;&nbsp;
	        		<input id="returnCompany" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
	             </td>
	        </tr>
        </table>
     	</c:otherwise>
     </c:choose>
     </form>
	</body>
</html>