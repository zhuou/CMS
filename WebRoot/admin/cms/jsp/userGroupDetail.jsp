<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>用户组详情</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	$(document).ready(function(){
			beautifyBottomButton(['bnt_Close']);
			$('#bnt_Close').click(function(){
				window.top.frames['iframeMenuContent'].callBackResult('1');
			});
		});
    	</script>
	</head>
	<body>
		<table id="table_tab1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" style="border-bottom:0px;">
		<tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>用户组ID：</strong></td>
            <td>
				${bean.groupId }
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>用户组名称：</strong></td>
            <td>
				${bean.groupName }
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>所属公司：</strong></td>
            <td>
				${bean.companyName }
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>用户组描述：</strong></td>
            <td>
				${bean.description }
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>创建时间：</strong></td>
            <td>
				${bean.createTime }
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>创建人：</strong></td>
            <td>
				${bean.createUser }
            </td>
        </tr>
        </table>
        <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
        <tr>
            <td align="center">
                <input type="button" class="inputbutton" id="bnt_Close" value="关闭" >
             </td>
        </tr>
     	</table>
	</body>
</html>