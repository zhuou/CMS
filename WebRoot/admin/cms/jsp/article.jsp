<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>文章新增修改</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
		var reqMethod = '${method}';
		window.UEDITOR_HOME_URL = '<%=request.getContextPath()%>';
    	</script>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/date/WdatePicker.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/editor/ueditor.all.min.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/editor/ueditor.config.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/cms/js/article.js" type="text/javascript"></script>
	</head>
	<body class="mainContent">
	<form id="articleForm" name="articleForm" action="#">
	<c:choose>
	    <c:when test="${method eq 'add'}">
     <table border="0" cellpadding="0" cellspacing="0" width="243px">
        <tr align="center">
            <td id="td_tab1" class="titlemouseover">
                基本信息</td>
            <td style="width:2px"></td>
             <td id="td_tab3" class="tabtitle">
                文章内容</td>
            <td style="width:2px"></td>
            <td id="td_tab2" class="tabtitle">
                属性选项</td>
        </tr>
    </table>
     <table id="tb_Pannel1" border="0" cellpadding="2" cellspacing="1" class="tableWrap dispalyTable" width="100%" style="border-bottom:0px;">
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>所属节点 ：&nbsp;</strong></td>
            <td>
            	<input type="text" style="width:250px;" class="inputtext" disabled="disabled" id="txt_CheckChannelName" value="${defaultChannel.channelName }"><input type="button" value="选择" class="inputbutton" id="bnt_checked">
            	<input id="txt_channelId" type="hidden" value="${defaultChannel.channelId }"/>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>标题前缀 ：&nbsp;</strong></td>
            <td>
            	<input id="txt_titlePrefix" type="text" style="width:120px;" class="input_public"/><span class="valignMiddle">&nbsp;<a name="a_titlePrefix" style="font-weight:bold;">[图文]</a>|&nbsp;<a name="a_titlePrefix" style="font-weight:bold;">[图组]</a>|&nbsp;<a name="a_titlePrefix" style="font-weight:bold;">[推荐]</a></span>
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>标题 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_title" type="text" style="width:450px;"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_titleTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>完整标题 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_titleIntact" type="text" style="width:450px;"/>
		                </td>
		                <td><div id="txt_titleIntactTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>副标题 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_subheading" type="text" style="width:450px;"/>
		                </td>
		                <td><div id="txt_subheadingTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>显示“评论”链接 ：&nbsp;</strong></td>
            <td>
                <input id="cb_showCommentLink" type="checkbox" class="valignMiddle"/><span style="color:red;" class="valignMiddle">列表显示时是否在标题旁显示“评论”链接</span>
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>关键字 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_keyword" type="text" style="width:350px;"/>
		                </td>
		                <td><div id="txt_keywordTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>作者 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_author" type="text" style="width:350px;"/>
		                </td>
		                <td><div id="txt_authorTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>来源 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_copyFrom" type="text" style="width:350px;"/>
		                </td>
		                <td><div id="txt_copyFromTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>简介 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <textarea id="txt_intro" rows="3" cols="55" class="inputtext"></textarea>
		                </td>
		                <td><div id="txt_introTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>封面图片 ：&nbsp;</strong></td>
            <td>
            	 <input id="txt_defaultPicUrl" class="input_public" style="width:300px;"/>&nbsp;<input name="bnt_selectImg" type="button" class="inputbutton" value="从已上传的文件中选择" callback="callbackDefaultPic"/>&nbsp;<input name="bnt_review" reviewfrom="txt_defaultPicUrl" type="button" value=" 预览 " class="inputbutton"/><br />
                 <iframe marginheight="0" marginwidth="0" scrolling="no" src="<%=request.getContextPath()%>/security/loadDefaultImg.html?callback=callbackDefaultPic&iframe=OpendialogWindow_AddArticle" width="100%" height="58px" frameborder="0"></iframe>
            </td>
        </tr>
		<tr class="tdbg">
			<td class="tdbgleft" style="width:200px;" align="right">
                <strong>图片属性 ：&nbsp;</strong></td>
            <td>
				<input id="focusImg" class="valignMiddle" type="checkbox" name="checkbox_setImg"/>
				<span class="valignMiddle">焦点(focus)</span>
				<input id="bigNewsImg" class="valignMiddle" type="checkbox" name="checkbox_setImg"/>
				<span class="valignMiddle">头条(bignews)</span>
				<input id="marqueeImg" class="valignMiddle" type="checkbox" name="checkbox_setImg"/>
				<span class="valignMiddle">滚动(marquee)</span>
            </td>
		</tr>
		<tbody id="tbody_imgs">
		</tbody>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>状态 ：&nbsp;</strong></td>
            <td>
                <input type="radio" class="valignMiddle" id="rd_Status_1" name="Site_Status"><span class="valignMiddle">草稿</span>
                <input type="radio" class="valignMiddle" id="rd_Status_2" name="Site_Status"><span class="valignMiddle">待审核</span>
                <input type="radio" class="valignMiddle" checked="checked" id="rd_Status_3" name="Site_Status"><span class="valignMiddle">终审通过</span>
            </td>
        </tr>
    </table>
    <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel3">
	    <tr class="tdbg">
	       <td style="width: 200px;" align="right" class="tdbgleft"> <strong>文章内容：&nbsp;</strong></td>
	       <td>
				<script id="editor" type="text/plain"></script>
	       </td>
	    </tr>
    </table>
    <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel2">
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>总点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_totlaHits" value="0" name="txt_TotlaHits"/>
		                </td>
		                <td><div id="txt_totlaHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>日点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_dayHits" value="0"/>
		                </td>
		                <td><div id="txt_dayHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>周点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_weekHits" value="0" name="txt_WeekHits"/>
		                </td>
		                <td><div id="txt_weekHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>月点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_monthHits" value="0" name="txt_MonthHits"/>
		                </td>
		                <td><div id="txt_monthHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>更新时间 ：&nbsp;</strong></td>
	            <td>
	                <input type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" id="txt_updataTime" value="${date}"/>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>推荐级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_eliteLevel" value="0"/>
		                </td>
		                <td><div id="txt_eliteLevelTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>优先级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_priority" value="0">
		                </td>
		                <td><div id="txt_priorityTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
			<tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>点“赞”数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_praise" value="0">
		                </td>
		                <td><div id="txt_praiseTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>点“踩”数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_tread" value="0">
		                </td>
		                <td><div id="txt_treadTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
     	</table>
        <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bnt_save" value="保 存">&nbsp;
	                <input type="button" class="inputbutton" id="bnt_back" value="关闭" >
	             </td>
	        </tr>
        </table>
	  </c:when>
      <c:otherwise>
     <table border="0" cellpadding="0" cellspacing="0" width="243px">
        <tr align="center">
            <td id="td_tab1" class="titlemouseover">
                基本信息</td>
            <td style="width:2px"></td>
             <td id="td_tab3" class="tabtitle">
                文章内容</td>
            <td style="width:2px"></td>
            <td id="td_tab2" class="tabtitle">
                属性选项</td>
        </tr>
    </table>
     <table id="tb_Pannel1" border="0" cellpadding="2" cellspacing="1" class="tableWrap dispalyTable" width="100%" style="border-bottom:0px;">
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>所属节点 ：&nbsp;</strong></td>
            <td>
            	<input type="text" style="width:250px;" class="inputtext" disabled="disabled" id="txt_CheckChannelName" value="${articleBean.channelName }"><input type="button" value="选择" class="inputbutton" id="bnt_checked">
            	<input id="txt_channelId" type="hidden" value="${articleBean.channelId }"/>
            	<input id="txt_commonModelId" type="hidden" value="${articleBean.commonModelId }"/>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>标题前缀 ：&nbsp;</strong></td>
            <td>
            	<input id="txt_titlePrefix" type="text" style="width:120px;" class="input_public" value="${articleBean.titlePrefix}"/><span class="valignMiddle">&nbsp;<a name="a_titlePrefix" style="font-weight:bold;">[图文]</a>|&nbsp;<a name="a_titlePrefix" style="font-weight:bold;">[图组]</a>|&nbsp;<a name="a_titlePrefix" style="font-weight:bold;">[推荐]</a></span>
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>标题 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_title" type="text" style="width:450px;" value="${articleBean.title }"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_titleTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>完整标题 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_titleIntact" type="text" style="width:450px;" value="${articleBean.titleIntact }"/>
		                </td>
		                <td><div id="txt_titleIntactTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>副标题 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_subheading" type="text" style="width:450px;" value="${articleBean.subheading }"/>
		                </td>
		                <td><div id="txt_subheadingTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>显示“评论”链接 ：&nbsp;</strong></td>
            <td>
                <input id="cb_showCommentLink" type="checkbox" class="valignMiddle" <c:if test="${articleBean.showCommentLink }">checked="checked"</c:if>/><span style="color:red;" class="valignMiddle">列表显示时是否在标题旁显示“评论”链接</span>
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>关键字 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_keyword" type="text" style="width:350px;" value="${articleBean.keyWord }"/>
		                </td>
		                <td><div id="txt_keywordTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>作者 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_author" type="text" style="width:350px;" value="${articleBean.author }"/>
		                </td>
		                <td><div id="txt_authorTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>来源 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_copyFrom" type="text" style="width:350px;" value="${articleBean.copyFrom }"/>
		                </td>
		                <td><div id="txt_copyFromTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>简介 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <textarea id="txt_intro" rows="3" cols="55" class="inputtext">${articleBean.intro }</textarea>
		                </td>
		                <td><div id="txt_introTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>封面图片 ：&nbsp;</strong></td>
            <td>
            	 <input id="txt_defaultPicUrl" class="input_public" style="width:300px;" value="${articleBean.defaultPicUrl }"/>&nbsp;<input name="bnt_selectImg" type="button" class="inputbutton" value="从已上传的文件中选择" callback="callbackDefaultPic"/>&nbsp;<input name="bnt_review" reviewfrom="txt_defaultPicUrl" type="button" value=" 预览 " class="inputbutton"/><br />
                 <iframe marginheight="0" marginwidth="0" scrolling="no" src="<%=request.getContextPath()%>/security/loadDefaultImg.html?callback=callbackDefaultPic&iframe=OpendialogWindow_AddArticle" width="100%" height="58px" frameborder="0"></iframe>
            </td>
        </tr>
		<tr class="tdbg">
			<td class="tdbgleft" style="width:200px;" align="right">
                <strong>图片属性 ：&nbsp;</strong></td>
            <td>
				<input id="focusImg" class="valignMiddle" type="checkbox" name="checkbox_setImg" <c:if test="${articleBean.focusImgNum>0 }">checked="checked"</c:if>/>
				<span class="valignMiddle">焦点(focus)</span>
				<input id="bigNewsImg" class="valignMiddle" type="checkbox" name="checkbox_setImg" <c:if test="${articleBean.bigNewsImgNum>0 }">checked="checked"</c:if>/>
				<span class="valignMiddle">头条(bignews)</span>
				<input id="marqueeImg" class="valignMiddle" type="checkbox" name="checkbox_setImg" <c:if test="${articleBean.marqueeImgNum>0 }">checked="checked"</c:if>/>
				<span class="valignMiddle">滚动(marquee)</span>
            </td>
		</tr>
		<tbody id="tbody_imgs">
			<c:if test="${articleBean.focusImgNum>0}">
			<tr class="tdbg" id="tr_focusImg">
				<td align="right" style="width:200px;" class="tdbgleft"><strong>焦点图片 ：&nbsp;</strong></td>
				<td>
					<input style="width:300px;" class="input_public" id="txt_focusImg" value="${articleBean.focusImg}"/>&nbsp;<input type="button" value="从已上传的文件中选择" class="inputbutton" name="bnt_selectImg" callback="callbackFocusImg"/>&nbsp;<input type="button" class="inputbutton" value=" 预览 " reviewfrom="txt_focusImg" name="bnt_review"><br><iframe width="100%" scrolling="no" height="58px" frameborder="0" src="<%=request.getContextPath()%>/security/loadDefaultImg.html" marginwidth="0" marginheight="0"></iframe>
				</td>
			</tr>
			</c:if>
			<c:if test="${articleBean.bigNewsImgNum>0}">
				<tr class="tdbg" id="tr_bigNewsImg">
					<td align="right" style="width:200px;" class="tdbgleft"><strong>头条图片 ：&nbsp;</strong></td>
					<td>
						<input style="width:300px;" class="input_public" id="txt_bigNewsImg" value="${articleBean.bigNewsImg}"/>&nbsp;<input type="button" value="从已上传的文件中选择" class="inputbutton" name="bnt_selectImg" callback="callbackBigNewsImg"/>&nbsp;<input type="button" class="inputbutton" value=" 预览 " reviewfrom="txt_bigNewsImg" name="bnt_review"><br><iframe width="100%" scrolling="no" height="58px" frameborder="0" src="<%=request.getContextPath()%>/security/loadDefaultImg.html" marginwidth="0" marginheight="0"></iframe>
					</td>
				</tr>
			</c:if>
			<c:if test="${articleBean.marqueeImgNum>0}">
			<tr class="tdbg" id="tr_marqueeImg">
				<td align="right" style="width:200px;" class="tdbgleft"><strong>滚动图片 ：&nbsp;</strong></td>
				<td>
					<input style="width:300px;" class="input_public" id="txt_marqueeImg" value="${articleBean.marqueeImg}"/>&nbsp;<input type="button" value="从已上传的文件中选择" class="inputbutton" name="bnt_selectImg" callback="callbackMarqueeImg"/>&nbsp;<input type="button" class="inputbutton" value=" 预览 " reviewfrom="txt_marqueeImg" name="bnt_review"><br><iframe width="100%" scrolling="no" height="58px" frameborder="0" src="<%=request.getContextPath()%>/security/loadDefaultImg.html" marginwidth="0" marginheight="0"></iframe>
				</td>
			</tr>
			</c:if>
		</tbody>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>状态 ：&nbsp;</strong></td>
            <td>
                <input type="radio" class="valignMiddle" id="rd_Status_1" name="Site_Status" <c:if test="${articleBean.status==-1 }">checked="checked"</c:if>/><span class="valignMiddle">草稿</span>
                <input type="radio" class="valignMiddle" id="rd_Status_2" name="Site_Status" <c:if test="${articleBean.status==0 }">checked="checked"</c:if>/><span class="valignMiddle">待审核</span>
                <input type="radio" class="valignMiddle" id="rd_Status_3" name="Site_Status" <c:if test="${articleBean.status==99 }">checked="checked"</c:if>/><span class="valignMiddle">终审通过</span>
            </td>
        </tr>
    </table>
    <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel3">
	    <tr class="tdbg">
	       <td style="width: 200px;" align="right" class="tdbgleft"> <strong>文章内容：&nbsp;</strong></td>
	       <td>
				<script id="editor" type="text/plain">${articleBean.content }</script>
	       </td>
	    </tr>
    </table>
    <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel2">
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>总点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_totlaHits" value="${articleBean.hits }"/>
		                </td>
		                <td><div id="txt_totlaHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>日点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_dayHits" value="${articleBean.dayHits }"/>
		                </td>
		                <td><div id="txt_dayHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>周点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_weekHits" value="${articleBean.weekHits }"/>
		                </td>
		                <td><div id="txt_weekHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>月点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_monthHits" value="${articleBean.monthHits }"/>
		                </td>
		                <td><div id="txt_monthHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>更新时间 ：&nbsp;</strong></td>
	            <td>
	                <input type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" id="txt_updataTime" value="${articleBean.updateTime }"/>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>推荐级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_eliteLevel" value="${articleBean.eliteLevel }"/>
		                </td>
		                <td><div id="txt_eliteLevelTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>优先级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_priority" value="${articleBean.priority }">
		                </td>
		                <td><div id="txt_priorityTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>点“赞”数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_praise" value="${articleBean.praise }">
		                </td>
		                <td><div id="txt_praiseTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>点“踩”数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_tread" value="${articleBean.tread }">
		                </td>
		                <td><div id="txt_treadTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
     	</table>
        <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bt_update" value="修 改">&nbsp;
	                <input type="button" class="inputbutton" id="bnt_back" value="关闭" >
	             </td>
	        </tr>
        </table>
      </c:otherwise>
     </c:choose>
    </form>
	</body>
</html>