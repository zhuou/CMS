<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>网站前台页面Url列表</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
		var reqMethod = '${method}';
    	</script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/cms/js/receptionPageUrlList.js"></script>
	</head>
	<body class="mainContent">
		<span class="localSpan">后台管理<span> &gt;&gt; </span><span> 内容管理 </span><span>&gt;&gt; </span><span>网站前台页面Url列表 </span></span>
	    <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
	        <tr class="topbottom">
	            <td colspan="10">
	            	<input id="receptionPageUrl_Add" class="topinput topinput_add" type="button" value="新建"/>
	            	<input id="receptionPageUrl_Update" class="topinput topinput_edit" type="button" value="修改"/>
	            	<input id="receptionPageUrl_Del" class="topinput topinput_del" type="button" value="删除选定选"/>
	            	<input id="receptionPageUrl_SearchText" class="inputtext_public" type="text" alt="请输入网站名称" style="width:150px;">
	            	<input id="receptionPageUrl_Search" class="topinput topinput_search" type="button" value="搜索"/>
	            </td>
	        </tr>
	        <c:choose>
		    	<c:when test="${pageBean.totalRecordNum>0}">
			        <tr class="title">
			            <th style="width:13%;">网站名称</th>
			            <th style="width:18%;">url类型</th>
			            <th style="width:12%;">页面别称</th>
			            <th style="width:12%;">JSP页面路径</th>
			            <th style="width:15%;">Url描述</th>
			            <th style="width:10%;">创建时间</th>
			            <th style="width:10%;">创建者</th>
			            <th style="width:10%;">更新时间</th>
			        </tr>
			        <tbody id="tbody_DataList">
			        <c:forEach var="list" items="${pageBean.dataList}">
			        <tr class="tdbg" value="${list.pageUrlId}">
			            <td align="center"><a id="detail_${list.pageUrlId}" name="${list.pageUrlId}" href="javascript:void(0);">${list.siteName}</a></td>
			            <td align="left">
			            	<c:choose>
			            		<c:when test="${list.urlType==1}">
								首页(/index.html)
			            		</c:when>
			            		<c:when test="${list.urlType==2}">
			            		栏目页(/channel/${list.nickName}.html或/${list.nickName}/index.html)
			            		</c:when>
			            		<c:when test="${list.urlType==3}">
			            		列表页(/${list.nickName}/list_1.html)
			            		</c:when>
			            		<c:when test="${list.urlType==4}">
			            		类型列表页(/${list.nickName}/type_new_list_1.html
			            		</c:when>
			            		<c:when test="${list.urlType==5}">
			            		栏目列表页(/${list.nickName}/girl/list_1.html)
			            		</c:when>
			            		<c:when test="${list.urlType==6}">
								栏目类型列表页(/${list.nickName}/girl/type_new_list_1.html)
			            		</c:when>
			            		<c:when test="${list.urlType==7}">
			            		详情页(/${list.nickName}/100/detail.html)
			            		</c:when>
			            		<c:when test="${list.urlType==8}">
			            		用户自定义页(/${list.nickName}.html)
			            		</c:when>
			            	</c:choose>
			            </td>
			            <td align="center">${list.nickName}</td>
			            <td align="center">${list.jspPath}</td>
			            <td align="left">${list.intro}</td>
			            <td align="center"><fmt:formatDate value="${list.createTime}" type="date" pattern="yyyy-MM-dd HH:mm"/></td>
			            <td align="center">${list.createUser}</td>
			            <td align="center"><fmt:formatDate value="${list.updateTime}" type="date" pattern="yyyy-MM-dd HH:mm"/></td>
			        </tr>
			        </c:forEach>
			        </tbody>
			        <tr align="right" class="tdbg" style="height:28px;">
			        	<td colspan="10">
			        		<c:import url="pageCurrent.jsp">
			        			<c:param name="totalRecordNum" value="${pageBean.totalRecordNum}"></c:param>
			        			<c:param name="pageRecordNum" value="${pageBean.pageRecordNum}"></c:param>
					        </c:import>
			        	</td>
			        </tr>
		        </c:when>
	     		<c:otherwise>
		     		<tr class="tdbg" style="height:80px;">
			             <td align="center">
			                  <strong class="valignMiddle">没有任何数据！</strong>
			             </td>
		            </tr>
	     		</c:otherwise>
      		</c:choose>
     </table>
	</body>
<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
</html>