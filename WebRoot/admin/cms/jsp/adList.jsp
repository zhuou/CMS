<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>广告列表</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/cms/js/adList.js"></script>
	</head>
	<body class="mainContent">
		<span class="localSpan">后台管理<span> &gt;&gt; </span><span> 内容管理 </span><span>&gt;&gt; </span><span>广告列表 </span></span>
	    <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
	        <tr class="topbottom">
	            <td colspan="10">
	            	<input id="ad_Add" class="topinput topinput_detail" type="button" value="新增"/>
	            	<input id="ad_Update" class="topinput topinput_edit" type="button" value="修改"/>
	            	<input id="ad_Del" class="topinput topinput_del" type="button" value="删除选定选"/>
	            </td>
	        </tr>
	        <c:choose>
		    	<c:when test="${pageBean.totalRecordNum>0}">
			        <tr class="title">
			        	<th style="width:4%;"><input id="cb_CheckedAll" type="checkbox"/></th>
			        	<th style="width:6%;">广告ID</th>
			            <th style="width:15%;">广告名称</th>
			            <th style="width:12%;">广告位名称</th>
			            <th style="width:15%;">广告状态</th>
			            <th style="width:5%;">点击数</th>
			            <th style="width:5%;">排序</th>
			            <th style="width:20%;">广告内容描述</th>
			            <th style="width:10%;">创建时间</th>
			            <th style="width:8%;">创建者</th>
			        </tr>
			        <tbody id="tbody_DataList">
			        <c:forEach var="list" items="${pageBean.dataList}">
			        <tr class="tdbg">
			        	<td align="center"><input type="checkbox" value="${list.adId}"/></td>
			        	<td align="center">${list.adId}</td>
			            <td align="center"><a href="#" name="detail_${list.adId}">${list.adName}</a></td>
			            <td align="center">${list.zoneName}</td>
			            <td align="center">
							<c:choose>
			            	<c:when test="${list.passed}">
								<span style="color:#00A600">审核通过</span>&nbsp;<a id="${list.adId}" name="aSetPassed" href="javascript:void(0);" value="false">设为待审核</a>
			            	</c:when>
			            	<c:otherwise>
								<span style="color:#FF0000">待审核</span>&nbsp;<a id="${list.adId}" name="aSetPassed" href="javascript:void(0);" value="true">设为审核通过</a>
			            	</c:otherwise>
			            	</c:choose>
			            </td>
			            <td align="center">${list.clicks}</td>
			            <td align="center">${list.sort}</td>
			            <td align="center" name="txt_adIntro">
			            	${list.adIntro}
			            </td>
			            <td align="center"><fmt:formatDate value="${list.createTime}" type="date" pattern="yyyy-MM-dd HH:mm"/></td>
			            <td align="center">${list.createUser}</td>
			        </tr>
			        </c:forEach>
			        </tbody>
			        <tr align="right" class="tdbg" style="height:28px;">
			        	<td colspan="10">
			        		<c:import url="pageCurrent.jsp">
			        			<c:param name="totalRecordNum" value="${pageBean.totalRecordNum}"></c:param>
			        			<c:param name="pageRecordNum" value="${pageBean.pageRecordNum}"></c:param>
					        </c:import>
			        	</td>
			        </tr>
		        </c:when>
	     		<c:otherwise>
		     		<tr class="tdbg" style="height:80px;">
			             <td align="center" colspan="11">
			                  <strong class="valignMiddle">没有任何数据！</strong>
			             </td>
		            </tr>
	     		</c:otherwise>
      		</c:choose>
     </table>
	</body>
<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
</html>