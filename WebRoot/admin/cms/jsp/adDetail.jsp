<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>广告详情</title>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
	</head>
	<body class="mainContent">
	<table border="0" cellpadding="2" cellspacing="1" class="tableWrap" width="100%" style="border-bottom:0px;">
		<tr class="tdbg">
            <td class="tdbgleft" style="width:15%;"><strong>所属广告位：</strong></td>
            <td>
            	${bean.zoneName}
       		</td>
        </tr>
		<tr class="tdbg">
             <td class="tdbgleft"><strong>广告名称：</strong></td>
             <td>
             	${bean.adName}
             </td>
         </tr>
         <tr class="tdbg">
              <td class="tdbgleft"><strong>广告类型：</strong></td>
              <td>
              	<c:choose>
				<c:when test="${bean.adType==1}">
              	图片广告
              	</c:when>
              	<c:when test="${bean.adType==2}">
              	动画广告
              	</c:when>
              	<c:when test="${bean.adType==3}">
              	文本广告
              	</c:when>
              	<c:otherwise>
              	<span style="color:#FF0000">广告类型错误！</span>
              	</c:otherwise>
              	</c:choose>
              </td>
         </tr>
		<tr class="tdbg">
              <td class="tdbgleft"><strong>广告内容：</strong></td>
              <td>
              	<c:choose>
				<c:when test="${bean.adType==1}">
                  <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
                      <tr class="tdbg">
                          <td class="tdbgleft">图片：</td>
                          <td>
                          	<img src="${bean.imgUrl}" alt="${bean.linkAlt}" <c:if test="${bean.imgWidth>0}">width="${bean.imgWidth}px"</c:if> <c:if test="${bean.imgHeight>0}">height="${bean.imgHeight}px"</c:if>/>
				          </td>
                      </tr>
                      <tr class="tdbg">
                          <td class="tdbgleft">图片尺寸：</td>
                          <td>
                             <table cellpadding="0" cellspacing="0" border="0">
			                	<tr>
				                	<td>宽：${bean.imgWidth} 像素</td>
				                </tr>
				                <tr>
				                	<td>高：${bean.imgHeight} 像素</td>
				                </tr>
				            </table>
                          </td>
                      </tr>
                      <tr class="tdbg">
                          <td class="tdbgleft">链接地址：</td>
                          <td>
                          ${bean.linkUrl}
                          </td>
                      </tr>
                      <tr class="tdbg">
                          <td class="tdbgleft">链接提示：</td>
                          <td>
                          ${bean.linkAlt}
                          </td>
                      </tr>
                      <tr class="tdbg">
                          <td class="tdbgleft">广告简介：</td>
                          <td>
                          ${bean.adIntro}
                          </td>
                      </tr>
                  </table>
                  </c:when>
                  <c:when test="${bean.adType==2}">
                  <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
                      <tr class="tdbg">
                          <td class="tdbgleft" style="width:70px;">动画上传：</td>
                          <td>
                          	${bean.imgUrl}
                          </td>
                      </tr>
                      <tr class="tdbg">
                          <td class="tdbgleft">动画尺寸：</td>
                          <td>
                          	<table cellpadding="0" cellspacing="0" border="0">
			                	<tr>
				                	<td>宽：${bean.imgWidth}像素</td>
				                </tr>
				                <tr>
				                	<td>高：${bean.imgHeight}像素</td>
				                </tr>
				            </table>
                          </td>
                      </tr>
                  </table>
                  </c:when>
                  <c:when test="${bean.adType==3}">
                  <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
                      <tr class="tdbg">
                          <td><strong>广告内容</strong></td>
                      </tr>
                      <tr class="tdbg">
                          <td>
                          	${bean.adIntro}
                          </td>
                      </tr>
                  </table>
                  </c:when>
                  <c:otherwise>
					<span style="color:#FF0000">广告内容显示错误！</span>
                  </c:otherwise>
                  </c:choose>
              </td>
          </tr>
          <tr class="tdbg">
              <td class="tdbgleft"><strong>权重：</strong></td>
              <td>
              	${bean.priority}
              </td>
          </tr>
          <tr class="tdbg">
              <td class="tdbgleft"><strong>排序：</strong></td>
              <td>
              	${bean.sort}
              </td>
          </tr>
          <tr class="tdbg">
              <td class="tdbgleft"><strong>状态：</strong></td>
              <td>
              	<c:choose>
              	<c:when test="${bean.passed}"><span style="color:#00A600">审核通过</span></c:when>
              	<c:otherwise><span style="color:#FF0000">待审核</span></c:otherwise>
              	</c:choose>
              </td>
          </tr>
          <c:if test="${bean.openTimeLimit}">
          <tr class="tdbg">
              <td class="tdbgleft"><strong>时间限制：</strong></td>
              <td class="radioMiddle">
               	<table cellpadding="0" cellspacing="0" border="0">
               		<tr>
               			<td>
						开始时间：<fmt:formatDate value="${bean.startTime}" type="date" pattern="yyyy-MM-dd HH:mm"/>
               			</td>
               		</tr>
               		<tr>
               			<td>
               			 结束时间：<fmt:formatDate value="${bean.endTime}" type="date" pattern="yyyy-MM-dd HH:mm"/>
               			</td>
               		</tr>
               	</table>
              </td>
          </tr>
          </c:if>
           <tr class="tdbg">
              <td class="tdbgleft"><strong>点击次数：</strong></td>
              <td>
              	${bean.clicks}
              </td>
          </tr>
           <tr class="tdbg">
              <td class="tdbgleft"><strong>创建时间：</strong></td>
              <td>
              	<fmt:formatDate value="${bean.createTime}" type="date" pattern="yyyy-MM-dd HH:mm"/>
              </td>
          </tr>
           <tr class="tdbg">
              <td class="tdbgleft"><strong>创建者：</strong></td>
              <td>
              	${bean.createUser}
              </td>
          </tr>
     </table>
     <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
        <tr>
            <td align="center">
                <input type="button" class="inputbutton" id="bnt_close" value="关闭" >
             </td>
        </tr>
     </table>
	</body>
<script type="text/javascript">
$(document).ready(function(){
	$('#bnt_close').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('0');
	});
});
</script>
</html>