<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>公司列表</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
		var reqMethod = '${method}';
    	</script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/cms/js/companyList.js"></script>
	</head>
	<body class="mainContent">
		<span class="localSpan">后台管理<span> &gt;&gt; </span><span> 内容管理 </span><span>&gt;&gt; </span><span>公司列表 </span></span>
	    <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
	        <tr class="topbottom">
	            <td colspan="10">
	            	<input id="company_Add" class="topinput topinput_add" type="button" value="新建"/>
	            	<input id="company_Update" class="topinput topinput_edit" type="button" value="修改"/>
	            	<input id="company_Del" class="topinput topinput_del" type="button" value="删除选定选"/>
	            	<input id="company_SearchText" class="inputtext_public" type="text" alt="请输入公司名称" style="width:150px;">
	            	<input id="company_Search" class="topinput topinput_search" type="button" value="搜索"/>
	            </td>
	        </tr>
	        <c:choose>
		    	<c:when test="${pageBean.totalRecordNum>0}">
			        <tr class="title">
			            <th style="width:17%;">公司名称</th>
			            <th style="width:17%;">公司简称</th>
			            <th style="width:10%;">公司性质</th>
			            <th style="width:12%;">联系地址</th>
			            <th style="width:8%;">联系人</th>
			            <th style="width:8%;">公司联系电话</th>
			            <th style="width:8%;">Email</th>
			            <th style="width:8%;">公司手机</th>
			            <th style="width:12%;">创建时间</th>
			        </tr>
			        <tbody id="tbody_DataList">
			        <c:forEach var="list" items="${pageBean.dataList}">
			        <tr class="tdbg" value="${list.companyId}">
			            <td align="center"><a id="detail_${list.companyId}" href="javascript:void(0);" name="${list.companyId}">${list.companyName}</a></td>
			            <td align="center">${list.referred}</td>
			            <td align="center">${list.companyPropertie}</td>
			            <td align="center">${list.address}</td>
			            <td align="center">${list.contact}</td>
			            <td align="center">${list.telephone}</td>
			            <td align="center">${list.email}</td>
			            <td align="center">${list.mobile}</td>
			            <td align="center">${list.createTime}</td>
			        </tr>
			        </c:forEach>
			        </tbody>
			        <tr align="right" class="tdbg" style="height:28px;">
			        	<td colspan="10">
			        		<c:import url="pageCurrent.jsp">
			        			<c:param name="totalRecordNum" value="${pageBean.totalRecordNum}"></c:param>
			        			<c:param name="pageRecordNum" value="${pageBean.pageRecordNum}"></c:param>
					        </c:import>
			        	</td>
			        </tr>
		        </c:when>
	     		<c:otherwise>
		     		<tr class="tdbg" style="height:80px;">
			             <td align="center">
			                  <strong class="valignMiddle">没有任何数据！</strong>
			             </td>
		            </tr>
	     		</c:otherwise>
      		</c:choose>
     </table>
	</body>
<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
</html>