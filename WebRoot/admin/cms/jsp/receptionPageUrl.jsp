<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>添加修改站点前台URL信息</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
    	<script type="text/javascript" src="<%=request.getContextPath()%>/admin/cms/js/receptionPageUrl.js"></script>
	</head>
	<body class="mainContent">
	    <form id="receptionPageUrlForm" name="receptionPageUrlForm" action="#">
	    <c:choose>
	    	<c:when test="${method eq 'add'}">
        <table id="tb_Pannel1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>所属站点 ：&nbsp;</strong></td>
				<td>
					<select id="select_webList">
					<c:forEach var="list" items="${webList}">
					<option value="${list.siteId}">${list.siteName}</option>
					</c:forEach>
					</select>
					<span style="color:#FF0000;">*</span>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>页面url类型 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr id="td_urlType">
	                	<td>
	                	<div class="valignMiddle"><input type="radio" class="valignMiddle" style="cursor:pointer;" name="rd_pageType" checked="checked" value="1"/>首页(/index.html)</div>
						<div class="valignMiddle"><input type="radio" class="valignMiddle" style="cursor:pointer;" name="rd_pageType" checked="checked" value="2"/>栏目页(/channel/栏目别名.html或/栏目别名/index.html,例如：/channel/about-us.html或/about-us/index.html)</div>
						<div class="valignMiddle"><input type="radio" class="valignMiddle" style="cursor:pointer;" name="rd_pageType" value="3"/>列表页(/页面别名/list_1.html,例如：/article/list_1.html)</div>
						<div class="valignMiddle"><input type="radio" class="valignMiddle" style="cursor:pointer;" name="rd_pageType" value="4"/>类型列表页(/页面别名/type_类型_list_1.html,例如：/article/type_new_list_1.html)</div>
						<div class="valignMiddle"><input type="radio" class="valignMiddle" style="cursor:pointer;" name="rd_pageType" value="5"/>栏目列表页(/页面别名/栏目别名/list_1.html,例如：/pic/girl/list_1.html)</div>
						<div class="valignMiddle"><input type="radio" class="valignMiddle" style="cursor:pointer;" name="rd_pageType" value="6"/>栏目类型列表页(/页面别名/栏目别名/type_类型_list_1.html,例如：/pic/girl/type_new_list_1.html)</div>
						<div class="valignMiddle"><input type="radio" class="valignMiddle" style="cursor:pointer;" name="rd_pageType" value="7"/>详情页(/页面别名/内容Id/detail.html,例如：/article/100/detail.html)</div>
						<div class="valignMiddle"><input type="radio" class="valignMiddle" style="cursor:pointer;" name="rd_pageType" value="8"/>用户自定义页(/页面别名.html,例如：/about-us.html)</div>
		                </td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>Url别称 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_nickName" type="text" style="width:200px;"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_nickNameTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>JSP页面路径 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_jspPath" type="text" style="width:250px;"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_jspPathTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>描述：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
	                	<textarea id="txt_intro" style="height:60px;width:300px;" name="txt_Intr"></textarea>
		                </td>
		                <td><div id="txt_introTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
        </table>
      <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input id="saveReceptionPageUrl" type="button" class="inputbutton" value=" 保 存 "/>&nbsp;&nbsp;
	        		<input id="closeReceptionPageUrl" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
	             </td>
	        </tr>
        </table>
     	</c:when>
     	<c:otherwise>
        <table id="tb_Pannel1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>所属站点 ：&nbsp;</strong></td>
				<td>
					<strong>${bean.siteName}</strong>
					<input id="txt_pageUrlId" type="hidden" value="${bean.pageUrlId}"/>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>页面url类型 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr id="td_urlType">
	                	<td>
	                	<div class="valignMiddle"><input type="radio" class="valignMiddle" style="cursor:pointer;" name="rd_pageType" value="1" <c:if test="${bean.urlType==1}">checked="checked"</c:if>/>首页(/index.html)</div>
						<div class="valignMiddle"><input type="radio" class="valignMiddle" style="cursor:pointer;" name="rd_pageType" value="2" <c:if test="${bean.urlType==2}">checked="checked"</c:if>/>栏目页(/channel/栏目别名.html或/栏目别名/index.html,例如：/channel/about-us.html或/about-us/index.html)</div>
						<div class="valignMiddle"><input type="radio" class="valignMiddle" style="cursor:pointer;" name="rd_pageType" value="3" <c:if test="${bean.urlType==3}">checked="checked"</c:if>/>列表页(/页面别名/list_1.html,例如：/article/list_1.html)</div>
						<div class="valignMiddle"><input type="radio" class="valignMiddle" style="cursor:pointer;" name="rd_pageType" value="4" <c:if test="${bean.urlType==4}">checked="checked"</c:if>/>类型列表页(/页面别名/type_类型_list_1.html,例如：/article/type_new_list_1.html)</div>
						<div class="valignMiddle"><input type="radio" class="valignMiddle" style="cursor:pointer;" name="rd_pageType" value="5" <c:if test="${bean.urlType==5}">checked="checked"</c:if>/>栏目列表页(/页面别名/栏目别名/list_1.html,例如：/pic/girl/list_1.html)</div>
						<div class="valignMiddle"><input type="radio" class="valignMiddle" style="cursor:pointer;" name="rd_pageType" value="6" <c:if test="${bean.urlType==6}">checked="checked"</c:if>/>栏目类型列表页(/页面别名/栏目别名/type_类型_list_1.html,例如：/pic/girl/type_new_list_1.html)</div>
						<div class="valignMiddle"><input type="radio" class="valignMiddle" style="cursor:pointer;" name="rd_pageType" value="7" <c:if test="${bean.urlType==7}">checked="checked"</c:if>/>详情页(/页面别名/内容Id/detail.html,例如：/article/100/detail.html)</div>
						<div class="valignMiddle"><input type="radio" class="valignMiddle" style="cursor:pointer;" name="rd_pageType" value="8" <c:if test="${bean.urlType==8}">checked="checked"</c:if>/>用户自定义页(/页面别名.html,例如：/about-us.html)</div>
		                </td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>Url别称 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_nickName" type="text" style="width:200px;" value="${bean.nickName}" <c:if test="${bean.urlType==1}">disabled="disabled"</c:if>/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_nickNameTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>JSP页面路径 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_jspPath" type="text" style="width:250px;" value="${bean.jspPath}"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_jspPathTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>描述：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
	                	<textarea id="txt_intro" style="height:60px;width:300px;" name="txt_Intr">${bean.intro}</textarea>
		                </td>
		                <td><div id="txt_introTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
        </table>
      <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input id="updateReceptionPageUrl" type="button" class="inputbutton" value=" 修 改 "/>&nbsp;&nbsp;
	        		<input id="closeReceptionPageUrl" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
	             </td>
	        </tr>
        </table>
     	</c:otherwise>
     </c:choose>
     </form>
	</body>
</html>