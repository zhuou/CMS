<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>图片上传</title>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
		$(document).ready(function(){
			beautifyBottomButton(['bnt_submit']);
			$.formValidator.initConfig({formID:"uploadPicForm",theme:"Small",
				onError:function(msg,obj,errorlist){
					alert(msg);
				}
			});
			$("#txt_picName").formValidator({onShowText:"请输入图片名称",onFocus:"图片名称为1-60个字"}).inputValidator({min:1,max:60,onErrorMin:'请输入图片名称',onError:"输入的图片名称长度不合法"});
			$("#txt_picAuthor").formValidator({empty:true}).inputValidator({max:60,onErrorMax:'作者名称最大不能超过60个汉字'});
			$("#txt_picIntro").formValidator({empty:true}).inputValidator({max:200,onErrorMax:'描述信息最大不能超过200个汉字'});
			$("#txt_picUrl").formValidator().inputValidator({min:1,onError:'请选择上传的图片'}).regexValidator({regExp:regexEnum.picture,onError:"请选择图片类型文件"});
		});
    	</script>
	</head>
	<body class="mainContent">
		<form id="uploadPicForm" name="uploadPicForm" action="<%=request.getContextPath()%>/security/uploadPic.html" method="post" enctype="multipart/form-data">
		<table border="0" cellpadding="2" cellspacing="1" width="100%">
			<tr class="tdbg">
	            <td class="tdbgleft">
	                <strong>图片名称：</strong></td>
	            <td>
	            	<input id="txt_picName" name="txt_picName" type="text" style="width:150px;" class="inputtext" />
	            	<input id="iframe" name="iframe" type="hidden" value="${iframe}"/>
	            </td>
        	</tr>
        	<tr class="tdbg">
	            <td class="tdbgleft">
	                <strong>图片作者：</strong></td>
	            <td>
	            	<input id="txt_picAuthor" name="txt_picAuthor" type="text" style="width:150px;" class="inputtext" />
	            </td>
        	</tr>
        	<tr class="tdbg">
	            <td class="tdbgleft">
	                <strong>图片简介：</strong></td>
	            <td>
	            	<textarea id="txt_picIntro" name="txt_picIntro" rows="3" cols="38" class="inputtext"></textarea>
	            </td>
        	</tr>
        	<tr class="tdbg">
	            <td class="tdbgleft">
	                <strong>图片：</strong></td>
	            <td>
	            	<input id="txt_picUrl" name="imgFile" type="file" style="width:250px;" />
	            </td>
        	</tr>
        	<tr class="tdbg">
	            <td colspan="2" align="center">
					<input id="bnt_submit" type="submit" class="inputbutton" value="提交" />
	            </td>
        	</tr>
		</table>
       </form>
	</body>
</html>