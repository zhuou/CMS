<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>网站信息详情</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			beautifyBottomButton(['returnCompany']);
			$('#returnCompany').click(function(){
				window.top.frames['iframeMenuContent'].callBackResult('0');
			});
		});
		</script>
	</head>
	<body class="mainContent">
        <table id="tb_Pannel1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站ID ：&nbsp;</strong></td>
				<td>
					${webBean.siteId}
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站名称 ：&nbsp;</strong></td>
				<td>
					${webBean.siteName}
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站网址 ：&nbsp;</strong></td>
				<td>
					${webBean.siteUrl}
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站logo ：&nbsp;</strong></td>
				<td>
					${webBean.logoUrl}
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站错误页面路径 ：&nbsp;</strong></td>
				<td>
					${webBean.errorPagePath}
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>容许上传图片的格式 ：&nbsp;</strong></td>
				<td>
					${webBean.upLoadImgType}
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>容许上传文件的格式 ：&nbsp;</strong></td>
				<td>
					${webBean.uploadFileType}
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>配置网站缩略图参数 ：&nbsp;</strong></td>
				<td>
					宽*高：${webBean.imgThumbWidth}*${webBean.imgThumbHeight}
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>版权 ：&nbsp;</strong></td>
				<td>
					${webBean.copyright}
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站关键字 ：&nbsp;</strong></td>
				<td>
					${webBean.metaKeywords}
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站描述 ：&nbsp;</strong></td>
				<td>
					${webBean.metaDescription}
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>创建者 ：&nbsp;</strong></td>
				<td>
					${webBean.createUser}
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>创建时间 ：&nbsp;</strong></td>
				<td>
					<fmt:formatDate value="${webBean.createTime}" type="date" pattern="yyyy-MM-dd HH:mm"/>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>修改时间 ：&nbsp;</strong></td>
				<td>
					<fmt:formatDate value="${webBean.updateTime}" type="date" pattern="yyyy-MM-dd HH:mm"/>
				</td>
			</tr>
        </table>
      <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	              <input id="returnCompany" type="button" class="inputbutton" value=" 关 闭 "/>
	             </td>
	        </tr>
        </table>
	</body>
</html>