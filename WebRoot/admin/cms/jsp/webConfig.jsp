<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>添加修改站点信息</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
    	<script type="text/javascript" src="<%=request.getContextPath()%>/admin/cms/js/webConfig.js"></script>
	</head>
	<body class="mainContent">
	    <form id="webConfigForm" name="companyForm" action="#">
	    <c:choose>
	    	<c:when test="${method eq 'add'}">
        <table id="tb_Pannel1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>所属公司 ：&nbsp;</strong></td>
				<td>
					<select id="select_company">
					<c:forEach var="list" items="${list}">
					<option value="${list.companyId}">${list.companyName}</option>
					</c:forEach>
					</select>
					<span style="color:#FF0000;">*</span>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站名称 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_siteName" type="text" style="width:250px;"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_siteNameTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站网址 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_siteUrl" type="text" style="width:250px;"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_siteUrlTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站logo ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_logoUrl" type="text" style="width:250px;"/>
		                </td>
		                <td><div id="txt_logoUrlTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站错误页面路径：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_errorPagePath" type="text" style="width:250px;"/>
		                </td>
		                <td><div id="txt_errorPagePathTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>容许上传图片的格式 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_upLoadImgType" type="text" style="width:250px;" value="jpg,jpeg,png,gif,bmp"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_upLoadImgTypeTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>容许上传文件的格式 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_uploadFileType" type="text" style="width:250px;" value="rar,doc,docx,zip,pdf,txt,swf,wmv"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_uploadFileTypeTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>配置网站缩略图参数 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
	                	宽：<input id="txt_imgThumbWidth" type="text" style="width:50px;" value="100"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td>&nbsp;</td>
		                <td>
	                	高：<input id="txt_imgThumbHeight" type="text" style="width:50px;" value="100"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_imgThumbWidthTip"></div></td>
		                <td><div id="txt_imgThumbHeightTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>版权 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_copyright" type="text" style="width:250px;"/>
		                </td>
		                <td><div id="txt_copyrightTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站关键字 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <textarea id="txt_metaKeywords" style="height:60px;width:300px;" name="txt_Intr"></textarea>
		                </td>
		                <td><div id="txt_metaKeywordsTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站描述 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <textarea id="txt_metaDescription" style="height:60px;width:300px;" name="txt_Intr"></textarea>
		                </td>
		                <td><div id="txt_metaDescriptionTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
        </table>
      <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input id="saveWebConfig" type="button" class="inputbutton" value=" 保 存 "/>&nbsp;&nbsp;
	        		<input id="returnWebConfig" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
	             </td>
	        </tr>
        </table>
     	</c:when>
     	<c:otherwise>
        <table id="tb_Pannel1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        	<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>所属公司 ：&nbsp;</strong></td>
				<td>
					<select id="select_company" disabled="disabled">
					<c:forEach var="list" items="${list}">
					<option value="${list.companyId}" <c:if test="${webBean.companyId==list.companyId}">selected="selected"</c:if>>${list.companyName}</option>
					</c:forEach>
					</select>
					<span style="color:#FF0000;">*</span>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站名称 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_siteName" type="text" style="width:250px;" value="${webBean.siteName}"/><span style="color:#FF0000;">*</span>
		                <input id="siteId" type="hidden" value="${webBean.siteId}"/>
		                </td>
		                <td><div id="txt_siteNameTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站网址 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_siteUrl" type="text" style="width:250px;" value="${webBean.siteUrl}"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_siteUrlTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站logo ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_logoUrl" type="text" style="width:250px;" value="${webBean.logoUrl}"/>
		                </td>
		                <td><div id="txt_logoUrlTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站错误页面路径：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_errorPagePath" type="text" style="width:250px;" value="${webBean.errorPagePath}"/>
		                </td>
		                <td><div id="txt_errorPagePathTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>容许上传图片的格式 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_upLoadImgType" type="text" style="width:250px;" value="${webBean.upLoadImgType}"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_upLoadImgTypeTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>容许上传文件的格式 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_uploadFileType" type="text" style="width:250px;" value="${webBean.uploadFileType}"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_uploadFileTypeTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>配置网站缩略图参数 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
	                	宽：<input id="txt_imgThumbWidth" type="text" style="width:50px;" value="${webBean.imgThumbWidth}"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td>&nbsp;</td>
		                <td>
	                	高：<input id="txt_imgThumbHeight" type="text" style="width:50px;" value="${webBean.imgThumbHeight}"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_imgThumbWidthTip"></div></td>
		                <td><div id="txt_imgThumbHeightTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>版权 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_copyright" type="text" style="width:250px;" value="${webBean.copyright}"/>
		                </td>
		                <td><div id="txt_copyrightTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站关键字 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <textarea id="txt_metaKeywords" style="height:60px;width:300px;" name="txt_Intr">${webBean.metaKeywords}</textarea>
		                </td>
		                <td><div id="txt_metaKeywordsTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>网站描述 ：&nbsp;</strong></td>
				<td>
					<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <textarea id="txt_metaDescription" style="height:60px;width:300px;" name="txt_Intr">${webBean.metaDescription}</textarea>
		                </td>
		                <td><div id="txt_metaDescriptionTip"></div></td>
	                </tr>
	            	</table>
				</td>
			</tr>
        </table>
      <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input id="updateWebConfig" type="button" class="inputbutton" value=" 修 改 "/>&nbsp;&nbsp;
	        		<input id="returnWebConfig" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
	             </td>
	        </tr>
        </table>
     	</c:otherwise>
     </c:choose>
     </form>
	</body>
</html>