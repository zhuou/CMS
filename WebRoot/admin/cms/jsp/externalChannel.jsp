<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>添加容器栏目</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/cms/js/externalChannel.js" type="text/javascript"></script>
    	<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
		var reqMethod = '${method}';
    	</script>
	</head>
	<body class="mainContent">
	    <form id="channelForm" name="companyForm" action="#">
	    <c:choose>
	    <c:when test="${method eq 'add'}">
	    <table border="0" cellpadding="0" cellspacing="0" width="162px">
	        <tr align="center">
	            <td id="td_tab1" class="titlemouseover">
	                基本信息</td>
	            <td style="width:2px;"></td>
	            <td id="td_tab2" class="tabtitle">
	                访问权限</td>
	        </tr>
        </table>
        <table id="tb_Pannel1" border="0" cellpadding="2" cellspacing="1" class="tableWrap dispalyTable" width="100%">
        <tr class="tdbg">
            <td class="tdbgleft" style="width:300px;">
                <strong>所属栏目：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
						<input id="txt_CheckChannelName" type="text" disabled="disabled" style="width:250px" value="无(所添加的栏目为根节点)"/><input id="bt_Checked" type="button" class="inputbutton" value="选择"/>
            			<input id="txt_ParentId" type="hidden" value="-1"/>
		                </td>
		                <td><div id="txt_CheckChannelNameTip"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft">
                <strong>外部链接栏目名称：</strong>
            </td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_ChannelName" type="text" class="inputtext" style="width:200px"/>
		                </td>
		                <td><div id="txt_ChannelNameTip"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft">
                <strong>栏目顺序：</strong><br />
                <span style="color:Red;">只能是数字，在页面显示的栏目顺序</span>
            </td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_Sort" type="text" style="width:30px" value="1"/>
		                </td>
		                <td><div id="txt_SortTip"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft">
                <strong>打开方式：</strong></td>
            <td valign="middle" style="height: 22px;">
                <div class="radioMiddle">
                	<input id="rb_OpenType1" type="radio" checked="checked" name="rb_OpenType"/><label>在原窗口打开</label>
                	<input id="rb_OpenType2" type="radio" name="rb_OpenType"/><label>在新窗口打开</label>
                </div>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft">
                <strong>外部链接地址：</strong>
            </td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_ChannelUrl" type="text" style="width:300px"/>
		                </td>
		                <td><div id="txt_ChannelUrlTip"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft">
                <strong>外部链接图片地址：</strong><br />
                <span style="color:Red;">用于在栏目页显示指定的图片</span>
            </td>
            <td>
            <table cellpadding="0" cellspacing="0" border="0">
                	<tr>
		                <td>
							<input id="txt_IconUrl" type="text" style="width:300px" class="input_public"/>&nbsp;<input id="bnt_selectImg" name="bnt_selectImg" type="button" class="inputbutton" value="从已上传的文件中选择" callback="callbackDefaultPic"/>&nbsp;<input id="bnt_review" reviewfrom="txt_IconUrl" type="button" value=" 预览 " class="inputbutton"/><br />
							<iframe marginheight="0" marginwidth="0" scrolling="no" src="<%=request.getContextPath()%>/security/loadDefaultImg.html?callback=callbackDefaultPic&iframe=OpendialogWindow_AddChannel" width="100%" height="58px" frameborder="0"></iframe>
		                </td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft">
                <strong>栏目提示：</strong><br />
                <span style="color:Red;">鼠标移至栏目名称上时将显示设定的提示文字(不支持HTML)</span>
            </td>
            <td>
                <textarea id="txt_Tip" rows="2" style="width:330px"></textarea>
            </td>
        </tr>
      </table>
	    <table id="tb_Pannel2" border="0" cellpadding="2" cellspacing="1" class="tableWrap diplayNone" width="100%">
	        <tr class="tdbg">
	            <td class="tdbgleft" style="width:300px;"> <strong>阅读权限：</strong></td>
	            <td>
	                <table border="0" cellpadding="2" cellspacing="1" width="100%">
	                    <tr style="height: 22px;">
	                        <td>
	                            <div class="radioMiddle">
	                            	<input id="rb_ReadPermAll" type="radio" checked="checked" name="rd_ReadPerm"/>
	                            	<label>所有用户（不限制任何对该栏目的访问）</label>
	                            </div>
	                        </td>
	                    </tr>
	                    <tr style="height: 22px;">
	                        <td>
	                            <div class="radioMiddle">
	                            	<input id="rb_ReadPermMember" type="radio" name="rd_ReadPerm"/><label>指定会员组（想单独对某些信息进行阅读权限设置，可以选择此项）</label>
	                            </div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td style="padding-left:20px;" id="td_AccessRight">
								<c:forEach var="list" items="${beans}">
									<div style="height:20px;">
                                	<input type="checkbox" value="${list.groupId}" id="cb_UserGroups_${list.groupId}"/><span style="vertical-align:middle;">${list.groupName}</span>
                                    </div>
								</c:forEach>
	                        </td>
	                    </tr>
	                </table>
	            </td>
	        </tr>
	    </table>
	    <table border="0" cellpadding="2" cellspacing="1" width="100%">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bt_Save" value="保存">&nbsp;
	                <input type="button" class="inputbutton" id="bt_Close" value="关闭" >
	            </td>
	        </tr>
	    </table>
	    </c:when>
     	<c:otherwise>
	    <table border="0" cellpadding="0" cellspacing="0" width="162px">
	        <tr align="center">
	            <td id="td_tab1" class="titlemouseover">
	                基本信息</td>
	            <td style="width:2px;"></td>
	            <td id="td_tab2" class="tabtitle">
	                访问权限</td>
	        </tr>
        </table>
        <table id="tb_Pannel1" border="0" cellpadding="2" cellspacing="1" class="tableWrap dispalyTable" width="100%">
        <tr class="tdbg">
            <td class="tdbgleft" style="width:300px;">
                <strong>所属栏目：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
					<c:choose>
	            		<c:when test="${empty parent}">
	            	<input id="txt_CheckChannelName" type="text" disabled="disabled" style="width:250px" value="无(所添加的栏目为根节点)"/>
	            	<input id="txt_ParentId" type="hidden" value="-1"/>
	            		</c:when>
	            		<c:otherwise>
	            	<input id="txt_CheckChannelName" type="text" disabled="disabled" style="width:250px" value="${parent.channelName}"/>
	            	<input id="txt_ParentId" type="hidden" value="${parent.channelId}"/>
	            		</c:otherwise>
	            	</c:choose>
		                </td>
		                <td><div id="txt_CheckChannelNameTip"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft">
                <strong>栏目名称：</strong>
            </td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_ChannelName" type="text" class="inputtext" style="width:200px" value="${channel.channelName}"/><input id="txt_ChannelId" type="hidden" value="${channel.channelId}"/><span class="cautiontitleRed">*</span>
		                </td>
		                <td><div id="txt_ChannelNameTip"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft">
                <strong>栏目顺序：</strong><br />
                <span style="color:Red;">只能是数字，在页面显示的栏目顺序</span>
            </td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_Sort" type="text" style="width:30px" value="${channel.sort}"/>
		                </td>
		                <td><div id="txt_SortTip"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft">
                <strong>打开方式：</strong></td>
            <td valign="middle" style="height: 22px;">
                <div class="radioMiddle">
                	<input id="rb_OpenType1" type="radio" <c:if test="${channel.openType==0}">checked="checked"</c:if> name="rb_OpenType"/><label>在原窗口打开</label>
                	<input id="rb_OpenType2" type="radio" <c:if test="${channel.openType==1}">checked="checked"</c:if> name="rb_OpenType"/><label>在新窗口打开</label>
                </div>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft">
                <strong>外部链接地址：</strong>
            </td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_ChannelUrl" type="text" style="width:300px" value="${channel.channelUrl}"/>
		                </td>
		                <td><div id="txt_ChannelUrlTip"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft">
                <strong>外部链接图片地址：</strong><br />
                <span style="color:Red;">用于在栏目页显示指定的图片</span>
            </td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
		                <td>
							<input id="txt_IconUrl" type="text" style="width:300px" class="input_public" value="${channel.iconUrl}"/>&nbsp;<input id="bnt_selectImg" name="bnt_selectImg" type="button" class="inputbutton" value="从已上传的文件中选择" callback="callbackDefaultPic"/>&nbsp;<input id="bnt_review" reviewfrom="txt_IconUrl" type="button" value=" 预览 " class="inputbutton"/><br />
							<iframe marginheight="0" marginwidth="0" scrolling="no" src="<%=request.getContextPath()%>/security/loadDefaultImg.html?callback=callbackDefaultPic&iframe=OpendialogWindow_AddChannel" width="100%" height="58px" frameborder="0"></iframe>
		                </td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft">
                <strong>栏目提示：</strong><br />
                <span style="color:Red;">鼠标移至栏目名称上时将显示设定的提示文字(不支持HTML)</span>
            </td>
            <td>
                <textarea id="txt_Tip" rows="2" style="width:330px">${channel.tips}</textarea>
            </td>
        </tr>
      </table>
	    <table id="tb_Pannel2" border="0" cellpadding="2" cellspacing="1" class="tableWrap diplayNone" width="100%">
	        <tr class="tdbg">
	            <td class="tdbgleft" style="width:300px;"> <strong>阅读权限：</strong></td>
	            <td>
	                <table border="0" cellpadding="2" cellspacing="1" width="100%">
	                    <tr style="height: 22px;">
	                        <td>
	                            <div class="radioMiddle">
	                            	<input id="rb_ReadPermAll" type="radio" checked="checked" name="rd_ReadPerm"/>
	                            	<label>所有用户（不限制任何对该栏目的访问）</label>
	                            </div>
	                        </td>
	                    </tr>
	                    <tr style="height: 22px;">
	                        <td>
	                            <div class="radioMiddle">
	                            	<input id="rb_ReadPermMember" type="radio" name="rd_ReadPerm"/><label>指定会员组（想单独对某些信息进行阅读权限设置，可以选择此项）</label>
	                            </div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td style="padding-left:20px;" id="td_AccessRight">
								<c:forEach var="list" items="${beans}">
									<div style="height:20px;">
                                	<input type="checkbox" value="${list.groupId}" id="cb_UserGroups_${list.groupId}"/><span style="vertical-align:middle;">${list.groupName}</span>
                                    </div>
								</c:forEach>
	                        </td>
	                    </tr>
	                </table>
	            </td>
	        </tr>
	    </table>
	    <table border="0" cellpadding="2" cellspacing="1" width="100%">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bt_Update" value="修 改">&nbsp;
	                <input type="button" class="inputbutton" id="bt_Close" value="关闭" >
	            </td>
	        </tr>
	    </table>
     	</c:otherwise>
     	</c:choose>
        </form>
	</body>
</html>