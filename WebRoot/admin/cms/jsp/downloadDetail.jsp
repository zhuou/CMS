<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>下载详情页面</title>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
	</head>
	<body class="mainContent">
     <table border="0" cellpadding="0" cellspacing="0" width="243px">
        <tr align="center">
            <td id="td_tab1" class="titlemouseover">
                基本信息</td>
            <td style="width:2px"></td>
            <td id="td_tab3" class="tabtitle">
                扩展信息</td>
            <td style="width:2px"></td>
            <td id="td_tab2" class="tabtitle">
                属性选项</td>
        </tr>
    </table>
	 <table id="tb_Pannel1" border="0" cellpadding="2" cellspacing="1" class="tableWrap dispalyTable" width="100%" style="border-bottom:0px;">
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>所属节点 ：&nbsp;</strong></td>
            <td>
            	${downloadBean.channelName }
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件/文件名称 ：&nbsp;</strong></td>
            <td>
               ${downloadBean.title }
	        </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>关键字 ：&nbsp;</strong></td>
            <td>
              ${downloadBean.keyword }
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>作者/开发商 ：&nbsp;</strong></td>
            <td>
               ${downloadBean.author }
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件/文件简介 ：&nbsp;</strong></td>
            <td>
				${downloadBean.fileIntro }
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>封面图片 ：&nbsp;</strong></td>
            <td>
            	${downloadBean.defaultPicUrl }
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件/文件下载地址 ：&nbsp;</strong></td>
            <td>
             <c:forEach var="list" items="${downloadBean.downloadUrlList}">
				${list}
			</c:forEach>
             </td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>文件大小 ：&nbsp;</strong></td>
            <td>
            	${downloadBean.fileSize }
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>解压密码 ：&nbsp;</strong></td>
            <td>
            	${downloadBean.decompressPassword }
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>状态 ：&nbsp;</strong></td>
             <td>
                 <c:choose>
                  	<c:when test="${downloadBean.status==-3}"><span style="color:#FF0000">已删除</span></c:when>
                  	<c:when test="${downloadBean.status==-1}"><span style="color:#2828FF">草稿</span></c:when>
                  	<c:when test="${downloadBean.status==0}"><span style="color:#000000">待审核</span></c:when>
                  	<c:otherwise><span style="color:#00A600">已审核</span></c:otherwise>
                  </c:choose>
             </td>
        </tr>
    </table>
    <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel3">
		 <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件版本 ：&nbsp;</strong></td>
            <td>
            	${downloadBean.fileVersion }
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件来源 ：&nbsp;</strong></td>
            <td>
            	${downloadBean.copyFrom }
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件演示地址 ：&nbsp;</strong></td>
            <td>
            	${downloadBean.demoUrl }
            </td>
        </tr>
       <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件平台 ：&nbsp;</strong></td>
            <td>
            	${downloadBean.operatingSystem }
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件类别 ：&nbsp;</strong></td>
            <td>
            	${downloadBean.fileType }
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件语言 ：&nbsp;</strong></td>
            <td>
            	${downloadBean.fileLanguage }
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>授权方式 ：&nbsp;</strong></td>
            <td>
            	${downloadBean.copyrightType }
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件注册地址 ：&nbsp;</strong></td>
            <td>
            	${downloadBean.regUrl }
             </td>
        </tr>
    </table>
     <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel2">
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>总点击数 ：&nbsp;</strong></td>
	            <td>
	            	${downloadBean.hits }
	            </td>
	        </tr>
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>日点击数 ：&nbsp;</strong></td>
	            <td>
	            	${downloadBean.dayHits }
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>周点击数 ：&nbsp;</strong></td>
	            <td>
	            	${downloadBean.weekHits }
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>月点击数 ：&nbsp;</strong></td>
	            <td>
	            	${downloadBean.monthHits }
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>更新时间 ：&nbsp;</strong></td>
	            <td>
	            	${downloadBean.updateTime }
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>推荐级 ：&nbsp;</strong></td>
	            <td>
	            	${downloadBean.eliteLevel }
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>优先级 ：&nbsp;</strong></td>
	            <td>
	            	${downloadBean.priority }
	            </td>
	        </tr>
     	</table>
       <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	    <tr>
	       <td align="left">
	         <input type="button" class="inputbutton" id="bt_Update" value="修改/审核">&nbsp;
	         <c:if test="${downloadBean.status!=-3}">
	         <input type="button" class="inputbutton" id="bt_Del" value="删除" >&nbsp;
			 </c:if>
	         <c:choose>
	         <c:when test="${downloadBean.status==-1||downloadBean.status==0}"><input type="button" class="inputbutton" id="bt_Passed" value="审核通过" >&nbsp;</c:when>
	         <c:when test="${downloadBean.status==-3}"><input type="button" class="inputbutton" id="bt_Restore" value="还原" >&nbsp;</c:when>
             <c:otherwise><input type="button" class="inputbutton" id="bt_CancelPassed" value="取消审核" >&nbsp;</c:otherwise>
             </c:choose>
	         <input type="button" class="inputbutton" id="bt_Back" value="关闭" >
	       </td>
	    </tr>
     </table>
<script type="text/javascript">
$(document).ready(function(){
	var id = '${downloadBean.commonModelId}';
	pageTab({'td_tab1':'tb_Pannel1','td_tab2':'tb_Pannel2','td_tab3':'tb_Pannel3'});
	beautifyBottomButton(['bt_Update','bt_Del','bt_Passed','bt_CancelPassed','bt_Back']);
	$('#bt_Del').click(function(){
		updateStatus('-3');
	});
	$('#bt_Passed').click(function(){
		updateStatus('99');
	});
	$('#bt_CancelPassed').click(function(){
		updateStatus('0');
	});
	$('#bt_Restore').click(function(){
		updateStatus('0');
	});
	$('#bt_Back').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('1');
	});
	function updateStatus(status){
		$.post(contextPath+'/security/updateStatus.html',{status:status,ids:id},function(data){
			if(data.code=='200'){
				alert(data.content);
				window.location.reload();
			}
			else{
				alert(data.content);
			}
		},'json');
	};
	$('#bt_Update').click(function(){
		window.top.frames['iframeMenuContent'].toUpdatePage(id);
	});
});
</script>
	</body>
</html>