<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>图片新增修改页面</title>
		<script type="text/javascript">
		var contextPath = '<%=request.getContextPath()%>';
		var reqMethod = '${method}';
		window.UEDITOR_HOME_URL = '<%=request.getContextPath()%>';
    	</script>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/date/WdatePicker.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/editor/ueditor.all.min.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/editor/ueditor.config2.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/cms/js/pic.js" type="text/javascript"></script>
		<script type="text/javascript">
		<c:if test="${method eq 'update'}">
		<c:if test="${picBean.list != null && fn:length(picBean.list)>0}">
		$(document).ready(function(){
		<c:forEach var="list" items="${picBean.list}" varStatus="status">
			if(!$.isEmptyObject(${list.json})){
				$('body').data('opt_${status.count}','${list.json}');
			}
		</c:forEach>
		});
		</c:if>
		</c:if>
    	</script>
	</head>
	<body class="mainContent">
	<form id="picForm" name="picForm" action="#">
		<c:choose>
	    <c:when test="${method eq 'add'}">
         <table border="0" cellpadding="0" cellspacing="0" width="161px">
            <tr align="center">
                <td id="td_tab1" class="titlemouseover">
                    基本信息</td>
                <td style="width:2px;"></td>
                <td id="td_tab2" class="tabtitle">
                    属性选项</td>
            </tr>
        </table>
		<table id="tb_Pannel1" border="0" cellpadding="2" cellspacing="1" class="tableWrap dispalyTable" width="100%" style="border-bottom:0px;">
            <tr class="tdbg">
                <td class="tdbgleft" style="width:200px;" align="right">
                <strong>所属节点 ：&nbsp;</strong></td>
	            <td>
	            	<input type="text" style="width:250px;" class="inputtext" disabled="disabled" id="txt_CheckChannelName" value="${defaultChannel.channelName }"><input type="button" value="选择" class="inputbutton" id="bnt_checked">
	            	<input id="txt_channelId" type="hidden" value="${defaultChannel.channelId }"/>
	            </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>图片名称 ：&nbsp;</strong></td>
               <td>
                   <table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_title" type="text" style="width:450px;"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_titleTip" style="width:280px"></div></td>
	                </tr>
	            	</table>
               </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>关键字 ：</strong></td>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_keyword" type="text" style="width:350px;"/>
		                </td>
		                <td><div id="txt_keywordTip" style="width:280px"></div></td>
	                </tr>
	            </table>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>作者 ：</strong></td>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_author" type="text" style="width:350px;"/>
		                </td>
		                <td><div id="txt_authorTip" style="width:280px"></div></td>
	                </tr>
	            </table>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>图片来源 ：</strong></td>
                <td>
                   	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_copyFrom" type="text" style="width:350px;"/>
		                </td>
		                <td><div id="txt_copyFromTip" style="width:280px"></div></td>
	                </tr>
	            	</table>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>图片简介 ：&nbsp;</strong></td>
               <td>
               	<script id="editor" type="text/plain"></script>
               </td>
            </tr>
              <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>图片地址 ：</strong></td>
                <td>
                    <table border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td>
                                <select size="4" id="select_picUrl" class="inputtext" style="height:165px;width:400px;">
                                </select>
                            </td>
                            <td>
                                <input id="bnt_showCheckImgUrl" type="button" class="inputbutton" value="从已上传的文件中选择" /><br />
                                <input id="bnt_upload" type="button" value="上传文件..." class="inputbutton"/><br />
                                <input id="bnt_addExternalUrl" type="button" value="添加外部地址" class="inputbutton" /><br />
                                <input id="bnt_showPreviewImg" type="button" value="预览图片" class="inputbutton" /><br />
                                <input id="bnt_removeUrl" type="button" value="删除当前文件" class="inputbutton" /><br />
                                <input id="bnt_moveNext" type="button" value="向下移" class="inputbutton" /><br />
                                <input id="bnt_movePrev" type="button" value="向上移" class="inputbutton" /><br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="tdbg">
                 <td class="tdbgleft" style="width:200px;" align="right">
                    <strong>封面图片 ：&nbsp;</strong></td>
                <td>
                    <input id="txt_defaultPicUrl" class="input_public" style="width:300px;"/>&nbsp;<input id="bnt_selectImg" type="button" class="inputbutton" value="从已上传的文件中选择" />&nbsp;<input id="bnt_review" type="button" value=" 预览 " class="inputbutton"/><br />
                 	<iframe marginheight="0" marginwidth="0" scrolling="no" src="<%=request.getContextPath()%>/security/loadDefaultImg.html?iframe=OpendialogWindow_AddPic&callback=callbackDefaultPic" width="100%" height="58px" frameborder="0"></iframe>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>状态 ：&nbsp;</strong></td>
               <td>
                    <input type="radio" class="valignMiddle" id="rd_Status_1" name="Site_Status" value="rd_Status_1"><span class="valignMiddle">草稿</span>
                <input type="radio" class="valignMiddle" id="rd_Status_2" name="Site_Status" value="rd_Status_2"><span class="valignMiddle">待审核</span>
                <input type="radio" class="valignMiddle" checked="checked" id="rd_Status_3" name="Site_Status" value="rd_Status_3"><span class="valignMiddle">终审通过</span>
               </td>
            </tr>
       </table>
      <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel2">
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>总点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_totlaHits" value="0" name="txt_TotlaHits"/>
		                </td>
		                <td><div id="txt_totlaHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>日点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_dayHits" value="0" name="txt_DayHits"/>
		                </td>
		                <td><div id="txt_dayHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>周点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_weekHits" value="0" name="txt_WeekHits"/>
		                </td>
		                <td><div id="txt_weekHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>月点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_monthHits" value="0" name="txt_MonthHits"/>
		                </td>
		                <td><div id="txt_monthHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>更新时间 ：&nbsp;</strong></td>
	            <td>
	                <input type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" id="txt_updataTime" value="${date}" />
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>推荐级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_eliteLevel" value="0" name="txt_EliteLevel"/>
		                </td>
		                <td><div id="txt_eliteLevelTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>优先级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_priority" value="0" name="txt_Priority">
		                </td>
		                <td><div id="txt_priorityTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
     	</table>
        <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bnt_save" value="保存">&nbsp;
	                <input type="button" class="inputbutton" id="bnt_back" value="关闭" >
	             </td>
	        </tr>
        </table>
		</c:when>
        <c:otherwise>
         <table border="0" cellpadding="0" cellspacing="0" width="161px">
            <tr align="center">
                <td id="td_tab1" class="titlemouseover">
                    基本信息</td>
                <td style="width:2px;"></td>
                <td id="td_tab2" class="tabtitle">
                    属性选项</td>
            </tr>
        </table>
		<table id="tb_Pannel1" border="0" cellpadding="2" cellspacing="1" class="tableWrap dispalyTable" width="100%" style="border-bottom:0px;">
            <tr class="tdbg">
                <td class="tdbgleft" style="width:200px;" align="right">
                <strong>所属节点 ：&nbsp;</strong></td>
	            <td>
	            	<input type="text" style="width:250px;" class="inputtext" disabled="disabled" id="txt_CheckChannelName" value="${picBean.channelName }"><input type="button" value="选择" class="inputbutton" id="bnt_checked">
	            	<input id="txt_channelId" type="hidden" value="${picBean.channelId }"/>
            		<input id="txt_commonModelId" type="hidden" value="${picBean.commonModelId }"/>
	            </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>图片名称 ：&nbsp;</strong></td>
               <td>
                   <table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_title" type="text" style="width:450px;" value="${picBean.title }"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_titleTip" style="width:280px"></div></td>
	                </tr>
	            	</table>
               </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>关键字 ：</strong></td>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_keyword" type="text" style="width:350px;" value="${picBean.keyword }"/>
		                </td>
		                <td><div id="txt_keywordTip" style="width:280px"></div></td>
	                </tr>
	            </table>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>作者 ：</strong></td>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_author" type="text" style="width:350px;" value="${picBean.picGalleryAuthor }"/>
		                </td>
		                <td><div id="txt_authorTip" style="width:280px"></div></td>
	                </tr>
	            </table>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>图片来源 ：</strong></td>
                <td>
                   	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_copyFrom" type="text" style="width:350px;" value="${picBean.copyFrom }"/>
		                </td>
		                <td><div id="txt_copyFromTip" style="width:280px"></div></td>
	                </tr>
	            	</table>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>图片简介 ：&nbsp;</strong></td>
               <td>
               	<script id="editor" type="text/plain">${picBean.picGalleryIntro }</script>
               </td>
            </tr>
              <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>图片地址 ：</strong></td>
                <td>
                    <table border="0" cellpadding="1" cellspacing="1">
                        <tr>
                            <td>
                                <select size="4" id="select_picUrl" class="inputtext" style="height:165px;width:400px;">
									<c:if test="${picBean.list != null && fn:length(picBean.list)>0}">
									<c:forEach var="list" items="${picBean.list}" varStatus="status">
										<option value="opt_${status.count}">${list.picUrl}</option>
									</c:forEach>
									</c:if>
                                </select>
                            </td>
                            <td>
                                <input id="bnt_showCheckImgUrl" type="button" class="inputbutton" value="从已上传的文件中选择" /><br />
                                <input id="bnt_upload" type="button" value="上传文件..." class="inputbutton"/><br />
                                <input id="bnt_addExternalUrl" type="button" value="添加外部地址" class="inputbutton" /><br />
                                <input id="bnt_showPreviewImg" type="button" value="预览图片" class="inputbutton" /><br />
                                <input id="bnt_removeUrl" type="button" value="删除当前文件" class="inputbutton" /><br />
                                <input id="bnt_moveNext" type="button" value="向下移" class="inputbutton" /><br />
                                <input id="bnt_movePrev" type="button" value="向上移" class="inputbutton" /><br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="tdbg">
                 <td class="tdbgleft" style="width:200px;" align="right">
                    <strong>封面图片 ：&nbsp;</strong></td>
                <td>
                    <input id="txt_defaultPicUrl" class="input_public" style="width:300px;" value="${picBean.defaultPicUrl }"/>&nbsp;<input id="bnt_selectImg" type="button" class="inputbutton" value="从已上传的文件中选择" />&nbsp;<input id="bnt_review" type="button" value=" 预览 " class="inputbutton" onclick="showPreviewImg();"/><br />
                 	<iframe marginheight="0" marginwidth="0" scrolling="no" src="<%=request.getContextPath()%>/security/loadDefaultImg.html?iframe=OpendialogWindow_AddPic&callback=callbackDefaultPic" width="100%" height="58px" frameborder="0"></iframe>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>状态 ：&nbsp;</strong></td>
               <td>
                    <input type="radio" class="valignMiddle" id="rd_Status_1" name="Site_Status" <c:if test="${picBean.status==-1 }">checked="checked"</c:if>/><span class="valignMiddle">草稿</span>
                	<input type="radio" class="valignMiddle" id="rd_Status_2" name="Site_Status" <c:if test="${picBean.status==0 }">checked="checked"</c:if>/><span class="valignMiddle">待审核</span>
                	<input type="radio" class="valignMiddle" id="rd_Status_3" name="Site_Status" <c:if test="${picBean.status==99 }">checked="checked"</c:if>/><span class="valignMiddle">终审通过</span>
            	</td>
            </tr>
       </table>
      <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel2">
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>总点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_totlaHits" value="${picBean.hits }" name="txt_TotlaHits"/>
		                </td>
		                <td><div id="txt_totlaHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>日点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_dayHits" value="${picBean.dayHits }" name="txt_DayHits"/>
		                </td>
		                <td><div id="txt_dayHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>周点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_weekHits" value="${picBean.weekHits }" name="txt_WeekHits"/>
		                </td>
		                <td><div id="txt_weekHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>月点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_monthHits" value="${picBean.monthHits }" name="txt_MonthHits"/>
		                </td>
		                <td><div id="txt_monthHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>更新时间 ：&nbsp;</strong></td>
	            <td>
	                <input type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" id="txt_updataTime" value="${picBean.updateTime }" />
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>推荐级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_eliteLevel" value="${picBean.eliteLevel }" name="txt_EliteLevel"/>
		                </td>
		                <td><div id="txt_eliteLevelTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>优先级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_priority" value="${picBean.priority }" name="txt_Priority">
		                </td>
		                <td><div id="txt_priorityTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
     	</table>
        <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bt_update" value="修改">&nbsp;
	                <input type="button" class="inputbutton" id="bnt_back" value="关闭" >
	             </td>
	        </tr>
        </table>
        </c:otherwise>
        </c:choose>
    </form>
	</body>
</html>