<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>容器栏目详情</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript">
			$(document).ready(function(){
				var _purviewType = '${channel.purviewType}';
				if(_purviewType!=''&&_purviewType!='0'){
					var _id = '${channel.accessRight}';
					$.getJSON('<%=request.getContextPath()%>/security/getUserGroupByIds.html?id='+_id,function(data){
						if(data.code=='200'){
							$('#td_accessRight').html(data.content);
						}
						else{
							$('#td_accessRight').html('所有用户（不限制任何对该栏目的访问）');
						}
					});
				}
				else{
					$('#td_accessRight').html('所有用户（不限制任何对该栏目的访问）');
				}
			});
		</script>
	</head>
	<body class="mainContent">
	    <form id="channelForm" name="companyForm" action="#">
	    <table border="0" cellpadding="0" cellspacing="0" width="162px">
	        <tr align="center">
	            <td id="td_tab1" class="titlemouseover">
	                基本信息</td>
	            <td style="width:2px;"></td>
	            <td id="td_tab2" class="tabtitle">
	                访问权限</td>
	        </tr>
        </table>
        <table id="tb_Pannel1" border="0" cellpadding="2" cellspacing="1" class="tableWrap dispalyTable" width="100%">
         <tr class="tdbg">
            <td class="tdbgleft" align="right">
                <strong>栏目ID ：&nbsp;</strong>
            </td>
            <td>
            	${channel.channelId}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>所属栏目：&nbsp;</strong></td>
            <td>
            	<c:choose>
            		<c:when test="${empty parent}">
            		无
            		</c:when>
            		<c:otherwise>
            		${parent.channelName}
            		</c:otherwise>
            	</c:choose>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right">
                <strong>栏目名称：&nbsp;</strong>
            </td>
            <td>
            ${channel.channelName}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right">
                <strong>栏目别称 ：&nbsp;</strong>
            </td>
            <td>
            	${channel.nickName}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right">
                <strong>栏目顺序：&nbsp;</strong>
            </td>
            <td>
            ${channel.sort}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right">
                <strong>打开方式：&nbsp;</strong></td>
            <td>
            <c:choose>
	           	<c:when test="channel.openType==0">
	           		原窗口打开
	           	</c:when>
	           	<c:otherwise>
	           		新窗口打开
	           	</c:otherwise>
           	</c:choose>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right">
                <strong>外部链接地址：&nbsp;</strong>
            </td>
            <td>${channel.channelUrl}</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right">
                <strong>外部链接图片地址：&nbsp;</strong>
            </td>
            <td>
            ${channel.iconUrl}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right">
                <strong>栏目提示：&nbsp;</strong>
            </td>
            <td>
            ${channel.tips}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right">
                <strong>创建者 ：&nbsp;</strong>
            </td>
            <td>
            	${channel.createUser}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right">
                <strong>创建时间 ：&nbsp;</strong>
            </td>
            <td>
            	${channel.createTime}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right">
                <strong>最后修改人 ：&nbsp;</strong>
            </td>
            <td>
            	${channel.lastEditUser}
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right">
                <strong>最后修改时间 ：&nbsp;</strong>
            </td>
            <td>
            	${channel.lastEditTime}
            </td>
        </tr>
      </table>
	    <table id="tb_Pannel2" border="0" cellpadding="2" cellspacing="1" class="tableWrap diplayNone" width="100%">
	        <tr class="tdbg">
	            <td class="tdbgleft" style="width:200px;" align="right"> <strong>阅读权限：</strong></td>
	            <td id="td_accessRight"></td>
	        </tr>
	    </table>
	    <table border="0" cellpadding="2" cellspacing="1" width="100%">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bt_Close" value="关闭" >
	            </td>
	        </tr>
	    </table>
        </form>
<script type="text/javascript">
$(document).ready(function(){
	pageTab({'td_tab1':'tb_Pannel1','td_tab2':'tb_Pannel2'});
	beautifyBottomButton(['bt_Close']);
	$('#bt_Close').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('0');
	});
});
</script>
	</body>
</html>