<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>留言新增修改页面</title>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
	</head>
	<body class="mainContent">
         <table border="0" cellpadding="0" cellspacing="0" width="243px">
            <tr align="center">
                <td id="td_tab1" class="titlemouseover">
                    基本信息</td>
                <td width="2px"></td>
                <td id="td_tab2" class="tabtitle">
                    属性选项</td>
                <td width="2px"></td>
                <td id="td_tab3" class="tabtitle">
                    回复内容</td>
            </tr>
        </table>
		<table id="tb_Pannel1" border="0" cellpadding="2" cellspacing="1" class="tableWrap dispalyTable" width="100%" style="border-bottom:0px;">
            <tr class="tdbg">
                <td class="tdbgleft" style="width:200px;" align="right">
                <strong>所属节点 ：&nbsp;</strong></td>
	            <td>
	            	${guestBookBean.channelName }
	            </td>
            </tr>
            <tr class="tdbg">
               <td class="tdbgleft" style="width:200px;" align="right">
                <strong>标题 ：&nbsp;</strong></td>
	            <td>
	            	${guestBookBean.title }
	            </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right">
                    <strong>用户名 ：</strong>
                </td>
                <td>
                	${guestBookBean.userName }
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>Email地址 ：</strong></td>
                <td>
                	${guestBookBean.guestEmail }
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>手机号码 ：</strong></td>
                <td>
                	${guestBookBean.phoneNumber }
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>QQ号码 ：</strong></td>
                <td>
                    ${guestBookBean.guestOicq }
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>留言内容 ：&nbsp;</strong></td>
               <td>
               	${guestBookBean.guestContent }
               </td>
            </tr>
            <tr class="tdbg">
                <td  class="tdbgleft" align="right"><strong>是否隐藏 ：&nbsp;</strong></td>
                <td>
                	<c:choose>
                  	<c:when test="${guestBookBean.guestIsPrivate}">是</c:when>
                  	<c:otherwise>否</c:otherwise>
                  	</c:choose>
                </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>状态 ：</strong></td>
                <td>
               		<c:choose>
                  	<c:when test="${guestBookBean.status==-3}"><span style="color:#FF0000">已删除</span></c:when>
                  	<c:when test="${guestBookBean.status==-1}"><span style="color:#2828FF">草稿</span></c:when>
                  	<c:when test="${guestBookBean.status==0}"><span style="color:#000000">待审核</span></c:when>
                  	<c:otherwise><span style="color:#00A600">已审核</span></c:otherwise>
                  	</c:choose>
             	</td>
            </tr>
       </table>
	   <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel2">
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>总点击数 ：&nbsp;</strong></td>
	            <td>
	            	${guestBookBean.hits }
	            </td>
	        </tr>
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>日点击数 ：&nbsp;</strong></td>
	            <td>
	            	${guestBookBean.dayHits }
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>周点击数 ：&nbsp;</strong></td>
	            <td>
	            	${guestBookBean.weekHits }
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>月点击数 ：&nbsp;</strong></td>
	            <td>
	            	${guestBookBean.monthHits }
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>更新时间 ：&nbsp;</strong></td>
	            <td>
	                ${guestBookBean.updateTime }
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>推荐级 ：&nbsp;</strong></td>
	            <td>
	            	${guestBookBean.eliteLevel }
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>优先级 ：&nbsp;</strong></td>
	            <td>
	            	${guestBookBean.priority }
	            </td>
	        </tr>
     	</table>
     	<table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel3">
			<c:choose>
	    	<c:when test="${replyCount>0}">
			<c:forEach var="list" items="${guestBookBean.list}">
			 <tr class="tdbg">
			 	<td style="width:220px;" valign="top">
					<table cellpadding="1" cellspacing="2" class="tableWrap" style="border:0px;width:100%">
						<tr class="tdbg">
							<td class="tdbgleft"><strong>回复ID：</strong></td>
							<td>${list.replyId }</td>
						</tr>
						<tr class="tdbg">
							<td class="tdbgleft"><strong>回复人：</strong></td>
							<td>${list.replyName }</td>
						</tr>
						<tr class="tdbg">
							<td class="tdbgleft"><strong>回复时间：</strong></td>
							<td>${list.replyTime }</td>
						</tr>
						<tr class="tdbg">
							<td class="tdbgleft"><strong>创建时间：</strong></td>
							<td>${list.createTime }</td>
						</tr>
					</table>
			 	</td>
			 	<td>
			 		${list.replyContent }
			 	</td>
			 	<td style="width:120px;" align="center">
			 		<a name="bnt_delReply" value="${list.replyId }">删除回复</a>&nbsp;<a name="bnt_updateReply" value="${list.replyId }">修改回复</a>
			 	</td>
			 </tr>
			</c:forEach>
			</c:when>
            <c:otherwise>
            	<tr class="tdbg" style="height:80px;">
					<td align="center">
			             <strong class="valignMiddle">没有任何数据！</strong>
			        </td>
            	</tr>
			</c:otherwise>
         	</c:choose>
     	</table>
        <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	    <tr>
	       <td align="left">
	         <input type="button" class="inputbutton" id="bt_Update" value="修改/审核">&nbsp;
	         <c:if test="${guestBookBean.status!=-3}">
	         <input type="button" class="inputbutton" id="bt_Del" value="删除" >&nbsp;
			 </c:if>
	         <c:choose>
	         <c:when test="${guestBookBean.status==-1||guestBookBean.status==0}"><input type="button" class="inputbutton" id="bt_Passed" value="审核通过" >&nbsp;</c:when>
	         <c:when test="${guestBookBean.status==-3}"><input type="button" class="inputbutton" id="bt_Restore" value="还原" >&nbsp;</c:when>
             <c:otherwise><input type="button" class="inputbutton" id="bt_CancelPassed" value="取消审核" >&nbsp;</c:otherwise>
             </c:choose>
	         <input type="button" class="inputbutton" id="bt_Back" value="关闭" >
	       </td>
	    </tr>
     </table>
<script type="text/javascript">
var _dialogGuestBookReply;
$(document).ready(function(){
	var id = '${guestBookBean.commonModelId}';
	pageTab({'td_tab1':'tb_Pannel1','td_tab2':'tb_Pannel2','td_tab3':'tb_Pannel3'});
	beautifyBottomButton(['bt_Update','bt_Del','bt_Passed','bt_CancelPassed','bt_Back']);
	$('#bt_Del').click(function(){
		updateStatus('-3');
	});
	$('#bt_Passed').click(function(){
		updateStatus('99');
	});
	$('#bt_CancelPassed').click(function(){
		updateStatus('0');
	});
	$('#bt_Restore').click(function(){
		updateStatus('0');
	});
	$('#bt_Back').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('1');
	});
	function updateStatus(status){
		$.post(contextPath+'/security/updateStatus.html',{status:status,ids:id},function(data){
			if(data.code=='200'){
				alert(data.content);
				window.location.reload();
			}
			else{
				alert(data.content);
			}
		},'json');
	};
	$('a[name="bnt_delReply"]').click(function(){
		var isOK = confirm('您确定要删除这条记录吗？');
		if(isOK){
			var _rId = $(this).attr('value');
			$.post(contextPath+'/security/delGuestBookReply.html',{id:_rId},function(data){
				if(data.code=='200'){
					alert(data.content);
					window.location.reload();
				}
				else{
					alert(data.content);
				}
			},'json');
		}
	});
	$('#bt_Update').click(function(){
		window.top.frames['iframeMenuContent'].toUpdatePage(id);
	});
	$('a[name="bnt_updateReply"]').click(function(){
		var _rid = $(this).attr('value');
		_dialogGuestBookReply = art.dialog.open(contextPath+'/security/loadUpdateGuestBookReply.html?iframe=OpendialogWindow_GuestBookDetail&id='+_rid,{height:'450px',width:'850px',title:'新增/修改留言回复',padding: 0,lock:true},false);
	});
});
var closeDialog = function(){
	_dialogGuestBookReply.close();
	window.location.reload();
};
</script>
	</body>
</html>