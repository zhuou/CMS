<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>图片上传结果</title>
	</head>
	<body class="mainContent">
		<c:choose>
	   <c:when test="${bean.code ne '200'}">
		<table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
	        <tr class="tdbg">
	        	<td><span style="color:#FF0000;">${bean.content}</span></td>
	        </tr>
        </table>
         </c:when>
        <c:otherwise>
		<script type="text/javascript">
			var _contentJson = '${bean.content}';
			window.top.frames['${iframe}'].insertOption(_contentJson);
    	</script>
        </c:otherwise>
        </c:choose>
	</body>
</html>