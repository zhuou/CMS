<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>站点前台URL信息详情</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
	</head>
	<body class="mainContent">
        <table id="tb_Pannel1" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        	<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>ID ：&nbsp;</strong></td>
				<td>
					<strong>${bean.pageUrlId}</strong>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>所属站点 ：&nbsp;</strong></td>
				<td>
					<strong>${bean.siteName}</strong>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>页面url类型 ：&nbsp;</strong></td>
				<td>
					<c:choose>
	            		<c:when test="${bean.urlType==1}">
						首页(/index.html)
	            		</c:when>
	            		<c:when test="${bean.urlType==2}">
	            		栏目页(/channel/${bean.nickName}.html或/${bean.nickName}/index.html)
	            		</c:when>
	            		<c:when test="${bean.urlType==3}">
	            		列表页(/${bean.nickName}/list_1.html)
	            		</c:when>
	            		<c:when test="${bean.urlType==4}">
	            		类型列表页(/${bean.nickName}/type_new_list_1.html
	            		</c:when>
	            		<c:when test="${bean.urlType==5}">
	            		栏目列表页(/${bean.nickName}/girl/list_1.html)
	            		</c:when>
	            		<c:when test="${bean.urlType==6}">
						栏目类型列表页(/${bean.nickName}/girl/type_new_list_1.html)
	            		</c:when>
	            		<c:when test="${bean.urlType==7}">
	            		详情页(/${bean.nickName}/100/detail.html)
	            		</c:when>
	            		<c:when test="${bean.urlType==8}">
	            		用户自定义页(/${bean.nickName}.html)
	            		</c:when>
	            	</c:choose>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>Url别称 ：&nbsp;</strong></td>
				<td>
					${bean.nickName}
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>JSP页面路径 ：&nbsp;</strong></td>
				<td>
					${bean.jspPath}
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>描述：&nbsp;</strong></td>
				<td>
					${bean.intro}
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>创建时间：&nbsp;</strong></td>
				<td>
					<fmt:formatDate value="${bean.createTime}" type="date" pattern="yyyy-MM-dd HH:mm"/>
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>创建者：&nbsp;</strong></td>
				<td>
					${bean.createUser}
				</td>
			</tr>
			<tr class="tdbg">
				<td class="tdbgleft" style="width:150px;" align="right"><strong>修改时间：&nbsp;</strong></td>
				<td>
				<fmt:formatDate value="${bean.updateTime}" type="date" pattern="yyyy-MM-dd HH:mm"/>
				</td>
			</tr>
        </table>
      <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	        		<input id="closeReceptionPageUrl" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
	             </td>
	        </tr>
        </table>
	</body>
<script type="text/javascript">
$(document).ready(function(){
	$('#closeReceptionPageUrl').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('0');
	});
});
</script>
</html>