<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>会员列表</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/cms/js/memberList.js"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
	</head>
	<body class="mainContent">
		<span class="localSpan">后台管理<span> &gt;&gt; </span><span>内容管理</span><span>&gt;&gt; </span><span>用户列表</span></span>
	    <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
	        <tr class="topbottom">
	            <td colspan="12">
	            	<input id="member_Add" class="topinput topinput_add" type="button" value="新建"/>
	            	<input id="member_Update" class="topinput topinput_edit" type="button" value="修改"/>
	            	<input id="member_Del" class="topinput topinput_del" type="button" value="删除选定选"/>
	            	<input id="member_Detail" class="topinput topinput_detail" type="button" value="查看详情"/>
	            </td>
	        </tr>
	        <c:choose>
		    	<c:when test="${pageBean.totalRecordNum>0}">
			        <tr class="title">
			        	<th style="width:4%;"><input id="cb_CheckedAll" type="checkbox"/></th>
			        	<th style="width:6%;">会员D</th>
			        	<th style="width:10%;">会员名</th>
			        	<th style="width:10%;">会员组</th>
			        	<th style="width:10%;">会员Email</th>
			        	<th style="width:9%;">真是姓名</th>
			        	<th style="width:8%;">移动电话</th>
			        	<th style="width:8%;">登录次数</th>
			        	<th style="width:9%;">最后登录时间</th>
			        	<th style="width:9%;">最后登录IP</th>
			            <th style="width:8%;">账号状态</th>
			            <th style="width:9%;">注册时间</th>
			        </tr>
			        <tbody id="tbody_DataList">
			        <c:forEach var="list" items="${pageBean.dataList}">
			        <tr class="tdbg">
			        	<td align="center"><input type="checkbox" value="${list.userId}"/></td>
			            <td align="center">${list.userId}</td>
			            <td align="center"><a name="detail_${list.userId}" href="javascript:void(0);">${list.userName}</a></td>
			            <td align="center">${list.groupName}</td>
			            <td align="center">${list.email}</td>
			            <td align="center">${list.trueName}</td>
			            <td align="center">${list.mobile}</td>
			            <td align="center">${list.loginTimes}</td>
			            <td align="center"><c:if test="${!empty list.lastLoginTime}"><fmt:formatDate value="${list.lastLoginTime}" type="date" pattern="yyyy-MM-dd HH:mm"/></c:if></td>
			            <td align="center">${list.lastLoginIP}</td>
			            <td align="center">
							<c:if test="${list.status==0}"><span style="color:#00A600">正常</span></c:if>
							<c:if test="${list.status==1}"><span style="color:#FF0000">锁定</span></c:if>
							<c:if test="${list.status==2}"><span style="color:#FF0000">未通过邮件验证</span></c:if>
							<c:if test="${list.status==4}"><span style="color:#FF0000">未通过管理员认证</span></c:if>
							<c:if test="${list.status==8}"><span style="color:#FF0000">未通过手机验证</span></c:if>
			            </td>
			            <td align="center"><fmt:formatDate value="${list.regTime}" type="date" pattern="yyyy-MM-dd HH:mm"/></td>
			        </tr>
			        </c:forEach>
			        </tbody>
			        <tr align="right" class="tdbg" style="height:28px;">
			        	<td colspan="12">
			        		<c:import url="pageCurrent.jsp">
			        			<c:param name="totalRecordNum" value="${pageBean.totalRecordNum}"></c:param>
			        			<c:param name="pageRecordNum" value="${pageBean.pageRecordNum}"></c:param>
					        </c:import>
			        	</td>
			        </tr>
		        </c:when>
	     		<c:otherwise>
		     		<tr class="tdbg" style="height:80px;">
			             <td align="center">
			                  <strong class="valignMiddle">没有任何数据！</strong>
			             </td>
		            </tr>
	     		</c:otherwise>
      		</c:choose>
     </table>
	</body>
</html>