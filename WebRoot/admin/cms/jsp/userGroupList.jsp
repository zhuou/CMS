<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>用户组列表</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/cms/js/userGroupList.js"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
	</head>
	<body class="mainContent">
		<span class="localSpan">后台管理<span> &gt;&gt; </span><span> 内容管理 </span><span>&gt;&gt; </span><span>用户组列表 </span></span>
	    <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
	        <tr class="topbottom">
	            <td colspan="7">
	            	<input id="userGroup_Add" class="topinput topinput_add" type="button" value="新建"/>
	            	<input id="userGroup_Update" class="topinput topinput_edit" type="button" value="修改"/>
	            	<input id="userGroup_Del" class="topinput topinput_del" type="button" value="删除选定选"/>
	            	<input id="userGroup_Detail" class="topinput topinput_detail" type="button" value="查看详情"/>
	            </td>
	        </tr>
	        <c:choose>
		    	<c:when test="${pageBean.totalRecordNum>0}">
			        <tr class="title">
			        	<th style="width:4%;"><input id="cb_CheckedAll" type="checkbox"/></th>
			        	<th style="width:8%;">用户组ID</th>
			            <th style="width:16%;">用户组名称</th>
			            <th style="width:16%;">所属公司</th>
			            <th style="width:28%;">描述</th>
			            <th style="width:10%;">创建时间</th>
			            <th style="width:10%;">创建人</th>
			        </tr>
			        <tbody id="tbody_DataList">
			        <c:forEach var="list" items="${pageBean.dataList}">
			        <tr class="tdbg">
			        	<td align="center"><input type="checkbox" value="${list.groupId}"/></td>
			            <td align="center">${list.groupId}</td>
			            <td align="center"><a onclick="userGroupDetail(${list.groupId});">${list.groupName}</a></td>
			            <td align="center">${list.companyName}</td>
			            <td align="center">${list.description}</td>
			            <td align="center">${list.createTime}</td>
			            <td align="center">${list.createUser}</td>
			        </tr>
			        </c:forEach>
			        </tbody>
			        <tr align="right" class="tdbg" style="height:28px;">
			        	<td colspan="7">
			        		<c:import url="pageCurrent.jsp">
			        			<c:param name="totalRecordNum" value="${pageBean.totalRecordNum}"></c:param>
			        			<c:param name="pageRecordNum" value="${pageBean.pageRecordNum}"></c:param>
					        </c:import>
			        	</td>
			        </tr>
		        </c:when>
	     		<c:otherwise>
		     		<tr class="tdbg" style="height:80px;">
			             <td align="center">
			                  <strong class="valignMiddle">没有任何数据！</strong>
			             </td>
		            </tr>
	     		</c:otherwise>
      		</c:choose>
     </table>
	</body>
</html>