<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>下载新增修改页面</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
		var reqMethod = '${method}';
		window.UEDITOR_HOME_URL = '<%=request.getContextPath()%>';
    	</script>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/date/WdatePicker.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/editor/ueditor.all.min.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/editor/ueditor.config2.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/cms/js/download.js" type="text/javascript"></script>
	</head>
	<body class="mainContent">
	<form id="downloadForm" name="downloadForm" action="#">
	<c:choose>
	   <c:when test="${method eq 'add'}">
     <table border="0" cellpadding="0" cellspacing="0" width="243px">
        <tr align="center">
            <td id="td_tab1" class="titlemouseover">
                基本信息</td>
            <td style="width:2px"></td>
            <td id="td_tab3" class="tabtitle">
                扩展信息</td>
            <td style="width:2px"></td>
            <td id="td_tab2" class="tabtitle">
                属性选项</td>
        </tr>
    </table>
	 <table id="tb_Pannel1" border="0" cellpadding="2" cellspacing="1" class="tableWrap dispalyTable" width="100%" style="border-bottom:0px;">
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>所属节点 ：&nbsp;</strong></td>
            <td>
            	<input type="text" style="width:250px;" class="inputtext" disabled="disabled" id="txt_CheckChannelName" value="${defaultChannel.channelName }"><input type="button" value="选择" class="inputbutton" id="bnt_checked">
            	<input id="txt_channelId" type="hidden" value="${defaultChannel.channelId }"/>
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件/文件名称 ：&nbsp;</strong></td>
            <td>
               <table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_title" type="text" style="width:450px;"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_titleTip" style="width:280px"></div></td>
	                </tr>
	            </table>
	        </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>关键字 ：&nbsp;</strong></td>
            <td>
               <table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_keyword" type="text" style="width:350px;"/>
		                </td>
		                <td><div id="txt_keywordTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>作者/开发商 ：&nbsp;</strong></td>
            <td>
               <table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_author" type="text" style="width:350px;"/>
		                </td>
		                <td><div id="txt_authorTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件/文件简介 ：&nbsp;</strong></td>
            <td>
				<script id="editor" type="text/plain"></script>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>封面图片 ：&nbsp;</strong></td>
            <td>
                 <input id="txt_defaultPicUrl" class="input_public" style="width:300px;"/>&nbsp;<input id="bnt_selectImg" name="bnt_selectImg" type="button" class="inputbutton" value="从已上传的文件中选择" callback="callbackDefaultPic"/>&nbsp;<input id="bnt_review" type="button" value=" 预览 " class="inputbutton"/><br />
                 <iframe marginheight="0" marginwidth="0" scrolling="no" src="<%=request.getContextPath()%>/security/loadDefaultImg.html?callback=callbackDefaultPic&iframe=OpendialogWindow_DownloadDetail" width="100%" height="58px" frameborder="0"></iframe>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件/文件下载地址 ：&nbsp;</strong></td>
            <td>
             <table border="0" cellpadding="1" cellspacing="1">
                <tr>
                    <td>
                        <select size="4" id="select_downloadUrl" style="height:100px;width:415px;">
                        </select>
                    </td>
                    <td>
                        <input id="bnt_addExternalUrl" type="button" value="添加外部地址" class="inputbutton"/><br />
                        <input id="bnt_remove" type="button" value="删除当前文件" class="inputbutton"/><br />
                        <input id="bnt_nextMove" type="button" value="向下移" class="inputbutton"/><br />
                        <input id="bnt_prevMove" type="button" value="向上移" class="inputbutton"/><br />
                    </td>
                </tr>
              </table>
               <table cellpadding="1" cellspacing="1" border="0">
                	<tr>
                		<td colspan="2">
                		<input id="bnt_selectFile" type="button" value="从已上传的文件中选择" class="thickbox inputbutton" value="从已上传的文件中选择" alt="checkFile.aspx?TB_iframe=true&height=400&width=500" title="从已上传的文件中选择"/>&nbsp;&nbsp;
                   		<input id="bnt_uploadFile" type="button" value="上传文件..." class="inputbutton" onclick="showUploadFilePage(4);"/>
                   		</td>
                	</tr>
	               	<tr>
	                	<td>
	                    <strong>文件大小 ：</strong><input id="txt_fileSize" type="text"/>
		                </td>
		                <td><div id="txt_fileSizeTip" style="width:280px"></div></td>
	                </tr>
		      </table>
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>解压密码 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_decompressPassword" type="text" style="width:151px;"/>
		                </td>
		                <td><div id="txt_decompressPasswordTip" style="width:280px"></div></td>
	                </tr>
	            </table>
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>状态 ：&nbsp;</strong></td>
            <td>
                <input type="radio" class="valignMiddle" id="rd_Status_1" name="Site_Status" value="rd_Status_1"><span class="valignMiddle">草稿</span>
                <input type="radio" class="valignMiddle" id="rd_Status_2" name="Site_Status" value="rd_Status_2"><span class="valignMiddle">待审核</span>
                <input type="radio" class="valignMiddle" checked="checked" id="rd_Status_3" name="Site_Status" value="rd_Status_3"><span class="valignMiddle">终审通过</span>
            </td>
        </tr>
    </table>
    <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel3">
		 <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件版本 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_fileVersion" type="text" style="width:350px;"/>
		                </td>
		                <td><div id="txt_fileVersionTip" style="width:280px"></div></td>
	                </tr>
	            </table>
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件来源 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_copyFrom" type="text" style="width:350px;"/>
		                </td>
		                <td><div id="txt_copyFromTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件演示地址 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_demoUrl" type="text" style="width:350px;"/>
		                </td>
		                <td><div id="txt_demoUrlTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
       <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件平台 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_operatingSystem" type="text" style="width:350px;"/>
		                </td>
		                <td><div id="txt_operatingSystemTip" style="width:280px"></div></td>
	                </tr>
	                <tr>
	                	<td colspan="2">
	                	<div>平台选择：
	                	<c:forEach var="list" items="${operatingSystem}">
	                	<a style="font-weight:bold;" onclick="checkSystemName(this);">${list.fieldValue }</a>|
	                	</c:forEach>
	                	</div>
	                	</td>
	                </tr>
	            </table>
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件类别 ：&nbsp;</strong></td>
            <td>
            	<select id="select_fileType">
            		<c:forEach var="list" items="${fileType}">
            		<option value="${list.fieldID }">${list.fieldValue }</option>
            		</c:forEach>
            	</select>
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件语言 ：&nbsp;</strong></td>
            <td>
            	<select id="select_fileLanguage">
            		<c:forEach var="list" items="${fileLanguage}">
            		<option value="${list.fieldID }">${list.fieldValue }</option>
            		</c:forEach>
            	</select>
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>授权方式 ：&nbsp;</strong></td>
            <td>
            	<select id="select_copyrightType">
            		<c:forEach var="list" items="${copyrightType}">
            		<option value="${list.fieldID }">${list.fieldValue }</option>
            		</c:forEach>
            	</select>
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件注册地址 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_regUrl" type="text" style="width:350px;"/>
		                </td>
		                <td><div id="txt_regUrlTip" style="width:280px"></div></td>
	                </tr>
	            </table>
             </td>
        </tr>
    </table>
     <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel2">
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>总点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_totlaHits" value="0" name="txt_TotlaHits"/>
		                </td>
		                <td><div id="txt_totlaHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>日点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_dayHits" value="0" name="txt_DayHits"/>
		                </td>
		                <td><div id="txt_dayHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>周点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_weekHits" value="0" name="txt_WeekHits"/>
		                </td>
		                <td><div id="txt_weekHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>月点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_monthHits" value="0" name="txt_MonthHits"/>
		                </td>
		                <td><div id="txt_monthHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>更新时间 ：&nbsp;</strong></td>
	            <td>
	                <input type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" id="txt_updataTime" value="${date }" />
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>推荐级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_eliteLevel" value="0" name="txt_EliteLevel"/>
		                </td>
		                <td><div id="txt_eliteLevelTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>优先级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_priority" value="0" name="txt_Priority">
		                </td>
		                <td><div id="txt_priorityTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
     	</table>
        <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bnt_save" value="保存">&nbsp;
	                <input type="button" class="inputbutton" id="bnt_back" value="关闭" >
	             </td>
	        </tr>
        </table>
       </c:when>
      <c:otherwise>
     <table border="0" cellpadding="0" cellspacing="0" width="243px">
        <tr align="center">
            <td id="td_tab1" class="titlemouseover">
                基本信息</td>
            <td style="width:2px"></td>
            <td id="td_tab3" class="tabtitle">
                扩展信息</td>
            <td style="width:2px"></td>
            <td id="td_tab2" class="tabtitle">
                属性选项</td>
        </tr>
    </table>
	 <table id="tb_Pannel1" border="0" cellpadding="2" cellspacing="1" class="tableWrap dispalyTable" width="100%" style="border-bottom:0px;">
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>所属节点 ：&nbsp;</strong></td>
            <td>
            	<input type="text" style="width:250px;" class="inputtext" disabled="disabled" id="txt_CheckChannelName" value="${downloadBean.channelName }"><input type="button" value="选择" class="inputbutton" id="bnt_checked">
            	<input id="txt_channelId" type="hidden" value="${downloadBean.channelId }"/>
            	<input id="txt_commonModelId" type="hidden" value="${downloadBean.commonModelId }"/>
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件/文件名称 ：&nbsp;</strong></td>
            <td>
               <table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_title" type="text" style="width:450px;" value="${downloadBean.title }"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_titleTip" style="width:280px"></div></td>
	                </tr>
	            </table>
	        </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>关键字 ：&nbsp;</strong></td>
            <td>
               <table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_keyword" type="text" style="width:350px;" value="${downloadBean.keyword }"/>
		                </td>
		                <td><div id="txt_keywordTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>作者/开发商 ：&nbsp;</strong></td>
            <td>
               <table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_author" type="text" style="width:350px;" value="${downloadBean.author }"/>
		                </td>
		                <td><div id="txt_authorTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件/文件简介 ：&nbsp;</strong></td>
            <td>
				<script id="editor" type="text/plain">${downloadBean.fileIntro }</script>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>封面图片 ：&nbsp;</strong></td>
            <td>
                 <input id="txt_defaultPicUrl" class="inputtext" style="width:300px;" value="${downloadBean.defaultPicUrl }"/>&nbsp;<input id="bnt_selectImg" type="button" class="inputbutton" value="从已上传的文件中选择" />&nbsp;<input id="bnt_review" type="button" value=" 预览 " class="inputbutton" onclick="showPreviewImg();"/><br />
                 <iframe marginheight="0" marginwidth="0" scrolling="no" src="<%=request.getContextPath()%>/security/loadDefaultImg.html?callback=callbackDefaultPic" width="100%" height="58px" frameborder="0"></iframe>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件/文件下载地址 ：&nbsp;</strong></td>
            <td>
             <table border="0" cellpadding="1" cellspacing="1">
                <tr>
                    <td>
                        <select size="4" id="select_downloadUrl" style="height:100px;width:415px;">
							<c:forEach var="list" items="${downloadBean.downloadUrlList}">
								<option>${list}</option>
							</c:forEach>
                        </select>
                    </td>
                    <td>
                        <input id="bnt_addExternalUrl" type="button" value="添加外部地址" class="inputbutton"/><br />
                        <input id="bnt_remove" type="button" value="删除当前文件" class="inputbutton"/><br />
                        <input id="bnt_nextMove" type="button" value="向下移" class="inputbutton"/><br />
                        <input id="bnt_prevMove" type="button" value="向上移" class="inputbutton"/><br />
                    </td>
                </tr>
              </table>
               <table cellpadding="1" cellspacing="1" border="0">
                	<tr>
                		<td colspan="2">
                		<input id="bnt_selectFile" type="button" value="从已上传的文件中选择" class="thickbox inputbutton" value="从已上传的文件中选择" alt="checkFile.aspx?TB_iframe=true&height=400&width=500" title="从已上传的文件中选择"/>&nbsp;&nbsp;
                   		<input id="bnt_uploadFile" type="button" value="上传文件..." class="inputbutton" onclick="showUploadFilePage(4);"/>
                   		</td>
                	</tr>
	               	<tr>
	                	<td>
	                    <strong>文件大小 ：</strong><input id="txt_fileSize" type="text" value="${downloadBean.fileSize }"/>
		                </td>
		                <td><div id="txt_fileSizeTip" style="width:280px"></div></td>
	                </tr>
		      </table>
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>解压密码 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_decompressPassword" type="text" style="width:151px;" value="${downloadBean.decompressPassword }"/>
		                </td>
		                <td><div id="txt_decompressPasswordTip" style="width:280px"></div></td>
	                </tr>
	            </table>
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>状态 ：&nbsp;</strong></td>
             <td>
                <input type="radio" class="valignMiddle" id="rd_Status_1" name="Site_Status" <c:if test="${downloadBean.status==-1 }">checked="checked"</c:if>><span class="valignMiddle">草稿</span>
                <input type="radio" class="valignMiddle" id="rd_Status_2" name="Site_Status" <c:if test="${downloadBean.status==0 }">checked="checked"</c:if>><span class="valignMiddle">待审核</span>
                <input type="radio" class="valignMiddle" id="rd_Status_3" name="Site_Status" <c:if test="${downloadBean.status==99 }">checked="checked"</c:if>><span class="valignMiddle">终审通过</span>
             </td>
        </tr>
    </table>
    <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel3">
		 <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件版本 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_fileVersion" type="text" style="width:350px;" value="${downloadBean.fileVersion }"/>
		                </td>
		                <td><div id="txt_fileVersionTip" style="width:280px"></div></td>
	                </tr>
	            </table>
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件来源 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_copyFrom" type="text" style="width:350px;" value="${downloadBean.copyFrom }"/>
		                </td>
		                <td><div id="txt_copyFromTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件演示地址 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_demoUrl" type="text" style="width:350px;" value="${downloadBean.demoUrl }"/>
		                </td>
		                <td><div id="txt_demoUrlTip" style="width:280px"></div></td>
	                </tr>
	            </table>
            </td>
        </tr>
       <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件平台 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_operatingSystem" type="text" style="width:350px;" value="${downloadBean.operatingSystem }"/>
		                </td>
		                <td><div id="txt_operatingSystemTip" style="width:280px"></div></td>
	                </tr>
	                <tr>
	                	<td colspan="2">
	                	<div>平台选择：
	                	<c:forEach var="list" items="${operatingSystem}">
	                	<a style="font-weight:bold;" onclick="checkSystemName(this);">${list.fieldValue }</a>|
	                	</c:forEach>
	                	</div>
	                	</td>
	                </tr>
	            </table>
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件类别 ：&nbsp;</strong></td>
            <td>
            	<select id="select_fileType">
            		<c:forEach var="list" items="${fileType}">
            		<option value="${list.fieldID }" <c:if test="${list.fieldValue eq downloadBean.fileType }">selected="selected"</c:if>>${list.fieldValue }</option>
            		</c:forEach>
            	</select>
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件语言 ：&nbsp;</strong></td>
            <td>
            	<select id="select_fileLanguage">
            		<c:forEach var="list" items="${fileLanguage}">
            		<option value="${list.fieldID }" <c:if test="${list.fieldValue eq downloadBean.fileLanguage }">selected="selected"</c:if>>${list.fieldValue }</option>
            		</c:forEach>
            	</select>
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>授权方式 ：&nbsp;</strong></td>
            <td>
            	<select id="select_copyrightType">
            		<c:forEach var="list" items="${copyrightType}">
            		<option value="${list.fieldID }" <c:if test="${list.fieldValue eq downloadBean.copyrightType }">selected="selected"</c:if>>${list.fieldValue }</option>
            		</c:forEach>
            	</select>
             </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>软件注册地址 ：&nbsp;</strong></td>
            <td>
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_regUrl" type="text" style="width:350px;" value="${downloadBean.regUrl }"/>
		                </td>
		                <td><div id="txt_regUrlTip" style="width:280px"></div></td>
	                </tr>
	            </table>
             </td>
        </tr>
    </table>
     <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel2">
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>总点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_totlaHits" value="${downloadBean.hits }"/>
		                </td>
		                <td><div id="txt_totlaHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>日点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_dayHits" value="${downloadBean.dayHits }"/>
		                </td>
		                <td><div id="txt_dayHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>周点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_weekHits" value="${downloadBean.weekHits }"/>
		                </td>
		                <td><div id="txt_weekHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>月点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_monthHits" value="${downloadBean.monthHits }"/>
		                </td>
		                <td><div id="txt_monthHitsTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>更新时间 ：&nbsp;</strong></td>
	            <td>
	                <input type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" id="txt_updataTime" value="${downloadBean.updateTime }" />
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>推荐级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_eliteLevel" value="${downloadBean.eliteLevel }"/>
		                </td>
		                <td><div id="txt_eliteLevelTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>优先级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_priority" value="${downloadBean.priority }">
		                </td>
		                <td><div id="txt_priorityTip" style="width:280px"></div></td>
	                </tr>
	                </table>
	            </td>
	        </tr>
     	</table>
        <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bnt_update" value="保存">&nbsp;
	                <input type="button" class="inputbutton" id="bnt_back" value="关闭" >
	             </td>
	        </tr>
        </table>
	  </c:otherwise>
     </c:choose>
    </form>
	</body>
</html>