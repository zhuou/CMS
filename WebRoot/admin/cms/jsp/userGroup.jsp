<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>用户组新增、修改</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/cms/js/userGroup.js"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	var reqMethod = '${method}';
    	</script>
	</head>
	<body>
		<form id="userGroupForm" name="userGroupForm" action="#">
		<c:choose>
	    <c:when test="${method eq 'add'}">
		<table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" style="border-bottom:0px;">
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>用户组名称：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><input id="txt_userGroupName" type="text" /></td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="txt_userGroupNameTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>用户组描述：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><textarea id="txt_description" style="height:100px;width:250px;" cols="25" rows="2"></textarea></td>
	            	</tr>
	            	<tr>
	            		<td><div id="txt_descriptionTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        </table>
        <table border="0" width="100%" cellpadding="2" cellspacing="1" class="tableWrap" style="border-top:0px;height:40px;background-color:#DEECF7;">
     	<tr class="height25">
        	<td colspan="2" align="center">
        		<input id="bnt_Save" type="button" class="inputbutton" value=" 保 存 "/>&nbsp;&nbsp;
        		<input id="bnt_Close" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
        	</td>
        </tr>
     </table>
     </c:when>
      <c:otherwise>
		<table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" style="border-bottom:0px;">
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>用户组名称：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td>
	            		<input id="txt_userGroupName" type="text" value="${userGroupBean.groupName }"/>
	            		<input id="txt_groupID" type="hidden" value="${userGroupBean.groupId }"/>
	            		</td>
	            		<td><span class="cautiontitleRed">*</span></td>
	            		<td><div id="txt_userGroupNameTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" width="105px" align="right"><strong>用户组描述：</strong></td>
            <td>
	            <table cellpadding="0" cellspacing="0" border="0">
	            	<tr>
	            		<td><textarea id="txt_description" style="height:100px;width:250px;" cols="25" rows="2">${userGroupBean.description }</textarea></td>
	            	</tr>
	            	<tr>
	            		<td><div id="txt_descriptionTip"></div></td>
	            	</tr>
	            </table>
            </td>
        </tr>
        </table>
        <table border="0" width="100%" cellpadding="2" cellspacing="1" class="tableWrap" style="border-top:0px;height:40px;background-color:#DEECF7;">
     	<tr class="height25">
        	<td colspan="2" align="center">
        		<input id="bnt_Update" type="button" class="inputbutton" value=" 修 改 "/>&nbsp;&nbsp;
        		<input id="bnt_Close" type="button" class="inputbutton" value=" 关 闭 "/>&nbsp;&nbsp;
        	</td>
        </tr>
     </table>
	  </c:otherwise>
     </c:choose>
    </form>
	</body>
</html>