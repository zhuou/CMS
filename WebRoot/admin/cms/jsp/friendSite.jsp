<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>添加修改友情链接</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/date/WdatePicker.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
    	<script type="text/javascript" src="<%=request.getContextPath()%>/admin/cms/js/friendSite.js"></script>
	</head>
	<body class="mainContent">
	 <form id="friendSiteForm" name="friendSiteForm" action="#">
	 <c:choose>
	   <c:when test="${method eq 'add'}">
	   <table width="161px" cellspacing="0" cellpadding="0" border="0">
           <tr align="center">
              <td class="titlemouseover" id="td_tab1">基本信息</td>
              <td width="2px"></td>
              <td class="tabtitle" id="td_tab2">属性选项</td>
           </tr>
       </table>
       <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap dispalyTable" id="tb_Pannel1">
            <tr class="tdbg">
                <td align="right" style="width:200px;" class="tdbgleft"><strong>所属节点 ：&nbsp;</strong></td>
               <td>
                   <input type="text" style="width:250px;" class="inputtext" disabled="disabled" id="txt_CheckChannelName" value="友情链接中心" name="txt_CheckChannelName"><input type="button" value="选择" class="inputbutton" id="bt_Checked">
                   <input id="txt_channelId" type="hidden" value="${defaultChannel.channelId }"/>
               </td>
            </tr>
             <tr class="tdbg">
                <td align="right" class="tdbgleft"><strong>网站名称 ：&nbsp;</strong></td>
               <td>
               <table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
						<input type="text" style="width:200px;" class="inputtext" id="txt_SiteName" name="txt_SiteName"><span id="messageInfo_1" style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_SiteNameTip" style="width:280px"></div></td>
	                </tr>
	            </table>
               </td>
            </tr>
             <tr class="tdbg">
                <td align="right" class="tdbgleft"><strong>网站地址 ：&nbsp;</strong></td>
               <td>
				<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
						<input type="text" style="width:200px;" class="inputtext" id="txt_SiteUrl" name="txt_SiteUrl">
		                </td>
		                <td><div id="txt_SiteUrlTip" style="width:280px"></div></td>
	                </tr>
	            </table>
               </td>
            </tr>
             <tr class="tdbg">
               <td align="right" class="tdbgleft"><strong>网站Logo地址 ：&nbsp;</strong></td>
               <td>
               		<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
						<div class="radioMiddle">
	                    <input type="radio" class="valignMiddle" id="rd_StatusPic1" name="rd_StatusPic" checked="checked"><span class="valignMiddle">网络图片</span>
	                   	<input type="radio" class="valignMiddle" id="rd_StatusPic2" name="rd_StatusPic"><span class="valignMiddle">本地图片</span>
	                   	</div>
		                </td>
	                </tr>
	                <tr>
	                	<td>
		                   <div style="padding-top: 5px;">
		                    <input type="text" style="width:280px;" class="input_public" id="txt_SiteLogoUrl" value="http://" name="txt_SiteLogoUrl"/><span id="span_viewImg" style="display:none;">&nbsp;<input id="bnt_selectImg" name="bnt_selectImg" type="button" class="inputbutton" value="从已上传的文件中选择" callback="callbackDefaultPic"/>&nbsp;<input id="bnt_review" reviewfrom="txt_SiteLogoUrl" type="button" value=" 预览 " class="inputbutton"/></span>
		                   </div>
		                   <div style="display:none;" id="div_UpLoad">
							<iframe marginheight="0" marginwidth="0" scrolling="no" src="<%=request.getContextPath()%>/security/loadDefaultImg.html?callback=callbackDefaultPic&iframe=OpendialogWindow_FriendSiteDetail" width="100%" height="58px" frameborder="0"></iframe>
		                   </div>
	                	</td>
	                </tr>
	            	</table>
               </td>
            </tr>
             <tr class="tdbg">
                <td align="right" class="tdbgleft"><strong>网站简介 ：&nbsp;</strong></td>
               <td>
                   <textarea style="height:90px;width:420px;" class="inputtext" id="txt_SiteIntro" cols="20" rows="2" name="txt_SiteDes"></textarea></td>
            </tr>
             <tr class="tdbg">
                <td align="right" class="tdbgleft"><strong>链接类型 ：&nbsp;</strong></td>
               <td>
                    <table cellspacing="5" cellpadding="0" border="0">
                        <tr>
                            <td><input type="radio" class="valignMiddle" checked="checked" id="rd_LinkType_1" name="LinkType" value="rd_LinkType_1"><span class="valignMiddle">文字类型</span></td>
                        </tr>
                        <tr>
                            <td><input type="radio" class="valignMiddle" id="rd_LinkType_2" name="LinkType" value="rd_LinkType_2"><span class="valignMiddle">图片类型</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="tdbg">
                <td align="right" class="tdbgleft"><strong>是否推荐 ：&nbsp;</strong></td>
                <td>&nbsp;<input type="checkbox" name="cb_IsElite" id="cb_IsElite"></td>
            </tr>
            <tr class="tdbg">
                <td align="right" class="tdbgleft"><strong>状态 ：&nbsp;</strong></td>
               <td>
                   <input type="radio" class="valignMiddle" id="rd_Status_1" name="Site_Status" value="rd_Status_1"><span class="valignMiddle">草稿</span>
                   <input type="radio" class="valignMiddle" id="rd_Status_2" name="Site_Status" value="rd_Status_2"><span class="valignMiddle">待审核</span>
                   <input type="radio" class="valignMiddle" checked="checked" id="rd_Status_3" name="Site_Status" value="rd_Status_3"><span class="valignMiddle">终审通过</span>
               </td>
            </tr>
         </table>
         <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel2">
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>总点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_TotlaHits" value="0" name="txt_TotlaHits"/>
		                </td>
		                <td><div id="txt_TotlaHitsTip" style="width:280px"></div></td>
	                </tr>
	            	</table>
	            </td>
	        </tr>
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>日点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
							<input type="text" style="width:50px;" class="inputtext" id="txt_DayHits" value="0" name="txt_DayHits"/>
		                </td>
		                <td><div id="txt_DayHitsTip" style="width:280px"></div></td>
	                </tr>
	            </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>周点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
						<input type="text" style="width:50px;" class="inputtext" id="txt_WeekHits" value="0" name="txt_WeekHits"/>
		                </td>
		                <td><div id="txt_WeekHitsTip" style="width:280px"></div></td>
	                </tr>
	            	</table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>月点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
						<input type="text" style="width:50px;" class="inputtext" id="txt_MonthHits" value="0" name="txt_MonthHits"/>
		                </td>
		                <td><div id="txt_MonthHitsTip" style="width:280px"></div></td>
	                </tr>
	            	</table>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>更新时间 ：&nbsp;</strong></td>
	            <td>
	            	<input type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" id="txt_UpdataTime" value="${date}" name="txt_UpdataTime"/>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>推荐级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_EliteLevel" value="0" name="txt_EliteLevel"/>
		                </td>
		                <td><div id="txt_EliteLevelTip" style="width:280px"></div></td>
	                </tr>
	            </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>优先级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:50px;" class="inputtext" id="txt_Priority" value="0" name="txt_Priority"/>
		                </td>
		                <td><div id="txt_PriorityTip" style="width:280px"></div></td>
	                </tr>
	            	</table>
	            </td>
	        </tr>
     	</table>
        <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bt_Save" value="保存">&nbsp;
	                <input type="button" class="inputbutton" id="bt_Back" value="关闭" >
	             </td>
	        </tr>
        </table>
        </c:when>
        <c:otherwise>
        <!-- 修改html代码 -->
	   <table width="161px" cellspacing="0" cellpadding="0" border="0">
           <tr align="center">
              <td class="titlemouseover" id="td_tab1">基本信息</td>
              <td width="2px"></td>
              <td class="tabtitle" id="td_tab2">属性选项</td>
           </tr>
       </table>
       <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap dispalyTable" id="tb_Pannel1">
            <tr class="tdbg">
                <td align="right" style="width:200px;" class="tdbgleft"><strong>所属节点 ：&nbsp;</strong></td>
               <td>
                   <input type="text" style="width:250px;" class="inputtext" disabled="disabled" id="txt_CheckChannelName" value="${bean.channelName}" name="txt_CheckChannelName"/><input type="button" onclick="showChannel();" value="选择" class="inputbutton" id="bt_Checked"/>
           		   	<input id="txt_channelId" type="hidden" value="${bean.channelId }"/>
            		<input id="txt_commonModelId" type="hidden" value="${bean.commonModelId }"/>
           		</td>
            </tr>
             <tr class="tdbg">
                <td align="right" class="tdbgleft"><strong>网站名称 ：&nbsp;</strong></td>
               <td>
               	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
						<input type="text" style="width:200px;" class="inputtext" id="txt_SiteName" name="txt_SiteName" value="${bean.title}"><span id="messageInfo_1" style="color:#FF0000;"/>*</span>
		                </td>
		                <td><div id="txt_SiteNameTip" style="width:280px"></div></td>
	                </tr>
	            </table>
                   </td>
            </tr>
             <tr class="tdbg">
                <td align="right" class="tdbgleft"><strong>网站地址 ：&nbsp;</strong></td>
               <td>
				<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
						<input type="text" style="width:200px;" class="inputtext" id="txt_SiteUrl" name="txt_SiteUrl" value="${bean.siteUrl}"/>
		                </td>
		                <td><div id="txt_SiteUrlTip" style="width:280px"></div></td>
	                </tr>
	            </table>
               </td>
            </tr>
             <tr class="tdbg">
               <td align="right" class="tdbgleft"><strong>网站Logo地址 ：&nbsp;</strong></td>
               <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
						<div class="radioMiddle">
	                    <input type="radio" class="valignMiddle" id="rd_StatusPic1" name="rd_StatusPic" checked="checked"><span class="valignMiddle">网络图片</span>
	                   	<input type="radio" class="valignMiddle" id="rd_StatusPic2" name="rd_StatusPic"><span class="valignMiddle">本地图片</span>
	                   	</div>
		                </td>
	                </tr>
	                <tr>
	                	<td>
		                   <div style="padding-top: 5px;">
		                    <input type="text" style="width:280px;" class="input_public" id="txt_SiteLogoUrl" value="${bean.logoUrl}" name="txt_SiteLogoUrl"/><span id="span_viewImg" style="display:none;">&nbsp;<input id="bnt_selectImg" name="bnt_selectImg" type="button" class="inputbutton" value="从已上传的文件中选择" callback="callbackDefaultPic"/>&nbsp;<input id="bnt_review" reviewfrom="txt_SiteLogoUrl" type="button" value=" 预览 " class="inputbutton"/></span>
		                   </div>
		                   <div style="display:none;" id="div_UpLoad">
							<iframe marginheight="0" marginwidth="0" scrolling="no" src="<%=request.getContextPath()%>/security/loadDefaultImg.html?callback=callbackDefaultPic&iframe=OpendialogWindow_FriendSiteDetail" width="100%" height="58px" frameborder="0"></iframe>
		                   </div>
	                	</td>
	                </tr>
	            	</table>
               </td>
            </tr>
             <tr class="tdbg">
                <td align="right" class="tdbgleft"><strong>网站简介 ：&nbsp;</strong></td>
               <td>
                   <textarea style="height:90px;width:420px;" class="inputtext" id="txt_SiteIntro" cols="20" rows="2" name="txt_SiteDes">${bean.siteIntro}</textarea></td>
            </tr>
             <tr class="tdbg">
                <td align="right" class="tdbgleft"><strong>链接类型 ：&nbsp;</strong></td>
               <td>
                    <table cellspacing="5" cellpadding="0" border="0">
                        <tr>
                            <td><input type="radio" class="valignMiddle" <c:if test="${bean.linkType==1}">checked="checked"</c:if> id="rd_LinkType_1" name="LinkType" value="rd_LinkType_1"><span class="valignMiddle">文字类型</span></td>
                        </tr>
                        <tr>
                            <td><input type="radio" class="valignMiddle" <c:if test="${bean.linkType==2}">checked="checked"</c:if> id="rd_LinkType_2" name="LinkType" value="rd_LinkType_2"><span class="valignMiddle">图片类型</span></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="tdbg">
                <td align="right" class="tdbgleft"><strong>是否推荐 ：&nbsp;</strong></td>
                <td>&nbsp;<input type="checkbox" name="cb_IsElite" id="cb_IsElite" <c:if test="${bean.isElite==true}">checked="checked"</c:if> /></td>
            </tr>
            <tr class="tdbg">
                <td align="right" class="tdbgleft"><strong>状态 ：&nbsp;</strong></td>
               <td>
                   <input type="radio" class="valignMiddle" id="rd_Status_1" name="Site_Status" <c:if test="${bean.status==-1}">checked="checked"</c:if>><span class="valignMiddle">草稿</span>
                   <input type="radio" class="valignMiddle" id="rd_Status_2" name="Site_Status" <c:if test="${bean.status==0}">checked="checked"</c:if>><span class="valignMiddle">待审核</span>
                   <input type="radio" class="valignMiddle" checked="checked" id="rd_Status_3" name="Site_Status" <c:if test="${bean.status==99}">checked="checked"</c:if>><span class="valignMiddle">终审通过</span>
               </td>
            </tr>
         </table>
         <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-bottom:0px;" class="tableWrap diplayNone" id="tb_Pannel2">
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>总点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
	                	<tr>
		                	<td>
							<input type="text" style="width:50px;" class="inputtext" id="txt_TotlaHits" value="${bean.hits}" name="txt_TotlaHits"/>
			                </td>
			                <td><div id="txt_TotlaHitsTip" style="width:280px"></div></td>
		                </tr>
		            </table>
	            </td>
	        </tr>
	         <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>日点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
						<input type="text" style="width:50px;" class="inputtext" id="txt_DayHits" value="${bean.dayHits}" name="txt_DayHits"/>
		                </td>
		                <td><div id="txt_DayHitsTip" style="width:280px"></div></td>
	                </tr>
	            	</table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>周点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
							<input type="text" style="width:50px;" class="inputtext" id="txt_WeekHits" value="${bean.weekHits}" name="txt_WeekHits"/>
		                </td>
		                <td><div id="txt_WeekHitsTip" style="width:280px"></div></td>
	                </tr>
	            	</table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>月点击数 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
						<input type="text" style="width:50px;" class="inputtext" id="txt_MonthHits" value="${bean.monthHits}" name="txt_MonthHits"/>
		                </td>
		                <td><div id="txt_MonthHitsTip" style="width:280px"></div></td>
	                </tr>
	            	</table>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>更新时间 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" id="txt_UpdataTime" value="${bean.updateTime}" name="txt_UpdataTime"/>
		                </td>
		                <td></td>
	                </tr>
	            </table>
	            </td>
	        </tr>
	        <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>推荐级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
						<input type="text" style="width:50px;" class="inputtext" id="txt_EliteLevel" value="${bean.eliteLevel}" name="txt_EliteLevel"/>
		                </td>
		                <td><div id="txt_EliteLevelTip" style="width:280px"></div></td>
	                </tr>
	            </table>
	            </td>
	        </tr>
	          <tr class="tdbg">
	            <td align="right" style="width:200px;" class="tdbgleft">
	                <strong>优先级 ：&nbsp;</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
							<input type="text" style="width:50px;" class="inputtext" id="txt_Priority" value="${bean.priority}" name="txt_Priority"/>
		                </td>
		                <td><div id="txt_PriorityTip" style="width:280px"></div></td>
	                </tr>
	            </table>
	            </td>
	        </tr>
     	</table>
        <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bt_Update" value="保存">&nbsp;
	                <input type="button" class="inputbutton" id="bt_Back" value="关闭" >
	             </td>
	        </tr>
        </table>
        </c:otherwise>
       </c:choose>
     </form>
	</body>
</html>