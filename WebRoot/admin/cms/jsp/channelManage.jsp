<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>栏目管理-站点栏目管理</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<link href="<%=request.getContextPath()%>/admin/common/zTree/css/zTreeStyle.css" rel="stylesheet" type="text/css"/>
		<script src="<%=request.getContextPath()%>/admin/common/zTree/js/jquery.ztree.core-3.5.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/cms/js/channelManage.js"></script>
		<style type="text/css">
		.spanTree{
			background-attachment: scroll;background-color: transparent;
		    background-image: url("<%=request.getContextPath()%>/admin/imgs/blue/tree_arrows.gif");
		    background-repeat: no-repeat;border: 0 none;cursor: pointer;display: inline-block;height: 18px;
		    line-height: 0;margin: 0;outline: medium none;vertical-align: middle;width: 16px;
		}
		.closeTree{
			background-position: 0 0;
		}
		.closeMouseOn{
			background-position: -32px 0;
		}
		.openTree{
			background-position: -18px 0;
		}
		.openMouseOn{
			background-position: -50px 0;
		}
		.spanMiddle{
			line-height: 18px;margin-right: 2px;
		}

		.divSys{
		    display: block;
			width:310px;
		}
		.divSys ul{
			list-style-type:none;
			width:310px;
		}
		.divSys li{
			padding:5px;
			float:left;
			text-align:center;
		}
		</style>
		<script type="text/javascript">
			var contextPath = '<%=request.getContextPath()%>';
			var setting = {
				view: {
					fontCss: getFont
				},
				data: {
					simpleData: {
						enable: true
					}
				}
			};
			var zNodes =[
					{id:0,pId:-99,name:'栏目节点',icon:contextPath+'/admin/imgs/blue/channel_root.png',font:{'font-weight':'bold'},open:true,type:0},
				<c:forEach var="list" items="${list}" varStatus="status">
				<c:choose>
					<c:when test="${list.channelType==0}">
					{id:${list.channelId},pId:<c:choose><c:when test="${list.parentId<=0}">0</c:when><c:otherwise>${list.parentId}</c:otherwise></c:choose>,name:"${list.channelName}",open:true,font:{'font-weight':'bold'},type:${list.channelType},icon:contextPath+'/admin/imgs/blue/channel_contain.png'}
					</c:when>
					<c:when test="${list.channelType==2}">
					{id:${list.channelId},pId:<c:choose><c:when test="${list.parentId<=0}">0</c:when><c:otherwise>${list.parentId}</c:otherwise></c:choose>,name:"${list.channelName}",open:true,font:{'font-weight':'bold'},type:${list.channelType},icon:contextPath+'/admin/imgs/blue/channel_single.png'}
					</c:when>
					<c:otherwise>
					{id:${list.channelId},pId:<c:choose><c:when test="${list.parentId<=0}">0</c:when><c:otherwise>${list.parentId}</c:otherwise></c:choose>,name:"${list.channelName}",open:true,font:{'font-weight':'bold'},type:${list.channelType},icon:contextPath+'/admin/imgs/blue/channel_external.png'}
					</c:otherwise>
				</c:choose>
					<c:if test="${!status.last}">,</c:if>
				</c:forEach>
			];
			function getFont(treeId, node) {
				return node.font ? node.font : {};
			}
		</script>
	</head>
	<body class="mainContent">
		<span class="localSpan">后台管理<span> &gt;&gt; </span><span> 内容管理 </span><span>&gt;&gt; </span><span>栏目列表 </span></span>
	    <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
	        <tr class="topbottom">
	            <td>
	            	<input id="channel_Add" class="topinput topinput_add" type="button" value="新建"/>
	            	<input id="channel_Update" class="topinput topinput_edit" type="button" value="修改"/>
	            	<input id="channel_Del" class="topinput topinput_del" type="button" value="删除选定选"/>
	            	<input id="channel_Detail" class="topinput topinput_detail" type="button" value="查看详情"/>
	            </td>
	        </tr>
	        <c:choose>
		    	<c:when test="${!empty list&&listNum>0}">
		    	<tr>
					<td align="center" class="tdbg" valign="top">
		        		<div class="zTreeDemoBackground left">
							<ul id="treeChannels" class="ztree"></ul>
						</div>
		        	</td>
	        	</tr>
	        	</c:when>
	        	<c:otherwise>
	        	<tr class="tdbg" style="height:80px;">
		             <td align="center">
		                  <strong class="valignMiddle">没有任何数据，请新增栏目！</strong>
		             </td>
		        </tr>
	        	</c:otherwise>
	        </c:choose>
     	</table>
	</body>
</html>