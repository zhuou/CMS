<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>栏目管理-栏目列表</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/cms/js/channelList.js"></script>
		<script type="text/javascript">
			var contextPath = '<%=request.getContextPath()%>';
		</script>
		<style type="text/css">
		.spanTree{
			background-attachment: scroll;background-color: transparent;
		    background-image: url("<%=request.getContextPath()%>/admin/imgs/blue/tree_arrows.gif");
		    background-repeat: no-repeat;border: 0 none;cursor: pointer;display: inline-block;height: 18px;
		    line-height: 0;margin: 0;outline: medium none;vertical-align: middle;width: 16px;
		}
		.closeTree{
			background-position: 0 0;
		}
		.closeMouseOn{
			background-position: -32px 0;
		}
		.openTree{
			background-position: -18px 0;
		}
		.openMouseOn{
			background-position: -50px 0;
		}
		.spanMiddle{
			line-height: 18px;margin-right: 2px;
		}

		.divSys{
		    display: block;
			width:310px;
		}
		.divSys ul{
			list-style-type:none;
			width:310px;
		}
		.divSys li{
			padding:5px;
			float:left;
			text-align:center;
		}
		</style>
	</head>
	<body class="mainContent">
		<span class="localSpan">后台管理<span> &gt;&gt; </span><span> 内容管理 </span><span>&gt;&gt; </span><span>栏目列表 </span></span>
	    <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
	        <tr class="topbottom">
	            <td colspan="10">
	            	<input id="channel_Add" class="topinput topinput_add" type="button" value="新建"/>
	            	<input id="channel_Update" class="topinput topinput_edit" type="button" value="修改"/>
	            	<input id="channel_Del" class="topinput topinput_del" type="button" value="删除选定选"/>
	            	<input id="channel_SearchText" class="inputtext_public" type="text" alt="请输入栏目名称" style="width:150px;">
	            	<input id="channel_Search" class="topinput topinput_search" type="button" value="搜索"/>
	            </td>
	        </tr>
	        <c:choose>
		    	<c:when test="${pageBean.totalRecordNum>0}">
	        <tr class="title">
	        	<th style="width:3%;"><input id="cb_CheckedAll" type="checkbox"/></th>
	            <th style="width:16%;">栏目名称</th>
	            <th style="width:13%;">栏目别名</th>
	            <th style="width:8%;">栏目类型</th>
	            <th style="width:8%;">所属用户</th>
	            <th style="width:8%;">排序</th>
	            <th style="width:8%;">打开方式</th>
	            <th style="width:10%;">栏目tip</th>
	            <th style="width:13%;">链接地址</th>
	            <th style="width:13%;">描述</th>
	        </tr>
	        <tbody id="tbody_DataList">
		        <c:forEach var="list" items="${pageBean.dataList}">
		        <tr class="tdbg">
		        	<td align="center"><input type="checkbox" value="${list.channelId}"/></td>
		        	<td>
			        <a href="#" name="aChannelDetail_${list.channelId}_${list.channelType}" style="background:transparent url(<%=request.getContextPath()%>/<c:choose><c:when test="${list.channelType==0}">admin/imgs/blue/channel_contain.png</c:when><c:when test="${list.channelType==2}">admin/imgs/blue/channel_single.png</c:when><c:otherwise>admin/imgs/blue/channel_external.png</c:otherwise></c:choose>) no-repeat scroll;padding: 3px 0px 2px 16px;">${list.channelName}</a>
		        	</td>
		        	<td align="center">${list.nickName}</td>
		        	<td align="center">
			        	<c:choose>
						<c:when test="${list.channelType==0}">
						内容栏目
						</c:when>
						<c:when test="${list.channelType==2}">
						单页栏目
						</c:when>
						<c:otherwise>
						外接栏目
						</c:otherwise>
						</c:choose>
		        	</td>
		        	<td align="center">${list.companyName}</td>
		        	<td align="center">${list.sort}</td>
		        	<td align="center">${list.openType==0?"原窗口打开":"新窗口打开"}</td>
		        	<td align="center">${list.tips}</td>
		        	<td align="center">${list.channelUrl}</td>
		        	<td align="center">${list.description}</td>
		        </tr>
		        </c:forEach>
		    </tbody>
		     <tr align="right" class="tdbg" style="height:28px;">
	        	<td colspan="10">&nbsp;
	        		<c:import url="pageCurrent.jsp">
	        			<c:param name="totalRecordNum" value="${pageBean.totalRecordNum}"></c:param>
	        			<c:param name="pageRecordNum" value="${pageBean.pageRecordNum}"></c:param>
			        </c:import>
	        	</td>
	         </tr>
	        </c:when>
	        <c:otherwise>
	        	<tr class="tdbg" style="height:80px;">
		             <td colspan="10" align="center">
		                  <strong class="valignMiddle">没有任何数据！</strong>
		             </td>
		        </tr>
	        </c:otherwise>
	        </c:choose>
     	</table>
	</body>
</html>