<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>广告新增修改</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
		var reqMethod = '${method}';
    	</script>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/cms/js/ad.js" type="text/javascript"></script>
	</head>
	<body class="mainContent">
	<form id="adForm" name="adForm" action="#">
	<c:choose>
	 <c:when test="${method eq 'add'}">
     <table border="0" cellpadding="2" cellspacing="1" class="tableWrap" width="100%" style="border-bottom:0px;">
		<tr class="tdbg">
            <td class="tdbgleft" style="width:15%;"><strong>所属广告位：</strong></td>
            <td>
				<select id="select_zoneName">
					<c:forEach var="list" items="${list}">
					<option value="${list.zoneId}">${list.zoneName}</option>
					</c:forEach>
				</select>
       		</td>
        </tr>
		<tr class="tdbg">
             <td class="tdbgleft"><strong>广告名称：</strong></td>
             <td>
				<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_adName" type="text" style="width:200px;"/><span style="color:#FF0000;">*</span>
		                </td>
		                <td><div id="txt_adNameTip"></div></td>
	                </tr>
	            </table>
             </td>
         </tr>
         <tr class="tdbg">
              <td class="tdbgleft"><strong>广告类型：</strong></td>
              <td class="radioMiddle">
              	<input type="radio" name="adType" checked="checked" class="valignMiddle" value="tablePic"/><span class="valignMiddle">图片</span>
              	<input type="radio" name="adType" class="valignMiddle" value="tableAnimation"/><span class="valignMiddle">动画</span>
              	<input type="radio" name="adType" class="valignMiddle" value="tableText"/><span class="valignMiddle">文本</span>
              </td>
         </tr>
		<tr class="tdbg">
              <td class="tdbgleft"><strong>广告内容：</strong></td>
              <td>
                  <table id="tablePic" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
                      <tr class="tdbg">
                          <td class="tdbgleft">图片上传：</td>
                          <td>
				            <input id="txt_imgUrl" class="input_public" style="width:300px;"/>&nbsp;<input id="bnt_selectImg" name="bnt_selectImg" type="button" class="inputbutton" value="从已上传的文件中选择" callback="callbackDefaultPic"/>&nbsp;<input id="bnt_review" reviewfrom="txt_imgUrl" type="button" value=" 预览 " class="inputbutton"/><br />
				            <iframe marginheight="0" marginwidth="0" scrolling="no" src="<%=request.getContextPath()%>/security/loadDefaultImg.html?callback=callbackDefaultPic&iframe=OpendialogWindow_AdDetail" width="100%" height="58px" frameborder="0"></iframe>
				          </td>
                      </tr>
                      <tr class="tdbg">
                          <td class="tdbgleft">图片尺寸：</td>
                          <td>
                             <table cellpadding="0" cellspacing="0" border="0">
			                	<tr>
				                	<td>宽：<input id="txt_imgWidth1" type="text" style="width:50px;"/>像素</td>
				                	<td><div id="txt_imgWidth1Tip"></div></td>
				                </tr>
				                <tr>
				                	<td>高：<input id="txt_imgHeight1" type="text" style="width:50px;"/>像素</td>
				                	<td><div id="txt_imgHeight1Tip"></div></td>
				                </tr>
				            </table>
                          </td>
                      </tr>
                      <tr class="tdbg">
                          <td class="tdbgleft">链接地址：</td>
                          <td>
                          	<table cellpadding="0" cellspacing="0" border="0">
                          		<tr>
	                          		<td><input id="txt_linkUrl" type="text" style="width:200px;"/></td>
	                          		<td><div id="txt_linkUrlTip"></div></td>
                          		</tr>
                          	</table>
                          </td>
                      </tr>
                      <tr class="tdbg">
                          <td class="tdbgleft">链接提示：</td>
                          <td>
                          	<table cellpadding="0" cellspacing="0" border="0">
                          		<tr>
	                          		<td><input id="txt_linkAlt" type="text" style="width:200px;"/></td>
	                          		<td><div id="txt_linkAltTip"></div></td>
                          		</tr>
                          	</table>
                          </td>
                      </tr>
                      <tr class="tdbg">
                          <td class="tdbgleft">广告简介：</td>
                          <td>
							<textarea id="txt_adIntro" rows="3" cols="46" class="inputtext"></textarea>
                          </td>
                      </tr>
                  </table>
                  <table id="tableAnimation" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" style="display:none;">
                      <tr class="tdbg">
                          <td class="tdbgleft" style="width:70px;">动画上传：</td>
                          <td>
							<input id="txt_animationUrl" class="input_public" style="width:300px;"/>&nbsp;<input id="bnt_selectFile" name="bnt_selectFile" type="button" class="inputbutton" value="从已上传的文件中选择" callback="callbackDefaultPic"/><br />
				            <iframe marginheight="0" marginwidth="0" scrolling="no" src="<%=request.getContextPath()%>/security/loadUploadFile.html?callback=callbackDefaultPic&iframe=OpendialogWindow_AddArticle" width="100%" height="55px" frameborder="0"></iframe>
                          </td>
                      </tr>
                      <tr class="tdbg">
                          <td class="tdbgleft">动画尺寸：</td>
                          <td>
                          	<table cellpadding="0" cellspacing="0" border="0">
			                	<tr>
				                	<td>宽：<input id="txt_imgWidth2" type="text" style="width:50px;"/>像素</td>
				                	<td><div id="txt_imgWidth2Tip"></div></td>
				                </tr>
				                <tr>
				                	<td>高：<input id="txt_imgHeight2" type="text" style="width:50px;"/>像素</td>
				                	<td><div id="txt_imgHeight2Tip"></div></td>
				                </tr>
				            </table>
                          </td>
                      </tr>
                  </table>
                  <table id="tableText" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" style="display:none;">
                      <tr class="tdbg">
                          <td><strong>广告内容设置--文本</strong></td>
                      </tr>
                      <tr class="tdbg">
                          <td>
							<textarea id="txt_adIntro1" rows="12" cols="80" class="inputtext"></textarea>
                          </td>
                      </tr>
                  </table>
              </td>
          </tr>
          <tr class="tdbg">
              <td class="tdbgleft"><strong>权重：</strong></td>
              <td>
              	<table cellpadding="0" cellspacing="0" border="0">
			    	<tr>
			    		<td style="width:50px;">
			    			<input id="txt_priority" type="text" style="width:50px;" value="0"/>
			    		</td>
			    		<td><div id="txt_priorityTip"></div></td>
			    	</tr>
					<tr>
			    		<td colspan="2">版位按权重（优先级、随机）显示时，此处设置的权重越大，该广告显示的机会就越多。</td>
			    	</tr>
			    </table>
              </td>
          </tr>
          <tr class="tdbg">
              <td class="tdbgleft"><strong>排序：</strong></td>
              <td>
              	<table cellpadding="0" cellspacing="0" border="0">
			    	<tr>
			    		<td>
			    			<input id="txt_sort" type="text" style="width:50px;" value="1"/>
			    		</td>
			    		<td><div id="txt_sortTip"></div></td>
			    	</tr>
			    </table>
              </td>
          </tr>
          <tr class="tdbg">
              <td class="tdbgleft"><strong>状态：</strong></td>
              <td class="radioMiddle">
              	<input id="rb_NoPassed" type="radio" name="rb_Passed" class="valignMiddle"/><span class="valignMiddle">待审核</span>
              	<input id="rb_Passed" type="radio" name="rb_Passed" checked="checked" class="valignMiddle"/><span class="valignMiddle">终审通过</span>
              </td>
          </tr>
          <tr class="tdbg">
              <td class="tdbgleft"><strong>时间限制：</strong></td>
              <td class="radioMiddle">
              	<input id="rb_CloseTime" type="radio" name="rb_Time" class="valignMiddle" checked="checked"/><span class="valignMiddle">关闭</span>
              	<input id="rb_OpenTime" type="radio" name="rb_Time" class="valignMiddle"/><span class="valignMiddle">开启</span>
                <span class="cautiontitle">如果启用时间限制，会按以下时间段进行显示！</span>
                <div id="div_Time" style="padding-top:5px; display:none;">
                	<table cellpadding="0" cellspacing="0" border="0">
                		<tr>
                			<td>
							开始时间：<input type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" id="txt_startTime"/>
                			</td>
                		</tr>
                		<tr>
                			<td>
                			 结束时间：<input type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" id="txt_endTime"/>
                			</td>
                		</tr>
                	</table>
                </div>
              </td>
          </tr>
     </table>
     <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
        <tr>
            <td align="center">
                <input type="button" class="inputbutton" id="bnt_save" value="保 存">&nbsp;
                <input type="button" class="inputbutton" id="bnt_close" value="关闭" >
             </td>
        </tr>
     </table>
	 </c:when>
     <c:otherwise>
		<table border="0" cellpadding="2" cellspacing="1" class="tableWrap" width="100%" style="border-bottom:0px;">
		<tr class="tdbg">
            <td class="tdbgleft" style="width:15%;"><strong>所属广告位：</strong></td>
            <td>
				<select id="select_zoneName">
					<c:forEach var="list" items="${list}">
					<option value="${list.zoneId}" <c:if test="${list.zoneName eq bean.zoneName}">selected="selected"</c:if>>${list.zoneName}</option>
					</c:forEach>
				</select>
       		</td>
        </tr>
		<tr class="tdbg">
             <td class="tdbgleft"><strong>广告名称：</strong></td>
             <td>
				<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
	                	<td>
		                <input id="txt_adName" type="text" style="width:200px;" value="${bean.adName}"/><span style="color:#FF0000;">*</span>
		                <input id="txt_adId" type="hidden" value="${bean.adId}"/>
		                </td>
		                <td><div id="txt_adNameTip"></div></td>
	                </tr>
	            </table>
             </td>
         </tr>
         <tr class="tdbg">
              <td class="tdbgleft"><strong>广告类型：</strong></td>
              <td class="radioMiddle">
              	<input type="radio" name="adType" class="valignMiddle" value="tablePic" <c:if test="${bean.adType==1}">checked="checked"</c:if>/><span class="valignMiddle">图片</span>
              	<input type="radio" name="adType" class="valignMiddle" value="tableAnimation" <c:if test="${bean.adType==2}">checked="checked"</c:if>/><span class="valignMiddle">动画</span>
              	<input type="radio" name="adType" class="valignMiddle" value="tableText" <c:if test="${bean.adType==3}">checked="checked"</c:if>/><span class="valignMiddle">文本</span>
              </td>
         </tr>
		<tr class="tdbg">
              <td class="tdbgleft"><strong>广告内容：</strong></td>
              <td>
                  <table id="tablePic" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" <c:choose><c:when test="${bean.adType==1}"></c:when><c:otherwise>style="display:none;"</c:otherwise></c:choose>>
                      <tr class="tdbg">
                          <td class="tdbgleft">图片上传：</td>
                          <td>
				            <input id="txt_imgUrl" class="input_public" style="width:300px;" value="<c:if test="${bean.adType==1}">${bean.imgUrl}</c:if>"/>&nbsp;<input id="bnt_selectImg" name="bnt_selectImg" type="button" class="inputbutton" value="从已上传的文件中选择" callback="callbackDefaultPic"/>&nbsp;<input id="bnt_review" reviewfrom="txt_imgUrl" type="button" value=" 预览 " class="inputbutton"/><br />
				            <iframe marginheight="0" marginwidth="0" scrolling="no" src="<%=request.getContextPath()%>/security/loadDefaultImg.html?callback=callbackDefaultPic&iframe=OpendialogWindow_AdDetail" width="100%" height="58px" frameborder="0"></iframe>
				          </td>
                      </tr>
                      <tr class="tdbg">
                          <td class="tdbgleft">图片尺寸：</td>
                          <td>
                             <table cellpadding="0" cellspacing="0" border="0">
			                	<tr>
				                	<td>宽：<input id="txt_imgWidth1" type="text" style="width:50px;" value="<c:if test="${bean.adType==1}">${bean.imgWidth}</c:if>"/>像素</td>
				                	<td><div id="txt_imgWidth1Tip"></div></td>
				                </tr>
				                <tr>
				                	<td>高：<input id="txt_imgHeight1" type="text" style="width:50px;" value="<c:if test="${bean.adType==1}">${bean.imgHeight}</c:if>"/>像素</td>
				                	<td><div id="txt_imgHeight1Tip"></div></td>
				                </tr>
				            </table>
                          </td>
                      </tr>
                      <tr class="tdbg">
                          <td class="tdbgleft">链接地址：</td>
                          <td>
                          	<table cellpadding="0" cellspacing="0" border="0">
                          		<tr>
	                          		<td><input id="txt_linkUrl" type="text" style="width:200px;" value="<c:if test="${bean.adType==1}">${bean.linkUrl}</c:if>"/></td>
	                          		<td><div id="txt_linkUrlTip"></div></td>
                          		</tr>
                          	</table>
                          </td>
                      </tr>
                      <tr class="tdbg">
                          <td class="tdbgleft">链接提示：</td>
                          <td>
                          	<table cellpadding="0" cellspacing="0" border="0">
                          		<tr>
	                          		<td><input id="txt_linkAlt" type="text" style="width:200px;" value="<c:if test="${bean.adType==1}">${bean.linkAlt}</c:if>"/></td>
	                          		<td><div id="txt_linkAltTip"></div></td>
                          		</tr>
                          	</table>
                          </td>
                      </tr>
                      <tr class="tdbg">
                          <td class="tdbgleft">广告简介：</td>
                          <td>
							<textarea id="txt_adIntro" rows="3" cols="46" class="inputtext"><c:if test="${bean.adType==1}">${bean.adIntro}</c:if></textarea>
                          </td>
                      </tr>
                  </table>
                  <table id="tableAnimation" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" <c:choose><c:when test="${bean.adType==2}"></c:when><c:otherwise>style="display:none;"</c:otherwise></c:choose>>
                      <tr class="tdbg">
                          <td class="tdbgleft" style="width:70px;">动画上传：</td>
                          <td>
							<input id="txt_animationUrl" class="input_public" style="width:300px;" value="<c:if test="${bean.adType==2}">${bean.imgUrl}</c:if>"/>&nbsp;<input id="bnt_selectFile" name="bnt_selectFile" type="button" class="inputbutton" value="从已上传的文件中选择" callback="callbackDefaultPic"/><br />
				            <iframe marginheight="0" marginwidth="0" scrolling="no" src="<%=request.getContextPath()%>/security/loadUploadFile.html?callback=callbackDefaultPic&iframe=OpendialogWindow_AddArticle" width="100%" height="55px" frameborder="0"></iframe>
                          </td>
                      </tr>
                      <tr class="tdbg">
                          <td class="tdbgleft">动画尺寸：</td>
                          <td>
                          	<table cellpadding="0" cellspacing="0" border="0">
			                	<tr>
				                	<td>宽：<input id="txt_imgWidth2" type="text" style="width:50px;" value="<c:if test="${bean.adType==2}">${bean.imgWidth}</c:if>"/>像素</td>
				                	<td><div id="txt_imgWidth2Tip"></div></td>
				                </tr>
				                <tr>
				                	<td>高：<input id="txt_imgHeight2" type="text" style="width:50px;" value="<c:if test="${bean.adType==2}">${bean.imgHeight}</c:if>"/>像素</td>
				                	<td><div id="txt_imgHeight2Tip"></div></td>
				                </tr>
				            </table>
                          </td>
                      </tr>
                  </table>
                  <table id="tableText" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1" <c:choose><c:when test="${bean.adType==3}"></c:when><c:otherwise>style="display:none;"</c:otherwise></c:choose>>
                      <tr class="tdbg">
                          <td><strong>广告内容设置--文本</strong></td>
                      </tr>
                      <tr class="tdbg">
                          <td>
							<textarea id="txt_adIntro1" rows="12" cols="80" class="inputtext"><c:if test="${bean.adType==3}">${bean.adIntro}</c:if></textarea>
                          </td>
                      </tr>
                  </table>
              </td>
          </tr>
          <tr class="tdbg">
              <td class="tdbgleft"><strong>权重：</strong></td>
              <td>
              	<table cellpadding="0" cellspacing="0" border="0">
			    	<tr>
			    		<td style="width:50px;">
			    			<input id="txt_priority" type="text" style="width:50px;" value="${bean.priority}" />
			    		</td>
			    		<td><div id="txt_priorityTip"></div></td>
			    	</tr>
					<tr>
			    		<td colspan="2">版位按权重（优先级、随机）显示时，此处设置的权重越大，该广告显示的机会就越多。</td>
			    	</tr>
			    </table>
              </td>
          </tr>
          <tr class="tdbg">
              <td class="tdbgleft"><strong>排序：</strong></td>
              <td>
              	<table cellpadding="0" cellspacing="0" border="0">
			    	<tr>
			    		<td>
			    			<input id="txt_sort" type="text" style="width:50px;" value="${bean.sort}"/>
			    		</td>
			    		<td><div id="txt_sortTip"></div></td>
			    	</tr>
			    </table>
              </td>
          </tr>
          <tr class="tdbg">
              <td class="tdbgleft"><strong>状态：</strong></td>
              <td class="radioMiddle">
              	<input id="rb_NoPassed" type="radio" name="rb_Passed" class="valignMiddle" <c:if test="${!bean.passed}">checked="checked"</c:if>/><span class="valignMiddle">待审核</span>
              	<input id="rb_Passed" type="radio" name="rb_Passed" class="valignMiddle" <c:if test="${bean.passed}">checked="checked"</c:if>/><span class="valignMiddle">终审通过</span>
              </td>
          </tr>
          <tr class="tdbg">
              <td class="tdbgleft"><strong>时间限制：</strong></td>
              <td class="radioMiddle">
              	<input id="rb_CloseTime" type="radio" name="rb_Time" class="valignMiddle" <c:if test="${!bean.openTimeLimit}">checked="checked"</c:if>/><span class="valignMiddle">关闭</span>
              	<input id="rb_OpenTime" type="radio" name="rb_Time" class="valignMiddle" <c:if test="${bean.openTimeLimit}">checked="checked"</c:if>/><span class="valignMiddle">开启</span>
                <span class="cautiontitle">如果启用时间限制，会按以下时间段进行显示！</span>
                <div id="div_Time" <c:choose><c:when test="${bean.openTimeLimit}">style="padding-top:5px;display:block;"</c:when><c:otherwise>style="padding-top:5px;display:none;"</c:otherwise></c:choose>>
                	<table cellpadding="0" cellspacing="0" border="0">
                		<tr>
                			<td>
							开始时间：<input type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" id="txt_startTime" value="<fmt:formatDate value="${bean.startTime}" type="date" pattern="yyyy-MM-dd HH:mm"/>"/>
                			</td>
                		</tr>
                		<tr>
                			<td>
                			 结束时间：<input type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" id="txt_endTime" value="<fmt:formatDate value="${bean.endTime}" type="date" pattern="yyyy-MM-dd HH:mm"/>"/>
                			</td>
                		</tr>
                	</table>
                </div>
              </td>
          </tr>
     </table>
     <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
        <tr>
            <td align="center">
                <input type="button" class="inputbutton" id="bnt_update" value="修改">&nbsp;
                <input type="button" class="inputbutton" id="bnt_close" value="关闭" >
             </td>
        </tr>
     </table>
      </c:otherwise>
     </c:choose>
    </form>
	</body>
<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/admin/common/date/WdatePicker.js" type="text/javascript"></script>
</html>