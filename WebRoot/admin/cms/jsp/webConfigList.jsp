<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>网站配置列表</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
		var reqMethod = '${method}';
    	</script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/cms/js/webConfigList.js"></script>
	</head>
	<body class="mainContent">
		<span class="localSpan">后台管理<span> &gt;&gt; </span><span> 内容管理 </span><span>&gt;&gt; </span><span>网站配置列表 </span></span>
	    <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
	        <tr class="topbottom">
	            <td colspan="10">
	            	<input id="webConfig_Add" class="topinput topinput_add" type="button" value="新建"/>
	            	<input id="webConfig_Update" class="topinput topinput_edit" type="button" value="修改"/>
	            	<input id="webConfig_Del" class="topinput topinput_del" type="button" value="删除选定选"/>
	            	<input id="webConfig_SearchText" class="inputtext_public" type="text" alt="请输入网站名称" style="width:150px;">
	            	<input id="webConfig_Search" class="topinput topinput_search" type="button" value="搜索"/>
	            </td>
	        </tr>
	        <c:choose>
		    	<c:when test="${pageBean.totalRecordNum>0}">
			        <tr class="title">
			            <th style="width:15%;">网站名称</th>
			            <th style="width:15%;">所属公司</th>
			            <th style="width:10%;">网站网址</th>
			            <th style="width:12%;">网站logo</th>
			            <th style="width:12%;">版权</th>
			            <th style="width:14%;">网站描述</th>
			            <th style="width:12%;">创建时间</th>
			            <th style="width:10%;">创建者</th>
			        </tr>
			        <tbody id="tbody_DataList">
			        <c:forEach var="list" items="${pageBean.dataList}">
			        <tr class="tdbg" value="${list.siteId}">
			            <td align="center"><a id="detail_${list.siteId}" name="${list.siteId}" href="javascript:void(0);">${list.siteName}</a></td>
			            <td align="center">${list.companyName}</td>
			            <td align="left">${list.siteUrl}</td>
			            <td align="center">
			            <c:if test="${!empty list.logoUrl}">
			            <img src="${list.logoUrl}" alt="logo"/>
						</c:if>
			            </td>
			            <td align="left">${list.copyright}</td>
			            <td align="left">${list.metaDescription}</td>
			            <td align="center"><fmt:formatDate value="${list.createTime}" type="date" pattern="yyyy-MM-dd HH:mm"/></td>
			            <td align="center">${list.createUser}</td>
			        </tr>
			        </c:forEach>
			        </tbody>
			        <tr align="right" class="tdbg" style="height:28px;">
			        	<td colspan="10">
			        		<c:import url="pageCurrent.jsp">
			        			<c:param name="totalRecordNum" value="${pageBean.totalRecordNum}"></c:param>
			        			<c:param name="pageRecordNum" value="${pageBean.pageRecordNum}"></c:param>
					        </c:import>
			        	</td>
			        </tr>
		        </c:when>
	     		<c:otherwise>
		     		<tr class="tdbg" style="height:80px;">
			             <td align="center">
			                  <strong class="valignMiddle">没有任何数据！</strong>
			             </td>
		            </tr>
	     		</c:otherwise>
      		</c:choose>
     </table>
	</body>
<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
</html>