<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>会员详情</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
	</head>
	<body>
		<table border="0" cellpadding="0" cellspacing="0" width="325px">
	        <tr align="center">
	            <td id="td_tab1" class="titlemouseover">基本信息</td>
	            <td style="width:2px"></td>
	            <td id="td_tab2" class="tabtitle">栏目选项</td>
	            <td style="width:2px"></td>
	            <td id="td_tab3" class="tabtitle">个人信息</td>
	            <td style="width:2px"></td>
	            <td id="td_tab4" class="tabtitle">业务信息</td>
	        </tr>
	    	</table>
			<table id="tb_Pannel1" border="0" cellpadding="2" cellspacing="1" class="tableWrap dispalyTable" width="100%" style="border-bottom:0px;">
		        <tr class="tdbg">
		            <td class="tdbgleft" style="width:150px;" align="right">
		                <strong>所属会员组 ：&nbsp;</strong>
		            </td>
		            <td>${bean.groupName}</td>
		        </tr>
				<tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>用户名：&nbsp;</strong>
		            </td>
		            <td>
						${bean.userName}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>真是姓名：&nbsp;</strong>
		            </td>
		            <td>
		            ${bean.trueName}
		            </td>
		        </tr>
		        <tr class="tdbg">
		        	<td class="tdbgleft" align="right"><strong>头像：&nbsp;</strong></td>
		        	<td>
		        		<img id="img_UserFace" src="${bean.userFace}" alt="用户头像" <c:if test="${bean.faceWidth>0}">width="${bean.faceWidth}px"</c:if> <c:if test="${bean.faceHeight>0}">height="${bean.faceHeight}px"</c:if>/>
		        	</td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>Email地址：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.email}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>是否容许修改密码：&nbsp;</strong>
		            </td>
		            <td>
		            	<c:if test="${bean.enableResetPassword}">容许</c:if>
		            	<c:if test="${!bean.enableResetPassword}">不容许</c:if>
		            </td>
		        </tr>
		         <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>登录次数：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.loginTimes}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>最后登录时间：&nbsp;</strong>
		            </td>
		            <td>
		            	<c:if test="${!empty bean.lastLoginTime}"><fmt:formatDate value="${bean.lastLoginTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss"/></c:if>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>最后登录IP：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.lastLoginIP}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>提示问题：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.answer}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>提示答案：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.question}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>注册时间：&nbsp;</strong>
		            </td>
		            <td>
						<c:if test="${!empty bean.regTime}"><fmt:formatDate value="${bean.regTime}" type="date" pattern="yyyy-MM-dd HH:mm:ss"/></c:if>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>账号状态：&nbsp;</strong>
		            </td>
		            <td>
					<c:if test="${bean.status==0}"><span style="color:#00A600">正常</span></c:if>
					<c:if test="${bean.status==1}"><span style="color:#FF0000">锁定</span></c:if>
					<c:if test="${bean.status==2}"><span style="color:#FF0000">未通过邮件验证</span></c:if>
					<c:if test="${bean.status==4}"><span style="color:#FF0000">未通过管理员认证</span></c:if>
					<c:if test="${bean.status==8}"><span style="color:#FF0000">未通过手机验证</span></c:if>
		            </td>
		        </tr>
		     </table>
		     <table id="tb_Pannel2" border="0" cellpadding="2" cellspacing="1" class="tableWrap diplayNone" width="100%" style="border-bottom:0px;">
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right" style="width:150px;">
		                <strong>通信地址：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.province}-${bean.city}-${bean.district}-${bean.address}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>办公电话：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.officePhone}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>移动电话：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.mobile}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>住宅电话：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.homePhone}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>QQ号码：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.qq}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>个人主页：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.homepage}
		            </td>
		        </tr>
		     </table>
			 <table id="tb_Pannel3" border="0" cellpadding="2" cellspacing="1" class="tableWrap diplayNone" width="100%" style="border-bottom:0px;">
			 	<tr class="tdbg">
		            <td class="tdbgleft" align="right" style="width:150px;">
		                <strong>出生日期：&nbsp;</strong>
		            </td>
		            <td>
		            	<c:if test="${!empty bean.birthday}"><fmt:formatDate value="${bean.birthday}" type="date" pattern="yyyy-MM-dd HH:mm"/></c:if>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>证件：&nbsp;</strong>
		            </td>
		            <td>
		            	<c:if test="${bean.cardType==1}">身份证</c:if>
		            	<c:if test="${bean.cardType==2}">护照</c:if>
		            	<c:if test="${bean.cardType==3}">驾驶证</c:if>
		            	<c:if test="${bean.cardType==4}">港澳通行证</c:if>
		            	<c:if test="${bean.cardType==99}">其他</c:if>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>证件类型：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.idCard}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>籍贯：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.nativePlace}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>民族：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.nation}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>性别：&nbsp;</strong>
		            </td>
		            <td>
		            <c:if test="${bean.sex==1}">男</c:if>
		            <c:if test="${bean.sex==2}">女</c:if>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>婚姻状况：&nbsp;</strong>
		            </td>
		            <td>
		            <c:if test="${bean.marriage==1}">保密</c:if>
		            <c:if test="${bean.marriage==2}">未婚</c:if>
		            <c:if test="${bean.marriage==3}">已婚</c:if>
		            <c:if test="${bean.marriage==4}">离异</c:if>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>学历：&nbsp;</strong>
		            </td>
		            <td>
		            	<c:if test="${!empty bean.education&&bean.education eq '小学'}">小学</c:if>
	                    <c:if test="${!empty bean.education&&bean.education eq '初中'}">初中</c:if>
	                    <c:if test="${!empty bean.education&&bean.education eq '高中'}">高中</c:if>
	                    <c:if test="${!empty bean.education&&bean.education eq '中专'}">中专</c:if>
	                    <c:if test="${!empty bean.education&&bean.education eq '大专'}">大专</c:if>
	                    <c:if test="${!empty bean.education&&bean.education eq '本科'}">本科</c:if>
	                    <c:if test="${!empty bean.education&&bean.education eq '硕士'}">硕士</c:if>
	                    <c:if test="${!empty bean.education&&bean.education eq '博士'}">博士</c:if>
	                    <c:if test="${!empty bean.education&&bean.education eq '博士后'}">博士后</c:if>
	                    <c:if test="${!empty bean.education&&bean.education eq '其他'}">其他</c:if>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>毕业学校：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.graduateFrom}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>生活爱好：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.interestsOfLife}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>文化爱好：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.interestsOfCulture}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>娱乐休闲爱好：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.interestsOfAmusement}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>体育爱好：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.interestsOfSport}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>其他爱好：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.interestsOfOther}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>月收入：&nbsp;</strong>
		            </td>
		            <td>
				       <c:if test="${!empty bean.income&&bean.income eq '1000元以下'}">1000元以下</c:if>
				       <c:if test="${!empty bean.income&&bean.income eq '1000-3000元'}">1000-3000元</c:if>
				       <c:if test="${!empty bean.income&&bean.income eq '3000-6000元'}">3000-6000元</c:if>
				       <c:if test="${!empty bean.income&&bean.income eq '6000-10000元'}">6000-10000元</c:if>
				       <c:if test="${!empty bean.income&&bean.income eq '10000元以上'}">10000元以上</c:if>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>家庭情况：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.family}
		            </td>
		        </tr>
			 </table>
			 <table id="tb_Pannel4" border="0" cellpadding="2" cellspacing="1" class="tableWrap diplayNone" width="100%" style="border-bottom:0px;">
			 	<tr class="tdbg">
		            <td class="tdbgleft" align="right" style="width:150px;">
		                <strong>单位名称：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.company}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>所属部门：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.department}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>职位：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.position}
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>单位地址：&nbsp;</strong>
		            </td>
		            <td>
		            	${bean.companyAddress}
		            </td>
		        </tr>
			 </table>
			<table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
		        <tr>
		            <td align="center">
		                <input type="button" class="inputbutton" id="bnt_close" value="关闭" >
		             </td>
		        </tr>
        	</table>
	</body>
<script type="text/javascript">
$(document).ready(function(){
	$('#bnt_close').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('1');
	});
	beautifyBottomButton(['bnt_close']);
	pageTab({'td_tab1':'tb_Pannel1','td_tab2':'tb_Pannel2','td_tab3':'tb_Pannel3','td_tab4':'tb_Pannel4'});
});
</script>
</html>