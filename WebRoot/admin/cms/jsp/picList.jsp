<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>图片列表</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/dialog/plugins/iframeTools.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/cms/js/picList.js" type="text/javascript"></script>
	</head>
	<body class="mainContent">
		<span class="localSpan">后台管理<span> &gt;&gt; </span><span> 内容管理 </span><span>&gt;&gt; </span><span>图片列表 </span></span>
	    <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr align="center">
                <td id="td_tab1" class="titlemouseover" style="width:80px;">
                    所有内容</td>
                <td style="width:2px"></td>
                <td id="td_tab2" class="tabtitle" style="width:80px;">
                    &nbsp;草稿&nbsp;</td>
                <td style="width:2px"></td>
                <td id="td_tab3" class="tabtitle" style="width:80px;">
                    待审核</td>
                <td style="width:2px"></td>
                <td id="td_tab4" class="tabtitle" style="width:80px;">
                    已审核</td>
                <td style="width:2px"></td>
                <td id="td_tab5" class="tabtitle" style="width:80px;">
                    回收站</td>
                <td>&nbsp;</td>
               <td align="right">
                   <select id="ddl_ChannelList">
                   	<option value="0" selected="selected">--请选择栏目--</option>
					<c:forEach var="list" items="${channels}">
					<option value="${list.channelId}" <c:if test="${channelId==list.channelId}">selected="selected"</c:if>>${list.channelName}</option>
					</c:forEach>
                   </select>
                   <select id="select_SearchConditions">
                   	<option value="CommonModelID_DESC" <c:if test="${orderType eq 'CommonModelID_DESC'}">selected="selected"</c:if>>按ID降序</option>
					<option value="CommonModelID_ASC" <c:if test="${orderType eq 'CommonModelID_ASC'}">selected="selected"</c:if>>按ID升序</option>
					<option value="UpdateTime_DESC" <c:if test="${orderType eq 'UpdateTime_DESC'}">selected="selected"</c:if>>按更新时间降序</option>
					<option value="UpdateTime_ASC" <c:if test="${orderType eq 'UpdateTime_ASC'}">selected="selected"</c:if>>按更新时间升序</option>
					<option value="Priority_DESC" <c:if test="${orderType eq 'Priority_DESC'}">selected="selected"</c:if>>按推荐级别降序</option>
					<option value="Priority_ASC" <c:if test="${orderType eq 'Priority_ASC'}">selected="selected"</c:if>>按推荐级别升序</option>
					<option value="DayHits_DESC" <c:if test="${orderType eq 'DayHits_DESC'}">selected="selected"</c:if>>按日点击数降序</option>
					<option value="DayHits_ASC" <c:if test="${orderType eq 'DayHits_ASC'}">selected="selected"</c:if>>按日点击数升序</option>
					<option value="WeekHits_DESC" <c:if test="${orderType eq 'WeekHits_DESC'}">selected="selected"</c:if>>按周点击数降序</option>
					<option value="WeekHits_ASC" <c:if test="${orderType eq 'WeekHits_ASC'}">selected="selected"</c:if>>按周点击数升序</option>
					<option value="MonthHits_DESC" <c:if test="${orderType eq 'MonthHits_DESC'}">selected="selected"</c:if>>按月点击数降序</option>
					<option value="MonthHits_ASC" <c:if test="${orderType eq 'MonthHits_ASC'}">selected="selected"</c:if>>按月点击数升序</option>
					<option value="Hits_DESC" <c:if test="${orderType eq 'Hits_DESC'}">selected="selected"</c:if>>按总点击数降序</option>
					<option value="Hits_ASC" <c:if test="${orderType eq 'Hits_ASC'}">selected="selected"</c:if>>按总点击数升序</option>
                   </select>
               </td>
            </tr>
       </table>
        <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
        	<c:choose>
        	<c:when test="${status==999}">
        	 <tr class="topbottom">
	            <td colspan="10">
	            	<input id="pic_Add" class="topinput topinput_add" type="button" value="新建"/>
	            	<input id="pic_Update" class="topinput topinput_edit" type="button" value="修改"/>
	            	<input id="pic_Recycle" class="topinput topinput_recycle" type="button" value="放入回收站"/>
	            	<input id="pic_Del" class="topinput topinput_del" type="button" value="彻底删除"/>
	            	<select id="select_SearchKey">
	            		<option value="CommonModelId">ID</option>
						<option value="Title">标题内容</option>
						<option value="Inputer">录入者</option>
						<option value="Hits">点击数</option>
						<option value="DayHits">日点击数</option>
						<option value="WeekHits">周点击数</option>
						<option value="MonthHits">月点击数</option>
                    </select>
	            	<input id="pic_SearchText" class="inputtext_public" type="text" style="width:150px;color:#000;">
	            	<input id="pic_Search" class="topinput topinput_search" type="button" value="搜索"/>
	            </td>
	        </tr>
	        </c:when>
	        <c:when test="${status==-1}">
	        <tr class="topbottom">
	            <td colspan="10">
	            	<input id="pic_Update" class="topinput topinput_edit" type="button" value="修改"/>
	            	<input id="pic_WaitPass" class="topinput topinput_wait" type="button" value="待审核"/>
	            	<input id="pic_Passed" class="topinput topinput_passed" type="button" value="审核通过"/>
	            	<input id="pic_Recycle" class="topinput topinput_recycle" type="button" value="放入回收站"/>
	            	<input id="pic_Del" class="topinput topinput_del" type="button" value="彻底删除"/>
	            	<select id="select_SearchKey">
	            		<option value="CommonModelId">ID</option>
						<option value="Title">标题内容</option>
						<option value="Inputer">录入者</option>
						<option value="Hits">点击数</option>
						<option value="DayHits">日点击数</option>
						<option value="WeekHits">周点击数</option>
						<option value="MonthHits">月点击数</option>
                    </select>
	            	<input id="pic_SearchText" class="inputtext_public" type="text" style="width:150px;color:#000;">
	            	<input id="pic_Search" class="topinput topinput_search" type="button" value="搜索"/>
	            </td>
	        </tr>
	        </c:when>
	        <c:when test="${status==0}">
	        <tr class="topbottom">
	            <td colspan="10">
	            	<input id="pic_Update" class="topinput topinput_edit" type="button" value="修改"/>
	            	<input id="pic_Draft" class="topinput topinput_draft" type="button" value="设为草稿"/>
	            	<input id="pic_Passed" class="topinput topinput_passed" type="button" value="审核通过"/>
	            	<input id="pic_Recycle" class="topinput topinput_recycle" type="button" value="放入回收站"/>
	            	<input id="pic_Del" class="topinput topinput_del" type="button" value="彻底删除"/>
	            	<select id="select_SearchKey">
	            		<option value="CommonModelId">ID</option>
						<option value="Title">标题内容</option>
						<option value="Inputer">录入者</option>
						<option value="Hits">点击数</option>
						<option value="DayHits">日点击数</option>
						<option value="WeekHits">周点击数</option>
						<option value="MonthHits">月点击数</option>
                    </select>
	            	<input id="pic_SearchText" class="inputtext_public" type="text" style="width:150px;color:#000;">
	            	<input id="pic_Search" class="topinput topinput_search" type="button" value="搜索"/>
	            </td>
	        </tr>
	        </c:when>
	        <c:when test="${status==99}">
	        <tr class="topbottom">
	            <td colspan="10">
	            	<input id="pic_Update" class="topinput topinput_edit" type="button" value="修改"/>
	            	<input id="pic_CancelPassed" class="topinput topinput_cancelpassed" type="button" value="取消审核"/>
	            	<input id="pic_Recycle" class="topinput topinput_recycle" type="button" value="放入回收站"/>
	            	<input id="pic_Del" class="topinput topinput_del" type="button" value="彻底删除"/>
	            	<select id="select_SearchKey">
	            		<option value="CommonModelId">ID</option>
						<option value="Title">标题内容</option>
						<option value="Inputer">录入者</option>
						<option value="Hits">点击数</option>
						<option value="DayHits">日点击数</option>
						<option value="WeekHits">周点击数</option>
						<option value="MonthHits">月点击数</option>
                    </select>
	            	<input id="pic_SearchText" class="inputtext_public" type="text" style="width:150px;color:#000;">
	            	<input id="pic_Search" class="topinput topinput_search" type="button" value="搜索"/>
	            </td>
	        </tr>
	        </c:when>
	        <c:otherwise>
	         <tr class="topbottom">
	            <td colspan="10">
	            	<input id="pic_Restore" class="topinput topinput_restore" type="button" value="还原"/>
	            	<input id="pic_Del" class="topinput topinput_del" type="button" value="彻底删除"/>
	            	<select id="select_SearchKey">
	            		<option value="CommonModelId">ID</option>
						<option value="Title">标题内容</option>
						<option value="Inputer">录入者</option>
						<option value="Hits">点击数</option>
						<option value="DayHits">日点击数</option>
						<option value="WeekHits">周点击数</option>
						<option value="MonthHits">月点击数</option>
                    </select>
	            	<input id="pic_SearchText" class="inputtext_public" type="text" style="width:150px;color:#000;">
	            	<input id="pic_Search" class="topinput topinput_search" type="button" value="搜索"/>
	            </td>
	        </tr>
	        </c:otherwise>
	        </c:choose>

	        <c:choose>
	    	<c:when test="${pageBean.totalRecordNum>0}">
            <tr class="title" style="height:25px;">
               <th scope="col" style="width: 3%; cursor: col-resize;"><input id="cb_CheckedAll" type="checkbox"/></th>
               <th scope="col" style="width: 4%; cursor: col-resize;">ID</th>
               <th scope="col" style="width: 26%; cursor: col-resize;">标题</th>
               <th scope="col" style="width: 10%; cursor: col-resize;">录入者</th>
               <th scope="col" style="width: 8%; cursor: col-resize;">点击数</th>
               <th scope="col" style="width: 8%; cursor: col-resize;">推荐级别</th>
               <th scope="col" style="width: 10%; cursor: col-resize;">优先级</th>
               <th scope="col" style="width: 9%; cursor: col-resize;">状态</th>
               <th scope="col" style="width: 10%; cursor: col-resize;">最后点击时间</th>
               <th scope="col" style="width: 12%; cursor: col-resize;">录入时间</th>
            </tr>
             <tbody id="tbody_DataList">
             	<c:forEach var="list" items="${pageBean.dataList}">
                <tr class="tdbg" style="height:25px;">
                   <td align="center"><input id="cb_" type="checkbox" value="${list.commonModelId}"/></td>
                   <td align="center">${list.commonModelId}</td>
                   <td align="left"><span class="valignMiddle"><img src="<%=request.getContextPath()%>/admin/imgs/blue/photo.gif" alt="" /></span><a onclick="picDetail('${list.commonModelId}');">${list.title}</a></td>
                   <td align="center">${list.inputer}</td>
                   <td align="center">${list.hits}</td>
                   <td align="center">${list.eliteLevel}</td>
                   <td align="center">${list.priority}</td>
                   <td align="center">
	                   <c:choose>
	                   	<c:when test="${list.status==-3}"><span style="color:#FF0000">已删除</span></c:when>
	                   	<c:when test="${list.status==-1}"><span style="color:#2828FF">草稿</span></c:when>
	                   	<c:when test="${list.status==0}"><span style="color:#000000">待审核</span></c:when>
	                   	<c:otherwise><span style="color:#00A600">已审核</span></c:otherwise>
	                   </c:choose>
                   </td>
                   <td align="center">${list.lastHitTime}</td>
                   <td align="center">${list.createTime}</td>
                 </tr>
                 </c:forEach>
            </tbody>
            <tr align="right" class="tdbg" style="height:28px;">
                <td colspan="10">
                   <c:import url="pageCurrent.jsp">
			        <c:param name="totalRecordNum" value="${pageBean.totalRecordNum}"></c:param>
			        <c:param name="pageRecordNum" value="${pageBean.pageRecordNum}"></c:param>
				   </c:import>
                </td>
            </tr>
            </c:when>
            <c:otherwise>
            	<tr class="tdbg" style="height:80px;">
			        <td colspan="10" align="center">
			             <strong class="valignMiddle">没有任何数据！</strong>
			        </td>
		        </tr>
            </c:otherwise>
         	</c:choose>
        </table>
	</body>
</html>