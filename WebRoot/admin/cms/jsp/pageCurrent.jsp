<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
共&nbsp;<span style="font-weight: bold;">${param.totalRecordNum}</span>&nbsp;条记录&nbsp;&nbsp;<span style="font-weight: bold;">${param.pageRecordNum}</span>条/页&nbsp;&nbsp;<a id="pageCurrent_FirstPage" href="javascript:void(0);">首页</a>&nbsp;<a id="pageCurrent_PrePage" href="javascript:void(0);">上一页</a>&nbsp;<a id="pageCurrent_NextPage" href="javascript:void(0);">下一页</a>&nbsp;<a id="pageCurrent_LastPage" href="javascript:void();">尾页</a>&nbsp;&nbsp;页次：<span id="pageCurrent_CurrentPageNum" style="font-weight: bold; color: #FF0000;"></span>/<span id="pageCurrent_TotalPageNum" style="font-weight: bold;"></span>页&nbsp;&nbsp;转到第<input id="currentPage_GoPage" size="2" type="text" />页&nbsp;
<script type="text/javascript">
    $(document).ready(function(){
    	var _intTotalRecordNum = 0,_intpageRecordNum = 0,_intCurrentPageNum = 1;
    	var totalRecordNum = '${param.totalRecordNum}',pageRecordNum = '${param.pageRecordNum}';
    	if(''!=totalRecordNum){
    		_intTotalRecordNum = parseInt(totalRecordNum);
    		_intpageRecordNum = parseInt(pageRecordNum);
    	}
    	var _intTotalPageNum = Math.ceil(_intTotalRecordNum/_intpageRecordNum);
    	parseUri.init();
    	var currentPageNum = parseUri.getParam('pageNum');
    	if('undefined' != typeof(currentPageNum)&&'' != currentPageNum){
    		_intCurrentPageNum = parseInt(currentPageNum);
    	}
		$('#pageCurrent_CurrentPageNum').text(_intCurrentPageNum);
		$('#pageCurrent_TotalPageNum').text(_intTotalPageNum);
    	if(_intTotalPageNum<=1){
    		$('#pageCurrent_FirstPage').attr('disabled','disabled');
    		$('#pageCurrent_PrePage').attr('disabled','disabled');
    		$('#pageCurrent_NextPage').attr('disabled','disabled');
    		$('#pageCurrent_LastPage').attr('disabled','disabled');
    	}
    	else {
    		if(_intCurrentPageNum==1){
    			$('#pageCurrent_FirstPage').attr('disabled','disabled');
    			$('#pageCurrent_PrePage').attr('disabled','disabled');
    			$('#pageCurrent_NextPage').removeAttr('disabled').click(function(){
    				window.location.href = parseUri.setParam('pageNum',_intCurrentPageNum+1);
    			});
    			$('#pageCurrent_LastPage').removeAttr('disabled').click(function(){
    				window.location.href = parseUri.setParam('pageNum',_intTotalPageNum);
    			});
    		}
    		else if(_intCurrentPageNum==_intTotalPageNum){
    			$('#pageCurrent_FirstPage').removeAttr('disabled').click(function(){
    				window.location.href = parseUri.setParam('pageNum',1);
    			});
    			$('#pageCurrent_PrePage').removeAttr('disabled').click(function(){
    				window.location.href = parseUri.setParam('pageNum',_intCurrentPageNum-1);;
    			});
    			$('#pageCurrent_NextPage').attr('disabled','disabled');
    			$('#pageCurrent_LastPage').attr('disabled','disabled');
    		}
    		else{
    			$('#pageCurrent_FirstPage').removeAttr('disabled').click(function(){
    				window.location.href = parseUri.setParam('pageNum',1);
    			});
    			$('#pageCurrent_PrePage').removeAttr('disabled').click(function(){
    				window.location.href = parseUri.setParam('pageNum',_intCurrentPageNum-1);;
    			});
    			$('#pageCurrent_NextPage').removeAttr('disabled').click(function(){
    				window.location.href = parseUri.setParam('pageNum',_intCurrentPageNum+1);
    			});
    			$('#pageCurrent_LastPage').removeAttr('disabled').click(function(){
    				window.location.href = parseUri.setParam('pageNum',_intTotalPageNum);
    			});
    		}
    	};
    	$('#currentPage_GoPage').blur(function(){
    		var goPageNum = jQuery.trim($(this).val());
    		if(!/^[0-9]*[1-9][0-9]*$/.test(goPageNum)){
    			$(this).val('');
    			return;
    		}
    		if(parseInt(goPageNum)>_intTotalPageNum){
    			alert('您输入的页数大于总页数！');
    			return;
    		}
    		window.location.href = parseUri.setParam('pageNum',goPageNum);
    	});
    });
</script>