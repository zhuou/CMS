<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>网站信息详情</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			beautifyBottomButton(['returnCompany']);
			pageTab({'td_tab1':'tb_Pannel1','td_tab2':'tb_Pannel2'});
			$('#returnCompany').click(function(){
				window.top.frames['iframeMenuContent'].callBackResult('0');
			});
		});
		</script>
	</head>
	<body class="mainContent">
	    <table id="tb_Pannel2" class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
	    <tr class="tdbg">
            <td class="tdbgleft" style="width:150px;" align="right"><strong>公司ID ：&nbsp;</strong></td>
            <td>${bean.companyId}</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:150px;" align="right"><strong>公司名称 ：&nbsp;</strong></td>
            <td>${bean.companyName}</td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司简称 ：&nbsp;</strong></td>
            <td>${bean.referred}</td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司性质 ：&nbsp;</strong></td>
            <td>${bean.companyPropertie}</td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司行业 ：&nbsp;</strong></td>
            <td>${bean.companyIndustry}</td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司规模 ：&nbsp;</strong></td>
            <td>${bean.companyScale}</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>所在地区 ：&nbsp;</strong></td>
            <td>${bean.province}-${bean.city}-${bean.district}</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>详细地址 ：&nbsp;</strong></td>
            <td>${bean.address}</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>邮政编码 ：&nbsp;</strong></td>
            <td>${bean.zipCode}</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>联系人 ：&nbsp;</strong></td>
            <td>${bean.contact}</td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司联系电话 ：&nbsp;</strong></td>
            <td>${bean.telephone}</td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>Email ：&nbsp;</strong></td>
            <td>${bean.email}</td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司手机 ：&nbsp;</strong></td>
            <td>${bean.mobile}</td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>公司简介 ：&nbsp;</strong></td>
            <td>${bean.companyIntr}</td>
        </tr>
         <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>备注 ：&nbsp;</strong></td>
            <td>${bean.remark}</td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" align="right"><strong>创建时间 ：&nbsp;</strong></td>
            <td>${bean.createTime}</td>
        </tr>
     </table>
      <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	              <input id="returnCompany" type="button" class="inputbutton" value=" 关闭 "/>
	             </td>
	        </tr>
        </table>
	</body>
</html>