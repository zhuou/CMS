<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>评论列表</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	</script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/cms/js/commentList.js"></script>
	</head>
	<body class="mainContent">
		<span class="localSpan">后台管理<span> &gt;&gt; </span><span> 内容管理 </span><span>&gt;&gt; </span><span>评论列表 </span></span>
	    <table class="tableWrap" border="0" width="100%" cellpadding="2" cellspacing="1">
	        <tr class="topbottom">
	            <td colspan="11">
	            	<input id="comment_Detail" class="topinput topinput_detail" type="button" value="详情"/>
	            	<input id="comment_Del" class="topinput topinput_del" type="button" value="删除选定选"/>
	            	<input id="comment_SearchText" class="inputtext_public" type="text" alt="请输入评论关键字" style="width:150px;">
	            	<input id="comment_Search" class="topinput topinput_search" type="button" value="搜索"/>
	            </td>
	        </tr>
	        <c:choose>
		    	<c:when test="${pageBean.totalRecordNum>0}">
			        <tr class="title">
			        	<th style="width:4%;"><input id="cb_CheckedAll" type="checkbox"/></th>
			        	<th style="width:5%;">评论ID</th>
			            <th style="width:15%;">评论标题</th>
			            <th style="width:8%;">评论用户</th>
			            <th style="width:18%;">评论内容</th>
			            <th style="width:10%;">是否精华</th>
			            <th style="width:10%;">是否公开</th>
			            <th style="width:7%;">支持数</th>
			            <th style="width:7%;">中立数</th>
			            <th style="width:7%;">反对数</th>
			            <th style="width:9%;">评论时间</th>
			        </tr>
			        <tbody id="tbody_DataList">
			        <c:forEach var="list" items="${pageBean.dataList}">
			        <tr class="tdbg">
			        	<td align="center"><input type="checkbox" value="${list.commentId}"/></td>
			        	<td align="center">${list.commentId}</td>
			            <td align="center"><a href="<%=request.getContextPath()%>/security/commentDetail.html?id=${list.commentId}">${list.commentTitle}</a></td>
			            <td align="center">${list.userName}</td>
			            <td align="center">${list.commentContent}</td>
			            <td align="center">
			             <c:choose>
		    				<c:when test="${list.isElite}">
		    					<span style="color:#FF0000">精华</span>&nbsp;<a name="bnt_cannelElite" itemid="${list.commentId}">取消精华</a>
		    				</c:when>
		    				<c:otherwise>
		    					普通&nbsp;<a name="bnt_setElite" itemid="${list.commentId}">设置精华</a>
		    				</c:otherwise>
		    			 </c:choose>
			            </td>
			            <td align="center">
			              <c:choose>
		    				<c:when test="${list.isPrivate}">
		    					<span style="color:#FF0000">不公开</span>&nbsp;<a name="bnt_setPublic" itemid="${list.commentId}">设为公开</a>
		    				</c:when>
		    				<c:otherwise>
		    					<span style="color:#00A600">公开</span>&nbsp;<a name="bnt_cannelPublic" itemid="${list.commentId}">取消公开</a>
		    				</c:otherwise>
		    			  </c:choose>
			            </td>
			            <td align="center">${list.agree}</td>
			            <td align="center">${list.neutral}</td>
			            <td align="center">${list.oppose}</td>
			            <td align="center">${list.createTime}</td>
			        </tr>
			        </c:forEach>
			        </tbody>
			        <tr align="right" class="tdbg" style="height:28px;">
			        	<td colspan="11">
			        		<c:import url="pageCurrent.jsp">
			        			<c:param name="totalRecordNum" value="${pageBean.totalRecordNum}"></c:param>
			        			<c:param name="pageRecordNum" value="${pageBean.pageRecordNum}"></c:param>
					        </c:import>
			        	</td>
			        </tr>
		        </c:when>
	     		<c:otherwise>
		     		<tr class="tdbg" style="height:80px;">
			             <td align="center" colspan="11">
			                  <strong class="valignMiddle">没有任何数据！</strong>
			             </td>
		            </tr>
	     		</c:otherwise>
      		</c:choose>
     </table>
	</body>
</html>