<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>评论详情</title>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
		$(document).ready(function(){
			beautifyBottomButton(['bnt_back']);
			var _id = '${bean.commentId }';
			function updateEliteStatus(_status){
				var _url = contextPath+'/security/updateEliteStatus.html?id='+_id+'&status='+_status;
				$.getJSON(_url,function(data){
					if(data.code=='200'){
						window.location.href=window.location.href;
					}
					else{
						alert(data.content);
					}
				});
			}
			function updatePrivateStatus(_status){
				var _url = contextPath+'/security/updatePrivateStatus.html?id='+_id+'&status='+_status;
				$.getJSON(_url,function(data){
					if(data.code=='200'){
						window.location.href=window.location.href;
					}
					else{
						alert(data.content);
					}
				});
			}
			$('#bnt_cannelElite').click(function(){
				updateEliteStatus('false');
			});
			$('#bnt_setElite').click(function(){
				updateEliteStatus('true');
			});
			$('#bnt_cannelPrivate').click(function(){
				updatePrivateStatus('false');
			});
			$('#bnt_setPrivate').click(function(){
				updatePrivateStatus('true');
			});
		});
    	</script>
	</head>
	<body class="mainContent">
	<span class="localSpan">后台管理<span> &gt;&gt; </span><span>文章中心 </span><span>&gt;&gt; </span><span>评论详情 </span></span>
     <table border="0" cellpadding="2" cellspacing="1" class="tableWrap" width="100%">
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>评论ID ：&nbsp;</strong></td>
            <td>
            	${bean.commentId }
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>评论标题 ：&nbsp;</strong></td>
            <td>
            	${bean.commentTitle }
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>评论用户 ：&nbsp;</strong></td>
            <td>
            	${bean.userName }
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>评论内容 ：&nbsp;</strong></td>
            <td>
            	${bean.commentContent }
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>是否精华 ：&nbsp;</strong></td>
            <td>
             <c:choose>
   				<c:when test="${bean.isElite}">
   					<span style="color:#FF0000">精华</span>&nbsp;<a id="bnt_cannelElite">取消精华</a>
   				</c:when>
   				<c:otherwise>
   					普通&nbsp;<a id="bnt_setElite">设置精华</a>
   				</c:otherwise>
   			 </c:choose>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>是否公开 ：&nbsp;</strong></td>
            <td>
           	  <c:choose>
   				<c:when test="${bean.isPrivate}">
   					<span style="color:#00A600">公开</span>&nbsp;<a id="bnt_cannelPrivate">取消公开</a>
   				</c:when>
   				<c:otherwise>
   					<span style="color:#FF0000">不公开</span>&nbsp;<a id="bnt_setPrivate">设为公开</a>
   				</c:otherwise>
   			  </c:choose>
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>评论的支持数 ：&nbsp;</strong></td>
            <td>
            ${bean.agree }
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>评论的中立数 ：&nbsp;</strong></td>
            <td>
            	${bean.neutral }
            </td>
        </tr>
          <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>评论的反对数 ：&nbsp;</strong></td>
            <td>
            	${bean.oppose }
            </td>
        </tr>
        <tr class="tdbg">
            <td class="tdbgleft" style="width:200px;" align="right">
                <strong>用户IP ：&nbsp;</strong></td>
            <td>
            	${bean.ip }
            </td>
        </tr>
        <tr class="tdbg">
	       <td style="width: 200px;" align="right" class="tdbgleft">
	       <strong>评论时间：&nbsp;</strong>
	       </td>
	       <td>
				${bean.createTime }
	       </td>
	    </tr>
        <tr class="tdbg">
	        <td align="center" colspan="2">
	             <input type="button" class="inputbutton" id="bnt_back" value="返 回" >
	          </td>
	   </tr>
    </table>
	</body>
</html>