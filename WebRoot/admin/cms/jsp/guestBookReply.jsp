<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>留言回复页面</title>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	var reqMethod = '${method}';
    	var _guestBookId = '${bean.guestBookId}';
		window.UEDITOR_HOME_URL = '<%=request.getContextPath()%>';
		window.iframeName='${iframe}';
		if(window.iframeName==''){
			window.iframeName='iframeMenuContent';
		}
    	</script>
		<script src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js" type="text/javascript"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/date/WdatePicker.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/editor/ueditor.all.min.js" type="text/javascript"></script>
    	<script src="<%=request.getContextPath()%>/admin/common/editor/ueditor.config2.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/cms/js/guestBookReply.js" type="text/javascript"></script>
	</head>
	<body class="mainContent">
		<form id="guestBookReplyForm" name="guestBookReplyForm" action="#">
		<c:choose>
	    	<c:when test="${method eq 'add'}">
		<table border="0" cellpadding="2" cellspacing="1" class="tableWrap" width="100%">
            <tr class="tdbg">
                <td class="tdbgleft" style="width:200px;" align="right">
                <strong>留言标题：</strong></td>
	            <td>${bean.title}</td>
            </tr>
            <tr class="tdbg">
               <td class="tdbgleft" style="width:200px;" align="right">
                <strong>回复人：</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
	                	<tr>
		                	<td>
			                <input id="txt_replyName" type="text" style="width:150px;" value="${replyName}"/>
			                </td>
			                <td><div id="txt_replyNameTip"></div></td>
		                </tr>
		            </table>
	            </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>留言内容：</strong></td>
               <td>
               	<script id="txt_replyContent" type="text/plain"></script>
               </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>回复时间：</strong></td>
                <td>
                	<table cellpadding="0" cellspacing="0" border="0">
	                	<tr>
		                	<td>
			                <input id="txt_replyTime" type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" id="txt_updataTime" value="${date }"/><span style="color:#FF0000;">*</span>
			                </td>
			                <td><div id="txt_replyTimeTip"></div></td>
		                </tr>
		            </table>
                </td>
            </tr>
       </table>
       <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bnt_add" value="新 增">&nbsp;
	                <input type="button" class="inputbutton" id="bnt_back" value="关闭" >
	             </td>
	        </tr>
        </table>
        </c:when>
     	<c:otherwise>
				<table border="0" cellpadding="2" cellspacing="1" class="tableWrap" width="100%">
            <tr class="tdbg">
                <td class="tdbgleft" style="width:200px;" align="right">
                <strong>留言标题：</strong></td>
	            <td>${bean.title}<input id="txt_replyId" type="hidden" value="${bean.replyId}"/></td>
            </tr>
            <tr class="tdbg">
               <td class="tdbgleft" style="width:200px;" align="right">
                <strong>回复人：</strong></td>
	            <td>
	            	<table cellpadding="0" cellspacing="0" border="0">
	                	<tr>
		                	<td>
			                <input id="txt_replyName" type="text" style="width:150px;" value="${bean.replyName}"/>
			                </td>
			                <td><div id="txt_replyNameTip"></div></td>
		                </tr>
		            </table>
	            </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>留言内容：</strong></td>
               <td>
               	<script id="txt_replyContent" type="text/plain">${bean.replyContent}</script>
               </td>
            </tr>
            <tr class="tdbg">
                <td class="tdbgleft" align="right"><strong>回复时间：</strong></td>
                <td>
                	<table cellpadding="0" cellspacing="0" border="0">
	                	<tr>
		                	<td>
			                <input id="txt_replyTime" type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" id="txt_updataTime" value="${bean.replyTime}"/><span style="color:#FF0000;">*</span>
			                </td>
			                <td><div id="txt_replyTimeTip"></div></td>
		                </tr>
		            </table>
                </td>
            </tr>
       </table>
       <table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
	        <tr>
	            <td align="center">
	                <input type="button" class="inputbutton" id="bnt_update" value="修 改">&nbsp;
	                <input type="button" class="inputbutton" id="bnt_back" value="关闭" >
	             </td>
	        </tr>
        </table>
     	</c:otherwise>
     	</c:choose>
       </form>
	</body>
</html>