<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>会员新增、修改</title>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/js/jquery-1.6.4.js"></script>
		<link href="<%=request.getContextPath()%>/admin/css/blue/mainPage.css" rel="stylesheet" type="text/css" />
		<script src="<%=request.getContextPath()%>/admin/js/initPage.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/dialog/jquery.artDialog.js?skin=blue" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidator-4.1.3.min.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/formvalidator/formValidatorRegex.js" type="text/javascript"></script>
		<script src="<%=request.getContextPath()%>/admin/common/date/WdatePicker.js" type="text/javascript"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/admin/cms/js/member.js"></script>
		<script type="text/javascript">
    	var contextPath = '<%=request.getContextPath()%>';
    	var reqMethod = '${method}';
    	</script>
	</head>
	<body>
		<form id="memberForm" name="memberForm" action="#">
		<c:choose>
	    <c:when test="${method eq 'add'}">
			<table border="0" cellpadding="0" cellspacing="0" width="325px">
	        <tr align="center">
	            <td id="td_tab1" class="titlemouseover">基本信息</td>
	            <td style="width:2px"></td>
	            <td id="td_tab2" class="tabtitle">栏目选项</td>
	            <td style="width:2px"></td>
	            <td id="td_tab3" class="tabtitle">个人信息</td>
	            <td style="width:2px"></td>
	            <td id="td_tab4" class="tabtitle">业务信息</td>
	        </tr>
	    	</table>
			<table id="tb_Pannel1" border="0" cellpadding="2" cellspacing="1" class="tableWrap dispalyTable" width="100%" style="border-bottom:0px;">
		        <tr class="tdbg">
		            <td class="tdbgleft" style="width:150px;" align="right">
		                <strong>所属会员组 ：&nbsp;</strong>
		            </td>
		            <td>
						<select id="select_MemberGroup">
							<c:forEach items="${list}" var="list">
							<option value="${list.groupId}">${list.groupName}</option>
							</c:forEach>
						</select>
		            </td>
		        </tr>
				<tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>用户名：&nbsp;</strong>
		            </td>
		            <td>
						<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_userName" type="text" style="width:200px;"/><span style="color:#FF0000;">*</span>
				                </td>
				                <td><div id="txt_userNameTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>用户密码：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_userPassword" type="text" style="width:200px;"/><span style="color:#FF0000;">*</span>
				                </td>
				                <td><div id="txt_userPasswordTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>真是姓名：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_trueName" type="text" style="width:200px;"/><span style="color:#FF0000;">*</span>
				                </td>
				                <td><div id="txt_trueNameTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		        	<td class="tdbgleft" align="right"><strong>头像：&nbsp;</strong></td>
		        	<td>
		        		<table border="0" cellpadding="2" cellspacing="1" width="100%">
		                    <tr>
		                    	<td style="width:80px;"><strong>头像地址：&nbsp;</strong></td>
		                        <td>
		                        	<input id="txt_userFace" type="text" style="width:210px;"/>
		                        </td>
		                        <td rowspan="4"><img id="img_UserFace" src="<%=request.getContextPath()%>/admin/imgs/blue/default_userface.gif" alt="用户头像" width="120px"/></td>
		                    </tr>
		                    <tr class="tdbg">
		                    	<td><strong>上传头像：&nbsp;</strong></td>
		                        <td>
		                        <iframe marginheight="0" marginwidth="0" scrolling="no" src="<%=request.getContextPath()%>/security/loadDefaultImg.html?callback=callbackDefaultPic&iframe=OpendialogWindow_MemberDetail" width="100%" height="58px" frameborder="0"></iframe>
		                        </td>
		                    </tr>
		                     <tr class="tdbg">
		                    	<td><strong>头像宽：&nbsp;</strong></td>
		                        <td>
		                        	<table cellpadding="0" cellspacing="0" border="0">
					                	<tr>
						                	<td>
							                <input id="txt_faceWidth" type="text" style="width:50px;" value="100"/>
							                </td>
							                <td><div id="txt_faceWidthTip"></div></td>
						                </tr>
						            </table>
		                        </td>
		                    </tr>
		                    <tr class="tdbg">
		                    	<td><strong>头像高：&nbsp;</strong></td>
		                        <td>
		                        	<table cellpadding="0" cellspacing="0" border="0">
					                	<tr>
						                	<td>
							                <input id="txt_faceHeight" type="text" style="width:50px;" value="100"/>
							                </td>
							                <td><div id="txt_faceHeightTip"></div></td>
						                </tr>
						            </table>
		                        </td>
		                    </tr>
		                </table>
		        	</td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>Email地址：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_email" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_emailTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>是否容许修改密码：&nbsp;</strong>
		            </td>
		            <td>
					  <span class="valignMiddle">容许</span><input id="radio_enableResetPassword1" name="radio_enableResetPassword" type="radio" checked="checked"/>
				      <span class="valignMiddle">不容许</span><input id="radio_enableResetPassword2" name="radio_enableResetPassword" type="radio" />
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>提示问题：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_question" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_questionTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>提示答案：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_answer" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_answerTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		     </table>
		     <table id="tb_Pannel2" border="0" cellpadding="2" cellspacing="1" class="tableWrap diplayNone" width="100%" style="border-bottom:0px;">
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right" style="width:150px;">
		                <strong>通信地址：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		            		<tr>
		            			<td colspan="2">
		            			<span class="valignMiddle">省：</span>
		            			<select id="select_province" class="valignMiddle">
		            				<option value="-1">请选择...</option>
		            				<c:forEach var="list" items="${areas}">
		            				<option value="${list.id}">${list.name}</option>
		            				</c:forEach>
		            			</select>
		            			<span class="valignMiddle">市：</span>
		            			<select id="select_city" class="valignMiddle" disabled="disabled">
		            				<option value="-1">请选择...</option>
		            			</select>
		            			<span class="valignMiddle">区、县：</span>
		            			<select id="select_district" class="valignMiddle" disabled="disabled">
		            				<option value="-1">请选择...</option>
		            			</select>
		            			</td>
		            		</tr>
		                	<tr>
			                	<td>
				                <input id="txt_address" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_addressTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>办公电话：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_officePhone" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_officePhoneTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>移动电话：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_mobile" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_mobileTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>住宅电话：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_homePhone" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_homePhoneTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>QQ号码：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_qq" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_qqTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>个人主页：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_homepage" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_homepageTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		     </table>
			 <table id="tb_Pannel3" border="0" cellpadding="2" cellspacing="1" class="tableWrap diplayNone" width="100%" style="border-bottom:0px;">
			 	<tr class="tdbg">
		            <td class="tdbgleft" align="right" style="width:150px;">
		                <strong>出生日期：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_birthday" type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" />
				                </td>
				                <td><div id="txt_birthdayTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>证件：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
		                		<td><strong>证件类型：&nbsp;</strong></td>
			                	<td>
			                		<select id="select_cardTyp" class="valignMiddle">
			                			<option value="1">身份证</option>
			                			<option value="2">护照</option>
			                			<option value="3">驾驶证</option>
			                			<option value="4">港澳通行证</option>
			                			<option value="99">其他</option>
		            				</select>
				                </td>
				                <td><div id="txt_cardTypeTip"></div></td>
			                </tr>
			                <tr>
		                		<td><strong>证件号码：&nbsp;</strong></td>
			                	<td>
				                <input id="txt_idCard" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_idCardTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>籍贯：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_nativePlace" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_nativePlaceTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>民族：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_nation" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_nationTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>性别：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <span class="valignMiddle">男</span><input id="radio_sex1" name="radio_sex" type="radio" checked="checked"/>
				                <span class="valignMiddle">女</span><input id="radio_sex2" name="radio_sex" type="radio" />
				                </td>
				                <td></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>婚姻状况：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <span class="valignMiddle">保密</span><input id="rd_Marriage_1" type="radio" class="valignMiddle" checked="checked" name="rd_Marriage"/>
                				<span class="valignMiddle">未婚</span><input id="rd_Marriage_2" type="radio" class="valignMiddle" name="rd_Marriage"/>
                				<span class="valignMiddle">已婚</span><input id="rd_Marriage_3" type="radio" class="valignMiddle" name="rd_Marriage"/>
                				<span class="valignMiddle">离异</span><input id="rd_Marriage_4" type="radio" class="valignMiddle" name="rd_Marriage"/>
				                </td>
				                <td><div id="txt_marriageTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>学历：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
			                	<select id="select_education" class="valignMiddle">
			                		<option Value="请选择...">请选择...</option>
				                    <option value="小学">小学</option>
				                    <option value="初中">初中</option>
				                    <option value="高中">高中</option>
				                    <option value="中专">中专</option>
				                    <option value="大专">大专</option>
				                    <option value="本科">本科</option>
				                    <option value="硕士">硕士</option>
				                    <option value="博士">博士</option>
				                    <option value="博士后">博士后</option>
				                    <option value="其他">其他</option>
			                	</select>
				                </td>
				                <td></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>毕业学校：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_graduateFrom" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_graduateFromTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>生活爱好：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_interestsOfLife" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_interestsOfLifeTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>文化爱好：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_interestsOfCulture" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_interestsOfCultureTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>娱乐休闲爱好：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_interestsOfAmusement" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_interestsOfAmusementTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>体育爱好：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_interestsOfSport" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_interestsOfSportTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>其他爱好：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_interestsOfOther" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_interestsOfOtherTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>月收入：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
			                		<select id="select_income">
			                		<option>请选择...</option>
				                    <option>1000元以下</option>
				                    <option>1000-3000元</option>
				                    <option>3000-6000元</option>
				                    <option>6000-10000元</option>
				                    <option>10000元以上</option>
			                		</select>
				                </td>
				                <td></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>家庭情况：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_family" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_familyTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
			 </table>
			 <table id="tb_Pannel4" border="0" cellpadding="2" cellspacing="1" class="tableWrap diplayNone" width="100%" style="border-bottom:0px;">
			 	<tr class="tdbg">
		            <td class="tdbgleft" align="right" style="width:150px;">
		                <strong>单位名称：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_company" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_companyTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>所属部门：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_department" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_departmentTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>职位：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_position" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_positionTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>单位地址：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_companyAddress" type="text" style="width:200px;"/>
				                </td>
				                <td><div id="txt_companyAddressTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
			 </table>
			<table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
		        <tr>
		            <td align="center">
		                <input type="button" class="inputbutton" id="bnt_save" value="新增">&nbsp;
		                <input type="button" class="inputbutton" id="bnt_close" value="关闭" >
		             </td>
		        </tr>
        	</table>
	    </c:when>
	    <c:otherwise>
			<table border="0" cellpadding="0" cellspacing="0" width="325px">
	        <tr align="center">
	            <td id="td_tab1" class="titlemouseover">基本信息</td>
	            <td style="width:2px"></td>
	            <td id="td_tab2" class="tabtitle">栏目选项</td>
	            <td style="width:2px"></td>
	            <td id="td_tab3" class="tabtitle">个人信息</td>
	            <td style="width:2px"></td>
	            <td id="td_tab4" class="tabtitle">业务信息</td>
	        </tr>
	    	</table>
			<table id="tb_Pannel1" border="0" cellpadding="2" cellspacing="1" class="tableWrap dispalyTable" width="100%" style="border-bottom:0px;">
		        <tr class="tdbg">
		            <td class="tdbgleft" style="width:150px;" align="right">
		                <strong>所属会员组 ：&nbsp;</strong>
		            </td>
		            <td>
						<select id="select_MemberGroup">
							<c:forEach items="${list}" var="list">
							<option value="${list.groupId}">${list.groupName}</option>
							</c:forEach>
						</select>
		            </td>
		        </tr>
				<tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>用户名：&nbsp;</strong>
		            </td>
		            <td>
						<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
									${bean.userName}
				                </td>
				                <td><input id="txt_userName" type="hidden"/><input id="txt_userPassword" type="hidden"/></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>真是姓名：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_trueName" type="text" style="width:200px;" value="${bean.trueName}"/><span style="color:#FF0000;">*</span>
				                </td>
				                <td><div id="txt_trueNameTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		        	<td class="tdbgleft" align="right"><strong>头像：&nbsp;</strong></td>
		        	<td>
		        		<table border="0" cellpadding="2" cellspacing="1" width="100%">
		                    <tr>
		                    	<td style="width:80px;"><strong>头像地址：&nbsp;</strong></td>
		                        <td>
		                        	<input id="txt_userFace" type="text" style="width:210px;" value="${bean.userFace}"/>
		                        </td>
		                        <td rowspan="4"><img id="img_UserFace" src="${bean.userFace}" alt="用户头像" width="120px"/></td>
		                    </tr>
		                    <tr class="tdbg">
		                    	<td><strong>上传头像：&nbsp;</strong></td>
		                        <td>
		                        <iframe marginheight="0" marginwidth="0" scrolling="no" src="<%=request.getContextPath()%>/security/loadDefaultImg.html?callback=callbackDefaultPic&iframe=OpendialogWindow_MemberDetail" width="100%" height="58px" frameborder="0"></iframe>
		                        </td>
		                    </tr>
		                     <tr class="tdbg">
		                    	<td><strong>头像宽：&nbsp;</strong></td>
		                        <td>
		                        	<table cellpadding="0" cellspacing="0" border="0">
					                	<tr>
						                	<td>
							                <input id="txt_faceWidth" type="text" style="width:50px;" value="${bean.faceWidth}"/>
							                </td>
							                <td><div id="txt_faceWidthTip"></div></td>
						                </tr>
						            </table>
		                        </td>
		                    </tr>
		                    <tr class="tdbg">
		                    	<td><strong>头像高：&nbsp;</strong></td>
		                        <td>
		                        	<table cellpadding="0" cellspacing="0" border="0">
					                	<tr>
						                	<td>
							                <input id="txt_faceHeight" type="text" style="width:50px;" value="${bean.faceHeight}"/>
							                </td>
							                <td><div id="txt_faceHeightTip"></div></td>
						                </tr>
						            </table>
		                        </td>
		                    </tr>
		                </table>
		        	</td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>Email地址：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_email" type="text" style="width:200px;" value="${bean.email}"/>
				                </td>
				                <td><div id="txt_emailTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>是否容许修改密码：&nbsp;</strong>
		            </td>
		            <td>
					  <span class="valignMiddle">容许</span><input id="radio_enableResetPassword1" name="radio_enableResetPassword" type="radio" <c:if test="${bean.enableResetPassword}">checked="checked"</c:if>/>
				      <span class="valignMiddle">不容许</span><input id="radio_enableResetPassword2" name="radio_enableResetPassword" type="radio" <c:if test="${!bean.enableResetPassword}">checked="checked"</c:if>/>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>提示问题：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_question" type="text" style="width:200px;" value="${bean.answer}"/>
				                </td>
				                <td><div id="txt_questionTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>提示答案：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_answer" type="text" style="width:200px;" value="${bean.question}"/>
				                </td>
				                <td><div id="txt_answerTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		     </table>
		     <table id="tb_Pannel2" border="0" cellpadding="2" cellspacing="1" class="tableWrap diplayNone" width="100%" style="border-bottom:0px;">
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right" style="width:150px;">
		                <strong>通信地址：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		            		<tr>
		            			<td colspan="2">
		            			<span class="valignMiddle">省：</span>
		            			<select id="select_province" class="valignMiddle">
		            				<option value="-1">请选择...</option>
		            				<c:forEach var="list" items="${areas}">
		            				<option value="${list.id}">${list.name}</option>
		            				</c:forEach>
		            			</select>
		            			<span class="valignMiddle">市：</span>
		            			<select id="select_city" class="valignMiddle" disabled="disabled">
		            				<option value="-1">请选择...</option>
		            			</select>
		            			<span class="valignMiddle">区、县：</span>
		            			<select id="select_district" class="valignMiddle" disabled="disabled">
		            				<option value="-1">请选择...</option>
		            			</select>
		            			</td>
		            		</tr>
		                	<tr>
			                	<td>
				                <input id="txt_address" type="text" style="width:200px;" value="${bean.address}"/>
				                </td>
				                <td><div id="txt_addressTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>办公电话：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_officePhone" type="text" style="width:200px;" value="${bean.officePhone}"/>
				                </td>
				                <td><div id="txt_officePhoneTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>移动电话：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_mobile" type="text" style="width:200px;" value="${bean.mobile}"/>
				                </td>
				                <td><div id="txt_mobileTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>住宅电话：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_homePhone" type="text" style="width:200px;" value="${bean.homePhone}"/>
				                </td>
				                <td><div id="txt_homePhoneTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>QQ号码：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_qq" type="text" style="width:200px;" value="${bean.qq}"/>
				                </td>
				                <td><div id="txt_qqTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>个人主页：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_homepage" type="text" style="width:200px;" value="${bean.homepage}"/>
				                </td>
				                <td><div id="txt_homepageTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		     </table>
			 <table id="tb_Pannel3" border="0" cellpadding="2" cellspacing="1" class="tableWrap diplayNone" width="100%" style="border-bottom:0px;">
			 	<tr class="tdbg">
		            <td class="tdbgleft" align="right" style="width:150px;">
		                <strong>出生日期：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_birthday" type="text" style="width:150px;" onfocus="WdatePicker({skin:'whyGreen',isShowClear:false,readOnly:true,dateFmt:'yyyy-MM-dd HH:mm'})" class="Wdate" <c:if test="${!empty bean.birthday}">value="<fmt:formatDate value="${bean.birthday}" type="date" pattern="yyyy-MM-dd HH:mm"/>"</c:if>/>
				                </td>
				                <td><div id="txt_birthdayTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>证件：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
		                		<td><strong>证件类型：&nbsp;</strong></td>
			                	<td>
			                		<select id="select_cardTyp" class="valignMiddle">
			                			<option value="1" <c:if test="${bean.cardType==1}">selected="selected"</c:if>>身份证</option>
			                			<option value="2" <c:if test="${bean.cardType==2}">selected="selected"</c:if>>护照</option>
			                			<option value="3" <c:if test="${bean.cardType==3}">selected="selected"</c:if>>驾驶证</option>
			                			<option value="4" <c:if test="${bean.cardType==4}">selected="selected"</c:if>>港澳通行证</option>
			                			<option value="99" <c:if test="${bean.cardType==99}">selected="selected"</c:if>>其他</option>
		            				</select>
				                </td>
				                <td></td>
			                </tr>
			                <tr>
		                		<td><strong>证件号码：&nbsp;</strong></td>
			                	<td>
				                <input id="txt_idCard" type="text" style="width:200px;" value="${bean.idCard}"/>
				                </td>
				                <td><div id="txt_idCardTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>籍贯：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_nativePlace" type="text" style="width:200px;" value="${bean.nativePlace}"/>
				                </td>
				                <td><div id="txt_nativePlaceTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>民族：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_nation" type="text" style="width:200px;" value="${bean.nation}"/>
				                </td>
				                <td><div id="txt_nationTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>性别：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <span class="valignMiddle">男</span><input id="radio_sex1" name="radio_sex" type="radio" <c:if test="${bean.sex==1}">checked="checked"</c:if>/>
				                <span class="valignMiddle">女</span><input id="radio_sex2" name="radio_sex" type="radio" <c:if test="${bean.sex==2}">checked="checked"</c:if>/>
				                </td>
				                <td></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>婚姻状况：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <span class="valignMiddle">保密</span><input id="rd_Marriage_1" type="radio" class="valignMiddle" name="rd_Marriage" <c:if test="${bean.marriage==1}">checked="checked"</c:if>/>
                				<span class="valignMiddle">未婚</span><input id="rd_Marriage_2" type="radio" class="valignMiddle" name="rd_Marriage" <c:if test="${bean.marriage==2}">checked="checked"</c:if>/>
                				<span class="valignMiddle">已婚</span><input id="rd_Marriage_3" type="radio" class="valignMiddle" name="rd_Marriage" <c:if test="${bean.marriage==3}">checked="checked"</c:if>/>
                				<span class="valignMiddle">离异</span><input id="rd_Marriage_4" type="radio" class="valignMiddle" name="rd_Marriage" <c:if test="${bean.marriage==4}">checked="checked"</c:if>/>
				                </td>
				                <td></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>学历：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
			                	<select id="select_education" class="valignMiddle">
			                		<option Value="请选择...">请选择...</option>
				                    <option value="小学" <c:if test="${!empty bean.education&&bean.education eq '小学'}">selected="selected"</c:if>>小学</option>
				                    <option value="初中" <c:if test="${!empty bean.education&&bean.education eq '初中'}">selected="selected"</c:if>>初中</option>
				                    <option value="高中" <c:if test="${!empty bean.education&&bean.education eq '高中'}">selected="selected"</c:if>>高中</option>
				                    <option value="中专" <c:if test="${!empty bean.education&&bean.education eq '中专'}">selected="selected"</c:if>>中专</option>
				                    <option value="大专" <c:if test="${!empty bean.education&&bean.education eq '大专'}">selected="selected"</c:if>>大专</option>
				                    <option value="本科" <c:if test="${!empty bean.education&&bean.education eq '本科'}">selected="selected"</c:if>>本科</option>
				                    <option value="硕士" <c:if test="${!empty bean.education&&bean.education eq '硕士'}">selected="selected"</c:if>>硕士</option>
				                    <option value="博士" <c:if test="${!empty bean.education&&bean.education eq '博士'}">selected="selected"</c:if>>博士</option>
				                    <option value="博士后" <c:if test="${!empty bean.education&&bean.education eq '博士后'}">selected="selected"</c:if>>博士后</option>
				                    <option value="其他" <c:if test="${!empty bean.education&&bean.education eq '其他'}">selected="selected"</c:if>>其他</option>
			                	</select>
				                </td>
				                <td></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>毕业学校：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_graduateFrom" type="text" style="width:200px;" value="${bean.graduateFrom}"/>
				                </td>
				                <td><div id="txt_graduateFromTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>生活爱好：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_interestsOfLife" type="text" style="width:200px;" value="${bean.interestsOfLife}"/>
				                </td>
				                <td><div id="txt_interestsOfLifeTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>文化爱好：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_interestsOfCulture" type="text" style="width:200px;" value="${bean.interestsOfCulture}"/>
				                </td>
				                <td><div id="txt_interestsOfCultureTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>娱乐休闲爱好：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_interestsOfAmusement" type="text" style="width:200px;" value="${bean.interestsOfAmusement}"/>
				                </td>
				                <td><div id="txt_interestsOfAmusementTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>体育爱好：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_interestsOfSport" type="text" style="width:200px;" value="${bean.interestsOfSport}"/>
				                </td>
				                <td><div id="txt_interestsOfSportTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>其他爱好：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_interestsOfOther" type="text" style="width:200px;" value="${bean.interestsOfOther}"/>
				                </td>
				                <td><div id="txt_interestsOfOtherTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>月收入：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
			                		<select id="select_income">
			                		<option>请选择...</option>
				                    <option <c:if test="${!empty bean.income&&bean.income eq '1000元以下'}">selected="selected"</c:if>>1000元以下</option>
				                    <option <c:if test="${!empty bean.income&&bean.income eq '1000-3000元'}">selected="selected"</c:if>>1000-3000元</option>
				                    <option <c:if test="${!empty bean.income&&bean.income eq '3000-6000元'}">selected="selected"</c:if>>3000-6000元</option>
				                    <option <c:if test="${!empty bean.income&&bean.income eq '6000-10000元'}">selected="selected"</c:if>>6000-10000元</option>
				                    <option <c:if test="${!empty bean.income&&bean.income eq '10000元以上'}">selected="selected"</c:if>>10000元以上</option>
			                		</select>
				                </td>
				                <td></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>家庭情况：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_family" type="text" style="width:200px;" value="${bean.family}"/>
				                </td>
				                <td><div id="txt_familyTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
			 </table>
			 <table id="tb_Pannel4" border="0" cellpadding="2" cellspacing="1" class="tableWrap diplayNone" width="100%" style="border-bottom:0px;">
			 	<tr class="tdbg">
		            <td class="tdbgleft" align="right" style="width:150px;">
		                <strong>单位名称：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_company" type="text" style="width:200px;" value="${bean.company}"/>
				                </td>
				                <td><div id="txt_companyTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>所属部门：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_department" type="text" style="width:200px;" value="${bean.department}"/>
				                </td>
				                <td><div id="txt_departmentTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>职位：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_position" type="text" style="width:200px;" value="${bean.position}"/>
				                </td>
				                <td><div id="txt_positionTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
		        <tr class="tdbg">
		            <td class="tdbgleft" align="right">
		                <strong>单位地址：&nbsp;</strong>
		            </td>
		            <td>
		            	<table cellpadding="0" cellspacing="0" border="0">
		                	<tr>
			                	<td>
				                <input id="txt_companyAddress" type="text" style="width:200px;" value="${bean.companyAddress}"/>
				                </td>
				                <td>&nbsp;<div id="txt_companyAddressTip"></div></td>
			                </tr>
			            </table>
		            </td>
		        </tr>
			 </table>
			<table width="100%" cellspacing="1" cellpadding="2" border="0" style="border-top:0px;height:40px;background-color:#DEECF7;" class="tableWrap">
		        <tr>
		            <td align="center">
		                <input type="button" class="inputbutton" id="bnt_update" value="修改">&nbsp;
		                <input type="button" class="inputbutton" id="bnt_close" value="关闭" >
		             </td>
		        </tr>
        	</table>
		</c:otherwise>
	    </c:choose>
	    </form>
	</body>
</html>