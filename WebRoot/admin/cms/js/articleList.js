var _gArticleDialog;
$(document).ready(function(){
	beautifyTopButton(['article_Add','article_Update','article_Recycle','article_Del','article_Search','article_Restore','article_Draft','article_Passed','article_CancelPassed','article_WaitPass']);
	beautifyTable('tbody_DataList');
	checkBoxFunction.init('cb_CheckedAll','tbody_DataList');
	$.fn.initPage(alertWaiting);
});
(function($){
	var View={
		init:function(alertWaiting){
			var checkedId = 'td_tab1';
			var status = parseUri.getParam('status');
			if(status=='-1'){
				checkedId = 'td_tab2';
			}
			else if(status=='0'){
				checkedId = 'td_tab3';
			}
			else if(status=='99'){
				checkedId = 'td_tab4';
			}
			else if(status=='-3'){
				checkedId = 'td_tab5';
			}
			else if(status=='999'){
				checkedId = 'td_tab1';
			}
			if('td_tab1'!=checkedId){
				$("#"+checkedId).attr("class", "titlemouseover");
		        $("#td_tab1").attr("class", "tabtitle");
			}

			$('#td_tab1').click(function(){
				changeUrl('td_tab1','999');
			});
			$('#td_tab2').click(function(){
				changeUrl('td_tab2','-1');
			});
			$('#td_tab3').click(function(){
				changeUrl('td_tab3','0');
			});
			$('#td_tab4').click(function(){
				changeUrl('td_tab4','99');
			});
			$('#td_tab5').click(function(){
				changeUrl('td_tab5','-3');
			});
			function changeUrl(id,status){
				if(checkedId!=id){
					parseUri.clear();
					window.location.href = parseUri.setParam('status',status);
				}
			}
			$('#article_Add').click(function(){
				_gArticleDialog=art.dialog.open(contextPath+'/security/loadAddArticle.html',{id:'dialogWindow_AddArticle',height:'92%',width:'90%',title:'新增文章',padding: 0,lock:true},false);
			});
			$('#article_Update').click(function(){
				var array = checkBoxFunction.getId();
				if(array.length<=0){
					alert('请选择要操作项！');
					return;
				}
				if(array.length>1){
					alert('请选择一条记录修改！');
					return;
				}
				_gArticleDialog=art.dialog.open(contextPath+'/security/loadUpdateArticle.html?id='+array.join(''),{id:'dialogWindow_AddArticle',height:'92%',width:'90%',title:'修改文章内容',padding: 0,lock:true},false);
			});
			$('#article_Recycle').click(function(){
				updateStatus('-3');
			});
			$('#article_Restore').click(function(){
				updateStatus('0');
			});
			$('#article_WaitPass').click(function(){
				updateStatus('0');
			});
			$('#article_Passed').click(function(){
				updateStatus('99');
			});
			$('#article_Draft').click(function(){
				updateStatus('-1');
			});
			$('#article_CancelPassed').click(function(){
				updateStatus('0');
			});
			function updateStatus(status){
				var array = checkBoxFunction.getId();
				if(array.length<=0){
					alert('请选择要操作项！');
					return;
				}
				var waiting = alertWaiting('请等待...');
				var _url = contextPath+'/security/updateStatus.html';
				$.post(_url,{status:status,ids:array.join(',')},function(data){
					waiting.close();
					if(data.code=='200'){
						alert(data.content);
						window.location.reload();
					}
					else{
						alert(data.content);
					}
				},'json');
			};
			$('#article_Search').click(function(){
				var txt = $.trim($('#article_SearchText').val());
				var searchType = $.trim($('#select_SearchKey').val());
				if(searchType==''){
					return;
				}
				parseUri.setParam('searchType',searchType);
				window.location.href = parseUri.setParam('keyword',txt);
			});
			$('#select_SearchConditions').change(function(){
				var val = $('#select_SearchConditions').val();
				var array = val.split('_');
				parseUri.setParam('orderField',array[0]);
				window.location.href = parseUri.setParam('orderType',array[1]);
			});
			$('#ddl_ChannelList').change(function(){
				var val = $('#ddl_ChannelList').val();
				if(val!=='0'){
					window.location.href = parseUri.setParam('channelId',val);
				}
				else{
					window.location.href = parseUri.setParam('channelId','');
				}
			});
			$('#article_Del').click(function(){
				var array = checkBoxFunction.getId();
				if(array.length<=0){
					alert('请选择要操作项！');
					return;
				}
				var isOK = confirm('您确定要删除这条记录吗？');
				if(isOK){
					var waiting = alertWaiting('请等待...');
					var _url = contextPath+'/security/delArticle.html';
					$.post(_url,{id:array.join(',')},function(data){
						waiting.close();
						if(data.code=='200'){
							alert(data.content);
						}
						else{
							alert(data.content);
						}
						window.location.reload();
					},'json');
				}
			});
		}
	};
	$.fn.initPage = function(alertWaiting){
		View.init(alertWaiting);
	};
})(jQuery);
function articleDetail(_id){
	_gArticleDialog=art.dialog.open(contextPath+'/security/articleDetail.html?id='+_id,{id:'dialogWindow_AddArticle',height:'92%',width:'90%',title:'文章详情',padding: 0,lock:true},false);
};
function callBackResult(resultFlag){
	_gArticleDialog.close();
	if(resultFlag=='1'){
		window.location.reload();
	}
};
function toUpdatePage(_aId){
	_gArticleDialog.close();
	_gArticleDialog=art.dialog.open(contextPath+'/security/loadUpdateArticle.html?id='+_aId,{id:'dialogWindow_AddArticle',height:'90%',width:'80%',title:'修改文章内容',padding: 0,lock:true},false);
};