var _dialogGuestBookReply;
$(document).ready(function(){
	beautifyTopButton(['reply_SearchText','reply_Search']);
	beautifyTable('tbody_DataList');
	beautifyInputText(['reply_SearchText']);


	$('#reply_Search').click(function(){
		var v = $('#reply_SearchText').val();
		var dv = $('#reply_SearchText').attr('alt');
		if(v == dv){
			v = '';
		}
		var _val = $('#ddl_findField').val();
		parseUri.setParam('pageNum',1)
		parseUri.setParam('field',_val)
		window.location.href = parseUri.setParam('keyword',v);
	});
	$('a[name="bnt_toDetail"]').click(function(){
		var _id = $(this).attr('value');
		_dialogGuestBookReply=art.dialog.open(contextPath+'/security/guestBookDetail.html?gid='+_id,{id:'dialogWindow_GuestBookDetail',height:'92%',width:'80%',title:'留言详情',padding: 0,lock:true},false);
	});
	$('a[name="bnt_updateReply"]').click(function(){
		var _rid = $(this).attr('value');
		_dialogGuestBookReply = art.dialog.open(contextPath+'/security/loadUpdateGuestBookReply.html?id='+_rid,{height:'450px',width:'850px',title:'新增/修改留言回复',padding: 0,lock:true},false);
	});
	$('a[name="bnt_delReply"]').click(function(){
		var isOK = confirm('您确定要删除这条记录吗？');
		if(isOK){
			var _rId = $(this).attr('value');
			$.post(contextPath+'/security/delGuestBookReply.html',{id:_rId},function(data){
				if(data.code=='200'){
					alert(data.content);
					window.location.reload();
				}
				else{
					alert(data.content);
				}
			},'json');
		}
	});
});
var closeDialog = function(){
	_dialogGuestBookReply.close();
	window.location.reload();
};
function callBackResult(resultFlag){
	_dialogGuestBookReply.close();
	if(resultFlag=='1'){
		window.location.reload();
	}
};
function toUpdatePage(_aId){
	_dialogGuestBookReply.close();
	_dialogGuestBookReply=art.dialog.open(contextPath+'/security/loadUpdateGuestBook.html?id='+_aId,{id:'dialogWindow_GuestBookDetail',height:'92%',width:'80%',title:'修改留言',padding: 0,lock:true},false);
};