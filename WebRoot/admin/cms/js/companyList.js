var _gDialogCompany;
$(document).ready(function(){
	beautifyTopButton(['company_Add','company_Del','company_Update','company_Search']);
	beautifyTable('tbody_DataList');
	beautifyInputText(['company_SearchText']);
	var tableTemp = new beautifySingleSelectedTable('tbody_DataList');
	$('#company_Del').click(function(){
		alert(tableTemp.getValue());
	});

	$('#company_Add').click(function(){
		_gDialogCompany=art.dialog.open(contextPath+'/security/loadAddCompany.html',{id:'dialogWindow_CompanyDetail',height:'500px',width:'810px',title:'新增公司',padding: 0,lock:true},false);
	});

	$('#company_Update').click(function(){
		var _id = tableTemp.getValue();
		if(_id==''){
			alert('请选择修改项！');
			return;
		}
		_gDialogCompany=art.dialog.open(contextPath+'/security/loadUpdateCompany.html?id='+_id,{id:'dialogWindow_CompanyDetail',height:'500px',width:'810px',title:'修改公司信息',padding: 0,lock:true},false);
	});

	$('#company_Search').click(function(){
		var v = $('#company_SearchText').val();
		var dv = $('#company_SearchText').attr('alt');
		if(v == dv){
			v = '';
		}
		parseUri.setParam('pageNum',1)
		window.location.href = parseUri.setParam('keyword',v);
	});
	$('a[id*="detail_"]').click(function(){
		var _id = $(this).attr('name');
		_gDialogCompany = art.dialog.open(contextPath+'/security/companyDetail.html?id='+_id,{id:'dialogWindow_CompanyDetail',height:'450px',width:'800px',title:'公司信息详情',padding: 0,lock:true},false);
	});
});
function callBackResult(resultFlag){
	_gDialogCompany.close();
	if(resultFlag=='1'){
		window.location.reload();
	}
};