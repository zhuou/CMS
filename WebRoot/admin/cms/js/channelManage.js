var _gChannelDialog;
$(document).ready(function(){
	beautifyTopButton(['channel_Add','channel_Del','channel_Update','channel_Detail']);
	$.fn.zTree.init($("#treeChannels"), setting, zNodes);
	$('#channel_Add').click(function(){
		var content = new Array();
		content.push('<div class="aui_content" style="padding: 0px;"><div class="divSys"><ul>');
		content.push('<li><a onclick="goAddChannelPage(0);"><img alt="" src="'+contextPath+'/admin/imgs/blue/channel_contain.png"></a>');
		content.push('<div><a onclick="goAddChannelPage(0);">添加内容栏目</a></div></li>');
		content.push('<li><a onclick="goAddChannelPage(2);"><img alt="" src="'+contextPath+'/admin/imgs/blue/channel_single.png"></a>');
		content.push('<div><a onclick="goAddChannelPage(2);">添加单页栏目</a></div></li>');
		content.push('<li><a onclick="goAddChannelPage(3);"><img alt="" src="'+contextPath+'/admin/imgs/blue/channel_external.png"></a>');
        content.push('<div><a onclick="goAddChannelPage(3);">添加外部链接栏目</a></div></li>');
		content.push('</ul></div></div>');
		_gChannelDialog = art.dialog({padding: 0,title: '请选择要添加的栏目类型',content: content.join(''),lock: true,drag:false});
	});
	$('#channel_Del').click(function(){
		var channelZTreeObj = $.fn.zTree.getZTreeObj("treeChannels");
		var nodes = channelZTreeObj.getSelectedNodes();
		if(nodes.length<=0||nodes[0].id<=0){
			alert('请选择要删除的栏目节点！');
			return;
		}
		var isOK = confirm('删除后数据将不可恢复，您确定要删除选中的记录吗？');
		if(isOK){
			var waiting = alertWaiting('请等待...');
			var _url = contextPath+'/security/delChannel.html';
			$.post(_url,{ids:nodes[0].id},function(data){
				waiting.close();
				if(data.code=='200'){
					alert(data.content);
					window.location.reload();
				}
				else{
					alert(data.content);
				}
			},'json');
		}
	});
	$('#channel_Update').click(function(){
		var channelZTreeObj = $.fn.zTree.getZTreeObj("treeChannels");
		var nodes = channelZTreeObj.getSelectedNodes();
		if(nodes.length<=0||nodes[0].id<=0){
			alert('请选择要查看的栏目节点！');
			return;
		}
	    var id = nodes[0].id;
		var type = nodes[0].type;
		if(type==0){
			_gChannelDialog = art.dialog.open(contextPath+'/security/loadUpdateContainCh.html?id='+id,{id:'dialogWindow_AddChannel',height:'540px',width:'900px',title:'修改容器栏目',padding: 0,lock:true},false);
		}
		else if(type==2){
			_gChannelDialog = art.dialog.open(contextPath+'/security/loadUpdateSingleCh.html?id='+id,{id:'dialogWindow_AddChannel',height:'540px',width:'940px',title:'修改单页栏目',padding: 0,lock:true},false);
		}
		else{
			_gChannelDialog = art.dialog.open(contextPath+'/security/loadUpdateExternalCh.html?id='+id,{id:'dialogWindow_AddChannel',height:'380px',width:'890px',title:'修改外部链接栏目',padding: 0,lock:true},false);
		}
	});

	$('#channel_Detail').click(function(){
		var channelZTreeObj = $.fn.zTree.getZTreeObj("treeChannels");
		var nodes = channelZTreeObj.getSelectedNodes();
		if(nodes.length<=0||nodes[0].id<=0){
			alert('请选择要查看的栏目节点！');
			return;
		}
	    var id = nodes[0].id;
		var type = nodes[0].type;
		if(type==0){
			_gChannelDialog = art.dialog.open(contextPath+'/security/containChDetail.html?id='+id,{id:'dialogWindow_ChannelDetail',height:'540px',width:'900px',title:'容器栏目详情',padding: 0,lock:true},false);
		}
		else if(type==2){
			_gChannelDialog = art.dialog.open(contextPath+'/security/singleChDetail.html?id='+id,{id:'dialogWindow_ChannelDetail',height:'540px',width:'940px',title:'单页栏目详情',padding: 0,lock:true},false);
		}
		else{
			_gChannelDialog = art.dialog.open(contextPath+'/security/externalChDetail.html?id='+id,{id:'dialogWindow_ChannelDetail',height:'380px',width:'890px',title:'外部链接栏目详情',padding: 0,lock:true},false);
		}
	});
});
function goAddChannelPage(_channelType){
	_gChannelDialog.close();
	if(_channelType==0){
		_gChannelDialog = art.dialog.open(contextPath+'/security/loadAddContainCh.html',{id:'dialogWindow_AddChannel',height:'540px',width:'900px',title:'添加容器栏目',padding: 0,lock:true},false);
	}
	else if(_channelType==2){
		_gChannelDialog = art.dialog.open(contextPath+'/security/loadAddSingleCh.html',{id:'dialogWindow_AddChannel',height:'540px',width:'940px',title:'添加单页栏目',padding: 0,lock:true},false);
	}
	else if(_channelType==3){
		_gChannelDialog = art.dialog.open(contextPath+'/security/loadAddExternalCh.html',{id:'dialogWindow_AddChannel',height:'350px',width:'890px',title:'添加外部链接栏目',padding: 0,lock:true},false);
	}
};
function callBackResult(resultFlag){
	_gChannelDialog.close();
	if(resultFlag=='1'){
		window.location.reload();
	}
};
