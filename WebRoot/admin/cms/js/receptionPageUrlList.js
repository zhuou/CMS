var _gDialogReceptionPageUrl;
$(document).ready(function(){
	beautifyTopButton(['receptionPageUrl_Add','receptionPageUrl_Update','receptionPageUrl_Del','receptionPageUrl_Search']);
	var tableTemp = new beautifySingleSelectedTable('tbody_DataList');
	beautifyInputText(['receptionPageUrl_SearchText']);
	$('#receptionPageUrl_Del').click(function(){
		var isOK = confirm('您确定要删除此条数据吗？');
		if(isOK){
			$.getJSON(contextPath+'/security/delReceptionPageUrl.html?id='+tableTemp.getValue(),function(data){
				if(data.code=='200'){
					alert(data.content);
					window.location.reload();
				}
				else{
					alert(data.content);
				}
			});
		}
	});

	$('#receptionPageUrl_Add').click(function(){
		_gDialogReceptionPageUrl=art.dialog.open(contextPath+'/security/loadAddReceptionPageUrl.html',{id:'dialogWindow_ReceptionPageUrl',height:'350px',width:'820px',title:'新增前台Url配置',padding: 0,lock:true},false);
	});

	$('#receptionPageUrl_Update').click(function(){
		var _id = tableTemp.getValue();
		if(_id==''){
			alert('请选择要操作项！');
			return;
		}
		_gDialogReceptionPageUrl=art.dialog.open(contextPath+'/security/loadUpdateReceptionPageUrl.html?id='+_id,{id:'dialogWindow_ReceptionPageUrl',height:'350px',width:'820px',title:'修改前台Url配置',padding: 0,lock:true},false);
	});

	$('#receptionPageUrl_Search').click(function(){
		var v = $('#receptionPageUrl_SearchText').val();
		var dv = $('#receptionPageUrl_SearchText').attr('alt');
		if(v == dv){
			v = '';
		}
		parseUri.setParam('pageNum',1)
		window.location.href = parseUri.setParam('keyword',v);
	});
	$('a[id*="detail_"]').click(function(){
		var _id = $(this).attr('name');
		_gDialogReceptionPageUrl = art.dialog.open(contextPath+'/security/receptionPageUrlDetail.html?id='+_id,{id:'dialogWindow_ReceptionPageUrl',height:'350px',width:'750px',title:'前台Url配置详情',padding: 0,lock:true},false);
	});
});
function callBackResult(resultFlag){
	_gDialogReceptionPageUrl.close();
	if(resultFlag=='1'){
		window.location.reload();
	}
};