var gDialogChannel;
$(document).ready(function(){
	beautifyBottomButton(['bnt_checked','bnt_selectImg','bnt_review','bnt_save','bnt_back']);
	pageTab({'td_tab1':'tb_Pannel1','td_tab2':'tb_Pannel2'});
	$.formValidator.initConfig({formID:"guestBookForm",theme:"Default",
		onError:function(msg,obj,errorlist){
		}
	});
	$("#txt_CheckChannelName").formValidator({onShowText:""});
	$("#txt_title").formValidator({onShowText:"请输入留言标题",onFocus:"留言标题为1-200个字"}).inputValidator({min:1,max:200,onErrorMin:'请输入留言标题',onError:"输入的留言标题长度不合法"});
	$("#txt_guestName").formValidator({onShowText:"请输入用户名",onFocus:"用户名为存在的用户"}).inputValidator({min:1,max:20,onErrorMin:'请输入用户名',onError:"您的输入超过了20个字符"});
	$("#txt_email").formValidator({onShowText:"请输入Email",onFocus:"请输入Email"}).inputValidator({min:1,onErrorMin:'请输入Email'}).regexValidator({regExp:regexEnum.email,onError:"您输入Email地址不合法"});
	$("#txt_phoneNumber").formValidator({empty:true,onFocus:"请输入手机号码"}).inputValidator({max:20,onErrorMin:'手机号码应小于20位'});
	$("#txt_guestOicq").formValidator({empty:true,onFocus:"请输入QQ号"}).inputValidator({max:20,onError:"您的输入超过了20个字符"});
	$("#txt_totlaHits").formValidator({empty:true,onFocus:"可以预置总点击数"}).inputValidator({max:9,onError:"总点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_dayHits").formValidator({empty:true,onFocus:"可以预置日点击数"}).inputValidator({max:9,onError:"日点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_weekHits").formValidator({empty:true,onFocus:"可以预置周点击数"}).inputValidator({max:9,onError:"周点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_monthHits").formValidator({empty:true,onFocus:"可以预置月点击数"}).inputValidator({max:9,onError:"月点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_eliteLevel").formValidator({empty:true,onFocus:"请输入推荐级"}).formValidator({empty:true,onFocus:"推荐级别数越大显示就越靠前"}).inputValidator({max:9,onError:"推荐级数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_priority").formValidator({empty:true,onFocus:"请输入优先级"}).formValidator({empty:true,onFocus:"优先级数越大显示就越靠前"}).inputValidator({max:9,onError:"优先级数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$('#bnt_checked').click(function(){
        gDialogChannel = art.dialog.open(contextPath+'/security/getChannel2.html?forums=-4&iframe=OpendialogWindow_GuestBookDetail', { height: '80%', width: '20%', title: "请选择栏目", lock: true }, false);//打开子窗体
	});
	$('#bnt_back').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('1');
	});
	UE.getEditor('editor');
	$.fn.initPage(alertWaiting);
});
(function($){
	function getData(){
		var data = {'isSuccess':false,data:{}};
		var channelId = $.trim($('#txt_channelId').val());
		if(channelId==''){
			alert('没有获取栏目Id，请和管理员联系');
			return data;
		}
		data.data['channelId'] = channelId;
		var title = $.trim($('#txt_title').val());
		data.data['title'] = title;
		data.data['userId'] = -1;
		var userName = $.trim($('#txt_guestName').val());
		data.data['userName'] = userName;
		var email = $.trim($('#txt_email').val());
		data.data['guestEmail'] = email;
		var phoneNumber = $.trim($('#txt_phoneNumber').val());
		data.data['phoneNumber'] = phoneNumber;
		var guestOicq = $.trim($('#txt_guestOicq').val());
		data.data['guestOicq'] = guestOicq;
		var guestIsPrivate = false;
		if($('#cb_isPrivate').is(":checked")){
			guestIsPrivate = true;
		}
		data.data['guestIsPrivate'] = guestIsPrivate;

		var status = 99;
		if($('#rd_Status_1').is(":checked")){
			status = -1;
		}
		else if($('#rd_Status_2').is(":checked")){
			status = 0;
		}
		data.data['status'] = status;
		var content = UE.getEditor('editor').getContent();
		if(content==''){
			alert('留言内容不能为空，请填写');
			return data;
		}
		data.data['guestContent'] = content;
		var hits = $.trim($('#txt_totlaHits').val());
		data.data['hits'] = hits;
		var dayHits = $.trim($('#txt_dayHits').val());
		data.data['dayHits'] = dayHits;
		var weekHits = $.trim($('#txt_weekHits').val());
		data.data['weekHits'] = weekHits;
		var monthHits = $.trim($('#txt_monthHits').val());
		data.data['monthHits'] = monthHits;
		var updateTime = $.trim($('#txt_updataTime').val());
		data.data['updateTime'] = updateTime;
		var eliteLevel = $.trim($('#txt_eliteLevel').val());
		data.data['eliteLevel'] = eliteLevel;
		var priority = $.trim($('#txt_priority').val());
		data.data['priority'] = priority;
		data['isSuccess']=true;
		return data;
	};
	var View={
		method:'add',
		init:function(alertWaiting){
			var Ajax = {
				submit:function(url,id){
					var isOk = $.formValidator.pageIsValid('1');
					if(!isOk){
						return;
					}
					var pageData = getData();
					if(!pageData.isSuccess){
						return;
					}
					var waiting = alertWaiting('正在提交数据，请等待...');
					pageData.data['commonModelId'] = id;
					$.ajax({
						type:"POST",
						url:url,
						data:pageData.data,
						dataType:'json',
						contentType:'application/x-www-form-urlencoded; charset=UTF-8',
						success: function(data){
							waiting.close();
							if(data.code=='200'){
								if(reqMethod == View.method){
									if(confirm(data.content+'，是否继续新增留言？')){
										document.getElementById("guestBookForm").reset();
										UE.getEditor('editor').setContent('',false);
									}
									else{
										window.top.frames['iframeMenuContent'].callBackResult('1');
									}
								}
								else{
									alert(data.content);
									window.top.frames['iframeMenuContent'].callBackResult('1');
								}
							}
							else{
								alertMessage(data.content);
							}
						}
					});
				}
			};

			if(reqMethod == this.method){
				$("#bnt_save").click(function(){
					Ajax.submit(contextPath+'/security/addGuestBook.html',0);
				});
			}
			else{
				$("#bnt_update").click(function(){
					var commonModelId = $('#txt_commonModelId').val();
					Ajax.submit(contextPath+'/security/updateGuestBook.html',commonModelId);
				});
			}
		}
	};
	$.fn.initPage = function(alertWaiting){
		View.init(alertWaiting);
	};
})(jQuery);
function callBackChannelInfo(_id,_name,_type){
	$('#txt_CheckChannelName').val(_name);
	$('#txt_channelId').val(_id);
	gDialogChannel.close();
};