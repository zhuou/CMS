$(document).ready(function(){
	$.formValidator.initConfig({formID:"webConfigForm",theme:"Small",
		onError:function(msg,obj,errorlist){
		}
	});
	beautifyBottomButton(['saveWebConfig','updateWebConfig','returnWebConfig']);
	$("#saveWebConfig").click(function(){
		var isOk = $.formValidator.pageIsValid('1');
		if(!isOk){
			return;
		}
		if($('#select_company').val()==''){
			alert('请先选择所属公司！');
			return;
		}
		var _webConfigData = getData();
		_webConfigData['companyId']=$('#select_company').val();
		var waiting = alertWaiting('正在提交数据，请等待...');
		$.ajax({
			type:"POST",
			url:contextPath+'/security/addWebConfig.html',
			data:_webConfigData,
			dataType:'json',
			contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			success: function(data){
				waiting.close();
				if(data.code=='200'){
					alert(data.content);
					window.top.frames['iframeMenuContent'].callBackResult('1');
				}
				else{
					alert(data.content);
				}
			}
		});
	});
	$('#updateWebConfig').click(function(){
		var isOk = $.formValidator.pageIsValid('1');
		if(!isOk){
			return;
		}
		var waiting = alertWaiting('正在提交数据，请等待...');
		var data = getData();
		data['siteId'] = $('#siteId').val();
		data['companyId']=$('#select_company').val();
		$.ajax({
			type:"POST",
			url:contextPath+'/security/updateWebConfig.html',
			data:data,
			dataType:'json',
			contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			success: function(data){
				waiting.close();
				if(data.code=='200'){
					alert(data.content);
					window.top.frames['iframeMenuContent'].callBackResult('1');
				}
				else{
					alert(data.content);
				}
			}
		});
	});
	$("#txt_siteName").formValidator({onShowText:"请输入站点名称",onFocus:"站点名称为小1-30个字"}).inputValidator({min:1,max:30,onErrorMin:'请输入站点名称',onError:"输入的站点名称长度不合法"});
	$("#txt_siteUrl").formValidator({onShowText:"请输入网站网址",onFocus:"网站网址为小1-250个字"}).inputValidator({min:1,max:250,onErrorMin:'请输入网站网址',onError:"输入的网站网址长度不合法"});
	$("#txt_logoUrl").formValidator({empty:true,onFocus:"网站logo为小1-250个字"}).inputValidator({max:250,onError:"输入的网站logo长度不合法"});
	$("#txt_upLoadImgType").formValidator({onShowText:"请输入图片格式",onFocus:"以“,”分隔，如jpg,png"}).inputValidator({min:1,max:120,onErrorMin:'请输入图片格式',onError:"输入的图片格式长度不合法"});
	$("#txt_uploadFileType").formValidator({onShowText:"请输入文件格式",onFocus:"以“,”分隔，如doc,rar"}).inputValidator({min:1,max:120,onErrorMin:'请输入文件格式',onError:"输入的文件格式长度不合法"});
	$("#txt_copyright").formValidator({empty:true,onFocus:"版权信息为小于250个字"}).inputValidator({max:250,onError:"输入的版权信息长度不合法"});
	$("#txt_metaKeywords").formValidator({empty:true,onFocus:"网站关键字小于250个字，便于搜索引擎搜索"}).inputValidator({max:250,onError:"输入的网站关键字长度不合法"});
	$("#txt_metaDescription").formValidator({empty:true,onFocus:"网站描述小于250个字，便于搜索引擎搜索"}).inputValidator({max:250,onError:"输入的网站描述长度不合法"});
	$("#txt_errorPagePath").formValidator({empty:true,onFocus:"如果不填则为系统错误页面"}).inputValidator({max:250,onError:"输入的错误页面路径长度不合法"});
	$("#txt_imgThumbWidth").formValidator({empty:true,onFocus:"请输入缩略图宽"}).inputValidator({max:4,onError:"数量超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_imgThumbHeight").formValidator({empty:true,onFocus:"请输入缩略图高"}).inputValidator({max:4,onError:"数量超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});

	$('#returnWebConfig').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('0');
	});
	var getData = function(){
		var data = {
					siteName:$('#txt_siteName').val(),
					siteUrl:$('#txt_siteUrl').val(),
					logoUrl:$('#txt_logoUrl').val(),
					copyright:$('#txt_copyright').val(),
					metaKeywords:$('#txt_metaKeywords').val(),
					metaDescription:$('#txt_metaDescription').val(),
					upLoadImgType:$('#txt_upLoadImgType').val(),
					uploadFileType:$('#txt_uploadFileType').val(),
					imgThumbWidth:$('#txt_imgThumbWidth').val(),
					imgThumbHeight:$('#txt_imgThumbHeight').val(),
					errorPagePath:$('#txt_errorPagePath').val()
					};
		 return data;
	};
});
