$(document).ready(function(){
	$.formValidator.initConfig({formID:"receptionPageUrlForm",theme:"Small",
		onError:function(msg,obj,errorlist){
		}
	});
	beautifyBottomButton(['saveReceptionPageUrl','updateReceptionPageUrl','closeReceptionPageUrl']);
	$("#saveReceptionPageUrl").click(function(){
		var isOk = $.formValidator.pageIsValid('1');
		if(!isOk){
			return;
		}
		var _data = getData();
		if($('#select_webList option').length<=0){
			alert('网站信息不存在，请先新增站点信息！');
			return;
		}
		_data['siteId']=$('#select_webList').find("option:selected").eq(0).attr('value');
		var waiting = alertWaiting('正在提交数据，请等待...');
		$.ajax({
			type:"POST",
			url:contextPath+'/security/addReceptionPageUrl.html',
			data:_data,
			dataType:'json',
			contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			success: function(data){
				waiting.close();
				if(data.code=='200'){
					alert(data.content);
					window.top.frames['iframeMenuContent'].callBackResult('1');
				}
				else{
					alert(data.content);
				}
			}
		});
	});
	$('#updateReceptionPageUrl').click(function(){
		var isOk = $.formValidator.pageIsValid('1');
		if(!isOk){
			return;
		}
		var waiting = alertWaiting('正在提交数据，请等待...');
		var data = getData();
		data['id'] = $('#txt_pageUrlId').val();
		$.ajax({
			type:"POST",
			url:contextPath+'/security/updateReceptionPageUrl.html',
			data:data,
			dataType:'json',
			contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			success: function(data){
				waiting.close();
				if(data.code=='200'){
					alert(data.content);
					window.top.frames['iframeMenuContent'].callBackResult('1');
				}
				else{
					alert(data.content);
				}
			}
		});
	});
	$("#txt_nickName").formValidator({onShowText:"请前台Url别称",onFocus:"前台Url别称为小1-30个英文"}).inputValidator({min:1,max:30,onErrorMin:'请输入前台Url别称',onError:"输入的前台Url别称长度不合法"});
	$("#txt_jspPath").formValidator({onShowText:"请输入Jsp页面相对地址",onFocus:"Jsp页面为小1-250个字"}).inputValidator({min:1,max:250,onErrorMin:'请输入Jsp页面相对地址',onError:"输入的Jsp页面相对地址长度不合法"});
	$("#txt_intro").formValidator({empty:true,onFocus:"前台Url描述小于250个字"}).inputValidator({max:250,onError:"输入的前台Url描述长度不合法"});
	$('#closeReceptionPageUrl').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('0');
	});
	var getData = function(){
		var data = {
					nickName:$('#txt_nickName').val(),
					jspPath:$('#txt_jspPath').val(),
					intro:$('#txt_intro').val(),
					urlType:$('#td_urlType').find('input:checked').eq(0).attr('value')};
		 return data;
	};
	$('input[name="rd_pageType"]').change(function(){
		var value = $(this).attr('value');
		if(value=='1'){
			$('#txt_nickName').val('index').attr('disabled','disabled');
		}
		else{
			$('#txt_nickName').val('').removeAttr('disabled','disabled');
		}
		$.formValidator.pageIsValid('1');
	});
});
