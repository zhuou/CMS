var _gPicDialog;
$(document).ready(function(){
	beautifyTopButton(['pic_Add','pic_Update','pic_Recycle','pic_Del','pic_Search','pic_Restore','pic_Draft','pic_Passed','pic_CancelPassed','pic_WaitPass']);
	beautifyTable('tbody_DataList');
	checkBoxFunction.init('cb_CheckedAll','tbody_DataList');
	initTab();

	$('#pic_Add').click(function(){
		_gPicDialog=art.dialog.open(contextPath+'/security/loadAddPic.html',{id:'dialogWindow_AddPic',height:'92%',width:'80%',title:'新增图片',padding: 0,lock:true},false);
	});
	$('#pic_Update').click(function(){
		var array = checkBoxFunction.getId();
		if(array.length<=0){
			alert('请选择要操作项！');
			return;
		}
		if(array.length>1){
			alert('请选择一条记录修改！');
			return;
		}
		_gPicDialog=art.dialog.open(contextPath+'/security/loadUpdatePic.html?id='+array.join(''),{id:'dialogWindow_AddPic',height:'92%',width:'80%',title:'修改图片',padding: 0,lock:true},false);
	});
	$('#pic_Recycle').click(function(){
		updateStatus('-3');
	});
	$('#pic_Del').click(function(){
		var array = checkBoxFunction.getId();
		if(array.length<=0){
			alert('请选择要操作项！');
			return;
		}
		var isOK = confirm('您确定要删除选中的记录吗？');
		if(isOK){
			var waiting = alertWaiting('请等待...');
			$.post(contextPath+'/security/delPic.html',{ids:array.join(',')},function(data){
				waiting.close();
				if(data.code=='200'){
					alert(data.content);
					window.location.reload();
				}
				else{
					alert(data.content);
				}
			},'json');
		}
	});
	$('#pic_Restore').click(function(){
		updateStatus('0');
	});
	$('#pic_WaitPass').click(function(){
		updateStatus('0');
	});
	$('#pic_Passed').click(function(){
		updateStatus('99');
	});
	$('#pic_Draft').click(function(){
		updateStatus('-1');
	});
	$('#pic_CancelPassed').click(function(){
		updateStatus('0');
	});
	function updateStatus(status){
		var array = checkBoxFunction.getId();
		if(array.length<=0){
			alert('请选择要操作项！');
			return;
		}
		$.post(contextPath+'/security/updateStatus.html',{status:status,ids:array.join(',')},function(data){
			if(data.code=='200'){
				alert(data.content);
				window.location.reload();
			}
			else{
				alert(data.content);
			}
		},'json');
	};
	$('#pic_Search').click(function(){
		var txt = $.trim($('#pic_SearchText').val());
		var searchType = $.trim($('#select_SearchKey').val());
		if(searchType==''){
			return;
		}
		parseUri.setParam('searchType',searchType);
		window.location.href = parseUri.setParam('keyword',txt);
	});
	$('#select_SearchConditions').change(function(){
		var val = $('#select_SearchConditions').val();
		var array = val.split('_');
		parseUri.setParam('orderField',array[0]);
		window.location.href = parseUri.setParam('orderType',array[1]);
	});
	$('#ddl_ChannelList').change(function(){
		var val = $('#ddl_ChannelList').val();
		if(val!=='0'){
			window.location.href = parseUri.setParam('channelId',val);
		}
		else{
			window.location.href = parseUri.setParam('channelId','');
		}
	});
});
var initTab = function(){
	var checkedId = 'td_tab1';
	var status = parseUri.getParam('status');
	if(status=='-1'){
		checkedId = 'td_tab2';
	}
	else if(status=='0'){
		checkedId = 'td_tab3';
	}
	else if(status=='99'){
		checkedId = 'td_tab4';
	}
	else if(status=='-3'){
		checkedId = 'td_tab5';
	}
	else if(status=='999'){
		checkedId = 'td_tab1';
	}
	if('td_tab1'!=checkedId){
		$("#"+checkedId).attr("class", "titlemouseover");
        $("#td_tab1").attr("class", "tabtitle");
	}

	$('#td_tab1').click(function(){
		changeUrl('td_tab1','999');
	});
	$('#td_tab2').click(function(){
		changeUrl('td_tab2','-1');
	});
	$('#td_tab3').click(function(){
		changeUrl('td_tab3','0');
	});
	$('#td_tab4').click(function(){
		changeUrl('td_tab4','99');
	});
	$('#td_tab5').click(function(){
		changeUrl('td_tab5','-3');
	});
	function changeUrl(id,status){
		if(checkedId!=id){
			parseUri.clear();
			window.location.href = parseUri.setParam('status',status);
		}
	}
};
function picDetail(_id){
	_gPicDialog=art.dialog.open(contextPath+'/security/picDetail.html?id='+_id,{id:'dialogWindow_AddPic',height:'92%',width:'80%',title:'图片详情',padding: 0,lock:true},false);
};
function callBackResult(resultFlag){
	_gPicDialog.close();
	if(resultFlag=='1'){
		window.location.reload();
	}
};
function toUpdatePage(_aId){
	_gPicDialog.close();
	_gPicDialog=art.dialog.open(contextPath+'/security/loadUpdatePic.html?id='+_aId,{id:'dialogWindow_AddPic',height:'92%',width:'80%',title:'修改图片',padding: 0,lock:true},false);
};