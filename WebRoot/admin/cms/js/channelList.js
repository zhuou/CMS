var _gChannelDialog;
$(document).ready(function(){
	beautifyTopButton(['channel_Add','channel_Del','channel_Update','channel_Search']);
	beautifyTable('tbody_DataList');
	beautifyInputText(['channel_SearchText']);
	checkBoxFunction.init('cb_CheckedAll','tbody_DataList');
	$('#channel_Del').click(function(){
		var isOK = confirm('删除后数据将不可恢复，您确定要删除选中的记录吗？');
		if(isOK){
			var waiting = alertWaiting('请等待...');
			var _url = contextPath+'/security/delChannel.html';
			$.post(_url,{ids:checkBoxFunction.getId().join(',')},function(data){
				waiting.close();
				if(data.code=='200'){
					alert(data.content);
					window.location.reload();
				}
				else{
					alert(data.content);
				}
			},'json');
		}
	});
	$('#channel_Add').click(function(){
		var content = new Array();
		content.push('<div class="aui_content" style="padding: 0px;"><div class="divSys"><ul>');
		content.push('<li><a onclick="goAddChannelPage(0);"><img alt="" src="'+contextPath+'/admin/imgs/blue/channel_contain.png"></a>');
		content.push('<div><a onclick="goAddChannelPage(0);">添加内容栏目</a></div></li>');
		content.push('<li><a onclick="goAddChannelPage(2);"><img alt="" src="'+contextPath+'/admin/imgs/blue/channel_single.png"></a>');
		content.push('<div><a onclick="goAddChannelPage(2);">添加单页栏目</a></div></li>');
		content.push('<li><a onclick="goAddChannelPage(3);"><img alt="" src="'+contextPath+'/admin/imgs/blue/channel_external.png"></a>');
        content.push('<div><a onclick="goAddChannelPage(3);">添加外部链接栏目</a></div></li>');
		content.push('</ul></div></div>');
		art.dialog({padding: 0,title: '请选择要添加的栏目类型',content: content.join(''),lock: true,drag:false});
	});
	$('#channel_Update').click(function(){
		var array = checkBoxFunction.getId();
		if(array.length<=0){
			alert('请选择修改项！');
			return;
		}
		if(array.length>1){
			alert('每次只能选择一个修改项！');
			return;
		}
		var str = array.join('').split('_');
		if(0==str[0]){
			_gChannelDialog = art.dialog.open(contextPath+'/security/loadUpdateContainCh.html?id='+str[1],{id:'dialogWindow_AddChannel',height:'520px',width:'900px',title:'添加容器栏目',padding: 0,lock:true},false);
		}
		else if(2==str[0]){
			_gChannelDialog = art.dialog.open(contextPath+'/security/loadUpdateSingleCh.html?id='+str[1],{id:'dialogWindow_AddChannel',height:'520px',width:'940px',title:'添加单页栏目',padding: 0,lock:true},false);
		}
		else{
			_gChannelDialog = art.dialog.open(contextPath+'/security/loadUpdateExternalCh.html?id='+str[1],{id:'dialogWindow_AddChannel',height:'380px',width:'890px',title:'添加外部链接栏目',padding: 0,lock:true},false);
		}
	});
	$('#channel_Search').click(function(){
		var v = $('#channel_SearchText').val();
		var dv = $('#channel_SearchText').attr('alt');
		if(v == dv){
			v = '';
		}
		parseUri.setParam('pageNum',1)
		window.location.href = parseUri.setParam('keyword',v);
	});
	$('a[name*="aChannelDetail_"]').live("click",function(){
		var $name = $(this).attr('name');
		var nameArr = $name.split('_');
		goDetail(nameArr[1],nameArr[2]);
	});
});
function goAddChannelPage(_channelType){
	if(_channelType==0){
		_gChannelDialog = art.dialog.open(contextPath+'/security/loadAddContainCh.html',{id:'dialogWindow_AddChannel',height:'520px',width:'900px',title:'添加容器栏目',padding: 0,lock:true},false);
	}
	else if(_channelType==2){
		_gChannelDialog = art.dialog.open(contextPath+'/security/loadAddSingleCh.html',{id:'dialogWindow_AddChannel',height:'520px',width:'940px',title:'添加单页栏目',padding: 0,lock:true},false);
	}
	else if(_channelType==3){
		_gChannelDialog = art.dialog.open(contextPath+'/security/loadAddExternalCh.html',{id:'dialogWindow_AddChannel',height:'350px',width:'890px',title:'添加外部链接栏目',padding: 0,lock:true},false);
	}
};
function goDetail(id,type){
	if(type==0){
		_gChannelDialog = art.dialog.open(contextPath+'/security/containChDetail.html?id='+id,{id:'dialogWindow_ChannelDetail',height:'520px',width:'900px',title:'添加容器栏目',padding: 0,lock:true},false);
	}
	else if(type==2){
		_gChannelDialog = art.dialog.open(contextPath+'/security/singleChDetail.html?id='+id,{id:'dialogWindow_ChannelDetail',height:'520px',width:'940px',title:'添加单页栏目',padding: 0,lock:true},false);
	}
	else{
		_gChannelDialog = art.dialog.open(contextPath+'/security/externalChDetail.html?id='+id,{id:'dialogWindow_ChannelDetail',height:'380px',width:'890px',title:'添加外部链接栏目',padding: 0,lock:true},false);
	}
};
function callBackResult(resultFlag){
	_gChannelDialog.close();
	if(resultFlag=='1'){
		window.location.reload();
	}
};