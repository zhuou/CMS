var _gDialogFriendSite;
$(document).ready(function(){
	beautifyTopButton(['friendSite_Add','friendSite_Update','friendSite_Recycle','friendSite_Del','friendSite_Search','friendSite_Restore','friendSite_Draft','friendSite_Passed','friendSite_CancelPassed','friendSite_WaitPass']);
	beautifyTable('tbody_DataList');
	checkBoxFunction.init('cb_CheckedAll','tbody_DataList');
	initTab();

	$('#friendSite_Add').click(function(){
		_gDialogFriendSite=art.dialog.open(contextPath+'/security/loadAddFriendSite.html',{id:'dialogWindow_FriendSiteDetail',height:'75%',width:'70%',title:'新增友情链接',padding: 0,lock:true},false);
	});
	$('#friendSite_Recycle').click(function(){
		updateStatus('-3');
	});
	$('#friendSite_Del').click(function(){
		var array = checkBoxFunction.getId();
		if(array.length<=0){
			alert('请选择要操作项！');
			return;
		}
		var waiting = alertWaiting('正在提交数据，请等待...');
		$.post(contextPath+'/security/delFriendSite.html',{ids:array.join(',')},function(data){
			waiting.close();
			if(data.code=='200'){
				alert(data.content);
				window.location.reload();
			}
			else{
				alert(data.content);
			}
		},'json');
	});
	$('#friendSite_Restore').click(function(){
		updateStatus('0');
	});
	$('#friendSite_WaitPass').click(function(){
		updateStatus('0');
	});
	$('#friendSite_Passed').click(function(){
		updateStatus('99');
	});
	$('#friendSite_Draft').click(function(){
		updateStatus('-1');
	});
	$('#friendSite_CancelPassed').click(function(){
		updateStatus('0');
	});
	function updateStatus(status){
		var array = checkBoxFunction.getId();
		if(array.length<=0){
			alert('请选择要操作项！');
			return;
		}
		var waiting = alertWaiting('正在提交数据，请等待...');
		$.post(contextPath+'/security/updateStatus.html',{status:status,ids:array.join(',')},function(data){
			waiting.close();
			if(data.code=='200'){
				alert(data.content);
				window.location.reload();
			}
			else{
				alert(data.content);
			}
		},'json');
	};
	$('#friendSite_Search').click(function(){
		var txt = $.trim($('#friendSite_SearchText').val());
		var searchType = $.trim($('#select_SearchKey').val());
		if(searchType==''){
			return;
		}
		parseUri.setParam('searchType',searchType);
		window.location.href = parseUri.setParam('keyword',txt);
	});
	$('#select_SearchConditions').change(function(){
		var val = $('#select_SearchConditions').val();
		var array = val.split('_');
		parseUri.setParam('orderField',array[0]);
		window.location.href = parseUri.setParam('orderType',array[1]);
	});
	$('#ddl_ChannelList').change(function(){
		var val = $('#ddl_ChannelList').val();
		if(val!=='0'){
			window.location.href = parseUri.setParam('channelId',val);
		}
		else{
			window.location.href = parseUri.setParam('channelId','');
		}
	});
	$('#friendSite_Update').click(function(){
		var array = checkBoxFunction.getId();
		if(array.length<=0){
			alert('请选择要操作项！');
			return;
		}
		if(array.length>1){
			alert('每次只能选择一个修改项！');
			return;
		}
		_gDialogFriendSite=art.dialog.open(contextPath+'/security/loadUpdateFriendSite.html?id='+array.join(''),{id:'dialogWindow_FriendSiteDetail',height:'75%',width:'70%',title:'修改友情链接',padding: 0,lock:true},false);
	});
	$('a[name="a_ToFriendSiteDetail"]').click(function(){
		var _id = $(this).attr('id');
		_gDialogFriendSite=art.dialog.open(contextPath+'/security/friendSiteDetail.html?id='+_id.split('_')[1],{id:'dialogWindow_FriendSiteDetail',height:'75%',width:'65%',title:'友情链接详情',padding: 0,lock:true},false);
	});
});
var initTab = function(){
	var checkedId = 'td_tab1';
	var status = parseUri.getParam('status');
	if(status=='-1'){
		checkedId = 'td_tab2';
	}
	else if(status=='0'){
		checkedId = 'td_tab3';
	}
	else if(status=='99'){
		checkedId = 'td_tab4';
	}
	else if(status=='-3'){
		checkedId = 'td_tab5';
	}
	else if(status=='999'){
		checkedId = 'td_tab1';
	}
	if('td_tab1'!=checkedId){
		$("#"+checkedId).attr("class", "titlemouseover");
        $("#td_tab1").attr("class", "tabtitle");
	}

	$('#td_tab1').click(function(){
		changeUrl('td_tab1','999');
	});
	$('#td_tab2').click(function(){
		changeUrl('td_tab2','-1');
	});
	$('#td_tab3').click(function(){
		changeUrl('td_tab3','0');
	});
	$('#td_tab4').click(function(){
		changeUrl('td_tab4','99');
	});
	$('#td_tab5').click(function(){
		changeUrl('td_tab5','-3');
	});
	function changeUrl(id,status){
		if(checkedId!=id){
			parseUri.clear();
			window.location.href = parseUri.setParam('status',status);
		}
	}
};
function callBackResult(resultFlag){
	_gDialogFriendSite.close();
	if(resultFlag=='1'){
		window.location.reload();
	}
};
function toUpdatePage(_aId){
	_gDialogFriendSite.close();
	_gDialogFriendSite=art.dialog.open(contextPath+'/security/loadUpdateFriendSite.html?id='+_aId,{id:'dialogWindow_FriendSiteDetail',height:'75%',width:'70%',title:'修改友情链接',padding: 0,lock:true},false);
};