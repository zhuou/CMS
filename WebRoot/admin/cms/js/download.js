var gDialogChannel;
$(document).ready(function(){
	beautifyBottomButton(['bnt_checked','bnt_selectImg','bnt_review','bnt_save','bnt_back','bnt_addExternalUrl','bnt_remove','bnt_nextMove','bnt_prevMove','bnt_update']);
	pageTab({'td_tab1':'tb_Pannel1','td_tab2':'tb_Pannel2','td_tab3':'tb_Pannel3'});
	$.formValidator.initConfig({formID:"downloadForm",theme:"Default",
		onError:function(msg,obj,errorlist){
			alertMessage(msg);
		}
	});
	$("#txt_CheckChannelName").formValidator({onShowText:""});
	$("#txt_title").formValidator({onShowText:"请输入下载文件标题",onFocus:"下载文件标题为1-200个字"}).inputValidator({min:1,max:200,onErrorMin:'请输入下载文件标题',onError:"输入的下载文件标题长度不合法"});
	$("#txt_keyword").formValidator({empty:true,onFocus:"下载文件关键字为1-200个字"}).inputValidator({min:1,max:200,onErrorMin:'请输入下载文件关键字',onError:"输入的下载文件关键字长度不合法"});
	$("#txt_author").formValidator({empty:true,onFocus:"下载文件作者为1-100个字"}).inputValidator({min:1,max:100,onErrorMin:'请输入下载文件作者',onError:"输入的下载文件作者长度不合法"});
	$("#txt_outTime").formValidator({empty:true,onFocus:"请输入有效期天数"}).inputValidator({max:9,onError:"有效期天数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_defaultPicUrl").formValidator({empty:true});
	$("#txt_fileSize").formValidator({empty:true,onFocus:"请填写文件大小，如：10M"}).inputValidator({max:10,onError:"最大输入10个字符"});
	$("#txt_decompressPassword").formValidator({empty:true,onFocus:"请填写解压密码，如：123456"}).inputValidator({max:10,onError:"解压密码最大输入10个字符"});

	$("#txt_fileVersion").formValidator({empty:true,onFocus:"请填写软件版本，如：1.0版本"}).inputValidator({max:10,onError:"软件版本最大输入10个字符"});
	$("#txt_copyFrom").formValidator({empty:true,onFocus:"请填写软件来源，如：微软公司"}).inputValidator({max:20,onError:"软件来源最大输入20个字符"});
	$("#txt_demoUrl").formValidator({empty:true,onFocus:"请填写软件演示地址，如：www.163.com"}).inputValidator({max:100,onError:"软件演示地址最大输入10个字符"});
	$("#txt_regUrl").formValidator({empty:true,onFocus:"请填写软件注册地址，如：www.163.com"}).inputValidator({max:100,onError:"软件注册地址最大输入10个字符"});
	$("#txt_operatingSystem").formValidator({empty:true,onFocus:"请选择软件运行平台"}).inputValidator({max:100,onError:"请选择软件运行平台"});

	$("#txt_totlaHits").formValidator({empty:true,onFocus:"可以预置总点击数"}).inputValidator({max:9,onError:"总点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_dayHits").formValidator({empty:true,onFocus:"可以预置日点击数"}).inputValidator({max:9,onError:"日点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_weekHits").formValidator({empty:true,onFocus:"可以预置周点击数"}).inputValidator({max:9,onError:"周点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_monthHits").formValidator({empty:true,onFocus:"可以预置月点击数"}).inputValidator({max:9,onError:"月点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_eliteLevel").formValidator({empty:true,onFocus:"请输入推荐级"}).formValidator({empty:true,onFocus:"推荐级别数越大显示就越靠前"}).inputValidator({max:9,onError:"推荐级数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_priority").formValidator({empty:true,onFocus:"请输入优先级"}).formValidator({empty:true,onFocus:"优先级数越大显示就越靠前"}).inputValidator({max:9,onError:"优先级数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$('#bnt_checked').click(function(){
        gDialogChannel = art.dialog.open(contextPath+'/security/getChannel2.html?forums=-3&iframe=OpendialogWindow_DownloadDetail', { height: '80%', width: '20%', title: "请选择栏目", lock: true }, false);//打开子窗体
	});
	$('#bnt_review').click(function(){
		var src = $.trim($('#txt_defaultPicUrl').val());
		if(src==''){
			alert('请输入图片地址');
			return;
		}
		art.dialog({padding: 0,title: '图片预览',content: '<img src="'+src+'" />',lock: true});
	});
	$('input[name="bnt_selectImg"]').live('click',function(){
		 var callback = $(this).attr('callback');
		 gDialogChannel = art.dialog.open(contextPath+'/security/getUploadImg.html?iframe=OpendialogWindow_DownloadDetail&callback='+callback, { height: '400px', width: '550px', title: "请双击选择图片", lock: true }, false);
	});
	$('#bnt_back').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('1');
	});
	UE.getEditor('editor');
	$.fn.initPage(alertWaiting);
});
(function($){
	function getData(){
		var data = {'isSuccess':false,data:{}};
		var channelId = $.trim($('#txt_channelId').val());
		if(channelId==''){
			alert('没有获取栏目Id，请和管理员联系');
			return data;
		}
		data.data['channelId'] = channelId;
		var title = $.trim($('#txt_title').val());
		data.data['title'] = title;
		var keyWord = $.trim($('#txt_keyword').val());
		data.data['keyword'] = keyWord;
		var author = $.trim($('#txt_author').val());
		data.data['author'] = author;

		var fileIntro = UE.getEditor('editor').getContent();
		data.data['fileIntro'] = fileIntro;
		var defaultPicUrl = $.trim($('#txt_defaultPicUrl').val());
		data.data['defaultPicUrl'] = defaultPicUrl;

		var aUrl = new Array();
		$('#select_downloadUrl option').each(function(i){
			var url = $(this).text();
	        if(url!=""){
	            aUrl.push(url);
	        }
    	});
    	if(aUrl.join('')==''){
    		alert('下载地址不能为空，请填写下载地址');
			return data;
    	}
    	data.data['downloadUrl'] = aUrl.join('|$|');
    	var fileSize = $.trim($('#txt_fileSize').val());
    	data.data['fileSize'] = fileSize;
    	var decompressPassword = $.trim($('#txt_decompressPassword').val());
    	data.data['decompressPassword'] = decompressPassword;
    	var fileVersion = $.trim($('#txt_fileVersion').val());
    	data.data['fileVersion'] = fileVersion;
    	var copyFrom = $.trim($('#txt_copyFrom').val());
    	data.data['copyFrom'] = copyFrom;
		var demoUrl = $.trim($('#txt_demoUrl').val());
		data.data['demoUrl'] = demoUrl;
		var operatingSystem = $.trim($('#txt_operatingSystem').val());
		data.data['operatingSystem'] = operatingSystem;
		var fileType = $("#select_fileType").find("option:selected").text();
		data.data['fileType'] = fileType;
		var fileLanguage = $("#select_fileLanguage").find("option:selected").text();
		data.data['fileLanguage'] = fileLanguage;
		var copyrightType = $("#select_copyrightType").find("option:selected").text();
		data.data['copyrightType'] = copyrightType;
		var regUrl= $.trim($('#txt_regUrl').val());
		data.data['regUrl'] = regUrl;

		var status = 99;
		if($('#rd_Status_1').is(":checked")){
			status = -1;
		}
		else if($('#rd_Status_2').is(":checked")){
			status = 0;
		}
		data.data['status'] = status;

		var hits = $.trim($('#txt_totlaHits').val());
		data.data['hits'] = hits;
		var dayHits = $.trim($('#txt_dayHits').val());
		data.data['dayHits'] = dayHits;
		var weekHits = $.trim($('#txt_weekHits').val());
		data.data['weekHits'] = weekHits;
		var monthHits = $.trim($('#txt_monthHits').val());
		data.data['monthHits'] = monthHits;
		var updateTime = $.trim($('#txt_updataTime').val());
		data.data['updateTime'] = updateTime;
		var eliteLevel = $.trim($('#txt_eliteLevel').val());
		data.data['eliteLevel'] = eliteLevel;
		var priority = $.trim($('#txt_priority').val());
		data.data['priority'] = priority;
		data['isSuccess']=true;
		return data;
	};
	var View={
		method:'add',
		init:function(alertWaiting){
			$('#bnt_addExternalUrl').click(function(){
				var url=window.prompt("请输入外部地址:");
			    if($.trim(url)==""){
			        return;
			    }
			    $("#select_downloadUrl").append("<option>"+url+"</option>")
			});
			$('#bnt_remove').click(function(){
				if($("#select_downloadUrl").get(0).selectedIndex<0){
			        alert("请选择要删除的项！");
			        return;
			    }
			    $("#select_downloadUrl").find("option:selected").remove();
			});
			$('#bnt_nextMove').click(function(){
				var obj = $("#select_downloadUrl");
			    var sel = obj.find("option:selected");
			    if (sel.length == 0) {
			        alert("请选择要移动的项！");
			        return;
			    }
				sel.insertAfter(sel.next());
			});
			$('#bnt_prevMove').click(function(){
				var obj = $("#select_downloadUrl");
			    var sel = obj.find("option:selected");
			    if (sel.length == 0) {
			        alert("请选择要移动的项！");
			        return;
			    }
				sel.insertBefore(sel.prev());
			});

			var Ajax = {
				submit:function(url,id){
					var isOk = $.formValidator.pageIsValid('1');
					if(!isOk){
						return;
					}
					var pageData = getData();
					if(!pageData.isSuccess){
						return;
					}
					var waiting = alertWaiting('正在提交数据，请等待...');
					pageData.data['commonModelId'] = id;
					$.ajax({
						type:"POST",
						url:url,
						data:pageData.data,
						dataType:'json',
						contentType:'application/x-www-form-urlencoded; charset=UTF-8',
						success: function(data){
							waiting.close();
							if(data.code=='200'){
								if(reqMethod == View.method){
									if(confirm(data.content+'，是否继续新增下载？')){
										document.getElementById("downloadForm").reset();
										UE.getEditor('editor').setContent('',false);
									}
									else{
										window.top.frames['iframeMenuContent'].callBackResult('1');
									}
								}
								else{
									alert(data.content);
									window.top.frames['iframeMenuContent'].callBackResult('1');
								}
							}
							else{
								alertMessage(data.content);
							}
						}
					});
				}
			};

			if(reqMethod == this.method){
				$("#bnt_save").click(function(){
					Ajax.submit(contextPath+'/security/addDownload.html',0);
				});
			}
			else{
				$("#bnt_update").click(function(){
					var commonModelId = $('#txt_commonModelId').val();
					Ajax.submit(contextPath+'/security/updateDownload.html',commonModelId);
				});
			}
		}
	};
	$.fn.initPage = function(alertWaiting){
		View.init(alertWaiting);
	};
})(jQuery);
function checkSystemName(obj){
	var text = $(obj).text();
	var val = $.trim($("#txt_operatingSystem").val());
	if(val!=''){
		text = val+','+text;
	}
    $("#txt_operatingSystem").val(text);
};
function callBackChannelInfo(_id,_name,_type){
	$('#txt_CheckChannelName').val(_name);
	$('#txt_channelId').val(_id);
	gDialogChannel.close();
};
function callbackDefaultPic(_relativePath){
	$('#txt_defaultPicUrl').val(_relativePath);
	if(typeof(gDialogChannel)!='undefined'&&gDialogChannel!=null){
		gDialogChannel.close();
	}
};