var _gDialogAdZone;
$(document).ready(function(){
	beautifyTopButton(['ad_Add','ad_Update','ad_Del']);
	beautifyTable('tbody_DataList');
	checkBoxFunction.init('cb_CheckedAll','tbody_DataList');
	$.fn.initPage(alertWaiting);
	$.each($('td[name="txt_adIntro"]').find('img'),function(i,eleObj){
		eleObj.onload = function () {
            var w = this.width, h = this.height;
            scale(this, 100, 120, 80);
        };
	});
	function scale(img, max, oWidth, oHeight) {
        var width = 0, height = 0, percent, ow = img.width || oWidth, oh = img.height || oHeight;
        if (ow > max || oh > max) {
            if (ow >= oh) {
                if (width = ow - max) {
                    percent = (width / ow).toFixed(2);
                    img.height = oh - oh * percent;
                    img.width = max;
                }
            } else {
                if (height = oh - max) {
                    percent = (height / oh).toFixed(2);
                    img.width = ow - ow * percent;
                    img.height = max;
                }
            }
        }
    };
});
(function($){
	var View={
		init:function(alertWaiting){
			$('#ad_Add').click(function(){
				_gDialogAdZone = art.dialog.open(contextPath+'/security/loadAddAd.html',{id:'dialogWindow_AdDetail',height:'540px',width:'750px',title:'新增广告',padding: 0,lock:true},false);
			});
			$('#ad_Update').click(function(){
				var array = checkBoxFunction.getId();
				if(array.length<=0){
					alert('请选择要操作项！');
					return;
				}
				if(array.length>1){
					alert('请选择一条记录修改！');
					return;
				}
				_gDialogAdZone = art.dialog.open(contextPath+'/security/loadUpdateAd.html?id='+array.join(''),{id:'dialogWindow_AdDetail',height:'540px',width:'750px',title:'修改广告',padding: 0,lock:true},false);
			});
			$('a[name*="detail_"]').click(function(){
				var _zoneId = $(this).attr('name');
				_gDialogAdZone = art.dialog.open(contextPath+'/security/getAdDetail.html?id='+_zoneId.split('_')[1],{id:'dialogWindow_AdDetail',height:'500px',width:'750px',title:'广告详情',padding: 0,lock:true},false);
			});
			$('a[name="aSetPassed"]').click(function(){
				var adId = $(this).attr('id');
				var passed = $(this).attr('value');
				$.post(contextPath+'/security/updateAdStatus.html',{id:adId,passed:passed},function(data){
					if(data.code=='200'){
						alert(data.content);
						window.location.reload();
					}
					else{
						alert(data.content);
					}
				},'json');
			});
			$('#ad_Del').click(function(){
				var array = checkBoxFunction.getId();
				if(array.length<=0){
					alert('请选择要操作项！');
					return;
				}
				var isOK = confirm('您确定要删除选中记录吗？');
				if(isOK){
					$.post(contextPath+'/security/deAd.html',{ids:array.join(',')},function(data){
						if(data.code=='200'){
							alert(data.content);
							window.location.reload();
						}
						else{
							alert(data.content);
						}
					},'json');
				}
			});
		}
	};
	$.fn.initPage = function(alertWaiting){
		View.init(alertWaiting);
	};
})(jQuery);
function callBackResult(resultFlag){
	_gDialogAdZone.close();
	if(resultFlag=='1'){
		window.location.reload();
	}
};