$(document).ready(function(){
	beautifyBottomButton(['bnt_add','bnt_back','bnt_update']);
	$.formValidator.initConfig({formID:"guestBookForm",theme:"Small",
		onError:function(msg,obj,errorlist){
		}
	});
	$("#txt_replyName").formValidator({empty:true,onFocus:"请输入回复人名称"}).inputValidator({max:20,onErrorMin:'名称应小于25个字符'});
	$('#bnt_back').click(function(){
		window.top.frames[window.iframeName].closeDialog();
	});
	UE.getEditor('txt_replyContent');
	$.fn.initPage(alertWaiting);
});
(function($){
	function getData(){
		var data = {'isSuccess':false,data:{}};
		var content = UE.getEditor('txt_replyContent').getContent();
		if(content==''){
			alert('留言内容不能为空，请填写');
			return data;
		}
		data.data['replyContent'] = content;
		data.data['guestBookId'] = _guestBookId;
		var replyName = $.trim($('#txt_replyName').val());
		data.data['replyName'] = replyName;
		var replyTime = $.trim($('#txt_replyTime').val());
		data.data['replyTime'] = replyTime;

		data['isSuccess']=true;
		return data;
	};
	var View={
		method:'add',
		init:function(alertWaiting){
			var Ajax = {
				submit:function(url,_replyId){
					var isOk = $.formValidator.pageIsValid('1');
					if(!isOk){
						return;
					}
					var pageData = getData();
					pageData.data['replyId'] = _replyId;
					if(!pageData.isSuccess){
						return;
					}
					var waiting = alertWaiting('正在提交数据，请等待...');
					$.ajax({
						type:"POST",
						url:url,
						data:pageData.data,
						dataType:'json',
						contentType:'application/x-www-form-urlencoded; charset=UTF-8',
						success: function(data){
							waiting.close();
							if(data.code=='200'){
								alert(data.content);
								window.top.frames[window.iframeName].closeDialog();
							}
							else{
								alert(data.content);
							}
						}
					});
				}
			};

			if(reqMethod == this.method){
				$("#bnt_add").click(function(){
					Ajax.submit(contextPath+'/security/addGuestBookReply.html',0);
				});
			}
			else{
				$("#bnt_update").click(function(){
					var _replyId = $.trim($('#txt_replyId').val());
					Ajax.submit(contextPath+'/security/updateGuestBookReply.html',_replyId);
				});
			}
		}
	};
	$.fn.initPage = function(alertWaiting){
		View.init(alertWaiting);
	};
})(jQuery);