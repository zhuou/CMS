$(document).ready(function(){
	$.formValidator.initConfig({formID:"companyForm",theme:"Small",
		onError:function(msg,obj,errorlist){
		}
	});
	beautifyBottomButton(['saveCompany','returnCompany']);
	$("#saveCompany").click(function(){
		var isOk = $.formValidator.pageIsValid('1');
		if(!isOk){
			return;
		}
		var waiting = alertWaiting('正在提交数据，请等待...');
		$.ajax({
			type:"POST",
			url:contextPath+'/security/addCompany.html',
			data:getData(),
			dataType:'json',
			contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			success: function(data){
				waiting.close();
				if(data.code=='200'){
					alert(data.content);
					window.top.frames['iframeMenuContent'].callBackResult('1');
				}
				else{
					alert(data.content);
				}
			}
		});
	});
	$('#updateCompany').click(function(){
		var isOk = $.formValidator.pageIsValid('1');
		if(!isOk){
			return;
		}
		var waiting = alertWaiting('正在提交数据，请等待...');
		var data = getData();
		data['companyId'] = $('#companyId').val();
		$.ajax({
			type:"POST",
			url:contextPath+'/security/updateCompany.html',
			data:data,
			dataType:'json',
			contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			success: function(data){
				waiting.close();
				if(data.code=='200'){
					alert(data.content);
					window.top.frames['iframeMenuContent'].callBackResult('1');
				}
				else{
					alert(data.content);
				}
			}
		});
	});

	$("#companyName").formValidator({onShowText:"请输入公司名称",onFocus:"公司名称为小1-50个字"}).inputValidator({min:1,max:50,onErrorMin:'请输入公司名称',onError:"输入的公司名称长度不合法"});
	$("#referred").formValidator({onShowText:"请输入公司简称",onFocus:"公司简称为小1-50个字"}).inputValidator({min:1,max:50,onErrorMin:'请输入公司简称',onError:"输入的公司简称长度不合法"});
	$("#address").inputValidator({max:125,onError:"详细地址长度超过了125个字"});
	$("#zipCode").formValidator({empty:true,onFocus:'邮政编码格式,如：210046'}).regexValidator({regExp:regexEnum.zipcode,onError:"输入的邮政编码格式不正确"});
	$("#contact").inputValidator({max:64,onError:"联系人长度超过了64个字"});
	$("#telephone").inputValidator({max:32,onError:"公司联系电话长度超过了32个字"});
	$("#email").formValidator({empty:true,onFocus:'邮箱的格式,如：test@163.com'}).regexValidator({regExp:regexEnum.email,onError:"输入的邮箱格式不正确"});
	$("#mobile").inputValidator({max:32,onError:"公司手机号码长度超过了32个字"});
	$("#companyIntr").inputValidator({max:500,onError:"公司简介长度超过了250个字"});
	$("#remark").inputValidator({max:500,onError:"备注长度超过了250个字"});
	$('#ddl_FirstProvince').change(function(){
		var id = $(this).val();
		$.ajax({
	        type:"GET",
	        dataType:'json',
	        url: contextPath+"/security/getCity.html?id="+id,
	        success: function(jsonData,status){
	            if(jsonData.code=='200'){
	            	var array = new Array();
	            	array.push('<option value="0">请选择...</option>');
	            	$.each(jsonData.objContent,function(i,e){
	            		array.push('<option value="'+e.id+'">'+e.name+'</option>');
	            	});
	            	$('#ddl_SecondCity').empty();
	            	$('#ddl_SecondCity').append(array.join(''));
	            }
	            else{
	                alert(jsonData.content);
	            }
	        },
	        error: function(){
	            alert("请求超时，请检查网络！");
	        }
    	});
	});
	$('#ddl_SecondCity').change(function(){
		var id = $(this).val();
		$.ajax({
	        type:"GET",
	        dataType:'json',
	        url: contextPath+"/security/getDistrict.html?id="+id,
	        success: function(jsonData,status){
	            if(jsonData.code=='200'){
	            	var array = new Array();
	            	array.push('<option value="0">请选择...</option>');
	            	$.each(jsonData.objContent,function(i,e){
	            		array.push('<option value="'+e.id+'">'+e.name+'</option>');
	            	});
	            	$('#ddl_ThirdDistrict').empty();
	            	$('#ddl_ThirdDistrict').append(array.join(''));
	            }
	            else{
	                alert(jsonData.content);
	            }
	        },
	        error: function(){
	            alert("请求超时，请检查网络！");
	        }
    	});
	});
	$('#returnCompany').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('0');
	});
	var getData = function(){
		var data = {companyName:$("#companyName").val(),
					referred:$("#referred").val(),
					companyPropertieId:$("#companyPropertieId option:selected").val(),
					companyIndustryId:$("#companyIndustryId option:selected").val(),
					companyScaleId:$("#companyScaleId option:selected").val(),
					province:$("#ddl_FirstProvince option:selected").text(),
					city:$("#ddl_SecondCity option:selected").text(),
					district:$("#ddl_ThirdDistrict option:selected").text(),
					address:$("#address").val(),
					zipCode:$("#zipCode").val(),
					contact:$("#contact").val(),
					telephone:$("#telephone").val(),
					companyIntr:$("#companyIntr").val(),
					email:$("#email").val(),
					mobile:$("#mobile").val(),
					remark:$("#remark").val()
					};
		 return data;
	};
});
