var gDialogChannel;
$(document).ready(function(){
	beautifyBottomButton(['bnt_checked','bnt_save','bnt_back','bt_update']);
	pageTab({'td_tab1':'tb_Pannel1','td_tab2':'tb_Pannel2','td_tab3':'tb_Pannel3'});
	$.formValidator.initConfig({formID:"articleForm",theme:"Default",
		onError:function(msg,obj,errorlist){
		}
	});
	$("#txt_CheckChannelName").formValidator({onShowText:"请选择栏目"});
	$("#txt_title").formValidator({onShowText:"请输入文章标题",onFocus:"文章标题为1-200个字"}).inputValidator({min:1,max:200,onErrorMin:'请输入文章标题',onError:"输入的文章标题长度不合法"});
	$("#txt_titleIntact").formValidator({empty:true,onFocus:"文章完整标题为1-200个字"}).inputValidator({min:1,max:200,onErrorMin:'请输入文章完整标题',onError:"输入的文章完整标题长度不合法"});
	$("#txt_subheading").formValidator({empty:true,onFocus:"文章副标题为1-200个字"}).inputValidator({min:1,max:200,onErrorMin:'请输入文章副标题',onError:"输入的文章副标题长度不合法"});
	$("#txt_keyword").formValidator({empty:true,onFocus:"文章关键字为1-200个字"}).inputValidator({min:1,max:200,onErrorMin:'请输入文章关键字',onError:"输入的文章关键字长度不合法"});
	$("#txt_author").formValidator({empty:true,onFocus:"文章作者为1-100个字"}).inputValidator({min:1,max:100,onErrorMin:'请输入文章作者',onError:"输入的文章作者长度不合法"});
	$("#txt_copyFrom").formValidator({empty:true,onFocus:"文章来源为1-100个字"}).inputValidator({min:1,max:100,onErrorMin:'请输入文章来源',onError:"输入的文章来源长度不合法"});
	$("#txt_intro").formValidator({empty:true,onFocus:"文章简介小于1000个字"}).inputValidator({min:0,max:1000,onError:"输入的文章简介长度不合法"});
	//$("#txt_defaultPicUrl").formValidator({empty:true});
	$("#txt_totlaHits").formValidator({empty:true,onFocus:"可以预置总点击数"}).inputValidator({max:9,onError:"总点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_dayHits").formValidator({empty:true,onFocus:"可以预置日点击数"}).inputValidator({max:9,onError:"日点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_weekHits").formValidator({empty:true,onFocus:"可以预置周点击数"}).inputValidator({max:9,onError:"周点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_monthHits").formValidator({empty:true,onFocus:"可以预置月点击数"}).inputValidator({max:9,onError:"月点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_eliteLevel").formValidator({empty:true,onFocus:"请输入推荐级"}).formValidator({empty:true,onFocus:"推荐级别数越大显示就越靠前"}).inputValidator({max:9,onError:"推荐级数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_priority").formValidator({empty:true,onFocus:"请输入优先级"}).formValidator({empty:true,onFocus:"优先级数越大显示就越靠前"}).inputValidator({max:9,onError:"优先级数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_praise").formValidator({empty:true,onFocus:"请输入“赞”数"}).formValidator({empty:true,onFocus:"请输入“赞”数"}).inputValidator({max:9,onError:"数量超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_tread").formValidator({empty:true,onFocus:"请输入“踩”数"}).formValidator({empty:true,onFocus:"请输入“踩”数"}).inputValidator({max:9,onError:"数量超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$('#bnt_checked').click(function(){
        gDialogChannel = art.dialog.open(contextPath+'/security/getChannel2.html?iframe=OpendialogWindow_AddArticle&forums=-1', { height: '80%', width: '20%', title: "请选择栏目", lock: true }, false);//打开子窗体
	});
	$('input[name="bnt_review"]').live('click', function(){
		var id = $(this).attr('reviewfrom');
		var src = $.trim($('#'+id).val());
		if(src==''){
			alert('请输入图片地址');
			return;
		}
		art.dialog({padding: 0,title: '图片预览',content: '<img src="'+src+'" />',lock: true});
	});
	$('input[name="bnt_selectImg"]').live('click',function(){
		 var callback = $(this).attr('callback');
		 gDialogChannel = art.dialog.open(contextPath+'/security/getUploadImg.html?iframe=OpendialogWindow_AddArticle&callback='+callback, { height: '400px', width: '550px', title: "请双击选择图片", lock: true }, false);
	});
	$('#bnt_back').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('1');
	});
	$('a[name="a_titlePrefix"]').click(function(){
		$('#txt_titlePrefix').val($(this).text());
	});
	UE.getEditor('editor');
	$.fn.initPage(alertWaiting);
});
(function($){
	function getData(){
		var data = {'isSuccess':false,data:{}};
		var channelId = $.trim($('#txt_channelId').val());
		if(channelId==''){
			alert('没有获取栏目Id，请和管理员联系');
			return data;
		}
		data.data['channelId'] = channelId;
		data.data['titlePrefix'] = $.trim($('#txt_titlePrefix').val());
		var title = $.trim($('#txt_title').val());
		data.data['title'] = title;
		var titleIntact = $.trim($('#txt_titleIntact').val());
		data.data['titleIntact'] = titleIntact;
		var subheading = $.trim($('#txt_subheading').val());
		data.data['subheading'] = subheading;
		var showCommentLink = false;
		if($('#cb_showCommentLink').is(":checked")){
			showCommentLink = true;
		}
		data.data['showCommentLink'] = showCommentLink;
		var keyWord = $.trim($('#txt_keyword').val());
		data.data['keyWord'] = keyWord;
		var author = $.trim($('#txt_author').val());
		data.data['author'] = author;
		var copyFrom = $.trim($('#txt_copyFrom').val());
		data.data['copyFrom'] = copyFrom;
		var intro = $.trim($('#txt_intro').val());
		data.data['intro'] = intro;
		var defaultPicUrl = $.trim($('#txt_defaultPicUrl').val());
		data.data['defaultPicUrl'] = defaultPicUrl;
		var status = 99;
		if($('#rd_Status_1').is(":checked")){
			status = -1;
		}
		else if($('#rd_Status_2').is(":checked")){
			status = 0;
		}
		data.data['status'] = status;
		var content = UE.getEditor('editor').getContent();
		if(content==''){
			alert('文章内容不能为空，请填写');
			return data;
		}
		data.data['content'] = content;
		var hits = $.trim($('#txt_totlaHits').val());
		data.data['hits'] = hits;
		var dayHits = $.trim($('#txt_dayHits').val());
		data.data['dayHits'] = dayHits;
		var weekHits = $.trim($('#txt_weekHits').val());
		data.data['weekHits'] = weekHits;
		var monthHits = $.trim($('#txt_monthHits').val());
		data.data['monthHits'] = monthHits;
		var updateTime = $.trim($('#txt_updataTime').val());
		data.data['updateTime'] = updateTime;
		var eliteLevel = $.trim($('#txt_eliteLevel').val());
		data.data['eliteLevel'] = eliteLevel;
		var priority = $.trim($('#txt_priority').val());
		data.data['priority'] = priority;
		var praise = $.trim($('#txt_praise').val());
		data.data['praise'] = praise;
		var tread = $.trim($('#txt_tread').val());
		data.data['tread'] = tread;

		if($('#focusImg').is(":checked")){
			data.data['focusImgNum'] = 1;
			var focusImg = $('#txt_focusImg').val();
			if($.trim(focusImg)==''){
				alert('请输入焦点图片路径');
				return data;
			}
			data.data['focusImg'] = focusImg;
		}
		if($('#bigNewsImg').is(":checked")){
			data.data['bigNewsImgNum'] = 1;
			var bigNewsImg = $('#txt_bigNewsImg').val();
			if($.trim(bigNewsImg)==''){
				alert('请输入头条图片路径');
				return data;
			}
			data.data['bigNewsImg'] = bigNewsImg;
		}
		if($('#marqueeImg').is(":checked")){
			data.data['marqueeImgNum'] = 1;
			var marqueeImg = $('#txt_marqueeImg').val();
			if($.trim(marqueeImg)==''){
				alert('请输入滚动图片路径');
				return data;
			}
			data.data['marqueeImg'] = marqueeImg;
		}

		data['isSuccess']=true;
		return data;
	};
	function addImg(title,inputId,callback){
		var array = new Array();
		array.push('<tr id="tr_'+inputId+'" class="tdbg">');
		array.push(' <td class="tdbgleft" style="width:200px;" align="right">');
		array.push('<strong>'+title+' ：&nbsp;</strong>');
		array.push('</td>');
		array.push('<td>');
		array.push('<input id="txt_'+inputId+'" class="input_public" style="width:300px;"/>');
		array.push('&nbsp;<input name="bnt_selectImg" type="button" class="inputbutton" value="从已上传的文件中选择" callback="'+callback+'"/>');
		array.push('&nbsp;<input name="bnt_review" reviewfrom="txt_'+inputId+'" type="button" value=" 预览 " class="inputbutton" /><br />');
		array.push('<iframe marginheight="0" marginwidth="0" scrolling="no" src="'+contextPath+'/security/loadDefaultImg.html?iframe=OpendialogWindow_AddArticle&callback='+callback+'" width="100%" height="58px" frameborder="0"></iframe>');
		array.push('</td>');
		array.push('</tr>');
		return array.join('');
	};
	var View={
		method:'add',
		init:function(alertWaiting){
			var Ajax = {
				submit:function(url,id){
					var isOk = $.formValidator.pageIsValid('1');
					if(!isOk){
						return;
					}
					var pageData = getData();
					if(!pageData.isSuccess){
						return;
					}
					var waiting = alertWaiting('正在提交数据，请等待...');
					pageData.data['commonModelId'] = id;
					$.ajax({
						type:"POST",
						url:url,
						data:pageData.data,
						dataType:'json',
						contentType:'application/x-www-form-urlencoded; charset=UTF-8',
						success: function(data){
							waiting.close();
							if(data.code=='200'){
								if(reqMethod == View.method){
									if(confirm(data.content+'，是否继续新增文章？')){
										document.getElementById("articleForm").reset();
										UE.getEditor('editor').setContent('',false);
									}
									else{
										window.top.frames['iframeMenuContent'].callBackResult('1');
									}
								}
								else{
									alert(data.content);
									window.top.frames['iframeMenuContent'].callBackResult('1');
								}
							}
							else{
								alert(data.content);
							}
						}
					});
				}
			};

			if(reqMethod == this.method){
				$("#bnt_save").click(function(){
					Ajax.submit(contextPath+'/security/addArticle.html',0);
				});
			}
			else{
				$("#bt_update").click(function(){
					var commonModelId = $('#txt_commonModelId').val();
					Ajax.submit(contextPath+'/security/updateArticle.html',commonModelId);
				});
			}
			$('input[name="checkbox_setImg"]').change(function(){
				var id = $(this).attr('id');
				if($(this).attr('checked')=='checked'){
					var title = '';
					var _callback = '';
					if(id=='focusImg'){
						title = '焦点图片';
						_callback = 'callbackFocusImg'
					}
					else if(id=='bigNewsImg'){
						title = '头条图片';
						_callback = 'callbackBigNewsImg';
					}
					else if(id=='marqueeImg'){
						title = '滚动图片';
						_callback = 'callbackMarqueeImg';
					}
					$('#tbody_imgs').append(addImg(title,id,_callback));
				}
				else{
					$('#tr_'+id).remove();
				}
			});
		}
	};
	$.fn.initPage = function(alertWaiting){
		View.init(alertWaiting);
	};
})(jQuery);
function callBackChannelInfo(_id,_name,_type){
	$('#txt_CheckChannelName').val(_name);
	$('#txt_channelId').val(_id);
	gDialogChannel.close();
};
function callbackDefaultPic(_relativePath){
	$('#txt_defaultPicUrl').val(_relativePath);
	if(typeof(gDialogChannel)!='undefined'&&gDialogChannel!=null){
		gDialogChannel.close();
	}
}
function callbackFocusImg(_relativePath){
	$('#txt_focusImg').val(_relativePath);
	if(typeof(gDialogChannel)!='undefined'&&gDialogChannel!=null){
		gDialogChannel.close();
	}
}
function callbackBigNewsImg(_relativePath){
	$('#txt_bigNewsImg').val(_relativePath);
	if(typeof(gDialogChannel)!='undefined'&&gDialogChannel!=null){
		gDialogChannel.close();
	}
};
function callbackMarqueeImg(_relativePath){
	$('#txt_marqueeImg').val(_relativePath);
	if(typeof(gDialogChannel)!='undefined'&&gDialogChannel!=null){
		gDialogChannel.close();
	}
};