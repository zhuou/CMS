;
var gDialogChannel,_gDialogCheckPic;
$(document).ready(function(){
	pageTab({'td_tab1':'tb_Pannel1','td_tab2':'tb_Pannel2'});
	beautifyBottomButton(['bt_Save','bt_Close','bt_Update']);
	$.formValidator.initConfig({formID:"channelForm",theme:"Small",
		onError:function(msg,obj,errorlist){
		}
	});
	$("#txt_CheckChannelName").formValidator({onShowText:"请选择栏目"});
	$("#txt_ChannelName").formValidator({onShowText:"请输入栏目名称",onFocus:"栏目名称为小1-30个字"}).inputValidator({min:1,max:60,onErrorMin:'请输入栏目名称',onError:"输入的栏目名称长度不合法"});
	$("#txt_Sort").formValidator({empty:false,onFocus:"可以预置栏目顺序"}).inputValidator({max:9,onError:"栏目顺序超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_ChannelUrl").formValidator({onShowText:"请输入栏目链接地址",onFocus:"请输入栏目链接地址"}).inputValidator({min:1,max:500,onErrorMin:'请输入栏目链接地址',onError:"输入的栏目链接地址长度太长"});
	//$("#txt_IconUrl").formValidator({empty:false,onFocus:"请输入栏目图片地址"}).inputValidator({max:500,onError:"栏目图片地址长度超过了最大数"});
	$('#bt_Checked').click(function(){
        gDialogChannel = art.dialog.open(contextPath+'/security/getChannel.html', { height: '80%', width: '20%', title: "选择父节点栏目", lock: true }, false);//打开子窗体
	});
	$.fn.initPage(alertWaiting);
	$('#bt_Close').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('1');
	});
	$('input[name="bnt_selectImg"]').live('click',function(){
		 var callback = $(this).attr('callback');
		 _gDialogCheckPic = art.dialog.open(contextPath+'/security/getUploadImg.html?iframe=OpendialogWindow_AddChannel&callback='+callback, { height: '400px', width: '550px', title: "请双击选择图片", lock: true }, false);
	});
	$('#bnt_review').live('click', function(){
		var id = $(this).attr('reviewfrom');
		var src = $.trim($('#'+id).val());
		if(src==''){
			alert('请输入图片地址');
			return;
		}
		art.dialog({padding: 0,title: '图片预览',content: '<img src="'+src+'" />',lock: true});
	});
});
function callBackChannel(id,name,type){
	if(type==3){
		return;
	}
	var parentId = 0;
	if(id!=-1){
		parentId = id;
	}
	$('#txt_CheckChannelName').val(name);
	$('#txt_ParentId').val(id);
	gDialogChannel.close();
};
(function($){
	// 去掉字符串头尾空格
	function trim(str) {
		return str.replace(/(^\s*)|(\s*$)/g, "");
	};

	// 是否为空
	function isStrEmpty(v) {
		if(v == undefined) {
			return true;
		} else {
			v = v.replace(/^\s+$/g, '');
		}
		if(v == '' || !v) {
			return true;
		} else {
			return false;
		}
	};

	// 是否为数字
	function IsNum(obj){
		var val = obj.value;
		var one = val.substr(0,1);
		obj.value = one != '-'? (parseInt(val) || '') : '' + (parseInt(val.substr(1,val.length)) || '');
	};
	function getData(){
		var parentId = trim($('#txt_ParentId').val());
		var channelName = trim($('#txt_ChannelName').val());
		var sort = trim($('#txt_Sort').val());
		var openType = 1;
		var value = $('#rb_OpenType1').is(":checked");
		if(value){
			openType = 0;
		}
		var iconUrl = trim($('#txt_IconUrl').val());
		var tips = trim($('#txt_Tip').val());
		var channelUrl = $('#txt_ChannelUrl').val();

		var data = {parentId:parentId,channelName:channelName,sort:sort,openType:openType,
		iconUrl:iconUrl,tips:tips,channelUrl:channelUrl};
		return data;
	};
	var View={
		method:'add',
		init:function(alertWaiting){
			var Ajax = {
				submit:function(url,id){
					var isOk = $.formValidator.pageIsValid('1');
					if(!isOk){
						return;
					}
					var perm;
					var purviewType = 0;

					if($("#rb_ReadPermMember").attr("checked")=='checked'){
						var sbPerm = new Array();
						$.each($("#td_AccessRight").find(":checkbox").filter("input:checked"),function(i,eleObj){
							var itemValue = $(eleObj).attr("value");
							sbPerm.push(itemValue);
						});
						if(sbPerm.toString()==''){
							alert('请给访问权限选择会员组');
							return;
						}
						purviewType = 1;
						perm = sbPerm.toString();
					}
					var waiting = alertWaiting('正在提交数据，请等待...');
					var data = getData();
					data['accessRight'] = perm;data['purviewType'] = purviewType;data['channelId'] = id;
					$.ajax({
						type:"POST",
						url:url,
						data:data,
						dataType:'json',
						contentType:'application/x-www-form-urlencoded; charset=UTF-8',
						success: function(data){
							waiting.close();
							if(data.code=='200'){
								if(reqMethod == View.method){
									if(confirm(data.content+'，是否继续新增栏目？')){
										document.getElementById("channelForm").reset();
									}
									else{
										window.top.frames['iframeMenuContent'].callBackResult('1');
									}
								}
								else{
									alert(data.content);
								}
							}
							else{
								alert(data.content);
							}
						}
					});
				}
			};

			if(reqMethod == this.method){
				$("#bt_Save").click(function(){
					Ajax.submit(contextPath+'/security/addExternalCh.html','');
				});
			}
			else{
				$("#bt_Update").click(function(){
					var channelId = $('#txt_ChannelId').val();
					Ajax.submit(contextPath+'/security/updateExternalCh.html',channelId);
				});
			}
		}
	};
	$.fn.initPage = function(alertWaiting){
		View.init(alertWaiting);
	};
})(jQuery);
function callbackDefaultPic(_relativePath){
	$('#txt_IconUrl').val(_relativePath);
	if(typeof(_gDialogCheckPic)!='undefined'&&_gDialogCheckPic!=null){
		_gDialogCheckPic.close();
	}
};