var _gDialogAdZone;
$(document).ready(function(){
	beautifyTopButton(['adZone_Add','adZone_Update','adZone_Del']);
	beautifyTable('tbody_DataList');
	checkBoxFunction.init('cb_CheckedAll','tbody_DataList');
	$.fn.initPage(alertWaiting);
});
(function($){
	var View={
		init:function(alertWaiting){
			$('#adZone_Add').click(function(){
				_gDialogAdZone = art.dialog.open(contextPath+'/security/loadAddAdZone.html',{id:'dialogWindow_AdZoneDetail',height:'250px',width:'780px',title:'新增广告位',padding: 0,lock:true},false);
			});
			$('#adZone_Update').click(function(){
				var array = checkBoxFunction.getId();
				if(array.length<=0){
					alert('请选择要操作项！');
					return;
				}
				if(array.length>1){
					alert('请选择一条记录修改！');
					return;
				}
				_gDialogAdZone = art.dialog.open(contextPath+'/security/loadUpdateAdZone.html?id='+array.join(''),{id:'dialogWindow_AdZoneDetail',height:'250px',width:'780px',title:'修改广告位',padding: 0,lock:true},false);
			});
			$('a[name*="detail_"]').click(function(){
				var _zoneId = $(this).attr('name');
				_gDialogAdZone = art.dialog.open(contextPath+'/security/getAdZoneDetail.html?id='+_zoneId.split('_')[1],{id:'dialogWindow_AdZoneDetail',height:'230px',width:'500px',title:'广告位详情',padding: 0,lock:true},false);
			});
			$('a[name*="adZoneStatus_"]').click(function(){
				var zoneInfo = $(this).attr('name').split('_');
				$.post(contextPath+'/security/updateAdZoneStatus.html',{status:zoneInfo[2],zoneId:zoneInfo[1]},function(data){
					if(data.code=='200'){
						alert(data.content);
						window.location.reload();
					}
					else{
						alert(data.content);
					}
				},'json');
			});
			$('#adZone_Del').click(function(){
				var array = checkBoxFunction.getId();
				if(array.length<=0){
					alert('请选择要操作项！');
					return;
				}
				var isOK = confirm('您确定要删除选中记录吗？');
				if(isOK){
					$.post(contextPath+'/security/delAdZone.html',{ids:array.join(',')},function(data){
						if(data.code=='200'){
							alert(data.content);
							window.location.reload();
						}
						else{
							alert(data.content);
						}
					},'json');
				}
			});
		}
	};
	$.fn.initPage = function(alertWaiting){
		View.init(alertWaiting);
	};
})(jQuery);
function callBackResult(resultFlag){
	_gDialogAdZone.close();
	if(resultFlag=='1'){
		window.location.reload();
	}
};