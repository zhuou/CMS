$(document).ready(function(){
	$.formValidator.initConfig({formID:"friendSiteForm",theme:"Default",
		onError:function(msg,obj,errorlist){
			alertMessage(msg);
		}
	});
	$("#txt_CheckChannelName").formValidator({onShowText:"请选择栏目"});
	$("#txt_SiteName").formValidator({onShowText:"请输入网站名称",onFocus:"网站名称为小1-30个字"}).inputValidator({min:1,max:60,onErrorMin:'请输入网站名称',onError:"输入的网站名称长度不合法"});
	$("#txt_SiteUrl").formValidator({empty:true,onShowText:"请输入网站地址",onFocus:"请输入网站地址,如:http://www.baidu.com"}).inputValidator({min:1,max:256,onErrorMin:'请输入网站地址',onError:"输入的网站地址长度不合法"}).regexValidator({regExp:regexEnum.url,onError:"输入的网站地址格式不正确"});
	//$("#txt_SiteLogoUrl").formValidator({empty:true,onFocus:"请输入网站Logo地址"}).regexValidator({regExp:regexEnum.picture,onError:"输入的网站Logo格式不正确"});
	$("#txt_TotlaHits").formValidator({empty:true,onFocus:"可以预置总点击数"}).inputValidator({max:9,onError:"总点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_DayHits").formValidator({empty:true,onFocus:"可以预置日点击数"}).inputValidator({max:9,onError:"日点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_WeekHits").formValidator({empty:true,onFocus:"可以预置周点击数"}).inputValidator({max:9,onError:"周点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_MonthHits").formValidator({empty:true,onFocus:"可以预置月点击数"}).inputValidator({max:9,onError:"月点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_EliteLevel").formValidator({empty:true,onFocus:"请输入推荐级"}).formValidator({empty:true,onFocus:"推荐级别数越大显示就越靠前"}).inputValidator({max:9,onError:"推荐级数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_Priority").formValidator({empty:true,onFocus:"请输入优先级"}).formValidator({empty:true,onFocus:"优先级数越大显示就越靠前"}).inputValidator({max:9,onError:"优先级数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	beautifyBottomButton(['bt_Checked','bt_Save','bt_Reset','bnt_selectImg','bnt_review']);
	pageTab({'td_tab1':'tb_Pannel1','td_tab2':'tb_Pannel2'});
	$('#rd_StatusPic1').change(function(){
		if(this.checked){
			$('#div_UpLoad').css({display:'none'});
			$('#span_viewImg').css({display:'none'});
			$('#txt_SiteLogoUrl').val('http://');
		}
	});
	$('#rd_StatusPic2').change(function(){
		if(this.checked){
			$('#div_UpLoad').css({display:'block'});
			$('#span_viewImg').css({display:'inline'});
			var siteUrl = $.trim($('#txt_SiteLogoUrl').val());
			if('http://'==siteUrl){
				$('#txt_SiteLogoUrl').val('');
			}
		}
	});
	function getData(){
		var status = 99;
		if($('rd_Status_1').is(":checked")){
			status = -1;
		}
		else if($('#rd_Status_2').is(":checked")){
			status = 0;
		}
		var linkType = 1;
		if($('#rd_LinkType_2').is(":checked")){
			linkType = 2;
		}
		var isElite = false;
		if($('#cb_IsElite').is(":checked")){
			isElite = true;
		}
		var data = {title:$.trim($('#txt_SiteName').val()),hits:$('#txt_TotlaHits').val(),dayHits:$('#txt_DayHits').val(),
		weekHits:$('#txt_WeekHits').val(),monthHits:$('#txt_MonthHits').val(),status:'',eliteLevel:$('#txt_EliteLevel').val(),
		priority:$('#txt_Priority').val(),siteUrl:$.trim($('#txt_SiteUrl').val()),siteIntro:$.trim($('#txt_SiteIntro').val()),
		logoUrl:$.trim($('#txt_SiteLogoUrl').val()),linkType:linkType,isElite:isElite,status:status,updateTime:$('#txt_UpdataTime').val()};
		return data;
	};
	$("#bt_Save").click(function(){
		var isOk = $.formValidator.pageIsValid('1');
		if(!isOk){
			return;
		}
		var waiting = alertWaiting('正在提交数据，请等待...');
		var _date = getData();
		var channelId = $.trim($('#txt_channelId').val());
		if(channelId==''){
			alert('没有获取栏目Id，请和管理员联系');
			return;
		}
		_date['channelId'] = channelId;
		$.ajax({
			type:"POST",
			url:contextPath+'/security/addFriendSite.html',
			data:_date,
			dataType:'json',
			contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			success: function(data){
				waiting.close();
				if(data.code=='200'){
					if(confirm(data.content+'，是否继续新增友情链接？')){
						document.getElementById("friendSiteForm").reset();
					}
					else{
						window.top.frames['iframeMenuContent'].callBackResult('1');
					}
				}
				else{
					alert(data.content);
				}
			}
		});
	});
	$('#bt_Update').click(function(){
		var isOk = $.formValidator.pageIsValid('1');
		if(!isOk){
			return;
		}
		var waiting = alertWaiting('正在提交数据，请等待...');
		var _date = getData();
		var channelId = $.trim($('#txt_channelId').val());
		if(channelId==''){
			alert('没有获取栏目Id，请和管理员联系');
			return;
		}
		_date['channelId'] = channelId;
		var commonModelId = $.trim($('#txt_commonModelId').val());
		if(commonModelId==''){
			alert('没有获取修改内容Id，请和管理员联系');
			return;
		}
		_date['commonModelId'] = commonModelId;
		$.ajax({
			type:"POST",
			url:contextPath+'/security/updateFriendSite.html',
			data:_date,
			dataType:'json',
			contentType:'application/x-www-form-urlencoded; charset=UTF-8',
			success: function(data){
				waiting.close();
				if(data.code=='200'){
					alert(data.content);
					window.top.frames['iframeMenuContent'].callBackResult('1');
				}
				else{
					alertMessage(data.content);
				}
			}
		});
	});
	$('#bt_Back').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('1');
	});
	$('#bt_Checked').click(function(){
        dialogChannel = art.dialog.open(contextPath+'/security/getChannel2.html?forums=-6&iframe=OpendialogWindow_FriendSiteDetail', { height: '80%', width: '20%', title: "请选择栏目", lock: true }, false);//打开子窗体
	});
	$('input[name="bnt_selectImg"]').live('click',function(){
		 var callback = $(this).attr('callback');
		 _gDialogCheckPic = art.dialog.open(contextPath+'/security/getUploadImg.html?iframe=OpendialogWindow_FriendSiteDetail&callback='+callback, { height: '400px', width: '550px', title: "请双击选择图片", lock: true }, false);
	});
	$('#bnt_review').live('click', function(){
		var id = $(this).attr('reviewfrom');
		var src = $.trim($('#'+id).val());
		if(src==''){
			alert('请输入图片地址');
			return;
		}
		art.dialog({padding: 0,title: '图片预览',content: '<img src="'+src+'" />',lock: true});
	});
});
var dialogChannel,_gDialogCheckPic;
function callBackChannelInfo(_id,_name,_type){
	$('#txt_CheckChannelName').val(_name);
	$('#txt_channelId').val(_id);
	dialogChannel.close();
};
function callbackDefaultPic(_relativePath){
	$('#txt_SiteLogoUrl').val(_relativePath);
	if(typeof(_gDialogCheckPic)!='undefined'&&_gDialogCheckPic!=null){
		_gDialogCheckPic.close();
	}
};