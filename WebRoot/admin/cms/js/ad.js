var _gDialogCheckPic;
$(document).ready(function(){
	beautifyBottomButton(['bnt_selectImg','bnt_save','bnt_close','bnt_update','bnt_review','bnt_selectFile']);
	$.formValidator.initConfig({formID:"adForm",theme:"Small",
		onError:function(msg,obj,errorlist){
		}
	});
	$("#txt_adName").formValidator({onShowText:"请输入广告名称",onFocus:"广告名称为1-30个字"}).inputValidator({min:1,max:200,onErrorMin:'请输入广告名称',onError:"输入的广告名称长度不合法"});
	$("#txt_priority").formValidator({empty:true,onFocus:"请输入权重"}).inputValidator({max:9,onError:"权重超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_sort").formValidator({empty:true,onFocus:"请输入排序数字"}).inputValidator({max:9,onError:"排序超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_imgWidth1").formValidator({empty:true,onFocus:"请输入图片宽度"}).inputValidator({max:9,onError:"图片宽度超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_imgHeight1").formValidator({empty:true,onFocus:"请输入图片高度"}).inputValidator({max:9,onError:"图片高度超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_imgWidth2").formValidator({empty:true,onFocus:"请输入动画宽度"}).inputValidator({max:9,onError:"动画宽度超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_imgHeight2").formValidator({empty:true,onFocus:"请输入动画高度"}).inputValidator({max:9,onError:"动画高度超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_linkUrl").formValidator({empty:true,onFocus:"链接地址为1-200个字符"}).inputValidator({min:1,max:200,onErrorMin:'请输入链接地址',onError:"输入的链接地址长度不合法"});
	$("#txt_linkAlt").formValidator({empty:true,onFocus:"链接提示为1-200个字"}).inputValidator({min:1,max:200,onErrorMin:'请输入链接提示',onError:"输入的链接提示长度不合法"});
	$.fn.initPage(alertWaiting);
});
(function($){
	var View={
		method:'add',
		adType:'tablePic',
		init:function(alertWaiting){
			$('#bnt_close').click(function(){
				window.top.frames['iframeMenuContent'].callBackResult('1');
			});
			$('input[name="adType"]').change(function(){
				var _value = $(this).val();
				$('#'+_value).show();
				$('#'+View.adType).hide();
				View.adType = _value;
			});
			$('input[name="rb_Time"]').change(function(){
				var _id = $(this).attr('id');
				if(_id=='rb_CloseTime'){
					$('#div_Time').hide();
				}
				if(_id=='rb_OpenTime'){
					$('#div_Time').show();
				}
			});
			var Ajax = {
				submit:function(url,id){
					var isOk = $.formValidator.pageIsValid('1');
					if(!isOk){
						return;
					}
					var pageData = getData();
					if(!pageData.isSuccess){
						return;
					}
					pageData.data['adId'] = id;
					var waiting = alertWaiting('正在提交数据，请等待...');
					$.ajax({
						type:"POST",
						url:url,
						data:pageData.data,
						dataType:'json',
						contentType:'application/x-www-form-urlencoded; charset=UTF-8',
						success: function(data){
							waiting.close();
							if(data.code=='200'){
								if(reqMethod == View.method){
									if(confirm(data.content+'，是否继续新增广告位？')){
										document.getElementById("adForm").reset();
										// 将选择焦点复原
										if(View.adType!='tablePic'){
											$('#tablePic').show();
											$('#'+View.adType).hide();
											View.adType = 'tablePic';
										}
									}
									else{
										window.top.frames['iframeMenuContent'].callBackResult('1');
									}
								}
								else{
									alert(data.content);
									window.top.frames['iframeMenuContent'].callBackResult('1');
								}
							}
							else{
								alert(data.content);
							}
						}
					});
				}
			};
			$('#bnt_save').click(function(){
				Ajax.submit(contextPath+'/security/addAd.html',0);
			});
			$('#bnt_update').click(function(){
				var _adId = $('#txt_adId').val();
				Ajax.submit(contextPath+'/security/updateAd.html',_adId);
			});
			$('input[name="bnt_selectImg"]').live('click',function(){
				 var callback = $(this).attr('callback');
				 _gDialogCheckPic = art.dialog.open(contextPath+'/security/getUploadImg.html?iframe=OpendialogWindow_AdDetail&callback='+callback, { height: '400px', width: '550px', title: "请双击选择图片", lock: true }, false);
			});
			$('#bnt_review').live('click', function(){
				var id = $(this).attr('reviewfrom');
				var src = $.trim($('#'+id).val());
				if(src==''){
					alert('请输入图片地址');
					return;
				}
				art.dialog({padding: 0,title: '图片预览',content: '<img src="'+src+'" />',lock: true});
			});
		}
	};
	$.fn.initPage = function(alertWaiting){
		View.init(alertWaiting);
	};
	function getData(){
		var data = {'isSuccess':false,data:{}};
		var zoneId = $('#select_zoneName').val();
		if(typeof(zoneId)=='undefined'||zoneId==''){
			alert('请先新增广告位！');
			return data;
		}
		var adName = $('#txt_adName').val();
		var adType = $('input:radio[name="adType"]:checked').val();
		if(adType=='tablePic'){
			//$("#txt_imgUrl").formValidator({onShowText:"请输入图片地址"}).inputValidator({min:1,onError:'请输入图片地址'});
			var imgUrl = $.trim($('#txt_imgUrl').val());
			if(imgUrl==''){
				alert('请输入图片地址！');
				return data;
			}
			var imgWidth = $('#txt_imgWidth1').val();
			var imgHeight = $('#txt_imgHeight1').val();
			var adIntro = $('#txt_adIntro').val();
			var linkUrl = $('#txt_linkUrl').val();
			var linkAlt = $('#txt_linkAlt').val();

			data.data['imgUrl']=imgUrl;
			data.data['imgWidth']=imgWidth;
			data.data['imgHeight']=imgHeight;
			data.data['adIntro']=adIntro;
			data.data['linkUrl']=linkUrl;
			data.data['linkAlt']=linkAlt;
			data.data['adType']='1';
		}
		else if(adType=='tableAnimation'){
			var animationUrl = $.trim($('#txt_animationUrl').val());
			if(animationUrl==''){
				alert('请上传动画或输入动画地址！');
				return data;
			}
			var imgWidth = $('#txt_imgWidth2').val();
			var imgHeight = $('#txt_imgHeight2').val();

			data.data['imgUrl']=animationUrl;
			data.data['imgWidth']=imgWidth;
			data.data['imgHeight']=imgHeight;
			data.data['adType']='2';
		}
		else if(adType=='tableText'){
			var adIntro = $.trim($('#txt_adIntro1').val());
			if(adIntro==''){
				alert('请广告文字！');
				return data;
			}
			data.data['adIntro']=adIntro;
			data.data['adType']='3';
		}
		else{
			alert('请选择广告类型！');
			return data;
		}
		var priority = $('#txt_priority').val();
		var sort = $('#txt_sort').val();
		var passed = 'true';
		if($('#rb_NoPassed').prop('checked')){
			passed = 'false';
		}
		var openTimeLimit = 'false';
		data.data['openTimeLimit']=openTimeLimit;
		if($('#rb_OpenTime').prop('checked')){
			var startTime = $.trim($('#txt_startTime').val());
			if(startTime==''){
				alert('请输入广告开始投放时间！');
				return data;
			}
			var endTime = $.trim($('#txt_endTime').val());
			if(endTime==''){
				alert('请输入广告结束投放时间！');
				return data;
			}
			data.data['startTime']=startTime;
			data.data['endTime']=endTime;
			data.data['openTimeLimit']='true';
		}
		data['isSuccess']=true;
		data.data['zoneId']=zoneId;
		data.data['adName']=adName;
		data.data['priority']=priority;
		data.data['sort']=sort;
		data.data['passed']=passed;
		return data;
	};
})(jQuery);
function callbackDefaultPic(_relativePath){
	$('#txt_imgUrl').val(_relativePath);
	if(typeof(_gDialogCheckPic)!='undefined'&&_gDialogCheckPic!=null){
		_gDialogCheckPic.close();
	}
};