$(document).ready(function(){
	beautifyBottomButton(['bnt_save','bnt_close','bnt_update']);
	$.formValidator.initConfig({formID:"adZoneForm",theme:"Small",
		onError:function(msg,obj,errorlist){
		}
	});
	$("#txt_zoneCode").formValidator({onShowText:"请输入广告位编码",onFocus:"编码为1-32个字母或者数字"}).inputValidator({min:1,max:200,onErrorMin:'请输入广告位编码',onError:"广告位编码长度不合法"});
	$("#txt_zoneName").formValidator({onShowText:"请输入广告位名称",onFocus:"广告位名称为1-30个字"}).inputValidator({min:1,max:200,onErrorMin:'请输入广告位名称',onError:"广告位名称长度不合法"});
	$("#txt_zoneIntro").formValidator({empty:true,onFocus:"广告位描述为1-200个字"}).inputValidator({min:1,max:200,onErrorMin:'请输入广告位描述',onError:"广告位描述长度不合法"});
	$.fn.initPage(alertWaiting);
});
(function($){
	var View={
		method:'add',
		init:function(alertWaiting){
			$('#bnt_close').click(function(){
				window.top.frames['iframeMenuContent'].callBackResult('1');
			});
			var Ajax = {
				submit:function(url,id){
					var isOk = $.formValidator.pageIsValid('1');
					if(!isOk){
						return;
					}
					var pageData = getData();
					if(!pageData.isSuccess){
						return;
					}
					pageData.data['zoneId'] = id;
					var waiting = alertWaiting('正在提交数据，请等待...');
					$.ajax({
						type:"POST",
						url:url,
						data:pageData.data,
						dataType:'json',
						contentType:'application/x-www-form-urlencoded; charset=UTF-8',
						success: function(data){
							waiting.close();
							if(data.code=='200'){
								if(reqMethod == View.method){
									if(confirm(data.content+'，是否继续新增广告位？')){
										document.getElementById("announceForm").reset();
									}
									else{
										window.top.frames['iframeMenuContent'].callBackResult('1');
									}
								}
								else{
									alert(data.content);
									window.top.frames['iframeMenuContent'].callBackResult('1');
								}
							}
							else{
								alert(data.content);
							}
						}
					});
				}
			};
			$('#bnt_save').click(function(){
				Ajax.submit(contextPath+'/security/addAdZone.html',0);
			});
			$('#bnt_update').click(function(){
				var _zoneId = $('#txt_zoneId').val();
				Ajax.submit(contextPath+'/security/updateAdZone.html',_zoneId);
			});
		}
	};
	$.fn.initPage = function(alertWaiting){
		View.init(alertWaiting);
	};
	function getData(){
		var data = {'isSuccess':false,data:{}};
		var zoneName =$.trim($('#txt_zoneName').val());
		if(zoneName==''){
			return data;
		}
		var zoneCode =$.trim($('#txt_zoneCode').val());
		if(zoneCode==''){
			return data;
		}
		var zoneIntro = $.trim($('#txt_zoneIntro').val());
		var status = 'false';
		if($('#cb_status').prop("checked")){
			status = 'true';
		}
		data['isSuccess']=true;
		data.data['zoneCode']=zoneCode;
		data.data['zoneName']=zoneName;
		data.data['status']=status;
		data.data['zoneIntro']=zoneIntro;
		return data;
	};
})(jQuery);