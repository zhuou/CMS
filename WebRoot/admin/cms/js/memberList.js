var _gDialogMember;
$(document).ready(function(){
	beautifyTopButton(['member_Add','member_Update','member_Del','member_Detail']);
	beautifyTable('tbody_DataList');
	checkBoxFunction.init('cb_CheckedAll','tbody_DataList');
	$.fn.initPage(alertWaiting);
});
(function($){
	var View={
		init:function(alertWaiting){
			$('#member_Add').click(function(){
				_gDialogMember = art.dialog.open(contextPath+'/security/loadAddMember.html',{id:'dialogWindow_MemberDetail',height:'500px',width:'700px',title:'新增会员',padding: 0,lock:true},false);
			});
			$('#member_Update').click(function(){
				var array = checkBoxFunction.getId();
				if(array.length<=0){
					alert('请选择要操作项！');
					return;
				}
				if(array.length>1){
					alert('请选择一条记录修改！');
					return;
				}
				_gDialogMember = art.dialog.open(contextPath+'/security/loadUpdateMember.html?id='+array.join(''),{id:'dialogWindow_MemberDetail',height:'540px',width:'750px',title:'修改会员信息',padding: 0,lock:true},false);
			});
			$('a[name*="detail_"]').click(function(){
				var _memberId = $(this).attr('name');
				_gDialogMember = art.dialog.open(contextPath+'/security/getMemberDetail.html?id='+_memberId.split('_')[1],{id:'dialogWindow_MemberDetail',height:'500px',width:'750px',title:'会员信息详情',padding: 0,lock:true},false);
			});
			$('#member_Del').click(function(){
				var array = checkBoxFunction.getId();
				if(array.length<=0){
					alert('请选择要操作项！');
					return;
				}
				var isOK = confirm('您确定要删除选中记录吗？');
				if(isOK){
					$.post(contextPath+'/security/delMember.html',{ids:array.join(',')},function(data){
						if(data.code=='200'){
							alert(data.content);
							window.location.reload();
						}
						else{
							alert(data.content);
						}
					},'json');
				}
			});
			$('#member_Detail').click(function(){
				var array = checkBoxFunction.getId();
				if(array.length<=0){
					alert('请选择要操作项！');
					return;
				}
				if(array.length>1){
					alert('请选择一条记录修改！');
					return;
				}
				_gDialogMember = art.dialog.open(contextPath+'/security/getMemberDetail.html?id='+array.join(''),{id:'dialogWindow_MemberDetail',height:'500px',width:'750px',title:'会员信息详情',padding: 0,lock:true},false);
			});
		}
	};
	$.fn.initPage = function(alertWaiting){
		View.init(alertWaiting);
	};
})(jQuery);
function callBackResult(resultFlag){
	_gDialogMember.close();
	if(resultFlag=='1'){
		window.location.reload();
	}
};