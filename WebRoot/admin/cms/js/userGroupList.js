var _gDialogUserGroup;
$(document).ready(function(){
	beautifyTopButton(['userGroup_Add','userGroup_Update','userGroup_Del','userGroup_Detail']);
	beautifyTable('tbody_DataList');
	checkBoxFunction.init('cb_CheckedAll','tbody_DataList');
	$('#userGroup_Add').click(function(){
        _gDialogUserGroup = art.dialog.open(contextPath+'/security/loadAddUserGroup.html', {height:'220px',width:'470px', title: "新增用户组", lock: true }, false);//打开子窗体
	});
	$('#userGroup_Update').click(function(){
		var array = checkBoxFunction.getId();
		if(array.length<=0){
			alert('请选择要操作项！');
			return;
		}
		if(array.length>1){
			alert('请选择一条记录修改！');
			return;
		}
        _gDialogUserGroup = art.dialog.open(contextPath+'/security/loadUpdateUserGroup.html?id='+array.join(''), {height:'220px',width:'470px', title: "修改用户组", lock: true }, false);//打开子窗体
	});
	$('#userGroup_Detail').click(function(){
		var array = checkBoxFunction.getId();
		if(array.length<=0){
			alert('请选择要操作项！');
			return;
		}
		if(array.length>1){
			alert('请选择一条记录修改！');
			return;
		}
		userGroupDetail(array.join(''));
	});

});
function userGroupDetail(_id){
	_gDialogUserGroup = art.dialog.open(contextPath+'/security/userGroupDetail.html?id='+_id, {height:'220px',width:'470px', title: "用户组详情", lock: true }, false);
};
function callBackResult(resultFlag){
	_gDialogUserGroup.close();
	if(resultFlag=='1'){
		window.location.reload();
	}
};