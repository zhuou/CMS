var _gDialogWebConfig;
$(document).ready(function(){
	beautifyTopButton(['webConfig_Add','webConfig_Del','webConfig_Update','webConfig_Search']);
	var tableTemp = new beautifySingleSelectedTable('tbody_DataList');
	beautifyInputText(['webConfig_SearchText']);
	$('#webConfig_Del').click(function(){
		var isOK = confirm('您将删除此站点下所有信息和文件，且数据不可恢复。您确定要删吗？');
		if(isOK){
			$.getJSON(contextPath+'/security/delWebConfig.html?id='+tableTemp.getValue(),function(data){
				if(data.code=='200'){
					alert(data.content);
					window.location.reload();
				}
				else{
					alert(data.content);
				}
			});
		}
	});

	$('#webConfig_Add').click(function(){
		_gDialogWebConfig=art.dialog.open(contextPath+'/security/loadAddWebConfig.html',{id:'dialogWindow_WebConfigDetail',height:'430px',width:'700px',title:'新增站点',padding: 0,lock:true},false);
	});

	$('#webConfig_Update').click(function(){
		var _id = tableTemp.getValue();
		if(_id==''){
			alert('请选择要操作项！');
			return;
		}
		_gDialogWebConfig=art.dialog.open(contextPath+'/security/loadUpdateWebConfig.html?id='+_id,{id:'dialogWindow_WebConfigDetail',height:'430px',width:'700px',title:'修改站点信息',padding: 0,lock:true},false);
	});

	$('#webConfig_Search').click(function(){
		var v = $('#webConfig_SearchText').val();
		var dv = $('#webConfig_SearchText').attr('alt');
		if(v == dv){
			v = '';
		}
		parseUri.setParam('pageNum',1)
		window.location.href = parseUri.setParam('keyword',v);
	});
	$('a[id*="detail_"]').click(function(){
		var _id = $(this).attr('name');
		_gDialogWebConfig = art.dialog.open(contextPath+'/security/webConfigDetail.html?id='+_id,{id:'dialogWindow_WebConfigDetail',height:'430px',width:'650px',title:'站点详情',padding: 0,lock:true},false);
	});
});
function callBackResult(resultFlag){
	_gDialogWebConfig.close();
	if(resultFlag=='1'){
		window.location.reload();
	}
};