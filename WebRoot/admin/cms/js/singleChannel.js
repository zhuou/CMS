;
var gDialogChannel,_gDialogCheckPic;
$(document).ready(function(){
	pageTab({'td_tab1':'tb_Pannel1','td_tab2':'tb_Pannel2','td_tab3':'tb_Pannel3','td_tab4':'tb_Pannel4'});
	beautifyBottomButton(['bt_Save','bt_Close','bt_Update']);
	$.formValidator.initConfig({formID:"channelForm",theme:"Small",
		onError:function(msg,obj,errorlist){
		}
	});
	$("#txt_CheckChannelName").formValidator({onShowText:"请选择栏目"});
	$("#txt_ChannelName").formValidator({onShowText:"请输入栏目名称",onFocus:"栏目名称为小1-30个字"}).inputValidator({min:1,max:60,onErrorMin:'请输入栏目名称',onError:"输入的栏目名称长度不合法"});
	$('#txt_NickName').formValidator({onShowText:"请输入栏目别名",onFocus:"栏目别名为小1-30个字"}).inputValidator({min:1,max:30,onErrorMin:'请输入栏目别名',onError:"输入的栏目别名长度不合法"});
	$("#txt_Sort").formValidator({empty:false,onFocus:"可以预置栏目顺序"}).inputValidator({max:9,onError:"栏目顺序超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	//$("#txt_IconUrl").formValidator({empty:true,onFocus:"请输入栏目图片地址"}).regexValidator({regExp:regexEnum.picture,onError:"输入的栏目图片地址格式不正确"});
	$("#txt_ChannelUrl").formValidator({onShowText:"请输入栏目Url",onFocus:"请输入栏目Url"}).inputValidator({min:1,max:500,onErrorMin:'请输入栏目Url',onError:"输入的栏目Url长度太长"});
	$('#bt_Checked').click(function(){
        gDialogChannel = art.dialog.open(contextPath+'/security/getChannel.html', { height: '80%', width: '20%', title: "选择父节点栏目", lock: true }, false);//打开子窗体
	});
	UE.getEditor('txt_Content');
	$.fn.initPage(alertWaiting);
	$('#bt_Close').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('1');
	});
	$('input[name="bnt_selectImg"]').live('click',function(){
		 var callback = $(this).attr('callback');
		 _gDialogCheckPic = art.dialog.open(contextPath+'/security/getUploadImg.html?iframe=OpendialogWindow_AddChannel&callback='+callback, { height: '400px', width: '550px', title: "请双击选择图片", lock: true }, false);
	});
	$('#bnt_review').live('click', function(){
		var id = $(this).attr('reviewfrom');
		var src = $.trim($('#'+id).val());
		if(src==''){
			alert('请输入图片地址');
			return;
		}
		art.dialog({padding: 0,title: '图片预览',content: '<img src="'+src+'" />',lock: true});
	});
});
function callBackChannel(id,name,type){
	if(type==3){
		return;
	}
	var parentId = 0;
	if(id!=-1){
		parentId = id;
	}
	$('#txt_CheckChannelName').val(name);
	$('#txt_ParentId').val(id);
	gDialogChannel.close();
};
(function($){
	// 去掉字符串头尾空格
	function trim(str) {
		return str.replace(/(^\s*)|(\s*$)/g, "");
	};

	// 是否为空
	function isStrEmpty(v) {
		if(v == undefined) {
			return true;
		} else {
			v = v.replace(/^\s+$/g, '');
		}
		if(v == '' || !v) {
			return true;
		} else {
			return false;
		}
	};

	// 是否为数字
	function IsNum(obj){
		var val = obj.value;
		var one = val.substr(0,1);
		obj.value = one != '-'? (parseInt(val) || '') : '' + (parseInt(val.substr(1,val.length)) || '');
	};
	function getData(){
		var parentId = trim($('#txt_ParentId').val());
		var channelName = trim($('#txt_ChannelName').val());
		var nickName = trim($('#txt_NickName').val());
		var sort = trim($('#txt_Sort').val());
		var openType = 1;
		var value = $('#rb_OpenType1').is(":checked");
		if(value){
			openType = 0;
		}
		var iconUrl = trim($('#txt_IconUrl').val());
		var tips = trim($('#txt_Tip').val());
		var description = trim($('#txt_Desc').val());
		var meta_Keywords = trim($('#txt_MetaKey').val());
		var meta_Description = trim($('#txt_MetaDesc').val());
		var content = trim(UE.getEditor('txt_Content').getContent());
		var channelUrl = $('#txt_ChannelUrl').val();

		var data = {parentId:parentId,channelName:channelName,nickName:nickName,sort:sort,openType:openType,content:content,channelUrl:channelUrl,
		iconUrl:iconUrl,tips:tips,description:description,meta_Keywords:meta_Keywords,meta_Description:meta_Description};
		return data;
	};
	var View={
		method:'add',
		init:function(alertWaiting){
			var Ajax = {
				submit:function(url,id){
					var isOk = $.formValidator.pageIsValid('1');
					if(!isOk){
						return;
					}
					var perm;
					var purviewType = 0;
					var sb = new Array();
					$.each($("#td_Forums").find(":checkbox").filter("input:checked"),function(i,eleObj){
						var itemValue = $(eleObj).attr("value");
						sb.push(itemValue);
					});
					//if(sb.toString()==''){
					//	alert('请选择所属版块');
					//	return;
					//}
					if($("#rb_ReadPermMember").attr("checked")=='checked'){
						var sbPerm = new Array();
						$.each($("#td_AccessRight").find(":checkbox").filter("input:checked"),function(i,eleObj){
							var itemValue = $(eleObj).attr("value");
							sbPerm.push(itemValue);
						});
						if(sbPerm.toString()==''){
							alert('请给访问权限选择会员组');
							return;
						}
						purviewType = 1;
						perm = sbPerm.toString();
					}
					var waiting = alertWaiting('正在提交数据，请等待...');
					var data = getData();
					data['forums'] = sb.toString();data['accessRight'] = perm;data['purviewType'] = purviewType;data['channelId'] = id;
					$.ajax({
						type:"POST",
						url:url,
						data:data,
						dataType:'json',
						contentType:'application/x-www-form-urlencoded; charset=UTF-8',
						success: function(data){
							waiting.close();
							if(data.code=='200'){
								if(reqMethod == View.method){
									if(confirm(data.content+'，是否继续新增栏目？')){
										document.getElementById("channelForm").reset();
									}
									else{
										window.top.frames['iframeMenuContent'].callBackResult('1');
									}
								}
								else{
									alert(data.content);
								}
							}
							else{
								alert(data.content);
							}
						}
					});
				}
			};

			if(reqMethod == this.method){
				$("#bt_Save").click(function(){
					Ajax.submit(contextPath+'/security/addSingleCh.html','');
				});
			}
			else{
				$("#bt_Update").click(function(){
					var channelId = $('#txt_ChannelId').val();
					Ajax.submit(contextPath+'/security/updateSingleCh.html',channelId);
				});
			}
		}
	};
	$.fn.initPage = function(alertWaiting){
		View.init(alertWaiting);
	};
})(jQuery);
function callbackDefaultPic(_relativePath){
	$('#txt_IconUrl').val(_relativePath);
	if(typeof(_gDialogCheckPic)!='undefined'&&_gDialogCheckPic!=null){
		_gDialogCheckPic.close();
	}
};