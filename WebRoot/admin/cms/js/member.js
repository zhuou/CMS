var _gDialogCheckPic;
$(document).ready(function(){
	beautifyBottomButton(['bnt_save','bnt_close','bnt_update']);
	pageTab({'td_tab1':'tb_Pannel1','td_tab2':'tb_Pannel2','td_tab3':'tb_Pannel3','td_tab4':'tb_Pannel4'});
	$.formValidator.initConfig({formID:"memberForm",theme:"Small",
		onError:function(msg,obj,errorlist){
		}
	});
	if(reqMethod=='add'){
		$("#txt_userName").formValidator({onShowText:"请输入会员名称",onFocus:'会员名称为英文、字母、"_"、"-"的3-16字符'}).regexValidator({regExp:regexEnum.username,onError:"会员名称不合法"});
		$("#txt_userPassword").formValidator({onShowText:"请输入密码",onFocus:'密码为英文、字母、"_"、"-"的6-18字符'}).regexValidator({regExp:regexEnum.password,onError:"密码不合法"});
	}
	$("#txt_trueName").formValidator({onShowText:"请输入真实姓名",onFocus:'真实姓名为2-16字符'}).inputValidator({min:2,max:16,onError:"真实姓名输入不合法"});
	$("#txt_email").formValidator({empty:true,onShowText:"",onFocus:'请输入Email'}).regexValidator({regExp:regexEnum.email,onError:"Email不合法"});
	$("#txt_userFace").formValidator({empty:true,onShowText:"",onFocus:'请输入头像地址'}).inputValidator({max:128,onError:"头像地址应小于128个字符"});
	$("#txt_faceWidth").formValidator({empty:true,onFocus:"请输入头像宽度"}).inputValidator({max:9,onError:"头像宽度超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_faceHeight").formValidator({empty:true,onFocus:"请输入头像高度"}).inputValidator({max:9,onError:"头像高度超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_question").formValidator({empty:true,onShowText:"",onFocus:'请输入提示问题'}).inputValidator({max:32,onError:"提示问题应小于32个字符"});
	$("#txt_answer").formValidator({empty:true,onShowText:"",onFocus:'请输入提示答案'}).inputValidator({max:32,onError:"提示答案应小于32个字符"});

	$("#txt_address").formValidator({empty:true,onShowText:"",onFocus:'请输入通讯地址'}).inputValidator({max:32,onError:"通讯地址应小于32个字符"});
	$("#txt_officePhone").formValidator({empty:true,onShowText:"",onFocus:'请输入办公电话'}).inputValidator({max:32,onError:"办公电话应小于32个字符"});
	$("#txt_mobile").formValidator({empty:true,onShowText:"",onFocus:'请输入移动电话'}).inputValidator({max:32,onError:"移动电话小于32个字符"});
	$("#txt_homePhone").formValidator({empty:true,onShowText:"",onFocus:'请输入住宅电话'}).inputValidator({max:32,onError:"住宅电话小于32个字符"});
	$("#txt_qq").formValidator({empty:true,onShowText:"",onFocus:'请输入QQ'}).inputValidator({max:32,onError:"QQ小于32个字符"});
	$("#txt_idCard").formValidator({empty:true,onShowText:"",onFocus:'请输入证件号码'}).inputValidator({max:64,onError:"证件号码小于64个字符"});
	$("#txt_nativePlace").formValidator({empty:true,onShowText:"",onFocus:'请输入籍贯'}).inputValidator({max:64,onError:"籍贯小于16个字符"});
	$("#txt_nation").formValidator({empty:true,onShowText:"",onFocus:'请输入民族'}).inputValidator({max:64,onError:"民族小于16个字符"});
	$("#txt_education").formValidator({empty:true,onShowText:"",onFocus:'请输入民族'}).inputValidator({max:64,onError:"民族小于16个字符"});
	$("#txt_graduateFrom").formValidator({empty:true,onShowText:"",onFocus:'请输入毕业学校'}).inputValidator({max:64,onError:"毕业学校小于32个字符"});
	$("#txt_interestsOfLife").formValidator({empty:true,onShowText:"",onFocus:'请输入生活爱好'}).inputValidator({max:128,onError:"生活爱好小于128个字符"});
	$("#txt_interestsOfCulture").formValidator({empty:true,onShowText:"",onFocus:'请输入文化爱好'}).inputValidator({max:128,onError:"文化爱好小于128个字符"});
	$("#txt_interestsOfAmusement").formValidator({empty:true,onShowText:"",onFocus:'请输入娱乐休闲爱好'}).inputValidator({max:128,onError:"娱乐休闲爱好小于128个字符"});
	$("#txt_interestsOfSport").formValidator({empty:true,onShowText:"",onFocus:'请输入体育爱好'}).inputValidator({max:128,onError:"体育爱好小于128个字符"});
	$("#txt_interestsOfOther").formValidator({empty:true,onShowText:"",onFocus:'请输入其他爱好'}).inputValidator({max:128,onError:"其他爱好小于128个字符"});
	$("#txt_family").formValidator({empty:true,onShowText:"",onFocus:'请输入家庭情况'}).inputValidator({max:128,onError:"家庭情况小于128个字符"});
	$("#txt_homepage").formValidator({empty:true,onShowText:"",onFocus:'请输入个人主页'}).inputValidator({max:128,onError:"个人主页小于128个字符"});

	$("#txt_company").formValidator({empty:true,onShowText:"",onFocus:'请输入单位名称'}).inputValidator({max:32,onError:"单位名称小于32个字符"});
	$("#txt_department").formValidator({empty:true,onShowText:"",onFocus:'请输入所属部门'}).inputValidator({max:32,onError:"所属部门小于32个字符"});
	$("#txt_position").formValidator({empty:true,onShowText:"",onFocus:'请输入职位'}).inputValidator({max:32,onError:"职位小于32个字符"});
	$("#txt_companyAddress").formValidator({empty:true,onShowText:"",onFocus:'请输入单位地址'}).inputValidator({max:64,onError:"单位地址小于64个字符"});
	$.fn.initPage(alertWaiting);
});
(function($){
	var View={
		method:'add',
		adType:'tablePic',
		init:function(alertWaiting){
			$('#bnt_close').click(function(){
				window.top.frames['iframeMenuContent'].callBackResult('1');
			});
			var Ajax = {
				submit:function(url,id){
					var isOk = $.formValidator.pageIsValid('1');
					if(!isOk){
						return;
					}
					var pageData = getData();
					if(!pageData.isSuccess){
						return;
					}
					pageData.data['userId'] = id;
					var waiting = alertWaiting('正在提交数据，请等待...');
					$.ajax({
						type:"POST",
						url:url,
						data:pageData.data,
						dataType:'json',
						contentType:'application/x-www-form-urlencoded; charset=UTF-8',
						success: function(data){
							waiting.close();
							if(data.code=='200'){
								if(reqMethod == View.method){
									if(confirm(data.content+'，是否继续新增会员？')){
										document.getElementById("memberForm").reset();
									}
									else{
										window.top.frames['iframeMenuContent'].callBackResult('1');
									}
								}
								else{
									alert(data.content);
									window.top.frames['iframeMenuContent'].callBackResult('1');
								}
							}
							else{
								alert(data.content);
							}
						}
					});
				}
			};
			$('#bnt_save').click(function(){
				Ajax.submit(contextPath+'/security/addMember.html',0);
			});
			$('#bnt_update').click(function(){
				var _adId = $('#txt_adId').val();
				Ajax.submit(contextPath+'/security/updateMember.html',_userId);
			});
			$('#select_province').change(function(){
				var $this = $(this);
				if($this.get(0).selectedIndex>0){
					var _url = contextPath+'/security/getCity.html?id='+$this.val();
					$.getJSON(_url,function(data){
						if(data.code=='200'){
							var array = new Array();
							array.push('<option value="-1">请选择...</option>');
							$.each(data.objContent,function(i,e){
			            		array.push('<option value="'+e.id+'">'+e.name+'</option>');
			            	});
							$('#select_city option').remove();
							$('#select_city').removeAttr('disabled').append(array.join(''));
						}
						else{
							alert(data.content);
						}
					});

					// 处理县区
					$('#select_district option').remove();
					$('#select_district').prepend("<option value='-1'>请选择...</option>").attr('disabled','disabled');
				}
				else{
					// 处理市县区
					$('#select_city option').remove();
					$('#select_city').prepend("<option value='-1'>请选择...</option>").attr('disabled','disabled');
					$('#select_district option').remove();
					$('#select_district').prepend("<option value='-1'>请选择...</option>").attr('disabled','disabled');
				}
			});
			$('#select_city').change(function(){
				var $this = $(this);
				if($this.get(0).selectedIndex>0){
					var _url = contextPath+'/security/getDistrict.html?id='+$this.val();
					$.getJSON(_url,function(data){
						if(data.code=='200'){
							var array = new Array();
							$.each(data.objContent,function(i,e){
			            		array.push('<option value="'+e.id+'">'+e.name+'</option>');
			            	});
							$('#select_district option').remove();
							$('#select_district').removeAttr('disabled').append(array.join(''));
						}
						else{
							alert(data.content);
						}
					});
				}
				else{
					$('#select_district option').remove();
					$('#select_district').prepend("<option value='-1'>请选择...</option>").attr('disabled','disabled');
				}
			});
		}
	};
	$.fn.initPage = function(alertWaiting){
		View.init(alertWaiting);
	};
	function getData(){
		var data = {'isSuccess':false,data:{}};
		data.data['groupId'] = $('#select_MemberGroup option:selected').val();
		data.data['userName']= $('#txt_userName').val();
		data.data['userPassword']= $('#txt_userPassword').val();
		data.data['trueName']= $('#txt_trueName').val();
		var userFace = '/admin/imgs/blue/default_userface.gif';
		if($('#txt_userFace').val()!=''){
			userFace = $('#txt_userFace').val();
		}
		data.data['userFace']= userFace;
		if($('#txt_faceWidth').val()!=''){
			data.data['faceWidth']= $('#txt_faceWidth').val();
		}
		if($('#txt_faceHeight').val()!=''){
			data.data['faceHeight']= $('#txt_faceHeight').val();
		}
		data.data['email']= $('#txt_email').val();
		var enableResetPassword = true;
		if($('#radio_enableResetPassword2').prop('checked')){
			enableResetPassword = false;
		}
		data.data['enableResetPassword']= enableResetPassword;
		data.data['question']= $('#txt_question').val();
		data.data['answer']= $('#txt_answer').val();
		if($('#select_province').val()!='-1'){
			data.data['province']= $('#select_province option:selected').text();
		}
		if($('#select_city').val()!='-1'){
			data.data['city']= $('#select_city option:selected').text();
		}
		if($('#select_district').val()!='-1'&&$('#select_city').val()!=''){
			data.data['district']= $('#select_district option:selected').text();
		}
		data.data['address']= $('#txt_address').val();
		data.data['officePhone']= $('#txt_officePhone').val();
		data.data['mobile']= $('#txt_mobile').val();
		data.data['homePhone']= $('#txt_homePhone').val();
		data.data['qq']= $('#txt_qq').val();
		data.data['birthday']= $('#txt_birthday').val();
		data.data['cardTyp']= $('#select_cardTyp').val();
		data.data['idCard']= $('#txt_idCard').val();
		data.data['nativePlace']= $('#txt_nativePlace').val();
		data.data['nation']= $('#txt_nation').val();
		var sex = 1;
		if($('#radio_sex2').prop('checked')){
			sex = 2;
		}
		data.data['sex']= sex;
		var marriage = 1
		if($('#rd_Marriage_2').prop('checked')){
			marriage = 2;
		}
		else if($('#rd_Marriage_3').prop('checked')){
			marriage = 3;
		}
		else if($('#rd_Marriage_4').prop('checked')){
			marriage = 4;
		}
		data.data['marriage']= marriage;
		if($('#select_education').get(0).selectedIndex>0){
			data.data['education']= $('#select_education option:selected').text();
		}
		data.data['graduateFrom']= $('#txt_graduateFrom').val();
		data.data['interestsOfLife']= $('#txt_interestsOfLife').val();
		data.data['interestsOfCulture']= $('#txt_interestsOfCulture').val();
		data.data['interestsOfAmusement']= $('#txt_interestsOfAmusement').val();
		data.data['interestsOfSport']= $('#txt_interestsOfSport').val();
		data.data['interestsOfOther']= $('#txt_interestsOfOther').val();
		if($('#select_income').get(0).selectedIndex>0){
			data.data['income']= $('#select_income option:selected').text();
		}
		data.data['family']= $('#txt_family').val();
		data.data['company']= $('#txt_company').val();
		data.data['department']= $('#txt_department').val();
		data.data['position']= $('#txt_position').val();
		data.data['companyAddress']= $('#txt_companyAddress').val();
		data.data['homepage']= $('#txt_homepage').val();
		data['isSuccess']=true;
		return data;
	};
})(jQuery);
function callbackDefaultPic(_relativePath){
	$('#txt_userFace').val(_relativePath);
	$('#img_UserFace').attr('src',_relativePath);
};