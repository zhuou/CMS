var gDialogChannel;
$(document).ready(function(){
	beautifyBottomButton(['bnt_checked','bnt_selectImg','bnt_review','bnt_save','bnt_back','bnt_showCheckImgUrl','bnt_upload','bnt_showPreviewImg','bnt_addExternalUrl','bnt_removeUrl','bnt_moveNext','bnt_movePrev']);
	pageTab({'td_tab1':'tb_Pannel1','td_tab2':'tb_Pannel2'});
	$.formValidator.initConfig({formID:"picForm",theme:"Default",
		onError:function(msg,obj,errorlist){
		}
	});
	$("#txt_CheckChannelName").formValidator({onShowText:"请选择栏目"});
	$("#txt_title").formValidator({onShowText:"请输入图片标题",onFocus:"图片标题为1-200个字"}).inputValidator({min:1,max:200,onErrorMin:'请输入图片标题',onError:"输入的图片标题长度不合法"});
	$("#txt_keyword").formValidator({empty:true,onFocus:"图片关键字为1-200个字"}).inputValidator({min:1,max:200,onErrorMin:'请输入图片关键字',onError:"输入的图片关键字长度不合法"});
	$("#txt_author").formValidator({empty:true,onFocus:"图片作者为1-100个字"}).inputValidator({min:1,max:100,onErrorMin:'请输入图片作者',onError:"输入的图片作者长度不合法"});
	$("#txt_copyFrom").formValidator({empty:true,onFocus:"图片来源为1-100个字"}).inputValidator({min:1,max:100,onErrorMin:'请输入图片来源',onError:"输入的图片来源长度不合法"});
	//$("#txt_defaultPicUrl").formValidator({empty:true}).inputValidator({max:1000,onErrorMin:''});
	$("#txt_totlaHits").formValidator({empty:true,onFocus:"可以预置总点击数"}).inputValidator({max:9,onError:"总点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_dayHits").formValidator({empty:true,onFocus:"可以预置日点击数"}).inputValidator({max:9,onError:"日点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_weekHits").formValidator({empty:true,onFocus:"可以预置周点击数"}).inputValidator({max:9,onError:"周点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_monthHits").formValidator({empty:true,onFocus:"可以预置月点击数"}).inputValidator({max:9,onError:"月点击数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_eliteLevel").formValidator({empty:true,onFocus:"请输入推荐级"}).formValidator({empty:true,onFocus:"推荐级别数越大显示就越靠前"}).inputValidator({max:9,onError:"推荐级数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$("#txt_priority").formValidator({empty:true,onFocus:"请输入优先级"}).formValidator({empty:true,onFocus:"优先级数越大显示就越靠前"}).inputValidator({max:9,onError:"优先级数超过了最大数"}).regexValidator({regExp:regexEnum.num1,onError:"请输入数字"});
	$('#bnt_checked').click(function(){
        gDialogChannel = art.dialog.open(contextPath+'/security/getChannel2.html?forums=-5&iframe=OpendialogWindow_AddPic', { height: '80%', width: '20%', title: "请选择栏目", lock: true }, false);//打开子窗体
	});
	$('#bnt_upload').click(function(){
		gDialogChannel = art.dialog.open(contextPath+'/security/loadUploadPic.html?iframe=OpendialogWindow_AddPic', { height: '180px', width: '370px', title: "上传图片", lock: true }, false);//打开子窗体
	});
	$('#bnt_addExternalUrl').click(function(){
		gDialogChannel = art.dialog.open(contextPath+'/security/loadUploadExPic.html?iframe=OpendialogWindow_AddPic', { height: '200px', width: '380px', title: "外部图片链接", lock: true }, false);//打开子窗体
	});
	$('#bnt_showPreviewImg').click(function(){
		var src = $("#select_picUrl").find("option:selected").text();
		if(src==''){
			alert('请选择图片地址');
			return;
		}
		art.dialog({padding: 0,title: '图片预览',content: '<img src="'+src+'"/>',lock: true});
	});
	$('#bnt_moveNext').click(function(){
		var obj = $("#select_picUrl");
	    var sel = obj.find("option:selected");
	    if (sel.length == 0) {
	        alert("请选择要移动的项！");
	        return;
	    }
	    sel.insertAfter(sel.next());
	});
	$('#bnt_movePrev').click(function(){
		var obj = $("#select_picUrl");
	    var sel = obj.find("option:selected");
	    if (sel.length == 0) {
	        alert("请选择要移动的项！");
	        return;
	    }
	    sel.insertBefore(sel.prev());
	});
	$('#bnt_removeUrl').click(function(){
		if($("#select_picUrl").get(0).selectedIndex<0){
	        alert("请选择要删除的项！");
	        return;
	    }
	    $("#select_picUrl").find("option:selected").remove();
	});
	$('#bnt_review').click(function(){
		var src = $.trim($('#txt_defaultPicUrl').val());
		if(src==''){
			alert('请输入图片地址');
			return;
		}
		art.dialog({padding: 0,title: '图片预览',content: '<img src="'+src+'" />',lock: true});
	});
	$('#bnt_showCheckImgUrl').click(function(){
		gDialogChannel = art.dialog.open(contextPath+'/security/getUploadImg.html?callback=callbackSheckExistPic&iframe=OpendialogWindow_AddPic', { height: '400px', width: '550px', title: "请双击选择图片", lock: true }, false);
	});
	$('#bnt_selectImg').click(function(){
		gDialogChannel = art.dialog.open(contextPath+'/security/getUploadImg.html?callback=callbackDefaultPic&iframe=OpendialogWindow_AddPic', { height: '400px', width: '550px', title: "请双击选择图片", lock: true }, false);
	});
	$('#bnt_back').click(function(){
		window.top.frames['iframeMenuContent'].callBackResult('1');
	});
	UE.getEditor('editor');
	$.fn.initPage(alertWaiting);
});
function insertOption(jsonContent){
	var _obj = jQuery.parseJSON(jsonContent);
	var nowTime = new Date().getTime();
	$("#select_picUrl").append('<option value="opt_'+nowTime+'">'+_obj.picUrl+'</option>');
	$('body').data('opt_'+nowTime,jsonContent);
	gDialogChannel.close();
};
(function($){
	function getData(){
		var data = {'isSuccess':false,data:{}};
		var channelId = $.trim($('#txt_channelId').val());
		if(channelId==''){
			alert('没有获取栏目Id，请和管理员联系');
			return data;
		}
		data.data['channelId'] = channelId;
		var title = $.trim($('#txt_title').val());
		data.data['title'] = title;
		var keyWord = $.trim($('#txt_keyword').val());
		data.data['keyword'] = keyWord;
		var author = $.trim($('#txt_author').val());
		data.data['picGalleryAuthor'] = author;
		var copyFrom = $.trim($('#txt_copyFrom').val());
		data.data['copyFrom'] = copyFrom;
		var defaultPicUrl = $.trim($('#txt_defaultPicUrl').val());
		data.data['defaultPicUrl'] = defaultPicUrl;
		var status = 99;
		if($('rd_Status_1').is(":checked")){
			status = -1;
		}
		else if($('#rd_Status_2').is(":checked")){
			status = 0;
		}
		data.data['status'] = status;
		var picGalleryIntro = UE.getEditor('editor').getContent();
		data.data['picGalleryIntro'] = picGalleryIntro;
		var hits = $.trim($('#txt_totlaHits').val());
		data.data['hits'] = hits;
		var dayHits = $.trim($('#txt_dayHits').val());
		data.data['dayHits'] = dayHits;
		var weekHits = $.trim($('#txt_weekHits').val());
		data.data['weekHits'] = weekHits;
		var monthHits = $.trim($('#txt_monthHits').val());
		data.data['monthHits'] = monthHits;
		var updateTime = $.trim($('#txt_updataTime').val());
		data.data['updateTime'] = updateTime;
		var eliteLevel = $.trim($('#txt_eliteLevel').val());
		data.data['eliteLevel'] = eliteLevel;
		var priority = $.trim($('#txt_priority').val());
		data.data['priority'] = priority;

		var picUrl = getPicUrl();
		//alert('picUrl='+picUrl);
		if(picUrl==''){
			alert('图片地址不能为空，请填写图片地址');
			return data;
		}
		data.data['picUrls']='['+picUrl+']';
		data['isSuccess']=true;
		return data;
	};
	function getPicUrl(){
		var data = new Array();
		$('#select_picUrl option').each(function(i){
			var val = $(this).val();
	    	if(val!=''){
				data.push($('body').data(val));
	    	}
    	});
    	return data.join(',');
	};
	var View={
		method:'add',
		init:function(alertWaiting){
			var Ajax = {
				method:'add',
				submit:function(url,id){
					var isOk = $.formValidator.pageIsValid('1');
					if(!isOk){
						return;
					}
					var pageData = getData();
					if(!pageData.isSuccess){
						return;
					}
					var waiting = alertWaiting('正在提交数据，请等待...');
					pageData.data['commonModelId'] = id;
					$.ajax({
						type:"POST",
						url:url,
						data:pageData.data,
						dataType:'json',
						contentType:'application/x-www-form-urlencoded; charset=UTF-8',
						success: function(data){
							waiting.close();
							if(data.code=='200'){
								if(reqMethod == Ajax.method){
									if(confirm(data.content+'，是否继续新增文章？')){
										document.getElementById("picForm").reset();
										UE.getEditor('editor').setContent('',false);
										$("#select_picUrl").find("option").remove();
									}
									else{
										window.top.frames['iframeMenuContent'].callBackResult('1');
									}
								}
								else{
									alert(data.content);
									window.top.frames['iframeMenuContent'].callBackResult('1');
								}
							}
							else{
								alertMessage(data.content);
							}
						}
					});
				}
			};
			if(reqMethod == this.method){
				$("#bnt_save").click(function(){
					Ajax.submit(contextPath+'/security/addPic.html',0);
				});
			}
			else{
				$("#bt_update").click(function(){
					var commonModelId = $('#txt_commonModelId').val();
					Ajax.submit(contextPath+'/security/updatePic.html',commonModelId);
				});
			}
		}
	};
	$.fn.initPage = function(alertWaiting){
		View.init(alertWaiting);
	};
})(jQuery);
function callBackChannelInfo(_id,_name,_type){
	$('#txt_CheckChannelName').val(_name);
	$('#txt_channelId').val(_id);
	gDialogChannel.close();
};
function callbackDefaultPic(_relativePath){
	$('#txt_defaultPicUrl').val(_relativePath);
	if(typeof(gDialogChannel)!='undefined'&&gDialogChannel!=null){
		gDialogChannel.close();
	}
};
function callbackSheckExistPic(_relativePath,_imgName){
	var nowTime = new Date().getTime();
	$("#select_picUrl").append('<option value="opt_'+nowTime+'">'+_relativePath+'</option>');
	var jsonContent = '{"picGalleryId":0,"picName":"'+_imgName+'","picAuthor":"","picUrl":"'+_relativePath+'","picIntro":"","createTime":null,"json":""}'
	$('body').data('opt_'+nowTime,jsonContent);
	gDialogChannel.close();
};