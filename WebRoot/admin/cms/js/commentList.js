$(document).ready(function(){
	beautifyTopButton(['comment_Del','comment_Detail','comment_Search']);
	beautifyTable('tbody_DataList');
	beautifyInputText(['comment_SearchText']);
	checkBoxFunction.init('cb_CheckedAll','tbody_DataList');
	$('#comment_Detail').click(function(){
		var array = checkBoxFunction.getId();
		if(array.length<=0){
			alert('请选择一条记录查看！');
			return;
		}
		if(array.length>1){
			alert('每次只能选择一条记录！');
			return;
		}
		window.location.href = contextPath+'/security/commentDetail.html?id='+array.join('');
	});

	$('#comment_Search').click(function(){
		var v = $('#comment_SearchText').val();
		var dv = $('#comment_SearchText').attr('alt');
		if(v == dv){
			v = '';
		}
		parseUri.setParam('pageNum',1)
		window.location.href = parseUri.setParam('keyword',v);
	});
	$('a[name="bnt_cannelElite"]').click(function(){
		var _id = $(this).attr('itemid');
		updateEliteStatus(_id,'false');
	});
	$('a[name="bnt_setElite"]').click(function(){
		var _id = $(this).attr('itemid');
		updateEliteStatus(_id,'true');
	});
	$('a[name="bnt_cannelPublic"]').click(function(){
		var _id = $(this).attr('itemid');
		updatePrivateStatus(_id,'true');
	});
	$('a[name="bnt_setPublic"]').click(function(){
		var _id = $(this).attr('itemid');
		updatePrivateStatus(_id,'false');
	});
	function updateEliteStatus(_id,_status){
		var _url = contextPath+'/security/updateEliteStatus.html?id='+_id+'&status='+_status;
		$.getJSON(_url,function(data){
			if(data.code=='200'){
				window.location.href=window.location.href;
			}
			else{
				alert(data.content);
			}
		});
	}
	function updatePrivateStatus(_id,_status){
		var _url = contextPath+'/security/updatePrivateStatus.html?id='+_id+'&status='+_status;
		$.getJSON(_url,function(data){
			if(data.code=='200'){
				window.location.href=window.location.href;
			}
			else{
				alert(data.content);
			}
		});
	}
	$('#comment_Del').click(function(){
		var array = checkBoxFunction.getId();
		if(array.length<=0){
			alert('请选择修改项！');
			return;
		}
		var _data = {};
		_data['ids'] = array.join(',');
		var postUrl = contextPath+'/security/delComment.html';
		$.post(postUrl,_data,function(data){
			if(data.code=='200'){
				window.location.href=window.location.href;
			}
			else{
				alert(data.content);
			}
		},'json');
	});
});
