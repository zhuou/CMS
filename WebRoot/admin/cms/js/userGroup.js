$(document).ready(function(){
	beautifyBottomButton(['bnt_Save','bnt_Reset','bnt_Close']);
	$.formValidator.initConfig({formID:"userGroupForm",theme:"Small",
		onError:function(msg,obj,errorlist){
			alertMessage(msg);
		}
	});
	$("#txt_userGroupName").formValidator({onShowText:"请输入用户组名称",onFocus:"用户组名称为2-20个字"}).inputValidator({min:2,max:20,onErrorMin:'请输入用户组名称',onError:"用户组名称长度不合法"});
	$("#txt_description").formValidator({empty:true,onFocus:"用户组描述最多为200个字"}).inputValidator({max:200,onError:"用户组描述长度不合法"});
	$.fn.initPage(alertWaiting);
});
(function($){
	function getData(){
		var data = {'isSuccess':false,data:{}};
		var groupName = $('#txt_userGroupName').val();
		var description = $('#txt_description').val();
		data.data['groupName']=groupName;
		data.data['description']=description;
		data['isSuccess']=true;
		return data;
	};
	var View={
		method:'add',
		init:function(alertWaiting){
			var Ajax = {
				submit:function(url,id){
					var isOk = $.formValidator.pageIsValid('1');
					if(!isOk){
						return;
					}
					var pageData = getData();
					if(!pageData.isSuccess){
						return;
					}
					var waiting = alertWaiting('正在提交数据，请等待...');
					pageData.data['groupID'] = id;
					$.ajax({
						type:"POST",
						url:url,
						data:pageData.data,
						dataType:'json',
						contentType:'application/x-www-form-urlencoded; charset=UTF-8',
						success: function(data){
							waiting.close();
							if(data.code=='200'){
								if(reqMethod == View.method){
									if(confirm(data.content+'，是否继续新会员组？')){
										document.getElementById("userGroupForm").reset();
									}
									else{
										window.top.frames['iframeMenuContent'].callBackResult('1');
									}
								}
								else{
									alert(data.content);
									window.top.frames['iframeMenuContent'].callBackResult('1');
								}
							}
							else{
								alert(data.content);
							}
						}
					});
				}
			};

			if(reqMethod == this.method){
				$("#bnt_Save").click(function(){
					Ajax.submit(contextPath+'/security/addUserGroup.html',0);
				});
			}
			else{
				$("#bnt_Update").click(function(){
					var groupID = $('#txt_groupID').val();
					Ajax.submit(contextPath+'/security/updateUserGroup.html',groupID);
				});
			}
			$('#bnt_Close').click(function(){
				window.top.frames['iframeMenuContent'].callBackResult('1');
			});
		}
	};
	$.fn.initPage = function(alertWaiting){
		View.init(alertWaiting);
	};
})(jQuery);