﻿<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>亲！-页面没有找到诶！ </title>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/common/404/css/error.css" media="screen" />
	</head>
<body>
<div id="container">
	<img class="png" src="<%=request.getContextPath()%>/common/404/images/404.png" />
	<img class="png msg" src="<%=request.getContextPath()%>/common/404/images/404_msg.png" />
  	<p><a href="/"><img class="png" src="<%=request.getContextPath()%>/common/404/images/404_to_index.png" /></a></p>
</div>
<div id="cloud" class="png"></div>
<pre style="display: none"></pre>
</body>
</html>