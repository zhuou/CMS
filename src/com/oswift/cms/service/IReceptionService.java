package com.oswift.cms.service;

import javax.servlet.http.HttpServletRequest;

import com.oswift.cms.entity.MemberBean;
import com.oswift.cms.entity.MessageBean;
import com.oswift.cms.entity.reception.AdZone;
import com.oswift.cms.entity.reception.Announce;
import com.oswift.cms.entity.reception.Article;
import com.oswift.cms.entity.reception.Channel;
import com.oswift.cms.entity.reception.Comment;
import com.oswift.cms.entity.reception.DataList;
import com.oswift.cms.entity.reception.Download;
import com.oswift.cms.entity.reception.FriendSite;
import com.oswift.cms.entity.reception.GuestBook;
import com.oswift.cms.entity.reception.Pic;
import com.oswift.utils.exception.PlatException;

public interface IReceptionService
{
    /**
     *
     * 查询栏目，查询类型
     * all查询公司下全部栏目，one-level查询全部一级栏目，fixed-all查询指定栏目及其所有子栏目，fixed-one查询指定栏目
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @channelId 栏目Id
     * @contentType 查询类型
     * @throws PlatException
     *             公共异常
     */
    Object getChannel(int companyId, int channelId, String contentType)
        throws PlatException;

    /**
     *
     * 根据别名查询出栏目信息
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @param nickName
     *            别名
     * @return Channel
     */
    Channel getChannelByNickName(int companyId, String nickName)
        throws PlatException;

    /**
     *
     * 根据栏目id获取栏目信息
     *
     * @author zhuou
     * @param channelId
     *            栏目id
     * @return Channel
     * @throws PlatException
     *             公共异常
     */
    Channel getChannelById(int channelId) throws PlatException;

    /**
     *
     * 根据内容id获取栏目信息
     *
     * @author zhuou
     * @param commonModelId
     *            内容Id
     * @return Channel
     * @throws PlatException
     *             公共异常
     */
    Channel getChannelByCommonModelId(long commonModelId) throws PlatException;

    /**
     *
     * 增加广告点击数
     *
     * @author zhuou
     * @param adId
     *            广告id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean addAdClicks(int adId);

    /**
     *
     * 新增评论
     *
     * @author zhuou
     * @param commentbean
     *            参数
     * @return int
     * @throws PlatException
     *             公共异常
     */
    boolean addComment(Comment commentbean, boolean isPrivate)
        throws PlatException;

    /**
     *
     * 新增对评论的反对 赞成 中立
     *
     * @author zhuou
     * @param commentId
     *            评论Id
     * @param type
     *            是反对 赞成 中立
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean commentAttitude(long commentId, String type) throws PlatException;

    /**
     *
     * 新增留言
     *
     * @author zhuou
     * @param bean
     *            留言
     * @companyId 公司Id
     * @status 状态
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean addGuestbook(GuestBook bean, int companyId, int status)
        throws PlatException;

    /**
     *
     *
     * 获取内容列表数据
     *
     * @author zhuou
     * @param companyId
     *            表示公司Id，可以从后台获取
     * @param channelId
     *            栏目Id 如果为-1则获取所有栏目文章列表
     * @param contentType
     *            内容类型
     * @param pageNum
     *            每页记录数
     * @param nowPage
     *            当为-1时表示不分页，大于0表示分页的第几页
     * @param tableName
     *            表名
     * @return Object
     */
    DataList contentList(int companyId, int channelId, String contentType,
            int pageNum, int nowPage, String tableName) throws PlatException;

    /**
     *
     * 根据广告位获取广告
     *
     * @author zhuou
     * @param zoneId
     *            广告位
     * @param topNum
     *            取几条广告
     * @return AdZone
     */
    AdZone getAdvertisement(int zoneId, int companyId, int topNum);

    /**
     *
     * 根据广告位编码查询广告
     *
     * @author zhuou
     * @param zoneCode
     *            广告位编码
     * @param companyId
     *            公司Id
     * @param topNum
     *            取几条广告
     * @return AdZone
     */
    AdZone getAdvertisement(String zoneCode, int companyId, int topNum);

    /**
     *
     * 获取评论列表信息
     *
     * @author zhuou
     * @param companyId
     *            单位Id
     * @param commonModelId
     *            内容id 如果为-1则表示从所有评论中获取，否则获取内容ID的评论信息
     * @param contentType
     *            内容类型
     * @param pageNum
     *            获取记录数，如果有分页，则表示每页记录数
     * @param nowPage
     *            当为-1时表示不分页，大于-1表示分页的第几页
     *
     * @return Object
     * @throws PlatException
     *             公共异常
     */
    Object getCommentList(int companyId, long commonModelId,
            String contentType, int pageNum, int nowPage) throws PlatException;

    /**
     *
     * 获取文章详情
     *
     * @author zhuou
     * @param commonId
     *            内容Id
     * @param companyId
     *            前台页面传过来的公司Id，为了防止多站点情况下通过修改url的id获取到其他网站的内容
     * @param isRelation
     *            是否要获取这篇内容的上一篇，下一篇内容
     * @param relationFlag
     *            当isRelation为true，此字段起作用。 当relationFlag=1时获取的是本栏目下的相关上一条下一条记录，
     *            当relationFlag=2时获取文章板块下上一条下一条记录，
     *            当relationFlag=3时获取本栏目及其子栏目下的文章上一条下一条记录
     * @return Article
     * @throws PlatException
     *             公共异常
     */
    Article getArticleDetail(long commonId, int companyId, boolean isRelation,
            int relationFlag) throws PlatException;

    /**
     *
     * 获取公告详情
     *
     * @author zhuou
     * @param commonId
     *            内容Id
     * @param isRelation
     *            是否要获取这篇公告的上一篇，下一篇内容
     * @return Announce
     * @throws PlatException
     *             公共异常
     */
    Announce getAnnounceDetail(long commonId, boolean isRelation)
        throws PlatException;

    /**
     *
     * 获取下载详情
     *
     * @author zhuou
     * @param commonId
     *            内容Id
     * @param isRelation
     *            是否要获取这篇下载的上一篇，下一篇内容
     * @return Download
     * @throws PlatException
     *             公共异常
     */
    Download getDownloadDetail(long commonId, boolean isRelation)
        throws PlatException;

    /**
     *
     * 获取图片详情
     *
     * @author zhuou
     * @param commonId
     *            内容Id
     * @param isRelation
     *            是否要获取这篇图片的上一篇，下一篇内容
     * @return Pic
     * @throws PlatException
     *             公共异常
     */
    Pic getPicDetail(long commonId, boolean isRelation) throws PlatException;

    /**
     *
     * 获取留言详情
     *
     * @author zhuou
     * @param commonId
     *            内容Id
     * @param isRelation
     *            是否要获取这篇留言的上一篇，下一篇内容
     * @return GuestBook
     * @throws PlatException
     *             公共异常
     */
    GuestBook getGuestBookDetail(long commonId, boolean isRelation)
        throws PlatException;

    /**
     *
     * 获取友情链接详情
     *
     * @author zhuou
     * @param commonId
     *            内容Id
     * @param isRelation
     *            是否要获取这篇友情链接的上一篇，下一篇内容
     * @return FriendSite
     * @throws PlatException
     *             公共异常
     */
    FriendSite getFriendSiteDetail(long commonId, boolean isRelation)
        throws PlatException;

    /**
     *
     * 文章点赞或者踩
     *
     * @author zhuou
     * @param articleId
     *            文章Id
     * @param type
     *            类型
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean clickAppraise(long articleId, String type) throws PlatException;

    /**
     *
     * 会员注册
     *
     * @author zhuou
     * @param memberBean
     *            会员实体类
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    MessageBean memberRegister(MemberBean memberBean, int companyId)
        throws PlatException;

    /**
     *
     * 会员登录
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @param userName
     *            用户名
     * @param password
     *            密码
     * @param loginIp
     *            登录Ip
     * @return MessageBean
     * @throws PlatException
     *             公共异常
     */
    MessageBean memberLogin(int companyId, String userName, String password,
            String loginIp) throws PlatException;

    /**
     *
     * 上传图片，并保存到磁盘上
     *
     * @author zhuou
     * @param request
     *            HttpServletRequest
     * @param companyId
     *            单位ID
     * @return UploadBean
     */
    MessageBean uploadImg(HttpServletRequest request, int companyId)
        throws PlatException;

    /**
     *
     * 前台删除文件
     *
     * @author zhuou
     * @param request
     *            HttpServletRequest
     * @param filesId
     *            文件Id
     * @return MessageBean
     * @throws PlatException
     *             公共异常
     */
    MessageBean delFile(HttpServletRequest request, long filesId, int companyId)
        throws PlatException;

    /**
     *
     * 新增文章
     *
     * @author zhuou
     * @param bean
     *            文章实体bean
     * @param companyId
     *            公司Id
     * @param status
     *            状态
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean addArticle(Article bean, int companyId, int status)
        throws PlatException;
}
