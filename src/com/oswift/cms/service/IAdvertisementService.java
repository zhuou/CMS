package com.oswift.cms.service;

import java.util.List;

import com.oswift.cms.entity.AdZoneBean;
import com.oswift.cms.entity.AdvertisementBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.utils.exception.PlatException;

public interface IAdvertisementService
{
    /**
     *
     * 新增广告位
     *
     * @author zhuou
     * @param bean
     *            广告位bean
     * @return int
     * @throws PlatException
     *             公共异常
     */
    int addAdZone(AdZoneBean bean) throws PlatException;

    /**
     *
     * 新增广告
     *
     * @author zhuou
     * @param bean
     *            广告bean
     * @return int
     * @throws PlatException
     *             公共异常
     */
    int addAdvertisement(AdvertisementBean bean) throws PlatException;

    /**
     *
     * 根据公司id获取广告位，有分页
     *
     * @author zhuou
     * @param companyId
     *            公司id
     * @param pageNum
     *            页码
     * @param pageRecordNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             公共异常
     */
    PageBean<AdZoneBean> getAdZoneList(int companyId, int pageNum,
            int pageRecordNum) throws PlatException;

    /**
     *
     * 根据公司id获取广告列表，有分页
     *
     * @author zhuou
     * @param companyId
     *            公司id
     * @param pageNum
     *            页码
     * @param pageRecordNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             公共异常
     */
    PageBean<AdvertisementBean> getAdvertisementList(int companyId,
            int pageNum, int pageRecordNum) throws PlatException;

    /**
     *
     * 修改广告位
     *
     * @author zhuou
     * @param bean
     *            广告位bean
     * @return int
     * @throws PlatException
     *             公共异常
     */
    int updateAdZone(AdZoneBean bean) throws PlatException;

    /**
     *
     * 修改广告
     *
     * @author zhuou
     * @param bean
     *            广告bean
     * @return int
     * @throws PlatException
     *             公共异常
     */
    int updateAdvertisement(AdvertisementBean bean) throws PlatException;

    /**
     *
     * 获取广告位详情
     *
     * @author zhuou
     * @param zoneId
     *            广告位Id
     * @return AdZoneBean
     * @throws PlatException
     *             公共异常
     */
    AdZoneBean getAdZoneById(int zoneId) throws PlatException;

    /**
     *
     * 获取广告详情
     *
     * @author zhuou
     * @param adId
     *            广告id
     * @return AdvertisementBean
     * @throws PlatException
     *             公共异常
     */
    AdvertisementBean getAdvertisementById(int adId) throws PlatException;

    /**
     *
     * 根据广告位Id删除广告位
     *
     * @author zhuou
     * @param zoneId
     *            广告位id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean delAdZone(int zoneId) throws PlatException;

    /**
     *
     * 根据广告id删除广告
     *
     * @author zhuou
     * @param adId
     *            广告id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean delAdvertisemen(int adId) throws PlatException;

    /**
     *
     * 根据广告位id修改广告位状态
     *
     * @author zhuou
     * @param zoneId
     *            广告位Id
     * @param status
     *            广告位状态
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean updateStatus(int zoneId, boolean status) throws PlatException;

    /**
     *
     * 根据公司id获取广告，有分页
     *
     * @author zhuou
     * @param companyId
     *            公司id
     * @param zoneId
     *            广告位id，如果广告位Id>0,则搜索此广告位下的广告
     * @param pageNum
     *            页码
     * @param pageRecordNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             公共异常
     */
    PageBean<AdvertisementBean> getAdList(int companyId, int zoneId,
            int pageNum, int pageRecordNum) throws PlatException;

    /**
     *
     * 新增广告
     *
     * @author zhuou
     * @param bean
     *            广告的bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean addAd(AdvertisementBean bean) throws PlatException;

    /**
     *
     * 修改广告
     *
     * @author zhuou
     * @param bean
     *            广告bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean updateAd(AdvertisementBean bean) throws PlatException;

    /**
     *
     * 删除一条广告
     *
     * @author zhuou
     * @param adId
     *            广告id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean delAd(int adId) throws PlatException;

    /**
     *
     * 根据广告id查询广告详情
     *
     * @author zhuou
     * @param adId
     *            广告id
     * @return AdvertisementBean
     * @throws PlatException
     *             公共异常
     */
    AdvertisementBean getAdDetail(int adId) throws PlatException;

    /**
     *
     * 根据companyId获取广告位列表
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @return List
     * @throws PlatException
     *             公共异常
     */
    List<AdZoneBean> getAdZoneListByCompanyId(int companyId)
        throws PlatException;

    /**
     *
     * 修改广告状态
     *
     * @author zhuou
     * @param adId
     *            广告Id
     * @param passed
     *            是否通过审核
     * @return int
     */
    int updateAdStatus(int adId, boolean passed) throws PlatException;

    /**
     *
     * 根据规则生成一个新的zoneCode
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @return String
     * @throws PlatException
     *             公共异常
     */
    String getZoneCode(int companyId) throws PlatException;

    /**
     *
     * 根据ZoneCode和公司Id查询出一条记录
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @param zoneCode
     *            编码
     * @return AdZoneBean
     * @throws PlatException
     *             公共异常
     */
    AdZoneBean getOneByZoneCode(int companyId, String zoneCode)
        throws PlatException;
}
