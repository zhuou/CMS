package com.oswift.cms.service;

import java.util.List;
import java.util.Map;

import com.oswift.cms.entity.CompanyBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.entity.WebConfigBean;
import com.oswift.utils.exception.PlatException;

/**
 *
 * 公司表的各种操作类
 *
 * @author zhuou
 * @version C03 Nov 12, 2012
 * @since OSwift GPM V1.0
 */
public interface ICompanyService
{
    /**
     *
     * 添加一条公司记录
     *
     * @author zhuou
     * @param companyBean
     *            公司记录
     * @return boolean
     * @throws PlatException
     *             CMS公共异常
     */
    boolean save(CompanyBean companyBean) throws PlatException;

    /**
     *
     * 获取公司的属性信息
     *
     * @author zhuou
     * @return map
     * @throws PlatException
     *             CMS公共异常
     */
    Map<String, Object> getCompanyOtherInfo() throws PlatException;

    /**
     *
     * 获取公司列表
     *
     * @author zhuou
     * @param keyword
     *            查询关键字
     * @param pageNum
     *            页码
     * @param pageRecordNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    PageBean<CompanyBean> getCompanyList(String keyword, int pageNum,
            int pageRecordNum, int companyId) throws PlatException;

    /**
     *
     * 获取公司详情
     *
     * @author zhuou
     * @param companyId
     *            id
     * @return CompanyBean
     * @throws PlatException
     *             CMS公共异常
     */
    Map<String, Object> getCompanyDetail(int companyId) throws PlatException;

    /**
     *
     * 获取公司详情,用于显示详情
     *
     * @author zhuou
     * @param companyId
     * @return CompanyBean
     * @throws PlatException
     *             CMS公共异常
     */
    CompanyBean companyDetail(int companyId) throws PlatException;

    /**
     *
     * 修改公司信息
     *
     * @author zhuou
     * @param companyBean
     * @return
     * @throws PlatException
     *             CMS公共异常
     */
    boolean update(CompanyBean companyBean) throws PlatException;

    /**
     *
     * 根据公司Id获取网站配置信息
     *
     * @author zhuou
     * @param siteId
     *            站点Id
     * @return WebConfigBean
     * @throws PlatException
     *             公共异常
     */
    WebConfigBean webConfigDetail(int siteId) throws PlatException;

    /**
     *
     * 根据公司id获取公司列表，如果company=-1获取所有公司，如果>-1则只获取此公司的信息
     *
     * @author zhuou
     * @param companyId
     *            公司id
     * @return List<CompanyBean>
     * @throws PlatException
     *             CMS公共异常
     */
    List<CompanyBean> getCompanysbyCId(int companyId) throws PlatException;

    /**
     *
     * 获取网站配置信息列表
     *
     * @author zhuou
     * @param keyword
     *            查询关键字
     * @param pageNum
     *            页码
     * @param pageRecordNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    PageBean<WebConfigBean> getWebConfigList(String keyword, int pageNum,
            int pageRecordNum, int companyId) throws PlatException;

    /**
     *
     * 新增站点信息
     *
     * @author zhuou
     * @param webBean
     *            站点bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean saveWebConfig(WebConfigBean webBean) throws PlatException;

    /**
     *
     * 修改站点信息
     *
     * @author zhuou
     * @param companyBean
     * @return
     * @throws PlatException
     *             CMS公共异常
     */
    boolean updateWebConfig(WebConfigBean webBean) throws PlatException;

    /**
     *
     * 删除某个站点信息
     *
     * @author zhuou
     * @param siteId
     *            站点id
     * @webPath 工程绝对路径
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean delWebConfig(int siteId, String webPath) throws PlatException;
}
