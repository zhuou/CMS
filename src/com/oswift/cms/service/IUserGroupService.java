package com.oswift.cms.service;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.entity.PageBean;
import com.oswift.cms.entity.UserGroupBean;
import com.oswift.utils.exception.PlatException;

public interface IUserGroupService
{
    /**
     *
     * 根据公司Id获取用户组信息
     *
     * @author zhuou
     * @param companyId
     * @return List
     * @throws PlatException
     *             CMS公共异常
     */
    List<UserGroupBean> getUserGroupByCompanyId(int companyId)
        throws PlatException;

    /**
     *
     * 获取用户组列表信息，有分页
     *
     * @author zhuou
     * @param pageRecordNum
     *            每页记录数
     * @param pageNum
     *            第几页
     * @param companyId
     *            单位Id
     * @return PageBean
     * @throws PlatException
     *             公共异常
     */
    PageBean<UserGroupBean> getUserGroups(int pageRecordNum, int pageNum,
            int companyId) throws PlatException;

    /**
     *
     * 加载用户组修改页面
     *
     * @author zhuou
     * @param pageName
     *            jsp页面
     * @param companyId
     *            单位Id
     * @param userGroupId
     *            用户组Id
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    ModelAndView loadUpdateUserGroup(String pageName, int companyId,
            int userGroupId) throws PlatException;

    /**
     *
     * 添加一条用户组信息
     *
     * @author zhuou
     * @param bean
     *            UserGroupBean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean save(UserGroupBean bean) throws PlatException;

    /**
     *
     * 修改一条用户组信息
     *
     * @author zhuou
     * @param bean
     *            UserGroupBean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean update(UserGroupBean bean) throws PlatException;

    /**
     *
     * 获取用户组详情信息
     *
     * @author zhuou
     * @param userGroupId
     *            用户组Id
     * @return UserGroupBean
     * @throws PlatException
     *             公共异常
     */
    UserGroupBean getOne(int userGroupId) throws PlatException;

    /**
     *
     * 根据id查询用户组
     *
     * @author zhuou
     * @param ids
     *            用户组id
     * @return List
     * @throws PlatException
     *             公共异常
     */
    List<UserGroupBean> getUserGroupByIds(String ids) throws PlatException;
}
