package com.oswift.cms.service;

import com.oswift.cms.entity.PageBean;
import com.oswift.cms.entity.ReceptionPageUrlBean;
import com.oswift.utils.exception.PlatException;

public interface IReceptionPageUrlService
{
    /**
     *
     * 获取网站前台URL配置信息列表
     *
     * @author zhuou
     * @param keyword
     *            查询关键字
     * @param pageNum
     *            页码
     * @param pageRecordNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    PageBean<ReceptionPageUrlBean> getReceptionPageUrlList(String keyword,
            int pageNum, int pageRecordNum, int companyId) throws PlatException;

    /**
     *
     * 新增站点前台url数据
     *
     * @author zhuou
     * @param bean
     *            站点前台url
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean add(ReceptionPageUrlBean bean) throws PlatException;

    /**
     *
     * 删除站点前台url数据
     *
     * @author zhuou
     * @param pageUrlId
     *            id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean del(int pageUrlId) throws PlatException;

    /**
     *
     * 修改一天站点url数据
     *
     * @author zhuou
     * @param newBean
     *            站点url数据
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean update(ReceptionPageUrlBean newBean) throws PlatException;

    /**
     *
     * 根据pageUrlId获取详情
     *
     * @author zhuou
     * @param pageUrlId
     * @return ReceptionPageUrlBean
     * @throws PlatException
     *             公共异常
     */
    ReceptionPageUrlBean getReceptionPageUrlDetail(int pageUrlId)
        throws PlatException;
}
