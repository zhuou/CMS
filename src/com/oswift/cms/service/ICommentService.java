package com.oswift.cms.service;

import com.oswift.cms.entity.CommentBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.utils.exception.PlatException;

public interface ICommentService
{
    /**
     *
     * 获取评论列表
     *
     * @author zhuou
     * @param keyword
     *            查询关键字,没有查询 赋值为null
     * @param pageNum
     *            页码
     * @param pageRecordNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    PageBean<CommentBean> getCommentList(String keyword, int companyId,
            int pageNum, int pageRecordNum) throws PlatException;

    /**
     *
     * 根据Id获取评论详情
     *
     * @author zhuou
     * @param commentId
     *            评论Id
     * @return CommentBean
     * @throws PlatException
     *             公共异常
     */
    CommentBean getOne(int commentId) throws PlatException;

    /**
     *
     * 修改是否精华状态
     *
     * @author zhuou
     * @param isElite
     *            是否精华
     * @param commentId
     *            评论Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean updateEliteStatus(boolean isElite, long commentId)
        throws PlatException;

    /**
     *
     * 修改是否公开状态
     *
     * @author zhuou
     * @param isPrivate
     *            是否公开
     * @param commentId
     *            评论Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean updatePrivateStatus(boolean isPrivate, long commentId)
        throws PlatException;

    /**
     *
     * 根据评论Id删除评论
     *
     * @author zhuou
     * @param commentIds
     *            评论Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean delCommentById(String commentIds) throws PlatException;
}
