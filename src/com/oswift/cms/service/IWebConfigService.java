package com.oswift.cms.service;

import java.util.List;

import com.oswift.cms.entity.WebConfigBean;
import com.oswift.utils.exception.PlatException;

public interface IWebConfigService
{
    /**
     *
     * 根据单位Id获取站点信息
     *
     * @author zhuou
     * @param siteId
     *            站点Id
     * @return WebConfigBean
     * @throws PlatException
     *             公共异常
     */
    WebConfigBean getWebConfigById(int siteId) throws PlatException;

    /**
     *
     * 根据公司Id获取站点信息
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @return List
     * @throws PlatException
     *             CMS公共异常
     */
    List<WebConfigBean> getWebConfigByCompanyId(int companyId)
        throws PlatException;
}
