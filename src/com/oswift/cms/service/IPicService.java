package com.oswift.cms.service;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.entity.CommonModelBean;
import com.oswift.cms.entity.MessageBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.entity.PicBean;
import com.oswift.cms.entity.UploadBean;
import com.oswift.utils.exception.PlatException;

public interface IPicService
{
    /**
     *
     * 获取公共内容信息，有分页
     *
     * @author zhuou
     * @param orderField
     *            排序字段
     * @param orderType
     *            排序类型ACS或者DESC
     * @param status
     *            状态
     * @param channelId
     *            栏目ID
     * @param searchType
     *            搜索字段
     * @param keyword
     *            关键字
     * @param pageRecordNum
     *            每页记录数
     * @param pageNum
     *            第几页
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    PageBean<CommonModelBean> getCommonModel(String orderField,
            String orderType, int status, int channelId, String searchType,
            String keyword, int pageRecordNum, int pageNum, String tableName,
            int companyId) throws PlatException;

    /**
     *
     * 加载图片新增页面
     *
     * @author zhuou
     * @param pageName
     *            jsp页面名称
     * @param companyId
     *            单位Id
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    ModelAndView loadAddPic(String pageName, int companyId)
        throws PlatException;

    /**
     *
     * 加载图片修改页面
     *
     * @author zhuou
     * @param pageName
     *            jsp页面名称
     * @param companyId
     *            单位Id
     * @param commonModelId
     *            内容公共ID
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    ModelAndView loadUpdatePic(String pageName, int companyId, int commonModelId)
        throws PlatException, IOException;

    /**
     *
     * 新增一条图片记录
     *
     * @author zhuou
     * @param Pic
     *            图片bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean save(PicBean picBean) throws PlatException;

    /**
     *
     * 修改图片记录
     *
     * @author zhuou
     * @param announceBean
     *            公告bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean update(PicBean picBean) throws PlatException;

    /**
     *
     * 删除一条图片记录
     *
     * @author zhuou
     * @param commonModelId
     *            公共Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean delete(int commonModelId) throws PlatException;

    /**
     *
     * 根据Id查询一条图片记录
     *
     * @author zhuou
     * @param commonModelId
     *            公共Id
     * @return PicBean
     * @throws PlatException
     *             公共异常
     */
    PicBean getOne(int commonModelId) throws PlatException;

    /**
     *
     * 上传图片，并保存到磁盘上
     *
     * @author zhuou
     * @param request
     *            HttpServletRequest
     * @param companyId
     *            单位ID
     * @return MessageBean
     */
    UploadBean uploadImg(HttpServletRequest request, int companyId)
        throws PlatException;

    /**
     *
     * 下载图片文件，保存到磁盘上
     *
     * @author zhuou
     * @param request
     *            HttpServletRequest
     * @param urlString
     *            图片url路径
     * @param companyId
     *            单位Id
     * @param imgName
     *            图片名称
     * @return MessageBean
     */
    MessageBean download(HttpServletRequest request, String urlString,
            int companyId, String imgName) throws PlatException;

    /**
     *
     * 上传文件除图片外的其他文件
     *
     * @author zhuou
     * @param request
     *            HttpServletRequest
     * @param companyId
     *            公司Id
     * @param uploadFile
     *            上传文件名称
     * @return UploadBean
     * @throws PlatException
     *             公共异常
     */
    UploadBean uploadFiles(HttpServletRequest request, int companyId,
            String uploadFile) throws PlatException;
}
