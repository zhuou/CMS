package com.oswift.cms.service;

import com.oswift.utils.exception.PlatException;

public interface ICommonModelService
{
    /**
     *
     * 更新天点击率
     *
     * @author zhuou
     * @return int
     * @throws PlatException
     *             CMS公共异常
     */
    int updateDayHit() throws PlatException;

    /**
     *
     * 更新周点击率
     *
     * @author zhuou
     * @return int
     * @throws PlatException
     *             CMS公共异常
     */
    int updateWeekHit() throws PlatException;

    /**
     *
     * 更新月点击率
     *
     * @author zhuou
     * @return int
     * @throws PlatException
     *             CMS公共异常
     */
    int updateMonthHit() throws PlatException;
}
