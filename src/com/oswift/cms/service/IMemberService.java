package com.oswift.cms.service;

import com.oswift.cms.entity.MemberBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.utils.exception.PlatException;

public interface IMemberService
{
    /**
     *
     * 根据公司id获取会员，有分页
     *
     * @author zhuou
     * @param companyId
     *            公司id
     * @param pageNum
     *            页码
     * @param pageRecordNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             公共异常
     */
    PageBean<MemberBean> getMemberList(int companyId, int pageNum,
            int pageRecordNum) throws PlatException;

    /**
     *
     * 新增会员
     *
     * @author zhuou
     * @param bean
     *            MemberBean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean addMember(MemberBean bean, int companyId) throws PlatException;

    /**
     *
     * 删除会员
     *
     * @author zhuou
     * @param userId
     *            会员id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean delMember(long userId) throws PlatException;

    /**
     *
     * 获取会员详情
     *
     * @author zhuou
     * @param userId
     *            会员Id
     * @return MemberBean
     * @throws PlatException
     *             公共异常
     */
    MemberBean getMemberDetail(long userId) throws PlatException;

    /**
     *
     * 修改会员
     *
     * @author zhuou
     * @param bean
     *            MemberBean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean updateMember(MemberBean bean) throws PlatException;
}
