package com.oswift.cms.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.entity.ChannelBean;
import com.oswift.cms.entity.DictionaryBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.entity.UserGroupBean;
import com.oswift.utils.exception.PlatException;

public interface IChannelService
{
    /**
     *
     * 保存栏目内容
     *
     * @author zhuou
     * @param channelBean
     *            栏目bean
     * @param channelType
     *            栏目类型
     * @return boolean
     * @throws PlatException
     *             CMS公共异常
     */
    boolean save(ChannelBean channelBean, int channelType) throws PlatException;

    /**
     *
     * 根据公司Id获取用户组信息
     *
     * @author zhuou
     * @param companyId
     * @return List
     * @throws PlatException
     *             CMS公共异常
     */
    List<UserGroupBean> getUserGroupByCompanyId(int companyId)
        throws PlatException;

    /**
     *
     * 栏目加载server处理方法
     *
     * @author zhuou
     * @param viewName
     *            视图名称
     * @param companyId
     *            公司Id
     * @return ModelAndView
     * @throws PlatException
     *             CMS公共异常
     */
    ModelAndView loadAddChannel(String viewName, int companyId)
        throws PlatException;

    /**
     *
     * 获取板块信息
     *
     * @author zhuou
     * @return List<DictionaryBean>
     */
    List<DictionaryBean> getForumInfo();

    /**
     *
     * 根据公司ID查询栏目
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @return ModelAndView
     * @throws PlatException
     *             CMS公共异常
     */
    ModelAndView getChannelByCompanyId(int companyId) throws PlatException;

    /**
     *
     * 获取channel详情
     *
     * @author zhuou
     * @param channelId
     * @param view
     *            页面名称
     * @return ModelAndView
     * @throws PlatException
     *             CMS公共异常
     */
    ModelAndView getChannelById(int channelId, String view, String errorPage)
        throws PlatException;

    /**
     *
     * 查询栏目信息，当companyId=-1时查询全部栏目，如果>1则查询相关公司栏目信息
     *
     * @author zhuou
     * @param companyId
     *            公司ID
     * @param keyword
     *            查询关键字
     * @param rowNum
     *            行数
     * @param pageNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    PageBean<ChannelBean> getChannelList(int companyId, String keyword,
            int pageNum, int pageRecordNum) throws PlatException;

    /**
     *
     * 根据parentId获取栏目信息
     *
     * @author zhuou
     * @param parentId
     * @return List<ChannelBean>
     * @throws PlatException
     *             CMS公共异常
     */
    List<ChannelBean> getChannelListByParentId(int parentId)
        throws PlatException;

    /**
     *
     * 根据公司ID和forums板块查询栏目列表
     *
     * @author zhuou
     * @param companyId
     * @param forums
     * @return List<ChannelBean>
     * @throws PlatException
     *             CMS公共异常
     */
    ModelAndView getChannel2(int companyId, String forums) throws PlatException;

    /**
     *
     * 加载修改栏目页面
     *
     * @author zhuou
     * @param viewName
     *            页面名称
     * @param companyId
     *            单位Id
     * @param channelId
     *            栏目Id
     * @return ModelAndView
     * @throws PlatException
     *             平台公共异常
     */
    ModelAndView loadUpdateChannel(String viewName, int companyId, int channelId)
        throws PlatException;

    /**
     *
     * 修改栏目信息
     *
     * @author zhuou
     * @param channelBean
     *            栏目bean
     * @return boolean
     * @throws PlatException
     *             平台公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    boolean update(ChannelBean channelBean) throws PlatException;

    /**
     *
     * 删除栏目时，会相应修改内容栏目ID字段值
     *
     * @author zhuou
     * @param channelId
     *            栏目ID
     * @param companyId
     *            单位Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean delChannel(int channelId, int companyId) throws PlatException;

    /**
     *
     * 根据公司ID查询出没有外接栏目的所有栏目
     *
     * @author zhuou
     * @param companyId
     *            公司ID
     * @return list
     * @throws PlatException
     *             CMS公共异常
     */
    List<ChannelBean> getChannel3(int companyId) throws PlatException;

    /**
     *
     * 根据公司ID和forums板块查询栏目列表
     *
     * @author zhuou
     * @param companyId
     * @param forums
     * @return List<ChannelBean>
     * @throws PlatException
     *             CMS公共异常
     */
    List<ChannelBean> getChannelList2(int companyId, String forums)
        throws PlatException;
}
