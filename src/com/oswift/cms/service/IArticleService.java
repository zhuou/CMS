package com.oswift.cms.service;

import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.entity.ArticleBean;
import com.oswift.cms.entity.CommonModelBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.utils.exception.PlatException;

public interface IArticleService
{
    /**
     *
     * 获取公共内容信息，有分页
     *
     * @author zhuou
     * @param orderField
     *            排序字段
     * @param orderType
     *            排序类型ACS或者DESC
     * @param status
     *            状态
     * @param channelId
     *            栏目ID
     * @param searchType
     *            搜索字段
     * @param keyword
     *            关键字
     * @param pageRecordNum
     *            每页记录数
     * @param pageNum
     *            第几页
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    PageBean<CommonModelBean> getCommonModel(String orderField, String orderType, int status,
            int channelId, String searchType, String keyword,
            int pageRecordNum, int pageNum, String tableName, int companyId)
        throws PlatException;

    /**
     *
     * 新增一条文章记录
     *
     * @author zhuou
     * @param articleBean
     *            文章bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean save(ArticleBean articleBean) throws PlatException;

    /**
     *
     * 修改文章记录
     *
     * @author zhuou
     * @param articleBean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean update(ArticleBean articleBean) throws PlatException;

    /**
     *
     * 删除一条文章记录
     *
     * @author zhuou
     * @param commonModelId
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean delete(int commonModelId) throws PlatException;

    /**
     *
     * 根据Id查询一条文章记录
     *
     * @author zhuou
     * @param commonModelId
     *            公共Id
     * @return ArticleBean
     * @throws PlatException
     *             公共异常
     */
    ArticleBean getOne(int commonModelId) throws PlatException;

    /**
     *
     * 加载文章新增页面
     *
     * @author zhuou
     * @param pageName
     *            jsp页面名称
     * @param companyId
     *            单位Id
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    ModelAndView loadAddArticle(String pageName, int companyId)
        throws PlatException;

    /**
     *
     * 加载文章修改页面
     *
     * @author zhuou
     * @param pageName
     *            jsp页面名称
     * @param companyId
     *            单位Id
     * @param commonModelId
     *            内容公共ID
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    ModelAndView loadUpdateArticle(String pageName, int companyId,
            int commonModelId) throws PlatException;
}
