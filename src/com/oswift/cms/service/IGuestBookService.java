package com.oswift.cms.service;

import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.entity.GuestBookBean;
import com.oswift.cms.entity.GuestBookReplyBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.utils.exception.PlatException;

public interface IGuestBookService
{
    /**
     *
     * 获取公共内容信息，有分页
     *
     * @author zhuou
     * @param orderField
     *            排序字段
     * @param orderType
     *            排序类型ACS或者DESC
     * @param status
     *            状态
     * @param channelId
     *            栏目ID
     * @param searchType
     *            搜索字段
     * @param keyword
     *            关键字
     * @param pageRecordNum
     *            每页记录数
     * @param pageNum
     *            第几页
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    PageBean<GuestBookBean> getCommonModel(String orderField,
            String orderType, int status, int channelId, String searchType,
            String keyword, int pageRecordNum, int pageNum, String tableName,
            int companyId) throws PlatException;

    /**
     *
     * 加载留言新增页面
     *
     * @author zhuou
     * @param pageName
     *            jsp页面名称
     * @param companyId
     *            单位Id
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    public ModelAndView loadAddGuestBook(String pageName, int companyId)
        throws PlatException;

    /**
     *
     * 加载留言修改页面
     *
     * @author zhuou
     * @param pageName
     *            jsp页面名称
     * @param companyId
     *            单位Id
     * @param commonModelId
     *            内容公共ID
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    ModelAndView loadUpdateGuestBook(String pageName, int companyId,
            int commonModelId) throws PlatException;

    /**
     *
     * 新增一条留言记录
     *
     * @author zhuou
     * @param GuestBook
     *            留言bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean save(GuestBookBean guestBookBean) throws PlatException;

    /**
     *
     * 修改留言记录
     *
     * @author zhuou
     * @param GuestBook
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean update(GuestBookBean guestBookBean) throws PlatException;

    /**
     *
     * 根据Id查询一条留言记录
     *
     * @author zhuou
     * @param commonModelId
     *            公共Id
     * @return GuestBookBean
     * @throws PlatException
     *             公共异常
     */
    GuestBookBean getOne(long commonModelId) throws PlatException;

    /**
     *
     * 删除一条留言记录
     *
     * @author zhuou
     * @param commonModelId
     *            公共Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean delete(int commonModelId) throws PlatException;

    /**
     *
     * 新增留言回复
     *
     * @author zhuou
     * @param replyBean
     *            回复bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean addReply(GuestBookReplyBean replyBean) throws PlatException;

    /**
     *
     * 删除一条回复内容
     *
     * @author zhuou
     * @param replyId
     *            回复Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean delReply(long replyId) throws PlatException;

    /**
     *
     *
     * 根据回复Id回去回复详情
     *
     * @author zhuou
     * @param replyId
     *            回复Id
     * @return GuestBookReplyBean
     * @throws PlatException
     *             公共异常
     */
    GuestBookReplyBean getReplyByRId(long replyId) throws PlatException;

    /**
     *
     * 修改回复内容
     *
     * @author zhuou
     * @param replyBean
     *            回复bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean updateReply(GuestBookReplyBean replyBean) throws PlatException;

    /**
     *
     * 获取留言回复，有分页
     *
     * @author zhuou
     * @param searchField
     *            搜索字段
     * @param keyword
     *            搜索关键字
     * @param pageNum
     *            第几页
     * @param pageRecordNum
     *            没有条数
     * @param companyId
     *            公司ID
     * @return PageBean
     * @throws PlatException
     *             公共异常
     */
    PageBean<GuestBookReplyBean> getReplysPageCurrent(String searchField,
            String keyword, int pageNum, int pageRecordNum, int companyId)
        throws PlatException;

    /**
     *
     * 根据留言Id获取留言详细信息
     *
     * @author zhuou
     * @param guestBookId
     *            留言Id
     * @return GuestBookBean
     * @throws PlatException
     *             公共异常
     */
    GuestBookBean getGuestBookByGId(long guestBookId) throws PlatException;
}
