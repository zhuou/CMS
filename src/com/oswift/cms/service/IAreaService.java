package com.oswift.cms.service;

import java.util.List;

import com.oswift.cms.entity.AreaBean;
import com.oswift.utils.exception.PlatException;

/**
 * 
 * 公司表的各种操作类
 * 
 * @author zhuou
 * @version C03 Nov 12, 2012
 * @since OSwift GPM V1.0
 */
public interface IAreaService
{
    /**
     * 
     * 获取省、市、区/县的信息
     * 
     * @author zhuou
     * @param areaType
     * @return map
     * @throws PlatException
     *             CMS公共异常
     */
    List<AreaBean> getArea(String areaType) throws PlatException;

    /**
     * 
     * 根据id获取市、区/县的信息
     * 
     * @author zhuou
     * @param areaType
     * @param id
     * @return
     * @throws PlatException
     *             CMS公共异常
     */
    List<AreaBean> getAreaById(String areaType, int id) throws PlatException;
}
