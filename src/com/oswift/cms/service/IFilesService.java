package com.oswift.cms.service;

import com.oswift.cms.entity.FilesBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.utils.exception.PlatException;

public interface IFilesService
{
    /**
     *
     * 组合查询，当fileType=null或者status<0就不按照其条件查询
     *
     * @author zhuou
     * @param company
     *            公司Id
     * @param fileType
     *            文件类型，如果fileType=null，条件不起作用
     * @param status
     *            文件标志，如果status<0，条件不起作用
     * @param pageRecordNum
     *            每页记录数
     * @param pageNum
     *            第几页
     * @return List
     */
    PageBean<FilesBean> getFiles(int company, String fileType, int status,
            int pageRecordNum, int pageNum) throws PlatException;
}
