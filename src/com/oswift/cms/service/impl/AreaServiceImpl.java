package com.oswift.cms.service.impl;

import java.util.List;

import com.oswift.cms.dao.AreaDao;
import com.oswift.cms.entity.AreaBean;
import com.oswift.cms.service.IAreaService;
import com.oswift.utils.exception.PlatException;

/**
 *
 * 公司表的各种操作类
 *
 * @author zhuou
 * @version C03 Nov 12, 2012
 * @since OSwift GPM V1.0
 */
public class AreaServiceImpl implements IAreaService
{
    /**
     * AreaDao对象
     */
    private AreaDao areaDao;

    /**
     *
     * 获取省、市、区/县的信息
     *
     * @author zhuou
     * @param areaType
     * @return map
     * @throws PlatException
     *             CMS公共异常
     */
    public List<AreaBean> getArea(String areaType) throws PlatException
    {
        return areaDao.getArea(areaType);
    }

    /**
     *
     * 根据id获取市、区/县的信息
     *
     * @author zhuou
     * @param areaType
     * @param id
     * @return
     * @throws PlatException
     *             CMS公共异常
     */
    public List<AreaBean> getAreaById(String areaType, int id)
        throws PlatException
    {
        return areaDao.getAreaById(areaType, id);
    }

    public AreaDao getAreaDao()
    {
        return areaDao;
    }

    public void setAreaDao(AreaDao areaDao)
    {
        this.areaDao = areaDao;
    }
}
