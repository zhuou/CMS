package com.oswift.cms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import com.oswift.cms.dao.MemberDao;
import com.oswift.cms.entity.MemberBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.service.IMemberService;
import com.oswift.cms.utils.PageCode;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

public class MemberServiceImpl implements IMemberService
{
    /**
     * spring对象注入
     */
    private MemberDao memberDao;

    /**
     *
     * 根据公司id获取会员，有分页
     *
     * @author zhuou
     * @param companyId
     *            公司id
     * @param pageNum
     *            页码
     * @param pageRecordNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             公共异常
     */
    public PageBean<MemberBean> getMemberList(int companyId, int pageNum,
            int pageRecordNum) throws PlatException
    {
        int rowNum = (pageNum - 1) * pageRecordNum;

        Map<String, Integer> param = new HashMap<String, Integer>();
        param.put("companyId", companyId);
        param.put("rowNum", rowNum);
        param.put("pageNum", pageRecordNum);

        List<MemberBean> dataList = memberDao.selectList(param);
        int totalRecordNum = memberDao.selectCount(param);

        PageBean<MemberBean> pageBean = new PageBean<MemberBean>();
        pageBean.setTotalRecordNum(totalRecordNum);
        pageBean.setDataList(dataList);
        return pageBean;
    }

    /**
     *
     * 新增会员
     *
     * @author zhuou
     * @param bean
     *            MemberBean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean addMember(MemberBean bean, int companyId) throws PlatException
    {
        // 用户名是否存在
        boolean isExist = memberDao.isExistByUserName(bean.getUserName(), companyId);
        if (isExist)
        {
            throw new PlatException(PageCode.EXISTUSERNAME);
        }

        // 新增主表
        int rows = memberDao.add(bean);
        if (rows <= 0)
        {
            return false;
        }

        // 新增扩张表
        rows = memberDao.addContacter(bean);
        if (rows <= 0)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
        return true;
    }

    /**
     *
     * 删除会员
     *
     * @author zhuou
     * @param userId
     *            会员id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean delMember(long userId) throws PlatException
    {
        MemberBean bean = memberDao.selectOne(userId);
        if (null == bean)
        {
            return true;
        }

        // 删除扩展表
        int rows = memberDao.delContacter(userId);
        if (rows <= 0)
        {
            return false;
        }
        rows = memberDao.delete(userId);
        if (rows <= 0)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }

        return true;
    }

    /**
     *
     * 获取会员详情
     *
     * @author zhuou
     * @param userId
     *            会员Id
     * @return MemberBean
     * @throws PlatException
     *             公共异常
     */
    public MemberBean getMemberDetail(long userId) throws PlatException
    {
        return memberDao.selectOne(userId);
    }

    /**
     *
     * 修改会员
     *
     * @author zhuou
     * @param bean
     *            MemberBean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean updateMember(MemberBean bean) throws PlatException
    {
        // 用户是否存在
        MemberBean oldBean = memberDao.selectOne(bean.getUserId());
        if (null == oldBean)
        {
            throw new PlatException(PageCode.EXISTNOMEMBER);
        }

        // 新增主表
        int rows = memberDao.update(bean);
        if (rows <= 0)
        {
            return false;
        }

        // 新增扩张表
        rows = memberDao.updateMemberContacter(bean);
        if (rows <= 0)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
        return true;
    }

    public void setMemberDao(MemberDao memberDao)
    {
        this.memberDao = memberDao;
    }
}
