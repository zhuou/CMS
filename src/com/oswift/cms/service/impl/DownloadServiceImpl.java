package com.oswift.cms.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.dao.ChannelDao;
import com.oswift.cms.dao.CommonModelDao;
import com.oswift.cms.dao.DictionaryDao;
import com.oswift.cms.dao.DownloadDao;
import com.oswift.cms.entity.ChannelBean;
import com.oswift.cms.entity.CommonModelBean;
import com.oswift.cms.entity.DictionaryBean;
import com.oswift.cms.entity.DownloadBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.service.IDownloadService;
import com.oswift.cms.system.cache.CMSCacheFileds;
import com.oswift.cms.system.cache.DictionaryManager;
import com.oswift.cms.utils.Field;
import com.oswift.cms.utils.PageCode;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.common.TimeUtil;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

public class DownloadServiceImpl implements IDownloadService
{
    /**
     * CommonModelDao对象注入
     */
    private CommonModelDao commonModelDao;

    /**
     * DownloadDao对象注入
     */
    private DownloadDao downloadDao;

    /**
     * ChannelDao对象注入
     */
    private ChannelDao channelDao;

    /**
     * DictionaryDao对象注入
     */
    private DictionaryDao dictionaryDao;

    /**
     *
     * 获取公共内容信息，有分页
     *
     * @author zhuou
     * @param orderField
     *            排序字段
     * @param orderType
     *            排序类型ACS或者DESC
     * @param status
     *            状态
     * @param channelId
     *            栏目ID
     * @param searchType
     *            搜索字段
     * @param keyword
     *            关键字
     * @param pageRecordNum
     *            每页记录数
     * @param pageNum
     *            第几页
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    public PageBean<CommonModelBean> getCommonModel(String orderField, String orderType,
            int status, int channelId, String searchType, String keyword,
            int pageRecordNum, int pageNum, String tableName, int companyId)
        throws PlatException
    {
        int rowNum = (pageNum - 1) * pageRecordNum;

        Map<String, Object> param = new HashMap<String, Object>();
        if (StringUtil.isEmpty(orderField))
        {
            param.put("orderField", null);
        }
        else
        {
            param.put("orderField", orderField);
        }

        if (StringUtil.isEmpty(orderType))
        {
            param.put("orderType", null);
        }
        else
        {
            param.put("orderType", orderType);
        }
        param.put("status", status);
        param.put("channelId", channelId);

        if (StringUtil.isEmpty(searchType))
        {
            param.put("searchType", null);
        }
        else
        {
            param.put("searchType", searchType);
        }
        if (StringUtil.isEmpty(keyword))
        {
            param.put("keyword", null);
        }
        else
        {
            param.put("keyword", keyword);
        }
        param.put("rowNum", rowNum);
        param.put("pageNum", pageRecordNum);
        param.put("tableName", tableName);
        param.put("companyId", companyId);

        return commonModelDao.getCommonModel(param);
    }

    /**
     *
     * 新增一条下载记录
     *
     * @author zhuou
     * @param Download
     *            下载bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean save(DownloadBean downloadBean) throws PlatException
    {
        int rows = downloadDao.add(downloadBean);
        if (rows <= 0 || downloadBean.getDownloadId() <= 0)
        {
            return false;
        }

        downloadBean.setItemId(downloadBean.getDownloadId());
        rows = commonModelDao.add((CommonModelBean) downloadBean);
        if (rows <= 0)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
        return true;
    }

    /**
     *
     * 修改下载记录
     *
     * @author zhuou
     * @param Download
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean update(DownloadBean downloadBean) throws PlatException
    {
        // 查询一下，判断是否被删除。防止多人操作
        DownloadBean bean = downloadDao.selectOne(downloadBean
                .getCommonModelId());
        if (null == bean)
        {
            throw new PlatException(PageCode.DATA_ALREADY_DEL);
        }

        downloadBean.setDownloadId(bean.getDownloadId());
        int rows = downloadDao.update(downloadBean);
        if (rows <= 0)
        {
            return false;
        }

        rows = commonModelDao.update((CommonModelBean) downloadBean);
        if (rows <= 0)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
        return true;
    }

    /**
     *
     * 删除一条下载记录
     *
     * @author zhuou
     * @param commonModelId
     *            公共Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean delete(int commonModelId) throws PlatException
    {
        DownloadBean downloadBean = downloadDao.selectOne(commonModelId);
        if (null != downloadBean)
        {
            int rows = commonModelDao.delete(commonModelId);
            if (rows <= 0)
            {
                return false;
            }

            rows = downloadDao.delete(downloadBean.getDownloadId());
            if (rows <= 0)
            {
                throw new PlatException(ErrorCode.COMMON_DB_ERROR);
            }
        }
        return true;
    }

    /**
     *
     * 根据Id查询一条下载记录
     *
     * @author zhuou
     * @param commonModelId
     *            公共Id
     * @return DownloadBean
     * @throws PlatException
     *             公共异常
     */
    public DownloadBean getOne(int commonModelId) throws PlatException
    {
        return downloadDao.selectOne(commonModelId);
    }

    /**
     *
     * 加载下载新增页面
     *
     * @author zhuou
     * @param pageName
     *            jsp页面名称
     * @param companyId
     *            单位Id
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    public ModelAndView loadAddDownload(String pageName, int companyId)
        throws PlatException
    {
        ModelAndView mv = new ModelAndView(pageName);

        // 获取默认栏目
        DictionaryBean bean = DictionaryManager
                .getValue(CMSCacheFileds.BusinessField.DEFAULT_DOWNLOAD_KEY);
        ChannelBean defaultChannel = channelDao.getDefaultChannel(companyId,
                bean.getFieldValue());
        mv.addObject("defaultChannel", defaultChannel);

        mv.addObject("date", TimeUtil.dateToString(new Date(),
                TimeUtil.ISO_DATE_NOSECOND_FORMAT));

        // 软件类别
        List<DictionaryBean> fileType = dictionaryDao
                .getDictionaryByFieldName(Field.Download.FILETYPE);
        mv.addObject("fileType", fileType);

        // 软件语言
        List<DictionaryBean> fileLanguage = dictionaryDao
                .getDictionaryByFieldName(Field.Download.FILELANGUAGE);
        mv.addObject("fileLanguage", fileLanguage);

        // 授权方式
        List<DictionaryBean> copyrightType = dictionaryDao
                .getDictionaryByFieldName(Field.Download.COPYRIGHTTYPE);
        mv.addObject("copyrightType", copyrightType);

        // 软件平台
        List<DictionaryBean> operatingSystem = dictionaryDao
                .getDictionaryByFieldName(Field.Download.OPERATINGSYSTEM);
        mv.addObject("operatingSystem", operatingSystem);

        return mv;
    }

    /**
     *
     * 加载下载修改页面
     *
     * @author zhuou
     * @param pageName
     *            jsp页面名称
     * @param companyId
     *            单位Id
     * @param commonModelId
     *            内容公共ID
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    public ModelAndView loadUpdateDownload(String pageName, int companyId,
            int commonModelId) throws PlatException
    {
        ModelAndView mv = new ModelAndView(pageName);
        DownloadBean bean = downloadDao.selectOne(commonModelId);
        mv.addObject("downloadBean", bean);

        mv.addObject("date", TimeUtil.dateToString(new Date(),
                TimeUtil.ISO_DATE_NOSECOND_FORMAT));

        // 获取默认栏目
        DictionaryBean dictionaryBean = DictionaryManager
                .getValue(CMSCacheFileds.BusinessField.DEFAULT_DOWNLOAD_KEY);
        ChannelBean defaultChannel = channelDao.getDefaultChannel(companyId,
                dictionaryBean.getFieldValue());
        mv.addObject("defaultChannel", defaultChannel);

        // 软件类别
        List<DictionaryBean> fileType = dictionaryDao
                .getDictionaryByFieldName(Field.Download.FILETYPE);
        mv.addObject("fileType", fileType);

        // 软件语言
        List<DictionaryBean> fileLanguage = dictionaryDao
                .getDictionaryByFieldName(Field.Download.FILELANGUAGE);
        mv.addObject("fileLanguage", fileLanguage);

        // 授权方式
        List<DictionaryBean> copyrightType = dictionaryDao
                .getDictionaryByFieldName(Field.Download.COPYRIGHTTYPE);
        mv.addObject("copyrightType", copyrightType);

        // 软件平台
        List<DictionaryBean> operatingSystem = dictionaryDao
                .getDictionaryByFieldName(Field.Download.OPERATINGSYSTEM);
        mv.addObject("operatingSystem", operatingSystem);

        return mv;
    }

    public CommonModelDao getCommonModelDao()
    {
        return commonModelDao;
    }

    public void setCommonModelDao(CommonModelDao commonModelDao)
    {
        this.commonModelDao = commonModelDao;
    }

    public DownloadDao getDownloadDao()
    {
        return downloadDao;
    }

    public void setDownloadDao(DownloadDao downloadDao)
    {
        this.downloadDao = downloadDao;
    }

    public ChannelDao getChannelDao()
    {
        return channelDao;
    }

    public void setChannelDao(ChannelDao channelDao)
    {
        this.channelDao = channelDao;
    }

    public void setDictionaryDao(DictionaryDao dictionaryDao)
    {
        this.dictionaryDao = dictionaryDao;
    }
}
