package com.oswift.cms.service.impl;

import java.util.List;

import com.oswift.cms.dao.WebConfigDao;
import com.oswift.cms.entity.WebConfigBean;
import com.oswift.cms.service.IWebConfigService;
import com.oswift.utils.exception.PlatException;

public class WebConfigService implements IWebConfigService
{
    /**
     * spring对象WebConfigDao注入
     */
    private WebConfigDao webConfigDao;

    /**
     *
     * 根据单位Id获取站点信息
     *
     * @author zhuou
     * @param siteId
     *            站点Id
     * @return WebConfigBean
     * @throws PlatException
     *             公共异常
     */
    public WebConfigBean getWebConfigById(int siteId) throws PlatException
    {
        return webConfigDao.selectOne(siteId);
    }

    /**
     *
     * 根据公司Id获取站点信息
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @return List
     * @throws PlatException
     *             CMS公共异常
     */
    public List<WebConfigBean> getWebConfigByCompanyId(int companyId)
        throws PlatException
    {
        return webConfigDao.getWebConfigByCompanyId(companyId);
    }

    public void setWebConfigDao(WebConfigDao webConfigDao)
    {
        this.webConfigDao = webConfigDao;
    }
}
