package com.oswift.cms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import com.oswift.cms.dao.AdZoneDao;
import com.oswift.cms.dao.AdvertisementDao;
import com.oswift.cms.entity.AdZoneBean;
import com.oswift.cms.entity.AdvertisementBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.service.IAdvertisementService;
import com.oswift.utils.exception.PlatException;

public class AdvertisementServiceImpl implements IAdvertisementService
{
    /**
     * spring注入
     */
    private AdvertisementDao advertisementDao;

    /**
     * spring注入
     */
    private AdZoneDao adZoneDao;

    /**
     *
     * 新增广告位
     *
     * @author zhuou
     * @param bean
     *            广告位bean
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int addAdZone(AdZoneBean bean) throws PlatException
    {
        return adZoneDao.add(bean);
    }

    /**
     *
     * 新增广告
     *
     * @author zhuou
     * @param bean
     *            广告bean
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int addAdvertisement(AdvertisementBean bean) throws PlatException
    {
        return advertisementDao.add(bean);
    }

    /**
     *
     * 根据公司id获取广告位，有分页
     *
     * @author zhuou
     * @param companyId
     *            公司id
     * @param pageNum
     *            页码
     * @param pageRecordNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             公共异常
     */
    public PageBean<AdZoneBean> getAdZoneList(int companyId, int pageNum,
            int pageRecordNum) throws PlatException
    {
        int rowNum = (pageNum - 1) * pageRecordNum;

        Map<String, Integer> param = new HashMap<String, Integer>();
        param.put("companyId", companyId);
        param.put("rowNum", rowNum);
        param.put("pageNum", pageRecordNum);

        List<AdZoneBean> dataList = adZoneDao.selectList(param);
        int totalRecordNum = adZoneDao.selectCount(param);

        PageBean<AdZoneBean> pageBean = new PageBean<AdZoneBean>();
        pageBean.setTotalRecordNum(totalRecordNum);
        pageBean.setDataList(dataList);
        return pageBean;
    }

    /**
     *
     * 根据公司id获取广告列表，有分页
     *
     * @author zhuou
     * @param companyId
     *            公司id
     * @param pageNum
     *            页码
     * @param pageRecordNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             公共异常
     */
    public PageBean<AdvertisementBean> getAdvertisementList(int companyId,
            int pageNum, int pageRecordNum) throws PlatException
    {
        int rowNum = (pageNum - 1) * pageRecordNum;

        Map<String, Integer> param = new HashMap<String, Integer>();
        param.put("companyId", companyId);
        param.put("rowNum", rowNum);
        param.put("pageNum", pageNum);

        List<AdvertisementBean> dataList = advertisementDao.selectList(param);
        int totalRecordNum = advertisementDao.selectCount(param);

        PageBean<AdvertisementBean> pageBean = new PageBean<AdvertisementBean>();
        pageBean.setTotalRecordNum(totalRecordNum);
        pageBean.setDataList(dataList);
        return pageBean;
    }

    /**
     *
     * 修改广告位
     *
     * @author zhuou
     * @param bean
     *            广告位bean
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updateAdZone(AdZoneBean bean) throws PlatException
    {
        return adZoneDao.update(bean);
    }

    /**
     *
     * 修改广告
     *
     * @author zhuou
     * @param bean
     *            广告bean
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updateAdvertisement(AdvertisementBean bean) throws PlatException
    {
        return advertisementDao.update(bean);
    }

    /**
     *
     * 获取广告位详情
     *
     * @author zhuou
     * @param zoneId
     *            广告位Id
     * @return AdZoneBean
     * @throws PlatException
     *             公共异常
     */
    public AdZoneBean getAdZoneById(int zoneId) throws PlatException
    {
        return adZoneDao.selectOne(zoneId);
    }

    /**
     *
     * 获取广告详情
     *
     * @author zhuou
     * @param adId
     *            广告id
     * @return AdvertisementBean
     * @throws PlatException
     *             公共异常
     */
    public AdvertisementBean getAdvertisementById(int adId)
        throws PlatException
    {
        return advertisementDao.selectOne(adId);
    }

    /**
     *
     * 根据广告位Id删除广告位
     *
     * @author zhuou
     * @param zoneId
     *            广告位id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean delAdZone(int zoneId) throws PlatException
    {
        AdZoneBean bean = adZoneDao.selectOne(zoneId);
        if (null == bean)
        {
            return true;
        }

        // 删除广告位
        int rows = adZoneDao.delete(zoneId);
        if (rows <= 0)
        {
            return false;
        }

        // 删除广告
        advertisementDao.delAdvertisementByZoneId(zoneId);

        return true;
    }

    /**
     *
     * 根据广告id删除广告
     *
     * @author zhuou
     * @param adId
     *            广告id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean delAdvertisemen(int adId) throws PlatException
    {
        AdvertisementBean bean = advertisementDao.selectOne(adId);
        if (null == bean)
        {
            return true;
        }
        return advertisementDao.delete(adId) > 0 ? true : false;
    }

    /**
     *
     * 根据广告位id修改广告位状态
     *
     * @author zhuou
     * @param zoneId
     *            广告位Id
     * @param status
     *            广告位状态
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean updateStatus(int zoneId, boolean status)
        throws PlatException
    {
        AdZoneBean bean = new AdZoneBean();
        bean.setZoneId(zoneId);
        bean.setStatus(status);
        return adZoneDao.updateStatus(bean) > 0 ? true : false;
    }

    /**
     *
     * 根据公司id获取广告，有分页
     *
     * @author zhuou
     * @param companyId
     *            公司id
     * @param zoneId
     *            广告位id，如果广告位Id>0,则搜索此广告位下的广告
     * @param pageNum
     *            页码
     * @param pageRecordNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             公共异常
     */
    public PageBean<AdvertisementBean> getAdList(int companyId, int zoneId,
            int pageNum, int pageRecordNum) throws PlatException
    {
        int rowNum = (pageNum - 1) * pageRecordNum;

        Map<String, Integer> param = new HashMap<String, Integer>();
        param.put("companyId", companyId);
        param.put("zoneId", zoneId);
        param.put("rowNum", rowNum);
        param.put("pageNum", pageRecordNum);

        List<AdvertisementBean> dataList = advertisementDao.selectList(param);
        int totalRecordNum = advertisementDao.selectCount(param);

        PageBean<AdvertisementBean> pageBean = new PageBean<AdvertisementBean>();
        pageBean.setTotalRecordNum(totalRecordNum);
        pageBean.setDataList(dataList);
        return pageBean;
    }

    /**
     *
     * 新增广告
     *
     * @author zhuou
     * @param bean
     *            广告的bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean addAd(AdvertisementBean bean) throws PlatException
    {
        return advertisementDao.add(bean) > 0 ? true : false;
    }

    /**
     *
     * 修改广告
     *
     * @author zhuou
     * @param bean
     *            广告bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean updateAd(AdvertisementBean bean) throws PlatException
    {
        return advertisementDao.update(bean) > 0 ? true : false;
    }

    /**
     *
     * 删除一条广告
     *
     * @author zhuou
     * @param adId
     *            广告id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean delAd(int adId) throws PlatException
    {
        return advertisementDao.delete(adId) > 0 ? true : false;
    }

    /**
     *
     * 根据广告id查询广告详情
     *
     * @author zhuou
     * @param adId
     *            广告id
     * @return AdvertisementBean
     * @throws PlatException
     *             公共异常
     */
    public AdvertisementBean getAdDetail(int adId) throws PlatException
    {
        return advertisementDao.selectOne(adId);
    }

    /**
     *
     * 根据companyId获取广告位列表
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @return List
     * @throws PlatException
     *             公共异常
     */
    public List<AdZoneBean> getAdZoneListByCompanyId(int companyId)
        throws PlatException
    {
        return adZoneDao.getAdZoneListByCompanyId(companyId);
    }

    /**
     *
     * 修改广告状态
     *
     * @author zhuou
     * @param adId
     *            广告Id
     * @param passed
     *            是否通过审核
     * @return int
     */
    public int updateAdStatus(int adId, boolean passed) throws PlatException
    {
        AdvertisementBean bean = new AdvertisementBean();
        bean.setAdId(adId);
        bean.setPassed(passed);
        return advertisementDao.updateAdStatus(bean);
    }

    /**
     *
     * 根据规则生成一个新的zoneCode
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @return String
     * @throws PlatException
     *             公共异常
     */
    public String getZoneCode(int companyId) throws PlatException
    {
        String newZoneCode = "A00001";
        AdZoneBean ad = adZoneDao.getTop1ByCompanyId(companyId);
        if (null != ad)
        {
            int zoneId = ad.getZoneId();
            newZoneCode = String.format("A%05d", zoneId + 1);
        }

        return newZoneCode;
    }

    /**
     *
     * 根据ZoneCode和公司Id查询出一条记录
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @param zoneCode
     *            编码
     * @return AdZoneBean
     * @throws PlatException
     *             公共异常
     */
    public AdZoneBean getOneByZoneCode(int companyId, String zoneCode)
        throws PlatException
    {
        return adZoneDao.getOneByZoneCode(companyId, zoneCode);
    }

    public void setAdvertisementDao(AdvertisementDao advertisementDao)
    {
        this.advertisementDao = advertisementDao;
    }

    public void setAdZoneDao(AdZoneDao adZoneDao)
    {
        this.adZoneDao = adZoneDao;
    }
}
