package com.oswift.cms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.oswift.cms.dao.CommentDao;
import com.oswift.cms.entity.CommentBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.service.ICommentService;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.PlatException;

public class CommentServiceImpl implements ICommentService
{
    /**
     * spring 注入实例
     */
    private CommentDao commentDao;

    /**
     *
     * 获取评论列表
     *
     * @author zhuou
     * @param keyword
     *            查询关键字,没有查询 赋值为null
     * @param pageNum
     *            页码
     * @param pageRecordNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    public PageBean<CommentBean> getCommentList(String keyword, int companyId,
            int pageNum, int pageRecordNum) throws PlatException
    {
        int rowNum = (pageNum - 1) * pageRecordNum;

        Map<String, Object> param = new HashMap<String, Object>();
        param.put("keyword", keyword);
        param.put("rowNum", rowNum);
        param.put("pageNum", pageRecordNum);
        param.put("companyId", companyId);

        // 获取列表
        List<CommentBean> dataList = commentDao.selectList(param);
        int totalRecordNum = commentDao.selectCount(param);

        PageBean<CommentBean> pageBean = new PageBean<CommentBean>();
        pageBean.setTotalRecordNum(totalRecordNum);
        pageBean.setDataList(dataList);

        return pageBean;
    }

    /**
     *
     * 根据Id获取评论详情
     *
     * @author zhuou
     * @param commentId
     *            评论Id
     * @return CommentBean
     * @throws PlatException
     *             公共异常
     */
    public CommentBean getOne(int commentId) throws PlatException
    {
        return commentDao.selectOne(commentId);
    }

    /**
     *
     * 修改是否精华状态
     *
     * @author zhuou
     * @param isElite
     *            是否精华
     * @param commentId
     *            评论Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean updateEliteStatus(boolean isElite, long commentId)
        throws PlatException
    {
        return commentDao.updateEliteStatus(isElite, commentId) > 0 ? true
                : false;
    }

    /**
     *
     * 修改是否公开状态
     *
     * @author zhuou
     * @param isPrivate
     *            是否公开
     * @param commentId
     *            评论Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean updatePrivateStatus(boolean isPrivate, long commentId)
        throws PlatException
    {
        return commentDao.updatePrivateStatus(isPrivate, commentId) > 0 ? true
                : false;
    }

    /**
     *
     * 根据评论Id删除评论
     *
     * @author zhuou
     * @param commentIds
     *            评论Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean delCommentById(String commentIds) throws PlatException
    {
        if (StringUtil.isEmpty(commentIds))
        {
            return false;
        }
        return commentDao.delCommentById(commentIds) > 0 ? true : false;
    }

    public void setCommentDao(CommentDao commentDao)
    {
        this.commentDao = commentDao;
    }
}
