package com.oswift.cms.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.dao.ChannelDao;
import com.oswift.cms.dao.CommonModelDao;
import com.oswift.cms.dao.FriendSiteDao;
import com.oswift.cms.entity.ChannelBean;
import com.oswift.cms.entity.CommonModelBean;
import com.oswift.cms.entity.DictionaryBean;
import com.oswift.cms.entity.FriendSiteBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.service.IFriendSiteService;
import com.oswift.cms.system.cache.CMSCacheFileds;
import com.oswift.cms.system.cache.DictionaryManager;
import com.oswift.cms.utils.Field;
import com.oswift.cms.utils.PageCode;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.common.TimeUtil;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

public class FriendSiteServiceImpl implements IFriendSiteService
{
    /**
     * FriendSiteDaoImpl对象注入
     */
    private FriendSiteDao friendSiteDao;

    /**
     * CommonModelDaoImpl对象注入
     */
    private CommonModelDao commonModelDao;

    /**
     * ChannelDaoImpl对象注入
     */
    private ChannelDao channelDao;

    /**
     *
     * 添加友情链接信息
     *
     * @author zhuou
     * @param bean
     *            FriendSiteBean
     * @return boolean
     * @throws PlatException
     *             CMS公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean save(FriendSiteBean bean) throws PlatException
    {
        int rows = friendSiteDao.add(bean);
        if (rows <= 0 || bean.getFriendSiteId() <= 0)
        {
            return false;
        }

        bean.setItemId(bean.getFriendSiteId());
        rows = commonModelDao.add((CommonModelBean) bean);
        if (rows <= 0)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
        return true;
    }

    /**
     *
     * 获取公共内容信息，有分页
     *
     * @author zhuou
     * @param orderField
     *            排序字段
     * @param orderType
     *            排序类型ACS或者DESC
     * @param status
     *            状态
     * @param channelId
     *            栏目ID
     * @param searchType
     *            搜索字段
     * @param keyword
     *            关键字
     * @param pageRecordNum
     *            每页记录数
     * @param pageNum
     *            第几页
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    public PageBean<CommonModelBean> getCommonModel(String orderField, String orderType,
            int status, int channelId, String searchType, String keyword,
            int pageRecordNum, int pageNum, String tableName, int companyId)
        throws PlatException
    {
        int rowNum = (pageNum - 1) * pageRecordNum;

        Map<String, Object> param = new HashMap<String, Object>();
        if (StringUtil.isEmpty(orderField))
        {
            param.put("orderField", null);
        }
        else
        {
            param.put("orderField", orderField);
        }

        if (StringUtil.isEmpty(orderType))
        {
            param.put("orderType", null);
        }
        else
        {
            param.put("orderType", orderType);
        }
        param.put("status", status);
        param.put("channelId", channelId);

        if (StringUtil.isEmpty(searchType))
        {
            param.put("searchType", null);
        }
        else
        {
            param.put("searchType", searchType);
        }
        if (StringUtil.isEmpty(keyword))
        {
            param.put("keyword", null);
        }
        else
        {
            param.put("keyword", keyword);
        }
        param.put("rowNum", rowNum);
        param.put("pageNum", pageRecordNum);
        param.put("tableName", tableName);
        param.put("companyId", companyId);

        return commonModelDao.getCommonModel(param);
    }

    /**
     *
     * 更新内容状态
     *
     * @author zhuou
     * @param status
     *            状态
     * @param ids
     *            以“,”分隔的id字符串
     * @return boolean
     * @throws PlatException
     *             CMS公共异常
     */
    public boolean updateStatus(String status, String ids) throws PlatException
    {
        String[] id = ids.split(Field.COMMA);
        List<String> idList = new ArrayList<String>();
        for (String item : id)
        {
            idList.add(item);
        }
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("status", status);
        param.put("idList", idList);

        int rows = commonModelDao.updateStatus(param);
        if (rows <= 0)
        {
            return false;
        }
        return true;
    }

    /**
     *
     * 获取友情链接详情
     *
     * @author zhuou
     * @param id
     *            commonModelId
     * @return FriendSiteBean
     * @throws PlatException
     *             CMS公共异常
     */
    public FriendSiteBean getFriendSiteDetail(long id) throws PlatException
    {
        return friendSiteDao.selectOne(id);
    }

    /**
     *
     * 修改友情链接记录
     *
     * @author zhuou
     * @param FriendSiteBean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean update(FriendSiteBean friendSiteBean) throws PlatException
    {
        // 查询一下，判断是否被删除。防止多人操作
        FriendSiteBean bean = friendSiteDao.selectOne(friendSiteBean
                .getCommonModelId());
        if (null == bean)
        {
            throw new PlatException(PageCode.DATA_ALREADY_DEL);
        }

        friendSiteBean.setFriendSiteId(bean.getFriendSiteId());
        int rows = friendSiteDao.update(friendSiteBean);
        if (rows <= 0)
        {
            return false;
        }

        rows = commonModelDao.update((CommonModelBean) friendSiteBean);
        if (rows <= 0)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
        return true;
    }

    /**
     *
     * 删除一条友情链接记录
     *
     * @author zhuou
     * @param commonModelId
     *            公共Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean delete(int commonModelId) throws PlatException
    {
        FriendSiteBean friendSiteBean = friendSiteDao.selectOne(commonModelId);
        if (null != friendSiteBean)
        {
            int rows = commonModelDao.delete(commonModelId);
            if (rows <= 0)
            {
                return false;
            }

            rows = friendSiteDao.delete(friendSiteBean.getFriendSiteId());
            if (rows <= 0)
            {
                throw new PlatException(ErrorCode.COMMON_DB_ERROR);
            }
        }
        return true;
    }

    /**
    *
    * 加载友情链接新增页面
    *
    * @author zhuou
    * @param pageName
    *            jsp页面名称
    * @param companyId
    *            单位Id
    * @return ModelAndView
    * @throws PlatException
    *             公共异常
    */
   public ModelAndView loadAddFriendSite(String pageName, int companyId)
       throws PlatException
   {
       ModelAndView mv = new ModelAndView(pageName);

       // 获取默认栏目
       DictionaryBean bean = DictionaryManager
               .getValue(CMSCacheFileds.BusinessField.DEFAULT_FRIENDSITE_KEY);
       ChannelBean defaultChannel = channelDao.getDefaultChannel(companyId,
               bean.getFieldValue());
       mv.addObject("defaultChannel", defaultChannel);

       mv.addObject("date", TimeUtil.dateToString(new Date(),
               TimeUtil.ISO_DATE_NOSECOND_FORMAT));
       return mv;
   }

    public FriendSiteDao getFriendSiteDao()
    {
        return friendSiteDao;
    }

    public void setFriendSiteDao(FriendSiteDao friendSiteDao)
    {
        this.friendSiteDao = friendSiteDao;
    }

    public CommonModelDao getCommonModelDao()
    {
        return commonModelDao;
    }

    public void setCommonModelDao(CommonModelDao commonModelDao)
    {
        this.commonModelDao = commonModelDao;
    }

    public void setChannelDao(ChannelDao channelDao)
    {
        this.channelDao = channelDao;
    }
}
