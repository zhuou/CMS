package com.oswift.cms.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.dao.AnnounceDao;
import com.oswift.cms.dao.ChannelDao;
import com.oswift.cms.dao.CommonModelDao;
import com.oswift.cms.entity.AnnounceBean;
import com.oswift.cms.entity.ChannelBean;
import com.oswift.cms.entity.CommonModelBean;
import com.oswift.cms.entity.DictionaryBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.service.IAnnounceService;
import com.oswift.cms.system.cache.CMSCacheFileds;
import com.oswift.cms.system.cache.DictionaryManager;
import com.oswift.cms.utils.PageCode;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.common.TimeUtil;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

public class AnnounceServiceImpl implements IAnnounceService
{
    /**
     * CommonModelDaoImpl对象注入
     */
    private CommonModelDao commonModelDao;

    /**
     * AnnounceDaoImpl对象注入
     */
    private AnnounceDao announceDao;

    /**
     * ChannelDaoImpl对象注入
     */
    private ChannelDao channelDao;

    /**
     *
     * 获取公共内容信息，有分页
     *
     * @author zhuou
     * @param orderField
     *            排序字段
     * @param orderType
     *            排序类型ACS或者DESC
     * @param status
     *            状态
     * @param channelId
     *            栏目ID
     * @param searchType
     *            搜索字段
     * @param keyword
     *            关键字
     * @param pageRecordNum
     *            每页记录数
     * @param pageNum
     *            第几页
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    public PageBean<CommonModelBean> getCommonModel(String orderField, String orderType,
            int status, int channelId, String searchType, String keyword,
            int pageRecordNum, int pageNum, String tableName, int companyId)
        throws PlatException
    {
        int rowNum = (pageNum - 1) * pageRecordNum;

        Map<String, Object> param = new HashMap<String, Object>();
        if (StringUtil.isEmpty(orderField))
        {
            param.put("orderField", null);
        }
        else
        {
            param.put("orderField", orderField);
        }

        if (StringUtil.isEmpty(orderType))
        {
            param.put("orderType", null);
        }
        else
        {
            param.put("orderType", orderType);
        }
        param.put("status", status);
        param.put("channelId", channelId);

        if (StringUtil.isEmpty(searchType))
        {
            param.put("searchType", null);
        }
        else
        {
            param.put("searchType", searchType);
        }
        if (StringUtil.isEmpty(keyword))
        {
            param.put("keyword", null);
        }
        else
        {
            param.put("keyword", keyword);
        }
        param.put("rowNum", rowNum);
        param.put("pageNum", pageRecordNum);
        param.put("tableName", tableName);
        param.put("companyId", companyId);

        return commonModelDao.getCommonModel(param);
    }

    /**
     *
     * 新增一条公告记录
     *
     * @author zhuou
     * @param announceBean
     *            公告bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean save(AnnounceBean announceBean) throws PlatException
    {
        int rows = announceDao.add(announceBean);
        if (rows <= 0 || announceBean.getAnnounceId() <= 0)
        {
            return false;
        }

        announceBean.setItemId(announceBean.getAnnounceId());
        rows = commonModelDao.add((CommonModelBean) announceBean);
        if (rows <= 0)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
        return true;
    }

    /**
     *
     * 修改公告记录
     *
     * @author zhuou
     * @param announceBean
     *            公告bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean update(AnnounceBean announceBean) throws PlatException
    {
        // 查询一下，判断是否被删除。防止多人操作
        AnnounceBean bean = announceDao.selectOne(announceBean
                .getCommonModelId());
        if (null == bean)
        {
            throw new PlatException(PageCode.DATA_ALREADY_DEL);
        }

        announceBean.setAnnounceId(bean.getAnnounceId());
        int rows = announceDao.update(announceBean);
        if (rows <= 0)
        {
            return false;
        }

        rows = commonModelDao.update((CommonModelBean) announceBean);
        if (rows <= 0)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
        return true;
    }

    /**
     *
     * 删除一条公告记录
     *
     * @author zhuou
     * @param commonModelId
     *            公共Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean delete(int commonModelId) throws PlatException
    {
        AnnounceBean bean = announceDao.selectOne(commonModelId);
        if (null != bean)
        {
            int rows = commonModelDao.delete(commonModelId);
            if (rows <= 0)
            {
                return false;
            }

            rows = announceDao.delete(bean.getAnnounceId());
            if (rows <= 0)
            {
                throw new PlatException(ErrorCode.COMMON_DB_ERROR);
            }
        }

        return true;
    }

    /**
     *
     * 根据Id查询一条公告记录
     *
     * @author zhuou
     * @param commonModelId
     *            公共Id
     * @return AnnounceBean
     * @throws PlatException
     *             公共异常
     */
    public AnnounceBean getOne(int commonModelId) throws PlatException
    {
        return announceDao.selectOne(commonModelId);
    }

    /**
     *
     * 加载公告新增页面
     *
     * @author zhuou
     * @param pageName
     *            jsp页面名称
     * @param companyId
     *            单位Id
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    public ModelAndView loadAddAnnounce(String pageName, int companyId)
        throws PlatException
    {
        ModelAndView mv = new ModelAndView(pageName);

        // 获取默认栏目
        DictionaryBean bean = DictionaryManager
                .getValue(CMSCacheFileds.BusinessField.DEFAULT_ANNOUNCE_KEY);
        ChannelBean defaultChannel = channelDao.getDefaultChannel(companyId,
                bean.getFieldValue());
        mv.addObject("defaultChannel", defaultChannel);

        mv.addObject("date", TimeUtil.dateToString(new Date(),
                TimeUtil.ISO_DATE_NOSECOND_FORMAT));
        return mv;
    }

    /**
     *
     * 加载公告修改页面
     *
     * @author zhuou
     * @param pageName
     *            jsp页面名称
     * @param companyId
     *            单位Id
     * @param commonModelId
     *            内容公共ID
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    public ModelAndView loadUpdateArticle(String pageName, int companyId,
            int commonModelId) throws PlatException
    {
        ModelAndView mv = new ModelAndView(pageName);

        AnnounceBean bean = announceDao.selectOne(commonModelId);
        mv.addObject("announceBean", bean);

        return mv;
    }

    public CommonModelDao getCommonModelDao()
    {
        return commonModelDao;
    }

    public void setCommonModelDao(CommonModelDao commonModelDao)
    {
        this.commonModelDao = commonModelDao;
    }

    public AnnounceDao getAnnounceDao()
    {
        return announceDao;
    }

    public void setAnnounceDao(AnnounceDao announceDao)
    {
        this.announceDao = announceDao;
    }

    public void setChannelDao(ChannelDao channelDao)
    {
        this.channelDao = channelDao;
    }
}
