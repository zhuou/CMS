package com.oswift.cms.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.dao.ArticleDao;
import com.oswift.cms.dao.ChannelDao;
import com.oswift.cms.dao.CommonModelDao;
import com.oswift.cms.entity.ArticleBean;
import com.oswift.cms.entity.ChannelBean;
import com.oswift.cms.entity.CommonModelBean;
import com.oswift.cms.entity.DictionaryBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.service.IArticleService;
import com.oswift.cms.system.cache.CMSCacheFileds;
import com.oswift.cms.system.cache.DictionaryManager;
import com.oswift.cms.utils.PageCode;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.common.TimeUtil;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

public class ArticleServiceImpl implements IArticleService
{
    /**
     * CommonModelDaoImpl对象注入
     */
    private CommonModelDao commonModelDao;

    /**
     * ArticleDaoImpl对象注入
     */
    private ArticleDao articleDao;

    /**
     * ChannelDaoImpl对象注入
     */
    private ChannelDao channelDao;

    /**
     *
     * 获取公共内容信息，有分页
     *
     * @author zhuou
     * @param orderField
     *            排序字段
     * @param orderType
     *            排序类型ACS或者DESC
     * @param status
     *            状态
     * @param channelId
     *            栏目ID
     * @param searchType
     *            搜索字段
     * @param keyword
     *            关键字
     * @param pageRecordNum
     *            每页记录数
     * @param pageNum
     *            第几页
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    public PageBean<CommonModelBean> getCommonModel(String orderField,
            String orderType, int status, int channelId, String searchType,
            String keyword, int pageRecordNum, int pageNum, String tableName,
            int companyId) throws PlatException
    {
        int rowNum = (pageNum - 1) * pageRecordNum;

        Map<String, Object> param = new HashMap<String, Object>();
        if (StringUtil.isEmpty(orderField))
        {
            param.put("orderField", null);
        }
        else
        {
            param.put("orderField", orderField);
        }

        if (StringUtil.isEmpty(orderType))
        {
            param.put("orderType", null);
        }
        else
        {
            param.put("orderType", orderType);
        }
        param.put("status", status);
        param.put("channelId", channelId);

        if (StringUtil.isEmpty(searchType))
        {
            param.put("searchType", null);
        }
        else
        {
            param.put("searchType", searchType);
        }
        if (StringUtil.isEmpty(keyword))
        {
            param.put("keyword", null);
        }
        else
        {
            param.put("keyword", keyword);
        }
        param.put("rowNum", rowNum);
        param.put("pageNum", pageRecordNum);
        param.put("tableName", tableName);
        param.put("companyId", companyId);

        return commonModelDao.getCommonModel(param);
    }

    /**
     *
     * 新增一条文章记录
     *
     * @author zhuou
     * @param articleBean
     *            文章bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean save(ArticleBean articleBean) throws PlatException
    {
        int rows = articleDao.add(articleBean);
        if (rows <= 0 || articleBean.getArticleId() <= 0)
        {
            return false;
        }

        articleBean.setItemId(articleBean.getArticleId());
        rows = commonModelDao.add((CommonModelBean) articleBean);
        if (rows <= 0)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
        return true;
    }

    /**
     *
     * 修改文章记录
     *
     * @author zhuou
     * @param articleBean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean update(ArticleBean articleBean) throws PlatException
    {
        // 查询一下，判断是否被删除。防止多人操作
        ArticleBean bean = articleDao.selectOne(articleBean.getCommonModelId());
        if (null == bean)
        {
            throw new PlatException(PageCode.DATA_ALREADY_DEL);
        }

        articleBean.setArticleId(bean.getArticleId());
        int rows = articleDao.update(articleBean);
        if (rows <= 0)
        {
            return false;
        }
        if (StringUtil.isBlank(articleBean.getUpdateTime()))
        {
            articleBean.setUpdateTime(TimeUtil
                    .getCurTimeByFormat(TimeUtil.ISO_DATE_TIME_FORMAT));
        }
        rows = commonModelDao.update((CommonModelBean) articleBean);
        if (rows <= 0)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
        return true;
    }

    /**
     *
     * 删除一条文章记录
     *
     * @author zhuou
     * @param commonModelId
     *            公共Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean delete(int commonModelId) throws PlatException
    {
        ArticleBean articleBean = articleDao.selectOne(commonModelId);
        if (null != articleBean)
        {
            int rows = commonModelDao.delete(commonModelId);
            if (rows <= 0)
            {
                return false;
            }

            rows = articleDao.delete(articleBean.getArticleId());
            if (rows <= 0)
            {
                throw new PlatException(ErrorCode.COMMON_DB_ERROR);
            }
        }
        return true;
    }

    /**
     *
     * 根据Id查询一条文章记录
     *
     * @author zhuou
     * @param commonModelId
     *            公共Id
     * @return ArticleBean
     * @throws PlatException
     *             公共异常
     */
    public ArticleBean getOne(int commonModelId) throws PlatException
    {
        return articleDao.selectOne(commonModelId);
    }

    /**
     *
     * 加载文章新增页面
     *
     * @author zhuou
     * @param pageName
     *            jsp页面名称
     * @param companyId
     *            单位Id
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    public ModelAndView loadAddArticle(String pageName, int companyId)
        throws PlatException
    {
        ModelAndView mv = new ModelAndView(pageName);

        // 获取默认栏目
        DictionaryBean bean = DictionaryManager
                .getValue(CMSCacheFileds.BusinessField.DEFAULT_ARTICLE_KEY);
        ChannelBean defaultChannel = channelDao.getDefaultChannel(companyId,
                bean.getFieldValue());
        mv.addObject("defaultChannel", defaultChannel);

        mv.addObject("date", TimeUtil.dateToString(new Date(),
                TimeUtil.ISO_DATE_NOSECOND_FORMAT));
        return mv;
    }

    /**
     *
     * 加载文章修改页面
     *
     * @author zhuou
     * @param pageName
     *            jsp页面名称
     * @param companyId
     *            单位Id
     * @param commonModelId
     *            内容公共ID
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    public ModelAndView loadUpdateArticle(String pageName, int companyId,
            int commonModelId) throws PlatException
    {
        ModelAndView mv = new ModelAndView(pageName);
        ArticleBean bean = articleDao.selectOne(commonModelId);
        mv.addObject("articleBean", bean);
        return mv;
    }

    public CommonModelDao getCommonModelDao()
    {
        return commonModelDao;
    }

    public void setCommonModelDao(CommonModelDao commonModelDao)
    {
        this.commonModelDao = commonModelDao;
    }

    public ArticleDao getArticleDao()
    {
        return articleDao;
    }

    public void setArticleDao(ArticleDao articleDao)
    {
        this.articleDao = articleDao;
    }

    public void setChannelDao(ChannelDao channelDao)
    {
        this.channelDao = channelDao;
    }
}
