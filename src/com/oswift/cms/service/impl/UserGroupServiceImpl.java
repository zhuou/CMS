package com.oswift.cms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.dao.UserGroupDao;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.entity.UserGroupBean;
import com.oswift.cms.service.IUserGroupService;
import com.oswift.cms.utils.PageCode;
import com.oswift.utils.exception.PlatException;

public class UserGroupServiceImpl implements IUserGroupService
{
    /**
     * UserGroupDaoImpl对象注入
     */
    private UserGroupDao userGroupDao;

    /**
     *
     * 根据公司Id获取用户组信息
     *
     * @author zhuou
     * @param companyId
     * @return List
     * @throws PlatException
     *             CMS公共异常
     */
    public List<UserGroupBean> getUserGroupByCompanyId(int companyId)
        throws PlatException
    {
        return userGroupDao.getUserGroupByCompanyId(companyId);
    }

    /**
     *
     * 获取用户组列表信息，有分页
     *
     * @author zhuou
     * @param pageRecordNum
     *            每页记录数
     * @param pageNum
     *            第几页
     * @param companyId
     *            单位Id
     * @return PageBean
     * @throws PlatException
     *             公共异常
     */
    public PageBean<UserGroupBean> getUserGroups(int pageRecordNum,
            int pageNum, int companyId) throws PlatException
    {
        int rowNum = (pageNum - 1) * pageRecordNum;
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("rowNum", rowNum);
        param.put("pageNum", pageRecordNum);
        param.put("companyId", companyId);

        PageBean<UserGroupBean> page = new PageBean<UserGroupBean>();
        List<UserGroupBean> list = userGroupDao.selectList(param);
        int recordNum = userGroupDao.selectCount(param);

        page.setTotalRecordNum(recordNum);
        page.setDataList(list);

        return page;
    }

    /**
     *
     * 加载用户组修改页面
     *
     * @author zhuou
     * @param pageName
     *            jsp页面
     * @param companyId
     *            单位Id
     * @param userGroupId
     *            用户组Id
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    public ModelAndView loadUpdateUserGroup(String pageName, int companyId,
            int userGroupId) throws PlatException
    {
        UserGroupBean bean = userGroupDao.selectOne(userGroupId);
        if (null == bean)
        {
            throw new PlatException(PageCode.DATA_ALREADY_DEL);
        }

        ModelAndView mv = new ModelAndView(pageName);
        mv.addObject("userGroupBean", bean);
        return mv;
    }

    /**
     *
     * 添加一条用户组信息
     *
     * @author zhuou
     * @param bean
     *            UserGroupBean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean save(UserGroupBean bean) throws PlatException
    {
        return userGroupDao.add(bean) > 0 ? true : false;
    }

    /**
     *
     * 修改一条用户组信息
     *
     * @author zhuou
     * @param bean
     *            UserGroupBean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean update(UserGroupBean bean) throws PlatException
    {
        UserGroupBean bean1 = userGroupDao.selectOne(bean.getGroupId());
        if (null == bean1)
        {
            throw new PlatException(PageCode.DATA_ALREADY_DEL);
        }
        return userGroupDao.update(bean) > 0 ? true : false;
    }

    /**
     *
     * 获取用户组详情信息
     *
     * @author zhuou
     * @param userGroupId
     *            用户组Id
     * @return UserGroupBean
     * @throws PlatException
     *             公共异常
     */
    public UserGroupBean getOne(int userGroupId) throws PlatException
    {
        UserGroupBean bean = userGroupDao.selectOne(userGroupId);
        if (null == bean)
        {
            throw new PlatException(PageCode.DATA_ALREADY_DEL);
        }
        return bean;
    }

    // public boolean delete(int userGroupId) throws PlatException
    // {
    // UserGroupBean bean = userGroupDao.selectOne(userGroupId);
    // if (null == bean)
    // {
    // throw new PlatException(PageCode.DATA_ALREADY_DEL);
    // }
    // }

    /**
     *
     * 根据id查询用户组
     *
     * @author zhuou
     * @param ids
     *            用户组id
     * @return List
     * @throws PlatException
     *             公共异常
     */
    public List<UserGroupBean> getUserGroupByIds(String ids)
        throws PlatException
    {
        return userGroupDao.getUserGroupByIds(ids);
    }

    public void setUserGroupDao(UserGroupDao userGroupDao)
    {
        this.userGroupDao = userGroupDao;
    }
}
