package com.oswift.cms.service.impl;

import com.oswift.cms.dao.CommonModelDao;
import com.oswift.cms.service.ICommonModelService;
import com.oswift.utils.exception.PlatException;

public class CommonModelServiceImpl implements ICommonModelService
{
    /**
     * CommonModelDaoImpl对象注入
     */
    private CommonModelDao commonModelDao;

    /**
     *
     * 更新天点击率
     *
     * @author zhuou
     * @return int
     * @throws PlatException
     *             CMS公共异常
     */
    public int updateDayHit() throws PlatException
    {
        return commonModelDao.updateDayHit();
    }

    /**
     *
     * 更新周点击率
     *
     * @author zhuou
     * @return int
     * @throws PlatException
     *             CMS公共异常
     */
    public int updateWeekHit() throws PlatException
    {
        return commonModelDao.updateWeekHit();
    }

    /**
     *
     * 更新月点击率
     *
     * @author zhuou
     * @return int
     * @throws PlatException
     *             CMS公共异常
     */
    public int updateMonthHit() throws PlatException
    {
        return commonModelDao.updateMonthHit();
    }

    public CommonModelDao getCommonModelDao()
    {
        return commonModelDao;
    }

    public void setCommonModelDao(CommonModelDao commonModelDao)
    {
        this.commonModelDao = commonModelDao;
    }
}
