package com.oswift.cms.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.dao.ChannelDao;
import com.oswift.cms.dao.CommonModelDao;
import com.oswift.cms.dao.GuestBookDao;
import com.oswift.cms.entity.ChannelBean;
import com.oswift.cms.entity.CommonModelBean;
import com.oswift.cms.entity.DictionaryBean;
import com.oswift.cms.entity.GuestBookBean;
import com.oswift.cms.entity.GuestBookReplyBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.service.IGuestBookService;
import com.oswift.cms.system.cache.CMSCacheFileds;
import com.oswift.cms.system.cache.DictionaryManager;
import com.oswift.cms.utils.PageCode;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.common.TimeUtil;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

public class GuestBookServiceImpl implements IGuestBookService
{
    /**
     * CommonModelDaoImpl对象注入
     */
    private CommonModelDao commonModelDao;

    /**
     * GuestBookDaoImpl对象注入
     */
    private GuestBookDao guestBookDao;

    /**
     * ChannelDaoImpl对象注入
     */
    private ChannelDao channelDao;

    /**
     *
     * 获取公共内容信息，有分页
     *
     * @author zhuou
     * @param orderField
     *            排序字段
     * @param orderType
     *            排序类型ACS或者DESC
     * @param status
     *            状态
     * @param channelId
     *            栏目ID
     * @param searchType
     *            搜索字段
     * @param keyword
     *            关键字
     * @param pageRecordNum
     *            每页记录数
     * @param pageNum
     *            第几页
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    public PageBean<GuestBookBean> getCommonModel(String orderField,
            String orderType, int status, int channelId, String searchType,
            String keyword, int pageRecordNum, int pageNum, String tableName,
            int companyId) throws PlatException
    {
        int rowNum = (pageNum - 1) * pageRecordNum;

        Map<String, Object> param = new HashMap<String, Object>();
        if (StringUtil.isEmpty(orderField))
        {
            param.put("orderField", null);
        }
        else
        {
            param.put("orderField", orderField);
        }

        if (StringUtil.isEmpty(orderType))
        {
            param.put("orderType", null);
        }
        else
        {
            param.put("orderType", orderType);
        }
        param.put("status", status);
        param.put("channelId", channelId);

        if (StringUtil.isEmpty(searchType))
        {
            param.put("searchType", null);
        }
        else
        {
            param.put("searchType", searchType);
        }
        if (StringUtil.isEmpty(keyword))
        {
            param.put("keyword", null);
        }
        else
        {
            param.put("keyword", keyword);
        }
        param.put("rowNum", rowNum);
        param.put("pageNum", pageRecordNum);
        param.put("tableName", tableName);
        param.put("companyId", companyId);

        return guestBookDao.getGuestBooks(param);
    }

    /**
     *
     * 加载留言新增页面
     *
     * @author zhuou
     * @param pageName
     *            jsp页面名称
     * @param companyId
     *            单位Id
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    public ModelAndView loadAddGuestBook(String pageName, int companyId)
        throws PlatException
    {
        ModelAndView mv = new ModelAndView(pageName);

        // 获取默认栏目
        DictionaryBean bean = DictionaryManager
                .getValue(CMSCacheFileds.BusinessField.DEFAULT_GUESTBOOK_KEY);
        ChannelBean defaultChannel = channelDao.getDefaultChannel(companyId,
                bean.getFieldValue());
        mv.addObject("defaultChannel", defaultChannel);

        mv.addObject("date", TimeUtil.dateToString(new Date(),
                TimeUtil.ISO_DATE_NOSECOND_FORMAT));
        return mv;
    }

    /**
     *
     * 加载留言修改页面
     *
     * @author zhuou
     * @param pageName
     *            jsp页面名称
     * @param companyId
     *            单位Id
     * @param commonModelId
     *            内容公共ID
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    public ModelAndView loadUpdateGuestBook(String pageName, int companyId,
            int commonModelId) throws PlatException
    {
        ModelAndView mv = new ModelAndView(pageName);
        GuestBookBean bean = guestBookDao.selectOne(commonModelId);
        mv.addObject("guestBookBean", bean);

        return mv;
    }

    /**
     *
     * 新增一条留言记录
     *
     * @author zhuou
     * @param GuestBook
     *            留言bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean save(GuestBookBean guestBookBean) throws PlatException
    {
        int rows = guestBookDao.add(guestBookBean);
        if (rows <= 0 || guestBookBean.getGuestBookId() <= 0)
        {
            return false;
        }

        guestBookBean.setItemId(guestBookBean.getGuestBookId());
        rows = commonModelDao.add((CommonModelBean) guestBookBean);
        if (rows <= 0)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
        return true;
    }

    /**
     *
     * 修改留言记录
     *
     * @author zhuou
     * @param GuestBook
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean update(GuestBookBean guestBookBean) throws PlatException
    {
        // 查询一下，判断是否被删除。防止多人操作
        GuestBookBean bean = guestBookDao.selectOne(guestBookBean
                .getCommonModelId());
        if (null == bean)
        {
            throw new PlatException(PageCode.DATA_ALREADY_DEL);
        }

        guestBookBean.setGuestBookId(bean.getGuestBookId());
        int rows = guestBookDao.update(guestBookBean);
        if (rows <= 0)
        {
            return false;
        }

        rows = commonModelDao.update((CommonModelBean) guestBookBean);
        if (rows <= 0)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
        return true;
    }

    /**
     *
     * 根据Id查询一条留言记录
     *
     * @author zhuou
     * @param commonModelId
     *            公共Id
     * @return GuestBookBean
     * @throws PlatException
     *             公共异常
     */
    public GuestBookBean getOne(long commonModelId) throws PlatException
    {
        GuestBookBean bean = guestBookDao.selectOne(commonModelId);
        List<GuestBookReplyBean> list = guestBookDao.getReplyList(bean
                .getGuestBookId());
        bean.setList(list);

        return bean;
    }

    /**
     *
     * 删除一条留言记录
     *
     * @author zhuou
     * @param commonModelId
     *            公共Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean delete(int commonModelId) throws PlatException
    {
        GuestBookBean guestBookBean = guestBookDao.selectOne(commonModelId);
        if (null == guestBookBean)
        {
            throw new PlatException(PageCode.DATA_ALREADY_DEL);
        }

        int rows = commonModelDao.delete(commonModelId);
        if (rows <= 0)
        {
            return false;
        }

        rows = guestBookDao.delete(guestBookBean.getGuestBookId());
        if (rows <= 0)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }

        // 删除留言下回复信息
        rows = guestBookDao.delReplyByGBId(guestBookBean.getGuestBookId());
        if (rows <= 0)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
        return true;
    }

    /**
     *
     * 新增留言回复
     *
     * @author zhuou
     * @param replyBean
     *            回复bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean addReply(GuestBookReplyBean replyBean) throws PlatException
    {
        boolean isSuccess = guestBookDao.addReply(replyBean) > 0 ? true : false;
        if (!isSuccess)
        {
            return isSuccess;
        }

        // 将回复数加1
        isSuccess = guestBookDao.updateGuestBookReplyNum("add", replyBean
                .getGuestBookId()) > 0 ? true : false;
        if (!isSuccess)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR,
                    " add ReplyCount is fail! ");
        }

        return isSuccess;
    }

    /**
     *
     * 删除一条回复内容
     *
     * @author zhuou
     * @param replyId
     *            回复Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean delReply(long replyId) throws PlatException
    {
        GuestBookReplyBean bean = guestBookDao.getReplyByRId(replyId);
        if (null == bean)
        {
            throw new PlatException("The Reply has been deleted!");
        }
        boolean isSuccess = guestBookDao.delReply(replyId) > 0 ? true : false;
        if (!isSuccess)
        {
            return isSuccess;
        }

        // 将回复数加1
        isSuccess = guestBookDao.updateGuestBookReplyNum("del", bean
                .getGuestBookId()) > 0 ? true : false;
        if (!isSuccess)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR,
                    " Subtract ReplyCount is fail! ");
        }

        return isSuccess;
    }

    /**
     *
     * 修改回复内容
     *
     * @author zhuou
     * @param replyBean
     *            回复bean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean updateReply(GuestBookReplyBean replyBean)
        throws PlatException
    {
        return guestBookDao.updateReply(replyBean) > 0 ? true : false;
    }

    /**
     *
     *
     * 根据回复Id回去回复详情
     *
     * @author zhuou
     * @param replyId
     *            回复Id
     * @return GuestBookReplyBean
     * @throws PlatException
     *             公共异常
     */
    public GuestBookReplyBean getReplyByRId(long replyId) throws PlatException
    {
        return guestBookDao.getReplyByRId(replyId);
    }

    /**
     *
     * 获取留言回复，有分页
     *
     * @author zhuou
     * @param searchField
     *            搜索字段
     * @param keyword
     *            搜索关键字
     * @param pageNum
     *            第几页
     * @param pageRecordNum
     *            没有条数
     * @param companyId
     *            公司ID
     * @return PageBean
     * @throws PlatException
     *             公共异常
     */
    public PageBean<GuestBookReplyBean> getReplysPageCurrent(
            String searchField, String keyword, int pageNum, int pageRecordNum,
            int companyId) throws PlatException
    {
        int rowNum = (pageNum - 1) * pageRecordNum;
        return guestBookDao.getReplysPageCurrent(searchField, keyword, rowNum,
                pageRecordNum, companyId);
    }

    /**
     *
     * 根据留言Id获取留言详细信息
     *
     * @author zhuou
     * @param guestBookId
     *            留言Id
     * @return GuestBookBean
     * @throws PlatException
     *             公共异常
     */
    public GuestBookBean getGuestBookByGId(long guestBookId)
        throws PlatException
    {
        return guestBookDao.getGuestBookByGId(guestBookId);
    }

    public CommonModelDao getCommonModelDao()
    {
        return commonModelDao;
    }

    public void setCommonModelDao(CommonModelDao commonModelDao)
    {
        this.commonModelDao = commonModelDao;
    }

    public GuestBookDao getGuestBookDao()
    {
        return guestBookDao;
    }

    public void setGuestBookDao(GuestBookDao guestBookDao)
    {
        this.guestBookDao = guestBookDao;
    }

    public void setChannelDao(ChannelDao channelDao)
    {
        this.channelDao = channelDao;
    }
}
