package com.oswift.cms.service.impl;

import org.springframework.transaction.annotation.Transactional;

import com.oswift.cms.dao.ReceptionPageUrlDao;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.entity.ReceptionPageUrlBean;
import com.oswift.cms.entity.reception.ReceptionPageUrl;
import com.oswift.cms.service.IReceptionPageUrlService;
import com.oswift.cms.system.cache.ReceptionPageUrlManager;
import com.oswift.cms.utils.PageCode;
import com.oswift.utils.exception.PlatException;

/**
 *
 * 操作前台页面url类
 *
 * @author zhuou
 * @version C03 2014-6-13
 * @since OSwift GPM V2.0
 */
public class ReceptionPageUrlServiceImpl implements IReceptionPageUrlService
{

    /**
     * spring注入
     */
    private ReceptionPageUrlDao receptionPageUrlDao;

    /**
     *
     * 获取网站前台URL配置信息列表
     *
     * @author zhuou
     * @param keyword
     *            查询关键字
     * @param pageNum
     *            页码
     * @param pageRecordNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    public PageBean<ReceptionPageUrlBean> getReceptionPageUrlList(
            String keyword, int pageNum, int pageRecordNum, int companyId)
        throws PlatException
    {
        int rowNum = (pageNum - 1) * pageRecordNum;

        return receptionPageUrlDao.getReceptionPageUrlList(keyword, rowNum,
                pageRecordNum, companyId);
    }

    /**
     *
     * 新增站点前台url数据
     *
     * @author zhuou
     * @param bean
     *            站点前台url
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean add(ReceptionPageUrlBean bean) throws PlatException
    {
        // 先判断是否存在
        int rows = receptionPageUrlDao.dataIsExist(bean.getSiteId(), bean
                .getUrlType(), bean.getNickName());
        if (rows > 0)
        {
            throw new PlatException(PageCode.RECEPTIONPAGEURL_ISEXIST);
        }

        rows = receptionPageUrlDao.add(bean);
        if (rows <= 0)
        {
            return false;
        }

        // 存入缓存中
        ReceptionPageUrl urlBean = new ReceptionPageUrl();
        urlBean.setJspPath(bean.getJspPath());
        urlBean.setNickName(bean.getNickName());
        urlBean.setPageUrlId(bean.getPageUrlId());
        urlBean.setSiteId(bean.getSiteId());
        urlBean.setUrlType(bean.getUrlType());
        ReceptionPageUrlManager.add(urlBean);
        return true;
    }

    /**
     *
     * 删除站点前台url数据
     *
     * @author zhuou
     * @param pageUrlId
     *            id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean del(int pageUrlId) throws PlatException
    {
        ReceptionPageUrlBean bean = receptionPageUrlDao.selectOne(pageUrlId);
        if (null == bean)
        {
            throw new PlatException(PageCode.RECEPTIONPAGEURL_ISNOTEXIST);
        }

        boolean isSuccess = receptionPageUrlDao.delete(pageUrlId) > 0 ? true
                : false;
        if (isSuccess)
        {
            ReceptionPageUrlManager.remove(bean.getSiteId(), bean.getUrlType(),
                    bean.getNickName());
        }

        return true;
    }

    /**
     *
     * 修改一天站点url数据
     *
     * @author zhuou
     * @param newBean
     *            站点url数据
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean update(ReceptionPageUrlBean newBean) throws PlatException
    {
        ReceptionPageUrlBean oldBean = receptionPageUrlDao.selectOne(newBean
                .getPageUrlId());
        if (null == oldBean)
        {
            throw new PlatException(PageCode.RECEPTIONPAGEURL_ISNOTEXIST);
        }

        // 获取siteId赋值给newBean实体
        newBean.setSiteId(oldBean.getSiteId());
        boolean isSuccess = receptionPageUrlDao.update(newBean) > 0 ? true
                : false;
        if (isSuccess)
        {
            // 先删除
            ReceptionPageUrlManager.remove(oldBean.getSiteId(), oldBean
                    .getUrlType(), oldBean.getNickName());

            // 在心中
            ReceptionPageUrl urlBean = new ReceptionPageUrl();
            urlBean.setJspPath(newBean.getJspPath());
            urlBean.setNickName(newBean.getNickName());
            urlBean.setPageUrlId(newBean.getPageUrlId());
            urlBean.setSiteId(newBean.getSiteId());
            urlBean.setUrlType(newBean.getUrlType());
            ReceptionPageUrlManager.add(urlBean);
        }

        return true;
    }

    /**
     *
     * 根据pageUrlId获取详情
     *
     * @author zhuou
     * @param pageUrlId
     * @return ReceptionPageUrlBean
     * @throws PlatException
     *             公共异常
     */
    public ReceptionPageUrlBean getReceptionPageUrlDetail(int pageUrlId)
        throws PlatException
    {
        return receptionPageUrlDao.selectOne(pageUrlId);
    }

    public void setReceptionPageUrlDao(ReceptionPageUrlDao receptionPageUrlDao)
    {
        this.receptionPageUrlDao = receptionPageUrlDao;
    }
}
