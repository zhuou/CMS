package com.oswift.cms.system.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oswift.cms.entity.reception.ReceptionPageUrl;
import com.oswift.utils.cache.BusinessCache;
import com.oswift.utils.cache.CacheAccessException;
import com.oswift.utils.cache.manager.CacheManager;
import com.oswift.utils.exception.PlatException;

/**
 *
 * 前台页面url缓存操作类
 *
 * @author zhuou
 * @version C03 2014-6-14
 * @since OSwift GPM V2.0
 */
public class ReceptionPageUrlManager
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(ReceptionPageUrlManager.class);

    /**
     * 记录系统参数缓存实例
     */
    private static BusinessCache pageUrlCacheInstance = CacheManager
            .getBusinessCache(CMSCacheFileds.CacheName.RECEPTIONPAGEURLCACHE);

    /**
     *
     * 根据key获取ReceptionPageUrl
     *
     * @author zhuou
     * @param key
     *            key值
     * @return PageBean
     */
    public static ReceptionPageUrl getValue(String key)
    {
        try
        {
            ReceptionPageUrl bean = (ReceptionPageUrl) pageUrlCacheInstance
                    .get(key);
            return bean;
        }
        catch (CacheAccessException e)
        {
            log.error("Fail to get ReceptionPageUrl, key=" + key);
        }
        return null;
    }

    /**
     *
     * 根据key删除指定的记录
     *
     * @author zhuou
     * @param siteId
     *            站点Id
     * @param urlType
     *            前台页面url类型
     * @param nickName
     *            前台页面url名称,英文命名，index表示首页 键值
     * @throws PlatException
     *             公共异常
     */
    public static void remove(int siteId, int urlType, String nickName)
        throws PlatException
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            sb.append(siteId);
            sb.append("|");
            sb.append(urlType);
            sb.append("|");
            sb.append(nickName);

            pageUrlCacheInstance.remove(sb.toString());
        }
        catch (CacheAccessException e)
        {
            String key = sb.toString();
            log.error("Fail to remove ReceptionPageUrl, key=" + key);
            throw new PlatException("Fail to remove ReceptionPageUrl, key="
                    + key);
        }
    }

    /**
     *
     * 新增一个page缓存
     *
     * @author zhuou
     * @param bean
     *            PageBean
     * @throws PlatException
     *             公共异常
     */
    public static void add(ReceptionPageUrl bean) throws PlatException
    {
        try
        {
            if (null == bean)
            {
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(bean.getSiteId());
            sb.append("|");
            sb.append(bean.getUrlType());
            sb.append("|");
            sb.append(bean.getNickName());

            pageUrlCacheInstance.set(sb.toString(), bean);
        }
        catch (CacheAccessException e)
        {
            log.error("Fail to add ReceptionPageUrl, pageUrlId="
                    + bean.getPageUrlId());
            throw new PlatException("Fail to add ReceptionPageUrl, pageUrlId="
                    + bean.getPageUrlId());
        }
    }
}
