package com.oswift.cms.system.cache.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oswift.cms.dao.DictionaryDao;
import com.oswift.cms.entity.DictionaryBean;
import com.oswift.cms.system.cache.CMSCacheFileds;
import com.oswift.utils.cache.AbstractBusinessCache;
import com.oswift.utils.cache.CacheAccessException;
import com.oswift.utils.cache.operate.CacheMap;
import com.oswift.utils.exception.PlatException;


/**
 *
 * 缓存字典表信息
 *
 * @author zhuou
 * @version C03 2013-3-14
 * @since OSwift GPM V1.0
 */
public class DictionaryCacheImpl extends AbstractBusinessCache
{

    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(DictionaryCacheImpl.class);

    /**
     * 注入对象DictionaryDaoImpl
     */
    private DictionaryDao dictionaryDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public void init() throws CacheAccessException
    {
        try
        {
            List<DictionaryBean> list = dictionaryDao.getCacheDictionarys();
            CacheMap cache = this.getCacheMap();

            // 先清除缓存
            cache.clear();

            if (null != list && !list.isEmpty())
            {
                for (DictionaryBean bean : list)
                {
                    cache.put(bean.getFieldName(), bean);
                }
            }
        }
        catch (PlatException e)
        {
            log.error("Failure data cache dictionary.", e);
        }

        log.info("Cache ->> " + CMSCacheFileds.CacheName.DICTIONARYCACHE
                + " had Loaded.");
    }

    public DictionaryDao getDictionaryDao()
    {
        return dictionaryDao;
    }

    public void setDictionaryDao(DictionaryDao dictionaryDao)
    {
        this.dictionaryDao = dictionaryDao;
    }
}
