package com.oswift.cms.system.cache.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oswift.cms.dao.reception.ReceptionWebConfigDao;
import com.oswift.cms.entity.reception.WebConfig;
import com.oswift.cms.system.cache.CMSCacheFileds;
import com.oswift.cms.utils.Field;
import com.oswift.utils.cache.AbstractBusinessCache;
import com.oswift.utils.cache.CacheAccessException;
import com.oswift.utils.cache.operate.CacheMap;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.PlatException;

public class WebConfigCacheImpl extends AbstractBusinessCache
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(WebConfigCacheImpl.class);

    /**
     * spring 注入
     */
    private ReceptionWebConfigDao webConfigDao;

    @Override
    public void init() throws CacheAccessException
    {
        try
        {
            CacheMap cache = this.getCacheMap();

            // 先清除缓存
            cache.clear();

            List<WebConfig> list = webConfigDao.selectList(null);
            for (WebConfig bean : list)
            {
                String siteUrl = bean.getSiteUrl();
                if (!StringUtil.isEmpty(siteUrl))
                {
                    if (siteUrl.indexOf(Field.COMMA) != -1)
                    {
                        String[] urls = siteUrl.split(Field.COMMA);
                        WebConfig newBean = null;
                        for (String s : urls)
                        {
                            // 为了多次生产新的对象
                            newBean = new WebConfig();
                            newBean.setCompanyId(bean.getCompanyId());
                            newBean.setSiteId(bean.getSiteId());
                            newBean.setCopyright(bean.getCopyright());
                            newBean.setErrorPagePath(bean.getErrorPagePath());
                            newBean.setLogoUrl(bean.getLogoUrl());
                            newBean.setMetaDescription(bean
                                    .getMetaDescription());
                            newBean.setMetaKeywords(bean.getMetaKeywords());
                            newBean.setSiteName(bean.getSiteName());
                            newBean.setCompanyIntr(bean.getCompanyIntr());
                            newBean.setSiteUrl(s.trim());
                            cache.put(s.trim(), newBean);
                        }
                    }
                    else
                    {
                        cache.put(siteUrl.trim(), bean);
                    }
                }
            }
        }
        catch (PlatException e)
        {
            log.error("Failure data cache WebConfig.", e);
        }

        log.info("Cache ->> " + CMSCacheFileds.CacheName.WEBCONFIGCACHE
                + " had Loaded.");
    }

    public void setWebConfigDao(ReceptionWebConfigDao webConfigDao)
    {
        this.webConfigDao = webConfigDao;
    }
}
