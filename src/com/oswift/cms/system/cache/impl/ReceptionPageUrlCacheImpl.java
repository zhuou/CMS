package com.oswift.cms.system.cache.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oswift.cms.dao.ReceptionPageUrlDao;
import com.oswift.cms.entity.ReceptionPageUrlBean;
import com.oswift.cms.entity.reception.ReceptionPageUrl;
import com.oswift.cms.system.cache.CMSCacheFileds;
import com.oswift.utils.cache.AbstractBusinessCache;
import com.oswift.utils.cache.CacheAccessException;
import com.oswift.utils.cache.operate.CacheMap;
import com.oswift.utils.exception.PlatException;

public class ReceptionPageUrlCacheImpl extends AbstractBusinessCache
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(ReceptionPageUrlCacheImpl.class);

    /**
     * 前台页面功能操作类
     */
    private ReceptionPageUrlDao pageUrlDao;

    @Override
    public void init() throws CacheAccessException
    {
        try
        {
            List<ReceptionPageUrlBean> list = pageUrlDao
                    .getAllReceptionPageUrl();
            CacheMap cache = this.getCacheMap();
            // 先清除缓存
            cache.clear();

            for (ReceptionPageUrlBean bean : list)
            {
                StringBuilder sb = new StringBuilder();
                sb.append(bean.getSiteId());
                sb.append('|');
                sb.append(bean.getUrlType());
                sb.append('|');
                sb.append(bean.getNickName());

                ReceptionPageUrl rBean = new ReceptionPageUrl();
                rBean.setSiteId(bean.getSiteId());
                rBean.setPageUrlId(bean.getPageUrlId());
                rBean.setUrlType(bean.getUrlType());
                rBean.setJspPath(bean.getJspPath());
                rBean.setNickName(bean.getNickName());

                cache.put(sb.toString(), rBean);
            }
        }
        catch (PlatException e)
        {
            log.error("Failure data cache PageURL's Function.", e);
        }

        log.info("Cache ->> " + CMSCacheFileds.CacheName.RECEPTIONPAGEURLCACHE
                + " had Loaded.");
    }

    public void setPageUrlDao(ReceptionPageUrlDao pageUrlDao)
    {
        this.pageUrlDao = pageUrlDao;
    }
}
