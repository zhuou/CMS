package com.oswift.cms.system.cache;

/**
 *
 * 定义缓存常量名称
 *
 * @author zhuou
 * @version C03 Oct 27, 2012
 * @since OSwift GPM V1.0
 */
public interface CMSCacheFileds
{

    /**
     *
     * 缓存名称常量
     *
     * @author zhuou
     * @version C03 Oct 27, 2012
     * @since OSwift GPM V1.0
     */
    interface CacheName
    {
        /**
         * WEB端系统参数缓存
         */
        String SYSTEMCONFIGCACHE = "systemConfigCache";

        /**
         * 缓存字典数据
         */
        String DICTIONARYCACHE = "dictionaryCache";

        /**
         * 前台页面URL功能缓存
         */
        String RECEPTIONPAGEURLCACHE = "receptionPageUrlCache";

        /**
         * web站点信息缓存
         */
        String WEBCONFIGCACHE = "webConfigCache";
    }

    /**
     *
     * 缓存对应业务变量常量名称
     * @author zhuou
     * @version C03 2013-3-16
     * @since OSwift GPM V1.0
     */
    interface BusinessField
    {
        /**
         * 文章频道信息
         */
        String DEFAULT_ARTICLE_KEY = "default_article";

        /**
         * 公告频道信息
         */
        String DEFAULT_ANNOUNCE_KEY = "default_announce";

        /**
         * 下载频道信息
         */
        String DEFAULT_DOWNLOAD_KEY = "default_download";

        /**
         * 留言频道信息
         */
        String DEFAULT_GUESTBOOK_KEY = "default_guestbook";

        /**
         * 图片频道信息
         */
        String DEFAULT_PICURL_KEY = "default_picurl";

        /**
         * 友情链接频道信息
         */
        String DEFAULT_FRIENDSITE_KEY = "default_friendsite";

        /**
         * 站点前台统一错误页面路径
         */
        String ERRORPAGEPATH = "ErrorPagePath";
    }
}
