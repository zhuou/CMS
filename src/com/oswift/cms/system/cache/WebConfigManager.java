package com.oswift.cms.system.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oswift.cms.entity.reception.WebConfig;
import com.oswift.utils.cache.BusinessCache;
import com.oswift.utils.cache.CacheAccessException;
import com.oswift.utils.cache.manager.CacheManager;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.PlatException;

public class WebConfigManager
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(WebConfigManager.class);

    /**
     * web站点信息缓存实例
     */
    private static BusinessCache webConfigCacheInstance = CacheManager
            .getBusinessCache(CMSCacheFileds.CacheName.WEBCONFIGCACHE);

    /**
     *
     * 根据key获取WebConfig
     *
     * @author zhuou
     * @param key
     *            key值
     * @return WebConfig
     */
    public static WebConfig getValue(String key)
    {
        try
        {
            WebConfig bean = (WebConfig) webConfigCacheInstance.get(key);
            return bean;
        }
        catch (CacheAccessException e)
        {
            log.error("Fail to get WebConfig info, key=" + key);
        }
        return null;
    }

    /**
     *
     * 向指定缓存中增加一条数据，若键已存在则覆盖原来的值
     *
     * @author zhuou
     * @param bean
     *            WebConfig
     * @throws PlatException
     *             公共异常
     */
    public static void setValue(WebConfig bean) throws PlatException
    {
        try
        {
            if (!StringUtil.isEmpty(bean.getSiteUrl()))
            {
                webConfigCacheInstance.set(bean.getSiteUrl(), bean);
            }
        }
        catch (CacheAccessException e)
        {
            log.error("Fail to set WebConfig info, bean=" + bean);
            throw new PlatException("Fail to set WebConfig's info.");
        }
    }

    /**
     *
     * 根据key删除一条缓存记录
     *
     * @author zhuou
     * @param key
     *            key
     * @throws PlatException
     *             公共异常
     */
    public static void remove(String key) throws PlatException
    {
        try
        {
            if (!StringUtil.isEmpty(key))
            {
                webConfigCacheInstance.remove(key);
            }
        }
        catch (CacheAccessException e)
        {
            log.error("Fail to remove WebConfig info, key=" + key);
            throw new PlatException("Fail to remove WebConfig's info.");
        }
    }
}
