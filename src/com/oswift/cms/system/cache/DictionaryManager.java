package com.oswift.cms.system.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oswift.cms.entity.DictionaryBean;
import com.oswift.utils.cache.BusinessCache;
import com.oswift.utils.cache.CacheAccessException;
import com.oswift.utils.cache.manager.CacheManager;
import com.oswift.utils.common.StringUtil;

/**
 *
 * 操作获取字典缓存类
 *
 * @author zhuou
 * @version C03 2012-12-22
 * @since OSwift GPM V1.0
 */
public class DictionaryManager
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(DictionaryManager.class);

    /**
     * 记录系统参数缓存实例
     */
    private static BusinessCache dictionaryCacheInstance = CacheManager
            .getBusinessCache(CMSCacheFileds.CacheName.DICTIONARYCACHE);

    /**
     *
     * 根据key获取字典value
     *
     * @author zhuou
     * @param key
     * @return 系统参数value
     */
    public static DictionaryBean getValue(String key)
    {
        if (StringUtil.isEmpty(key))
        {
            return null;
        }
        try
        {
            DictionaryBean value = (DictionaryBean) dictionaryCacheInstance
                    .get(key);
            return value;
        }
        catch (CacheAccessException e)
        {
            log.error("Fail to get Dictionary Config, key=" + key);
        }
        return null;
    }
}
