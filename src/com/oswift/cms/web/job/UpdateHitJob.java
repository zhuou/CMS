package com.oswift.cms.web.job;

import java.util.Date;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oswift.cms.service.ICommonModelService;
import com.oswift.utils.exception.PlatException;

public class UpdateHitJob
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(UpdateHitJob.class);

    /**
     * 公共内容数据
     */
    @Resource
    private ICommonModelService commonModelService;

    /**
     *
     * 更新天点击率
     *
     * @author zhuou
     */
    public void updateDayHit()
    {
        try
        {
            int rows = commonModelService.updateDayHit();
            log.info("updateDayHit:更新了记录条数：" + rows);
        }
        catch (PlatException e)
        {
            log.error("The job of updateDayHit is error.", e);
        }
        System.out.println("day=" + new Date());
    }

    /**
     *
     * 更新周点击率
     *
     * @author zhuou
     */
    public void updateWeekHit()
    {
        try
        {
            int rows = commonModelService.updateWeekHit();
            log.info("updateWeekHit:更新了记录条数：" + rows);
        }
        catch (PlatException e)
        {
            log.error("The job of updateWeekHit is error.", e);
        }
        System.out.println("week=" + new Date());
    }

    /**
     *
     * 更新月点击率
     *
     * @author zhuou
     */
    public void updateMonthHit()
    {
        try
        {
            int rows = commonModelService.updateMonthHit();
            log.info("updateMonthHit:更新了记录条数：" + rows);
        }
        catch (PlatException e)
        {
            log.error("The job of updateMonthHit is error.", e);
        }
        System.out.println("month=" + new Date());
    }

    public void setCommonModelService(ICommonModelService commonModelService)
    {
        this.commonModelService = commonModelService;
    }
}
