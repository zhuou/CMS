package com.oswift.cms.web.reception;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.entity.reception.ReceptionPageUrl;
import com.oswift.cms.entity.reception.WebConfig;
import com.oswift.cms.utils.ReceptionConstant;
import com.oswift.cms.utils.ReceptionConstant.ReceptionPageUrLState;
import com.oswift.utils.common.RegexUtils;
import com.oswift.utils.common.StringUtil;

@Controller
public class ReceptionAction extends BaseAction
{
    /**
     *
     * url：/页面别名/list.html 例如：/article/list.html
     *
     * @author zhuou
     */
    @RequestMapping(value = "/{pageName}/list", method = RequestMethod.GET)
    public ModelAndView list(HttpServletRequest request,
            @PathVariable(value = "pageName")
            String pageName)
    {
        return getList(request, pageName, ReceptionConstant.DEFAULT_PAGENUM);
    }

    /**
     *
     * url：/页面别名/list_第几页.html 例如：/article/list_2.html
     *
     * @author zhuou
     */
    @RequestMapping(value = "/{pageName}/list_{nowPage}", method = RequestMethod.GET)
    public ModelAndView list_1(HttpServletRequest request,
            @PathVariable(value = "pageName")
            String pageName, @PathVariable(value = "nowPage")
            String nowPage)
    {
        int iNowPage = ReceptionConstant.DEFAULT_PAGENUM;
        // 如果不是数字，则证明是别名，根据别名查询出id
        if (RegexUtils.isNumber(nowPage))
        {
            iNowPage = Integer.valueOf(nowPage);
        }
        return getList(request, pageName, iNowPage);
    }

    /**
     *
     * url：/页面别名/type_类型_list.html,例如：/article/type_new_list.html
     *
     * @author zhuou
     */
    @RequestMapping(value = "/{pageName}/type_{contentType}_list", method = RequestMethod.GET)
    public ModelAndView typeList(HttpServletRequest request,
            @PathVariable(value = "pageName")
            String pageName, @PathVariable(value = "contentType")
            String contentType)
    {
        return getTypeList(request, pageName, contentType,
                ReceptionConstant.DEFAULT_PAGENUM);
    }

    /**
     *
     * url：/页面别名/type_类型_list_第几页.html,例如：/article/type_new_list_1.html
     *
     * @author zhuou
     */
    @RequestMapping(value = "/{pageName}/type_{contentType}_list_{nowPage}", method = RequestMethod.GET)
    public ModelAndView typeList_1(HttpServletRequest request,
            @PathVariable(value = "pageName")
            String pageName, @PathVariable(value = "contentType")
            String contentType, @PathVariable(value = "nowPage")
            String nowPage)
    {
        int iNowPage = ReceptionConstant.DEFAULT_PAGENUM;
        // 如果不是数字，则证明是别名，根据别名查询出id
        if (RegexUtils.isNumber(nowPage))
        {
            iNowPage = Integer.valueOf(nowPage);
        }
        return getTypeList(request, pageName, contentType, iNowPage);
    }

    /**
     *
     * url:/页面别名/栏目别名/list.html,例如：/pic/girl/list.html
     *
     * @author zhuou
     */
    @RequestMapping(value = "/{pageName}/{channel}/list", method = RequestMethod.GET)
    public ModelAndView channelList(HttpServletRequest request,
            @PathVariable(value = "pageName")
            String pageName, @PathVariable(value = "channel")
            String channel)
    {
        return getChannelList(request, pageName, channel,
                ReceptionConstant.DEFAULT_PAGENUM);
    }

    /**
     *
     * url:/页面别名/栏目别名/list_第几页.html,例如：/pic/girl/list_1.html
     *
     * @author zhuou
     */
    @RequestMapping(value = "/{pageName}/{channel}/list_{nowPage}", method = RequestMethod.GET)
    public ModelAndView channelList_1(HttpServletRequest request,
            @PathVariable(value = "pageName")
            String pageName, @PathVariable(value = "channel")
            String channel, @PathVariable(value = "nowPage")
            String nowPage)
    {
        int iNowPage = ReceptionConstant.DEFAULT_PAGENUM;
        // 如果不是数字，则证明是别名，根据别名查询出id
        if (RegexUtils.isNumber(nowPage))
        {
            iNowPage = Integer.valueOf(nowPage);
        }
        return getChannelList(request, pageName, channel, iNowPage);
    }

    /**
     *
     * url:/页面别名/栏目别名/type_类型_list.html,例如：/pic/girl/type_new_list.html
     *
     * @author zhuou
     */
    @RequestMapping(value = "/{pageName}/{channel}/type_{contentType}_list", method = RequestMethod.GET)
    public ModelAndView channelTypeList(HttpServletRequest request,
            @PathVariable(value = "pageName")
            String pageName, @PathVariable(value = "channel")
            String channel, @PathVariable(value = "contentType")
            String contentType)
    {
        return getChannelTypeList(request, pageName, channel, contentType,
                ReceptionConstant.DEFAULT_PAGENUM);
    }

    /**
     *
     * url:/页面别名/栏目别名/type_类型_list_第几页.html,例如：/pic/girl/type_new_list_1.html
     *
     * @author zhuou
     */
    @RequestMapping(value = "/{pageName}/{channel}/type_{contentType}_list_{nowPage}", method = RequestMethod.GET)
    public ModelAndView channelTypeList_1(HttpServletRequest request,
            @PathVariable(value = "pageName")
            String pageName, @PathVariable(value = "channel")
            String channel, @PathVariable(value = "contentType")
            String contentType, @PathVariable(value = "nowPage")
            String nowPage)
    {
        int iNowPage = ReceptionConstant.DEFAULT_PAGENUM;
        // 如果不是数字，则证明是别名，根据别名查询出id
        if (RegexUtils.isNumber(nowPage))
        {
            iNowPage = Integer.valueOf(nowPage);
        }
        return getChannelTypeList(request, pageName, channel, contentType,
                iNowPage);
    }

    /**
     *
     * url:/article/100/detail.html
     *
     * @author zhuou
     */
    @RequestMapping(value = "/{pageName}/{contentId}/detail", method = RequestMethod.GET)
    public ModelAndView detail(HttpServletRequest request,
            @PathVariable(value = "pageName")
            String pageName, @PathVariable(value = "contentId")
            String contentId)
    {
        return getDetail(request, pageName, contentId);
    }

    /**
     *
     * url:/article/detail/100.html
     *
     * @author zhuou
     */
    @RequestMapping(value = "/{pageName}/detail/{contentId}", method = RequestMethod.GET)
    public ModelAndView detail_1(HttpServletRequest request,
            @PathVariable(value = "pageName")
            String pageName, @PathVariable(value = "contentId")
            String contentId)
    {
        return getDetail(request, pageName, contentId);
    }

    /**
     *
     * 用户自定义前台页面URL
     *
     * @author 13100267
     */
    @RequestMapping(value = "/{pageName}", method = RequestMethod.GET)
    public ModelAndView initPage(HttpServletRequest request,
            @PathVariable(value = "pageName")
            String pageName)
    {
        ModelAndView mv = null;
        WebConfig webConfig = this.getWebConfig(request);
        int siteId = webConfig.getSiteId();

        // 判断页面别名
        if (StringUtil.isBlank(pageName))
        {
            mv = new ModelAndView(getErrorPage(webConfig));
            return mv;
        }

        ReceptionPageUrl bean = this.getReceptionPageUrlFun(siteId,
                ReceptionPageUrLState.USER_DEFINED, pageName.trim());
        // 跳转到错误页面
        if (null == bean)
        {
            mv = new ModelAndView(getErrorPage(webConfig));
            return mv;
        }

        mv = new ModelAndView(pagePath(bean.getJspPath(), siteId));
        mv.addObject("webConfig", webConfig);
        mv.addObject("webPath", webPath(request, siteId));
        mv.addObject("contextPath", request.getContextPath());
        mv.addObject("request", request);
        mv.addObject("pageName", pageName);

        // 将url参数作为值传给前台页面
        Map<?, ?> map = request.getParameterMap();
        for (Map.Entry<?, ?> entry : map.entrySet())
        {
            String key = String.valueOf(entry.getKey());
            if (null != key && !StringUtil.isBlank(key))
            {
                String[] value = (String[]) entry.getValue();
                if (null != value && value.length > 0)
                {
                    mv.addObject(key, value[0]);
                }
            }
        }
        return mv;
    }

    /**
     *
     * 404页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/404")
    public ModelAndView web404(HttpServletRequest request)
    {
        // 获取错误页面
        String errorPage = ReceptionConstant.errorPagePath;
        WebConfig webConfig = this.getWebConfig(request);

        // 问题：可能带来循环跳转
        if (!StringUtil.isBlank(webConfig.getErrorPagePath()))
        {
            errorPage = pagePath(webConfig.getErrorPagePath(), webConfig
                    .getSiteId());
        }
        ModelAndView mv = new ModelAndView(errorPage);
        return mv;
    }
}