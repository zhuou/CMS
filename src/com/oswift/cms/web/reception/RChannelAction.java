package com.oswift.cms.web.reception;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.entity.reception.Channel;
import com.oswift.cms.entity.reception.ReceptionPageUrl;
import com.oswift.cms.entity.reception.WebConfig;
import com.oswift.cms.utils.ReceptionConstant;
import com.oswift.cms.utils.ReceptionConstant.ReceptionPageUrLState;
import com.oswift.utils.common.RegexUtils;
import com.oswift.utils.common.StringUtil;

@Controller
public class RChannelAction extends BaseAction
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(RChannelAction.class);

    /**
     *
     * 跳转到首页
     *
     * @author zhuou
     */
    @RequestMapping(value = "/")
    public String goIndex(HttpServletRequest request)
    {
        return "forward:/index.html";
    }

    /**
     *
     * 站点前台页面首页
     *
     * @author zhuou
     */
    @RequestMapping(value = "/index")
    public ModelAndView index(HttpServletRequest request)
    {
        ModelAndView mv = null;
        WebConfig webConfig = this.getWebConfig(request);
        int siteId = webConfig.getSiteId();
        int companyId = webConfig.getCompanyId();
        ReceptionPageUrl bean = this.getReceptionPageUrlFun(siteId,
                ReceptionPageUrLState.INDEX, ReceptionConstant.INDEX_NAME);

        // 跳转到错误页面
        if (null == bean)
        {
            mv = new ModelAndView(getErrorPage(webConfig));
            return mv;
        }
        try
        {
            // 如果首页有分页
            int iNowPage = ReceptionConstant.DEFAULT_PAGENUM;
            String nowPage = request.getParameter("p");
            if (!StringUtil.isBlank(nowPage))
            {
                if (RegexUtils.isNumber(nowPage))
                {
                    iNowPage = Integer.valueOf(nowPage);
                }
            }

            mv = new ModelAndView(pagePath(bean.getJspPath(), siteId));
            mv.addObject("nowPage", iNowPage);
            mv.addObject("webConfig", webConfig);
            mv.addObject("webPath", webPath(request, siteId));
            mv.addObject("contextPath", request.getContextPath());
            mv.addObject("request", request);
        }
        catch (Exception e)
        {
            log.error("The 'index' of front page is exception，companyId="
                    + companyId, e);
            mv = new ModelAndView(getErrorPage(webConfig));
        }
        return mv;
    }

    /**
     *
     * 站点前台每个栏目的首页
     * (如：/channel/栏目别名.html或/栏目别名/index.html,例如：/channel/about-us.html或/about-us/index.html)
     * (如：/channel/栏目ID.html或/ID/index.html,例如：/channel/1002.html或/1002/index.html)
     *
     * @author zhuou
     */
    @RequestMapping(value = "/{channel}/index", method = RequestMethod.GET)
    public ModelAndView channelIndex(HttpServletRequest request,
            @PathVariable(value = "channel")
            String channel)
    {
        ModelAndView mv = null;
        WebConfig webConfig = this.getWebConfig(request);
        int companyId = webConfig.getCompanyId();
        int siteId = webConfig.getSiteId();

        // 如果没有获取到值跳转到错误页面
        if (StringUtil.isEmpty(channel))
        {
            mv = new ModelAndView(getErrorPage(webConfig));
            return mv;
        }

        try
        {
            Channel currentChannel = null;
            // 如果不是数字，则证明是别名，根据别名查询出id
            if (!RegexUtils.isNumber(channel))
            {
                currentChannel = receptionService.getChannelByNickName(
                        companyId, channel);
            }
            else
            {
                currentChannel = receptionService.getChannelById(Integer
                        .valueOf(channel));
            }
            // 证明没有此栏目
            if (null == currentChannel)
            {
                mv = new ModelAndView(getErrorPage(webConfig));
                return mv;
            }

            // 栏目页面别名为系统设置（如：channel_2014）
            ReceptionPageUrl bean = this
                    .getReceptionPageUrlFun(siteId,
                            ReceptionPageUrLState.CHANNEL, currentChannel
                                    .getNickName());

            // 跳转到错误页面
            if (null == bean)
            {
                mv = new ModelAndView(getErrorPage(webConfig));
                return mv;
            }

            // 如果首页有分页
            int iNowPage = ReceptionConstant.DEFAULT_PAGENUM;
            String nowPage = request.getParameter("p");
            if (!StringUtil.isBlank(nowPage))
            {
                if (RegexUtils.isNumber(nowPage))
                {
                    iNowPage = Integer.valueOf(nowPage);
                }
            }

            mv = new ModelAndView(pagePath(bean.getJspPath(), siteId));
            mv.addObject("currentChannel", currentChannel);
            mv.addObject("webConfig", webConfig);
            mv.addObject("webPath", webPath(request, siteId));
            mv.addObject("contextPath", request.getContextPath());
            mv.addObject("nowPage", iNowPage);
            mv.addObject("request", request);
        }
        catch (Exception e)
        {
            log.error(
                    "The 'index' of front channel's page is exception，companyId="
                            + companyId, e);
            mv = new ModelAndView(getErrorPage(webConfig));
        }
        return mv;
    }

    /**
     *
     * 站点前台每个栏目的首页(如：/channel/栏目别名.html或/栏目别名/index.html,例如：/channel/about-us.html或/about-us/index.html)
     *
     * @author zhuou
     */
    @RequestMapping(value = "/channel/{channel}", method = RequestMethod.GET)
    public ModelAndView channelIndex_1(HttpServletRequest request,
            @PathVariable(value = "channel")
            String channel)
    {
        ModelAndView mv = null;
        WebConfig webConfig = this.getWebConfig(request);
        int companyId = webConfig.getCompanyId();
        int siteId = webConfig.getSiteId();

        // 如果没有获取到值跳转到错误页面
        if (StringUtil.isEmpty(channel))
        {
            mv = new ModelAndView(getErrorPage(webConfig));
            return mv;
        }

        try
        {
            Channel currentChannel = null;
            // 如果不是数字，则证明是别名，根据别名查询出id
            if (!RegexUtils.isNumber(channel))
            {
                currentChannel = receptionService.getChannelByNickName(
                        companyId, channel);
            }
            else
            {
                currentChannel = receptionService.getChannelById(Integer
                        .valueOf(channel));
            }
            // 证明没有此栏目
            if (null == currentChannel)
            {
                mv = new ModelAndView(getErrorPage(webConfig));
                return mv;
            }

            // 栏目页面别名为系统设置（如：channel_2014）
            ReceptionPageUrl bean = this
                    .getReceptionPageUrlFun(siteId,
                            ReceptionPageUrLState.CHANNEL, currentChannel
                                    .getNickName());

            // 跳转到错误页面
            if (null == bean)
            {
                mv = new ModelAndView(getErrorPage(webConfig));
                return mv;
            }

            // 如果首页有分页
            int iNowPage = ReceptionConstant.DEFAULT_PAGENUM;
            String nowPage = request.getParameter("p");
            if (!StringUtil.isBlank(nowPage))
            {
                if (RegexUtils.isNumber(nowPage))
                {
                    iNowPage = Integer.valueOf(nowPage);
                }
            }

            mv = new ModelAndView(pagePath(bean.getJspPath(), siteId));
            mv.addObject("currentChannel", currentChannel);
            mv.addObject("webConfig", webConfig);
            mv.addObject("webPath", webPath(request, siteId));
            mv.addObject("contextPath", request.getContextPath());
            mv.addObject("nowPage", iNowPage);
            mv.addObject("request", request);
        }
        catch (Exception e)
        {
            log.error(
                    "The 'index' of front channel's page is exception，companyId="
                            + companyId, e);
            mv = new ModelAndView(getErrorPage(webConfig));
        }
        return mv;
    }
}
