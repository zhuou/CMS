package com.oswift.cms.web.reception;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.WebApplicationContext;

import com.oswift.cms.entity.reception.AdZone;
import com.oswift.cms.entity.reception.Announce;
import com.oswift.cms.entity.reception.Article;
import com.oswift.cms.entity.reception.Download;
import com.oswift.cms.entity.reception.FriendSite;
import com.oswift.cms.entity.reception.GuestBook;
import com.oswift.cms.entity.reception.Member;
import com.oswift.cms.entity.reception.MemberLoginInfo;
import com.oswift.cms.entity.reception.Pic;
import com.oswift.cms.service.IReceptionService;
import com.oswift.cms.service.impl.ReceptionServiceImpl;
import com.oswift.cms.utils.Field;
import com.oswift.cms.web.servlet.SpringAppContext;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.PlatException;

/**
 *
 * 前台数据自定义jstl标签
 *
 * @author zhuou
 * @version C03 2014-6-11
 * @since OSwift CMS V2.0
 */
public class JSTLFunctions
{
    /**
     * 通过spring WebAppContext获取ReceptionServiceImpl对象
     */
    private static IReceptionService receptionService;

    static
    {
        // 获取spring注入对象
        WebApplicationContext WebAppContext = SpringAppContext
                .getWebAppContext();
        receptionService = WebAppContext.getBean("receptionService",
                ReceptionServiceImpl.class);
    }

    /**
     *
     *
     * 获取文章列表数据
     *
     * @author zhuou
     * @param companyId
     *            表示公司Id，可以从后台获取
     * @param channelId
     *            栏目Id 如果为-1则获取所有栏目文章列表
     * @param contentType
     *            内容类型
     * @param pageNum
     *            获取记录数，如果有分页，则表示每页记录数
     * @param nowPage
     *            当为-1时表示不分页，大于-1表示分页的第几页
     * @return Object
     * @throws PlatException
     */
    public static Object articleList(int companyId, int channelId,
            String contentType, int pageNum, int nowPage) throws PlatException
    {
        return receptionService.contentList(companyId, channelId, contentType,
                pageNum, nowPage, Field.TableName.ARTICLE);
    }

    /**
     *
     *
     * 获取公告列表数据
     *
     * @author zhuou
     * @param companyId
     *            表示公司Id，可以从后台获取
     * @param channelId
     *            栏目Id 如果为-1则获取所有栏目文章列表
     * @param contentType
     *            内容类型
     * @param pageNum
     *            获取记录数，如果有分页，则表示每页记录数
     * @param nowPage
     *            当为-1时表示不分页，大于-1表示分页的第几页
     * @return Object
     * @throws PlatException
     */
    public static Object announceList(int companyId, int channelId,
            String contentType, int pageNum, int nowPage) throws PlatException
    {
        return receptionService.contentList(companyId, channelId, contentType,
                pageNum, nowPage, Field.TableName.ANNOUNCE);
    }

    /**
     *
     *
     * 获取下载列表数据
     *
     * @author zhuou
     * @param companyId
     *            表示公司Id，可以从后台获取
     * @param channelId
     *            栏目Id 如果为-1则获取所有栏目文章列表
     * @param contentType
     *            内容类型
     * @param pageNum
     *            获取记录数，如果有分页，则表示每页记录数
     * @param nowPage
     *            当为-1时表示不分页，大于-1表示分页的第几页
     * @return Object
     * @throws PlatException
     */
    public static Object downloadList(int companyId, int channelId,
            String contentType, int pageNum, int nowPage) throws PlatException
    {
        return receptionService.contentList(companyId, channelId, contentType,
                pageNum, nowPage, Field.TableName.DOWNLOAD);
    }

    /**
     *
     *
     * 获取留言列表数据
     *
     * @author zhuou
     * @param companyId
     *            表示公司Id，可以从后台获取
     * @param channelId
     *            栏目Id 如果为-1则获取所有栏目文章列表
     * @param contentType
     *            内容类型
     * @param pageNum
     *            获取记录数，如果有分页，则表示每页记录数
     * @param nowPage
     *            当为-1时表示不分页，大于-1表示分页的第几页
     * @return Object
     * @throws PlatException
     */
    public static Object guestBookList(int companyId, int channelId,
            String contentType, int pageNum, int nowPage) throws PlatException
    {
        return receptionService.contentList(companyId, channelId, contentType,
                pageNum, nowPage, Field.TableName.GUESTBOOK);
    }

    /**
     *
     *
     * 获取图片列表数据
     *
     * @author zhuou
     * @param companyId
     *            表示公司Id，可以从后台获取
     * @param channelId
     *            栏目Id 如果为-1则获取所有栏目文章列表
     * @param contentType
     *            内容类型
     * @param pageNum
     *            获取记录数，如果有分页，则表示每页记录数
     * @param nowPage
     *            当为-1时表示不分页，大于-1表示分页的第几页
     * @return Object
     * @throws PlatException
     */
    public static Object picList(int companyId, int channelId,
            String contentType, int pageNum, int nowPage) throws PlatException
    {
        return receptionService.contentList(companyId, channelId, contentType,
                pageNum, nowPage, Field.TableName.PICURL);
    }

    /**
     *
     *
     * 获取友情链接列表数据
     *
     * @author zhuou
     * @param companyId
     *            表示公司Id，可以从后台获取
     * @param channelId
     *            栏目Id 如果为-1则获取所有栏目文章列表
     * @param contentType
     *            内容类型
     * @param pageNum
     *            获取记录数，如果有分页，则表示每页记录数
     * @param nowPage
     *            当为-1时表示不分页，大于-1表示分页的第几页
     * @return Object
     * @throws PlatException
     */
    public static Object friendSiteList(int companyId, int channelId,
            String contentType, int pageNum, int nowPage) throws PlatException
    {
        return receptionService.contentList(companyId, channelId, contentType,
                pageNum, nowPage, Field.TableName.FRIENDSITE);
    }

    /**
     *
     * 获取广告列表
     *
     * @author zhuou
     * @param zoneId
     *            广告位Id
     * @param companyId
     *            表示公司Id，可以从后台获取
     * @param topNum
     *            获取记录数
     * @return Object
     */
    public static AdZone adList(int zoneId, int companyId, int topNum)
    {
        return receptionService.getAdvertisement(zoneId, companyId, topNum);
    }

    /**
     *
     * 获取广告列表
     *
     * @author zhuou
     * @param zoneCode
     *            广告位编码
     * @param companyId
     *            表示公司Id，可以从后台获取
     * @param topNum
     *            获取记录数
     * @return Object
     */
    public static AdZone adList(String zoneCode, int companyId, int topNum)
    {
        return receptionService.getAdvertisement(zoneCode, companyId, topNum);
    }

    /**
     *
     * 获取评论列表信息
     *
     * @author zhuou
     * @param companyId
     *            单位Id
     * @param commonModelId
     *            内容id 如果为-1则表示从所有评论中获取，否则获取内容ID的评论信息
     * @param contentType
     *            内容类型
     * @param pageNum
     *            获取记录数，如果有分页，则表示每页记录数
     * @param nowPage
     *            当为-1时表示不分页，大于-1表示分页的第几页
     *
     * @return Object
     * @throws PlatException
     *             公共异常
     */
    public static Object commentList(int companyId, long commonModelId,
            String contentType, int pageNum, int nowPage) throws PlatException
    {
        return receptionService.getCommentList(companyId, commonModelId,
                contentType, pageNum, nowPage);
    }

    /**
     *
     * 截取字符串
     *
     * @author zhuou
     * @param str
     *            目标字符串
     * @param length
     *            要截取的长度
     * @param endStr
     *            后缀字符串
     * @return String
     */
    public static String substring(String str, int length, String endStr)
    {
        if (StringUtil.isBlank(str))
        {
            return "";
        }
        length = length * 2;
        int currentLength = 0;
        StringBuilder sb = new StringBuilder();
        char[] charArray = str.toCharArray();
        try
        {
            for (char c : charArray)
            {
                currentLength += String.valueOf(c).getBytes("GBK").length;
                if (currentLength <= length)
                {
                    sb.append(c);
                }
                else
                {
                    sb.append(endStr);
                    break;
                }
            }
        }
        catch (UnsupportedEncodingException e)
        {
            return str;
        }
        return sb.toString();
    }

    /**
     *
     * 获取文章详情
     *
     * @author zhuou
     * @param commonModelId
     *            内容id
     * @param companyId
     *            前台页面传过来的公司Id，为了防止多站点情况下通过修改url的id获取到其他网站的内容
     * @param isRelation
     *            是否显示相关上一条下一条记录
     * @param relationFlag
     *            当isRelation为true，此字段起作用。 当relationFlag=1时获取的是本栏目下的相关上一条下一条记录，
     *            当relationFlag=2时获取文章板块下上一条下一条记录，
     *            当relationFlag=3时获取本栏目及其子栏目下的文章上一条下一条记录
     * @return Article
     * @throws PlatException
     *             公共异常
     */
    public static Article article(int commonModelId, int companyId,
            boolean isRelation, int relationFlag) throws PlatException
    {
        return receptionService.getArticleDetail(commonModelId, companyId,
                isRelation, relationFlag);
    }

    /**
     *
     * 获取下载详情
     *
     * @author zhuou
     * @param commonModelId
     *            内容id
     * @param isRelation
     *            是否显示相关上一条下一条记录
     * @return Download
     * @throws PlatException
     *             公共异常
     */
    public static Download download(int commonModelId, boolean isRelation)
        throws PlatException
    {
        return receptionService.getDownloadDetail(commonModelId, isRelation);
    }

    /**
     *
     * 获取留言详情
     *
     * @author zhuou
     * @param commonModelId
     *            内容id
     * @param isRelation
     *            是否显示相关上一条下一条记录
     * @return GuestBook
     * @throws PlatException
     *             公共异常
     */
    public static GuestBook guestBook(int commonModelId, boolean isRelation)
        throws PlatException
    {
        return receptionService.getGuestBookDetail(commonModelId, isRelation);
    }

    /**
     *
     * 获取图片详情
     *
     * @author zhuou
     * @param commonModelId
     *            内容id
     * @param isRelation
     *            是否显示相关上一条下一条记录
     * @return Pic
     * @throws PlatException
     *             公共异常
     */
    public static Pic pic(int commonModelId, boolean isRelation)
        throws PlatException
    {
        return receptionService.getPicDetail(commonModelId, isRelation);
    }

    /**
     *
     * 获取公告详情
     *
     * @author zhuou
     * @param commonModelId
     *            内容id
     * @param isRelation
     *            是否显示相关上一条下一条记录
     * @return Announce
     * @throws PlatException
     *             公共异常
     */
    public static Announce announce(int commonModelId, boolean isRelation)
        throws PlatException
    {
        return receptionService.getAnnounceDetail(commonModelId, isRelation);
    }

    /**
     *
     * 获取友情链接详情
     *
     * @author zhuou
     * @param commonModelId
     *            内容id
     * @return FriendSite
     * @throws PlatException
     *             公共异常
     */
    public static FriendSite friendSite(int commonModelId) throws PlatException
    {
        return receptionService.getFriendSiteDetail(commonModelId, false);
    }

    /**
     *
     * 查询栏目，查询类型
     * all查询公司下全部栏目，one-level查询全部一级栏目，fixed-all查询指定栏目及父栏目和其所有子栏目，fixed-one查询指定栏目
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @param channelId
     *            栏目Id
     * @param contentType
     *            查询类型
     * @return all和one-level 返回list类型，fixed-all和fixed-one返回Channel类型
     * @throws PlatException
     *             公共异常
     */
    public static Object channel(int companyId, int channelId,
            String contentType) throws PlatException
    {
        return receptionService.getChannel(companyId, channelId, contentType);
    }

    /**
     *
     * 格式化时间
     *
     * @author zhuou
     * @param date
     *            时间
     * @param formatString
     *            格式
     * @return String
     */
    public static String timeFormat(Date date, String formatString)
    {
        if (null == date)
        {
            return "";
        }
        DateFormat dateFormat = new SimpleDateFormat(formatString);
        return dateFormat.format(date);
    }

    /**
     *
     * 截取字符串
     *
     * @author zhuou
     * @param targetStr
     *            目标字符串
     * @param cutStr
     *            分隔符
     * @param index
     *            取第几个
     * @return String
     */
    public static String split(String targetStr, String cutStr, int index)
    {
        if (StringUtil.isBlank(targetStr))
        {
            return "";
        }
        String[] strs = targetStr.split(cutStr);
        if (index + 1 > strs.length)
        {
            return "";
        }
        return strs[index];
    }

    /**
     *
     * 判断用户是否登录
     *
     * @author zhuou
     * @param request
     *            HttpServletRequest
     * @return MemberLoginInfo
     */
    public static MemberLoginInfo isLogin(HttpServletRequest request)
    {
        // 用户登录信息
        MemberLoginInfo memberLoginInfo = new MemberLoginInfo();

        HttpSession session = request.getSession();
        if (null == session
                || null == session.getAttribute(Field.MEMBER_SESSION))
        {
            return memberLoginInfo;
        }

        // 获取放在session里的用户信息
        Member member = (Member) session.getAttribute(Field.MEMBER_SESSION);
        memberLoginInfo.setLoginFlag(true);
        memberLoginInfo.setMember(member);

        return memberLoginInfo;
    }

    /**
     *
     * 获取字符串,Collection,Map,Iterator,Enumeration长度
     *
     * @author zhuou
     * @param obj
     *            对象
     * @return int
     */
    @SuppressWarnings("unchecked")
    public static int length(Object obj)
    {
        if (obj == null)
        {
            return 0;
        }
        if (obj instanceof String)
        {
            return ((String) obj).length();
        }
        if (obj instanceof Collection)
        {
            return ((Collection) obj).size();
        }
        if (obj instanceof Map)
        {
            return ((Map) obj).size();
        }
        int count = 0;
        if (obj instanceof Iterator)
        {
            Iterator iter = (Iterator) obj;
            count = 0;
            while (iter.hasNext())
            {
                count++;
                iter.next();
            }
            return count;
        }
        if (obj instanceof Enumeration)
        {
            Enumeration _enum = (Enumeration) obj;
            count = 0;
            while (_enum.hasMoreElements())
            {
                count++;
                _enum.nextElement();
            }
            return count;
        }
        try
        {
            return Array.getLength(obj);
        }
        catch (IllegalArgumentException ex)
        {
            // 异常抛弃
        }
        return count;
    }

    public static Object search(String criteria, int type)
    {
        return null;
    }
}
