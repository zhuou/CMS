package com.oswift.cms.web.backstage;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.entity.AdminBean;
import com.oswift.cms.entity.ChannelBean;
import com.oswift.cms.entity.DictionaryBean;
import com.oswift.cms.entity.ErrorBean;
import com.oswift.cms.entity.MessageBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.service.IChannelService;
import com.oswift.cms.utils.Field;
import com.oswift.cms.utils.PageCode;
import com.oswift.cms.utils.Field.PageName;
import com.oswift.utils.cache.ResourceManager;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.PlatException;

@Controller
@RequestMapping("/security")
public class ChannelAction extends BaseAction
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(ChannelAction.class);

    /**
     * ChannelServiceImpl注入对象
     */
    @Resource
    private IChannelService channelService;

    /**
     *
     * 加载容器栏目
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadAddContainCh", method = RequestMethod.GET)
    public ModelAndView loadAddContainChannel(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            mv = channelService.loadAddChannel(PageName.CONTAINCHANNEL,
                    getAdmin(request).getCompanyID());
            mv.addObject(METHOD, METHOD_ADD);
            return mv;
        }
        catch (PlatException e)
        {
            log.error("Load AddContainChannel page is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 保存容器栏目内容
     *
     * @author zhuou
     */
    @RequestMapping(value = "/addContainCh", method = RequestMethod.POST)
    @ResponseBody
    public Object addContainChannel(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            ChannelBean bean = getPageData(request);

            if (StringUtil.isBlank(bean.getChannelName())
                    || StringUtil.isBlank(bean.getNickName()))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }

            AdminBean adminBean = getAdmin(request);
            bean.setCompanyId(adminBean.getCompanyID());
            bean.setCategory(Field.ChannelCategory.CREATE);
            bean.setCreateUser(adminBean.getAdminName());

            boolean isSuccess = channelService.save(bean,
                    Field.ChannelType.CONTAIN);

            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.CONTAINCHANNEL_SAVE_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.CONTAINCHANNEL_SAVE_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.CONTAINCHANNEL_SAVE_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("Fail to save contain channel info error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 内容栏目详情
     *
     * @author zhuou
     */
    @RequestMapping(value = "/containChDetail", method = RequestMethod.GET)
    public ModelAndView containChDetail(@RequestParam("id")
    int id)
    {
        ModelAndView mv = null;
        try
        {
            mv = channelService.getChannelById(id, PageName.CONTAINCHDETAIL,
                    PageName.ERROR_PAGE);
        }
        catch (PlatException e)
        {
            log.error("Get containChDetail page is error.id=" + id, e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 内容栏目详情
     *
     * @author zhuou
     */
    @RequestMapping(value = "/singleChDetail", method = RequestMethod.GET)
    public ModelAndView singleChDetail(@RequestParam("id")
    int id)
    {
        ModelAndView mv = null;
        try
        {
            mv = channelService.getChannelById(id, PageName.SINGLECHDETAIL,
                    PageName.ERROR_PAGE);
        }
        catch (PlatException e)
        {
            log.error("Get singleChDetail page is error.id=" + id, e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 内容栏目详情
     *
     * @author zhuou
     */
    @RequestMapping(value = "/externalChDetail", method = RequestMethod.GET)
    public ModelAndView externalChDetail(@RequestParam("id")
    int id)
    {
        ModelAndView mv = null;
        try
        {
            mv = channelService.getChannelById(id, PageName.EXTERNALCHDETAIL,
                    PageName.ERROR_PAGE);
        }
        catch (PlatException e)
        {
            log.error("Get externalChDetail page is error.id=" + id, e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 加载单页栏目
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadAddSingleCh", method = RequestMethod.GET)
    public ModelAndView loadAddSingleChannel(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            mv = channelService.loadAddChannel(PageName.SINGLECHANNEL,
                    getAdmin(request).getCompanyID());
            mv.addObject(METHOD, METHOD_ADD);
            return mv;
        }
        catch (PlatException e)
        {
            log.error("Load AddSingleChannel page is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            bean.setUrl("channelList");
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 保存单页栏目内容
     *
     * @author zhuou
     */
    @RequestMapping(value = "/addSingleCh", method = RequestMethod.POST)
    @ResponseBody
    public Object addSingleChannel(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            // 获取页面数据
            ChannelBean bean = getPageData(request);

            if (StringUtil.isBlank(bean.getChannelName())
                    || StringUtil.isBlank(bean.getNickName()))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }
            AdminBean adminBean = getAdmin(request);
            bean.setCompanyId(adminBean.getCompanyID());
            bean.setCategory(Field.ChannelCategory.CREATE);
            bean.setCreateUser(adminBean.getAdminName());

            boolean isSuccess = channelService.save(bean,
                    Field.ChannelType.SINGLE);

            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.SINGLECHANNEL_SAVE_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.SINGLECHANNEL_SAVE_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.SINGLECHANNEL_SAVE_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("Fail to save single channel info error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 加载外接栏目
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadAddExternalCh", method = RequestMethod.GET)
    public ModelAndView loadAddExternalChannel(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            mv = channelService.loadAddChannel(PageName.EXTERNALCHANNEL,
                    getAdmin(request).getCompanyID());
            mv.addObject(METHOD, METHOD_ADD);
            return mv;
        }
        catch (PlatException e)
        {
            log.error("Load AddExternalChannel page is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            bean.setUrl("getCompanyList");
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 根据公司id获取栏目
     *
     * @author zhuou
     */
    @RequestMapping(value = "/getChannel", method = RequestMethod.GET)
    public ModelAndView getChannel(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            mv = channelService.getChannelByCompanyId(getAdmin(request)
                    .getCompanyID());
            mv.setViewName(PageName.CHANNEL_1);
            return mv;
        }
        catch (PlatException e)
        {
            log.error("Get Channel is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 根据公司ID和forums板块查询栏目列表
     *
     * @author zhuou
     */
    @RequestMapping(value = "/getChannel2", method = RequestMethod.GET)
    public ModelAndView getChannel2(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String forums = request.getParameter("forums");
            String iframe = request.getParameter("iframe");
            if (StringUtil.isEmpty(forums))
            {
                mv = new ModelAndView(PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setCode(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
            }
            else
            {
                mv = channelService.getChannel2(getAdmin(request)
                        .getCompanyID(), forums);
                mv.setViewName(PageName.CHANNEL_2);
                mv.addObject("iframe", iframe);
            }
        }
        catch (PlatException e)
        {
            log.error("Get Channel2 is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 根据公司id获取栏目
     *
     * @author zhuou
     */
    @RequestMapping(value = "/getChannel3", method = RequestMethod.GET)
    public ModelAndView getChannel3(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            // String dialogId = request.getParameter("dialogId");
            mv = channelService.getChannelByCompanyId(getAdmin(request)
                    .getCompanyID());
            mv.setViewName(PageName.CHANNEL_3);
            // mv.addObject("dialogId", dialogId);

            return mv;
        }
        catch (PlatException e)
        {
            log.error("Get Channel is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 保存外接栏目内容
     *
     * @author zhuou
     */
    @RequestMapping(value = "/addExternalCh", method = RequestMethod.POST)
    @ResponseBody
    public Object addExternalChannel(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            ChannelBean bean = getPageData(request);
            if (StringUtil.isBlank(bean.getChannelName())
                    || StringUtil.isBlank(bean.getNickName()))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }
            AdminBean adminBean = getAdmin(request);
            bean.setCompanyId(adminBean.getCompanyID());
            bean.setCategory(Field.ChannelCategory.CREATE);
            bean.setCreateUser(adminBean.getAdminName());

            boolean isSuccess = channelService.save(bean,
                    Field.ChannelType.EXTERNAL);

            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.EXTERNALCHANNEL_SAVE_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.EXTERNALCHANNEL_SAVE_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.EXTERNALCHANNEL_SAVE_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("Fail to save external channel info error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 查询栏目信息，当companyId=-1时查询全部栏目，如果>1则查询相关公司栏目信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/getChannelList", method = RequestMethod.GET)
    public ModelAndView getChannelList(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String keyword = request.getParameter("keyword");
            if (!StringUtil.isEmpty(keyword))
            {
                keyword = decodeKeyword(keyword);
            }
            else
            {
                keyword = null;
            }

            int pageNum = DEFAULT_PAGENUM;
            if (!StringUtil.isEmpty(request.getParameter("pageNum")))
            {
                pageNum = Integer.valueOf(request.getParameter("pageNum"));
            }

            PageBean<ChannelBean> pageBean = channelService.getChannelList(this
                    .getAdmin(request).getCompanyID(), keyword, pageNum,
                    this.pageRecordNum);

            // 将每个栏目板块id替换成文字，展现在页面
            getForumInfo(pageBean.getDataList());
            pageBean.setPageRecordNum(this.pageRecordNum);
            mv = new ModelAndView(PageName.CHANNELLIST);
            mv.addObject("pageBean", pageBean);
        }
        catch (PlatException e)
        {
            log.error("Get channel list is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 根据parentId查询栏目
     *
     * @author zhuou
     */
    @RequestMapping(value = "/getChildChannels", method = RequestMethod.GET)
    @ResponseBody
    public Object getChildChannels(@RequestParam("id")
    int id)
    {
        MessageBean bean = new MessageBean();
        try
        {
            if (id <= 0)
            {
                bean.setCode(PageCode.GET_WEBPARAM_FAIL);
                bean.setContent(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
            }
            else
            {
                List<ChannelBean> beans = channelService
                        .getChannelListByParentId(id);

                // 将每个栏目板块id替换成文字，展现在页面
                getForumInfo(beans);
                bean.setCode(PageCode.COMMON_SUCCESS);
                bean.setObjContent(beans);
            }
        }
        catch (PlatException e)
        {
            bean.setCode(PageCode.COMMON_ERROR);
            bean.setContent(e.toString());
        }
        return bean;
    }

    /**
     *
     * 将每个栏目板块id替换成文字，展现在页面
     *
     * @author zhuou
     * @param beans
     *            栏目
     */
    private void getForumInfo(List<ChannelBean> beans)
    {
        if (null == beans || beans.isEmpty())
        {
            return;
        }
        List<DictionaryBean> dList = channelService.getForumInfo();
        for (ChannelBean item : beans)
        {
            String st = item.getForums();
            if (!StringUtil.isEmpty(st))
            {
                for (DictionaryBean d : dList)
                {
                    if (-1 != st.indexOf(d.getFieldValue()))
                    {
                        st = st.replace(d.getFieldValue(), d.getTitle());
                    }
                }
            }
            item.setForums(st);
        }
    }

    /**
     *
     * 加载内容栏目的修改页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadUpdateContainCh", method = RequestMethod.GET)
    public ModelAndView loadUpdateContainChannel(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String id = request.getParameter("id");
            if (StringUtil.isEmpty(id))
            {
                mv = new ModelAndView(PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setCode(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
                return mv;
            }

            mv = channelService.loadUpdateChannel(PageName.CONTAINCHANNEL,
                    getAdmin(request).getCompanyID(), Integer.valueOf(id));
            mv.addObject(METHOD, METHOD_UPDATE);
            return mv;
        }
        catch (PlatException e)
        {
            log.error("loadUpdateContainCh page is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }

        return mv;
    }

    /**
     *
     * 加载单页栏目修改页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadUpdateSingleCh", method = RequestMethod.GET)
    public ModelAndView loadUpdateSingleChannel(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String id = request.getParameter("id");
            if (StringUtil.isEmpty(id))
            {
                mv = new ModelAndView(PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setCode(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
                return mv;
            }

            mv = channelService.loadUpdateChannel(PageName.SINGLECHANNEL,
                    getAdmin(request).getCompanyID(), Integer.valueOf(id));
            mv.addObject(METHOD, METHOD_UPDATE);
        }
        catch (PlatException e)
        {
            log.error("loadUpdateSingleChannel page is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }

        return mv;
    }

    /**
     *
     * 加载外接栏目修改页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadUpdateExternalCh", method = RequestMethod.GET)
    public ModelAndView loadUpdateExternalChannel(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String id = request.getParameter("id");
            if (StringUtil.isEmpty(id))
            {
                mv = new ModelAndView(PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setCode(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
                return mv;
            }

            mv = channelService.loadUpdateChannel(PageName.EXTERNALCHANNEL,
                    getAdmin(request).getCompanyID(), Integer.valueOf(id));
            mv.addObject(METHOD, METHOD_UPDATE);
        }
        catch (PlatException e)
        {
            log.error("loadUpdateExternalChannel page is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }

        return mv;
    }

    /**
     *
     * 修改内容栏目信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/updateContainCh", method = RequestMethod.POST)
    @ResponseBody
    public Object updateContainChannel(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            ChannelBean bean = getPageData(request);

            if (StringUtil.isBlank(bean.getChannelName())
                    || bean.getChannelId() <= 0
                    || StringUtil.isBlank(bean.getNickName()))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }

            bean.setLastEditUser(getAdmin(request).getAdminName());

            boolean isSuccess = channelService.update(bean);

            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.CONTAINCHANNEL_UPDATE_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.CONTAINCHANNEL_UPDATE_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.CONTAINCHANNEL_UPDATE_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("Fail to update contain channel info error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 修改单页栏目信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/updateSingleCh", method = RequestMethod.POST)
    @ResponseBody
    public Object updateSingleChannel(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            ChannelBean bean = getPageData(request);
            if (StringUtil.isBlank(bean.getChannelName())
                    || bean.getChannelId() <= 0
                    || StringUtil.isBlank(bean.getNickName()))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }

            bean.setLastEditUser(getAdmin(request).getAdminName());

            boolean isSuccess = channelService.update(bean);

            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.SINGLECHANNEL_UPDATE_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.SINGLECHANNEL_UPDATE_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.SINGLECHANNEL_UPDATE_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("Fail to update Single channel info error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 修改外接栏目信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/updateExternalCh", method = RequestMethod.POST)
    @ResponseBody
    public Object updateExternalChannel(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            ChannelBean bean = getPageData(request);

            if (StringUtil.isBlank(bean.getChannelName())
                    || bean.getChannelId() <= 0
                    || StringUtil.isBlank(bean.getNickName()))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }

            bean.setLastEditUser(getAdmin(request).getAdminName());

            boolean isSuccess = channelService.update(bean);

            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.EXTERNALCHANNEL_UPDATE_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.EXTERNALCHANNEL_UPDATE_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.EXTERNALCHANNEL_UPDATE_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("Fail to update External channel info error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 删除栏目，可以删除一条记录，也可以删除多条记录
     *
     * @author zhuou
     */
    @RequestMapping(value = "/delChannel", method = RequestMethod.POST)
    @ResponseBody
    public Object delChannel(HttpServletRequest request)
    {
        int successNum = 0, failNum = 0;
        MessageBean messBean = new MessageBean();
        try
        {
            String ids = request.getParameter("ids");
            if (StringUtil.isEmpty(ids))
            {
                messBean.setCode(PageCode.GET_WEBPARAM_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                return messBean;
            }
            int companyId = getAdmin(request).getCompanyID();
            String[] idArray = ids.split(Field.COMMA);
            for (String id : idArray)
            {
                boolean isSuccess = channelService.delChannel(Integer
                        .valueOf(id), companyId);
                if (isSuccess)
                {
                    successNum++;
                }
                else
                {
                    failNum++;
                }
            }
        }
        catch (Exception e)
        {
            log.error("Fail to del channel info error.", e);
        }

        if (failNum <= 0 && successNum > 0)
        {
            // 全部删除成功
            messBean.setCode(PageCode.COMMON_SUCCESS);
            messBean.setContent(ResourceManager
                    .getValue(PageCode.CHANNEL_DEL_SUCCESS));
        }
        else if (failNum >= 0 && successNum <= 0)
        {
            // 全部删除失败
            messBean.setCode(PageCode.CHANNEL_DEL_FAIL);
            messBean.setContent(ResourceManager
                    .getValue(PageCode.CHANNEL_DEL_FAIL));
        }
        else
        {
            // 部分删除成功
            messBean.setCode(PageCode.COMMON_SUCCESS);
            messBean.setContent(ResourceManager
                    .getValue(PageCode.CHANNEL_DEL_PART_SUCCESS));
        }

        return messBean;
    }

    /**
     *
     * 栏目管理页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadChannelManage", method = RequestMethod.GET)
    public ModelAndView loadChannelManage(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            mv = channelService.getChannelByCompanyId(getAdmin(request)
                    .getCompanyID());
            mv.setViewName(PageName.CHANNELMANAGE);

            return mv;
        }
        catch (PlatException e)
        {
            log.error("loadChannelManage is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 获取前台页面的数据
     *
     * @author zhuou
     */
    public ChannelBean getPageData(HttpServletRequest request)
    {
        ChannelBean bean = new ChannelBean();
        String sid = request.getParameter("channelId");
        String channelName = request.getParameter("channelName");
        String nickName = request.getParameter("nickName");
        String content = request.getParameter("content");
        String description = request.getParameter("description");
        String iconUrl = request.getParameter("iconUrl");
        String meta_Description = request.getParameter("meta_Description");
        String meta_Keywords = request.getParameter("meta_Keywords");
        String openType = request.getParameter("openType");
        String parentId = request.getParameter("parentId");
        String purviewType = request.getParameter("purviewType");
        String sort = request.getParameter("sort");
        String tips = request.getParameter("tips");
        String forums = request.getParameter("forums");
        String accessRight = request.getParameter("accessRight");
        String channelUrl = request.getParameter("channelUrl");

        if (StringUtil.isEmpty(sid))
        {
            bean.setChannelId(0);
        }
        else
        {
            bean.setChannelId(Integer.valueOf(sid));
        }

        bean.setChannelName(StringUtil.trim(channelName, ""));
        bean.setNickName(StringUtil.trim(nickName, ""));
        bean.setContent(StringUtil.trim(content, ""));
        bean.setDescription(StringUtil.trim(description, ""));
        bean.setIconUrl(StringUtil.trim(iconUrl, ""));
        bean.setMeta_Description(StringUtil.trim(meta_Description, ""));
        bean.setMeta_Keywords(StringUtil.trim(meta_Keywords, ""));
        bean.setOpenType(Integer.valueOf(openType));
        bean.setParentId(Integer.valueOf(parentId));
        bean.setPurviewType(Integer.valueOf(purviewType));
        bean.setSort(Integer.valueOf(sort));
        bean.setTips(StringUtil.trim(tips, ""));
        bean.setForums(forums);
        bean.setAccessRight(accessRight);
        bean.setChannelUrl(channelUrl);

        return bean;
    }

    public void setChannelService(IChannelService channelService)
    {
        this.channelService = channelService;
    }
}