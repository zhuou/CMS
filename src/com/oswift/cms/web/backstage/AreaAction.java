package com.oswift.cms.web.backstage;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oswift.cms.entity.AreaBean;
import com.oswift.cms.entity.MessageBean;
import com.oswift.cms.service.IAreaService;
import com.oswift.cms.utils.Field;
import com.oswift.cms.utils.PageCode;
import com.oswift.utils.cache.ResourceManager;
import com.oswift.utils.exception.PlatException;

@Controller
@RequestMapping("/security")
public class AreaAction extends BaseAction
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(AreaAction.class);

    /**
     * spring注入AreaServiceImpl对象
     */
    @Resource
    private IAreaService areaService;

    /**
     *
     * 根据ID获取城市信息
     *
     * @author zhuou
     * @param id
     *            省ID
     * @return json
     */
    @RequestMapping(value = "/getCity", method = RequestMethod.GET)
    @ResponseBody
    public Object getCity(@RequestParam("id") int id)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            List<AreaBean> list = areaService.getAreaById(Field.Area.CITY, id);
            messBean.setCode(PageCode.COMMON_SUCCESS);
            messBean.setObjContent(list);
        }
        catch (PlatException e)
        {
            log.error("Fail to get City info error.id=" + id, e);
            messBean.setCode(PageCode.GET_AREA_FAIL);
            messBean.setContent(ResourceManager
                    .getValue(PageCode.GET_AREA_FAIL));
        }
        return messBean;
    }

    /**
     *
     * 根据ID获取区/县信息
     *
     * @author zhuou
     * @param id
     * @return json
     */
    @RequestMapping(value = "/getDistrict", method = RequestMethod.GET)
    @ResponseBody
    public Object getDistrict(@RequestParam("id") int id)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            List<AreaBean> list = areaService.getAreaById(Field.Area.DISTRICT,
                    id);
            messBean.setCode(PageCode.COMMON_SUCCESS);
            messBean.setObjContent(list);
        }
        catch (PlatException e)
        {
            log.error("Fail to get Area info error.id=" + id, e);
            messBean.setCode(PageCode.GET_AREA_FAIL);
            messBean.setContent(ResourceManager
                    .getValue(PageCode.GET_AREA_FAIL));
        }
        return messBean;
    }

    public void setAreaService(IAreaService areaService)
    {
        this.areaService = areaService;
    }
}
