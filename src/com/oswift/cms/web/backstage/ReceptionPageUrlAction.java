package com.oswift.cms.web.backstage;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.entity.AdminBean;
import com.oswift.cms.entity.ErrorBean;
import com.oswift.cms.entity.MessageBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.entity.ReceptionPageUrlBean;
import com.oswift.cms.entity.WebConfigBean;
import com.oswift.cms.service.IReceptionPageUrlService;
import com.oswift.cms.service.IWebConfigService;
import com.oswift.cms.utils.PageCode;
import com.oswift.cms.utils.Field.PageName;
import com.oswift.utils.cache.ResourceManager;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.PlatException;

@Controller
@RequestMapping("/security")
public class ReceptionPageUrlAction extends BaseAction
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(ReceptionPageUrlAction.class);

    /**
     * spring注入
     */
    @Resource
    private IReceptionPageUrlService receptionPageUrlService;

    /**
     * spring注入CompanyServiceImpl对象
     */
    @Resource
    private IWebConfigService webConfigService;

    /**
     *
     * 获取站点前台Url配置信息列表
     *
     * @author zhuou
     */
    @RequestMapping(value = "/getReceptionPageUrlList", method = RequestMethod.GET)
    public ModelAndView getReceptionPageUrlList(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String keyword = request.getParameter("keyword");
            if (!StringUtil.isEmpty(keyword))
            {
                keyword = decodeKeyword(keyword);
            }
            else
            {
                keyword = null;
            }

            int pageNum = DEFAULT_PAGENUM;
            if (!StringUtil.isEmpty(request.getParameter("pageNum")))
            {
                pageNum = Integer.valueOf(request.getParameter("pageNum"));
            }

            AdminBean adminBean = this.getAdmin(request);
            int companyId = adminBean.getCompanyID();
            PageBean<ReceptionPageUrlBean> pageBean = receptionPageUrlService
                    .getReceptionPageUrlList(keyword, pageNum, pageRecordNum,
                            companyId);
            pageBean.setPageRecordNum(this.pageRecordNum);

            mv = new ModelAndView(PageName.RECEPTIONPAGEURLLIST);
            mv.addObject("pageBean", pageBean);
        }
        catch (PlatException e)
        {
            log.error("Get getReceptionPageUrlList is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 加载新增页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadAddReceptionPageUrl", method = RequestMethod.GET)
    public ModelAndView loadAddReceptionPageUrl(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            AdminBean adminBean = this.getAdmin(request);
            int companyId = adminBean.getCompanyID();
            List<WebConfigBean> webList = webConfigService
                    .getWebConfigByCompanyId(companyId);
            mv = new ModelAndView(PageName.RECEPTIONPAGEURL);
            mv.addObject(METHOD, METHOD_ADD);
            mv.addObject("webList", webList);
        }
        catch (PlatException e)
        {
            log.error("The method of loadAddReceptionPageUrl is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 新增网站前台url配置信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/addReceptionPageUrl", method = RequestMethod.POST)
    @ResponseBody
    public Object addReceptionPageUrl(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String nickName = request.getParameter("nickName");
            String jspPath = request.getParameter("jspPath");
            String intro = request.getParameter("intro");
            String urlType = request.getParameter("urlType");
            String siteId = request.getParameter("siteId");
            if (StringUtil.isBlank(nickName) || StringUtil.isBlank(jspPath)
                    || StringUtil.isBlank(urlType)
                    || StringUtil.isBlank(siteId))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }
            AdminBean adminBean = this.getAdmin(request);
            ReceptionPageUrlBean bean = new ReceptionPageUrlBean();
            bean.setCreateTime(new Date());
            bean.setCreateUser(adminBean.getAdminName());
            bean.setIntro(intro);
            bean.setJspPath(jspPath);
            bean.setNickName(nickName);
            bean.setSiteId(Integer.valueOf(siteId));
            bean.setUpdateTime(new Date());
            bean.setUrlType(Integer.valueOf(urlType));
            boolean isSuccess = receptionPageUrlService.add(bean);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.ADD_RECEPTIONPAGEURL_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.ADD_RECEPTIONPAGEURL_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.ADD_RECEPTIONPAGEURL_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of addReceptionPageUrl is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        return messBean;
    }

    /**
     *
     * 删除一条信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/delReceptionPageUrl", method = RequestMethod.GET)
    @ResponseBody
    public Object delReceptionPageUrl(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String id = request.getParameter("id");
            if (StringUtil.isBlank(id))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }
            boolean isSuccess = receptionPageUrlService
                    .del(Integer.valueOf(id));
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.DEL_RECEPTIONPAGEURL_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.DEL_RECEPTIONPAGEURL_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.DEL_RECEPTIONPAGEURL_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of delReceptionPageUrl is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        return messBean;
    }

    /**
     *
     * 加载修改页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadUpdateReceptionPageUrl", method = RequestMethod.GET)
    public ModelAndView loadUpdateReceptionPageUrl(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String id = request.getParameter("id");
            if (StringUtil.isBlank(id))
            {
                mv = new ModelAndView(PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                mv.addObject("bean", bean);
                return mv;
            }
            ReceptionPageUrlBean bean = receptionPageUrlService
                    .getReceptionPageUrlDetail(Integer.valueOf(id));
            mv = new ModelAndView(PageName.RECEPTIONPAGEURL);
            mv.addObject(METHOD, METHOD_UPDATE);
            mv.addObject("bean", bean);
        }
        catch (PlatException e)
        {
            log.error("The method of loadUpdateReceptionPageUrl is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 修改网站前台url配置信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/updateReceptionPageUrl", method = RequestMethod.POST)
    @ResponseBody
    public Object updateReceptionPageUrl(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String nickName = request.getParameter("nickName");
            String jspPath = request.getParameter("jspPath");
            String intro = request.getParameter("intro");
            String urlType = request.getParameter("urlType");
            String id = request.getParameter("id");
            if (StringUtil.isBlank(nickName) || StringUtil.isBlank(jspPath)
                    || StringUtil.isBlank(urlType) || StringUtil.isBlank(id))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }
            ReceptionPageUrlBean bean = new ReceptionPageUrlBean();
            bean.setUpdateTime(new Date());
            bean.setIntro(intro);
            bean.setJspPath(jspPath);
            bean.setNickName(nickName);
            bean.setPageUrlId(Integer.valueOf(id));
            bean.setUpdateTime(new Date());
            bean.setUrlType(Integer.valueOf(urlType));
            boolean isSuccess = receptionPageUrlService.update(bean);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.UPDATE_RECEPTIONPAGEURL_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.UPDATE_RECEPTIONPAGEURL_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.UPDATE_RECEPTIONPAGEURL_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of updateReceptionPageUrl is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        return messBean;
    }

    /**
     *
     * 前台url配置信息详情
     *
     * @author zhuou
     */
    @RequestMapping(value = "/receptionPageUrlDetail", method = RequestMethod.GET)
    public ModelAndView receptionPageUrlDetail(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String id = request.getParameter("id");
            if (StringUtil.isBlank(id))
            {
                mv = new ModelAndView(PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                mv.addObject("bean", bean);
                return mv;
            }
            ReceptionPageUrlBean bean = receptionPageUrlService
                    .getReceptionPageUrlDetail(Integer.valueOf(id));
            mv = new ModelAndView(PageName.RECEPTIONPAGEURLDETAIL);
            mv.addObject("bean", bean);
        }
        catch (PlatException e)
        {
            log.error("The method of receptionPageUrlDetail is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    public void setReceptionPageUrlService(
            IReceptionPageUrlService receptionPageUrlService)
    {
        this.receptionPageUrlService = receptionPageUrlService;
    }

    public void setWebConfigService(IWebConfigService webConfigService)
    {
        this.webConfigService = webConfigService;
    }
}
