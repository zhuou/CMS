package com.oswift.cms.web.backstage;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.entity.ErrorBean;
import com.oswift.cms.entity.FilesBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.service.IFilesService;
import com.oswift.cms.utils.Field;
import com.oswift.cms.utils.Field.PageName;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.PlatException;

@Controller
@RequestMapping("/security")
public class FilesAction extends BaseAction
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(FilesAction.class);

    /**
     * 每页显示记录条数
     */
    private static int PAGENUM = 16;

    /**
     *
     * spring 注入
     */
    @Resource
    private IFilesService filesService;

    /**
     *
     * 选择已上传图片页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/getUploadImg", method = RequestMethod.GET)
    public ModelAndView getUploadImg(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String callback = request.getParameter("callback");
            String iframe = request.getParameter("iframe");
            int pageNum = DEFAULT_PAGENUM;
            if (!StringUtil.isEmpty(request.getParameter("pageNum")))
            {
                pageNum = Integer.valueOf(request.getParameter("pageNum"));
            }

            PageBean<FilesBean> pageBean = filesService.getFiles(this.getAdmin(request)
                    .getCompanyID(), null, Field.Files.IMG, PAGENUM,
                    pageNum);

            pageBean.setPageRecordNum(PAGENUM);
            mv = new ModelAndView(PageName.CHECKUPLOADPICS);
            mv.addObject("pageBean", pageBean);
            mv.addObject("callback", callback);
            mv.addObject("iframe", iframe);
        }
        catch (PlatException e)
        {
            log.error("Load getUploadImg page is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    public void setFilesService(IFilesService filesService)
    {
        this.filesService = filesService;
    }
}
