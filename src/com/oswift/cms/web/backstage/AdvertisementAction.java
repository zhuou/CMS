package com.oswift.cms.web.backstage;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.entity.AdZoneBean;
import com.oswift.cms.entity.AdminBean;
import com.oswift.cms.entity.AdvertisementBean;
import com.oswift.cms.entity.ErrorBean;
import com.oswift.cms.entity.MessageBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.service.IAdvertisementService;
import com.oswift.cms.utils.Field;
import com.oswift.cms.utils.PageCode;
import com.oswift.cms.utils.Field.PageName;
import com.oswift.utils.cache.ResourceManager;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.common.TimeUtil;
import com.oswift.utils.exception.PlatException;

@Controller
@RequestMapping("/security")
public class AdvertisementAction extends BaseAction
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(AdvertisementAction.class);

    /**
     * spring注入
     */
    @Resource
    private IAdvertisementService advertisementService;

    /**
     *
     * 获取广告位列表
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadAdZoneList", method = RequestMethod.GET)
    public ModelAndView loadAdZoneList(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            int pageNum = DEFAULT_PAGENUM;
            if (!StringUtil.isEmpty(request.getParameter("pageNum")))
            {
                pageNum = Integer.valueOf(request.getParameter("pageNum"));
            }
            AdminBean adminBean = this.getAdmin(request);
            int companyId = adminBean.getCompanyID();

            PageBean<AdZoneBean> pageBean = advertisementService.getAdZoneList(
                    companyId, pageNum, pageRecordNum);

            // 设置每页显示多少天数据
            pageBean.setPageRecordNum(this.pageRecordNum);
            mv = new ModelAndView(PageName.ADZONELIST);
            mv.addObject("pageBean", pageBean);
        }
        catch (PlatException e)
        {
            log.error("The method of loadAdZoneList is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }
        catch (Exception e)
        {
            log.error("The method of loadAdZoneList is system's error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.SYSTEM_ERROR);
            bean.setMessInfo(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
            mv.addObject("bean", bean);
        }

        return mv;
    }

    /**
     *
     * 新增广告位
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadAddAdZone", method = RequestMethod.GET)
    public ModelAndView loadAddAdZone(HttpServletRequest request)
    {
        String newZoneCode = "";
        ModelAndView mv = new ModelAndView(PageName.ADDADZONE);
        try
        {
            AdminBean adminBean = this.getAdmin(request);
            int companyId = adminBean.getCompanyID();
            newZoneCode = advertisementService.getZoneCode(companyId);
        }
        catch (PlatException e)
        {
            log.error("The method of loadAddAdZone is error.", e);
        }
        mv.addObject(METHOD, METHOD_ADD);
        mv.addObject("zoneCode", newZoneCode);
        return mv;
    }

    /**
     *
     * 修改广告位
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadUpdateAdZone", method = RequestMethod.GET)
    public ModelAndView loadUpdateAdZone(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String id = request.getParameter("id");
            if (StringUtil.isEmpty(id))
            {
                mv = new ModelAndView(PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setCode(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
                return mv;
            }
            mv = new ModelAndView(PageName.ADDADZONE);
            AdZoneBean bean = advertisementService.getAdZoneById(Integer
                    .valueOf(id));
            mv.addObject(METHOD, METHOD_UPDATE);
            mv.addObject("bean", bean);
        }
        catch (PlatException e)
        {
            log.error("The method of loadUpdateAdZone is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }
        catch (Exception e)
        {
            log.error("The method of loadUpdateAdZone is system's error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.SYSTEM_ERROR);
            bean.setMessInfo(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 新增广告位
     *
     * @author zhuou
     */
    @RequestMapping(value = "/addAdZone", method = RequestMethod.POST)
    @ResponseBody
    public Object addAdZone(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String zoneCode = StringUtil.trim(request.getParameter("zoneCode"));
            String zoneName = StringUtil.trim(request.getParameter("zoneName"));
            String zoneIntro = StringUtil.trim(request
                    .getParameter("zoneIntro"));
            String status = StringUtil.trim(request.getParameter("status"));
            if (StringUtil.isEmpty(zoneCode) || StringUtil.isEmpty(zoneName)
                    || StringUtil.isEmpty(status))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }

            AdminBean adminBean = this.getAdmin(request);
            AdZoneBean adZone = new AdZoneBean();
            adZone.setZoneCode(zoneCode);
            adZone.setZoneName(zoneName);
            adZone.setCompanyId(adminBean.getCompanyID());
            adZone.setZoneIntro(zoneIntro);
            adZone.setCreateUser(adminBean.getAdminName());
            adZone.setCreateTime(new Date());
            if (Field.FALSE.equals(status))
            {
                adZone.setStatus(false);
            }
            else
            {
                adZone.setStatus(true);
            }

            // 验证ZoneCode是否有重复
            AdZoneBean adOne = advertisementService.getOneByZoneCode(adminBean
                    .getCompanyID(), zoneCode);
            if (null != adOne)
            {
                messBean.setCode(PageCode.ZONECODE_IS_EXIST);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.ZONECODE_IS_EXIST));
            }
            else
            {
                // 新增
                int rows = advertisementService.addAdZone(adZone);
                if (rows > 0)
                {
                    messBean.setCode(PageCode.COMMON_SUCCESS);
                    messBean.setContent(ResourceManager
                            .getValue(PageCode.ADZONE_ADD_SUCCESS));
                }
                else
                {
                    messBean.setCode(PageCode.ADZONE_ADD_FAIL);
                    messBean.setContent(ResourceManager
                            .getValue(PageCode.ADZONE_ADD_FAIL));
                }
            }
        }
        catch (PlatException e)
        {
            log.error("The method of addAdZone is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        catch (Exception e)
        {
            log.error("The method of addAdZone is system's error.", e);
            messBean.setCode(PageCode.SYSTEM_ERROR);
            messBean.setContent(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 修改广告位
     *
     * @author zhuou
     */
    @RequestMapping(value = "/updateAdZone", method = RequestMethod.POST)
    @ResponseBody
    public Object updateAdZone(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String zoneId = StringUtil.trim(request.getParameter("zoneId"));
            String zoneCode = StringUtil.trim(request.getParameter("zoneCode"));
            String zoneName = StringUtil.trim(request.getParameter("zoneName"));
            String zoneIntro = StringUtil.trim(request
                    .getParameter("zoneIntro"));
            String status = StringUtil.trim(request.getParameter("status"));
            if (StringUtil.isEmpty(zoneId) || StringUtil.isEmpty(zoneName)
                    || StringUtil.isEmpty(status))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }

            AdZoneBean adZone = new AdZoneBean();
            int iZoneId = Integer.valueOf(zoneId);
            adZone.setZoneId(iZoneId);
            adZone.setZoneName(zoneName);
            adZone.setZoneIntro(zoneIntro);
            if (Field.FALSE.equals(status))
            {
                adZone.setStatus(false);
            }
            else
            {
                adZone.setStatus(true);
            }

            AdminBean adminBean = this.getAdmin(request);
            // 验证ZoneCode是否有重复
            AdZoneBean adOne = advertisementService.getOneByZoneCode(adminBean
                    .getCompanyID(), zoneCode);
            if (null != adOne && adOne.getZoneId() != iZoneId)
            {
                messBean.setCode(PageCode.ZONECODE_IS_EXIST);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.ZONECODE_IS_EXIST));
            }
            else
            {
                // 修改
                int rows = advertisementService.updateAdZone(adZone);
                if (rows > 0)
                {
                    messBean.setCode(PageCode.COMMON_SUCCESS);
                    messBean.setContent(ResourceManager
                            .getValue(PageCode.ADZONE_UPDATE_SUCCESS));
                }
                else
                {
                    messBean.setCode(PageCode.ADZONE_UPDATE_FAIL);
                    messBean.setContent(ResourceManager
                            .getValue(PageCode.ADZONE_UPDATE_FAIL));
                }
            }
        }
        catch (PlatException e)
        {
            log.error("The method of updateAdZone is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        catch (Exception e)
        {
            log.error("The method of updateAdZone is system's error.", e);
            messBean.setCode(PageCode.SYSTEM_ERROR);
            messBean.setContent(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 删除广告位
     *
     * @author zhuou
     */
    @RequestMapping(value = "/delAdZone", method = RequestMethod.POST)
    @ResponseBody
    public Object delAdZone(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String ids = request.getParameter("ids");
            if (StringUtil.isEmpty(ids))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }

            int rows = 0;
            String arrayId[] = ids.split(Field.COMMA);
            for (String id : arrayId)
            {
                boolean isSuccess = advertisementService.delAdZone(Integer
                        .valueOf(id));
                if (isSuccess)
                {
                    rows++;
                }
            }
            if (rows > 0)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.ADZONE_DEL_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.ADZONE_DEL_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.ADZONE_DEL_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of delAdZone is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        catch (Exception e)
        {
            log.error("The method of delAdZone is system's error.", e);
            messBean.setCode(PageCode.SYSTEM_ERROR);
            messBean.setContent(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 获取广告位详情
     *
     * @author zhuou
     */
    @RequestMapping(value = "/getAdZoneDetail", method = RequestMethod.GET)
    public ModelAndView getAdZoneDetail(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String id = request.getParameter("id");
            if (StringUtil.isEmpty(id))
            {
                mv = new ModelAndView(PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setCode(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
                return mv;
            }
            AdZoneBean bean = advertisementService.getAdZoneById(Integer
                    .valueOf(id));
            mv = new ModelAndView(PageName.ADZONEDETAIL);
            mv.addObject(METHOD, METHOD_UPDATE);
            mv.addObject("bean", bean);
        }
        catch (PlatException e)
        {
            log.error("The method of getAdZoneDetail is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }
        catch (Exception e)
        {
            log.error("The method of getAdZoneDetail is system's error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.SYSTEM_ERROR);
            bean.setMessInfo(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
            mv.addObject("bean", bean);
        }

        return mv;
    }

    /**
     *
     * 修改广告位状态
     *
     * @author zhuou
     */
    @RequestMapping(value = "/updateAdZoneStatus", method = RequestMethod.POST)
    @ResponseBody
    public Object updateAdZoneStatus(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String zoneId = request.getParameter("zoneId");
            String status = request.getParameter("status");
            if (StringUtil.isEmpty(zoneId) || StringUtil.isEmpty(status))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }
            boolean bStatus = true;
            if (Field.FALSE.equals(status))
            {
                bStatus = false;
            }
            boolean isSuccess = advertisementService.updateStatus(Integer
                    .valueOf(zoneId), bStatus);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.ADZONE_UPDATESTATUS_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.ADZONE_UPDATESTATUS_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.ADZONE_UPDATESTATUS_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of updateAdZoneStatus is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        catch (Exception e)
        {
            log.error("The method of updateAdZoneStatus is system's error.", e);
            messBean.setCode(PageCode.SYSTEM_ERROR);
            messBean.setContent(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 获取广告列表
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadAdList", method = RequestMethod.GET)
    public ModelAndView loadAdList(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            int pageNum = DEFAULT_PAGENUM;
            if (!StringUtil.isEmpty(request.getParameter("pageNum")))
            {
                pageNum = Integer.valueOf(request.getParameter("pageNum"));
            }

            int zoneId = 0;
            if (!StringUtil.isEmpty(request.getParameter("zoneId")))
            {
                zoneId = Integer.valueOf(request.getParameter("zoneId"));
            }

            AdminBean adminBean = this.getAdmin(request);
            int companyId = adminBean.getCompanyID();

            PageBean<AdvertisementBean> pageBean = advertisementService
                    .getAdList(companyId, zoneId, pageNum, pageRecordNum);

            // 设置每页显示多少天数据
            pageBean.setPageRecordNum(this.pageRecordNum);
            mv = new ModelAndView(PageName.ADLIST);
            mv.addObject("pageBean", pageBean);
        }
        catch (PlatException e)
        {
            log.error("The method of loadAdList is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }
        catch (Exception e)
        {
            log.error("The method of loadAdList is system's error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.SYSTEM_ERROR);
            bean.setMessInfo(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
            mv.addObject("bean", bean);
        }

        return mv;
    }

    /**
     *
     * 加载新增广告页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadAddAd", method = RequestMethod.GET)
    public ModelAndView loadAddAd(HttpServletRequest request)
    {
        ModelAndView mv = new ModelAndView(PageName.ADVERTISEMENT);
        try
        {
            AdminBean adminBean = this.getAdmin(request);
            int companyId = adminBean.getCompanyID();

            List<AdZoneBean> adList = advertisementService
                    .getAdZoneListByCompanyId(companyId);
            mv.addObject("list", adList);
            mv.addObject(METHOD, METHOD_ADD);
        }
        catch (PlatException e)
        {
            log.error("The method of loadAddAd is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }
        catch (Exception e)
        {
            log.error("The method of loadAddAd is system's error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.SYSTEM_ERROR);
            bean.setMessInfo(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 加载修改广告页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadUpdateAd", method = RequestMethod.GET)
    public ModelAndView loadUpdateAd(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String id = request.getParameter("id");
            if (StringUtil.isEmpty(id))
            {
                mv = new ModelAndView(PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setCode(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
                return mv;
            }
            AdminBean adminBean = this.getAdmin(request);
            int companyId = adminBean.getCompanyID();

            mv = new ModelAndView(PageName.ADVERTISEMENT);
            AdvertisementBean bean = advertisementService.getAdDetail(Integer
                    .valueOf(id));
            List<AdZoneBean> adList = advertisementService
                    .getAdZoneListByCompanyId(companyId);

            mv.addObject("list", adList);
            mv.addObject(METHOD, METHOD_UPDATE);
            mv.addObject("bean", bean);
        }
        catch (PlatException e)
        {
            log.error("The method of loadUpdateAd is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }
        catch (Exception e)
        {
            log.error("The method of loadUpdateAd is system's error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.SYSTEM_ERROR);
            bean.setMessInfo(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 新增广告
     *
     * @author zhuou
     */
    @RequestMapping(value = "/addAd", method = RequestMethod.POST)
    @ResponseBody
    public Object addAd(HttpServletRequest request)
    {
        MessageBean messBean = null;
        try
        {
            messBean = getWebData(request);
            if (!PageCode.COMMON_SUCCESS.equals(messBean.getCode()))
            {
                return messBean;
            }

            // 新增
            int rows = advertisementService
                    .addAdvertisement((AdvertisementBean) messBean
                            .getObjContent());
            if (rows > 0)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.AD_ADD_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.AD_ADD_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.AD_ADD_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of addAd is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        catch (Exception e)
        {
            log.error("The method of addAd is system's error.", e);
            messBean.setCode(PageCode.SYSTEM_ERROR);
            messBean.setContent(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 修改广告
     *
     * @author zhuou
     */
    @RequestMapping(value = "/updateAd", method = RequestMethod.POST)
    @ResponseBody
    public Object updateAd(HttpServletRequest request)
    {
        MessageBean messBean = null;
        try
        {
            messBean = getWebData(request);
            if (!PageCode.COMMON_SUCCESS.equals(messBean.getCode()))
            {
                return messBean;
            }

            AdvertisementBean bean = (AdvertisementBean) messBean
                    .getObjContent();
            if (bean.getAdId() <= 0)
            {
                messBean.setCode(PageCode.GET_WEBPARAM_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                return messBean;
            }

            // 修改
            int rows = advertisementService.updateAdvertisement(bean);
            if (rows > 0)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.AD_UPDATE_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.AD_UPDATE_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.AD_UPDATE_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of updateAd is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        catch (Exception e)
        {
            log.error("The method of updateAd is system's error.", e);
            messBean.setCode(PageCode.SYSTEM_ERROR);
            messBean.setContent(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 获取广告位详情
     *
     * @author zhuou
     */
    @RequestMapping(value = "/getAdDetail", method = RequestMethod.GET)
    public ModelAndView getAdDetail(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String id = request.getParameter("id");
            if (StringUtil.isEmpty(id))
            {
                mv = new ModelAndView(PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setCode(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
                return mv;
            }
            AdvertisementBean bean = advertisementService.getAdDetail(Integer
                    .valueOf(id));
            mv = new ModelAndView(PageName.ADDETAIL);
            mv.addObject("bean", bean);
        }
        catch (PlatException e)
        {
            log.error("The method of getAdDetail is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }
        catch (Exception e)
        {
            log.error("The method of getAdDetail is system's error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.SYSTEM_ERROR);
            bean.setMessInfo(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
            mv.addObject("bean", bean);
        }

        return mv;
    }

    /**
     *
     * 修改广告通过审核状态
     *
     * @author zhuou
     */
    @RequestMapping(value = "/updateAdStatus", method = RequestMethod.POST)
    @ResponseBody
    public Object updateAdStatus(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String id = request.getParameter("id");
            String passed = request.getParameter("passed");
            if (StringUtil.isEmpty(id) || StringUtil.isEmpty(passed))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }

            int iId = Integer.valueOf(id);
            boolean bPassed = true;
            if (Field.FALSE.equals(passed))
            {
                bPassed = false;
            }

            // 新增
            int rows = advertisementService.updateAdStatus(iId, bPassed);

            if (rows > 0)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.AD_UPDATE_STATUS_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.AD_UPDATE_STATUS_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.AD_UPDATE_STATUS_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of updateAdStatus is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        catch (Exception e)
        {
            log.error("The method of updateAdStatus is system's error.", e);
            messBean.setCode(PageCode.SYSTEM_ERROR);
            messBean.setContent(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 删除广告
     *
     * @author zhuou
     */
    @RequestMapping(value = "/deAd", method = RequestMethod.POST)
    @ResponseBody
    public Object deAd(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String ids = request.getParameter("ids");
            if (StringUtil.isEmpty(ids))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }

            int rows = 0;
            String arrayId[] = ids.split(Field.COMMA);
            for (String id : arrayId)
            {
                boolean isSuccess = advertisementService.delAd(Integer
                        .valueOf(id));
                if (isSuccess)
                {
                    rows++;
                }
            }
            if (rows > 0)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.AD_DEL_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.AD_DEL_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.AD_DEL_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of deAd is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        catch (Exception e)
        {
            log.error("The method of deAd is system's error.", e);
            messBean.setCode(PageCode.SYSTEM_ERROR);
            messBean.setContent(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 获取页面数据
     *
     * @author zhuou
     */
    public MessageBean getWebData(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
        messBean.setContent(ResourceManager
                .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));

        String adId = request.getParameter("adId");
        String zoneId = request.getParameter("zoneId");
        String adName = request.getParameter("adName");
        String adType = request.getParameter("adType");
        String imgHeight = request.getParameter("imgHeight");
        String imgWidth = request.getParameter("imgWidth");
        String imgUrl = request.getParameter("imgUrl");
        String linkAlt = request.getParameter("linkAlt");
        String adIntro = request.getParameter("adIntro");
        String linkUrl = request.getParameter("linkUrl");
        String openTimeLimit = request.getParameter("openTimeLimit");
        String priority = request.getParameter("priority");
        String sort = request.getParameter("sort");
        String passed = request.getParameter("passed");
        String startTime = request.getParameter("startTime");
        String endTime = request.getParameter("endTime");
        AdvertisementBean bean = new AdvertisementBean();
        if (!StringUtil.isEmpty(adId))
        {
            bean.setAdId(Integer.valueOf(adId));
        }
        if (StringUtil.isEmpty(zoneId))
        {
            return messBean;
        }
        bean.setZoneId(Integer.valueOf(zoneId));
        if (StringUtil.isEmpty(adName))
        {
            return messBean;
        }
        bean.setAdName(adName);
        if (String.valueOf(Field.AdType.IMG).equals(adType))
        {
            bean.setAdType(Field.AdType.IMG);
            if (StringUtil.isEmpty(imgUrl))
            {
                return messBean;
            }
            bean.setImgUrl(imgUrl);
            bean.setAdIntro(adIntro);
        }
        else if (String.valueOf(Field.AdType.ANIMATION).equals(adType))
        {
            bean.setAdType(Field.AdType.ANIMATION);
            if (StringUtil.isEmpty(imgUrl))
            {
                return messBean;
            }
            bean.setImgUrl(imgUrl);
        }
        else if (String.valueOf(Field.AdType.TXT).equals(adType))
        {
            bean.setAdType(Field.AdType.TXT);
            if (StringUtil.isEmpty(adIntro))
            {
                return messBean;
            }
            bean.setAdIntro(adIntro);
        }
        else
        {
            messBean.setCode(PageCode.GET_WEBPARAM_FAIL);
            messBean.setContent(ResourceManager
                    .getValue(PageCode.GET_WEBPARAM_FAIL));
            return messBean;
        }
        if (!StringUtil.isEmpty(imgHeight))
        {
            bean.setImgHeight(Integer.valueOf(imgHeight));
        }
        if (!StringUtil.isEmpty(imgWidth))
        {
            bean.setImgWidth(Integer.valueOf(imgWidth));
        }
        bean.setLinkAlt(linkAlt);
        bean.setLinkUrl(linkUrl);
        if (Field.TRUE.equals(openTimeLimit))
        {
            bean.setOpenTimeLimit(true);
            if (StringUtil.isEmpty(startTime))
            {
                return messBean;
            }
            if (StringUtil.isEmpty(endTime))
            {
                return messBean;
            }
            bean.setStartTime(TimeUtil.stringToDate(startTime,
                    TimeUtil.ISO_DATE_NOSECOND_FORMAT));
            bean.setEndTime(TimeUtil.stringToDate(endTime,
                    TimeUtil.ISO_DATE_NOSECOND_FORMAT));
        }
        else
        {
            bean.setOpenTimeLimit(false);
        }
        if (!StringUtil.isEmpty(priority))
        {
            bean.setPriority(Integer.valueOf(priority));
        }
        if (!StringUtil.isEmpty(sort))
        {
            bean.setSort(Integer.valueOf(sort));
        }
        if (Field.TRUE.equals(passed))
        {
            bean.setPassed(true);
        }
        else
        {
            bean.setPassed(false);
        }
        bean.setCreateUser(this.getAdmin(request).getAdminName());
        messBean.setCode(PageCode.COMMON_SUCCESS);
        messBean.setObjContent(bean);
        return messBean;
    }

    public void setAdvertisementService(
            IAdvertisementService advertisementService)
    {
        this.advertisementService = advertisementService;
    }
}
