package com.oswift.cms.web.backstage;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.entity.AdminBean;
import com.oswift.cms.entity.CommentBean;
import com.oswift.cms.entity.ErrorBean;
import com.oswift.cms.entity.MessageBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.service.ICommentService;
import com.oswift.cms.utils.Field;
import com.oswift.cms.utils.PageCode;
import com.oswift.cms.utils.Field.PageName;
import com.oswift.utils.cache.ResourceManager;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.PlatException;

@Controller
@RequestMapping("/security")
public class CommentAction extends BaseAction
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(CommentAction.class);

    /**
     * spring 注入
     */
    @Resource
    private ICommentService commentService;

    /**
     *
     * 获取评论列表
     *
     * @author zhuou
     */
    @RequestMapping(value = "/getCommentList", method = RequestMethod.GET)
    public ModelAndView getCommentList(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String keyword = request.getParameter("keyword");
            if (!StringUtil.isEmpty(keyword))
            {
                keyword = decodeKeyword(keyword);
            }
            else
            {
                keyword = null;
            }

            int pageNum = DEFAULT_PAGENUM;
            if (!StringUtil.isEmpty(request.getParameter("pageNum")))
            {
                pageNum = Integer.valueOf(request.getParameter("pageNum"));
            }

            AdminBean adminBean = this.getAdmin(request);
            int companyId = adminBean.getCompanyID();

            PageBean<CommentBean> pageBean = commentService.getCommentList(
                    keyword, companyId, pageNum, this.pageRecordNum);
            pageBean.setPageRecordNum(this.pageRecordNum);

            mv = new ModelAndView(PageName.COMMENTLIST);
            mv.addObject("pageBean", pageBean);
        }
        catch (PlatException e)
        {
            log.error("The method of getCommentList is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }
        catch (Exception e)
        {
            log.error("The method of getCommentList is system's error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.SYSTEM_ERROR);
            bean.setMessInfo(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 获取评论详情
     *
     * @author zhuou
     */
    @RequestMapping(value = "/commentDetail", method = RequestMethod.GET)
    public ModelAndView commentDetail(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String id = request.getParameter("id");
            if (StringUtil.isEmpty(id))
            {
                mv = new ModelAndView(PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setCode(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
                return mv;
            }

            CommentBean bean = commentService.getOne(Integer.valueOf(id));
            mv = new ModelAndView(PageName.COMMENTDETAIL);
            mv.addObject("bean", bean);
        }
        catch (PlatException e)
        {
            log.error("The method of commentDetail is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }
        catch (Exception e)
        {
            log.error("The method of commentDetail is system's error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.SYSTEM_ERROR);
            bean.setMessInfo(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 修改是否精华状态
     *
     * @author zhuou
     */
    @RequestMapping(value = "/updateEliteStatus", method = RequestMethod.GET)
    @ResponseBody
    public Object updateEliteStatus(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String status = request.getParameter("status");
            String id = request.getParameter("id");
            if (StringUtil.isEmpty(status) || StringUtil.isEmpty(id))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }
            boolean isElite = false;
            if (Field.TRUE.equals(status))
            {
                isElite = true;
            }

            boolean isSuccess = commentService.updateEliteStatus(isElite, Long
                    .valueOf(id));
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.UPDATE_ELITESTATUS_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.UPDATE_ELITESTATUS_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.UPDATE_ELITESTATUS_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("updateEliteStatus's Action is error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    /**
     *
     * 修改是否公开状态
     *
     * @author zhuou
     */
    @RequestMapping(value = "/updatePrivateStatus", method = RequestMethod.GET)
    @ResponseBody
    public Object updatePrivateStatus(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String status = request.getParameter("status");
            String id = request.getParameter("id");
            if (StringUtil.isEmpty(status) || StringUtil.isEmpty(id))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }

            boolean isPrivate = false;
            if (Field.TRUE.equals(status))
            {
                isPrivate = true;
            }

            boolean isSuccess = commentService.updatePrivateStatus(isPrivate,
                    Long.valueOf(id));
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.UPDATE_PRIVATESTATUS_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.UPDATE_PRIVATESTATUS_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.UPDATE_PRIVATESTATUS_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("updatePrivateStatus's Action is error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    /**
     *
     * 根据评论Id删除评论
     *
     * @author zhuou
     */
    @RequestMapping(value = "/delComment", method = RequestMethod.POST)
    @ResponseBody
    public Object delCommentById(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String ids = request.getParameter("ids");
            if (StringUtil.isEmpty(ids))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }

            boolean isSuccess = commentService.delCommentById(ids);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.DEL_COMMENTBYID_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.DEL_COMMENTBYID_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.DEL_COMMENTBYID_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("delCommentById's Action is error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    public void setCommentService(ICommentService commentService)
    {
        this.commentService = commentService;
    }
}
