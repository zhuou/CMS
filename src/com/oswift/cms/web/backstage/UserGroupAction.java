package com.oswift.cms.web.backstage;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.entity.AdminBean;
import com.oswift.cms.entity.ErrorBean;
import com.oswift.cms.entity.MessageBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.entity.UserGroupBean;
import com.oswift.cms.service.IUserGroupService;
import com.oswift.cms.utils.Field;
import com.oswift.cms.utils.PageCode;
import com.oswift.cms.utils.Field.PageName;
import com.oswift.utils.cache.ResourceManager;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.PlatException;

@Controller
@RequestMapping("/security")
public class UserGroupAction extends BaseAction
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(UserGroupAction.class);

    /**
     * spring 注入
     */
    @Resource
    private IUserGroupService userGroupService;

    /**
     *
     * 加载用户组新增页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadAddUserGroup", method = RequestMethod.GET)
    public ModelAndView loadAddUserGroup()
    {
        ModelAndView mv = new ModelAndView(PageName.USERGROUP);
        mv.addObject(METHOD, METHOD_ADD);
        return mv;
    }

    /**
     *
     * 添加用户组信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/addUserGroup", method = RequestMethod.POST)
    @ResponseBody
    public Object addUserGroup(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String groupName = request.getParameter("groupName");
            String description = request.getParameter("description");
            if (StringUtil.isEmpty(groupName))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }
            AdminBean adminBean = this.getAdmin(request);
            int companyId = adminBean.getCompanyID();

            UserGroupBean bean = new UserGroupBean();
            bean.setGroupName(groupName);
            bean.setDescription(description);
            bean.setCompanyId(companyId);
            bean.setGroupType(Field.MemberGroupType.USER_DEFINED);
            bean.setCreateUser(adminBean.getAdminName());

            boolean isSuccess = userGroupService.save(bean);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.USERGROUP_ADD_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.USERGROUP_ADD_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.USERGROUP_ADD_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of addUserGroup is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        catch (Exception e)
        {
            log.error("The method of addUserGroup is system's error.", e);
            messBean.setCode(PageCode.SYSTEM_ERROR);
            messBean.setContent(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 用户组详情
     *
     * @author zhuou
     */
    @RequestMapping(value = "/userGroupDetail", method = RequestMethod.GET)
    public ModelAndView userGroupDetail(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String id = request.getParameter("id");
            if (StringUtil.isEmpty(id))
            {
                mv = new ModelAndView(PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setCode(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
                return mv;
            }

            UserGroupBean bean = userGroupService.getOne(Integer.valueOf(id));
            mv = new ModelAndView(PageName.USERGROUP_DETAIL);
            mv.addObject("bean", bean);
        }
        catch (PlatException e)
        {
            log.error("The method of userGroupDetail is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }
        catch (Exception e)
        {
            log.error("The method of userGroupDetail is system's error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.SYSTEM_ERROR);
            bean.setMessInfo(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 加载用户组修改页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadUpdateUserGroup", method = RequestMethod.GET)
    public ModelAndView loadUpdateUserGroup(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String id = request.getParameter("id");
            if (StringUtil.isEmpty(id))
            {
                mv = new ModelAndView(PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setCode(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
                return mv;
            }
            AdminBean adminBean = this.getAdmin(request);
            int companyId = adminBean.getCompanyID();

            mv = userGroupService.loadUpdateUserGroup(PageName.USERGROUP,
                    companyId, Integer.valueOf(id));
            mv.addObject(METHOD, METHOD_UPDATE);
        }
        catch (PlatException e)
        {
            log.error("The method of loadUpdateUserGroup is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }
        catch (Exception e)
        {
            log
                    .error(
                            "The method of loadUpdateUserGroup is system's error.",
                            e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.SYSTEM_ERROR);
            bean.setMessInfo(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 修改用户组信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/updateUserGroup", method = RequestMethod.POST)
    @ResponseBody
    public Object updateUserGroup(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String groupID = request.getParameter("groupID");
            String groupName = request.getParameter("groupName");
            String description = request.getParameter("description");
            if (StringUtil.isEmpty(groupID) || StringUtil.isEmpty(groupName))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }
            AdminBean adminBean = this.getAdmin(request);
            int companyId = adminBean.getCompanyID();

            UserGroupBean bean = new UserGroupBean();
            bean.setGroupId(Integer.valueOf(groupID));
            bean.setGroupName(groupName);
            bean.setDescription(description);
            bean.setCompanyId(companyId);
            bean.setCreateUser(adminBean.getAdminName());

            boolean isSuccess = userGroupService.update(bean);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.USERGROUP_UPDATE_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.USERGROUP_UPDATE_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.USERGROUP_UPDATE_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of updateUserGroup is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        catch (Exception e)
        {
            log.error("The method of updateUserGroup is system's error.", e);
            messBean.setCode(PageCode.SYSTEM_ERROR);
            messBean.setContent(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 加载用户组列表页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/getUserGroupList", method = RequestMethod.GET)
    public ModelAndView getUserGroupList(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String s_PageNum = request.getParameter("pageNum");

            int pageNum = DEFAULT_PAGENUM;
            if (!StringUtil.isEmpty(s_PageNum))
            {
                pageNum = Integer.valueOf(s_PageNum);
            }

            AdminBean adminBean = this.getAdmin(request);
            int companyId = adminBean.getCompanyID();

            PageBean<UserGroupBean> bean = userGroupService.getUserGroups(
                    this.pageRecordNum, pageNum, companyId);
            mv = new ModelAndView(PageName.USERGROUP_LIST);
            mv.addObject("pageBean", bean);
        }
        catch (PlatException e)
        {
            log.error("The method of getUserGroupList is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }
        catch (Exception e)
        {
            log.error("The method of getUserGroupList is system's error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.SYSTEM_ERROR);
            bean.setMessInfo(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 根据ids获取用户组名称
     *
     * @author zhuou
     */
    @RequestMapping(value = "/getUserGroupByIds", method = RequestMethod.POST)
    @ResponseBody
    public Object getUserGroupByIds(HttpServletRequest request)
    {
        String ids = request.getParameter("id");
        MessageBean messBean = new MessageBean();

        if (StringUtil.isEmpty(ids))
        {
            messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
            messBean.setContent(ResourceManager
                    .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
            return messBean;
        }

        try
        {
            List<UserGroupBean> list = userGroupService.getUserGroupByIds(ids);
            StringBuffer sb = new StringBuffer();
            if (null != list && !list.isEmpty())
            {
                for (UserGroupBean item : list)
                {
                    sb.append(item.getGroupName() + "<br/>");
                }
            }
            messBean.setCode(PageCode.COMMON_SUCCESS);
            messBean.setContent(sb.toString());
        }
        catch (PlatException e)
        {
            log.error("The method of getUserGroupByIds is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }

        return messBean;
    }

    public void setUserGroupService(IUserGroupService userGroupService)
    {
        this.userGroupService = userGroupService;
    }
}
