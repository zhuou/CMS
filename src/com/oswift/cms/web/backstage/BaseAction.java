package com.oswift.cms.web.backstage;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;

import com.oswift.cms.entity.AdminBean;
import com.oswift.cms.entity.reception.WebConfig;
import com.oswift.cms.system.cache.WebConfigManager;
import com.oswift.gpm.entity.LoginBean;
import com.oswift.gpm.system.cache.LoginManager;
import com.oswift.gpm.utils.Constant;

public class BaseAction
{
    /**
     * 每页显示记录数
     */
    protected int pageRecordNum = 15;

    /**
     * 页码默认值
     */
    protected static int DEFAULT_PAGENUM = 1;

    /**
     * 方法标志
     */
    protected static String METHOD = "method";

    /**
     * 添加方法标志
     */
    protected static String METHOD_ADD = "add";

    /**
     * 修改方法标志
     */
    protected static String METHOD_UPDATE = "update";

    /**
     *
     * 将查询字符串解码
     *
     * @author zhuou
     * @param keyword
     *            字符串
     * @return 解码字符串
     */
    public String decodeKeyword(String keyword)
    {
        // 解决中文乱码的问题
        if (null != keyword)
        {
            try
            {
                keyword = new String(keyword.getBytes("ISO-8859-1"), "UTF-8");
                keyword = URLDecoder.decode(keyword, "UTF-8");
            }
            catch (UnsupportedEncodingException e)
            {
                // 异常抛弃
            }
        }
        return keyword;
    }

    /**
     *
     * 获取缓存数据 CMS和GPS登录、权限融合方案
     * 1.登录、菜单展示、权限控制由GPS完成
     * 2.adminId、adminName登录信息从GPS缓存中获取
     * 3.companyId根据域名从CMS中获取
     * 存在问题：
     * 用户在A站点登录后，也可以访问B站点后台，导致查看到B站点数据
     * 解决方案（待实现）：
     * 获取companyId不要使用域名从缓存中获取，应该adminId通过查询获取
     *
     * @author zhuou
     * @return SessionBean
     */
    public AdminBean getAdmin(HttpServletRequest request)
    {
        AdminBean bean = new AdminBean();

        // 已经在拦截器中做过null判断，此处不需要在做处理
        Object obj = request.getSession().getAttribute(Constant.SESSION_KEY);

        // 获取登录信息
        LoginBean loginBean = LoginManager.getLoginBean(String.valueOf(obj));
        bean.setAdminId(loginBean.getUserId());
        bean.setAdminName(loginBean.getUserName());

        // 获取站点信息
        // 获取域名或者IP
        String webSitUrl = request.getServerName();
        WebConfig webConfigBean = WebConfigManager.getValue(webSitUrl);
        //bean.setCompanyID(1);
        bean.setCompanyID(webConfigBean.getCompanyId());
        bean.setSiteId(webConfigBean.getSiteId());
        return bean;
    }
}
