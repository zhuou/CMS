package com.oswift.cms.web.backstage;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.entity.MessageBean;
import com.oswift.cms.entity.UploadBean;
import com.oswift.cms.service.IPicService;
import com.oswift.cms.utils.Field;
import com.oswift.cms.utils.PageCode;
import com.oswift.cms.utils.Field.PageName;
import com.oswift.utils.cache.ResourceManager;
import com.oswift.utils.common.ImageUtils;

@Controller
@RequestMapping("/security")
public class UploadAction extends BaseAction
{
    /**
     * PicServiceImpl注入对象
     */
    @Resource
    private IPicService picService;

    /**
     *
     * 加载上传首页默认图片页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadDefaultImg", method = RequestMethod.GET)
    public ModelAndView loadDefaultImg(HttpServletRequest request)
    {
        ModelAndView mv = new ModelAndView(PageName.UPLOADDEFAULTIMG);
        String callback = request.getParameter("callback");
        String iframe = request.getParameter("iframe");
        mv.addObject("callback", callback);
        mv.addObject("iframe", iframe);
        return mv;
    }

    /**
     *
     * 上传图片
     *
     * @author zhuou
     */
    @RequestMapping(value = "/uploadDefaultImg", method = RequestMethod.POST)
    public ModelAndView uploadDefaultImg(HttpServletRequest request)
    {
        ModelAndView mv = new ModelAndView(PageName.UPLOADDEFAULTIMG);
        String sWidth = request.getParameter("imgWidth");
        String sHeight = request.getParameter("imgHight");
        String sIsThumb = request.getParameter("isThumb");
        String callback = request.getParameter("callback");
        String iframe = request.getParameter("iframe");
        MessageBean messBean = new MessageBean();
        try
        {
            UploadBean uploadBean = picService.uploadImg(request, this
                    .getAdmin(request).getCompanyID());

            if (PageCode.COMMON_SUCCESS.equals(uploadBean.getState()))
            {
                if (Field.TRUE.equals(sIsThumb))
                {
                    String path = uploadBean.getAbsolutePath();
                    int iwidth = Integer.valueOf(sWidth);
                    int iheight = Integer.valueOf(sHeight);
                    ImageUtils.scale2(path, path, iheight, iwidth, false);
                }

                // 上传成功
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(uploadBean.getRelativePath());
            }
            else
            {

                // 上传失败获取失败码和失败信息
                messBean.setCode(uploadBean.getState());
                messBean.setContent(uploadBean.getRemark());
            }

            mv.addObject("bean", messBean);
            mv.addObject("callback", callback);
            mv.addObject("iframe", iframe);
        }
        catch (Exception e)
        {
            messBean.setCode(PageCode.UPLOAD_IMG_FAIL);
            messBean.setContent(ResourceManager
                    .getValue(PageCode.UPLOAD_IMG_FAIL)
                    + e.getMessage());
            mv.addObject("bean", messBean);
        }

        return mv;
    }

    /**
     *
     * 加载上传文件页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadUploadFile", method = RequestMethod.GET)
    public ModelAndView loadUploadFile(HttpServletRequest request)
    {
        ModelAndView mv = new ModelAndView(PageName.UPLOADFILE);
        String callback = request.getParameter("callback");
        String iframe = request.getParameter("iframe");
        mv.addObject("callback", callback);
        mv.addObject("iframe", iframe);
        return mv;
    }

    /**
     *
     * 上传文件
     *
     * @author zhuou
     */
    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    public ModelAndView uploadFile(HttpServletRequest request)
    {
        ModelAndView mv = new ModelAndView(PageName.UPLOADFILE);
        String callback = request.getParameter("callback");
        String fileName = request.getParameter("fileName");
        String iframe = request.getParameter("iframe");
        MessageBean messBean = new MessageBean();
        try
        {
            UploadBean uploadBean = picService.uploadFiles(request, this
                    .getAdmin(request).getCompanyID(), fileName);

            if (PageCode.COMMON_SUCCESS.equals(uploadBean.getState()))
            {
                // 上传成功
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(uploadBean.getRelativePath());
            }
            else
            {
                // 上传失败获取失败码和失败信息
                messBean.setCode(uploadBean.getState());
                messBean.setContent(uploadBean.getRemark());
            }

            mv.addObject("bean", messBean);
            mv.addObject("callback", callback);
            mv.addObject("iframe", iframe);
        }
        catch (Exception e)
        {
            messBean.setCode(PageCode.UPLOAD_IMG_FAIL);
            messBean.setContent(ResourceManager
                    .getValue(PageCode.UPLOAD_IMG_FAIL)
                    + e.getMessage());
            mv.addObject("bean", messBean);
        }

        return mv;
    }

    public void setPicService(IPicService picService)
    {
        this.picService = picService;
    }
}
