package com.oswift.cms.web.backstage;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.cms.entity.AdminBean;
import com.oswift.cms.entity.ChannelBean;
import com.oswift.cms.entity.DictionaryBean;
import com.oswift.cms.entity.ErrorBean;
import com.oswift.cms.entity.GuestBookBean;
import com.oswift.cms.entity.GuestBookReplyBean;
import com.oswift.cms.entity.MessageBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.service.IChannelService;
import com.oswift.cms.service.IGuestBookService;
import com.oswift.cms.system.cache.CMSCacheFileds;
import com.oswift.cms.system.cache.DictionaryManager;
import com.oswift.cms.utils.Field;
import com.oswift.cms.utils.PageCode;
import com.oswift.cms.utils.Field.PageName;
import com.oswift.utils.cache.ResourceManager;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.common.TimeUtil;
import com.oswift.utils.exception.PlatException;

@Controller
@RequestMapping("/security")
public class GuestBookAction extends BaseAction
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(GuestBookAction.class);

    /**
     * GuestBookServiceImpl注入对象
     */
    @Resource
    private IGuestBookService guestBookService;

    /**
     * ChannelServiceImpl注入对象
     */
    @Resource
    private IChannelService channelService;

    /**
     *
     * 加载留言新增页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadAddGuestBook", method = RequestMethod.GET)
    public ModelAndView loadAddGuestBook(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            AdminBean adminBean = this.getAdmin(request);
            int companyId = adminBean.getCompanyID();

            mv = guestBookService.loadAddGuestBook(PageName.GUESTBOOK,
                    companyId);
            mv.addObject(METHOD, METHOD_ADD);
        }
        catch (PlatException e)
        {
            log.error("The method of loadAddGuestBook is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }
        catch (Exception e)
        {
            log.error("The method of loadAddGuestBook is system's error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.SYSTEM_ERROR);
            bean.setMessInfo(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 加载留言修改页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadUpdateGuestBook", method = RequestMethod.GET)
    public ModelAndView loadUpdateGuestBook(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String id = request.getParameter("id");
            if (StringUtil.isEmpty(id))
            {
                mv = new ModelAndView(PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setCode(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
                return mv;
            }
            AdminBean adminBean = this.getAdmin(request);
            int companyId = adminBean.getCompanyID();

            mv = guestBookService.loadUpdateGuestBook(PageName.GUESTBOOK,
                    companyId, Integer.valueOf(id));
            mv.addObject(METHOD, METHOD_UPDATE);
        }
        catch (PlatException e)
        {
            log.error("The method of loadUpdateGuestBook is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }
        catch (Exception e)
        {
            log
                    .error(
                            "The method of loadUpdateGuestBook is system's error.",
                            e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.SYSTEM_ERROR);
            bean.setMessInfo(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 修改留言信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/updateGuestBook", method = RequestMethod.POST)
    @ResponseBody
    public Object updateGuestBook(GuestBookBean bean)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            if (bean.getCommonModelId() <= 0)
            {
                messBean.setCode(PageCode.GET_WEBPARAM_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                return messBean;
            }

            if (StringUtil.isEmpty(bean.getTitle())
                    || StringUtil.isEmpty(bean.getGuestContent())
                    || bean.getChannelId() <= 0)
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }

            boolean isSuccess = guestBookService.update(bean);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GUESTBOOK_UPDATE_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.GUESTBOOK_UPDATE_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GUESTBOOK_UPDATE_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of updateGuestBook is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        catch (Exception e)
        {
            log.error("The method of updateGuestBook is system's error.", e);
            messBean.setCode(PageCode.SYSTEM_ERROR);
            messBean.setContent(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 添加留言信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/addGuestBook", method = RequestMethod.POST)
    @ResponseBody
    public Object addGuestBook(HttpServletRequest request, GuestBookBean bean)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            if (StringUtil.isEmpty(bean.getTitle())
                    || StringUtil.isEmpty(bean.getGuestContent())
                    || bean.getChannelId() <= 0)
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }

            AdminBean adminBean = getAdmin(request);
            bean.setCompanyId(adminBean.getCompanyID());
            bean.setTableName(Field.TableName.GUESTBOOK);
            bean.setInputer(adminBean.getAdminName());

            boolean isSuccess = guestBookService.save(bean);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GUESTBOOK_ADD_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.GUESTBOOK_ADD_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GUESTBOOK_ADD_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of addGuestBook is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        catch (Exception e)
        {
            log.error("The method of addGuestBook is system's error.", e);
            messBean.setCode(PageCode.SYSTEM_ERROR);
            messBean.setContent(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 删除文章信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/delGuestBook", method = RequestMethod.POST)
    @ResponseBody
    public Object delGuestBook(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String id = request.getParameter("id");
            if (StringUtil.isEmpty(id))
            {
                messBean.setCode(PageCode.GET_WEBPARAM_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                return messBean;
            }

            boolean isSuccess = guestBookService.delete(Integer.valueOf(id));
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GUESTBOOK_DEL_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.GUESTBOOK_DEL_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GUESTBOOK_DEL_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of delGuestBook is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        catch (Exception e)
        {
            log.error("The method of delGuestBook is system's error.", e);
            messBean.setCode(PageCode.SYSTEM_ERROR);
            messBean.setContent(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 获取公共内容信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/getGuestBookList", method = RequestMethod.GET)
    public ModelAndView getGuestBookList(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String orderField = request.getParameter("orderField");
            String orderType = request.getParameter("orderType");
            String s_Status = request.getParameter("status");
            String s_ChannelId = request.getParameter("channelId");
            String searchType = request.getParameter("searchType");
            String keyword = request.getParameter("keyword");
            String s_PageNum = request.getParameter("pageNum");

            int status = Field.Status.ALL;
            if (!StringUtil.isEmpty(s_Status))
            {
                status = Integer.valueOf(s_Status);
            }
            int channelId = -1;
            if (!StringUtil.isEmpty(s_ChannelId))
            {
                channelId = Integer.valueOf(s_ChannelId);
            }
            int pageNum = DEFAULT_PAGENUM;
            if (!StringUtil.isEmpty(s_PageNum))
            {
                pageNum = Integer.valueOf(s_PageNum);
            }

            // 权限判断
            AdminBean adminBean = this.getAdmin(request);
            int companyId = adminBean.getCompanyID();

            PageBean<GuestBookBean> bean = guestBookService.getCommonModel(
                    orderField, orderType, status, channelId, searchType,
                    keyword, this.pageRecordNum, pageNum,
                    Field.TableName.GUESTBOOK, companyId);

            // 获取包含文章板块的栏目
            DictionaryBean dictionaryBean = DictionaryManager
                    .getValue(CMSCacheFileds.BusinessField.DEFAULT_GUESTBOOK_KEY);
            List<ChannelBean> channelList = channelService.getChannelList2(
                    companyId, dictionaryBean.getFieldValue());

            mv = new ModelAndView(PageName.GUESTBOOKLIST);
            mv.addObject("pageBean", bean);
            mv.addObject("status", status);
            mv.addObject("channels", channelList);
            mv.addObject("orderType", orderField + "_" + orderType);
            mv.addObject("channelId", channelId);
        }
        catch (PlatException e)
        {
            log.error("Get guestBookList info is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            bean.setUrl("getGuestBookList.html");
            mv.addObject("bean", bean);
        }

        return mv;
    }

    /**
     *
     * 留言详情
     *
     * @author zhuou
     */
    @RequestMapping(value = "/guestBookDetail", method = RequestMethod.GET)
    public ModelAndView guestBookDetail(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String id = request.getParameter("id");
            String gId = request.getParameter("gid");
            if (StringUtil.isEmpty(id) && StringUtil.isEmpty(gId))
            {
                mv = new ModelAndView(PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setCode(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
                return mv;
            }

            long commonId = 0L;

            // 根据留言Id回去CommonModelId
            if (!StringUtil.isEmpty(gId))
            {
                GuestBookBean guestBookBean = guestBookService
                        .getGuestBookByGId(Integer.valueOf(gId));
                commonId = guestBookBean.getCommonModelId();
            }
            if (!StringUtil.isEmpty(id))
            {
                commonId = Long.parseLong(id);
            }

            GuestBookBean bean = guestBookService.getOne(commonId);
            mv = new ModelAndView(PageName.GUESTBOOK_DETAIL);
            int replyCount = 0;
            if (null != bean.getList() && !bean.getList().isEmpty())
            {
                replyCount = bean.getList().size();
            }
            mv.addObject("replyCount", replyCount);
            mv.addObject("guestBookBean", bean);
        }
        catch (PlatException e)
        {
            log.error("The method of guestBookDetail is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }
        catch (Exception e)
        {
            log.error("The method of guestBookDetail is system's error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.SYSTEM_ERROR);
            bean.setMessInfo(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 加载留言回复新增页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadAddGuestBookReply", method = RequestMethod.GET)
    public ModelAndView loadAddGuestBookReply(HttpServletRequest request)
    {
        ModelAndView mv = null;
        String id = request.getParameter("id");
        if (StringUtil.isEmpty(id))
        {
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.GET_WEBPARAM_FAIL);
            bean.setMessInfo(ResourceManager
                    .getValue(PageCode.GET_WEBPARAM_FAIL));
            mv.addObject("bean", bean);
            return mv;
        }
        try
        {
            mv = new ModelAndView(PageName.GUESTBOOKREPLY);
            GuestBookBean bean = guestBookService.getOne(Integer.valueOf(id));
            mv.addObject("date", TimeUtil.dateToString(new Date(),
                    TimeUtil.ISO_DATE_NOSECOND_FORMAT));
            mv.addObject("bean", bean);
            mv.addObject(METHOD, METHOD_ADD);
            mv.addObject("replyName", this.getAdmin(request).getAdminName());
        }
        catch (PlatException e)
        {
            log.error("The method of loadAddGuestBookReply is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }

        return mv;
    }

    /**
     *
     * 添加留言回复信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/addGuestBookReply", method = RequestMethod.POST)
    @ResponseBody
    public Object addGuestBookReply(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            GuestBookReplyBean bean = getParamFromPage(request);
            if (bean.getGuestBookId() <= 0
                    || StringUtil.isEmpty(bean.getReplyContent()))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }

            boolean isSuccess = guestBookService.addReply(bean);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GUESTBOOKREPLY_ADD_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.GUESTBOOKREPLY_ADD_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GUESTBOOKREPLY_ADD_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of addGuestBookReply is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        catch (Exception e)
        {
            log.error("The method of addGuestBookReply is system's error.", e);
            messBean.setCode(PageCode.SYSTEM_ERROR);
            messBean.setContent(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 从页面获取参数
     *
     * @author zhuou
     * @param request
     *            HttpServletRequest
     * @return GuestBookReplyBean
     */
    private GuestBookReplyBean getParamFromPage(HttpServletRequest request)
    {
        GuestBookReplyBean bean = new GuestBookReplyBean();
        String sRreplyId = request.getParameter("replyId");
        String sGuestBookId = request.getParameter("guestBookId");
        String sReplyContent = request.getParameter("replyContent");
        String sReplyName = request.getParameter("replyName");
        String sReplyTime = request.getParameter("replyTime");
        if (StringUtil.isEmpty(sGuestBookId))
        {
            bean.setGuestBookId(0L);
        }
        else
        {
            bean.setGuestBookId(Long.parseLong(sGuestBookId));
        }

        if (StringUtil.isEmpty(sRreplyId))
        {
            bean.setReplyId(0L);
        }
        else
        {
            bean.setReplyId(Long.parseLong(sRreplyId));
        }

        bean.setReplyName(sReplyName);
        bean.setReplyContent(sReplyContent);

        Date replyTime = new Date();
        if (!StringUtil.isEmpty(sReplyTime))
        {
            replyTime = TimeUtil.compStrToDate(sReplyTime);
            bean.setReplyTime(replyTime);
        }

        // 获取操作管理员Id
        bean.setAdminId(this.getAdmin(request).getAdminId());

        return bean;
    }

    /**
     *
     * 根据回复Id删除回复
     *
     * @author zhuou
     */
    @RequestMapping(value = "/delGuestBookReply", method = RequestMethod.POST)
    @ResponseBody
    public Object delGuestBookReply(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String id = request.getParameter("id");
            if (StringUtil.isEmpty(id))
            {
                messBean.setCode(PageCode.GET_WEBPARAM_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                return messBean;
            }

            boolean isSuccess = guestBookService.delReply(Long.parseLong(id));
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GUESTBOOKREPLY_DEL_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.GUESTBOOKREPLY_DEL_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GUESTBOOKREPLY_DEL_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of delGuestBookReply is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        catch (Exception e)
        {
            log.error("The method of delGuestBookReply is system's error.", e);
            messBean.setCode(PageCode.SYSTEM_ERROR);
            messBean.setContent(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 加载留言回复修改页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/loadUpdateGuestBookReply", method = RequestMethod.GET)
    public ModelAndView loadUpdateGuestBookReply(HttpServletRequest request)
    {
        ModelAndView mv = null;
        String id = request.getParameter("id");
        String iframe = request.getParameter("iframe");
        if (StringUtil.isEmpty(id))
        {
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.GET_WEBPARAM_FAIL);
            bean.setMessInfo(ResourceManager
                    .getValue(PageCode.GET_WEBPARAM_FAIL));
            mv.addObject("bean", bean);
            return mv;
        }
        try
        {
            mv = new ModelAndView(PageName.GUESTBOOKREPLY);
            GuestBookReplyBean bean = guestBookService.getReplyByRId(Long
                    .parseLong(id));
            mv.addObject("bean", bean);
            mv.addObject(METHOD, METHOD_UPDATE);
            if (StringUtil.isEmpty(iframe))
            {
                mv.addObject("iframe", "");
            }
            else
            {
                mv.addObject("iframe", iframe);
            }
        }
        catch (PlatException e)
        {
            log.error("The method of loadUpdateGuestBookReply is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }

        return mv;
    }

    /**
     *
     * 修改留言回复信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/updateGuestBookReply", method = RequestMethod.POST)
    @ResponseBody
    public Object updateGuestBookReply(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            GuestBookReplyBean bean = getParamFromPage(request);
            if (bean.getGuestBookId() <= 0 || bean.getReplyId() <= 0
                    || StringUtil.isEmpty(bean.getReplyContent()))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }

            boolean isSuccess = guestBookService.updateReply(bean);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GUESTBOOKREPLY_UPDATE_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.GUESTBOOKREPLY_UPDATE_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GUESTBOOKREPLY_UPDATE_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("The method of updateGuestBookReply is error.", e);
            messBean.setCode(e.getExceptionCode());
            messBean.setContent(ResourceManager.getValue(e.getExceptionCode()));
        }
        catch (Exception e)
        {
            log.error("The method of updateGuestBookReply is system's error.",
                    e);
            messBean.setCode(PageCode.SYSTEM_ERROR);
            messBean.setContent(ResourceManager.getValue(PageCode.SYSTEM_ERROR)
                    + e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 获取留言回复详情
     *
     * @author zhuou
     */
    @RequestMapping(value = "/getReplyList", method = RequestMethod.GET)
    public ModelAndView getReplyList(HttpServletRequest request)
    {
        ModelAndView mv = null;
        try
        {
            String searchField = request.getParameter("field");
            String keyword = request.getParameter("keyword");
            if (!StringUtil.isEmpty(keyword))
            {
                keyword = decodeKeyword(keyword);
            }
            else
            {
                keyword = null;
            }

            int pageNum = DEFAULT_PAGENUM;
            if (!StringUtil.isEmpty(request.getParameter("pageNum")))
            {
                pageNum = Integer.valueOf(request.getParameter("pageNum"));
            }

            AdminBean adminBean = this.getAdmin(request);
            int companyId = adminBean.getCompanyID();

            PageBean<GuestBookReplyBean> pageBean = guestBookService
                    .getReplysPageCurrent(searchField, keyword, pageNum,
                            this.pageRecordNum, companyId);
            pageBean.setPageRecordNum(this.pageRecordNum);

            mv = new ModelAndView(PageName.GUESTBOOKREPLYLIST);
            mv.addObject("pageBean", pageBean);
        }
        catch (PlatException e)
        {
            log.error("Get getReplyList is error.", e);
            mv = new ModelAndView(PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(ResourceManager.getValue(e.getExceptionCode()));
            mv.addObject("bean", bean);
        }
        return mv;
    }

    public void setGuestBookService(IGuestBookService guestBookService)
    {
        this.guestBookService = guestBookService;
    }

    public void setChannelService(IChannelService channelService)
    {
        this.channelService = channelService;
    }
}