package com.oswift.cms.web.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.oswift.utils.cache.CacheAccessException;
import com.oswift.utils.cache.manager.CacheManager;

/**
 *
 * 系统启动初始化系统所有参数
 *
 * @author zhuou
 * @version C03 2012-12-22
 * @since OSwift GPM V1.0
 */
public class SysInitServlet extends HttpServlet
{

    /**
     * serialVersion
     */
    private static final long serialVersionUID = 794052937845362218L;

    /**
     * 系统启动初始化数据
     *
     * @throws CacheAccessException
     */
    public void init() throws ServletException
    {
        try
        {
            // 初始化缓存
            CacheManager.init();

            // 实例化spring获取的bean对象
            SpringAppContext.init(this);
        }
        catch (CacheAccessException e)
        {
            throw new ServletException("System init Error ->> "
                    + e.getMessage());
        }
        catch (Exception e)
        {
            throw new ServletException("System init Error ->> "
                    + e.getMessage());
        }
    }
}
