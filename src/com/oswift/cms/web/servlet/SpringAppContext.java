package com.oswift.cms.web.servlet;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * 系统启动时初始化spring
 *
 * @author zhuou
 * @version C03 2012-11-22
 * @since Vanceinfo Telematics V1.0.0
 */
public class SpringAppContext
{
    /**
     * WebAppContext
     */
    private static WebApplicationContext WebAppContext;

    /**
     *
     * 实例化WebApplicationContext
     *
     * @author zhuou
     * @param context
     *            HttpServlet
     */
    public static void init(HttpServlet context) throws Exception
    {
        try
        {
            ServletContext servletContext = context.getServletContext();
            WebAppContext = WebApplicationContextUtils
                    .getWebApplicationContext(servletContext);
        }
        catch (Exception e)
        {
            throw new Exception("Fail to Instantiate WebApplicationContext.");
        }
    }

    /**
     *
     * 获取WebAppContext
     *
     * @author Administrator
     * @return WebApplicationContext
     */
    public static WebApplicationContext getWebAppContext()
    {
        return WebAppContext;
    }
}
