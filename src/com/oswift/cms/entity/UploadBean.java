package com.oswift.cms.entity;

public class UploadBean
{
    /**
     * 文件原始名称
     */
    private String originalName;

    /**
     * 上传保存的相对路径
     */
    private String relativePath;

    /**
     * 上传保存的绝对路径
     */
    private String absolutePath;

    /**
     * 是否成功状态位
     */
    private String state;

    /**
     * 备注
     */
    private String remark;

    public String getOriginalName()
    {
        return originalName;
    }

    public void setOriginalName(String originalName)
    {
        this.originalName = originalName;
    }

    /**
     *
     * 上传保存的相对路径
     *
     * @author zhuou
     * @return 上传保存的相对路径
     */
    public String getRelativePath()
    {
        return relativePath;
    }

    /**
     *
     * 上传保存的相对路径
     *
     * @author zhuou
     * @param 相对路径
     */
    public void setRelativePath(String relativePath)
    {
        this.relativePath = relativePath;
    }

    /**
     *
     * 上传保存的绝对路径
     *
     * @author zhuou
     * @return 上传保存的绝对路径
     */
    public String getAbsolutePath()
    {
        return absolutePath;
    }

    /**
     *
     * 上传保存的绝对路径
     *
     * @author zhuou
     * @param absolutePath
     *            绝对路径
     */
    public void setAbsolutePath(String absolutePath)
    {
        this.absolutePath = absolutePath;
    }

    public String getRemark()
    {
        return remark;
    }

    public void setRemark(String remark)
    {
        this.remark = remark;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }
}
