package com.oswift.cms.entity;

import java.util.Date;

import com.oswift.utils.common.TimeUtil;

/**
 *
 * 留言回复bean类
 *
 * @author zhuou
 * @version C03 2013-1-24
 * @since OSwift GPM V1.0
 */
public class GuestBookReplyBean
{
    /**
     * 留言回复ID
     */
    private long replyId;

    /**
     * 标题
     */
    private String title;

    /**
     * 留言ID
     */
    private long guestBookId;

    /**
     * 管理员ID
     */
    private int adminId;

    /**
     * 回复人名称
     */
    private String replyName;

    /**
     * 管理员回复内容
     */
    private String replyContent;

    /**
     * 管理员回复日期
     */
    private String replyTime;

    /**
     * 创建时间
     */
    private String createTime;

    public long getReplyId()
    {
        return replyId;
    }

    public void setReplyId(long replyId)
    {
        this.replyId = replyId;
    }

    public long getGuestBookId()
    {
        return guestBookId;
    }

    public void setGuestBookId(long guestBookId)
    {
        this.guestBookId = guestBookId;
    }

    public int getAdminId()
    {
        return adminId;
    }

    public void setAdminId(int adminId)
    {
        this.adminId = adminId;
    }

    public String getReplyName()
    {
        return replyName;
    }

    public void setReplyName(String replyName)
    {
        this.replyName = replyName;
    }

    public String getReplyContent()
    {
        return replyContent;
    }

    public void setReplyContent(String replyContent)
    {
        this.replyContent = replyContent;
    }

    public String getReplyTime()
    {
        return replyTime;
    }

    public void setReplyTime(Date replyTime)
    {
        if (null != replyTime)
        {
            this.replyTime = TimeUtil.dateToString(replyTime,
                    TimeUtil.ISO_DATE_NOSECOND_FORMAT);
        }
        else
        {
            this.replyTime = TimeUtil
                    .getCurTimeByFormat(TimeUtil.ISO_DATE_NOSECOND_FORMAT);
        }
    }

    public String getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        if (null != createTime)
        {
            this.createTime = TimeUtil.dateToString(createTime,
                    TimeUtil.ISO_DATE_NOSECOND_FORMAT);
        }
        else
        {
            this.createTime = TimeUtil
                    .getCurTimeByFormat(TimeUtil.ISO_DATE_NOSECOND_FORMAT);
        }
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }
}
