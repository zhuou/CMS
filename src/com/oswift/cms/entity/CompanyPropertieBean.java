package com.oswift.cms.entity;

/**
 * 
 * 获取公司性质bean
 * 
 * @author zhuou
 * @version C03 2012-12-5
 * @since OSwift GPM V1.0
 */
public class CompanyPropertieBean
{
    /**
     * ID
     */
    private int companyPropertieId;

    /**
     * 公司性质
     */
    private String companyPropertie;

    public int getCompanyPropertieId()
    {
        return companyPropertieId;
    }

    public void setCompanyPropertieId(int companyPropertieId)
    {
        this.companyPropertieId = companyPropertieId;
    }

    public String getCompanyPropertie()
    {
        return companyPropertie;
    }

    public void setCompanyPropertie(String companyPropertie)
    {
        this.companyPropertie = companyPropertie;
    }
}
