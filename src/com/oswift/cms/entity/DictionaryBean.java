package com.oswift.cms.entity;

public class DictionaryBean
{
    /**
     * 字段ID
     */
    private int fieldID;

    /**
     * 字段标题
     */
    private String title;

    /**
     * 字段名
     */
    private String fieldName;

    /**
     * 字段值（选项名|是否启用值|默认值）
     */
    private String fieldValue;

    /**
     * 是否缓存
     */
    private boolean isCache;

    /**
     * 是否被选中（此字段主要用在修改操作）
     */
    private boolean checked = false;

    public int getFieldID()
    {
        return fieldID;
    }

    public void setFieldID(int fieldID)
    {
        this.fieldID = fieldID;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getFieldName()
    {
        return fieldName;
    }

    public void setFieldName(String fieldName)
    {
        this.fieldName = fieldName;
    }

    public String getFieldValue()
    {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue)
    {
        this.fieldValue = fieldValue;
    }

    public boolean isCache()
    {
        return isCache;
    }

    public void setCache(boolean isCache)
    {
        this.isCache = isCache;
    }

    public boolean isChecked()
    {
        return checked;
    }

    public void setChecked(boolean checked)
    {
        this.checked = checked;
    }
}
