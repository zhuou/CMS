package com.oswift.cms.entity;

/**
 * 
 * 错误bean
 * 
 * @author zhuou
 * @version C03 2012-12-22
 * @since OSwift GPM V1.0
 */
public class ErrorBean
{
    /**
     * 错误码
     */
    private String code;

    /**
     * 提示信息
     */
    private String messInfo = "";

    /**
     * 返回url地址
     */
    private String url;

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getMessInfo()
    {
        return messInfo;
    }

    public void setMessInfo(String messInfo)
    {
        this.messInfo = messInfo;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }
}
