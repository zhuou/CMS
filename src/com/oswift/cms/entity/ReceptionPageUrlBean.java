package com.oswift.cms.entity;

import java.util.Date;

/**
 *
 * 前台页面url地址配置信息类
 *
 * @author zhuou
 * @version C03 2014-6-13
 * @since OSwift GPM V2.0
 */
public class ReceptionPageUrlBean
{
    /**
     * 站点前台页面Url Id
     */
    private int pageUrlId;

    /**
     * 站点Id
     */
    private int siteId;

    /**
     * 网站名称
     */
    private String siteName;

    /**
     * 前台页面url类型
     */
    private int urlType;

    /**
     * 站点前台页面名称,英文命名，index表示首页
     */
    private String nickName;

    /**
     * JSP页面路径
     */
    private String jspPath;

    /**
     * 描述
     */
    private String intro;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 更新时间
     */
    private Date updateTime;

    public int getPageUrlId()
    {
        return pageUrlId;
    }

    public void setPageUrlId(int pageUrlId)
    {
        this.pageUrlId = pageUrlId;
    }

    public int getSiteId()
    {
        return siteId;
    }

    public void setSiteId(int siteId)
    {
        this.siteId = siteId;
    }

    public int getUrlType()
    {
        return urlType;
    }

    public void setUrlType(int urlType)
    {
        this.urlType = urlType;
    }

    public String getNickName()
    {
        return nickName;
    }

    public void setNickName(String nickName)
    {
        this.nickName = nickName;
    }

    public String getJspPath()
    {
        return jspPath;
    }

    public void setJspPath(String jspPath)
    {
        this.jspPath = jspPath;
    }

    public Date getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        this.createTime = createTime;
    }

    public String getCreateUser()
    {
        return createUser;
    }

    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }

    public Date getUpdateTime()
    {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime)
    {
        this.updateTime = updateTime;
    }

    public String getIntro()
    {
        return intro;
    }

    public void setIntro(String intro)
    {
        this.intro = intro;
    }

    public String getSiteName()
    {
        return siteName;
    }

    public void setSiteName(String siteName)
    {
        this.siteName = siteName;
    }
}
