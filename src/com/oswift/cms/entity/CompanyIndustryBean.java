package com.oswift.cms.entity;

/**
 * 
 * 公司行业信息bean
 * 
 * @author zhuou
 * @version C03 2012-12-5
 * @since OSwift GPM V1.0
 */
public class CompanyIndustryBean
{
    /**
     * ID
     */
    private int companyIndustryId;

    /**
     * 公司行业信息
     */
    private String companyIndustry;

    public int getCompanyIndustryId()
    {
        return companyIndustryId;
    }

    public void setCompanyIndustryId(int companyIndustryId)
    {
        this.companyIndustryId = companyIndustryId;
    }

    public String getCompanyIndustry()
    {
        return companyIndustry;
    }

    public void setCompanyIndustry(String companyIndustry)
    {
        this.companyIndustry = companyIndustry;
    }
}
