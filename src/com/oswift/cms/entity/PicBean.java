package com.oswift.cms.entity;

import java.util.List;

/**
 *
 * 图片URL bean类
 *
 * @author zhuou
 * @version C03 2013-1-17
 * @since OSwift GPM V1.0
 */
public class PicBean extends CommonModelBean
{
    /**
     * 图片ID
     */
    private long picGalleryId;

    /**
     * 作者
     */
    private String picGalleryAuthor;

    /**
     * 来源
     */
    private String copyFrom;

    /**
     * 关键字
     */
    private String keyword;

    /**
     * 描述
     */
    private String picGalleryIntro;

    /**
     * PicUrlBean字符串形式
     */
    private String picUrls;

    /**
     * PicUrlBean列表
     */
    private List<PicUrlBean> list;

    public long getPicGalleryId()
    {
        return picGalleryId;
    }

    public void setPicGalleryId(long picGalleryId)
    {
        this.picGalleryId = picGalleryId;
    }

    public String getPicGalleryAuthor()
    {
        return picGalleryAuthor;
    }

    public void setPicGalleryAuthor(String picGalleryAuthor)
    {
        this.picGalleryAuthor = picGalleryAuthor;
    }

    public String getCopyFrom()
    {
        return copyFrom;
    }

    public void setCopyFrom(String copyFrom)
    {
        this.copyFrom = copyFrom;
    }

    public String getKeyword()
    {
        return keyword;
    }

    public void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }

    public String getPicGalleryIntro()
    {
        return picGalleryIntro;
    }

    public void setPicGalleryIntro(String picGalleryIntro)
    {
        this.picGalleryIntro = picGalleryIntro;
    }

    public List<PicUrlBean> getList()
    {
        return list;
    }

    public void setList(List<PicUrlBean> list)
    {
        this.list = list;
    }

    public String getPicUrls()
    {
        return picUrls;
    }

    public void setPicUrls(String picUrls)
    {
        this.picUrls = picUrls;
    }
}
