package com.oswift.cms.entity;

import java.util.Date;

public class MemberContacterBean
{
    /**
     * 用户扩展Id
     */
    private long contacterId;

    /**
     * 真实姓名
     */
    private String trueName;

    /**
     * 用户Email
     */
    private String email;

    /**
     * 性别 1男 2女
     */
    private int sex;

    /**
     * 单位名称
     */
    private String company;

    /**
     * 所属部门
     */
    private String department;

    /**
     * 职务
     */
    private String position;

    /**
     * 单位地址
     */
    private String companyAddress;

    /**
     * 相关网址
     */
    private String homepage;

    /**
     * QQ号码
     */
    private String qq;

    /**
     * 办公电话
     */
    private String officePhone;

    /**
     * 住宅电话
     */
    private String homePhone;

    /**
     * 移动电话
     */
    private String mobile;

    /**
     * 省、直辖市
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区、县
     */
    private String district;

    /**
     * 邮政编码
     */
    private String zipCode;

    /**
     * 联系地址
     */
    private String address;

    /**
     * 籍贯
     */
    private String nativePlace;

    /**
     * 民族
     */
    private String nation;

    /**
     * 出生日期
     */
    private Date birthday;

    /**
     * 证件类型
     */
    private int cardType;

    /**
     * 证件号
     */
    private String idCard;

    /**
     * 婚姻状况 1未婚 2已婚
     */
    private int marriage;

    /**
     * 家庭情况
     */
    private String family;

    /**
     * 月收入
     */
    private String income;

    /**
     * 学历
     */
    private String education;

    /**
     * 毕业学校
     */
    private String graduateFrom;

    /**
     * 生活爱好
     */
    private String interestsOfLife;

    /**
     * 文化爱好
     */
    private String interestsOfCulture;

    /**
     * 娱乐休闲爱好
     */
    private String interestsOfAmusement;

    /**
     * 体育爱好
     */
    private String interestsOfSport;

    /**
     * 其他爱好
     */
    private String interestsOfOther;

    /**
     * 附件路径
     */
    private String attachment;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    public long getContacterID()
    {
        return contacterId;
    }

    public void setContacterID(long contacterId)
    {
        this.contacterId = contacterId;
    }

    public String getTrueName()
    {
        return trueName;
    }

    public void setTrueName(String trueName)
    {
        this.trueName = trueName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public int getSex()
    {
        return sex;
    }

    public void setSex(int sex)
    {
        this.sex = sex;
    }

    public String getCompany()
    {
        return company;
    }

    public void setCompany(String company)
    {
        this.company = company;
    }

    public String getDepartment()
    {
        return department;
    }

    public void setDepartment(String department)
    {
        this.department = department;
    }

    public String getPosition()
    {
        return position;
    }

    public void setPosition(String position)
    {
        this.position = position;
    }

    public String getCompanyAddress()
    {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress)
    {
        this.companyAddress = companyAddress;
    }

    public String getHomepage()
    {
        return homepage;
    }

    public void setHomepage(String homepage)
    {
        this.homepage = homepage;
    }

    public String getQq()
    {
        return qq;
    }

    public void setQq(String qq)
    {
        this.qq = qq;
    }

    public String getOfficePhone()
    {
        return officePhone;
    }

    public void setOfficePhone(String officePhone)
    {
        this.officePhone = officePhone;
    }

    public String getHomePhone()
    {
        return homePhone;
    }

    public void setHomePhone(String homePhone)
    {
        this.homePhone = homePhone;
    }

    public String getMobile()
    {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getProvince()
    {
        return province;
    }

    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getDistrict()
    {
        return district;
    }

    public void setDistrict(String district)
    {
        this.district = district;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setZipCode(String zipCode)
    {
        this.zipCode = zipCode;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getNativePlace()
    {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace)
    {
        this.nativePlace = nativePlace;
    }

    public String getNation()
    {
        return nation;
    }

    public void setNation(String nation)
    {
        this.nation = nation;
    }

    public Date getBirthday()
    {
        return birthday;
    }

    public void setBirthday(Date birthday)
    {
        this.birthday = birthday;
    }

    public int getCardType()
    {
        return cardType;
    }

    public void setCardType(int cardType)
    {
        this.cardType = cardType;
    }

    public String getIdCard()
    {
        return idCard;
    }

    public void setIdCard(String idCard)
    {
        this.idCard = idCard;
    }

    public int getMarriage()
    {
        return marriage;
    }

    public void setMarriage(int marriage)
    {
        this.marriage = marriage;
    }

    public String getFamily()
    {
        return family;
    }

    public void setFamily(String family)
    {
        this.family = family;
    }

    public String getIncome()
    {
        return income;
    }

    public void setIncome(String income)
    {
        this.income = income;
    }

    public String getEducation()
    {
        return education;
    }

    public void setEducation(String education)
    {
        this.education = education;
    }

    public String getGraduateFrom()
    {
        return graduateFrom;
    }

    public void setGraduateFrom(String graduateFrom)
    {
        this.graduateFrom = graduateFrom;
    }

    public String getInterestsOfLife()
    {
        return interestsOfLife;
    }

    public void setInterestsOfLife(String interestsOfLife)
    {
        this.interestsOfLife = interestsOfLife;
    }

    public String getInterestsOfCulture()
    {
        return interestsOfCulture;
    }

    public void setInterestsOfCulture(String interestsOfCulture)
    {
        this.interestsOfCulture = interestsOfCulture;
    }

    public String getInterestsOfAmusement()
    {
        return interestsOfAmusement;
    }

    public void setInterestsOfAmusement(String interestsOfAmusement)
    {
        this.interestsOfAmusement = interestsOfAmusement;
    }

    public String getInterestsOfSport()
    {
        return interestsOfSport;
    }

    public void setInterestsOfSport(String interestsOfSport)
    {
        this.interestsOfSport = interestsOfSport;
    }

    public String getInterestsOfOther()
    {
        return interestsOfOther;
    }

    public void setInterestsOfOther(String interestsOfOther)
    {
        this.interestsOfOther = interestsOfOther;
    }

    public Date getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        this.createTime = createTime;
    }

    public Date getUpdateTime()
    {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime)
    {
        this.updateTime = updateTime;
    }

    public String getAttachment()
    {
        return attachment;
    }

    public void setAttachment(String attachment)
    {
        this.attachment = attachment;
    }
}
