package com.oswift.cms.entity;

import java.util.Date;

public class WebConfigBean
{
    /**
     * 站点主键Id
     */
    private int siteId;

    /**
     * 公司ID
     */
    private int companyId;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 网站名称
     */
    private String siteName;

    /**
     * 网站网址
     */
    private String siteUrl;

    /**
     * 网站logo
     */
    private String logoUrl;

    /**
     * 版权
     */
    private String copyright;

    /**
     * 网站关键字
     */
    private String metaKeywords;

    /**
     * 网站描述
     */
    private String metaDescription;

    /**
     * 容许上传图片的格式，以“,”分隔
     */
    private String upLoadImgType;

    /**
     * 容许上传文件的格式，以“,”分隔
     */
    private String uploadFileType;

    /**
     * 网站缩略图宽
     */
    private int imgThumbWidth;

    /**
     * 网站缩略图高
     */
    private int imgThumbHeight;

    /**
     * 配置错误页面地址
     */
    private String errorPagePath;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建者
     */
    private String createUser;

    /**
     * 修改时间
     */
    private Date updateTime;

    public int getSiteId()
    {
        return siteId;
    }

    public void setSiteId(int siteId)
    {
        this.siteId = siteId;
    }

    public int getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(int companyId)
    {
        this.companyId = companyId;
    }

    public String getSiteName()
    {
        return siteName;
    }

    public void setSiteName(String siteName)
    {
        this.siteName = siteName;
    }

    public String getSiteUrl()
    {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl)
    {
        this.siteUrl = siteUrl;
    }

    public String getLogoUrl()
    {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl)
    {
        this.logoUrl = logoUrl;
    }

    public String getCopyright()
    {
        return copyright;
    }

    public void setCopyright(String copyright)
    {
        this.copyright = copyright;
    }

    public String getMetaKeywords()
    {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords)
    {
        this.metaKeywords = metaKeywords;
    }

    public String getMetaDescription()
    {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription)
    {
        this.metaDescription = metaDescription;
    }

    public String getUpLoadImgType()
    {
        return upLoadImgType;
    }

    public void setUpLoadImgType(String upLoadImgType)
    {
        this.upLoadImgType = upLoadImgType;
    }

    public String getUploadFileType()
    {
        return uploadFileType;
    }

    public void setUploadFileType(String uploadFileType)
    {
        this.uploadFileType = uploadFileType;
    }

    public String getErrorPagePath()
    {
        return errorPagePath;
    }

    public void setErrorPagePath(String errorPagePath)
    {
        this.errorPagePath = errorPagePath;
    }

    public Date getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        this.createTime = createTime;
    }

    public String getCreateUser()
    {
        return createUser;
    }

    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }

    public Date getUpdateTime()
    {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime)
    {
        this.updateTime = updateTime;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public int getImgThumbWidth()
    {
        return imgThumbWidth;
    }

    public void setImgThumbWidth(int imgThumbWidth)
    {
        this.imgThumbWidth = imgThumbWidth;
    }

    public int getImgThumbHeight()
    {
        return imgThumbHeight;
    }

    public void setImgThumbHeight(int imgThumbHeight)
    {
        this.imgThumbHeight = imgThumbHeight;
    }
}
