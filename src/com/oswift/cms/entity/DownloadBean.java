package com.oswift.cms.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * 下载baen类
 *
 * @author zhuou
 * @version C03 2013-1-17
 * @since OSwift GPM V1.0
 */
public class DownloadBean extends CommonModelBean
{
    /**
     * 下载ID
     */
    private long downloadId;

    /**
     * 软件版本
     */
    private String fileVersion;

    /**
     * 作者
     */
    private String author;

    /**
     * 软件来源
     */
    private String copyFrom;

    /**
     * 示例地址
     */
    private String demoUrl;

    /**
     * 关键字
     */
    private String keyword;

    /**
     * 软件平台
     */
    private String operatingSystem;

    /**
     * 软件类型
     */
    private String fileType;

    /**
     * 软件语言
     */
    private String fileLanguage;

    /**
     * 授权方式
     */
    private String copyrightType;

    /**
     * 注册地址
     */
    private String regUrl;

    /**
     * 软件介绍
     */
    private String fileIntro;

    /**
     * 软件大小
     */
    private String fileSize;

    /**
     * 下载地址
     */
    private String downloadUrl;

    /**
     * 下载地址list
     */
    private List<String> downloadUrlList;

    /**
     * 解压密码
     */
    private String decompressPassword;

    public long getDownloadId()
    {
        return downloadId;
    }

    public void setDownloadId(long downloadId)
    {
        this.downloadId = downloadId;
    }

    public String getFileVersion()
    {
        return fileVersion;
    }

    public void setFileVersion(String fileVersion)
    {
        this.fileVersion = fileVersion;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public String getCopyFrom()
    {
        return copyFrom;
    }

    public void setCopyFrom(String copyFrom)
    {
        this.copyFrom = copyFrom;
    }

    public String getDemoUrl()
    {
        return demoUrl;
    }

    public void setDemoUrl(String demoUrl)
    {
        this.demoUrl = demoUrl;
    }

    public String getKeyword()
    {
        return keyword;
    }

    public void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }

    public String getOperatingSystem()
    {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem)
    {
        this.operatingSystem = operatingSystem;
    }

    public String getFileType()
    {
        return fileType;
    }

    public void setFileType(String fileType)
    {
        this.fileType = fileType;
    }

    public String getFileLanguage()
    {
        return fileLanguage;
    }

    public void setFileLanguage(String fileLanguage)
    {
        this.fileLanguage = fileLanguage;
    }

    public String getCopyrightType()
    {
        return copyrightType;
    }

    public void setCopyrightType(String copyrightType)
    {
        this.copyrightType = copyrightType;
    }

    public String getRegUrl()
    {
        return regUrl;
    }

    public void setRegUrl(String regUrl)
    {
        this.regUrl = regUrl;
    }

    public String getFileIntro()
    {
        return fileIntro;
    }

    public void setFileIntro(String fileIntro)
    {
        this.fileIntro = fileIntro;
    }

    public String getFileSize()
    {
        return fileSize;
    }

    public void setFileSize(String fileSize)
    {
        this.fileSize = fileSize;
    }

    public String getDownloadUrl()
    {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl)
    {
        this.downloadUrl = downloadUrl;
    }

    public String getDecompressPassword()
    {
        return decompressPassword;
    }

    public void setDecompressPassword(String decompressPassword)
    {
        this.decompressPassword = decompressPassword;
    }

    public List<String> getDownloadUrlList()
    {
        downloadUrlList = new ArrayList<String>();
        if (null != downloadUrl && !"".equals(downloadUrl))
        {
            Pattern pattern = Pattern.compile("\\|\\$\\|");
            String[] array = pattern.split(downloadUrl);
            for (String s : array)
            {
                downloadUrlList.add(s);
            }
        }
        return downloadUrlList;
    }

    public void setDownloadUrlList(List<String> downloadUrlList)
    {
        this.downloadUrlList = downloadUrlList;
    }
}
