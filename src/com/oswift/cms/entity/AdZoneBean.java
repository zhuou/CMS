package com.oswift.cms.entity;

import java.util.Date;

/**
 *
 * 广告位
 *
 * @author zhuou
 * @version C03 2014-2-10
 * @since OSwift GPM V1.0
 */
public class AdZoneBean
{
    /**
     * 广告位ID
     */
    private int zoneId;

    /**
     * 广告位编码，用于前台查询广告位使用
     */
    private String zoneCode;

    /**
     * 公司ID
     */
    private int companyId;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 广告位名称
     */
    private String zoneName;

    /**
     * 广告位描述
     */
    private String zoneIntro;

    /**
     * 广告位状态，false为不显示 true为活动广告位
     */
    private boolean status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建者
     */
    private String createUser;

    public int getZoneId()
    {
        return zoneId;
    }

    public void setZoneId(int zoneId)
    {
        this.zoneId = zoneId;
    }

    public String getZoneCode()
    {
        return zoneCode;
    }

    public void setZoneCode(String zoneCode)
    {
        this.zoneCode = zoneCode;
    }

    public String getZoneName()
    {
        return zoneName;
    }

    public void setZoneName(String zoneName)
    {
        this.zoneName = zoneName;
    }

    public String getZoneIntro()
    {
        return zoneIntro;
    }

    public void setZoneIntro(String zoneIntro)
    {
        this.zoneIntro = zoneIntro;
    }

    public void setStatus(boolean status)
    {
        this.status = status;
    }

    public boolean isStatus()
    {
        return status;
    }

    public Date getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        this.createTime = createTime;
    }

    public String getCreateUser()
    {
        return createUser;
    }

    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }

    public int getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(int companyId)
    {
        this.companyId = companyId;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }
}
