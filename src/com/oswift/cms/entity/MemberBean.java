package com.oswift.cms.entity;

import java.util.Date;

public class MemberBean extends MemberContacterBean
{
    /**
     * 用户Id
     */
    private long userId;

    /**
     * 用户组Id
     */
    private int groupId;

    /**
     * 用户组名
     */
    private String groupName;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户密码
     */
    private String userPassword;

    /**
     * 提示问题
     */
    private String question;

    /**
     * 提示答案
     */
    private String answer;

    /**
     * 注册时间
     */
    private Date regTime;

    /**
     * 登录次数
     */
    private int loginTimes;

    /**
     * 最后登录时间
     */
    private Date lastLoginTime;

    /**
     * 最后登录IP
     */
    private String lastLoginIP;

    /**
     * 上次修改密码的时间
     */
    private Date lastPasswordChangedTime;

    /**
     * 上次被锁定的时间
     */
    private Date lastLockoutTime;

    /**
     * 使用无效密码登录的次数，正确登录后置为0
     */
    private int failedPasswordAttemptCount;

    /**
     * 使用无效答案找回密码的次数
     */
    private int failedPasswordAnswerAttempCount;

    /**
     * 用户状态。0－－正常，1－－锁定，2－－未通过邮件验证，4－－未通过管理员认证 8－－未通过手机验证
     */
    private int status;

    /**
     * 是否允许用户修改密码
     */
    private boolean enableResetPassword;

    /**
     * 用户头像
     */
    private String userFace;

    /**
     * 头像宽度
     */
    private int faceWidth;

    /**
     * 头像高度
     */
    private int faceHeight;

    /**
     * 用户积分
     */
    private int userExp;

    /**
     * 消费的积分数
     */
    private int consumeExp;

    /**
     * 取回密码随机串
     */
    private String getPasswordSid;

    /**
     * 上次取回密码时间
     */
    private Date getPasswordTime;

    public long getUserId()
    {
        return userId;
    }

    public void setUserId(long userId)
    {
        this.userId = userId;
    }

    public int getGroupId()
    {
        return groupId;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public void setGroupId(int groupId)
    {
        this.groupId = groupId;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserPassword()
    {
        return userPassword;
    }

    public void setUserPassword(String userPassword)
    {
        this.userPassword = userPassword;
    }

    public String getQuestion()
    {
        return question;
    }

    public void setQuestion(String question)
    {
        this.question = question;
    }

    public String getAnswer()
    {
        return answer;
    }

    public void setAnswer(String answer)
    {
        this.answer = answer;
    }

    public Date getRegTime()
    {
        return regTime;
    }

    public void setRegTime(Date regTime)
    {
        this.regTime = regTime;
    }

    public int getLoginTimes()
    {
        return loginTimes;
    }

    public void setLoginTimes(int loginTimes)
    {
        this.loginTimes = loginTimes;
    }

    public Date getLastLoginTime()
    {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime)
    {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLastLoginIP()
    {
        return lastLoginIP;
    }

    public void setLastLoginIP(String lastLoginIP)
    {
        this.lastLoginIP = lastLoginIP;
    }

    public Date getLastPasswordChangedTime()
    {
        return lastPasswordChangedTime;
    }

    public void setLastPasswordChangedTime(Date lastPasswordChangedTime)
    {
        this.lastPasswordChangedTime = lastPasswordChangedTime;
    }

    public Date getLastLockoutTime()
    {
        return lastLockoutTime;
    }

    public void setLastLockoutTime(Date lastLockoutTime)
    {
        this.lastLockoutTime = lastLockoutTime;
    }

    public int getFailedPasswordAttemptCount()
    {
        return failedPasswordAttemptCount;
    }

    public void setFailedPasswordAttemptCount(int failedPasswordAttemptCount)
    {
        this.failedPasswordAttemptCount = failedPasswordAttemptCount;
    }

    public int getFailedPasswordAnswerAttempCount()
    {
        return failedPasswordAnswerAttempCount;
    }

    public void setFailedPasswordAnswerAttempCount(
            int failedPasswordAnswerAttempCount)
    {
        this.failedPasswordAnswerAttempCount = failedPasswordAnswerAttempCount;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public boolean isEnableResetPassword()
    {
        return enableResetPassword;
    }

    public void setEnableResetPassword(boolean enableResetPassword)
    {
        this.enableResetPassword = enableResetPassword;
    }

    public String getUserFace()
    {
        return userFace;
    }

    public void setUserFace(String userFace)
    {
        this.userFace = userFace;
    }

    public int getFaceWidth()
    {
        return faceWidth;
    }

    public void setFaceWidth(int faceWidth)
    {
        this.faceWidth = faceWidth;
    }

    public int getFaceHeight()
    {
        return faceHeight;
    }

    public void setFaceHeight(int faceHeight)
    {
        this.faceHeight = faceHeight;
    }

    public int getUserExp()
    {
        return userExp;
    }

    public void setUserExp(int userExp)
    {
        this.userExp = userExp;
    }

    public int getConsumeExp()
    {
        return consumeExp;
    }

    public void setConsumeExp(int consumeExp)
    {
        this.consumeExp = consumeExp;
    }

    public String getGetPasswordSid()
    {
        return getPasswordSid;
    }

    public void setGetPasswordSid(String getPasswordSid)
    {
        this.getPasswordSid = getPasswordSid;
    }

    public Date getGetPasswordTime()
    {
        return getPasswordTime;
    }

    public void setGetPasswordTime(Date getPasswordTime)
    {
        this.getPasswordTime = getPasswordTime;
    }
}
