package com.oswift.cms.entity.reception;

/**
 * 用户实体类，用户登录后，将此对象存储到session中
 *
 * @author zhuou
 * @version C03 2014-9-14
 * @since OSwift GPM V1.0
 */
public class Member
{
    /**
     * 用户Id
     */
    private long userId;

    /**
     * 用户组Id
     */
    private int groupId;

    /**
     * 用户组名
     */
    private String groupName;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户头像
     */
    private String userFace;

    /**
     * 头像宽度
     */
    private int faceWidth;

    /**
     * 头像高度
     */
    private int faceHeight;

    /**
     * 用户积分
     */
    private int userExp;

    /**
     * 消费的积分数
     */
    private int consumeExp;

    public long getUserId()
    {
        return userId;
    }

    public void setUserId(long userId)
    {
        this.userId = userId;
    }

    public int getGroupId()
    {
        return groupId;
    }

    public void setGroupId(int groupId)
    {
        this.groupId = groupId;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserFace()
    {
        return userFace;
    }

    public void setUserFace(String userFace)
    {
        this.userFace = userFace;
    }

    public int getFaceWidth()
    {
        return faceWidth;
    }

    public void setFaceWidth(int faceWidth)
    {
        this.faceWidth = faceWidth;
    }

    public int getFaceHeight()
    {
        return faceHeight;
    }

    public void setFaceHeight(int faceHeight)
    {
        this.faceHeight = faceHeight;
    }

    public int getUserExp()
    {
        return userExp;
    }

    public void setUserExp(int userExp)
    {
        this.userExp = userExp;
    }

    public int getConsumeExp()
    {
        return consumeExp;
    }

    public void setConsumeExp(int consumeExp)
    {
        this.consumeExp = consumeExp;
    }
}
