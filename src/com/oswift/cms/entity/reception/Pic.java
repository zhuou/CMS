package com.oswift.cms.entity.reception;

import java.util.List;

/**
 *
 * 前台图片URL bean类
 *
 * @author zhuou
 * @version C03 2013-8-22
 * @since OSwift GPM V1.0
 */
public class Pic extends CommonModel
{
    /**
     * 图片ID
     */
    private long picGalleryId;

    /**
     * 作者
     */
    private String picGalleryAuthor;

    /**
     * 来源
     */
    private String copyFrom;

    /**
     * 关键字
     */
    private String keyword;

    /**
     * 描述
     */
    private String picGalleryIntro;

    /**
     * PicUrlBean列表
     */
    private List<PicUrl> list;

    /**
     * 上一篇
     */
    private Pic preItem;

    /**
     * 下一篇
     */
    private Pic nextItem;

    public long getPicGalleryId()
    {
        return picGalleryId;
    }

    public void setPicGalleryId(long picGalleryId)
    {
        this.picGalleryId = picGalleryId;
    }

    public String getPicGalleryAuthor()
    {
        return picGalleryAuthor;
    }

    public void setPicGalleryAuthor(String picGalleryAuthor)
    {
        this.picGalleryAuthor = picGalleryAuthor;
    }

    public String getCopyFrom()
    {
        return copyFrom;
    }

    public void setCopyFrom(String copyFrom)
    {
        this.copyFrom = copyFrom;
    }

    public String getKeyword()
    {
        return keyword;
    }

    public void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }

    public String getPicGalleryIntro()
    {
        return picGalleryIntro;
    }

    public void setPicGalleryIntro(String picGalleryIntro)
    {
        this.picGalleryIntro = picGalleryIntro;
    }

    public List<PicUrl> getList()
    {
        return list;
    }

    public void setList(List<PicUrl> list)
    {
        this.list = list;
    }

    public Pic getPreItem()
    {
        return preItem;
    }

    public void setPreItem(Pic preItem)
    {
        this.preItem = preItem;
    }

    public Pic getNextItem()
    {
        return nextItem;
    }

    public void setNextItem(Pic nextItem)
    {
        this.nextItem = nextItem;
    }
}
