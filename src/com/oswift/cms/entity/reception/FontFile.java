package com.oswift.cms.entity.reception;

import java.util.Date;
import java.util.List;

/**
 *
 * 上传文件实体类
 *
 * @author zhuou
 * @version C03 2013-10-28
 * @since OSwift GPM V1.0
 */
public class FontFile
{
    /**
     * 文件ID
     */
    private long filesId;

    /**
     * 公司Id
     */
    private int companyId;

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 文件大小 单位KB
     */
    private int size;

    /**
     * 文件储存路径
     */
    private String path;

    /**
     * 文件类型
     */
    private String fileType;

    /**
     * 1为图片，2为除图片其他文件
     */
    private int status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 用于存储相关联文件信息
     */
    private List<FontFile> list;

    public long getFilesId()
    {
        return filesId;
    }

    public void setFilesId(long filesId)
    {
        this.filesId = filesId;
    }

    public int getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(int companyId)
    {
        this.companyId = companyId;
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public int getSize()
    {
        return size;
    }

    public void setSize(int size)
    {
        this.size = size;
    }

    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        this.path = path;
    }

    public String getFileType()
    {
        return fileType;
    }

    public void setFileType(String fileType)
    {
        this.fileType = fileType;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public Date getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        this.createTime = createTime;
    }

    public List<FontFile> getList()
    {
        return list;
    }

    public void setList(List<FontFile> list)
    {
        this.list = list;
    }
}
