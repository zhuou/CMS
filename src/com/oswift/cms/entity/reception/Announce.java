package com.oswift.cms.entity.reception;

import java.util.Date;

/**
 *
 * 前台公告bean类
 *
 * @author zhuou
 * @version C03 2013-8-22
 * @since OSwift GPM V1.0
 */
public class Announce extends CommonModel
{
    /**
     * 公告ID
     */
    private long announceId;

    /**
     * 关键字
     */
    private String keyword;

    /**
     * 作者
     */
    private String author;

    /**
     * 有效期天数
     */
    private double outTime;

    /**
     * 公告内容
     */
    private String content;

    /**
     * 公告发布时间
     */
    private Date publishTime;

    public long getAnnounceId()
    {
        return announceId;
    }

    public void setAnnounceId(long announceId)
    {
        this.announceId = announceId;
    }

    public String getKeyword()
    {
        return keyword;
    }

    public void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public double getOutTime()
    {
        return outTime;
    }

    public void setOutTime(double outTime)
    {
        this.outTime = outTime;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public Date getPublishTime()
    {
        return publishTime;
    }

    public void setPublishTime(Date publishTime)
    {
        this.publishTime = publishTime;
    }
}
