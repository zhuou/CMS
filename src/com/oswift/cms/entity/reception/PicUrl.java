package com.oswift.cms.entity.reception;

import java.util.Date;

/**
 *
 * 前台图片bean类
 *
 * @author zhuou
 * @version C03 2013-8-22
 * @since OSwift GPM V1.0
 */
public class PicUrl
{
    /**
     * 图片集ID
     */
    private long picGalleryId;

    /**
     * 图片名称
     */
    private String picName;

    /**
     * 图片作者
     */
    private String picAuthor;

    /**
     * 图片URL
     */
    private String picUrl;

    /**
     * 图片简介
     */
    private String picIntro;

    /**
     * 创建时间
     */
    private Date createTime;

    public long getPicGalleryId()
    {
        return picGalleryId;
    }

    public void setPicGalleryId(long picGalleryId)
    {
        this.picGalleryId = picGalleryId;
    }

    public String getPicName()
    {
        return picName;
    }

    public void setPicName(String picName)
    {
        this.picName = picName;
    }

    public String getPicAuthor()
    {
        return picAuthor;
    }

    public void setPicAuthor(String picAuthor)
    {
        this.picAuthor = picAuthor;
    }

    public String getPicUrl()
    {
        return picUrl;
    }

    public void setPicUrl(String picUrl)
    {
        this.picUrl = picUrl;
    }

    public String getPicIntro()
    {
        return picIntro;
    }

    public void setPicIntro(String picIntro)
    {
        this.picIntro = picIntro;
    }

    public Date getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        this.createTime = createTime;
    }
}
