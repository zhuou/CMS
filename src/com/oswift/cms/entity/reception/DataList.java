package com.oswift.cms.entity.reception;

import java.util.List;

/**
 *
 * 主要用于前台列表页面分页
 *
 * @author zhuou
 * @version C03 2013-8-22
 * @since OSwift GPM V1.0
 */
public class DataList
{
    /**
     * 总记录数
     */
    private int totalRecordNum = 0;

    /**
     * 每页记录数
     */
    private int pageRecordNum;

    /**
     * 页数
     */
    private int pageNum = 1;

    /**
     * 当前页码
     */
    private int pageCurrentNum = 1;

    /**
     * 获取的数据
     */
    private List<?> list;

    public int getTotalRecordNum()
    {
        return totalRecordNum;
    }

    public void setTotalRecordNum(int totalRecordNum)
    {
        this.totalRecordNum = totalRecordNum;
    }

    public int getPageRecordNum()
    {
        return pageRecordNum;
    }

    public void setPageRecordNum(int pageRecordNum)
    {
        this.pageRecordNum = pageRecordNum;
    }

    public int getPageNum()
    {
        return pageNum;
    }

    public void setPageNum(int pageNum)
    {
        this.pageNum = pageNum;
    }

    public int getPageCurrentNum()
    {
        return pageCurrentNum;
    }

    public void setPageCurrentNum(int pageCurrentNum)
    {
        this.pageCurrentNum = pageCurrentNum;
    }

    public List<?> getList()
    {
        return list;
    }

    public void setList(List<?> list)
    {
        this.list = list;
    }
}
