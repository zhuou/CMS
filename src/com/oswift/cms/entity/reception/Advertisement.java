package com.oswift.cms.entity.reception;

/**
 *
 * 广告表
 *
 * @author zhuou
 * @version C03 2014-2-10
 * @since OSwift GPM V1.0
 */
public class Advertisement
{
    /**
     * 广告ID
     */
    private int adId;

    /**
     * 广告名称
     */
    private String adName;

    /**
     * 广告类型
     */
    private int adType;

    /**
     * 图片路径
     */
    private String imgUrl;

    /**
     * 图片宽
     */
    private int imgWidth;

    /**
     * 图片高
     */
    private int imgHeight;

    /**
     * 广告内容描述
     */
    private String adIntro;

    /**
     * 链接地址
     */
    private String linkUrl;

    /**
     * 链接alt提示信息
     */
    private String linkAlt;

    /**
     * 排序
     */
    private int sort;

    /**
     * 广告项目的权重
     */
    private int priority;

    public int getAdId()
    {
        return adId;
    }

    public void setAdId(int adId)
    {
        this.adId = adId;
    }

    public String getAdName()
    {
        return adName;
    }

    public void setAdName(String adName)
    {
        this.adName = adName;
    }

    public int getAdType()
    {
        return adType;
    }

    public void setAdType(int adType)
    {
        this.adType = adType;
    }

    public String getImgUrl()
    {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl)
    {
        this.imgUrl = imgUrl;
    }

    public int getImgWidth()
    {
        return imgWidth;
    }

    public void setImgWidth(int imgWidth)
    {
        this.imgWidth = imgWidth;
    }

    public int getImgHeight()
    {
        return imgHeight;
    }

    public void setImgHeight(int imgHeight)
    {
        this.imgHeight = imgHeight;
    }

    public String getAdIntro()
    {
        return adIntro;
    }

    public void setAdIntro(String adIntro)
    {
        this.adIntro = adIntro;
    }

    public String getLinkUrl()
    {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl)
    {
        this.linkUrl = linkUrl;
    }

    public String getLinkAlt()
    {
        return linkAlt;
    }

    public void setLinkAlt(String linkAlt)
    {
        this.linkAlt = linkAlt;
    }

    public int getSort()
    {
        return sort;
    }

    public void setSort(int sort)
    {
        this.sort = sort;
    }

    public int getPriority()
    {
        return priority;
    }

    public void setPriority(int priority)
    {
        this.priority = priority;
    }
}
