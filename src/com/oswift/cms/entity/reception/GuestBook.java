package com.oswift.cms.entity.reception;

import java.util.List;

/**
 *
 * 前台留言bean类
 *
 * @author zhuou
 * @version C03 2013-8-22
 * @since OSwift GPM V1.0
 */
public class GuestBook extends CommonModel
{
    /**
     * 留言ID
     */
    private long guestBookId;

    /**
     * 用户ID
     */
    private long userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * Email
     */
    private String guestEmail;

    /**
     * 手机号码
     */
    private String phoneNumber;

    /**
     * 用户头像
     */
    private String guestFace;

    /**
     * QQ
     */
    private String guestOicq;

    /**
     * 是否隐藏
     */
    private boolean guestIsPrivate;

    /**
     * 留言内容
     */
    private String guestContent;

    /**
     * 回复数
     */
    private int replyCount;

    /**
     * 留言回复
     */
    private List<GuestBookReply> replyList;

    public long getGuestBookId()
    {
        return guestBookId;
    }

    public void setGuestBookId(long guestBookId)
    {
        this.guestBookId = guestBookId;
    }

    public long getUserId()
    {
        return userId;
    }

    public void setUserId(long userId)
    {
        this.userId = userId;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public boolean isGuestIsPrivate()
    {
        return guestIsPrivate;
    }

    public void setGuestIsPrivate(boolean guestIsPrivate)
    {
        this.guestIsPrivate = guestIsPrivate;
    }

    public String getGuestContent()
    {
        return guestContent;
    }

    public void setGuestContent(String guestContent)
    {
        this.guestContent = guestContent;
    }

    public String getGuestEmail()
    {
        return guestEmail;
    }

    public void setGuestEmail(String guestEmail)
    {
        this.guestEmail = guestEmail;
    }

    public String getGuestFace()
    {
        return guestFace;
    }

    public void setGuestFace(String guestFace)
    {
        this.guestFace = guestFace;
    }

    public String getGuestOicq()
    {
        return guestOicq;
    }

    public void setGuestOicq(String guestOicq)
    {
        this.guestOicq = guestOicq;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public int getReplyCount()
    {
        return replyCount;
    }

    public void setReplyCount(int replyCount)
    {
        this.replyCount = replyCount;
    }

    public List<GuestBookReply> getReplyList()
    {
        return replyList;
    }

    public void setReplyList(List<GuestBookReply> replyList)
    {
        this.replyList = replyList;
    }
}
