package com.oswift.cms.entity.reception;

public class MemberLoginInfo
{
    /**
     * true 登录，false 未登录
     */
    public boolean loginFlag = false;

    /**
     * 登录用户信息
     */
    public Member member;

    public boolean isLoginFlag()
    {
        return loginFlag;
    }

    public void setLoginFlag(boolean loginFlag)
    {
        this.loginFlag = loginFlag;
    }

    public Member getMember()
    {
        return member;
    }

    public void setMember(Member member)
    {
        this.member = member;
    }
}
