package com.oswift.cms.entity.reception;

public class WebConfig
{
    /**
     * 公司ID
     */
    private int companyId;

    /**
     * 站点Id
     */
    private int siteId;

    /**
     * 网站名称
     */
    private String siteName;

    /**
     * 公司简介
     */
    private String companyIntr;

    /**
     * 网站网址
     */
    private String siteUrl;

    /**
     * 网站logo
     */
    private String logoUrl;

    /**
     * 版权
     */
    private String copyright;

    /**
     * 网站关键字
     */
    private String metaKeywords;

    /**
     * 网站描述
     */
    private String metaDescription;

    /**
     * 配置错误页面地址
     */
    private String errorPagePath;

    public int getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(int companyId)
    {
        this.companyId = companyId;
    }

    public int getSiteId()
    {
        return siteId;
    }

    public void setSiteId(int siteId)
    {
        this.siteId = siteId;
    }

    public String getSiteName()
    {
        return siteName;
    }

    public void setSiteName(String siteName)
    {
        this.siteName = siteName;
    }

    public String getSiteUrl()
    {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl)
    {
        this.siteUrl = siteUrl;
    }

    public String getLogoUrl()
    {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl)
    {
        this.logoUrl = logoUrl;
    }

    public String getCopyright()
    {
        return copyright;
    }

    public void setCopyright(String copyright)
    {
        this.copyright = copyright;
    }

    public String getMetaKeywords()
    {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords)
    {
        this.metaKeywords = metaKeywords;
    }

    public String getMetaDescription()
    {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription)
    {
        this.metaDescription = metaDescription;
    }

    public String getErrorPagePath()
    {
        return errorPagePath;
    }

    public void setErrorPagePath(String errorPagePath)
    {
        this.errorPagePath = errorPagePath;
    }

    public String getCompanyIntr()
    {
        return companyIntr;
    }

    public void setCompanyIntr(String companyIntr)
    {
        this.companyIntr = companyIntr;
    }
}
