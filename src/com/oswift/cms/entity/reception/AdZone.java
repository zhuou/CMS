package com.oswift.cms.entity.reception;

import java.util.List;

/**
 *
 * 广告位
 *
 * @author zhuou
 * @version C03 2014-2-10
 * @since OSwift GPM V1.0
 */
public class AdZone
{
    /**
     * 广告位ID
     */
    private int zoneId;

    /**
     * 广告位名称
     */
    private String zoneName;

    /**
     * 广告位描述
     */
    private String zoneIntro;

    /**
     * 广告数目,默认为0
     */
    private int count = 0;

    /**
     * 广告列表
     */
    private List<Advertisement> ads;

    public int getZoneId()
    {
        return zoneId;
    }

    public void setZoneId(int zoneId)
    {
        this.zoneId = zoneId;
    }

    public String getZoneName()
    {
        return zoneName;
    }

    public void setZoneName(String zoneName)
    {
        this.zoneName = zoneName;
    }

    public String getZoneIntro()
    {
        return zoneIntro;
    }

    public void setZoneIntro(String zoneIntro)
    {
        this.zoneIntro = zoneIntro;
    }

    public List<Advertisement> getAds()
    {
        return ads;
    }

    public void setAds(List<Advertisement> ads)
    {
        this.ads = ads;
    }

    public int getCount()
    {
        return count;
    }

    public void setCount(int count)
    {
        this.count = count;
    }
}
