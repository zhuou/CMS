package com.oswift.cms.entity.reception;

import java.util.Date;

/**
 *
 * 留言回复bean类
 *
 * @author zhuou
 * @version C03 2013-4-23
 * @since OSwift GPM V1.0
 */
public class GuestBookReply
{
    /**
     * 留言回复ID
     */
    private long replyId;

    /**
     * 留言ID
     */
    private long guestBookId;

    /**
     * 回复人ID
     */
    private int adminId;

    /**
     * 回复人名称
     */
    private String replyName;

    /**
     * 管理员回复内容
     */
    private String replyContent;

    /**
     * 管理员回复日期
     */
    private Date replyTime;

    /**
     * 创建时间
     */
    private Date createTime;

    public long getReplyId()
    {
        return replyId;
    }

    public void setReplyId(long replyId)
    {
        this.replyId = replyId;
    }

    public int getAdminId()
    {
        return adminId;
    }

    public void setAdminId(int adminId)
    {
        this.adminId = adminId;
    }

    public String getReplyName()
    {
        return replyName;
    }

    public void setReplyName(String replyName)
    {
        this.replyName = replyName;
    }

    public String getReplyContent()
    {
        return replyContent;
    }

    public void setReplyContent(String replyContent)
    {
        this.replyContent = replyContent;
    }

    public Date getReplyTime()
    {
        return replyTime;
    }

    public void setReplyTime(Date replyTime)
    {
        this.replyTime = replyTime;
    }

    public Date getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        this.createTime = createTime;
    }

    public long getGuestBookId()
    {
        return guestBookId;
    }

    public void setGuestBookId(long guestBookId)
    {
        this.guestBookId = guestBookId;
    }
}
