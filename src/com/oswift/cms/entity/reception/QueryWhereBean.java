package com.oswift.cms.entity.reception;

public class QueryWhereBean
{
    /**
     * 查询条件
     */
    private String strWhere = "";

    /**
     * 排序
     */
    private String orderColumn;

    /**
     * 升序还是降序
     */
    private boolean orderType;

    public String getStrWhere()
    {
        return strWhere;
    }

    public void setStrWhere(String strWhere)
    {
        this.strWhere = strWhere;
    }

    public String getOrderColumn()
    {
        return orderColumn;
    }

    public void setOrderColumn(String orderColumn)
    {
        this.orderColumn = orderColumn;
    }

    public boolean isOrderType()
    {
        return orderType;
    }

    public void setOrderType(boolean orderType)
    {
        this.orderType = orderType;
    }
}
