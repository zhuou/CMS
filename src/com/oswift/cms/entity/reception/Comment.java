package com.oswift.cms.entity.reception;

import java.util.Date;

/**
 *
 * 评论bean类
 *
 * @author zhuou
 * @version C03 2013-1-24
 * @since OSwift GPM V1.0
 */
public class Comment
{
    /**
     * 评论ID
     */
    private long commentId;

    /**
     * 内容全局ID
     */
    private long commonModelId;

    /**
     * 公司Id
     */
    private int companyId;

    /**
     * 用户ID
     */
    private long userId;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 评论标题
     */
    private String commentTitle;

    /**
     * 评论内容
     */
    private String commentContent;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 当前评论的支持数
     */
    private int agree;

    /**
     * 当前评论的反对数
     */
    private int oppose;

    /**
     * 当前评论的中立数
     */
    private int neutral;

    /**
     * IP
     */
    private String ip;

    /**
     * 是否精华
     */
    private boolean isElite;

    /**
     * 评论类别 1-网站用户2-手机用户
     */
    private int commentType;

    public long getCommentId()
    {
        return commentId;
    }

    public void setCommentId(long commentId)
    {
        this.commentId = commentId;
    }

    public long getCommonModelId()
    {
        return commonModelId;
    }

    public void setCommonModelId(long commonModelId)
    {
        this.commonModelId = commonModelId;
    }

    public long getUserId()
    {
        return userId;
    }

    public void setUserId(long userId)
    {
        this.userId = userId;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getCommentTitle()
    {
        return commentTitle;
    }

    public void setCommentTitle(String commentTitle)
    {
        this.commentTitle = commentTitle;
    }

    public String getCommentContent()
    {
        return commentContent;
    }

    public void setCommentContent(String commentContent)
    {
        this.commentContent = commentContent;
    }

    public Date getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        this.createTime = createTime;
    }

    public int getAgree()
    {
        return agree;
    }

    public void setAgree(int agree)
    {
        this.agree = agree;
    }

    public int getOppose()
    {
        return oppose;
    }

    public void setOppose(int oppose)
    {
        this.oppose = oppose;
    }

    public int getNeutral()
    {
        return neutral;
    }

    public void setNeutral(int neutral)
    {
        this.neutral = neutral;
    }

    public String getIp()
    {
        return ip;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public boolean getIsElite()
    {
        return isElite;
    }

    public void setIsElite(boolean isElite)
    {
        this.isElite = isElite;
    }

    public int getCommentType()
    {
        return commentType;
    }

    public void setCommentType(int commentType)
    {
        this.commentType = commentType;
    }

    public int getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(int companyId)
    {
        this.companyId = companyId;
    }
}
