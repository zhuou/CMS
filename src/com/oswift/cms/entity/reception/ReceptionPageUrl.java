package com.oswift.cms.entity.reception;


/**
 *
 * 前台页面url地址配置信息类
 *
 * @author zhuou
 * @version C03 2014-6-13
 * @since OSwift GPM V2.0
 */
public class ReceptionPageUrl
{
    /**
     * 站点前台页面Url Id
     */
    private int pageUrlId;

    /**
     * 站点Id
     */
    private int siteId;

    /**
     * 前台页面url类型
     */
    private int urlType;

    /**
     * 站点前台页面名称,英文命名，index表示首页
     */
    private String nickName;

    /**
     * JSP页面路径
     */
    private String jspPath;

    public int getPageUrlId()
    {
        return pageUrlId;
    }

    public void setPageUrlId(int pageUrlId)
    {
        this.pageUrlId = pageUrlId;
    }

    public int getSiteId()
    {
        return siteId;
    }

    public void setSiteId(int siteId)
    {
        this.siteId = siteId;
    }

    public int getUrlType()
    {
        return urlType;
    }

    public void setUrlType(int urlType)
    {
        this.urlType = urlType;
    }

    public String getNickName()
    {
        return nickName;
    }

    public void setNickName(String nickName)
    {
        this.nickName = nickName;
    }

    public String getJspPath()
    {
        return jspPath;
    }

    public void setJspPath(String jspPath)
    {
        this.jspPath = jspPath;
    }
}
