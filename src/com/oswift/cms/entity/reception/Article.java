package com.oswift.cms.entity.reception;

/**
 *
 * 前台文章bean类
 *
 * @author zhuou
 * @version C03 2013-8-22
 * @since OSwift GPM V1.0
 */
public class Article extends CommonModel
{
    /**
     * 文章ID
     */
    private long articleId;

    /**
     * 完整标题
     */
    private String titleIntact;

    /**
     * 副标题
     */
    private String subheading;

    /**
     * 作者
     */
    private String author;

    /**
     * 来源
     */
    private String copyFrom;

    /**
     * 关键字
     */
    private String keyWord;

    /**
     * 焦点新闻图片
     */
    private String focusImg;

    /**
     * 头条新闻图片
     */
    private String bigNewsImg;

    /**
     * 滚动图片
     */
    private String marqueeImg;

    /**
     * 描述
     */
    private String intro;

    /**
     * 内容
     */
    private String content;

    /**
     * 内容中有多少张图片，默认值为0
     */
    private int contentImgNum = 0;

    /**
     * 当前点"赞"数
     */
    private int praise;

    /**
     * 当前点"踩"数
     */
    private int tread;

    /**
     * 上一篇文章
     */
    private Article preItem;

    /**
     * 下一篇文章
     */
    private Article nextItem;

    public long getArticleId()
    {
        return articleId;
    }

    public void setArticleId(long articleId)
    {
        this.articleId = articleId;
    }

    public String getTitleIntact()
    {
        return titleIntact;
    }

    public void setTitleIntact(String titleIntact)
    {
        this.titleIntact = titleIntact;
    }

    public String getSubheading()
    {
        return subheading;
    }

    public void setSubheading(String subheading)
    {
        this.subheading = subheading;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public String getCopyFrom()
    {
        return copyFrom;
    }

    public void setCopyFrom(String copyFrom)
    {
        this.copyFrom = copyFrom;
    }

    public String getKeyWord()
    {
        return keyWord;
    }

    public void setKeyWord(String keyWord)
    {
        this.keyWord = keyWord;
    }

    public String getIntro()
    {
        return intro;
    }

    public void setIntro(String intro)
    {
        this.intro = intro;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public int getContentImgNum()
    {
        return contentImgNum;
    }

    public void setContentImgNum(int contentImgNum)
    {
        this.contentImgNum = contentImgNum;
    }

    public String getFocusImg()
    {
        return focusImg;
    }

    public void setFocusImg(String focusImg)
    {
        this.focusImg = focusImg;
    }

    public String getBigNewsImg()
    {
        return bigNewsImg;
    }

    public void setBigNewsImg(String bigNewsImg)
    {
        this.bigNewsImg = bigNewsImg;
    }

    public String getMarqueeImg()
    {
        return marqueeImg;
    }

    public void setMarqueeImg(String marqueeImg)
    {
        this.marqueeImg = marqueeImg;
    }

    public Article getPreItem()
    {
        return preItem;
    }

    public void setPreItem(Article preItem)
    {
        this.preItem = preItem;
    }

    public Article getNextItem()
    {
        return nextItem;
    }

    public void setNextItem(Article nextItem)
    {
        this.nextItem = nextItem;
    }

    public int getPraise()
    {
        return praise;
    }

    public void setPraise(int praise)
    {
        this.praise = praise;
    }

    public int getTread()
    {
        return tread;
    }

    public void setTread(int tread)
    {
        this.tread = tread;
    }
}
