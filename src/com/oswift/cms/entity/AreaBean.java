package com.oswift.cms.entity;

/**
 * 
 * 地区bean
 * 
 * @author zhuou
 * @version C03 2012-12-22
 * @since OSwift GPM V1.0
 */
public class AreaBean
{
    /**
     * id
     */
    private int id;

    /**
     * 地区名称
     */
    private String name;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

}
