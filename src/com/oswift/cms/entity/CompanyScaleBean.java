package com.oswift.cms.entity;

/**
 * 
 * 公司规模bean
 * 
 * @author zhuou
 * @version C03 2012-12-5
 * @since OSwift GPM V1.0
 */
public class CompanyScaleBean
{
    /**
     * ID
     */
    private int companyScaleId;

    /**
     * 公司规模
     */
    private String companyScale;

    public int getCompanyScaleId()
    {
        return companyScaleId;
    }

    public void setCompanyScaleId(int companyScaleId)
    {
        this.companyScaleId = companyScaleId;
    }

    public String getCompanyScale()
    {
        return companyScale;
    }

    public void setCompanyScale(String companyScale)
    {
        this.companyScale = companyScale;
    }
}
