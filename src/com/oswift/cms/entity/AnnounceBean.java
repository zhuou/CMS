package com.oswift.cms.entity;

import java.util.Date;

import com.oswift.utils.common.TimeUtil;

/**
 *
 * 公告bean类
 *
 * @author zhuou
 * @version C03 2013-1-24
 * @since OSwift GPM V1.0
 */
public class AnnounceBean extends CommonModelBean
{
    /**
     * 公告ID
     */
    private long announceId;

    /**
     * 关键字
     */
    private String keyword;

    /**
     * 作者
     */
    private String author;

    /**
     * 有效期天数
     */
    private double outTime;

    /**
     * 公告内容
     */
    private String content;

    /**
     * 公告发布时间
     */
    private String publishTime;

    public long getAnnounceId()
    {
        return announceId;
    }

    public void setAnnounceId(long announceId)
    {
        this.announceId = announceId;
    }

    public String getKeyword()
    {
        return keyword;
    }

    public void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public double getOutTime()
    {
        return outTime;
    }

    public void setOutTime(double outTime)
    {
        this.outTime = outTime;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public String getPublishTime()
    {
        return publishTime;
    }

    public void setPublishTime(String publishTime)
    {
        if (null != publishTime && !"".equals(publishTime))
        {
            Date date = TimeUtil.stringToDate(publishTime,
                    TimeUtil.ISO_DATE_NOSECOND_FORMAT);
            if (null != date)
            {
                this.publishTime = TimeUtil.dateToString(date,
                        TimeUtil.ISO_DATE_NOSECOND_FORMAT);
            }
            else
            {
                this.publishTime = "";
            }
        }
        else
        {
            this.publishTime = "";
        }
    }
}
