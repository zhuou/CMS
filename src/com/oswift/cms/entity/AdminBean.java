package com.oswift.cms.entity;

/**
 *
 * 用于Session缓存的数据
 *
 * @author zhuou
 * @version C03 2013-1-17
 * @since OSwift GPM V1.0
 */
public class AdminBean
{
    /**
     * 用户ID
     */
    private int adminId;

    /**
     * 站点id
     */
    private int siteId;

    /**
     * 用户名称
     */
    private String adminName;

    /**
     * 公司ID
     */
    private int companyID;

    public int getAdminId()
    {
        return adminId;
    }

    public void setAdminId(int adminId)
    {
        this.adminId = adminId;
    }

    public int getSiteId()
    {
        return siteId;
    }

    public void setSiteId(int siteId)
    {
        this.siteId = siteId;
    }

    public String getAdminName()
    {
        return adminName;
    }

    public void setAdminName(String adminName)
    {
        this.adminName = adminName;
    }

    public int getCompanyID()
    {
        return companyID;
    }

    public void setCompanyID(int companyID)
    {
        this.companyID = companyID;
    }
}
