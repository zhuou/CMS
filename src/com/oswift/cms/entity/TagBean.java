package com.oswift.cms.entity;

import java.util.Date;

/**
 *
 * 标签实体类
 *
 * @author zhuou
 * @version C03 2014-5-30
 * @since OSwift GPM V1.0
 */
public class TagBean
{
    /**
     * 标签ID
     */
    private int tagid;

    /**
     * 公司ID
     */
    private int companyId;

    /**
     * 标签名称
     */
    private String tagName;

    /**
     * 标签被使用次数
     */
    private int usedNum;

    /**
     * 标签被点击次数
     */
    private int tagClick;

    /**
     * 创建时间
     */
    private Date createTime;

    public int getTagid()
    {
        return tagid;
    }

    public void setTagid(int tagid)
    {
        this.tagid = tagid;
    }

    public int getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(int companyId)
    {
        this.companyId = companyId;
    }

    public String getTagName()
    {
        return tagName;
    }

    public void setTagName(String tagName)
    {
        this.tagName = tagName;
    }

    public int getUsedNum()
    {
        return usedNum;
    }

    public void setUsedNum(int usedNum)
    {
        this.usedNum = usedNum;
    }

    public int getTagClick()
    {
        return tagClick;
    }

    public void setTagClick(int tagClick)
    {
        this.tagClick = tagClick;
    }

    public Date getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        this.createTime = createTime;
    }
}
