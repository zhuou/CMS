package com.oswift.cms.entity;

import java.util.Date;

import com.oswift.utils.common.TimeUtil;

/**
 *
 * 内容公共类
 *
 * @author zhuou
 * @version C03 2013-1-10
 * @since OSwift GPM V1.0
 */
public class CommonModelBean
{
    /**
     * 内容全局ID
     */
    protected long commonModelId;

    /**
     * 栏目ID
     */
    protected int channelId;

    /**
     * 栏目名称
     */
    protected String channelName;

    /**
     * 公司ID
     */
    protected int companyId;

    /**
     * 相应表的记录ID
     */
    protected long itemId;

    /**
     * 模型表名
     */
    protected String tableName;

    /**
     * 标题
     */
    protected String title;

    /**
     * 标题前缀 [图文] [组图] [推荐] [注意]
     */
    protected String titlePrefix;

    /**
     * 录入者
     */
    protected String inputer;

    /**
     * 总点击数
     */
    protected int hits;

    /**
     * 日点击数
     */
    protected int dayHits;

    /**
     * 周点击数
     */
    protected int weekHits;

    /**
     * 月点击数
     */
    protected int monthHits;

    /**
     * 链接打开方式,即是否在另一个窗口打开。false在本窗口打开，true在另一个窗口打开
     */
    protected boolean openType;

    /**
     * -3为删除，-2为退稿，-1为草稿，0为待审核，99为终审通过，其它为自定义
     */
    protected int status;

    /**
     * 推荐级别
     */
    protected int eliteLevel;

    /**
     * 优先级别
     */
    protected int priority;

    /**
     * 评论总数
     */
    protected int commentCount;

    /**
     * 更新时间
     */
    protected String updateTime;

    /**
     * 创建时间
     */
    protected String createTime;

    /**
     * 审核通过时间
     */
    protected String passedTime;

    /**
     * 上次点击时间
     */
    protected String lastHitTime;

    /**
     * 默认封面图片
     */
    protected String defaultPicUrl;

    /**
     * 列表显示时是否在标题旁显示“评论”链接 tinyint
     */
    protected boolean showCommentLink;

    /**
     * 是否生成静态页面
     */
    protected boolean isCreateHtml;

    /**
     * 静态页URL
     */
    protected String htmlUrl;

    public long getCommonModelId()
    {
        return commonModelId;
    }

    public void setCommonModelId(long commonModelId)
    {
        this.commonModelId = commonModelId;
    }

    public int getChannelId()
    {
        return channelId;
    }

    public void setChannelId(int channelId)
    {
        this.channelId = channelId;
    }

    public String getChannelName()
    {
        return channelName;
    }

    public void setChannelName(String channelName)
    {
        this.channelName = channelName;
    }

    public int getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(int companyId)
    {
        this.companyId = companyId;
    }

    public long getItemId()
    {
        return itemId;
    }

    public void setItemId(long itemId)
    {
        this.itemId = itemId;
    }

    public String getTableName()
    {
        return tableName;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitlePrefix()
    {
        return titlePrefix;
    }

    public void setTitlePrefix(String titlePrefix)
    {
        this.titlePrefix = titlePrefix;
    }

    public String getInputer()
    {
        return inputer;
    }

    public void setInputer(String inputer)
    {
        this.inputer = inputer;
    }

    public int getHits()
    {
        return hits;
    }

    public void setHits(int hits)
    {
        this.hits = hits;
    }

    public int getDayHits()
    {
        return dayHits;
    }

    public void setDayHits(int dayHits)
    {
        this.dayHits = dayHits;
    }

    public int getWeekHits()
    {
        return weekHits;
    }

    public void setWeekHits(int weekHits)
    {
        this.weekHits = weekHits;
    }

    public int getMonthHits()
    {
        return monthHits;
    }

    public void setMonthHits(int monthHits)
    {
        this.monthHits = monthHits;
    }

    public boolean getOpenType()
    {
        return openType;
    }

    public void setOpenType(boolean openType)
    {
        this.openType = openType;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public int getEliteLevel()
    {
        return eliteLevel;
    }

    public void setEliteLevel(int eliteLevel)
    {
        this.eliteLevel = eliteLevel;
    }

    public int getPriority()
    {
        return priority;
    }

    public void setPriority(int priority)
    {
        this.priority = priority;
    }

    public int getCommentCount()
    {
        return commentCount;
    }

    public void setCommentCount(int commentCount)
    {
        this.commentCount = commentCount;
    }

    public String getUpdateTime()
    {
        return updateTime;
    }

    public void setUpdateTime(String updateTime)
    {
        if (null != updateTime && !"".equals(updateTime))
        {
            Date date = TimeUtil.stringToDate(updateTime,
                    TimeUtil.ISO_DATE_NOSECOND_FORMAT);
            if (null != date)
            {
                this.updateTime = TimeUtil.dateToString(date,
                        TimeUtil.ISO_DATE_NOSECOND_FORMAT);
            }
            else
            {
                this.updateTime = "";
            }
        }
        else
        {
            this.updateTime = "";
        }
    }

    public String getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        if (null != createTime)
        {
            this.createTime = TimeUtil.dateToString(createTime,
                    TimeUtil.ISO_DATE_NOSECOND_FORMAT);
        }
        else
        {
            this.createTime = "";
        }
    }

    public String getPassedTime()
    {
        return passedTime;
    }

    public void setPassedTime(Date passedTime)
    {
        if (null != passedTime)
        {
            this.passedTime = TimeUtil.dateToString(passedTime,
                    TimeUtil.ISO_DATE_NOSECOND_FORMAT);
        }
        else
        {
            this.passedTime = "";
        }
    }

    public String getLastHitTime()
    {
        return lastHitTime;
    }

    public void setLastHitTime(Date lastHitTime)
    {
        if (null != lastHitTime)
        {
            this.lastHitTime = TimeUtil.dateToString(lastHitTime,
                    TimeUtil.ISO_DATE_NOSECOND_FORMAT);
        }
        else
        {
            this.lastHitTime = "";
        }
    }

    public String getDefaultPicUrl()
    {
        return defaultPicUrl;
    }

    public void setDefaultPicUrl(String defaultPicUrl)
    {
        this.defaultPicUrl = defaultPicUrl;
    }

    public boolean getShowCommentLink()
    {
        return showCommentLink;
    }

    public void setShowCommentLink(boolean showCommentLink)
    {
        this.showCommentLink = showCommentLink;
    }

    public boolean getIsCreateHtml()
    {
        return isCreateHtml;
    }

    public void setCreateHtml(boolean isCreateHtml)
    {
        this.isCreateHtml = isCreateHtml;
    }

    public String getHtmlUrl()
    {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl)
    {
        this.htmlUrl = htmlUrl;
    }
}
