package com.oswift.cms.entity;

import java.util.Date;

import com.oswift.utils.common.TimeUtil;

public class CompanyBean
{
    /**
     * 公司ID
     */
    private int companyId;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 公司规模ID
     */
    private int companyScaleId;

    /**
     * 公司规模
     */
    private String companyScale;

    /**
     * 公司性质ID
     */
    private int companyPropertieId;

    /**
     * 公司性质
     */
    private String companyPropertie;

    /**
     * 公司行业ID
     */
    private int companyIndustryId;

    /**
     * 公司行业
     */
    private String companyIndustry;

    /**
     * 公司简称
     */
    private String referred;

    /**
     * 国家/地区
     */
    private String country;

    /**
     * 省市/州郡
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 区/县
     */
    private String district;

    /**
     * 联系地址
     */
    private String address;

    /**
     * 邮政编码
     */
    private String zipCode;

    /**
     * 联系人
     */
    private String contact;

    /**
     * 公司联系电话
     */
    private String telephone;

    /**
     * 公司简介
     */
    private String companyIntr;

    /**
     * Email
     */
    private String email;

    /**
     * 公司手机
     */
    private String mobile;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 备注
     */
    private String remark;

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public int getCompanyScaleId()
    {
        return companyScaleId;
    }

    public void setCompanyScaleId(int companyScaleId)
    {
        this.companyScaleId = companyScaleId;
    }

    public int getCompanyPropertieId()
    {
        return companyPropertieId;
    }

    public void setCompanyPropertieId(int companyPropertieId)
    {
        this.companyPropertieId = companyPropertieId;
    }

    public int getCompanyIndustryId()
    {
        return companyIndustryId;
    }

    public void setCompanyIndustryId(int companyIndustryId)
    {
        this.companyIndustryId = companyIndustryId;
    }

    public String getReferred()
    {
        return referred;
    }

    public void setReferred(String referred)
    {
        this.referred = referred;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getProvince()
    {
        return province;
    }

    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setZipCode(String zipCode)
    {
        this.zipCode = zipCode;
    }

    public String getContact()
    {
        return contact;
    }

    public void setContact(String contact)
    {
        this.contact = contact;
    }

    public String getTelephone()
    {
        return telephone;
    }

    public void setTelephone(String telephone)
    {
        this.telephone = telephone;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getMobile()
    {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        if (null != createTime)
        {
            this.createTime = TimeUtil.dateToString(createTime,
                    TimeUtil.ISO_DATE_NOSECOND_FORMAT);
        }
    }

    public String getCompanyIntr()
    {
        return companyIntr;
    }

    public void setCompanyIntr(String companyIntr)
    {
        this.companyIntr = companyIntr;
    }

    public String getRemark()
    {
        return remark;
    }

    public void setRemark(String remark)
    {
        this.remark = remark;
    }

    public String getDistrict()
    {
        return district;
    }

    public void setDistrict(String district)
    {
        this.district = district;
    }

    public String getCompanyScale()
    {
        return companyScale;
    }

    public void setCompanyScale(String companyScale)
    {
        this.companyScale = companyScale;
    }

    public String getCompanyPropertie()
    {
        return companyPropertie;
    }

    public void setCompanyPropertie(String companyPropertie)
    {
        this.companyPropertie = companyPropertie;
    }

    public String getCompanyIndustry()
    {
        return companyIndustry;
    }

    public void setCompanyIndustry(String companyIndustry)
    {
        this.companyIndustry = companyIndustry;
    }

    public int getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(int companyId)
    {
        this.companyId = companyId;
    }
}
