package com.oswift.cms.entity;

/**
 *
 * 栏目Bean
 *
 * @author zhuou
 * @version C03 2013-1-18
 * @since OSwift GPM V1.0
 */
public class ChannelBean
{
    /**
     * 栏目ID
     */
    private int channelId;

    /**
     * 公司ID
     */
    private int companyId;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 栏目别称，英文名，可以在前台url中使用和栏目id作用一致
     */
    private String nickName;

    /**
     * 栏目名称
     */
    private String channelName;

    /**
     * 节点类型。0为容器栏目，1为专题栏目，2为单个页面，3为外部链接
     */
    private int channelType;

    /**
     * 父节点,根节点的值为0
     */
    private int parentId;

    /**
     * 是否有子节点，false没有，true有
     */
    private boolean isChild;

    /**
     * 排序
     */
    private int sort;

    /**
     * 栏目tip
     */
    private String tips;

    /**
     * 栏目关键字
     */
    private String meta_Keywords;

    /**
     * 栏目描述
     */
    private String meta_Description;

    /**
     * 描述
     */
    private String description;

    /**
     * 栏目URL或者外接栏目地址
     */
    private String channelUrl;

    /**
     * 栏目iconURL
     */
    private String iconUrl;

    /**
     * 打开方式 0原窗口打开 1新窗口打开
     */
    private int openType;

    /**
     * 栏目内容
     */
    private String content;

    /**
     * 栏目类别 0为预置栏目 1为用户创建栏目
     */
    private int category;

    /**
     * 栏目权限。0--开放栏目 1--认证栏目
     */
    private int purviewType;

    /**
     * 访问权限，和用户组ID相关联，如：1,2,3
     */
    private String accessRight;

    /**
     * 包含板块
     */
    private String forums;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 最后编辑时间
     */
    private String lastEditTime;

    /**
     * 最后编辑人
     */
    private String lastEditUser;

    public int getChannelId()
    {
        return channelId;
    }

    public void setChannelId(int channelId)
    {
        this.channelId = channelId;
    }

    public int getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(int companyId)
    {
        this.companyId = companyId;
    }

    public String getChannelName()
    {
        return channelName;
    }

    public void setChannelName(String channelName)
    {
        this.channelName = channelName;
    }

    public String getNickName()
    {
        return nickName;
    }

    public void setNickName(String nickName)
    {
        this.nickName = nickName;
    }

    public int getChannelType()
    {
        return channelType;
    }

    public void setChannelType(int channelType)
    {
        this.channelType = channelType;
    }

    public int getParentId()
    {
        return parentId;
    }

    public void setParentId(int parentId)
    {
        this.parentId = parentId;
    }

    public boolean isChild()
    {
        return isChild;
    }

    public void setChild(boolean isChild)
    {
        this.isChild = isChild;
    }

    public int getSort()
    {
        return sort;
    }

    public void setSort(int sort)
    {
        this.sort = sort;
    }

    public String getTips()
    {
        return tips;
    }

    public void setTips(String tips)
    {
        this.tips = tips;
    }

    public String getMeta_Keywords()
    {
        return meta_Keywords;
    }

    public void setMeta_Keywords(String meta_Keywords)
    {
        this.meta_Keywords = meta_Keywords;
    }

    public String getMeta_Description()
    {
        return meta_Description;
    }

    public void setMeta_Description(String meta_Description)
    {
        this.meta_Description = meta_Description;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getChannelUrl()
    {
        return channelUrl;
    }

    public void setChannelUrl(String channelUrl)
    {
        this.channelUrl = channelUrl;
    }

    public String getIconUrl()
    {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl)
    {
        this.iconUrl = iconUrl;
    }

    public int getOpenType()
    {
        return openType;
    }

    public void setOpenType(int openType)
    {
        this.openType = openType;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public String getForums()
    {
        return forums;
    }

    public void setForums(String forums)
    {
        this.forums = forums;
    }

    public int getCategory()
    {
        return category;
    }

    public void setCategory(int category)
    {
        this.category = category;
    }

    public int getPurviewType()
    {
        return purviewType;
    }

    public void setPurviewType(int purviewType)
    {
        this.purviewType = purviewType;
    }

    public String getAccessRight()
    {
        return accessRight;
    }

    public void setAccessRight(String accessRight)
    {
        this.accessRight = accessRight;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(String createTime)
    {
        this.createTime = createTime;
    }

    public String getCreateUser()
    {
        return createUser;
    }

    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }

    public String getLastEditTime()
    {
        return lastEditTime;
    }

    public void setLastEditTime(String lastEditTime)
    {
        this.lastEditTime = lastEditTime;
    }

    public String getLastEditUser()
    {
        return lastEditUser;
    }

    public void setLastEditUser(String lastEditUser)
    {
        this.lastEditUser = lastEditUser;
    }
}
