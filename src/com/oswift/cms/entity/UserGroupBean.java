package com.oswift.cms.entity;

import java.util.Date;

import com.oswift.utils.common.TimeUtil;

public class UserGroupBean
{
    /**
     * 用户组ID
     */
    private int groupId;

    /**
     * 公司ID
     */
    private int companyId;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 用户组名
     */
    private String groupName;

    /**
     * 会员组类型 1表示内置会员组(不容许删除) 2表示用户创建会员组
     */
    private int groupType;

    /**
     * 用户组说明
     */
    private String description;

    /**
     * 用户升级参数设置
     */
    private String upgradeSetting;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 创建者
     */
    private String createUser;

    public int getGroupId()
    {
        return groupId;
    }

    public void setGroupId(int groupId)
    {
        this.groupId = groupId;
    }

    public int getCompanyId()
    {
        return companyId;
    }

    public void setCompanyId(int companyId)
    {
        this.companyId = companyId;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getUpgradeSetting()
    {
        return upgradeSetting;
    }

    public void setUpgradeSetting(String upgradeSetting)
    {
        this.upgradeSetting = upgradeSetting;
    }

    public String getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(String createTime)
    {
        if (null != createTime && !"".equals(createTime))
        {
            Date date = TimeUtil.stringToDate(createTime,
                    TimeUtil.ISO_DATE_TIME_FORMAT);
            if (null != date)
            {
                this.createTime = TimeUtil.dateToString(date,
                        TimeUtil.ISO_DATE_TIME_FORMAT);
            }
            else
            {
                this.createTime = "";
            }
        }
        else
        {
            this.createTime = "";
        }
    }

    public String getCreateUser()
    {
        return createUser;
    }

    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public int getGroupType()
    {
        return groupType;
    }

    public void setGroupType(int groupType)
    {
        this.groupType = groupType;
    }
}
