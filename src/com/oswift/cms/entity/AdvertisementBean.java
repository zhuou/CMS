package com.oswift.cms.entity;

import java.util.Date;

/**
 *
 * 广告表
 *
 * @author zhuou
 * @version C03 2014-2-10
 * @since OSwift GPM V1.0
 */
public class AdvertisementBean
{
    /**
     * 广告ID
     */
    private int adId;

    /**
     * 广告id
     */
    private int zoneId;

    /**
     * 广告名称
     */
    private String adName;

    /**
     * 广告位名称
     */
    private String zoneName;

    /**
     * 广告类型
     */
    private int adType;

    /**
     * 图片路径
     */
    private String imgUrl;

    /**
     * 图片宽
     */
    private int imgWidth;

    /**
     * 图片高
     */
    private int imgHeight;

    /**
     * 广告内容描述
     */
    private String adIntro;

    /**
     * 链接地址
     */
    private String linkUrl;

    /**
     * 链接alt提示信息
     */
    private String linkAlt;

    /**
     * 广告项目的权重
     */
    private int priority;

    /**
     * 是否通过审核
     */
    private boolean passed;

    /**
     * 排序
     */
    private int sort;

    /**
     * 点击次数
     */
    private int clicks;

    /**
     * 是否开启时间限制 0为不开启 1开启
     */
    private boolean openTimeLimit;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建者
     */
    private String createUser;

    public int getAdId()
    {
        return adId;
    }

    public void setAdId(int adId)
    {
        this.adId = adId;
    }

    public int getZoneId()
    {
        return zoneId;
    }

    public void setZoneId(int zoneId)
    {
        this.zoneId = zoneId;
    }

    public String getAdName()
    {
        return adName;
    }

    public void setAdName(String adName)
    {
        this.adName = adName;
    }

    public String getZoneName()
    {
        return zoneName;
    }

    public void setZoneName(String zoneName)
    {
        this.zoneName = zoneName;
    }

    public int getAdType()
    {
        return adType;
    }

    public void setAdType(int adType)
    {
        this.adType = adType;
    }

    public String getImgUrl()
    {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl)
    {
        this.imgUrl = imgUrl;
    }

    public int getImgWidth()
    {
        return imgWidth;
    }

    public void setImgWidth(int imgWidth)
    {
        this.imgWidth = imgWidth;
    }

    public int getImgHeight()
    {
        return imgHeight;
    }

    public void setImgHeight(int imgHeight)
    {
        this.imgHeight = imgHeight;
    }

    public String getAdIntro()
    {
        return adIntro;
    }

    public void setAdIntro(String adIntro)
    {
        this.adIntro = adIntro;
    }

    public String getLinkUrl()
    {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl)
    {
        this.linkUrl = linkUrl;
    }

    public String getLinkAlt()
    {
        return linkAlt;
    }

    public void setLinkAlt(String linkAlt)
    {
        this.linkAlt = linkAlt;
    }

    public int getPriority()
    {
        return priority;
    }

    public void setPriority(int priority)
    {
        this.priority = priority;
    }

    public boolean isPassed()
    {
        return passed;
    }

    public void setPassed(boolean passed)
    {
        this.passed = passed;
    }

    public int getSort()
    {
        return sort;
    }

    public void setSort(int sort)
    {
        this.sort = sort;
    }

    public int getClicks()
    {
        return clicks;
    }

    public void setClicks(int clicks)
    {
        this.clicks = clicks;
    }

    public boolean isOpenTimeLimit()
    {
        return openTimeLimit;
    }

    public void setOpenTimeLimit(boolean openTimeLimit)
    {
        this.openTimeLimit = openTimeLimit;
    }

    public Date getStartTime()
    {
        return startTime;
    }

    public void setStartTime(Date startTime)
    {
        this.startTime = startTime;
    }

    public Date getEndTime()
    {
        return endTime;
    }

    public void setEndTime(Date endTime)
    {
        this.endTime = endTime;
    }

    public Date getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        this.createTime = createTime;
    }

    public String getCreateUser()
    {
        return createUser;
    }

    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }
}
