package com.oswift.cms.entity;

/**
 * 
 * 友情链接类
 * 
 * @author zhuou
 * @version C03 2013-1-10
 * @since OSwift GPM V1.0
 */
public class FriendSiteBean extends CommonModelBean
{
    /**
     * 友情链接ID
     */
    private long friendSiteId;

    /**
     * 友情链接Url
     */
    private String siteUrl;

    /**
     * 友情链接描述
     */
    private String siteIntro;

    /**
     * 友情链接logo
     */
    private String logoUrl;

    /**
     * 链接类型1表示文字类型;2表示图片类型
     */
    private int linkType;

    /**
     * 是否推荐
     */
    private boolean isElite;

    public long getFriendSiteId()
    {
        return friendSiteId;
    }

    public void setFriendSiteId(long friendSiteId)
    {
        this.friendSiteId = friendSiteId;
    }

    public String getSiteUrl()
    {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl)
    {
        this.siteUrl = siteUrl;
    }

    public String getSiteIntro()
    {
        return siteIntro;
    }

    public void setSiteIntro(String siteIntro)
    {
        this.siteIntro = siteIntro;
    }

    public String getLogoUrl()
    {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl)
    {
        this.logoUrl = logoUrl;
    }

    public boolean getIsElite()
    {
        return isElite;
    }

    public void setElite(boolean isElite)
    {
        this.isElite = isElite;
    }

    public int getLinkType()
    {
        return linkType;
    }

    public void setLinkType(int linkType)
    {
        this.linkType = linkType;
    }
}
