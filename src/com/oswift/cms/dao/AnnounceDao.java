package com.oswift.cms.dao;

import org.springframework.stereotype.Repository;

import com.oswift.cms.entity.AnnounceBean;

@Repository("announceDao")
public class AnnounceDao extends BaseDao<AnnounceBean>
{

    public AnnounceDao()
    {
        super(SqlMapper.Announce.NAMESPACE);
    }
}
