package com.oswift.cms.dao;

import org.springframework.stereotype.Repository;

import com.oswift.cms.entity.ArticleBean;

@Repository("articleDao")
public class ArticleDao extends BaseDao<ArticleBean>
{
    public ArticleDao()
    {
        super(SqlMapper.Article.NAMESPACE);
    }
}
