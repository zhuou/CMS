package com.oswift.cms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.cms.entity.AdZoneBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("adZoneDao")
public class AdZoneDao extends BaseDao<AdZoneBean>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(AdZoneDao.class);

    public AdZoneDao()
    {
        super(SqlMapper.AdZone.NAMESPACE);
    }

    /**
     *
     * 根据广告位id修改广告位状态
     *
     * @author zhuou
     * @param bean
     *            广告位bean
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updateStatus(AdZoneBean bean) throws PlatException
    {
        try
        {
            return this.sqlSession.update(SqlMapper.AdZone.UPDATESTATUS, bean);
        }
        catch (Exception e)
        {
            log.error("Fail to updateStatus info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据companyId获取广告位列表
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @return List
     * @throws PlatException
     *             公共异常
     */
    public List<AdZoneBean> getAdZoneListByCompanyId(int companyId)
        throws PlatException
    {
        try
        {
            return this.sqlSession.selectList(
                    SqlMapper.AdZone.GETADZONELISTBYCOMPANYID, companyId);
        }
        catch (Exception e)
        {
            log.error("Fail to getAdZoneListByCompanyId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据CompanyId查询出主键值最大的一条记录
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @return AdZoneBean
     * @throws PlatException
     *             公共异常
     */
    public AdZoneBean getTop1ByCompanyId(int companyId) throws PlatException
    {
        try
        {
            return this.sqlSession.selectOne(
                    SqlMapper.AdZone.GETTOP1BYCOMPANYID, companyId);
        }
        catch (Exception e)
        {
            log.error("Fail to getTop1ByCompanyId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据ZoneCode和公司Id查询出一条记录
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @param zoneCode
     *            编码
     * @return AdZoneBean
     * @throws PlatException
     *             公共异常
     */
    public AdZoneBean getOneByZoneCode(int companyId, String zoneCode)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("companyId", companyId);
            param.put("zoneCode", zoneCode);
            return this.sqlSession.selectOne(SqlMapper.AdZone.GETONEBYZONECODE,
                    param);
        }
        catch (Exception e)
        {
            log.error("Fail to getOneByZoneCode info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }
}
