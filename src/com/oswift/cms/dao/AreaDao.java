package com.oswift.cms.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.oswift.cms.entity.AreaBean;
import com.oswift.cms.utils.Field;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("areaDao")
public class AreaDao extends BaseDao<AreaBean>
{
    public AreaDao()
    {
        super(SqlMapper.Area.NAMESPACE);
    }

    /**
     *
     * 获取省、市、区/县的信息
     *
     * @author zhuou
     * @param areaType
     * @return map
     * @throws PlatException
     *             CMS公共异常
     */
    public List<AreaBean> getArea(String areaType) throws PlatException
    {
        try
        {
            if (StringUtil.isEmpty(areaType))
            {
                return null;
            }

            String sql = SqlMapper.Area.GETPROVINCE;
            if (areaType.equals(Field.Area.CITY))
            {
                sql = SqlMapper.Area.GETCITY;
            }
            else if (areaType.equals(Field.Area.DISTRICT))
            {
                sql = SqlMapper.Area.GETDISTRICT;
            }

            List<AreaBean> list = sqlSession.selectList(sql);
            return list;
        }
        catch (Exception e)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据id获取市、区/县的信息
     *
     * @author zhuou
     * @param areaType
     * @param id
     * @return
     * @throws PlatException
     *             CMS公共异常
     */
    public List<AreaBean> getAreaById(String areaType, int id)
        throws PlatException
    {
        try
        {
            String sql = SqlMapper.Area.GETCITYBYID;
            if (areaType.equals(Field.Area.DISTRICT))
            {
                sql = SqlMapper.Area.GETDISTRICTBYID;
            }

            List<AreaBean> list = sqlSession.selectList(sql, id);
            return list;
        }
        catch (Exception e)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }
}
