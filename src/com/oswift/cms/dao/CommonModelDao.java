package com.oswift.cms.dao;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.cms.entity.CommonModelBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("commonModelDao")
public class CommonModelDao extends BaseDao<CommonModelBean>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(CommonModelDao.class);

    public CommonModelDao()
    {
        super(SqlMapper.CommonModel.NAMESPACE);
    }

    /**
     *
     * 获取公共内容信息，有分页
     *
     * @author zhuou
     * @param paramMap
     *            Map
     * @return int
     * @throws PlatException
     *             CMS公共异常
     */
    public PageBean<CommonModelBean> getCommonModel(Map<String, Object> paramMap)
        throws PlatException
    {

        try
        {
            List<CommonModelBean> dataList = sqlSession.selectList(
                    SqlMapper.CommonModel.GET_COMMONMODEL, paramMap);

            int totalRecordNum = (Integer) sqlSession.selectOne(
                    SqlMapper.CommonModel.GET_COUNTCOMMONMODEL, paramMap);

            PageBean<CommonModelBean> pageBean = new PageBean<CommonModelBean>();
            pageBean.setTotalRecordNum(totalRecordNum);
            pageBean.setDataList(dataList);

            return pageBean;
        }
        catch (Exception e)
        {
            log.error("Fail to save getCommonModel info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 更新内容状态
     *
     * @author zhuou
     * @param param
     *            Map
     * @return int
     * @throws PlatException
     *             CMS公共异常
     */
    public int updateStatus(Map<String, Object> param) throws PlatException
    {
        try
        {
            int rows = sqlSession.update(SqlMapper.CommonModel.UPDATE_STATUS,
                    param);
            return rows;
        }
        catch (Exception e)
        {
            log.error("Fail to updateStatus.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 更新天点击率
     *
     * @author zhuou
     * @return int
     * @throws PlatException
     *             CMS公共异常
     */
    public int updateDayHit() throws PlatException
    {
        try
        {
            int rows = sqlSession.update(SqlMapper.CommonModel.UPDATE_DAYHIT,
                    null);
            return rows;
        }
        catch (Exception e)
        {
            log.error("Fail to updateDayHit.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 更新周点击率
     *
     * @author zhuou
     * @return int
     * @throws PlatException
     *             CMS公共异常
     */
    public int updateWeekHit() throws PlatException
    {
        try
        {
            int rows = sqlSession.update(SqlMapper.CommonModel.UPDATE_WEEKHIT,
                    null);
            return rows;
        }
        catch (Exception e)
        {
            log.error("Fail to updateWeekHit.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 更新月点击率
     *
     * @author zhuou
     * @return int
     * @throws PlatException
     *             CMS公共异常
     */
    public int updateMonthHit() throws PlatException
    {
        try
        {
            int rows = sqlSession.update(SqlMapper.CommonModel.UPDATE_MONTHHIT,
                    null);
            return rows;
        }
        catch (Exception e)
        {
            log.error("Fail to updateMonthHit.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }
}
