package com.oswift.cms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.cms.entity.CompanyBean;
import com.oswift.cms.entity.CompanyIndustryBean;
import com.oswift.cms.entity.CompanyPropertieBean;
import com.oswift.cms.entity.CompanyScaleBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("companyDao")
public class CompanyDao extends BaseDao<CompanyBean>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(CompanyDao.class);

    public CompanyDao()
    {
        super(SqlMapper.Company.NAMESPACE);
    }

    /**
     *
     * 获取公司行业信息
     *
     * @author zhuou
     * @return list
     * @throws PlatException
     *             CMS公共异常
     */
    public List<CompanyIndustryBean> getIndustry() throws PlatException
    {
        try
        {
            List<CompanyIndustryBean> list = sqlSession
                    .selectList(SqlMapper.Company.GET_COMPANYINDUSTRY);
            return list;
        }
        catch (Exception e)
        {
            log.error("Fail to save companyIndustry info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 获取获取公司性质
     *
     * @author zhuou
     * @return list
     * @throws PlatException
     *             CMS公共异常
     */
    public List<CompanyPropertieBean> getPropertie() throws PlatException
    {
        try
        {
            List<CompanyPropertieBean> list = sqlSession
                    .selectList(SqlMapper.Company.GET_COMPANYPROPERTIE);
            return list;
        }
        catch (Exception e)
        {
            log.error("Fail to save CompanyPropertie info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 获取公司规模
     *
     * @author zhuou
     * @return list
     * @throws PlatException
     *             CMS公共异常
     */
    public List<CompanyScaleBean> getScaleBean() throws PlatException
    {
        try
        {
            List<CompanyScaleBean> list = sqlSession
                    .selectList(SqlMapper.Company.GET_COMPANYSCALE);
            return list;
        }
        catch (Exception e)
        {
            log.error("Fail to save CompanyScale info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 获取公司列表
     *
     * @author zhuou
     * @param keyword
     *            关键字
     * @param rowNum
     *            行数
     * @param pageNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    public PageBean<CompanyBean> getCompanyList(String keyword, int rowNum,
            int pageNum, int companyId) throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("keyword", keyword);
            param.put("rowNum", rowNum);
            param.put("pageNum", pageNum);
            param.put("companyId", companyId);

            List<CompanyBean> dataList = sqlSession.selectList(
                    SqlMapper.Company.GET_COMPANYLIST, param);

            int totalRecordNum = (Integer) sqlSession.selectOne(
                    SqlMapper.Company.GET_COMPANYNUM, param);

            PageBean<CompanyBean> pageBean = new PageBean<CompanyBean>();
            pageBean.setTotalRecordNum(totalRecordNum);
            pageBean.setDataList(dataList);

            return pageBean;
        }
        catch (Exception e)
        {
            log.error("Fail to save CompanyList info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据公司id获取公司列表，如果company=-1获取所有公司，如果>-1则只获取此公司的信息
     *
     * @author zhuou
     * @param companyId
     *            公司id
     * @return List<CompanyBean>
     * @throws PlatException
     *             CMS公共异常
     */
    public List<CompanyBean> getCompanysbyCId(int companyId)
        throws PlatException
    {
        try
        {
            Map<String, Integer> param = new HashMap<String, Integer>();
            param.put("companyId", companyId);
            return sqlSession.selectList(SqlMapper.Company.GET_COMPANYSBYCID,
                    param);
        }
        catch (Exception e)
        {
            log.error("Fail to getCompanysbyCId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }
}
