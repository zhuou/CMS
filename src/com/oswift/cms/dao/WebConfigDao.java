package com.oswift.cms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.cms.dao.SqlMapper.BackWebConfig;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.entity.WebConfigBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("webConfigDao")
public class WebConfigDao extends BaseDao<WebConfigBean>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(WebConfigDao.class);

    public WebConfigDao()
    {
        super(BackWebConfig.NAMESPACE);
    }

    /**
     *
     * 获取网站配置信息列表
     *
     * @author zhuou
     * @param keyword
     *            关键字
     * @param rowNum
     *            行数
     * @param pageNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    public PageBean<WebConfigBean> getWebConfigList(String keyword, int rowNum,
            int pageNum, int companyId) throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("rowNum", rowNum);
            param.put("pageNum", pageNum);
            param.put("companyId", companyId);

            List<WebConfigBean> dataList = sqlSession.selectList(
                    SqlMapper.BackWebConfig.GET_WEBCONFIGLIST, param);

            int totalRecordNum = (Integer) sqlSession.selectOne(
                    SqlMapper.BackWebConfig.GET_WEBCONFIGNUM, param);

            PageBean<WebConfigBean> pageBean = new PageBean<WebConfigBean>();
            pageBean.setTotalRecordNum(totalRecordNum);
            pageBean.setDataList(dataList);

            return pageBean;
        }
        catch (Exception e)
        {
            log.error("Fail to getWebConfigList info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据公司Id获取站点信息
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @return List
     * @throws PlatException
     *             CMS公共异常
     */
    public List<WebConfigBean> getWebConfigByCompanyId(int companyId)
        throws PlatException
    {
        try
        {
            Map<String, Integer> param = new HashMap<String, Integer>();
            param.put("companyId", companyId);
            List<WebConfigBean> dataList = sqlSession.selectList(
                    SqlMapper.BackWebConfig.GET_WEBCONFIGBYCOMPANYID, param);
            return dataList;
        }
        catch (Exception e)
        {
            log.error("Fail to getWebConfigByCompanyId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }
}
