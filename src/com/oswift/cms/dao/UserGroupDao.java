package com.oswift.cms.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.cms.entity.UserGroupBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("userGroupDao")
public class UserGroupDao extends BaseDao<UserGroupBean>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(UserGroupDao.class);

    public UserGroupDao()
    {
        super(SqlMapper.UserGroup.NAMESPACE);
    }

    /**
     *
     * 根据公司Id获取用户组信息
     *
     * @author zhuou
     * @param companyId
     * @return List
     * @throws PlatException
     *             CMS公共异常
     */
    public List<UserGroupBean> getUserGroupByCompanyId(int companyId)
        throws PlatException
    {
        try
        {
            List<UserGroupBean> list = sqlSession.selectList(
                    SqlMapper.UserGroup.GET_USERGROUPBYCOMPANYID, companyId);
            return list;
        }
        catch (Exception e)
        {
            log.error("Fail to get UserGroup info by CompanyId.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据id查询用户组
     *
     * @author zhuou
     * @param ids
     *            用户组id
     * @return List
     * @throws PlatException
     *             公共异常
     */
    public List<UserGroupBean> getUserGroupByIds(String ids)
        throws PlatException
    {
        try
        {
            List<UserGroupBean> list = sqlSession.selectList(
                    SqlMapper.UserGroup.GET_USERGROUPBYIDS, ids);
            return list;
        }
        catch (Exception e)
        {
            log.error("Fail to get getUserGroupByIds.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }
}
