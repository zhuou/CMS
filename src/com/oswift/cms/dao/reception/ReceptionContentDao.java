package com.oswift.cms.dao.reception;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.oswift.cms.dao.BaseDao;
import com.oswift.cms.dao.SqlMapper;
import com.oswift.cms.entity.reception.AdZone;
import com.oswift.cms.entity.reception.Advertisement;
import com.oswift.cms.entity.reception.Announce;
import com.oswift.cms.entity.reception.Article;
import com.oswift.cms.entity.reception.Channel;
import com.oswift.cms.entity.reception.Comment;
import com.oswift.cms.entity.reception.CommonModel;
import com.oswift.cms.entity.reception.DataList;
import com.oswift.cms.entity.reception.Download;
import com.oswift.cms.entity.reception.FontFile;
import com.oswift.cms.entity.reception.FriendSite;
import com.oswift.cms.entity.reception.GuestBook;
import com.oswift.cms.entity.reception.GuestBookReply;
import com.oswift.cms.entity.reception.Pic;
import com.oswift.cms.entity.reception.PicUrl;
import com.oswift.cms.utils.Field;
import com.oswift.cms.utils.PageCode;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("receptionContentDao")
public class ReceptionContentDao extends BaseDao<CommonModel>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(ReceptionContentDao.class);

    public ReceptionContentDao()
    {
        super(SqlMapper.Reception.NAMESPACE);
    }

    /**
     *
     * 查询内容,带分页
     *
     * @author zhuou
     * @param topNum
     *            查询多少条记录
     * @param strWhere
     *            查询条件，可以为空值
     * @param orderColumn
     *            排序列
     * @param orderType
     *            排序类型，0为升序，1为降序
     * @param channelId
     *            栏目Id
     * @param companyId
     *            单位Id
     * @param nowPage
     *            当前第几页
     * @param tableName
     *            表名称
     * @return DataList
     * @throws PlatException
     *             公共异常
     */
    public DataList getContentPage(int topNum, String strWhere,
            String orderColumn, boolean orderType, long channelId,
            int companyId, int nowPage, String tableName) throws PlatException
    {
        try
        {
            // 计算出从第几行分页
            int rowNum = (nowPage - 1) * topNum;

            Map<String, Object> paraMap = new HashMap<String, Object>();
            paraMap.put("topNum", topNum);
            if (StringUtil.isEmpty(strWhere))
            {
                paraMap.put("strWhere", "");
            }
            else
            {
                paraMap.put("strWhere", strWhere);
            }
            paraMap.put("orderColumn", orderColumn);
            paraMap.put("orderType", orderType);
            paraMap.put("channelId", channelId);
            paraMap.put("companyId", companyId);
            paraMap.put("rowNum", rowNum);
            paraMap.put("totalRecordNum", 0);
            paraMap.put("tableName", tableName);

            DataList dataList = new DataList();
            if (Field.TableName.ARTICLE.equals(tableName))
            {
                List<Article> list = sqlSession.selectList(
                        SqlMapper.Reception.GET_ARTICLEPAGE, paraMap);
                dataList.setTotalRecordNum(list.size());
                dataList.setList(list);
            }
            else if (Field.TableName.ANNOUNCE.equals(tableName))
            {
                List<Announce> list = sqlSession.selectList(
                        SqlMapper.Reception.GET_ANNOUNCESPAGE, paraMap);
                dataList.setTotalRecordNum(list.size());
                dataList.setList(list);
            }
            else if (Field.TableName.DOWNLOAD.equals(tableName))
            {
                List<Download> list = sqlSession.selectList(
                        SqlMapper.Reception.GET_DOWNLOADSPAGE, paraMap);
                dataList.setTotalRecordNum(list.size());
                dataList.setList(list);
            }
            else if (Field.TableName.FRIENDSITE.equals(tableName))
            {
                List<FriendSite> list = sqlSession.selectList(
                        SqlMapper.Reception.GET_FRIENDSITESPAGE, paraMap);
                dataList.setTotalRecordNum(list.size());
                dataList.setList(list);
            }
            else if (Field.TableName.GUESTBOOK.equals(tableName))
            {
                List<GuestBook> list = sqlSession.selectList(
                        SqlMapper.Reception.GET_GUESTBOOKSPAGE, paraMap);

                // 获取留言的回复
                if (list.size() > 0)
                {
                    List<String> ids = new ArrayList<String>();
                    for (GuestBook item : list)
                    {
                        ids.add(String.valueOf(item.getGuestBookId()));
                    }
                    List<GuestBookReply> replyList = sqlSession.selectList(
                            SqlMapper.Reception.GETGUESTBOOKREPLYBYIDS, ids);
                    for (GuestBook item : list)
                    {
                        List<GuestBookReply> replys = new ArrayList<GuestBookReply>();
                        for (GuestBookReply reply : replyList)
                        {
                            if (reply.getGuestBookId() == item.getGuestBookId())
                            {
                                replys.add(reply);
                            }
                        }
                        item.setReplyList(replys);
                    }
                }
                dataList.setTotalRecordNum(list.size());
                dataList.setList(list);
            }
            else if (Field.TableName.PICURL.equals(tableName))
            {
                List<Pic> list = sqlSession.selectList(
                        SqlMapper.Reception.GET_PICSPAGE, paraMap);

                if (list.size() > 0)
                {
                    List<String> ids = new ArrayList<String>();
                    for (Pic item : list)
                    {
                        ids.add(String.valueOf(item.getPicGalleryId()));
                    }
                    List<PicUrl> picUrls = sqlSession.selectList(
                            SqlMapper.Reception.GETPICURLBYIDS, ids);
                    for (Pic item : list)
                    {
                        List<PicUrl> items = new ArrayList<PicUrl>();
                        for (PicUrl picUrl : picUrls)
                        {
                            if (item.getPicGalleryId() == picUrl
                                    .getPicGalleryId())
                            {
                                items.add(picUrl);
                            }
                        }
                        item.setList(items);
                    }
                }

                dataList.setTotalRecordNum(list.size());
                dataList.setList(list);
            }

            // 总记录数
            int totalRNum = 0;
            Object oNum = paraMap.get("totalRecordNum");
            if (null != oNum)
            {
                totalRNum = (Integer) oNum;
            }
            dataList.setTotalRecordNum(totalRNum);
            dataList.setPageCurrentNum(nowPage);

            // 总页数
            int pageNum = totalRNum % topNum == 0 ? totalRNum / topNum
                    : totalRNum / topNum + 1;
            dataList.setPageNum(pageNum);

            dataList.setPageRecordNum(topNum);
            return dataList;
        }
        catch (Exception e)
        {
            log.error("Fail to getContentPage.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 查询内容
     *
     * @author zhuou
     * @param topNum
     *            查询多少条记录
     * @param strWhere
     *            查询条件，可以为空值
     * @param orderColumn
     *            排序列
     * @param orderType
     *            排序类型，0为升序，1为降序
     * @param channelId
     *            栏目Id
     * @param companyId
     *            单位Id
     * @param tableName
     *            表名称
     * @return DataList
     * @throws PlatException
     *             公共异常
     */
    public DataList getContentListNoPage(int topNum, String strWhere,
            String orderColumn, boolean orderType, long channelId,
            int companyId, String tableName) throws PlatException
    {
        try
        {
            Map<String, Object> paraMap = new HashMap<String, Object>();
            paraMap.put("topNum", topNum);
            if (StringUtil.isEmpty(strWhere))
            {
                paraMap.put("strWhere", "");
            }
            else
            {
                paraMap.put("strWhere", strWhere);
            }
            paraMap.put("orderColumn", orderColumn);
            paraMap.put("orderType", orderType);
            paraMap.put("channelId", channelId);
            paraMap.put("companyId", companyId);
            paraMap.put("tableName", tableName);

            DataList dataList = new DataList();
            if (Field.TableName.ARTICLE.equals(tableName))
            {
                List<Article> list = sqlSession.selectList(
                        SqlMapper.Reception.GET_ARTICLENOPAGE, paraMap);
                if (null == list)
                {
                    list = new ArrayList<Article>();
                }
                dataList.setPageRecordNum(list.size());
                dataList.setTotalRecordNum(list.size());
                dataList.setList(list);
            }
            else if (Field.TableName.ANNOUNCE.equals(tableName))
            {
                List<Announce> list = sqlSession.selectList(
                        SqlMapper.Reception.GET_ANNOUNCESNOPAGE, paraMap);
                if (null == list)
                {
                    list = new ArrayList<Announce>();
                }
                dataList.setPageRecordNum(list.size());
                dataList.setTotalRecordNum(list.size());
                dataList.setList(list);
            }
            else if (Field.TableName.DOWNLOAD.equals(tableName))
            {
                List<Download> list = sqlSession.selectList(
                        SqlMapper.Reception.GET_DOWNLOADSNOPAGE, paraMap);
                if (null == list)
                {
                    list = new ArrayList<Download>();
                }
                dataList.setPageRecordNum(list.size());
                dataList.setTotalRecordNum(list.size());
                dataList.setList(list);
            }
            else if (Field.TableName.FRIENDSITE.equals(tableName))
            {
                List<FriendSite> list = sqlSession.selectList(
                        SqlMapper.Reception.GET_FRIENDSITESNOPAGE, paraMap);
                if (null == list)
                {
                    list = new ArrayList<FriendSite>();
                }
                dataList.setPageRecordNum(list.size());
                dataList.setTotalRecordNum(list.size());
                dataList.setList(list);
            }
            else if (Field.TableName.GUESTBOOK.equals(tableName))
            {
                List<GuestBook> list = sqlSession.selectList(
                        SqlMapper.Reception.GET_GUESTBOOKSNOPAGE, paraMap);
                if (null == list)
                {
                    list = new ArrayList<GuestBook>();
                }
                // 获取留言的回复
                if (list.size() > 0)
                {
                    List<String> ids = new ArrayList<String>();
                    for (GuestBook item : list)
                    {
                        ids.add(String.valueOf(item.getGuestBookId()));
                    }
                    List<GuestBookReply> replyList = sqlSession.selectList(
                            SqlMapper.Reception.GETGUESTBOOKREPLYBYIDS, ids);
                    for (GuestBook item : list)
                    {
                        List<GuestBookReply> replys = new ArrayList<GuestBookReply>();
                        for (GuestBookReply reply : replyList)
                        {
                            if (reply.getGuestBookId() == item.getGuestBookId())
                            {
                                replys.add(reply);
                            }
                        }
                        item.setReplyList(replys);
                    }
                }
                dataList.setPageRecordNum(list.size());
                dataList.setTotalRecordNum(list.size());
                dataList.setList(list);
            }
            else if (Field.TableName.PICURL.equals(tableName))
            {
                List<Pic> list = sqlSession.selectList(
                        SqlMapper.Reception.GET_PICSNOPAGE, paraMap);
                if (null == list)
                {
                    list = new ArrayList<Pic>();
                }
                if (list.size() > 0)
                {
                    List<String> ids = new ArrayList<String>();
                    for (Pic item : list)
                    {
                        ids.add(String.valueOf(item.getPicGalleryId()));
                    }
                    List<PicUrl> picUrls = sqlSession.selectList(
                            SqlMapper.Reception.GETPICURLBYIDS, ids);
                    for (Pic item : list)
                    {
                        List<PicUrl> items = new ArrayList<PicUrl>();
                        for (PicUrl picUrl : picUrls)
                        {
                            if (item.getPicGalleryId() == picUrl
                                    .getPicGalleryId())
                            {
                                items.add(picUrl);
                            }
                        }
                        item.setList(items);
                    }
                }
                dataList.setPageRecordNum(list.size());
                dataList.setTotalRecordNum(list.size());
                dataList.setList(list);
            }

            return dataList;
        }
        catch (Exception e)
        {
            log.error("Fail to getContentListNoPage.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 获取文章详情
     *
     * @author zhuou
     * @param commonId
     *            内容Id
     * @param companyId
     *            前台页面传过来的公司Id，为了防止多站点情况下通过修改url的id获取到其他网站的内容
     * @param isRelation
     *            是否要获取这篇内容的上一篇，下一篇内容
     * @param relationFlag
     *            当isRelation为true，此字段起作用。 当relationFlag=1时获取的是本栏目下的相关上一条下一条记录，
     *            当relationFlag=2时获取文章板块下上一条下一条记录，
     *            当relationFlag=3时获取本栏目及其子栏目下的文章上一条下一条记录
     * @return Article
     * @throws PlatException
     *             公共异常
     */
    public Article getArticleDetail(long commonId, int companyId,
            boolean isRelation, int relationFlag) throws PlatException
    {
        try
        {
            Map<String, Object> paraMap = new HashMap<String, Object>();
            paraMap.put("commonModelId", commonId);
            paraMap.put("tableName", Field.TableName.ARTICLE);
            Article item = sqlSession.selectOne(
                    SqlMapper.Reception.GET_ARTICLEDETAIL, paraMap);
            if (null != item)
            {
                // 公司id不相等，则此请求非法
                if (item.getCompanyId() != companyId)
                {
                    item = new Article();
                }
                else
                {
                    if (isRelation)
                    {

                        long articleId = item.getArticleId();
                        paraMap.put("articleId", articleId);
                        paraMap.put("channelId", item.getChannelId());
                        paraMap.put("relationFlag", relationFlag);
                        paraMap.put("companyId", companyId);
                        List<Article> list = sqlSession.selectList(
                                SqlMapper.Reception.GET_PREANDNEXTARTICLES,
                                paraMap);
                        if (null != list && !list.isEmpty())
                        {
                            for (Article obj : list)
                            {
                                if (obj.getArticleId() > articleId)
                                {
                                    item.setNextItem(obj);
                                }
                                else
                                {
                                    item.setPreItem(obj);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                // 为了防止前台页面空指针异常，new一个对象
                item = new Article();
            }
            return item;
        }
        catch (Exception e)
        {
            log.error("Fail to getArticleDetail.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 获取公告详情
     *
     * @author zhuou
     * @param commonId
     *            内容Id
     * @param isRelation
     *            是否要获取这篇公告的上一篇，下一篇内容
     * @return Announce
     * @throws PlatException
     *             公共异常
     */
    public Announce getAnnounceDetail(long commonId, boolean isRelation)
        throws PlatException
    {
        try
        {
            Map<String, Object> paraMap = new HashMap<String, Object>();
            paraMap.put("commonModelId", commonId);
            paraMap.put("tableName", Field.TableName.ANNOUNCE);
            Announce bean = sqlSession.selectOne(
                    SqlMapper.Reception.GET_ANNOUNCEDETAIL, paraMap);

            return bean;
        }
        catch (Exception e)
        {
            log.error("Fail to getAnnounceDetail.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 获取下载详情
     *
     * @author zhuou
     * @param commonId
     *            内容Id
     * @param isRelation
     *            是否要获取这篇下载的上一篇，下一篇内容
     * @return Download
     * @throws PlatException
     *             公共异常
     */
    public Download getDownloadDetail(long commonId, boolean isRelation)
        throws PlatException
    {
        try
        {
            Map<String, Object> paraMap = new HashMap<String, Object>();
            paraMap.put("commonModelId", commonId);
            paraMap.put("tableName", Field.TableName.DOWNLOAD);
            Download bean = sqlSession.selectOne(
                    SqlMapper.Reception.GET_DOWNLOADDETAIL, paraMap);

            return bean;
        }
        catch (Exception e)
        {
            log.error("Fail to getDownloadDetail.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 获取图片详情
     *
     * @author zhuou
     * @param commonId
     *            内容Id
     * @param isRelation
     *            是否要获取这篇图片的上一篇，下一篇内容
     * @return Pic
     * @throws PlatException
     *             公共异常
     */
    public Pic getPicDetail(long commonId, boolean isRelation)
        throws PlatException
    {
        try
        {
            Map<String, Object> paraMap = new HashMap<String, Object>();
            paraMap.put("commonModelId", commonId);
            paraMap.put("tableName", Field.TableName.PICURL);
            Pic bean = sqlSession.selectOne(SqlMapper.Reception.GET_PICDETAIL,
                    paraMap);
            if (null != bean)
            {
                List<String> id = new ArrayList<String>();
                id.add(String.valueOf(bean.getPicGalleryId()));

                List<PicUrl> picUrls = sqlSession.selectList(
                        SqlMapper.Reception.GETPICURLBYIDS, id);
                bean.setList(picUrls);

                if (isRelation)
                {
                    paraMap.put("picGalleryId", bean.getPicGalleryId());
                    paraMap.put("channelId", bean.getChannelId());
                    List<Pic> list = sqlSession.selectList(
                            SqlMapper.Reception.GET_PREANDNEXTPICS, paraMap);
                    if (null != list && !list.isEmpty())
                    {
                        for (Pic item : list)
                        {
                            if (item.getPicGalleryId() < bean.getPicGalleryId())
                            {
                                bean.setPreItem(item);
                            }
                            else
                            {
                                bean.setNextItem(item);
                            }
                        }
                    }
                }
            }

            return bean;
        }
        catch (Exception e)
        {
            log.error("Fail to getPicDetail.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 获取留言详情
     *
     * @author zhuou
     * @param commonId
     *            内容Id
     * @param isRelation
     *            是否要获取这篇留言的上一篇，下一篇内容
     * @return GuestBook
     * @throws PlatException
     *             公共异常
     */
    public GuestBook getGuestBookDetail(long commonId, boolean isRelation)
        throws PlatException
    {
        try
        {
            Map<String, Object> paraMap = new HashMap<String, Object>();
            paraMap.put("commonModelId", commonId);
            paraMap.put("tableName", Field.TableName.GUESTBOOK);
            GuestBook guestBook = sqlSession.selectOne(
                    SqlMapper.Reception.GET_GUESTBOOKDETAIL, paraMap);

            if (guestBook.getReplyCount() > 0)
            {
                List<GuestBookReply> replyList = getGuestBookReplybyId(guestBook
                        .getGuestBookId());
                guestBook.setReplyList(replyList);
            }
            else
            {
                guestBook.setReplyList(new ArrayList<GuestBookReply>());
            }
            return guestBook;
        }
        catch (Exception e)
        {
            log.error("Fail to getGuestBookDetail.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 获取友情链接详情
     *
     * @author zhuou
     * @param commonId
     *            内容Id
     * @param isRelation
     *            是否要获取这篇友情链接的上一篇，下一篇内容
     * @return FriendSite
     * @throws PlatException
     *             公共异常
     */
    public FriendSite getFriendSiteDetail(long commonId, boolean isRelation)
        throws PlatException
    {
        try
        {
            Map<String, Object> paraMap = new HashMap<String, Object>();
            paraMap.put("commonModelId", commonId);
            paraMap.put("tableName", Field.TableName.FRIENDSITE);
            FriendSite bean = sqlSession.selectOne(
                    SqlMapper.Reception.GET_FRIENDSITEDETAIL, paraMap);

            return bean;
        }
        catch (Exception e)
        {
            log.error("Fail to getFriendSiteDetail.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 查询评论
     *
     * @author zhuou
     * @param topNum
     *            查询多少条记录
     * @param strWhere
     *            查询条件，可以为空值
     * @param orderColumn
     *            排序列
     * @param orderType
     *            排序类型，0为升序，1为降序
     * @param commonModelId
     *            内容Id
     * @param companyId
     *            单位Id
     * @return DataList
     * @throws PlatException
     *             公共异常
     */
    public DataList getCommentNoPage(int topNum, long commonModelId,
            String strWhere, String orderColumn, boolean orderType,
            int companyId) throws PlatException
    {
        Map<String, Object> paraMap = new HashMap<String, Object>();
        paraMap.put("topNum", topNum);
        if (StringUtil.isEmpty(strWhere))
        {
            paraMap.put("strWhere", "");
        }
        else
        {
            paraMap.put("strWhere", strWhere);
        }
        paraMap.put("commonModelId", commonModelId);
        paraMap.put("orderColumn", orderColumn);
        paraMap.put("orderType", orderType);
        paraMap.put("companyId", companyId);

        DataList dataList = new DataList();
        try
        {
            List<Comment> list = sqlSession.selectList(
                    SqlMapper.Reception.GET_COMMENTNOPAGE, paraMap);

            // 为了防止前台引用报空指针异常，当为null是new一个对象
            if (null == list)
            {
                list = new ArrayList<Comment>();
            }
            dataList.setList(list);
            dataList.setTotalRecordNum(list.size());
            return dataList;
        }
        catch (Exception e)
        {
            log.error("Fail to getCommentNoPage.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 查询评论
     *
     * @author zhuou
     * @param topNum
     *            查询多少条记录
     * @param strWhere
     *            查询条件，可以为空值
     * @param orderColumn
     *            排序列
     * @param orderType
     *            排序类型，0为升序，1为降序
     * @param commonModelId
     *            内容Id
     * @param companyId
     *            单位Id
     * @param nowPage
     *            当前第几页
     * @return DataList
     * @throws PlatException
     *             公共异常
     */
    public DataList getCommentPage(int topNum, long commonModelId,
            String strWhere, String orderColumn, boolean orderType,
            int companyId, int nowPage) throws PlatException
    {
        // 计算出从第几行分页
        int rowNum = (nowPage - 1) * topNum;

        Map<String, Object> paraMap = new HashMap<String, Object>();
        paraMap.put("topNum", topNum);
        if (StringUtil.isEmpty(strWhere))
        {
            paraMap.put("strWhere", "");
        }
        else
        {
            paraMap.put("strWhere", strWhere);
        }
        paraMap.put("commonModelId", commonModelId);
        paraMap.put("orderColumn", orderColumn);
        paraMap.put("orderType", orderType);
        paraMap.put("companyId", companyId);
        paraMap.put("rowNum", rowNum);
        paraMap.put("totalRecordNum", 0);

        DataList dataList = new DataList();
        try
        {
            List<Comment> list = sqlSession.selectList(
                    SqlMapper.Reception.GET_COMMENTPAGE, paraMap);

            // 为了防止前台引用报空指针异常，当为null是new一个对象
            if (null == list)
            {
                list = new ArrayList<Comment>();
            }
            dataList.setList(list);

            // 总记录数
            int totalRNum = 0;
            Object oNum = paraMap.get("totalRecordNum");
            if (null != oNum)
            {
                totalRNum = (Integer) oNum;
            }
            dataList.setTotalRecordNum(totalRNum);
            dataList.setPageCurrentNum(nowPage);

            // 总页数
            int pageNum = totalRNum % topNum == 0 ? totalRNum / topNum
                    : totalRNum / topNum + 1;
            dataList.setPageNum(pageNum);

            dataList.setPageRecordNum(topNum);
        }
        catch (Exception e)
        {
            log.error("Fail to getCommentPage.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
        return dataList;
    }

    /**
     *
     * 根据条件查询栏目
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @param channelId
     *            栏目Id
     * @param status
     *            查询类型
     *            all查询公司下全部栏目，one-level查询全部一级栏目，fixed-all查询指定栏目及其所有子栏目，fixed-one查询指定栏目
     * @return list
     * @throws PlatException
     *             公共异常
     */
    public List<Channel> getChannel(int companyId, int channelId, String status)
        throws PlatException
    {
        try
        {
            Map<String, Object> paraMap = new HashMap<String, Object>();
            paraMap.put("companyId", companyId);
            paraMap.put("channelId", channelId);
            paraMap.put("status", status);

            List<Channel> list = sqlSession.selectList(
                    SqlMapper.Reception.GET_CHANNEL, paraMap);
            return list;
        }
        catch (Exception e)
        {
            log.error("Fail to getChannel.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 根据别名查询出栏目信息
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @param nickName
     *            别名
     * @return Channel
     */
    public Channel getChannelByNickName(int companyId, String nickName)
        throws PlatException
    {
        try
        {
            Map<String, Object> paraMap = new HashMap<String, Object>();
            paraMap.put("companyId", companyId);
            paraMap.put("nickName", nickName);

            List<Channel> list = sqlSession.selectList(
                    SqlMapper.Reception.GET_CHANNELBYNICKNAME, paraMap);
            if (null == list || list.size() <= 0)
            {
                return null;
            }

            // 获取当前栏目
            Channel channel = null;
            for (Channel item : list)
            {
                if (nickName.equals(item.getNickName()))
                {
                    channel = item;
                    break;
                }
            }

            // 获取当前栏目的父栏目和子栏目
            if (null != channel && list.size() > 1)
            {
                List<Channel> childList = new ArrayList<Channel>();
                for (Channel item : list)
                {
                    if (channel.getParentId() != Field.PARENT_ID
                            && channel.getParentId() == item.getChannelId())
                    {
                        channel.setParent(item);
                    }
                    if (item.getParentId() == channel.getChannelId())
                    {
                        childList.add(item);
                    }
                }
                channel.setChildList(childList);
            }

            return channel;
        }
        catch (Exception e)
        {
            log.error("Fail to getChannelByNickName.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 根据栏目id获取栏目信息
     *
     * @author zhuou
     * @param channelId
     *            栏目id
     * @return Channel
     * @throws PlatException
     *             公共异常
     */
    public Channel getChannelById(int channelId) throws PlatException
    {
        try
        {
            List<Channel> list = sqlSession.selectList(
                    SqlMapper.Reception.GET_CHANNELBYCHANNELID, channelId);

            // 获取当前栏目
            Channel channel = null;
            for (Channel item : list)
            {
                if (item.getChannelId() == channelId)
                {
                    channel = item;
                    break;
                }
            }

            // 获取当前栏目的父栏目和子栏目
            if (null != channel && list.size() > 1)
            {
                List<Channel> childList = new ArrayList<Channel>();
                for (Channel item : list)
                {
                    if (channel.getParentId() != Field.PARENT_ID
                            && channel.getParentId() == item.getChannelId())
                    {
                        channel.setParent(item);
                    }
                    if (item.getParentId() == channelId)
                    {
                        childList.add(item);
                    }
                }
                channel.setChildList(childList);
            }

            return channel;
        }
        catch (Exception e)
        {
            log.error("Fail to getChannelById.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 根据内容id获取栏目信息
     *
     * @author zhuou
     * @param commonModelId
     *            内容Id
     * @return Channel
     * @throws PlatException
     *             公共异常
     */
    public Channel getChannelByCommonModelId(long commonModelId)
        throws PlatException
    {
        try
        {
            Channel channel = sqlSession.selectOne(
                    SqlMapper.Reception.GET_CHANNELBYCOMMONMODELID,
                    commonModelId);
            if (null != channel)
            {
                List<Channel> list = sqlSession.selectList(
                        SqlMapper.Reception.GET_CHANNELBYCHANNELID, channel
                                .getChannelId());

                // 获取当前栏目的父栏目和子栏目
                if (list.size() > 1)
                {
                    List<Channel> childList = new ArrayList<Channel>();
                    for (Channel item : list)
                    {
                        if (channel.getParentId() != Field.PARENT_ID
                                && channel.getParentId() == item.getChannelId())
                        {
                            channel.setParent(item);
                        }
                        if (item.getParentId() == channel.getChannelId())
                        {
                            childList.add(item);
                        }
                    }
                    channel.setChildList(childList);
                }
            }

            return channel;
        }
        catch (Exception e)
        {
            log.error("Fail to getChannelByCommonModelId.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 根据广告位编码获取广告位
     *
     * @author zhuou
     * @param zoneCode
     *            广告位编码
     * @param companyId
     *            公司Id
     * @return AdZone
     * @throws PlatException
     *             公共异常
     */
    public AdZone getAdZoneByZoneCode(String zoneCode, int companyId)
    {
        try
        {
            Map<String, Object> paraMap = new HashMap<String, Object>();
            paraMap.put("zoneCode", zoneCode);
            paraMap.put("companyId", companyId);
            return this.sqlSession.selectOne(
                    SqlMapper.Reception.GETADZONEBYZONECODE, paraMap);
        }
        catch (Exception e)
        {
            log.error("Fail to getAdZoneByZoneCode info.", e);
            return null;
        }
    }

    /**
     *
     * 根据广告位获取广告
     *
     * @author zhuou
     * @param zoneId
     *            广告位
     * @param topNum
     *            取几条广告
     * @return AdZone
     */
    public AdZone getAdvertisement(int zoneId, int companyId, int topNum)
    {
        Map<String, Object> paraMap = new HashMap<String, Object>();
        paraMap.put("zoneId", zoneId);
        paraMap.put("companyId", companyId);

        AdZone adZone = sqlSession.selectOne(
                SqlMapper.Reception.GET_ADZONEBYSTATUSANDID, paraMap);
        if (null == adZone)
        {
            return new AdZone();
        }

        paraMap.put("topNum", topNum);

        List<Advertisement> list = sqlSession.selectList(
                SqlMapper.Reception.get_AdvertisementList, paraMap);

        adZone.setAds(list);
        if (null != list && !list.isEmpty())
        {
            adZone.setCount(list.size());
        }

        return adZone;
    }

    /**
     *
     * 增加广告点击数
     *
     * @author zhuou
     * @param adId
     *            广告id
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int addAdClicks(int adId) throws PlatException
    {
        try
        {
            return this.sqlSession.update(SqlMapper.Reception.ADD_ADCLICKS,
                    adId);
        }
        catch (Exception e)
        {
            log.error("Fail to addAdClicks info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 点赞
     *
     * @author zhuou
     * @param articleId
     *            文章Id
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int addPraise(long articleId) throws PlatException
    {
        try
        {
            return this.sqlSession.update(SqlMapper.Reception.ADD_PRAISE,
                    articleId);
        }
        catch (Exception e)
        {
            log.error("Fail to addPraise info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 点踩
     *
     * @author zhuou
     * @param articleId
     *            文章Id
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int addTread(long articleId) throws PlatException
    {
        try
        {
            return this.sqlSession.update(SqlMapper.Reception.ADD_TREAD,
                    articleId);
        }
        catch (Exception e)
        {
            log.error("Fail to addTread info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 新增评论
     *
     * @author zhuou
     * @param paramMap
     *            参数
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int addComment(Comment commentbean, boolean isPrivate)
        throws PlatException
    {
        try
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("commentId", commentbean.getCommentId());
            params.put("commonModelId", commentbean.getCommonModelId());
            params.put("companyId", commentbean.getCompanyId());
            params.put("userId", commentbean.getUserId());
            params.put("userName", commentbean.getUserName());
            params.put("commentTitle", commentbean.getCommentTitle());
            params.put("commentContent", commentbean.getCommentContent());
            params.put("ip", commentbean.getIp());
            params.put("isPrivate", isPrivate);
            params.put("commentType", commentbean.getCommentType());

            int rows = this.sqlSession.insert(SqlMapper.Reception.ADD_COMMENT,
                    params);
            if (rows > 0)
            {
                if (null != params.get("commentId"))
                {
                    String sId = String.valueOf(params.get("commentId"));
                    commentbean.setCommentId(Long.valueOf(sId));
                }
            }
            return rows;
        }
        catch (Exception e)
        {
            log.error("Fail to addComment info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 评论后更新T_CMS_CommonModel表的CommentCount评论总数
     *
     * @author zhuou
     * @param commonModelId
     *            内容全局ID
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updateCommentCount(long commonModelId) throws PlatException
    {
        try
        {
            return this.sqlSession.update(
                    SqlMapper.Reception.UPDATECOMMENTCOUNT, commonModelId);
        }
        catch (Exception e)
        {
            log.error("Fail to updateCommentCount info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 新增对评论的赞成
     *
     * @author zhuou
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int commentAgree(long commentId) throws PlatException
    {
        try
        {
            return this.sqlSession.update(SqlMapper.Reception.ADD_COMMENTAGREE,
                    commentId);
        }
        catch (Exception e)
        {
            log.error("Fail to commentAgree info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 新增对评论的中立
     *
     * @author zhuou
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int commentNeutral(long commentId) throws PlatException
    {
        try
        {
            return this.sqlSession.update(
                    SqlMapper.Reception.ADD_COMMENTNEUTRAL, commentId);
        }
        catch (Exception e)
        {
            log.error("Fail to commentNeutral info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 新增对评论的反对
     *
     * @author zhuou
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int commentOppose(long commentId) throws PlatException
    {
        try
        {
            return this.sqlSession.update(
                    SqlMapper.Reception.ADD_COMMENTOPPOSE, commentId);
        }
        catch (Exception e)
        {
            log.error("Fail to commentOppose info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 新增留言
     *
     * @author zhuou
     * @param bean
     *            留言
     * @param companyId
     *            公司id
     * @param status
     *            状态
     * @return int
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean addGuestbook(GuestBook bean, int companyId, int status)
        throws PlatException
    {
        try
        {
            boolean isSuccess = this.sqlSession.insert(
                    SqlMapper.Reception.ADD_GUESTBOOK, bean) > 0 ? true : false;
            if (!isSuccess)
            {
                return false;
            }

            bean.setTableName(Field.TableName.GUESTBOOK);
            CommonModel commonModel = (CommonModel) bean;
            isSuccess = addCommonModel(commonModel, companyId, status, bean
                    .getGuestBookId()) > 0 ? true : false;
            if (!isSuccess)
            {
                throw new PlatException(PageCode.GUESTBOOK_ADD_FAIL);
            }
        }
        catch (Exception e)
        {
            log.error("Fail to addGuestbook info.", e);
            throw new PlatException(PageCode.GUESTBOOK_ADD_FAIL);
        }
        return true;
    }

    /**
     *
     * 新增文章
     *
     * @author zhuou
     * @param bean
     *            文章实体bean
     * @param companyId
     *            公司Id
     * @param status
     *            状态
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean addArticle(Article bean, int companyId, int status)
        throws PlatException
    {
        try
        {
            boolean isSuccess = this.sqlSession.insert(
                    SqlMapper.Reception.ADD_ARTICLE, bean) > 0 ? true : false;
            if (!isSuccess)
            {
                return false;
            }

            bean.setTableName(Field.TableName.ARTICLE);
            CommonModel commonModel = (CommonModel) bean;
            isSuccess = addCommonModel(commonModel, companyId, status, bean
                    .getArticleId()) > 0 ? true : false;
            if (!isSuccess)
            {
                throw new PlatException(PageCode.ARTICLE_ADD_FAIL);
            }
        }
        catch (Exception e)
        {
            log.error("Fail to addArticle info.", e);
            throw new PlatException(PageCode.ARTICLE_ADD_FAIL);
        }
        return true;
    }

    /**
     *
     * 根据留言id获取留言回复列表
     *
     * @author zhuou
     * @param guestBookId
     *            留言id
     * @return List
     * @throws PlatException
     *             公共异常
     */
    public List<GuestBookReply> getGuestBookReplybyId(long guestBookId)
        throws PlatException
    {
        try
        {
            return this.sqlSession.selectList(
                    SqlMapper.Reception.GET_GUESTBOOKREPLYBYID, guestBookId);
        }
        catch (Exception e)
        {
            log.error("Fail to getGuestBookReplybyId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 新增公共内容
     *
     * @author zhuou
     * @param commonModel
     *            公共内容
     * @param companyId
     *            公司id
     * @param status
     *            状态
     * @param itemid
     *            子版块id
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int addCommonModel(CommonModel commonModel, int companyId,
            int status, long itemid) throws PlatException
    {
        try
        {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("channelId", commonModel.getChannelId());
            map.put("commentCount", commonModel.getCommentCount());
            map.put("companyId", companyId);
            map.put("dayHits", commonModel.getDayHits());
            map.put("defaultPicUrl", commonModel.getDefaultPicUrl());
            map.put("eliteLevel", commonModel.getEliteLevel());
            map.put("hits", commonModel.getHits());
            map.put("htmlUrl", commonModel.getHtmlUrl());
            map.put("inputer", commonModel.getInputer());
            map.put("isCreateHtml", commonModel.getIsCreateHtml());
            map.put("itemId", itemid);
            map.put("monthHits", commonModel.getMonthHits());
            map.put("openType", commonModel.getOpenType());
            map.put("priority", commonModel.getPriority());
            map.put("showCommentLink", commonModel.getShowCommentLink());
            map.put("status", status);
            map.put("tableName", commonModel.getTableName());
            map.put("title", commonModel.getTitle());
            map.put("titlePrefix", commonModel.getTitlePrefix());
            map.put("weekHits", commonModel.getWeekHits());
            map.put("updateTime", commonModel.getUpdateTime());
            return this.sqlSession.insert(SqlMapper.Reception.ADDCOMMONMODEL,
                    map);
        }
        catch (Exception e)
        {
            log.error("Fail to addCommonModel info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据CommonModelId查询t_cms_commonmodel一条记录
     *
     * @author zhuou
     * @param commonModelId
     *            全局内容Id
     * @return CommonModel
     * @throws PlatException
     *             公共异常
     */
    public CommonModel getCommonModelById(long commonModelId)
        throws PlatException
    {
        try
        {
            return this.sqlSession.selectOne(
                    SqlMapper.Reception.GETCOMMONMODELBYID, commonModelId);
        }
        catch (Exception e)
        {
            log.error("Fail to getCommonModelById info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 获取默认栏目
     *
     * @author zhuou
     * @param companyId
     *            单位ID
     * @param forums
     *            板块Id
     * @return Channel
     * @throws PlatException
     *             公共异常
     */
    public Channel getDefaultChannel(int companyId, String forums)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("companyId", companyId);
            param.put("forums", forums);

            return sqlSession.selectOne(SqlMapper.Reception.GETDEFAULTCHANNEL,
                    param);
        }
        catch (Exception e)
        {
            log.error("Fail to ReceptionContentDao.getDefaultChannel info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据图片大小查询图片，用于判断是否有重复的图片
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @param fileSize
     *            图片大小 KB
     * @return List
     * @throws PlatException
     *             公共异常
     */
    public List<FontFile> getFilesbySize(int companyId, int fileSize)
        throws PlatException
    {
        try
        {
            Map<String, Integer> param = new HashMap<String, Integer>();
            param.put("companyId", companyId);
            param.put("size", fileSize);
            return this.sqlSession.selectList(
                    SqlMapper.Reception.GET_FILESBYSIZE, param);
        }
        catch (Exception e)
        {
            log.error("Fail to getFilesbySize info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 保存文件信息
     *
     * @author zhuou
     * @param bean
     *            FilesBean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean saveFile(FontFile bean) throws PlatException
    {
        try
        {
            return sqlSession.delete(SqlMapper.Reception.ADD_FILE, bean) > 0 ? true
                    : false;
        }
        catch (Exception e)
        {
            log.error("Fail to ReceptionContentDao.saveFile info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据文件Id删除文件信息
     *
     * @author zhuou
     * @param filesId
     *            文件Id
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int delFileByFileId(long filesId) throws PlatException
    {
        try
        {
            return sqlSession.delete(SqlMapper.Reception.DEL_FILEBYFILEID,
                    filesId);
        }
        catch (Exception e)
        {
            log.error("Fail to ReceptionContentDao.delFileByFileId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据id查询一条文件记录
     *
     * @author zhuou
     * @param filesId
     *            文件Id
     * @return FontFile
     * @throws PlatException
     *             公共异常
     */
    public FontFile getFileById(int companyId, long filesId)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("companyId", companyId);
            param.put("filesId", filesId);
            return sqlSession
                    .selectOne(SqlMapper.Reception.GET_FILEBYID, param);
        }
        catch (Exception e)
        {
            log.error("Fail to ReceptionContentDao.getFileById info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }
}
