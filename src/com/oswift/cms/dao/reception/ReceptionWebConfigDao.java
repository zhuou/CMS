package com.oswift.cms.dao.reception;

import org.springframework.stereotype.Repository;

import com.oswift.cms.dao.BaseDao;
import com.oswift.cms.dao.SqlMapper;
import com.oswift.cms.entity.reception.WebConfig;

@Repository("receptionWebConfigDao")
public class ReceptionWebConfigDao extends BaseDao<WebConfig>
{
    public ReceptionWebConfigDao()
    {
        super(SqlMapper.ReceptionWebConfig.NAMESPACE);
    }
}
