package com.oswift.cms.dao;

import org.springframework.stereotype.Repository;

import com.oswift.cms.entity.DownloadBean;

@Repository("downloadDao")
public class DownloadDao extends BaseDao<DownloadBean>
{
    public DownloadDao()
    {
        super(SqlMapper.Download.NAMESPACE);
    }
}
