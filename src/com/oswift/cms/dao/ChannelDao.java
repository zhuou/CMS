package com.oswift.cms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.cms.entity.ChannelBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("channelDao")
public class ChannelDao extends BaseDao<ChannelBean>
{
    public ChannelDao()
    {
        super(SqlMapper.Channel.NAMESPACE);
    }

    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(ChannelDao.class);

    /**
     *
     * 添加容器栏目信息
     *
     * @author zhuou
     * @param channelBean
     *            栏目bean
     * @return int
     * @throws PlatException
     *             MS公共异常
     */
    public int saveContainCh(ChannelBean channelBean) throws PlatException
    {
        try
        {
            int rows = sqlSession.insert(SqlMapper.Channel.SAVE_CONTAINCHANNEL,
                    channelBean);
            return rows;
        }
        catch (Exception e)
        {
            log.error("Fail to save ContainChannel info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 添加单页栏目信息
     *
     * @author zhuou
     * @param channelBean
     *            栏目bean
     * @return int
     * @throws PlatException
     *             MS公共异常
     */
    public int saveSingleCh(ChannelBean channelBean) throws PlatException
    {
        try
        {
            int rows = sqlSession.insert(SqlMapper.Channel.SAVE_SINGLECHANNEL,
                    channelBean);
            return rows;
        }
        catch (Exception e)
        {
            log.error("Fail to save SingleChannel info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 添加外接栏目信息
     *
     * @author zhuou
     * @param channelBean
     *            栏目bean
     * @return int
     * @throws PlatException
     *             MS公共异常
     */
    public int saveExternalCh(ChannelBean channelBean) throws PlatException
    {
        try
        {
            int rows = sqlSession.insert(
                    SqlMapper.Channel.SAVE_EXTERNALCHANNEL, channelBean);
            return rows;
        }
        catch (Exception e)
        {
            log.error("Fail to save ExternalChannel info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据channelId获取栏目信息
     *
     * @author zhuou
     * @param channelId
     * @return 栏目bean
     * @throws PlatException
     *             CMS公共异常
     */
    public ChannelBean getChannelById(int channelId) throws PlatException
    {
        try
        {
            ChannelBean bean = sqlSession.selectOne(
                    SqlMapper.Channel.GET_CHANNELBYID, channelId);
            return bean;
        }
        catch (Exception e)
        {
            log.error("Fail to get Channel info by channelId.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据公司ID查询栏目
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @return List<ChannelBean>
     * @throws PlatException
     *             CMS公共异常
     */
    public List<ChannelBean> getChannelByCompanyId(int companyId)
        throws PlatException
    {
        try
        {
            List<ChannelBean> list = sqlSession.selectList(
                    SqlMapper.Channel.GET_CHANNELBYCOMPANYID, companyId);
            return list;
        }
        catch (Exception e)
        {
            log.error("Fail to getChannelByCompanyId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据parentId获取栏目信息
     *
     * @author zhuou
     * @param parentId
     * @return List<ChannelBean>
     * @throws PlatException
     *             CMS公共异常
     */
    public List<ChannelBean> getChannelListByParentId(int parentId)
        throws PlatException
    {
        try
        {
            List<ChannelBean> list = sqlSession.selectList(
                    SqlMapper.Channel.GET_CHANNELLISTBYPARENTID, parentId);
            return list;
        }
        catch (Exception e)
        {
            log.error("Fail to getChannelListByParentId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 查询栏目信息，当companyId=-1时查询全部栏目，如果>1则查询相关公司栏目信息
     *
     * @author zhuou
     * @param companyId
     *            公司ID
     * @param keyword
     *            查询关键字
     * @param rowNum
     *            行数
     * @param pageNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    public PageBean<ChannelBean> getChannelList(int companyId, String keyword,
            int rowNum, int pageNum) throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("keyword", keyword);
            param.put("rowNum", rowNum);
            param.put("pageNum", pageNum);
            param.put("companyId", companyId);

            List<ChannelBean> dataList = sqlSession.selectList(
                    SqlMapper.Channel.GET_CHANNELLIST, param);

            int totalRecordNum = (Integer) sqlSession.selectOne(
                    SqlMapper.Channel.GET_CHANNELNUM, param);

            PageBean<ChannelBean> pageBean = new PageBean<ChannelBean>();
            pageBean.setTotalRecordNum(totalRecordNum);
            pageBean.setDataList(dataList);

            return pageBean;
        }
        catch (Exception e)
        {
            log.error("Fail to getChannelList info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据公司ID和forums板块查询栏目列表
     *
     * @author zhuou
     * @param companyId
     * @param forums
     * @return List<ChannelBean>
     * @throws PlatException
     *             CMS公共异常
     */
    public List<ChannelBean> getChannel2(int companyId, String forums)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("companyId", companyId);
            param.put("forums", forums);

            List<ChannelBean> list = sqlSession.selectList(
                    SqlMapper.Channel.GET_CHANNEL2, param);
            return list;
        }
        catch (Exception e)
        {
            log.error("Fail to getChannel2 info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据公司ID查询出没有外接栏目的所有栏目
     *
     * @author zhuou
     * @param companyId
     *            公司ID
     * @return list
     * @throws PlatException
     *             CMS公共异常
     */
    public List<ChannelBean> getChannel3(int companyId) throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("companyId", companyId);

            List<ChannelBean> list = sqlSession.selectList(
                    SqlMapper.Channel.GET_CHANNEL3, param);
            return list;
        }
        catch (Exception e)
        {
            log.error("Fail to getChannel3 info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 查询出channelId最大的栏目
     *
     * @author zhuou
     * @return ChannelBean
     * @throws PlatException
     *             公共异常
     */
    public ChannelBean getTop1Channel() throws PlatException
    {
        try
        {

            return sqlSession
                    .selectOne(SqlMapper.Channel.GET_TOP1CHANNEL, null);
        }
        catch (Exception e)
        {
            log.error("Fail to getTop1Channel info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 获取默认栏目
     *
     * @author zhuou
     * @param companyId
     *            单位ID
     * @param forums
     *            板块Id
     * @return ChannelBean
     * @throws PlatException
     *             公共异常
     */
    public ChannelBean getDefaultChannel(int companyId, String forums)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("companyId", companyId);
            param.put("forums", forums);

            return sqlSession.selectOne(SqlMapper.Channel.GET_DEFAULTCHANNEL,
                    param);
        }
        catch (Exception e)
        {
            log.error("Fail to getDefaultChannel info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据companyId获取所有栏目的Forums集合
     *
     * @author zhuou
     * @param companyId
     *            单位ID
     * @return String
     * @throws PlatException
     *             公共异常
     */
    public String getChannelForums(int companyId) throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("companyId", companyId);

            return sqlSession.selectOne(SqlMapper.Channel.GET_CHANNELFORUMS,
                    param);
        }
        catch (Exception e)
        {
            log.error("Fail to getChannelForums info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据ChannelId获取此栏目的Forums
     *
     * @author zhuou
     * @param channelId
     *            栏目Id
     * @return String
     * @throws PlatException
     *             公共异常
     */
    public String getChannelForumsById(int channelId) throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("channelId", channelId);

            return sqlSession.selectOne(
                    SqlMapper.Channel.GET_CHANNELFORUMSBYID, param);
        }
        catch (Exception e)
        {
            log.error("Fail to getChannelForumsById info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据栏目Id和TableName修改t_cms_commonmodel，应用场景：当删除栏目时，会相应修改内容栏目ID字段值
     *
     * @author zhuou
     * @param newChannelId
     *            新栏目ID
     * @param oldChannelId
     *            要删除的栏目Id
     * @param tableName
     *            表名称
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updateCommonModel(int newChannelId, int oldChannelId,
            String tableName) throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("newChannelId", newChannelId);
            param.put("oldChannelId", oldChannelId);
            param.put("tableName", tableName);

            return sqlSession
                    .update(SqlMapper.Channel.UPDATECOMMONMODEL, param);
        }
        catch (Exception e)
        {
            log.error("Fail to updateCommonModel info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 当删除某个栏目时，修改此栏目的子栏目的ParentId
     *
     * @author zhuou
     * @param oldParentId
     *            父栏目id
     * @param newParentId
     *            新的父栏目id
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updateParentId(int oldParentId, int newParentId)
        throws PlatException
    {
        try
        {
            Map<String, Integer> param = new HashMap<String, Integer>();
            param.put("oldParentId", oldParentId);
            param.put("newParentId", newParentId);

            return sqlSession.update(SqlMapper.Channel.UPDATEPARENTID, param);
        }
        catch (Exception e)
        {
            log.error("Fail to updateParentId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 批量更新是否有子节点字段
     *
     * @author zhuou
     * @param param
     *            参数isChild、channelId
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updateChildFlag(Map<String, Object> param) throws PlatException
    {
        if (null == param || param.isEmpty())
        {
            return 0;
        }

        try
        {
            return sqlSession.update(SqlMapper.Channel.UPDATECHILDFLAG, param);
        }
        catch (Exception e)
        {
            log.error("Fail to updateChildFlag info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }
}
