package com.oswift.cms.dao;

/**
 *
 * 定义sql名称
 *
 * @author zhuou
 * @version C03 Oct 11, 2012
 * @since OSwift GPM V1.0
 */
public interface SqlMapper
{
    public interface Announce
    {
        public static final String NAMESPACE = "cms.announceMapper";
    }

    public interface Area
    {
        public static final String NAMESPACE = "cms.areaMapper";

        public static final String GETPROVINCE = NAMESPACE + ".getProvince";

        public static final String GETCITY = NAMESPACE + ".getCity";

        public static final String GETDISTRICT = NAMESPACE + ".getDistrict";

        public static final String GETCITYBYID = NAMESPACE + ".getCityById";

        public static final String GETDISTRICTBYID = NAMESPACE
                + ".getDistrictById";
    }

    public interface Article
    {
        public static final String NAMESPACE = "cms.articleMapper";
    }

    public interface Channel
    {
        public static final String NAMESPACE = "cms.channelMapper";

        public static final String SAVE_CONTAINCHANNEL = NAMESPACE
                + ".saveContainChannel";

        public static final String SAVE_SINGLECHANNEL = NAMESPACE
                + ".saveSingleChannel";

        public static final String SAVE_EXTERNALCHANNEL = NAMESPACE
                + ".saveExternalChannel";

        public static final String GET_CHANNELBYID = NAMESPACE
                + ".getChannelById";

        // public static final String UPDATE_ARRROOT = NAMESPACE
        // + ".updateArrRoot";

        public static final String GET_CHANNELBYCOMPANYID = NAMESPACE
                + ".getChannelByCompanyId";

        public static final String GET_CHANNELLISTBYPARENTID = NAMESPACE
                + ".getChannelListByParentId";

        public static final String GET_CHANNELLIST = NAMESPACE
                + ".getChannelList";

        public static final String GET_CHANNELNUM = NAMESPACE
                + ".getChannelNum";

        public static final String GET_CHANNEL2 = NAMESPACE + ".getChannel2";

        /**
         * 根据公司ID查询出没有外接栏目的所有栏目
         */
        public static final String GET_CHANNEL3 = NAMESPACE + ".getChannel3";

        /**
         * 查询出channelId最大的栏目
         */
        public static final String GET_TOP1CHANNEL = NAMESPACE
                + ".getTop1Channel";

        /**
         * 获取默认栏目信息
         */
        public static final String GET_DEFAULTCHANNEL = NAMESPACE
                + ".getDefaultChannel";

        /**
         * 根据companyId获取所有栏目的Forums集合
         */
        public static final String GET_CHANNELFORUMS = NAMESPACE
                + ".getChannelForums";

        /**
         * 根据ChannelId获取此栏目的Forums
         */
        public static final String GET_CHANNELFORUMSBYID = NAMESPACE
                + ".getChannelForumsById";

        /**
         * 根据栏目Id和TableName修改t_cms_commonmodel，应用场景：当删除栏目时，会相应修改内容栏目ID字段值
         */
        public static final String UPDATECOMMONMODEL = NAMESPACE
                + ".updateCommonModel";

        /**
         * 当删除某个栏目时，修改此栏目的子栏目的ParentId
         */
        public static final String UPDATEPARENTID = NAMESPACE
                + ".updateParentId";

        /**
         * 批量更新是否有子节点字段
         */
        public static final String UPDATECHILDFLAG = NAMESPACE
                + ".updateChildFlag";
    }

    public interface CommonModel
    {
        public static final String NAMESPACE = "cms.commonModelMapper";

        public static final String GET_COMMONMODEL = NAMESPACE
                + ".getCommonModel";

        public static final String GET_COUNTCOMMONMODEL = NAMESPACE
                + ".getCountCommonModel";

        public static final String UPDATE_STATUS = NAMESPACE + ".updateStatus";

        /**
         * 更新天点击率
         */
        public static final String UPDATE_DAYHIT = NAMESPACE + ".updateDayHit";

        /**
         * 更新周点击率
         */
        public static final String UPDATE_WEEKHIT = NAMESPACE + ".updateWeekHit";

        /**
         * 更新月点击率
         */
        public static final String UPDATE_MONTHHIT = NAMESPACE + ".updateMonthHit";
    }

    public interface Company
    {
        public static final String NAMESPACE = "cms.companyMapper";

        public static final String GET_COMPANYINDUSTRY = NAMESPACE
                + ".getCompanyIndustry";

        public static final String GET_COMPANYPROPERTIE = NAMESPACE
                + ".getCompanyPropertie";

        public static final String GET_COMPANYSCALE = NAMESPACE
                + ".getCompanyScale";

        public static final String GET_COMPANYLIST = NAMESPACE
                + ".getCompanyList";

        public static final String GET_COMPANYNUM = NAMESPACE
                + ".getCompanyNum";

        public static final String GET_COMPANYSBYCID = NAMESPACE
                + ".getCompanysbyCId";
    }

    public interface Dictionary
    {
        public static final String NAMESPACE = "cms.dictionaryMapper";

        public static final String GET_CACHEDICTIONARY = NAMESPACE
                + ".getCacheDictionary";

        /**
         * 根据FieldName查找字段数据信息
         */
        public static final String GET_DICTIONARYBYFIELDNAME = NAMESPACE
                + ".getDictionaryByFieldName";

        /**
         * 根据Title查找字段数据信息
         */
        public static final String GET_DICTIONARYBYTITLE = NAMESPACE
                + ".getDictionaryByTitle";
    }

    public interface Download
    {
        public static final String NAMESPACE = "cms.downloadMapper";
    }

    public interface FriendSite
    {
        public static final String NAMESPACE = "cms.friendSiteMapper";
    }

    public interface GuestBook
    {
        public static final String NAMESPACE = "cms.guestBook";

        /**
         * 新增回复
         */
        public static final String ADD_REPLY = NAMESPACE + ".addReply";

        /**
         * 修改回复
         */
        public static final String UPDATE_REPLY = NAMESPACE + ".updateReply";

        /**
         * 删除一条回复内容
         */
        public static final String DEL_REPLY = NAMESPACE + ".delReply";

        /**
         * 根据留言Id删除留言下全部回复信息
         */
        public static final String DEL_REPLYBYGBID = NAMESPACE
                + ".delReplyByGBId";

        /**
         * 根据留言Id获取回复
         */
        public static final String GET_REPLYLIST = NAMESPACE + ".getReplyList";

        /**
         * 根据回复Id回去回复详情
         */
        public static final String GET_REPLYBYRID = NAMESPACE
                + ".getReplyByRId";

        /**
         * 获取留言回复，有分页
         */
        public static final String GET_REPLYSPAGECURRENT = NAMESPACE
                + ".getReplysPageCurrent";

        /**
         * 获取留言回复数量
         */
        public static final String GET_REPLYSPAGECOUNT = NAMESPACE
                + ".getReplysPageCount";

        /**
         * 根据留言Id获取留言详细信息
         */
        public static final String GET_GUESTBOOKBYGID = NAMESPACE
                + ".getGuestBookByGId";

        /**
         * 修改留言回复数
         */
        public static final String UPDATE_GUESTBOOKREPLYNUM = NAMESPACE
                + ".updateGuestBookReplyNum";

        /**
         * 获取留言列表
         */
        public static final String GETGUESTBOOKS = NAMESPACE + ".getGuestBooks";

        /**
         * 获取留言列表数目
         */
        public static final String GETCOUNTGUESTBOOKS = NAMESPACE
                + ".getCountGuestBooks";
    }

    public interface Pic
    {
        public static final String NAMESPACE = "cms.picMapper";

        /**
         * 添加图片地址信息
         */
        public static final String ADD_PICURL = NAMESPACE + ".addPicUrl";

        /**
         * 删除图片地址信息
         */
        public static final String DEL_PICURL = NAMESPACE + ".delPicUrl";

        /**
         * 根据PicGalleryId获取图片地址
         */
        public static final String GET_PICURLLIST = NAMESPACE
                + ".getPicUrlList";
    }

    public interface UserGroup
    {
        public static final String NAMESPACE = "cms.userGroupMapper";

        public static final String GET_USERGROUPBYCOMPANYID = NAMESPACE
                + ".getUserGroupByCompanyId";

        /**
         * 根据id查询用户组
         */
        public static final String GET_USERGROUPBYIDS = NAMESPACE
                + ".getUserGroupByIds";
    }

    public interface Member
    {
        public static final String NAMESPACE = "cms.userMapper";

        /**
         * 新增用户扩展信息
         */
        public static final String ADD_CONTACTER = NAMESPACE
                + ".addMemberContacter";

        /**
         * 删除
         */
        public static final String DEL_CONTACTER = NAMESPACE + ".delContacter";

        /**
         * 用户名是否存在
         */
        public static final String ISEXISTBYUSERNAME = NAMESPACE
                + ".isExistByUserName";

        /**
         * 用户的邮箱是否存在
         */
        public static final String ISEXISTBYEMAIL = NAMESPACE
                + ".isExistByEmail";

        /**
         * 修改member
         */
        public static final String UPDATE_MEMBERCONTACTER = NAMESPACE
                + ".updateMemberContacter";

        /**
         * 根据companyId获取默认的用户组
         */
        public static final String GETDEFAULTUSERGROUP = NAMESPACE
                + ".getDefaultUserGroup";

        /**
         * 根据userName查询用户信息
         */
        public static final String GETUSERBYUSERNAME = NAMESPACE
                + ".getUserByUserName";

        /**
         * 根据Email查询用户信息
         */
        public static final String GETUSERBYEMAIL = NAMESPACE
                + ".getUserByEmail";

        /**
         * 登录修改表中的相关字段
         */
        public static final String UPDATEMEMBERWHENLOGIN = NAMESPACE
                + ".updateMemberWhenLogin";
    }

    public interface Comment
    {
        public static final String NAMESPACE = "cms.commentMapper";

        /**
         * 修改是否精华状态
         */
        public static final String UPDATE_ELITESTATUS = NAMESPACE
                + ".updateEliteStatus";

        /**
         * 修改是否公开状态
         */
        public static final String UPDATE_PRIVATESTATUS = NAMESPACE
                + ".updatePrivateStatus";
    }

    /**
     *
     * 前台sql
     *
     * @author zhuou
     * @version C03 2013-8-27
     * @since OSwift GPM V1.0
     */
    public interface Reception
    {
        public static final String NAMESPACE = "cms.receptionMapper";

        /**
         * 无分页查询文章内容存储过程
         */
        public static final String GET_ARTICLENOPAGE = NAMESPACE
                + ".getArticleNoPage";

        /**
         * 无分页查询公告内容存储过程
         */
        public static final String GET_ANNOUNCESNOPAGE = NAMESPACE
                + ".getAnnouncesNoPage";

        /**
         * 无分页查询下载内容存储过程
         */
        public static final String GET_DOWNLOADSNOPAGE = NAMESPACE
                + ".getDownloadsNoPage";

        /**
         * 无分页查询友情链接内容存储过程
         */
        public static final String GET_FRIENDSITESNOPAGE = NAMESPACE
                + ".getFriendSitesNoPage";

        /**
         * 无分页查询留言内容存储过程
         */
        public static final String GET_GUESTBOOKSNOPAGE = NAMESPACE
                + ".getGuestBooksNoPage";

        /**
         * 无分页查询图片内容存储过程
         */
        public static final String GET_PICSNOPAGE = NAMESPACE
                + ".getPicsNoPage";

        /**
         * 带分页查询文章内容存储过程
         */
        public static final String GET_ARTICLEPAGE = NAMESPACE
                + ".getArticlePage";

        /**
         * 带分页查询文章公告存储过程
         */
        public static final String GET_ANNOUNCESPAGE = NAMESPACE
                + ".getAnnouncesPage";

        /**
         * 带分页查询下载内容存储过程
         */
        public static final String GET_DOWNLOADSPAGE = NAMESPACE
                + ".getDownloadsPage";

        /**
         * 带分页查询友情链接内容存储过程
         */
        public static final String GET_FRIENDSITESPAGE = NAMESPACE
                + ".getFriendSitesPage";

        /**
         * 带分页查询留言内容存储过程
         */
        public static final String GET_GUESTBOOKSPAGE = NAMESPACE
                + ".getGuestBooksPage";

        /**
         * 带分页查询图片内容存储过程
         */
        public static final String GET_PICSPAGE = NAMESPACE + ".getPicsPage";

        /**
         * 文章详情
         */
        public static final String GET_ARTICLEDETAIL = NAMESPACE
                + ".getArticleDetail";

        /**
         * 公告详情
         */
        public static final String GET_ANNOUNCEDETAIL = NAMESPACE
                + ".getAnnounceDetail";

        /**
         * 下载详情
         */
        public static final String GET_DOWNLOADDETAIL = NAMESPACE
                + ".getDownloadDetail";

        /**
         * 友情链接详情
         */
        public static final String GET_FRIENDSITEDETAIL = NAMESPACE
                + ".getFriendSiteDetail";

        /**
         * 留言详情
         */
        public static final String GET_GUESTBOOKDETAIL = NAMESPACE
                + ".getGuestBookDetail";

        /**
         * 图片详情
         */
        public static final String GET_PICDETAIL = NAMESPACE + ".getPicDetail";

        /**
         * 无分页评论列表
         */
        public static final String GET_COMMENTNOPAGE = NAMESPACE
                + ".getCommentNoPage";

        /**
         * 有分页评论
         */
        public static final String GET_COMMENTPAGE = NAMESPACE
                + ".getCommentPage";

        /**
         * 根据条件查询栏目
         */
        public static final String GET_CHANNEL = NAMESPACE + ".getChannel";

        /**
         * 根据别名查询出栏目信息
         */
        public static final String GET_CHANNELBYNICKNAME = NAMESPACE
                + ".getChannelByNickName";

        /**
         * 根据栏目id查询栏目信息
         */
        public static final String GET_CHANNELBYCHANNELID = NAMESPACE
                + ".getChannelByChannelId";

        /**
         * 根据内容id获取栏目信息
         */
        public static final String GET_CHANNELBYCOMMONMODELID = NAMESPACE
                + ".getChannelByCommonModelId";

        /**
         * 查询和内容相关链的上一篇和下一篇文章
         */
        public static final String GET_PREANDNEXTARTICLES = NAMESPACE
                + ".getPreAndNextArticles";

        /**
         * 查询和内容相关链的上一篇和下一篇图片
         */
        public static final String GET_PREANDNEXTPICS = NAMESPACE
                + ".getPreAndNextPics";

        /**
         * 根据id获取广告位
         */
        public static final String GET_ADZONEBYID = NAMESPACE
                + ".getAdZoneById";

        /**
         * 根据状态、id、公司id获取广告位
         */
        public static final String GET_ADZONEBYSTATUSANDID = NAMESPACE
                + ".getAdZoneByStatusAndId";

        /**
         * 根据广告位获取广告列表
         */
        public static final String get_AdvertisementList = NAMESPACE
                + ".getAdvertisementList";

        /**
         * 广告点击数
         */
        public static final String ADD_ADCLICKS = NAMESPACE + ".AddAdClicks";

        /**
         * 新增评论
         */
        public static final String ADD_COMMENT = NAMESPACE + ".addComment";

        /**
         * 支持
         */
        public static final String ADD_COMMENTAGREE = NAMESPACE
                + ".addCommentAgree";

        /**
         * 中立
         */
        public static final String ADD_COMMENTNEUTRAL = NAMESPACE
                + ".addCommentNeutral";

        /**
         * 反对
         */
        public static final String ADD_COMMENTOPPOSE = NAMESPACE
                + ".addCommentOppose";

        /**
         * 点赞
         */
        public static final String ADD_PRAISE = NAMESPACE + ".addPraise";

        /**
         * 点踩
         */
        public static final String ADD_TREAD = NAMESPACE + ".addTread";

        /**
         * 新增留言
         */
        public static final String ADD_GUESTBOOK = NAMESPACE + ".addGuestbook";

        /**
         * 新增文章
         */
        public static final String ADD_ARTICLE = NAMESPACE + ".addArticle";

        /**
         * 根据留言id获取留言回复列表
         */
        public static final String GET_GUESTBOOKREPLYBYID = NAMESPACE
                + ".getGuestBookReplybyId";

        /**
         * 根据留言ids获取留言回复列表
         */
        public static final String GETGUESTBOOKREPLYBYIDS = NAMESPACE
                + ".getGuestBookReplybyIds";

        /**
         * 根据图片id查询图片url列表
         */
        public static final String GETPICURLBYIDS = NAMESPACE
                + ".getPicUrlbyIds";

        /**
         * 前台页面新增文章、图片、留言、友情链接、公告、下载内容
         */
        public static final String ADDCOMMONMODEL = NAMESPACE
                + ".addCommonModel";

        /**
         * 评论后更新T_CMS_CommonModel表的CommentCount评论总数
         */
        public static final String UPDATECOMMENTCOUNT = NAMESPACE
                + ".updateCommentCount";

        /**
         * 根据CommonModelId查询t_cms_commonmodel一条记录
         */
        public static final String GETCOMMONMODELBYID = NAMESPACE
                + ".getCommonModelById";

        /**
         * 获取默认栏目信息
         */
        public static final String GETDEFAULTCHANNEL = NAMESPACE
                + ".getDefaultChannel";

        /**
         * 新增上传文件信息
         */
        public static final String ADD_FILE = NAMESPACE
                + ".addFile";

        /**
         * 根据图片大小查询图片，用于判断是否有重复的图片
         */
        public static final String GET_FILESBYSIZE = NAMESPACE
                + ".getFilesbySize";

        /**
         * 根据文件Id删除文件信息
         */
        public static final String DEL_FILEBYFILEID = NAMESPACE
                + ".delFileByFileId";

        /**
         * 根据id查询一条文件记录
         */
        public static final String GET_FILEBYID = NAMESPACE
                + ".getFileById";

        /**
         * 根据广告位编码获取广告位
         */
        public static final String GETADZONEBYZONECODE = NAMESPACE
                + ".getAdZoneByZoneCode";
    }

    /**
     *
     * 页面功能sql
     *
     * @author zhuou
     * @version C03 2013-8-28
     * @since OSwift GPM V1.0
     */
    // public interface ReceptionPageFun
    // {
    // public static final String NAMESPACE = "cms.receptionPageFunMapper";
    //
    // /**
    // * 获取所有前台页面
    // */
    // public static final String GET_RECEPTIONPAGELIST = NAMESPACE
    // + ".getReceptionPageList";
    //
    // /**
    // * 根据PageId获取页面所有功能
    // */
    // public static final String GET_RECEPTIONPAGEFUNLIST = NAMESPACE
    // + ".getReceptionPageFunList";
    //
    // }
    /**
     *
     * 前台缓存web站点信息
     *
     * @author zhuou
     * @version C03 2013-8-30
     * @since OSwift GPM V1.0
     */
    public interface ReceptionWebConfig
    {
        public static final String NAMESPACE = "cms.receptionWebConfigMapper";
    }

    /**
     *
     * 后台管理前台页面的配置
     *
     * @author zhuou
     * @version C03 2013-8-30
     * @since OSwift GPM V1.0
     */
    // public interface Page
    // {
    // public static final String NAMESPACE = "cms.pageMapper";
    //
    // /**
    // * 添加站点前台页面
    // */
    // public static final String ADD_PAGE = NAMESPACE + ".addPage";
    //
    // /**
    // * 添加站点前台页面中功能
    // */
    // public static final String ADD_PAGEFUN = NAMESPACE + ".addPageFun";
    //
    // /**
    // * 根据pageId删除t_cms_pagefunction表记录
    // */
    // public static final String DEL_PAGEFUNBYPAGEID = NAMESPACE
    // + ".delPageFunByPageId";
    //
    // /**
    // * 根据pageId删除t_cms_page表一条记录
    // */
    // public static final String DEL_PAGEBYPAGEID = NAMESPACE
    // + ".delPageByPageId";
    //
    // /**
    // * 根据channelId，companyId，functionPageId是否存在页面
    // */
    // public static final String SELECT_ISEXIST = NAMESPACE
    // + ".selectIsExist";
    //
    // /**
    // * 根据PageId查询页面信息
    // */
    // public static final String GET_RECEPTIONPAGE = NAMESPACE
    // + ".getReceptionPage";
    //
    // /**
    // * 根据PageId查询页面功能列表信息
    // */
    // public static final String GET_RECEPTIONPAGEFUNLIST = NAMESPACE
    // + ".getReceptionPageFunList";
    //
    // /**
    // * 根据functionId删除一条功能记录
    // */
    // public static final String DEL_PAGEFUNBYFUNID = NAMESPACE
    // + ".delPageFunByFunId";
    //
    // /**
    // * 根据functionId获取一条页面功能信息
    // */
    // public static final String GET_ONERECEPTIONPAGEFUN = NAMESPACE
    // + ".getOneReceptionPageFun";
    //
    // /**
    // * 根据siteId删除t_cms_page表信息
    // */
    // public static final String DEL_PAGEBYSITEID = NAMESPACE
    // + ".delPageBySiteId";
    //
    // /**
    // * 根据siteId删除t_cms_pagefunction表信息
    // */
    // public static final String DEL_PAGEFUNBYSITEID = NAMESPACE
    // + ".delPageFunBySiteId";
    // }
    /**
     *
     * 后台对T_CMS_WebSiteConfig表的操作
     *
     * @author zhuou
     * @version C03 2013-9-9
     * @since OSwift GPM V1.0
     */
    public interface BackWebConfig
    {
        public static final String NAMESPACE = "cms.webConfigMapper";

        /**
         * 获取网站配置信息
         */
        public static final String GET_WEBCONFIGLIST = NAMESPACE
                + ".getWebConfigList";

        /**
         * 获取网站配置信息记录数
         */
        public static final String GET_WEBCONFIGNUM = NAMESPACE
                + ".getWebConfigNum";

        /**
         * 根据公司Id获取站点信息
         */
        public static final String GET_WEBCONFIGBYCOMPANYID = NAMESPACE
                + ".getWebConfigByCompanyId";
    }

    /**
     *
     * 上传文件管理
     *
     * @author zhuou
     * @version C03 2013-10-28
     * @since OSwift GPM V1.0
     */
    public interface Files
    {
        public static final String NAMESPACE = "cms.filesMapper";
    }

    /**
     *
     * 前台页面参数配置
     *
     * @author zhuou
     * @version C03 2014-1-12
     * @since OSwift GPM V1.0
     */
    // public interface OtherPage
    // {
    // public static final String NAMESPACE = "cms.otherPageMapper";
    //
    // /**
    // * 根据siteId删除t_cms_otherpage表信息
    // */
    // public static final String DEL_OTHERPAGEBYSITEID = NAMESPACE
    // + ".delOtherPageBySiteId";
    // }
    /**
     *
     * 广告位
     *
     * @author zhuou
     * @version C03 2014-3-5
     * @since OSwift GPM V1.0
     */
    public interface AdZone
    {
        public static final String NAMESPACE = "cms.adZoneMapper";

        /**
         * 修改广告位状态
         */
        public static final String UPDATESTATUS = NAMESPACE + ".updateStatus";

        /**
         * 根据companyId获取广告位列表
         */
        public static final String GETADZONELISTBYCOMPANYID = NAMESPACE
                + ".getAdZoneListByCompanyId";

        /**
         * 根据CompanyId查询出主键值最大的一条记录
         */
        public static final String GETTOP1BYCOMPANYID = NAMESPACE
                + ".getTop1ByCompanyId";

        /**
         * 根据ZoneCode和公司Id查询出一条记录
         */
        public static final String GETONEBYZONECODE = NAMESPACE
                + ".getOneByZoneCode";
    }

    /**
     *
     * 广告
     *
     * @author zhuou
     * @version C03 2014-3-5
     * @since OSwift GPM V1.0
     */
    public interface Advertisement
    {
        public static final String NAMESPACE = "cms.advertisementMapper";

        /**
         * 根据广告位id删除广告信息
         */
        public static final String DEL_ADVERTISEMENTBYZONEID = NAMESPACE
                + ".delAdvertisementByZoneId";

        /**
         * 修改广告状态
         */
        public static final String UPDATEADSTATUS = NAMESPACE
                + ".updateAdStatus";
    }

    /**
     *
     * 前台页面url配置
     *
     * @author zhuou
     * @version C03 2014-6-13
     * @since OSwift GPM V2.0
     */
    public interface ReceptionPageUrl
    {
        public static final String NAMESPACE = "cms.receptionPageUrl";

        /**
         * 根据站点id查询出前台url所有配置
         */
        public static final String GET_RECEPTIONPAGEURLLISTBYSITEID = NAMESPACE
                + ".getReceptionPageUrlListBySiteId";

        /**
         * 根据站点id删除前台url所有配置
         */
        public static final String DEL_ALLBYSITEID = NAMESPACE
                + ".delAllBySiteId";

        /**
         * 获取所有前台url配置数据
         */
        public static final String GET_ALLRECEPTIONPAGEURL = NAMESPACE
                + ".getAllReceptionPageUrl";

        /**
         * 根据站点id、url类型、url别称查找数据是否存在
         */
        public static final String DATAISEXIST = NAMESPACE + ".dataIsExist";
    }
}
