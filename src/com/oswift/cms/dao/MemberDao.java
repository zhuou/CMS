package com.oswift.cms.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.cms.entity.MemberBean;
import com.oswift.cms.entity.UserGroupBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

/**
 *
 * 站点会员操作Dao类
 *
 * @author zhuou
 * @version C03 2013-11-4
 * @since OSwift GPM V1.0
 */
@Repository("memberDao")
public class MemberDao extends BaseDao<MemberBean>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(MemberDao.class);

    public MemberDao()
    {
        super(SqlMapper.Member.NAMESPACE);
    }

    /**
     *
     * 新增用户扩展信息
     *
     * @author zhuou
     * @param memberBean
     *            用户bean
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int addContacter(MemberBean memberBean) throws PlatException
    {
        try
        {
            return sqlSession
                    .insert(SqlMapper.Member.ADD_CONTACTER, memberBean);
        }
        catch (Exception e)
        {
            log.error("Fail to addContacter.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     *
     * 根据UserId删除会员扩展表
     *
     * @author zhuou
     * @param userId
     *            会员Id
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int delContacter(long userId) throws PlatException
    {
        try
        {
            return sqlSession.delete(SqlMapper.Member.DEL_CONTACTER, userId);
        }
        catch (Exception e)
        {
            log.error("Fail to delContacter.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 用户名是否存在
     *
     * @author zhuou
     * @param userName
     *            用户名
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean isExistByUserName(String userName, int companyId)
        throws PlatException
    {
        try
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("userName", userName);
            params.put("companyId", companyId);
            int count = (Integer) sqlSession.selectOne(
                    SqlMapper.Member.ISEXISTBYUSERNAME, params);
            return count > 0 ? true : false;
        }
        catch (Exception e)
        {
            log.error("Fail to isExistByUserName.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 判断用户的Email是否存在，用户注册时不能有重复的邮箱
     *
     * @author zhuou
     * @param email
     *            Email
     * @param companyId
     *            公司Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean isExistByEmail(String email, int companyId)
        throws PlatException
    {
        try
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("email", email);
            params.put("companyId", companyId);
            int count = (Integer) sqlSession.selectOne(
                    SqlMapper.Member.ISEXISTBYEMAIL, params);
            return count > 0 ? true : false;
        }
        catch (Exception e)
        {
            log.error("Fail to isExistByEmail.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 修改member
     *
     * @author zhuou
     * @param memberBean
     *            会员bean
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updateMemberContacter(MemberBean memberBean)
        throws PlatException
    {
        try
        {
            return sqlSession.update(SqlMapper.Member.UPDATE_MEMBERCONTACTER,
                    memberBean);
        }
        catch (Exception e)
        {
            log.error("Fail to updateMemberContacter.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据companyId获取默认的用户组
     *
     * @author zhuou
     * @param companyId
     *            公司Id
     * @return UserGroupBean
     * @throws PlatException
     *             公共异常
     */
    public UserGroupBean getDefaultUserGroup(int companyId)
        throws PlatException
    {
        try
        {
            return sqlSession.selectOne(SqlMapper.Member.GETDEFAULTUSERGROUP,
                    companyId);
        }
        catch (Exception e)
        {
            log.error("Fail to getDefaultUserGroup.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据userName查询用户信息
     *
     * @author zhuou
     * @param userName
     *            用户名
     * @param companyId
     *            公司id
     * @return MemberBean
     * @throws PlatException
     *             公共异常
     */
    public MemberBean getUserByUserName(String userName, int companyId)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("userName", userName);
            param.put("companyId", companyId);
            return sqlSession.selectOne(SqlMapper.Member.GETUSERBYUSERNAME,
                    param);
        }
        catch (Exception e)
        {
            log.error("Fail to getUserByUserName.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据Email查询用户信息
     *
     * @author zhuou
     * @param email
     *            用户Email
     * @param companyId
     *            公司Id
     * @return MemberBean
     * @throws PlatException
     *             公共异常
     */
    public MemberBean getUserByEmail(String email, int companyId)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("email", email);
            param.put("companyId", companyId);
            return sqlSession.selectOne(SqlMapper.Member.GETUSERBYEMAIL, param);
        }
        catch (Exception e)
        {
            log.error("Fail to getUserByEmail.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 登录修改表中的相关字段
     *
     * @author zhuou
     * @param loginTimes
     *            登录次数 可为null
     * @param lastLoginTime
     *            最后登录时间
     * @param lastLoginIP
     *            最后登录IP
     * @param failedPasswordAttemptCount
     *            失败登录次数 可为null
     * @param userId
     *            用户Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean updateMemberWhenLogin(Integer loginTimes,
            Date lastLoginTime, String lastLoginIP,
            Integer failedPasswordAttemptCount, Long userId)
        throws PlatException
    {
        try
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params
                    .put("failedPasswordAttemptCount",
                            failedPasswordAttemptCount);
            params.put("loginTimes", loginTimes);
            params.put("lastLoginTime", lastLoginTime);
            params.put("lastLoginIP", lastLoginIP);
            params.put("userId", userId);

            return sqlSession.update(SqlMapper.Member.UPDATEMEMBERWHENLOGIN,
                    params) > 0 ? true : false;
        }
        catch (Exception e)
        {
            log.error("Fail to updateMemberWhenLogin.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }
}
