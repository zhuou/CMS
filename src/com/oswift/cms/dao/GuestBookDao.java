package com.oswift.cms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.cms.entity.GuestBookBean;
import com.oswift.cms.entity.GuestBookReplyBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("guestBookDao")
public class GuestBookDao extends BaseDao<GuestBookBean>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(GuestBookDao.class);

    public GuestBookDao()
    {
        super(SqlMapper.GuestBook.NAMESPACE);
    }

    /**
     *
     * 新增留言回复
     *
     * @author zhuou
     * @param replyBean
     *            回复bean
     * @return 回复ID
     * @throws PlatException
     *             公共异常
     */
    public long addReply(GuestBookReplyBean replyBean) throws PlatException
    {
        try
        {
            int rows = sqlSession.insert(SqlMapper.GuestBook.ADD_REPLY,
                    replyBean);
            return rows;
        }
        catch (Exception e)
        {
            log.error("Fail to addReply", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 修改回复内容
     *
     * @author zhuou
     * @param replyBean
     *            回复bean
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updateReply(GuestBookReplyBean replyBean) throws PlatException
    {
        try
        {
            return sqlSession.update(SqlMapper.GuestBook.UPDATE_REPLY,
                    replyBean);
        }
        catch (Exception e)
        {
            log.error("Fail to updateReply", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 删除一条回复内容
     *
     * @author zhuou
     * @param replyId
     *            回复Id
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int delReply(long replyId) throws PlatException
    {
        try
        {
            return sqlSession.update(SqlMapper.GuestBook.DEL_REPLY, replyId);
        }
        catch (Exception e)
        {
            log.error("Fail to replyId", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据留言Id删除留言下全部回复信息
     *
     * @author zhuou
     * @param guestBookId
     *            留言Id
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int delReplyByGBId(long guestBookId) throws PlatException
    {
        try
        {
            return sqlSession.update(SqlMapper.GuestBook.DEL_REPLYBYGBID,
                    guestBookId);
        }
        catch (Exception e)
        {
            log.error("Fail to delReplyByGBId", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据留言Id获取回复
     *
     * @author zhuou
     * @param guestBookId
     *            留言Id
     * @return list
     */
    public List<GuestBookReplyBean> getReplyList(long guestBookId)
        throws PlatException
    {
        try
        {
            return sqlSession.selectList(SqlMapper.GuestBook.GET_REPLYLIST,
                    guestBookId);
        }
        catch (Exception e)
        {
            log.error("Fail to getReplyList", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     *
     * 根据回复Id回去回复详情
     *
     * @author zhuou
     * @param replyId
     *            回复Id
     * @return GuestBookReplyBean
     * @throws PlatException
     *             公共异常
     */
    public GuestBookReplyBean getReplyByRId(long replyId) throws PlatException
    {
        try
        {
            return sqlSession.selectOne(SqlMapper.GuestBook.GET_REPLYBYRID,
                    replyId);
        }
        catch (Exception e)
        {
            log.error("Fail to getReplyByRId", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 获取留言回复，有分页
     *
     * @author zhuou
     * @param searchField
     *            搜索字段
     * @param keyword
     *            搜索关键字
     * @param rowNum
     *            第几页
     * @param pageNum
     *            没有条数
     * @param companyId
     *            公司ID
     * @return PageBean
     * @throws PlatException
     *             公共异常
     */
    public PageBean<GuestBookReplyBean> getReplysPageCurrent(
            String searchField, String keyword, int rowNum, int pageNum,
            int companyId) throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("searchField", searchField);
            param.put("keyword", keyword);
            param.put("rowNum", rowNum);
            param.put("pageNum", pageNum);
            param.put("companyId", companyId);

            List<GuestBookReplyBean> dataList = sqlSession.selectList(
                    SqlMapper.GuestBook.GET_REPLYSPAGECURRENT, param);

            int totalRecordNum = (Integer) sqlSession.selectOne(
                    SqlMapper.GuestBook.GET_REPLYSPAGECOUNT, param);

            PageBean<GuestBookReplyBean> pageBean = new PageBean<GuestBookReplyBean>();
            pageBean.setTotalRecordNum(totalRecordNum);
            pageBean.setDataList(dataList);

            return pageBean;
        }
        catch (Exception e)
        {
            log.error("Fail to getReplysPageCurrent info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据留言Id获取留言详细信息
     *
     * @author zhuou
     * @param guestBookId
     *            留言Id
     * @return GuestBookBean
     * @throws PlatException
     *             公共异常
     */
    public GuestBookBean getGuestBookByGId(long guestBookId)
        throws PlatException
    {
        try
        {
            return sqlSession.selectOne(SqlMapper.GuestBook.GET_GUESTBOOKBYGID,
                    guestBookId);
        }
        catch (Exception e)
        {
            log.error("Fail to getGuestBookByGId", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 修改留言回复数
     *
     * @author zhuou
     * @param method
     *            add为ReplyCount+1 del为ReplyCount-1
     * @param guestBookId
     *            留言id
     * @return 影响行数
     * @throws PlatException
     *             公共异常
     */
    public int updateGuestBookReplyNum(String method, long guestBookId)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("method", method);
            param.put("guestBookId", guestBookId);
            return sqlSession.update(
                    SqlMapper.GuestBook.UPDATE_GUESTBOOKREPLYNUM, param);
        }
        catch (Exception e)
        {
            log.error("Fail to updateGuestBookReplyNum", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 获取公共内容信息，有分页
     *
     * @author zhuou
     * @param paramMap
     *            Map
     * @return int
     * @throws PlatException
     *             CMS公共异常
     */
    public PageBean<GuestBookBean> getGuestBooks(Map<String, Object> paramMap)
        throws PlatException
    {

        try
        {
            List<GuestBookBean> dataList = sqlSession.selectList(
                    SqlMapper.GuestBook.GETGUESTBOOKS, paramMap);

            int totalRecordNum = (Integer) sqlSession.selectOne(
                    SqlMapper.GuestBook.GETCOUNTGUESTBOOKS, paramMap);

            PageBean<GuestBookBean> pageBean = new PageBean<GuestBookBean>();
            pageBean.setTotalRecordNum(totalRecordNum);
            pageBean.setDataList(dataList);

            return pageBean;
        }
        catch (Exception e)
        {
            log.error("Fail to save getGuestBooks info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }
}
