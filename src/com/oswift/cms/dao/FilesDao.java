package com.oswift.cms.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.cms.dao.SqlMapper.Files;
import com.oswift.cms.entity.FilesBean;
import com.oswift.cms.entity.PageBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("filesDao")
public class FilesDao extends BaseDao<FilesBean>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(FilesDao.class);

    public FilesDao()
    {
        super(Files.NAMESPACE);
    }

    /**
     *
     * 组合查询，当fileType=null或者status<0就不按照其条件查询
     *
     * @author zhuou
     * @param company
     *            公司Id
     * @param fileType
     *            文件类型，如果fileType=null，条件不起作用
     * @param status
     *            文件标志，如果status<0，条件不起作用
     * @param pageRecordNum
     *            每页记录数
     * @param pageNum
     *            第几页
     * @return List
     */
    public PageBean<FilesBean> getFiles(int company, String fileType,
            int status, int pageRecordNum, int pageNum) throws PlatException
    {
        try
        {
            int rowNum = (pageNum - 1) * pageRecordNum;

            Map<String, Object> param = new HashMap<String, Object>();
            param.put("companyId", company);
            param.put("status", status);
            param.put("fileType", fileType);
            param.put("rowNum", rowNum);
            param.put("pageNum", pageRecordNum);

            List<FilesBean> list = this.selectList(param);
            int totalRecordNum = this.selectCount(param);

            PageBean<FilesBean> pageBean = new PageBean<FilesBean>();
            pageBean.setTotalRecordNum(totalRecordNum);
            pageBean.setPageRecordNum(pageRecordNum);
            // 为了防止前台空指针异常，在此处new一个
            if (null == list)
            {
                list = new ArrayList<FilesBean>();
            }
            pageBean.setDataList(list);

            return pageBean;
        }
        catch (Exception e)
        {
            log.error("Fail to getFiles info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 保存文件信息
     *
     * @author zhuou
     * @param bean
     *            FilesBean
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean save(FilesBean bean) throws PlatException
    {
        return this.add(bean) > 0 ? true : false;
    }
}
