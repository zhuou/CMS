package com.oswift.cms.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.cms.entity.DictionaryBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("dictionaryDao")
public class DictionaryDao extends BaseDao<DictionaryBean>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(DictionaryDao.class);

    public DictionaryDao()
    {
        super(SqlMapper.Dictionary.NAMESPACE);
    }

    /**
     *
     * 查询需要缓存的数据字典数据
     *
     * @author zhuou
     * @return List<DictionaryBean>
     * @throws PlatException
     *             CMS公共异常
     */
    public List<DictionaryBean> getCacheDictionarys() throws PlatException
    {
        try
        {
            List<DictionaryBean> list = sqlSession
                    .selectList(SqlMapper.Dictionary.GET_CACHEDICTIONARY);
            return list;
        }
        catch (Exception e)
        {
            log.error("Fail to getCacheDictionarys.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据FieldName查找字段数据信息
     *
     * @author zhuou
     * @param fieldName
     *            字段名称
     * @return list
     * @throws PlatException
     *             公共异常
     */
    public List<DictionaryBean> getDictionaryByFieldName(String fieldName)
        throws PlatException
    {
        try
        {
            List<DictionaryBean> list = sqlSession.selectList(
                    SqlMapper.Dictionary.GET_DICTIONARYBYFIELDNAME, fieldName);
            return list;
        }
        catch (Exception e)
        {
            log.error("Fail to getDictionaryByFieldName.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据Title查找字段数据信息
     *
     * @author zhuou
     * @param title
     *            titile
     * @return List
     * @throws PlatException
     *             公共异常
     */
    public List<DictionaryBean> getDictionaryByTitle(String title)
        throws PlatException
    {
        try
        {
            List<DictionaryBean> list = sqlSession.selectList(
                    SqlMapper.Dictionary.GET_DICTIONARYBYTITLE, title);
            return list;
        }
        catch (Exception e)
        {
            log.error("Fail to getDictionaryByTitle.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

}
