package com.oswift.cms.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.cms.entity.PicBean;
import com.oswift.cms.entity.PicUrlBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("picDao")
public class PicDao extends BaseDao<PicBean>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(PicDao.class);

    public PicDao()
    {
        super(SqlMapper.Pic.NAMESPACE);
    }

    /**
     *
     * 添加图片地址信息
     *
     * @author zhuou
     * @param picUrlBeans
     *            图片地址信息
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int addPicUrl(List<PicUrlBean> picUrlBeans) throws PlatException
    {
        try
        {
            return sqlSession.insert(SqlMapper.Pic.ADD_PICURL, picUrlBeans);
        }
        catch (Exception e)
        {
            log.error("Fail to addPicUrl.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 删除图片地址信息
     *
     * @author zhuou
     * @param picGalleryId
     *            图片id
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int delPicUrl(long picGalleryId) throws PlatException
    {
        try
        {
            return sqlSession.insert(SqlMapper.Pic.DEL_PICURL, picGalleryId);
        }
        catch (Exception e)
        {
            log.error("Fail to delPicUrl.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据PicGalleryId获取图片地址
     *
     * @author zhuou
     * @param picGalleryId
     * @return list
     * @throws PlatException
     *             公共异常
     */
    public List<PicUrlBean> getPicUrlList(long picGalleryId)
        throws PlatException
    {
        try
        {
            return sqlSession.selectList(SqlMapper.Pic.GET_PICURLLIST,
                    picGalleryId);
        }
        catch (Exception e)
        {
            log.error("Fail to getPicUrlList.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }
}
