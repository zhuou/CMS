package com.oswift.cms.dao;

import org.springframework.stereotype.Repository;

import com.oswift.cms.entity.FriendSiteBean;

@Repository("friendSiteDao")
public class FriendSiteDao extends BaseDao<FriendSiteBean>
{
    public FriendSiteDao()
    {
        super(SqlMapper.FriendSite.NAMESPACE);
    }
}
