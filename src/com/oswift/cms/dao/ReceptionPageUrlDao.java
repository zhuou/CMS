package com.oswift.cms.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.cms.dao.SqlMapper.ReceptionPageUrl;
import com.oswift.cms.entity.PageBean;
import com.oswift.cms.entity.ReceptionPageUrlBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

/**
 *
 * 前台页面url数据库操作类
 *
 * @author zhuou
 * @version C03 2014-6-13
 * @since OSwift GPM V2.0
 */
@Repository("receptionPageUrlDao")
public class ReceptionPageUrlDao extends BaseDao<ReceptionPageUrlBean>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory
            .getLogger(ReceptionPageUrlDao.class);

    public ReceptionPageUrlDao()
    {
        super(ReceptionPageUrl.NAMESPACE);
    }

    /**
     *
     * 根据站点id查询出前台url所有配置
     *
     * @author zhuou
     * @param siteId
     *            站点Id
     * @return List
     * @throws PlatException
     *             公共异常
     */
    public List<ReceptionPageUrlBean> getReceptionPageUrlListBySiteId(int siteId)
        throws PlatException
    {
        try
        {
            return sqlSession.selectList(
                    ReceptionPageUrl.GET_RECEPTIONPAGEURLLISTBYSITEID, siteId);
        }
        catch (Exception e)
        {
            log.error("Fail to getReceptionPageUrlListBySiteId.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据站点id删除前台url所有配置
     *
     * @author zhuou
     * @param siteId
     *            站点Id
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int delAllBySiteId(int siteId) throws PlatException
    {
        try
        {
            return sqlSession.delete(
                    ReceptionPageUrl.GET_RECEPTIONPAGEURLLISTBYSITEID, siteId);
        }
        catch (Exception e)
        {
            log.error("Fail to delAllBySiteId.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 获取所有前台url配置数据
     *
     * @author zhuou
     * @return List
     * @throws PlatException
     *             公共异常
     */
    public List<ReceptionPageUrlBean> getAllReceptionPageUrl()
        throws PlatException
    {
        try
        {
            return sqlSession.selectList(
                    ReceptionPageUrl.GET_ALLRECEPTIONPAGEURL, null);
        }
        catch (Exception e)
        {
            log.error("Fail to getAllReceptionPageUrl.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 获取网站前台URL配置信息列表
     *
     * @author zhuou
     * @param keyword
     *            关键字
     * @param rowNum
     *            行数
     * @param pageNum
     *            每页显示记录数
     * @return PageBean
     * @throws PlatException
     *             CMS公共异常
     */
    public PageBean<ReceptionPageUrlBean> getReceptionPageUrlList(
            String keyword, int rowNum, int pageNum, int companyId)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("rowNum", rowNum);
            param.put("pageNum", pageNum);
            param.put("companyId", companyId);
            param.put("keyword", keyword);

            List<ReceptionPageUrlBean> dataList = this.selectList(param);

            int totalRecordNum = this.selectCount(param);

            PageBean<ReceptionPageUrlBean> pageBean = new PageBean<ReceptionPageUrlBean>();
            pageBean.setTotalRecordNum(totalRecordNum);
            pageBean.setDataList(dataList);

            return pageBean;
        }
        catch (Exception e)
        {
            log.error("Fail to getReceptionPageUrlList info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据站点id、url类型、url别称查找数据是否存在
     *
     * @author zhuou
     * @param siteId
     *            站点id
     * @param urlType
     *            url类型
     * @param nickName
     *            url别称
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int dataIsExist(int siteId, int urlType, String nickName)
        throws PlatException
    {
        try
        {
            ReceptionPageUrlBean bean = new ReceptionPageUrlBean();
            bean.setSiteId(siteId);
            bean.setUrlType(urlType);
            bean.setNickName(nickName);
            return (Integer) sqlSession.selectOne(ReceptionPageUrl.DATAISEXIST,
                    bean);
        }
        catch (Exception e)
        {
            log.error("Fail to dataIsExist.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }
}
