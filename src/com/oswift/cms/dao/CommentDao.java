package com.oswift.cms.dao;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.cms.entity.CommentBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("commentDao")
public class CommentDao extends BaseDao<CommentBean>
{

    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(CommentDao.class);

    public CommentDao()
    {
        super(SqlMapper.Comment.NAMESPACE);
    }

    /**
     *
     * 修改是否精华状态
     *
     * @author zhuou
     * @param isElite
     *            是否精华
     * @param commentId
     *            评论Id
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updateEliteStatus(boolean isElite, long commentId)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("isElite", isElite);
            param.put("commentId", commentId);
            return sqlSession.update(SqlMapper.Comment.UPDATE_ELITESTATUS,
                    param);
        }
        catch (Exception e)
        {
            log.error("Fail to updateEliteStatus info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 修改是否公开状态
     *
     * @author zhuou
     * @param isPrivate
     *            是否公开
     * @param commentId
     *            评论Id
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updatePrivateStatus(boolean isPrivate, long commentId)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("isPrivate", isPrivate);
            param.put("commentId", commentId);
            return sqlSession.update(SqlMapper.Comment.UPDATE_PRIVATESTATUS,
                    param);
        }
        catch (Exception e)
        {
            log.error("Fail to updatePrivateStatus info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据评论Id删除评论
     *
     * @author zhuou
     * @param commentIds
     *            评论Id
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int delCommentById(String commentIds) throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("commentIds", commentIds);
            return this.delete(param);
        }
        catch (Exception e)
        {
            log.error("Fail to DelComment info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }
}
