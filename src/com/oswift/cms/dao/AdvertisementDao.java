package com.oswift.cms.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.cms.entity.AdvertisementBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("advertisementDao")
public class AdvertisementDao extends BaseDao<AdvertisementBean>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(AdvertisementDao.class);

    public AdvertisementDao()
    {
        super(SqlMapper.Advertisement.NAMESPACE);
    }

    /**
     *
     * 根据广告位id删除广告信息
     *
     * @author zhuou
     * @param zoneId
     *            广告位id
     * @return int
     */
    public int delAdvertisementByZoneId(int zoneId)
    {
        return this.sqlSession.delete(
                SqlMapper.Advertisement.DEL_ADVERTISEMENTBYZONEID, zoneId);
    }

    /**
     *
     * 修改广告状态
     *
     * @author zhuou
     * @param bean
     *            广告bean
     * @return int
     */
    public int updateAdStatus(AdvertisementBean bean) throws PlatException
    {
        try
        {
            return this.sqlSession.update(
                    SqlMapper.Advertisement.UPDATEADSTATUS, bean);
        }
        catch (Exception e)
        {
            log.error("Fail to updateAdStatus info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }
}
