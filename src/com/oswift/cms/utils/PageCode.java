package com.oswift.cms.utils;

public interface PageCode
{
    /**
     * 成功
     */
    public static final String COMMON_SUCCESS = "200";

    /**
     * 错误
     */
    public static final String COMMON_ERROR = "500";

    /**
     * 系统内部错误，请和管理员联系(13276661981)
     */
    public static final String SYSTEM_ERROR = "600";

    /** **********6000-6020定义页面公共错误提示信息************* */
    /**
     * 获取web页面参数失败
     */
    public static final String GET_WEBPARAM_FAIL = "6000";

    /**
     * 必填项不能为空，请填写
     */
    public static final String TEXTFIELD_ISNOT_EMPTY = "6001";

    /**
     * 参数不能为空
     */
    public static final String PARAMETER_ISNOT_EMPTY = "6002";

    /**
     * 参数类型错误
     */
    public static final String PARAMETER_TYPE_ERROR = "6003";

    /** **********6021-6100 朱欧页面公共错误提示信息************* */
    /**
     * 获取地区信息失败
     */
    public static final String GET_AREA_FAIL = "6021";

    /**
     * 公司信息保存成功
     */
    public static final String COMPANY_SAVE_SUCCESS = "6022";

    /**
     * 公司信息保存失败
     */
    public static final String COMPANY_SAVE_FAIL = "6023";

    /**
     * 公司信息修改成功
     */
    public static final String COMPANY_UPDATE_SUCCESS = "6024";

    /**
     * 公司信息修改失败
     */
    public static final String COMPANY_UPDATE_FAIL = "6025";

    /**
     * 友情链接添加成功
     */
    public static final String FRIENDSITE_ADD_SUCCESS = "6026";

    /**
     * 友情链接添加失败
     */
    public static final String FRIENDSITE_ADD_FAIL = "6027";

    /**
     * 操作成功
     */
    public static final String UPDATE_STATUS_SUCCESS = "6028";

    /**
     * 操作失败
     */
    public static final String UPDATE_STATUS_FAIL = "6029";

    /**
     * 容器栏目信息添加成功
     */
    public static final String CONTAINCHANNEL_SAVE_SUCCESS = "6030";

    /**
     * 容器栏目信息添加失败
     */
    public static final String CONTAINCHANNEL_SAVE_FAIL = "6031";

    /**
     * 单页栏目信息添加成功
     */
    public static final String SINGLECHANNEL_SAVE_SUCCESS = "6032";

    /**
     * 单页栏目信息添加失败
     */
    public static final String SINGLECHANNEL_SAVE_FAIL = "6033";

    /**
     * 外部链接栏目信息添加成功
     */
    public static final String EXTERNALCHANNEL_SAVE_SUCCESS = "6034";

    /**
     * 外部链接栏目信息添加失败
     */
    public static final String EXTERNALCHANNEL_SAVE_FAIL = "6035";

    /**
     * 内容栏目修改成功
     */
    public static final String CONTAINCHANNEL_UPDATE_SUCCESS = "6036";

    /**
     * 内容栏目修改失败
     */
    public static final String CONTAINCHANNEL_UPDATE_FAIL = "6037";

    /**
     * 单页栏目修改成功
     */
    public static final String SINGLECHANNEL_UPDATE_SUCCESS = "6038";

    /**
     * 单页栏目修改失败
     */
    public static final String SINGLECHANNEL_UPDATE_FAIL = "6039";

    /**
     * 外部链接栏目修改成功
     */
    public static final String EXTERNALCHANNEL_UPDATE_SUCCESS = "6040";

    /**
     * 外部链接栏目修改失败
     */
    public static final String EXTERNALCHANNEL_UPDATE_FAIL = "6041";

    /**
     * 数据已经被删除，请刷新页面
     */
    public static final String DATA_ALREADY_DEL = "6042";

    /**
     * 文章新增成功
     */
    public static final String ARTICLE_ADD_SUCCESS = "6043";

    /**
     * 文章新增失败
     */
    public static final String ARTICLE_ADD_FAIL = "6044";

    /**
     * 文章修改成功
     */
    public static final String ARTICLE_UPDATE_SUCCESS = "6045";

    /**
     * 文章修改失败
     */
    public static final String ARTICLE_UPDATE_FAIL = "6046";

    /**
     * 文章删除成功
     */
    public static final String ARTICLE_DEL_SUCCESS = "6047";

    /**
     * 文章删除失败
     */
    public static final String ARTICLE_DEL_FAIL = "6048";

    /**
     * 公告新增成功
     */
    public static final String ANNOUNCE_ADD_SUCCESS = "6049";

    /**
     * 公告新增失败
     */
    public static final String ANNOUNCE_ADD_FAIL = "6050";

    /**
     * 公告修改成功
     */
    public static final String ANNOUNCE_UPDATE_SUCCESS = "6051";

    /**
     * 公告修改失败
     */
    public static final String ANNOUNCE_UPDATE_FAIL = "6052";

    /**
     * 公告删除成功
     */
    public static final String ANNOUNCE_DEL_SUCCESS = "6053";

    /**
     * 公告删除失败
     */
    public static final String ANNOUNCE_DEL_FAIL = "6054";

    /**
     * 下载新增成功
     */
    public static final String DOWNLOAD_ADD_SUCCESS = "6055";

    /**
     * 下载新增失败
     */
    public static final String DOWNLOAD_ADD_FAIL = "6056";

    /**
     * 下载修改成功
     */
    public static final String DOWNLOAD_UPDATE_SUCCESS = "6057";

    /**
     * 下载修改失败
     */
    public static final String DOWNLOAD_UPDATE_FAIL = "6058";

    /**
     * 下载删除成功
     */
    public static final String DOWNLOAD_DEL_SUCCESS = "6059";

    /**
     * 下载删除失败
     */
    public static final String DOWNLOAD_DEL_FAIL = "6060";

    /**
     * 留言新增成功
     */
    public static final String GUESTBOOK_ADD_SUCCESS = "6061";

    /**
     * 留言新增失败
     */
    public static final String GUESTBOOK_ADD_FAIL = "6062";

    /**
     * 留言修改成功
     */
    public static final String GUESTBOOK_UPDATE_SUCCESS = "6063";

    /**
     * 留言修改失败
     */
    public static final String GUESTBOOK_UPDATE_FAIL = "6064";

    /**
     * 留言删除成功
     */
    public static final String GUESTBOOK_DEL_SUCCESS = "6065";

    /**
     * 留言删除失败
     */
    public static final String GUESTBOOK_DEL_FAIL = "6066";

    /**
     * 图片新增成功
     */
    public static final String PIC_ADD_SUCCESS = "6067";

    /**
     * 图片新增失败
     */
    public static final String PIC_ADD_FAIL = "6068";

    /**
     * 图片修改成功
     */
    public static final String PIC_UPDATE_SUCCESS = "6069";

    /**
     * 图片修改失败
     */
    public static final String PIC_UPDATE_FAIL = "6070";

    /**
     * 图片删除成功
     */
    public static final String PIC_DEL_SUCCESS = "6071";

    /**
     * 图片信息成功删除{0}条，删除失败{1}条
     */
    public static final String PIC_DEL_FAIL = "6072";

    /**
     * 用户组新增成功
     */
    public static final String USERGROUP_ADD_SUCCESS = "6073";

    /**
     * 用户组新增失败
     */
    public static final String USERGROUP_ADD_FAIL = "6074";

    /**
     * 用户组修改成功
     */
    public static final String USERGROUP_UPDATE_SUCCESS = "6075";

    /**
     * 用户组修改失败
     */
    public static final String USERGROUP_UPDATE_FAIL = "6076";

    /**
     * 用户组删除成功
     */
    public static final String USERGROUP_DEL_SUCCESS = "6077";

    /**
     * 用户组删除失败
     */
    public static final String USERGROUP_DEL_FAIL = "6078";

    /**
     * 系统错误（获取栏目ID参数失败，请联系管理员）
     */
    public static final String GET_CHANNELID_FAIL = "6079";

    /**
     * 页面功能不能为空，请填写页面功能
     */
    public static final String PAGEFUN_ISEMPTY = "6080";

    /**
     * 站点前台页面功能设置成功
     */
    public static final String PAGEFUN_ADD_SUCCESS = "6081";

    /**
     * 站点前台页面功能设置失败
     */
    public static final String PAGEFUN_ADD_FAIL = "6082";

    /**
     * 图片上传成功
     */
    public static final String UPLOAD_IMG_SUCCESS = "6083";

    /**
     * 图片上传失败
     */
    public static final String UPLOAD_IMG_FAIL = "6084";

    /**
     * 图片类型错误，请上传({0})类型图片
     */
    public static final String IMG_TYPE_ERROR = "6085";

    /**
     * 请选择上传图片
     */
    public static final String UPLOAD_IMG_ISEMPTY = "6086";

    /**
     * 创建文件夹失败
     */
    public static final String CREATE_FOLDER_FAIL = "6087";

    /**
     * 文件下载失败
     */
    public static final String DOWNLOAD_FAIL = "6088";

    /**
     * 图片地址不能为空，请填写图片地址
     */
    public static final String PICURL_ISEMPTY = "6089";

    /**
     * 此条信息已经被删除
     */
    public static final String PAGE_ALREADYDEL = "6090";

    /**
     * 功能页面信息删除成功
     */
    public static final String PAGE_DEL_SUCCESS = "6091";

    /**
     * 功能页面信息删除失败
     */
    public static final String PAGE_DEL_FAIL = "6092";

    /**
     * 页面功能已经存在，如果要新增，请先删除
     */
    public static final String PAGE_ISEXIST = "6093";

    /**
     * 此条功能删除成功
     */
    public static final String FUN_DEL_SUCCESS = "6094";

    /**
     * 此条功能删除失败
     */
    public static final String FUN_DEL_FAIL = "6095";

    /**
     * 此栏目不存在，可能已经被删除
     */
    public static final String CHANNEL_ISONT_EXIST = "6096";

    /**
     * 栏目删除成功
     */
    public static final String CHANNEL_DEL_SUCCESS = "6097";

    /**
     * 栏目删除失败
     */
    public static final String CHANNEL_DEL_FAIL = "6098";

    /**
     * 栏目信息部分删除成功
     */
    public static final String CHANNEL_DEL_PART_SUCCESS = "6099";

    /**
     * 精华状态设置成功
     */
    public static final String UPDATE_ELITESTATUS_SUCCESS = "6100";

    /**
     * 精华状态设置失败
     */
    public static final String UPDATE_ELITESTATUS_FAIL = "6101";

    /**
     * 是否公开状态设置成功
     */
    public static final String UPDATE_PRIVATESTATUS_SUCCESS = "6102";

    /**
     * 是否公开状态设置失败
     */
    public static final String UPDATE_PRIVATESTATUS_FAIL = "6103";

    /**
     * 删除评论成功
     */
    public static final String DEL_COMMENTBYID_SUCCESS = "6104";

    /**
     * 删除评论失败
     */
    public static final String DEL_COMMENTBYID_FAIL = "6105";

    /**
     * 友情链接修改成功
     */
    public static final String FRIENDSITE_UPDATE_SUCCESS = "6106";

    /**
     * 友情链接修改失败
     */
    public static final String FRIENDSITE_UPDATE_FAIL = "6107";

    /**
     * 友情链接删除成功
     */
    public static final String FRIENDSITE_DEL_SUCCESS = "6108";

    /**
     * 友情链接成功删除{0}条，删除失败{1}条
     */
    public static final String FRIENDSITE_DEL_FAIL = "6109";

    /**
     * 留言回复新增成功
     */
    public static final String GUESTBOOKREPLY_ADD_SUCCESS = "6110";

    /**
     * 留言回复新增失败
     */
    public static final String GUESTBOOKREPLY_ADD_FAIL = "6111";

    /**
     * 留言回复删除成功
     */
    public static final String GUESTBOOKREPLY_DEL_SUCCESS = "6112";

    /**
     * 留言回复删除失败
     */
    public static final String GUESTBOOKREPLY_DEL_FAIL = "6113";

    /**
     * 留言回复修改成功
     */
    public static final String GUESTBOOKREPLY_UPDATE_SUCCESS = "6114";

    /**
     * 留言回复修改失败
     */
    public static final String GUESTBOOKREPLY_UPDATE_FAIL = "6115";

    /**
     * 广告位新增成功
     */
    public static final String ADZONE_ADD_SUCCESS = "6116";

    /**
     * 广告位新增失败
     */
    public static final String ADZONE_ADD_FAIL = "6117";

    /**
     * 广告位修改成功
     */
    public static final String ADZONE_UPDATE_SUCCESS = "6118";

    /**
     * 广告位修改失败
     */
    public static final String ADZONE_UPDATE_FAIL = "6119";

    /**
     * 广告位状态修改成功
     */
    public static final String ADZONE_UPDATESTATUS_SUCCESS = "6120";

    /**
     * 广告位状态修改失败
     */
    public static final String ADZONE_UPDATESTATUS_FAIL = "6121";

    /**
     * 容许上传的文件类型错误，请上传({0})类型文件
     */
    public static final String FILE_TYPE_ERROR = "6122";

    /**
     * 广告新增成功
     */
    public static final String AD_ADD_SUCCESS = "6123";

    /**
     * 广告新增失败
     */
    public static final String AD_ADD_FAIL = "6124";

    /**
     * 广告修改成功
     */
    public static final String AD_UPDATE_SUCCESS = "6125";

    /**
     * 广告修改失败
     */
    public static final String AD_UPDATE_FAIL = "6126";

    /**
     * 广告删除成功
     */
    public static final String AD_DEL_SUCCESS = "6127";

    /**
     * 广告删除失败
     */
    public static final String AD_DEL_FAIL = "6128";

    /**
     * 广告审核状态设置成功
     */
    public static final String AD_UPDATE_STATUS_SUCCESS = "6129";

    /**
     * 广告审核状态设置失败
     */
    public static final String AD_UPDATE_STATUS_FAIL = "6130";

    /**
     * 广告位删除成功
     */
    public static final String ADZONE_DEL_SUCCESS = "6131";

    /**
     * 广告位删除失败
     */
    public static final String ADZONE_DEL_FAIL = "6132";

    /**
     * 会员新增成功
     */
    public static final String MEMBER_ADD_SUCCESS = "6133";

    /**
     * 会员新增失败
     */
    public static final String MEMBER_ADD_FAIL = "6134";

    /**
     * 会员修改成功
     */
    public static final String MEMBER_UPDATE_SUCCESS = "6135";

    /**
     * 会员修改失败
     */
    public static final String MEMBER_UPDATE_FAIL = "6136";

    /**
     * 会员删除成功
     */
    public static final String MEMBER_DEL_SUCCESS = "6137";

    /**
     * 会员删除失败
     */
    public static final String MEMBER_DEL_FAIL = "6138";

    /**
     * 此用户名已经被使用，请重新输入
     */
    public static final String EXISTUSERNAME = "6139";

    /**
     * 此会员信息不存在或者已经被删除，请联系管理员
     */
    public static final String EXISTNOMEMBER = "6140";

    /**
     * 广告点击数增加失败
     */
    public static final String ADD_ADCLICK_FAIL = "6141";

    /**
     * 评论态度新增成功
     */
    public static final String COMMENTATTITUDE_SUCCESS = "6142";

    /**
     * 评论态度新增失败
     */
    public static final String COMMENTATTITUDE_FAIL = "6143";

    /**
     * 站点信息保存成功
     */
    public static final String WEBCONFIG_SAVE_SUCCESS = "6144";

    /**
     * 站点信息保存失败
     */
    public static final String WEBCONFIG_SAVE_FAIL = "6145";

    /**
     * 站点信息修改成功
     */
    public static final String WEBCONFIG_UPDATE_SUCCESS = "6146";

    /**
     * 站点信息修改失败
     */
    public static final String WEBCONFIG_UPDATE_FAIL = "6147";

    /**
     * 站点信息删除成功
     */
    public static final String WEBCONFIG_DEL_SUCCESS = "6148";

    /**
     * 站点信息删除失败
     */
    public static final String WEBCONFIG_DEL_FAIL = "6149";

    /**
     * URL数据已经存在
     */
    public static final String RECEPTIONPAGEURL_ISEXIST = "6150";

    /**
     * URL数据不存在
     */
    public static final String RECEPTIONPAGEURL_ISNOTEXIST = "6151";

    /**
     * 前台页面Url新增成功
     */
    public static final String ADD_RECEPTIONPAGEURL_SUCCESS = "6152";

    /**
     * 前台页面Url新增失败
     */
    public static final String ADD_RECEPTIONPAGEURL_FAIL = "6153";

    /**
     * 前台页面Url删除成功
     */
    public static final String DEL_RECEPTIONPAGEURL_SUCCESS = "6154";

    /**
     * 前台页面Url删除失败
     */
    public static final String DEL_RECEPTIONPAGEURL_FAIL = "6155";

    /**
     * 前台页面Url修改成功
     */
    public static final String UPDATE_RECEPTIONPAGEURL_SUCCESS = "6156";

    /**
     * 前台页面Url修改失败
     */
    public static final String UPDATE_RECEPTIONPAGEURL_FAIL = "6157";

    /**
     * 请输入验证码
     */
    public static final String VALIDATECODE_EMPTY = "7506";

    /**
     * 验证码获错误，请重新输入
     */
    public static final String VALIDATECODE_ERROR = "7508";

    /**
     * 验证码获取超时，请刷新验证码
     */
    public static final String VALIDATECODE_OVERTIME = "7507";

    /**
     * 预约删除成功
     */
    public static final String RESERVATION_DEL_SUCCESS = "7515";

    /**
     * 预约删除失败
     */
    public static final String RESERVATION_DEL_FAIL = "7516";

    /**
     * 修改预约状态成功
     */
    public static final String RESERVATION_UPDATESTATUS_SUCCESS = "7517";

    /**
     * 修改预约状态失败
     */
    public static final String RESERVATION_UPDATESTATUS_FAIL = "7518";

    /**
     * 评价成功
     */
    public static final String CLICK_APPRAISE_SUCCESS = "7519";

    /**
     * 评价失败
     */
    public static final String CLICK_APPRAISE_FAIL = "7520";

    /**
     * 用户名不能为空，请输入正确的用户名
     */
    public static final String MEMBER_USERISEMPTY = "7521";

    /**
     * 密码不能为空，请输入正确的密码
     */
    public static final String MEMBER_PASSWORDISEMPTY = "7522";

    /**
     * 用户注册成功
     */
    public static final String MEMBER_REGISTER_SUCCESS = "7523";

    /**
     * 用户注册失败
     */
    public static final String MEMBER_REGISTER_FAIL = "7524";

    /**
     * 您输入的{0}不存在，请重新输入
     */
    public static final String MEMBERLOGIN_USERNAMEERROR = "7525";

    /**
     * 您的账号已经被锁定，请联系网站管理人员
     */
    public static final String MEMBER_LOCKED = "7526";

    /**
     * 登录成功
     */
    public static final String MEMBERLOGIN_SUCCESS = "7527";

    /**
     * 确认密码不能为空，请输入确认密码
     */
    public static final String MEMBER_REPASSWORDISEMPTY = "7528";

    /**
     * 两次输入的密码不一致，请重新输入
     */
    public static final String MEMBER_PASSWORDTWOERROR = "7529";

    /**
     * 此邮箱地址已经被使用，请使用其他Email注册
     */
    public static final String MEMBER_EMAIL_EXIST = "7530";

    /**
     * 您输入的密码错误，请重新输入
     */
    public static final String MEMBERLOGIN_PASSWORDERROR = "7531";

    /**
     * 评论内容不能为空，请填写评论
     */
    public static final String COMMENTCONTENT_ISEMPTY = "7532";

    /**
     * 评论添加成功
     */
    public static final String ADD_COMMENT_SUCCESS = "7533";

    /**
     * Sorry！评论添加失败，请重试
     */
    public static final String ADD_COMMENT_FAIL = "7534";

    /**
     * 会员组信息不存在，请联系管理员
     */
    public static final String MEMBERGROUP_ISNOT_EXIST = "7535";

    /**
     * 您上传的图片太大，请上传小于{0}M的图片
     */
    public static final String UPLOADIMG_MAXSIZE_TOOBIG = "7536";

    /**
     * 文件删除成功
     */
    public static final String DEL_FILE_SUCCESS = "7537";

    /**
     * 文件删除失败
     */
    public static final String DEL_FILE_FAIL = "7538";

    /**
     * 广告位编码已经存在，请修改编码
     */
    public static final String ZONECODE_IS_EXIST = "7539";
}
