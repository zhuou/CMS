package com.oswift.cms.utils;

import com.oswift.cms.system.cache.CMSCacheFileds;
import com.oswift.cms.system.cache.DictionaryManager;

public interface ReceptionConstant
{
    /**
     * sql连接查询AND
     */
    public static final String SQL_AND = "AND";

    /**
     * 默认分页页码
     */
    public static final int DEFAULT_PAGENUM = 1;

    /**
     * 默认分页页码
     */
    public static final String STR_DEFAULT_PAGENUM = "1";

    /**
     * 用户页面路径
     */
    public static final String USERFILE = "userfile";

    /**
     * 用户页面存放文件夹的前缀名称
     */
    public static final String USERFILENAME = "site";

    /**
     * 文件路径分隔符
     */
    public static final String FILE_SEPARATOR = System
            .getProperty("file.separator");

    /**
     * 站点前台统一错误页面路径
     */
    public static String errorPagePath = DictionaryManager.getValue(
            CMSCacheFileds.BusinessField.ERRORPAGEPATH).getFieldValue();

    /**
     * 页面默认功能
     */
    public static int DEFAULT_FUN = 1;

    /**
     * 首页默认名称
     */
    public static String INDEX_NAME = "index";

    /**
     * 当前栏目/内容代号
     */
    public static final int CURRENT_CODE = -2;

    public interface ContentType
    {
        /**
         * 最新
         */
        public static final String NEW = "new";

        /**
         * 最热
         */
        public static final String HOT = "hot";

        /**
         * 本月最热
         */
        public static final String MONTHHOT = "mhot";

        /**
         * 本周最热
         */
        public static final String WEEKHOT = "whot";

        /**
         * 天最热
         */
        public static final String DAYHOT = "dhot";

        /**
         * 图文
         */
        public static final String IMGTXT = "imgtxt";

        /**
         * 推荐
         */
        public static final String RECOMMEND = "recommend";

        /**
         * 最先优先级
         */
        public static final String PRIORITY = "priority";

        /**
         * 最多评论
         */
        public static final String MOSTCOMMENT = "mostcomment";

        /**
         * 焦点
         */
        public static final String FOCUS = "focus";

        /**
         * 头条
         */
        public static final String BIGNEWS = "bignews";

        /**
         * 滚动
         */
        public static final String MARQUEE = "marquee";

        /**
         * 封面
         */
        public static final String DEFAULTIMG = "defaultimg";

        /**
         * 查询公司下全部栏目
         */
        public static final String CHANNEL_ALL = "all";

        /**
         * 查询全部一级栏目
         */
        public static final String CHANNEL_ONE_LEVEL = "one-level";

        /**
         * 查询指定栏目及其所有父栏目和所有子栏目
         */
        public static final String CHANNEL_FIXED_ALL = "fixed-all";

        /**
         * 查询指定栏目
         */
        public static final String CHANNEL_FIXED_ONE = "fixed-one";

        /**
         * 用户自定义搜索条件，前缀关键字
         */
        public static final String USER_DEFINED = "defined:";

        /**
         * 用户自定义排序条件，前缀关键字
         */
        public static final String USER_DEFINED_ORDERBY = "orderby:";
    }

    /**
     *
     * 排序条件
     *
     * @author zhuou
     * @version C03 2014-9-14
     * @since OSwift GPM V1.0
     */
    public interface OrderBy
    {
        /**
         * 升序
         */
        public static final String ASC = "asc";

        /**
         * 降序
         */
        public static final String DESC = "desc";
    }

    /**
     *
     * 站点前台页面编码
     *
     * @author zhuou
     * @version C03 2013-9-5
     * @since OSwift GPM V1.0
     */
    public interface ReceptionPageUrLState
    {
        /**
         * 栏目前缀名称
         */
        public static final String CHANNEL_HEAHNAME = "channel_";

        /**
         * 首页(/index.html)
         */
        public static final int INDEX = 1;

        /**
         * 栏目页(/页面别名/栏目别名.html,例如：/web-base/about-us.html)
         */
        public static final int CHANNEL = 2;

        /**
         * 列表页(/页面别名/list_1.html,例如：/article/list_1.html)
         */
        public static final int LIST = 3;

        /**
         * 类型列表页(/页面别名/list_new_1.html,例如：/article/list_new_1.html)
         */
        public static final int LIST_TYPE = 4;

        /**
         * 栏目列表页(/页面别名/栏目别名/list_1.html,例如：/pic/girl/list_1.html)
         */
        public static final int CHANNEL_LIST = 5;

        /**
         * 栏目类型列表页(/页面别名/栏目别名/list_new_1.html,例如：/pic/girl/list_new_1.html)
         */
        public static final int CHANNEL_LIST_TYPE = 6;

        /**
         * 详情页(/页面别名/内容Id/detail.html,例如：/article/100/detail.html)
         */
        public static final int DETAIL = 7;

        /**
         * 用户自定义url
         */
        public static final int USER_DEFINED = 8;
    }

    /**
     *
     * 会员常量定义
     *
     * @author zhuou
     * @version C03 2014-3-31
     * @since OSwift GPM V1.0
     */
    public interface MemberConstant
    {
        /**
         *
         * 证件类型
         *
         * @author zhuou
         * @version C03 2014-3-31
         * @since OSwift GPM V1.0
         */
        public interface CardType
        {
            /**
             * 身份证
             */
            public static final int IDCARD = 1;

            /**
             * 护照
             */
            public static final int PASSPORT = 2;

            /**
             * 驾驶证
             */
            public static final int DRIVINGLICENCE = 3;

            /**
             * 港澳通行证
             */
            public static final int LAISSEZPASSER = 4;

            /**
             * 其他
             */
            public static final int OTHER = 99;
        }

        /**
         *
         * 婚姻状况
         *
         * @author zhuou
         * @version C03 2014-3-31
         * @since OSwift GPM V1.0
         */
        public interface Marriage
        {
            /**
             * 保密
             */
            public static final int SECRET = 1;

            /**
             * 未婚
             */
            public static final int UNMARRIED = 2;

            /**
             * 已婚
             */
            public static final int MARRIED = 3;

            /**
             * 离异
             */
            public static final int DIVORCE = 4;
        }

        /**
         *
         * 性别
         *
         * @author zhuou
         * @version C03 2014-3-31
         * @since OSwift GPM V1.0
         */
        public interface Sex
        {
            /**
             * 男
             */
            public static final int MALE = 1;

            /**
             * 女
             */
            public static final int FEMALE = 2;
        }

        /**
         *
         * 会员账号状态
         *
         * @author zhuou
         * @version C03 2014-3-31
         * @since OSwift GPM V1.0
         */
        public interface Status
        {
            /**
             * 正常
             */
            public static final int NORMAL = 0;

            /**
             * 锁定
             */
            public static final int LOCK = 1;
        }
    }
}
