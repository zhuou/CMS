package com.oswift.cms.utils;

public interface Field
{
    /**
     * 默认父节点Id常量
     */
    public static final int PARENT_ID = -1;

    /**
     * 默认节点深度
     */
    public static final int DEFAULT_NOTELEVEL = 1;

    /**
     * 栏目默认ID
     */
    public static final int DEFAULT_CHANNELID = 1001;

    /**
     * true字符表达式
     */
    public static final String TRUE = "true";

    /**
     * false字符表达式
     */
    public static final String FALSE = "false";

    /**
     * 逗号
     */
    public static final String COMMA = ",";

    /**
     * http
     */
    public static final String HTTP = "http";

    /**
     * www
     */
    public static final String WWW = "www";

    /**
     * 会员登录信息保存session的key
     */
    public static final String MEMBER_SESSION = "member.login";

    /**
     *
     * 定义地区的状态字符串
     *
     * @author zhuou
     * @version C03 2012-12-22
     * @since OSwift GPM V1.0
     */
    public interface Area
    {
        /**
         * 省
         */
        static final String PROVINCE = "province";

        /**
         * 市
         */
        static final String CITY = "city";

        /**
         * 区/县
         */
        static final String DISTRICT = "district";
    }

    /**
     *
     * 内容表名称
     *
     * @author zhuou
     * @version C03 2013-1-17
     * @since OSwift GPM V1.0
     */
    public interface TableName
    {
        /**
         * 文章频道表
         */
        static final String ARTICLE = "T_CMS_Article";

        /**
         * 公告频道表
         */
        static final String ANNOUNCE = "T_CMS_Announce";

        /**
         * 下载频道表
         */
        static final String DOWNLOAD = "T_CMS_Download";

        /**
         * 留有频道表
         */
        static final String GUESTBOOK = "T_CMS_GuestBook";

        /**
         * 图片频道表
         */
        static final String PICURL = "T_CMS_PicGallery";

        /**
         * 友情链接频道表
         */
        static final String FRIENDSITE = "T_CMS_FriendSite";

        /**
         * 广告频道字段
         */
        static final String AD = "T_CMS_Ad";

        /**
         * 内容详情（文章、公告、留言、图片、下载、友情链接）
         */
        static final String DETAIL = "T_CMS_Detail";

        /**
         * 评论
         */
        static final String COMMENT = "T_CMS_Comment";

        /**
         * 栏目
         */
        static final String CHANNEL = "T_CMS_Channel";
    }

    /**
     *
     * 预置栏目名称
     *
     * @author zhuou
     * @version C03 2013-8-6
     * @since OSwift GPM V1.0
     */
    public interface InitName
    {
        /**
         * 文章中心
         */
        static final String ARTICLE = "文章中心";

        /**
         * 文章默认NickName
         */
        static final String ARTICLE_DEFAULT_NICKNAME = "default-articles";

        /**
         * 公告中心
         */
        static final String ANNOUNCE = "公告中心";

        /**
         * 公告默认NickName
         */
        static final String ANNOUNCE_DEFAULT_NICKNAME = "default-announces";

        /**
         * 下载中心
         */
        static final String DOWNLOAD = "下载中心";

        /**
         * 下载默认NickName
         */
        static final String DOWNLOAD_DEFAULT_NICKNAME = "default-downloads";

        /**
         * 留言中心
         */
        static final String GUESTBOOK = "留言中心";

        /**
         * 留言默认NickName
         */
        static final String GUESTBOOK_DEFAULT_NICKNAME = "default-guestBooks";

        /**
         * 图片中心
         */
        static final String PIC = "图片中心";

        /**
         * 图片默认NickName
         */
        static final String PIC_DEFAULT_NICKNAME = "default-pics";

        /**
         * 友情链接中心
         */
        static final String FRIENDSITE = "友情链接中心";

        /**
         * 友情链接默认NickName
         */
        static final String FRIENDSITE_DEFAULT_NICKNAME = "default-friendSites";

        /**
         * 描述
         */
        static final String DESCRIPTION = "预置栏目";

        /**
         * 内置会员组
         */
        static final String GROUPNAME = "注册会员";
    }

    public interface Status
    {
        /**
         * 所有内容
         */
        static final int ALL = 999;

        /**
         * -3为删除
         */
        static final int DEL = -3;

        /**
         * -2为退稿
         */
        static final int BACK = -2;

        /**
         * -1为草稿
         */
        static final int DRAFT = -1;

        /**
         * 0为待审核
         */
        static final int PENDING_AUDIT = 0;

        /**
         * 99为终审通过
         */
        static final int PASS = 99;
    }

    /**
     *
     * 栏目类型 0为容器栏目，1为专题栏目，2为单个页面，3为外部链接
     *
     * @author zhuou
     * @version C03 2013-2-20
     * @since OSwift GPM V1.0
     */
    public interface ChannelType
    {
        /**
         * 0为容器栏目
         */
        static final int CONTAIN = 0;

        /**
         * 2为单个页面
         */
        static final int SINGLE = 2;

        /**
         * 3为外部链接
         */
        static final int EXTERNAL = 3;
    }

    /**
     *
     * 栏目类别 0为预置栏目 1为用户创建栏目
     *
     * @author zhuou
     * @version C03 Apr 8, 2013
     * @since OSwift GPM V1.0
     */
    public interface ChannelCategory
    {
        static final int DEFAULT = 0;

        static final int CREATE = 1;
    }

    /**
     *
     * 下载功能的常量字段
     *
     * @author zhuou
     * @version C03 2013-8-14
     * @since OSwift GPM V1.0
     */
    public interface Download
    {
        /**
         * 软件类别
         */
        static final String FILETYPE = "FileType";

        /**
         * 软件语言
         */
        static final String FILELANGUAGE = "FileLanguage";

        /**
         * 授权方式
         */
        static final String COPYRIGHTTYPE = "CopyrightType";

        /**
         * 软件平台
         */
        static final String OPERATINGSYSTEM = "OperatingSystem";
    }

    /**
     *
     * 页面名称
     *
     * @author zhuou
     * @version C03 2013-8-5
     * @since OSwift GPM V1.0
     */
    public interface PageName
    {
        /**
         * 公共路径
         */
        public static final String COMMON_PATH = "admin/cms/jsp/";

        /**
         * 公共错误页面
         */
        public static String ERROR_PAGE = COMMON_PATH + "errorInfo";

        /**
         * 文章列表
         */
        public static final String ARTICLELIST = COMMON_PATH + "articleList";

        /**
         * 文章页面
         */
        public static final String ARTICLE = COMMON_PATH + "article";

        /**
         * 文章详情
         */
        public static final String ARTICLE_DETAIL = COMMON_PATH
                + "articleDetail";

        /**
         * 公告列表
         */
        public static final String ANNOUNCELIST = COMMON_PATH + "announceList";

        /**
         * 公告页面
         */
        public static final String ANNOUNCE = COMMON_PATH + "announce";

        /**
         * 公告详情
         */
        public static final String ANNOUNCE_DETAIL = COMMON_PATH
                + "announceDetail";

        /**
         * 下载列表
         */
        public static final String DOWNLOADLIST = COMMON_PATH + "downloadList";

        /**
         * 下载页面
         */
        public static final String DOWNLOAD = COMMON_PATH + "download";

        /**
         * 下载详情
         */
        public static final String DOWNLOAD_DETAIL = COMMON_PATH
                + "downloadDetail";

        /**
         * 留言页面
         */
        public static final String GUESTBOOK = COMMON_PATH + "guestBook";

        /**
         * 留言列表
         */
        public static final String GUESTBOOKLIST = COMMON_PATH
                + "guestBookList";

        /**
         * 留言详情
         */
        public static final String GUESTBOOK_DETAIL = COMMON_PATH
                + "guestBookDetail";

        /**
         * 图片列表页面
         */
        public static final String PIC_LIST = COMMON_PATH + "picList";

        /**
         * 图片页面
         */
        public static final String PIC = COMMON_PATH + "pic";

        /**
         * 图片详情
         */
        public static final String PICDETAIL = COMMON_PATH + "picDetail";

        /**
         * 上传首页默认图片页面
         */
        public static final String UPLOADDEFAULTIMG = COMMON_PATH
                + "uploadDefaultImg";

        /**
         * 用户组列表页面
         */
        public static final String USERGROUP_LIST = COMMON_PATH
                + "userGroupList";

        /**
         * 用户组页面
         */
        public static final String USERGROUP = COMMON_PATH + "userGroup";

        /**
         * 用户组详情
         */
        public static final String USERGROUP_DETAIL = COMMON_PATH
                + "userGroupDetail";

        /**
         * 前台栏目页面功能管理页面
         */
        public static final String SET_RECEPTIONPAGE = COMMON_PATH
                + "setReceptionPage";

        /**
         * 新增页面功能
         */
        public static final String ADD_PAGEFUN = COMMON_PATH + "addPageFun";

        /**
         * 新增一条页面功能
         */
        public static final String ADD_ONEPAGEFUN = COMMON_PATH
                + "addOnePageFun";

        /**
         * 容器栏目
         */
        public static final String CONTAINCHANNEL = COMMON_PATH
                + "containChannel";

        /**
         * 容器栏目详情
         */
        public static final String CONTAINCHDETAIL = COMMON_PATH
                + "containChDetail";

        /**
         * 单页栏目详情
         */
        public static final String SINGLECHDETAIL = COMMON_PATH
                + "singleChDetail";

        /**
         * 外接栏目详情
         */
        public static final String EXTERNALCHDETAIL = COMMON_PATH
                + "externalChDetail";

        /**
         * 单页栏目
         */
        public static final String SINGLECHANNEL = COMMON_PATH
                + "singleChannel";

        /**
         * 外接栏目
         */
        public static final String EXTERNALCHANNEL = COMMON_PATH
                + "externalChannel";

        /**
         * 栏目1
         */
        public static final String CHANNEL_1 = COMMON_PATH + "channel_1";

        /**
         * 栏目2
         */
        public static final String CHANNEL_2 = COMMON_PATH + "channel_2";

        /**
         * 栏目3
         */
        public static final String CHANNEL_3 = COMMON_PATH + "channel_3";

        /**
         * 栏目列表
         */
        public static final String CHANNELLIST = COMMON_PATH + "channelList";

        /**
         * 栏目管理加载页面
         */
        public static final String CHANNELMANAGE = COMMON_PATH
                + "channelManage";

        /**
         * 图片频道上传结果页面
         */
        public static final String UPLOADPICRESULT = COMMON_PATH
                + "uploadPicResult";

        /**
         * 图片频道上传页面
         */
        public static final String UPLOADPIC = COMMON_PATH + "uploadPic";

        /**
         * 图片频道外部图片页面
         */
        public static final String UPLOADEXTERNALPIC = COMMON_PATH
                + "uploadExternalPic";

        /**
         * 友情链接列表
         */
        public static final String FRIENDSITELIST = COMMON_PATH
                + "friendSiteList";

        /**
         * 友情链接详情
         */
        public static final String FRIENDSITEDETAIL = COMMON_PATH
                + "friendSiteDetail";

        /**
         * 友情链接
         */
        public static final String FRIENDSITE = COMMON_PATH + "friendSite";

        /**
         * 评论列表
         */
        public static final String COMMENTLIST = COMMON_PATH + "commentList";

        /**
         * 评论详情
         */
        public static final String COMMENTDETAIL = COMMON_PATH
                + "commentDetail";

        /**
         * 公司页面
         */
        public static final String COMPANY = COMMON_PATH + "company";

        /**
         * 公司列表
         */
        public static final String COMPANYLIST = COMMON_PATH + "companyList";

        /**
         * 公司详情
         */
        public static final String COMPANYDETAIL = COMMON_PATH
                + "companyDetail";

        /**
         * 网站信息配置
         */
        public static final String WEBCONFIG = COMMON_PATH + "webConfig";

        /**
         * 网站信息配置列表
         */
        public static final String WEBCONFIGLIST = COMMON_PATH
                + "webConfigList";

        /**
         * 站点详情
         */
        public static final String WEBCONFIGDETAIL = COMMON_PATH
                + "webConfigDetail";

        /**
         * 选择上传图片页面
         */
        public static final String CHECKUPLOADPICS = COMMON_PATH
                + "checkUploadPics";

        /**
         * 新增修改留言回复页面
         */
        public static final String GUESTBOOKREPLY = COMMON_PATH
                + "guestBookReply";

        /**
         * 回复列表页面
         */
        public static final String GUESTBOOKREPLYLIST = COMMON_PATH
                + "guestBookReplyList";

        /**
         * 广告位列表
         */
        public static final String ADZONELIST = COMMON_PATH + "adZoneList";

        /**
         * 新增广告位
         */
        public static final String ADDADZONE = COMMON_PATH + "addAdZone";

        /**
         * 广告位详情
         */
        public static final String ADZONEDETAIL = COMMON_PATH + "adZoneDetail";

        /**
         * 广告列表页面
         */
        public static final String ADLIST = COMMON_PATH + "adList";

        /**
         * 广告页面
         */
        public static final String ADVERTISEMENT = COMMON_PATH
                + "advertisement";

        /**
         * 上传文件
         */
        public static final String UPLOADFILE = COMMON_PATH + "uploadFile";

        /**
         * 广告详情
         */
        public static final String ADDETAIL = COMMON_PATH + "adDetail";

        /**
         * 会员
         */
        public static final String MEMBER = COMMON_PATH + "member";

        /**
         * 会员列表
         */
        public static final String MEMBERLIST = COMMON_PATH + "memberList";

        /**
         * 会员详情
         */
        public static final String MEMBERDETAIL = COMMON_PATH + "memberDetail";

        /**
         * 前台页面url列表页面
         */
        public static final String RECEPTIONPAGEURLLIST = COMMON_PATH
                + "receptionPageUrlList";

        /**
         * 前台页面url新增修改页面
         */
        public static final String RECEPTIONPAGEURL = COMMON_PATH
                + "receptionPageUrl";

        /**
         * 前台页面url详情
         */
        public static final String RECEPTIONPAGEURLDETAIL = COMMON_PATH
                + "receptionPageUrlDetail";
    }

    /**
     *
     * 文件上传服务器相关配置
     *
     * @author zhuou
     * @version C03 2013-9-9
     * @since OSwift GPM V1.0
     */
    public interface Upload
    {
        /**
         * 文件上传的根目录
         */
        public static final String UPLOAD_ROOT_FILENAME = "upload";

        /**
         * 图片存储的目录名称
         */
        public static final String UPLOAD_IMG_FILENAME = "img";

        /**
         * 其他文件上传保存的目录名称
         */
        public static final String UPLOAD_FILE_FILENAME = "file";

        /**
         * 默认图片类型
         */
        public static final String DEFAULT_UPLOADIMG_TYPE = "jpg,jpeg,png,gif,bmp";

        /**
         * 默认文件类型
         */
        public static final String DEFAULT_UPLOADFILE_TYPE = "rar,doc,docx,zip,pdf,txt,swf,wmv";

        /**
         * 图片类型或者文件类型分隔符
         */
        public static final String FILE_SEPARATOR = ",";

        /**
         * URL分隔符
         */
        public static final String URL_SEPARATOR = "/";

        /**
         * 编辑器定义上传状态类型：成功
         */
        public static final String SUCCESS = "SUCCESS";

        /**
         * 编辑器定义上传状态类型：未包含文件上传域
         */
        public static final String NOFILE = "NOFILE";

        /**
         * 编辑器定义上传状态类型：不允许的文件格式
         */
        public static final String TYPE = "TYPE";

        /**
         * 编辑器定义上传状态类型：文件大小超出限制
         */
        public static final String SIZE = "SIZE";

        /**
         * 编辑器定义上传状态类型：请求类型ENTYPE错误
         */
        public static final String ENTYPE = "ENTYPE";

        /**
         * 编辑器定义上传状态类型：上传请求异常
         */
        public static final String REQUEST = "REQUEST";

        /**
         * 编辑器定义上传状态类型：IO异常
         */
        public static final String IO = "IO";

        /**
         * 编辑器定义上传状态类型：目录创建失败
         */
        public static final String DIR = "DIR";

        /**
         * 编辑器定义上传状态类型：未知错误
         */
        public static final String UNKNOWN = "UNKNOWN";
    }

    /**
     *
     * 上传文件需要定义的静态变量
     *
     * @author zhuou
     * @version C03 2013-10-28
     * @since OSwift GPM V1.0
     */
    public interface Files
    {
        /**
         * 图片类型
         */
        public static final int IMG = 1;

        /**
         * 文件类型
         */
        public static final int OTHERFILE = 2;
    }

    /**
     *
     * 页面类型0表示是栏目页面，1表示是用户设置的页面
     *
     * @author zhuou
     * @version C03 2014-1-16
     * @since OSwift GPM V1.0
     */
    public interface PageType
    {
        /**
         * 0表示是栏目页面
         */
        public static final int CHANNEL = 0;

        /**
         * 1表示是用户设置的页面
         */
        public static final int USER_DEFINED = 1;
    }

    /**
     *
     * 广告类型
     *
     * @author zhuou
     * @version C03 2014-3-16
     * @since OSwift GPM V1.0
     */
    public interface AdType
    {
        /**
         * 图片类型
         */
        public static final int IMG = 1;

        /**
         * 动画类型
         */
        public static final int ANIMATION = 2;

        /**
         * 文本类型
         */
        public static final int TXT = 3;
    }

    /**
     *
     * 用户组类型
     *
     * @author zhuou
     * @version C03 2014-3-23
     * @since OSwift GPM V1.0
     */
    public interface MemberGroupType
    {
        /**
         * 内置类型
         */
        public static final int BUILT_IN = 1;

        /**
         * 用户自定义类型
         */
        public static final int USER_DEFINED = 2;
    }

    /**
     *
     * 新增对评论的反对 赞成 中立
     *
     * @author zhuou
     * @version C03 2014-4-13
     * @since OSwift GPM V1.0
     */
    public interface CommentAttitude
    {
        /**
         * 支持
         */
        public static final String AGREE = "1";

        /**
         * 中立
         */
        public static final String NEUTRAL = "2";

        /**
         * 反对
         */
        public static final String OPPOSE = "3";
    }

    /**
     *
     * 文章点赞和踩
     *
     * @author zhuou
     * @version C03 2014-8-25
     * @since OSwift GPM V1.0
     */
    public interface Appraise
    {
        /**
         * 赞
         */
        public static final String PRAISE = "1";

        /**
         * 踩
         */
        public static final String TREAD = "2";
    }

    /**
     *
     * 会员常量字段类
     *
     * @author zhupu
     * @version C03 2014-9-19
     * @since OSwift CMS V1.0
     */
    public interface MemberField
    {
        /**
         * 用户名
         */
        public static final String LOGIN_USERNAME = "用户名";

        /**
         * Email
         */
        public static final String LOGIN_EMAIL = "Email";
    }

    /**
     *
     * 评论常量字段类
     *
     * @author zhuou
     * @version C03 2014-9-20
     * @since OSwift CMS V1.0
     */
    public interface CommentField
    {
        /**
         * 来自PC端评论
         */
        public static final int PC = 1;

        /**
         * 来自手机端评论
         */
        public static final int PHONE = 2;

        /**
         * 默认用户id，-1表示是游客
         */
        public static final long DEFAULT_USERID = -1;

        /**
         * 默认用户名
         */
        public static final String DEFAULT_USERNAME = "游客";
    }
}
