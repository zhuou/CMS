package com.oswift.utils.common;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * 读取properties配置文件数据
 *
 * @author zhuou
 * @version C03 2012-12-22
 * @since OSwift GPM V1.0
 */
public class ResourceHelper
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(ResourceHelper.class);

    /**
     * 国际化配置对象实例
     */
    private ResourceBundle config;

    /**
     *
     * 构造函数
     *
     * @param resourcePath
     *            资源文件名称或者路径
     */
    public ResourceHelper(String resourcePath)
    {
        config = ResourceBundle.getBundle(resourcePath);
    }

    /**
     * 根据配置项关键字获取主配置文件中配置项的值。当key不存在时，默认返回空字符串。
     *
     * @param key
     *            配置项关键字。
     * @return String 配置项的值。当key不存在时，默认返回空字符串。
     */
    public String getValueByKey(String key)
    {
        String value = "";
        try
        {
            value = config.getString(key);
        }
        catch (java.util.MissingResourceException e)
        {
            log
                    .error("ConfigHelper the key is not exist; the key is "
                            + key, e);
        }
        return value;
    }

    /**
     * 根据配置项关键字获取主配置文件中配置项的值。当不存在关键字为key的配置项时，返回defaultValue。
     *
     * @param key
     *            配置项关键字。
     * @param defaultValue
     *            配置项的默认值。当key不存在时，默认返回该值。
     * @return String 配置项的值。
     */
    public String getValueByKey(String key, String defaultValue)
    {
        String value = defaultValue;
        try
        {
            value = config.getString(key);
        }
        catch (java.util.MissingResourceException e)
        {
            log
                    .error("ConfigHelper the key is not exist; the key is "
                            + key, e);
        }
        if (StringUtil.isEmpty(value))
        {
            return defaultValue;
        }
        return value;
    }

    /**
     *
     * 获取所有properties配置文件数据
     *
     * @author zhuou
     * @return Map
     */
    public Map<String, String> getAllResource()
    {
        Map<String, String> map = new HashMap<String, String>();

        Enumeration<String> keys = config.getKeys();
        while (keys.hasMoreElements())
        {
            String key = keys.nextElement();
            String value = getValueByKey(key);
            map.put(key, value);
        }

        return map;
    }
}
