package com.oswift.utils.common;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SuppressWarnings("unchecked")
public class KeywordFilter
{
    /**
     * 直接禁止的
     */
    private HashMap keysMap = new HashMap();

    /**
     * 1:最小长度匹配 2：最大长度匹配
     */
    private int matchType = 1;

    /**
     *
     * 添加关键字
     *
     * @author zhuou
     * @param keywords
     *            关键字列表
     */
    public void addKeywords(List<String> keywords)
    {
        for (int i = 0; i < keywords.size(); i++)
        {
            String key = keywords.get(i).trim();
            HashMap nowhash = null;
            nowhash = keysMap;
            for (int j = 0; j < key.length(); j++)
            {
                char word = key.charAt(j);
                Object wordMap = nowhash.get(word);
                if (wordMap != null)
                {
                    nowhash = (HashMap) wordMap;
                }
                else
                {
                    HashMap<String, String> newWordHash = new HashMap<String, String>();
                    newWordHash.put("isEnd", "0");
                    nowhash.put(word, newWordHash);
                    nowhash = newWordHash;
                }
                if (j == key.length() - 1)
                {
                    nowhash.put("isEnd", "1");
                }
            }
        }
    }

    /**
     * 重置关键词
     */
    public void clearKeywords()
    {
        keysMap.clear();
    }

    /**
     *
     * 检查一个字符串从begin位置起开始是否有keyword符合， 如果有符合的keyword值，返回值为匹配keyword的长度，否则返回零
     *
     * @author zhuou
     * @param txt
     *            匹配文本
     * @param begin
     *            开始位置
     * @param flag
     *            flag 1:最小长度匹配 2：最大长度匹配
     * @return 返回值为匹配keyword的长度，否则返回零
     */
    private int checkKeyWords(String txt, int begin, int flag)
    {
        HashMap nowhash = null;
        nowhash = keysMap;
        int maxMatchRes = 0;
        int res = 0;
        int l = txt.length();
        char word = 0;
        for (int i = begin; i < l; i++)
        {
            word = txt.charAt(i);
            Object wordMap = nowhash.get(word);
            if (wordMap != null)
            {
                res++;
                nowhash = (HashMap) wordMap;
                if (((String) nowhash.get("isEnd")).equals("1"))
                {
                    if (flag == 1)
                    {
                        wordMap = null;
                        nowhash = null;
                        txt = null;
                        return res;
                    }
                    else
                    {
                        maxMatchRes = res;
                    }
                }
            }
            else
            {
                txt = null;
                nowhash = null;
                return maxMatchRes;
            }
        }
        txt = null;
        nowhash = null;
        return maxMatchRes;
    }

    /**
     *
     * 返回txt中关键字的列表
     *
     * @author zhuou
     * @param txt
     *            要匹配文本
     * @return 匹配字符串
     */
    public Set<String> getTxtKeyWords(String txt)
    {
        Set set = new HashSet();
        int l = txt.length();
        for (int i = 0; i < l;)
        {
            int len = checkKeyWords(txt, i, matchType);
            if (len > 0)
            {
                set.add(txt.substring(i, i + len));
                i += len;
            }
            else
            {
                i++;
            }
        }
        txt = null;
        return set;
    }

    /**
     *
     * 将关键字替换成其他字符
     *
     * @author zhuou
     * @param txt
     *            字符串
     * @param target
     *            要替换成的字符
     * @return String
     */
    public String replaceKeyword(String txt, char target)
    {
        int l = txt.length();
        for (int i = 0; i < l;)
        {
            int len = checkKeyWords(txt, i, matchType);
            if (len > 0)
            {
                StringBuilder sb = new StringBuilder(len);
                for (int j = 0; j < len; j++)
                {
                    sb.append(target);
                }
                txt = txt.substring(0, i).concat(sb.toString()).concat(
                        txt.substring(i + len));
                i += len;
            }
            else
            {
                i++;
            }
        }

        return txt;
    }

    /**
     * 仅判断txt中是否有关键字
     */
    public boolean isContentKeyWords(String txt)
    {
        for (int i = 0; i < txt.length(); i++)
        {
            int len = checkKeyWords(txt, i, 1);
            if (len > 0)
            {
                return true;
            }
        }
        txt = null;
        return false;
    }

    public int getMatchType()
    {
        return matchType;
    }

    public void setMatchType(int matchType)
    {
        this.matchType = matchType;
    }

    // public static void main(String[] args)
    // {
    // KeywordFilter filter = new KeywordFilter();
    // List<String> keywords = new ArrayList<String>();
    // keywords.add("回答这");
    // keywords.add("接受现状");
    // filter.addKeywords(keywords);
    // String txt = "回答这些问题前，我们不妨先来看看国内 APP
    // 的现状：大量新加入应用红海市场的APP，有许多以内嵌浏览内核来提供业务容器和网页浏览能力。系统浏览内核稳定性差强人意（崩溃闪退率接近1%），对安全漏洞的反应总是慢半拍，对于包括网络攻击、网络性能瓶颈在内的外部因素更是无能为力，而
    // app 的开发者限于研发周期、资金或技术制约，常常有心无力，只能接受现状。";
    // boolean boo = filter.isContentKeyWords(txt);
    // System.out.println(boo);
    // Set set = filter.getTxtKeyWords(txt);
    //
    // System.out.println(set);
    //
    // String str = filter.replaceKeyword(txt,'*');
    // System.out.println(str);
    // }
}
