package com.oswift.utils.common;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

/**
 * 汉字拼音工具
 *
 * @author zhuou
 */
public class PinyinUtils
{
    /**
     * 获取汉字串拼音首字母，英文字符不变
     *
     * @author zhuou
     * @param chinese
     *            汉字串
     * @return 汉语拼音首字母
     */
    public static String cn2FirstSpell(String chinese)
    {
        StringBuffer pybf = new StringBuffer();
        char[] arr = chinese.toCharArray();
        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
        defaultFormat.setCaseType(HanyuPinyinCaseType.UPPERCASE);
        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (int i = 0; i < arr.length; i++)
        {
            if (arr[i] > '\u4e00' && arr[i] < '\u9FA5')
            {
                try
                {
                    String[] t = PinyinHelper.toHanyuPinyinStringArray(arr[i],
                            defaultFormat);
                    if (t != null)
                    {
                        pybf.append(t[0].charAt(0));
                    }
                }
                catch (BadHanyuPinyinOutputFormatCombination e)
                {
                    e.printStackTrace();
                }
            }
            else
            {
                pybf.append(arr[i]);
            }
        }
        return pybf.toString().replaceAll("\\W", "").trim();
    }

    /**
     * 获取汉字串拼音，英文字符不变
     *
     * @author zhuou
     * @param chinese
     *            汉字串
     * @return 汉语拼音
     */
    public static String cn2Spell(String chinese)
    {
        StringBuffer pybf = new StringBuffer();
        char[] arr = chinese.toCharArray();
        HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
        defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (int i = 0; i < arr.length; i++)
        {
            if (arr[i] > '\u4e00' && arr[i] < '\u9FA5')
            {
                try
                {
                    pybf.append(PinyinHelper.toHanyuPinyinStringArray(arr[i],
                            defaultFormat)[0]);
                }
                catch (BadHanyuPinyinOutputFormatCombination e)
                {
                    e.printStackTrace();
                }
            }
            else
            {
                pybf.append(arr[i]);
            }
        }
        return pybf.toString();
    }
}
