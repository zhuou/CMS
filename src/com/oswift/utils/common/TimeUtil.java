package com.oswift.utils.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang.time.DateFormatUtils;

/**
 *
 * 日期时间处理工具类，支持UTC时间，并可配置。 <BR>
 *
 * 说明： <BR>
 * 1. 以ITZ结尾的函数是按照配置文件中配置的"接口时区"获取的时间,向接口里传时间参数以及解析接口里的时间参数时应该使用这类函数 <BR>
 * 2. 非ITZ结尾的函数是若未显式指定时区概念则是按照JVM的时区获取的时间,向数据库中插入时间字段数据时应该使用这类函数 <BR>
 *
 * @author zhuou
 * @version C03 2012-12-29
 * @since OSwift GPM V1.0
 */
public final class TimeUtil
{

    /**
     * 紧凑日期格式
     */
    public static final String COMPACT_DATE_FORMAT = "yyyyMM";

    /**
     * 紧凑日期格式+天
     */
    public static final String COMPACT_DATE_DAY_FORMAT = "yyyyMMdd";

    /**
     * 紧凑日期+时间格式
     */
    public static final String COMPACT_DATE_TIME_FORMAT = "yyyyMMddHHmmss";

    /**
     * 紧凑日期+时间(带毫秒)格式
     */
    public static final String COMPACT_DATE_TIME_MS_FORMAT = "yyyyMMddHHmmssSSS";

    /**
     * ISO标准日期格式
     */
    public static final String ISO_DATE_FORMAT = "yyyy-MM-dd";

    /**
     * ISO标准日期+时间格式
     */
    public static final String ISO_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * ISO标准日期+没有秒的时间格式
     */
    public static final String ISO_DATE_NOSECOND_FORMAT = "yyyy-MM-dd HH:mm";

    /**
     * ISO标准日期+时间(带毫秒)格式
     */
    public static final String ISO_DATE_TIME_MS_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

    private TimeUtil()
    {
    }

    /**
     * 获取当前时间
     *
     * @return 当前时间,类型为:java.sql.Timestamp
     */
    public static java.sql.Timestamp getCurrentSqlTimestamp()
    {
        return new java.sql.Timestamp(System.currentTimeMillis());
    }

    /**
     * 按照指定时区获取指定格式的当前时间字符串
     *
     * @param format
     *            时间格式
     * @param timezone
     *            时区名称
     * @return 当前时间字符串
     */
    public static String getCurTimeByFormat(String format, String timezone)
    {
        return DateFormatUtils.format(new Date(), format, TimeZone
                .getTimeZone(timezone));
    }

    /**
     * 按照JVM时区获取指定格式的当前时间字符串
     *
     * @param format
     *            时间格式
     * @return 当前时间字符串
     */
    public static String getCurTimeByFormat(String format)
    {
        return DateFormatUtils.format(new Date(), format);
    }

    /**
     * 按照JVM时区获取代表当前日期的ISO格式字符串
     *
     * @return 当前时间，格式为：yyyy-MM-dd
     */
    public static String getCurDateISOStr()
    {
        return getCurTimeByFormat(ISO_DATE_FORMAT);
    }

    /**
     * 按照JVM时区获取代表当前时间的紧凑格式字符串
     *
     * @return 当前时间，格式为：yyyyMMddHHmmss
     */
    public static String getCurDateTimeCompStr()
    {
        return getCurTimeByFormat(COMPACT_DATE_TIME_FORMAT);
    }

    /**
     * 按照JVM时区获取代表当前时间的ISO格式字符串,无毫秒值
     *
     * @return 当前时间，格式为：yyyy-MM-dd HH:mm:ss
     */
    public static String getCurDateTimeISOStr()
    {
        return getCurTimeByFormat(ISO_DATE_TIME_FORMAT);
    }

    /**
     * 按照JVM时区,将时间字符串转换为java.util.Date类型
     *
     * @param timeStr
     *            时间
     * @param format
     *            时间格式
     * @return java.util.Date类型的时间
     */
    public static java.util.Date stringToDate(String timeStr, String format)
    {
        DateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setLenient(false);
        java.util.Date date = null;
        try
        {
            date = dateFormat.parse(timeStr);
        }
        catch (ParseException e)
        {
            return null;
        }
        return date;
    }

    /**
     * 按照JVM时区,将时间字符串转换为java.sql.Date类型
     *
     * @param timeStr
     *            时间
     * @param format
     *            时间格式
     * @return java.sql.Date类型的时间
     */
    public static java.sql.Date stringToSqlDate(String timeStr, String format)
    {
        java.util.Date date = TimeUtil.stringToDate(timeStr, format);
        java.sql.Date sqlDate = null;
        if (null != date)
        {
            sqlDate = new java.sql.Date(date.getTime());
        }
        return sqlDate;
    }

    /**
     * 按照JVM时区,将时间字符串转换为java.sql.Timestamp类型
     *
     * @param timeStr
     *            时间
     * @param format
     *            时间格式
     * @return java.sql.Timestamp类型的时间
     */
    public static java.sql.Timestamp stringToSqlTimestamp(String timeStr,
            String format)
    {
        java.util.Date date = TimeUtil.stringToDate(timeStr, format);
        java.sql.Timestamp timestamp = null;
        if (null != date)
        {
            timestamp = new java.sql.Timestamp(date.getTime());
        }
        return timestamp;
    }

    /**
     * 按照JVM时区,将紧凑格式的时间字符串转换为java.util.Date类型
     *
     * @param timeStr
     *            时间字符串,格式为:yyyyMMddHHmmss
     * @return java.util.Date类型的时间
     */
    public static java.util.Date compStrToDate(String timeStr)
    {
        return stringToDate(timeStr, COMPACT_DATE_TIME_FORMAT);
    }

    /**
     * 按照JVM时区,将紧凑格式的时间字符串转换为java.sql.Date类型
     *
     * @param timeStr
     *            时间字符串,格式为:yyyyMMddHHmmss
     * @return java.sql.Date类型的时间
     */
    public static java.sql.Date compStrToSqlDate(String timeStr)
    {
        java.util.Date date = compStrToDate(timeStr);
        java.sql.Date sqlDate = null;
        if (null != date)
        {
            sqlDate = new java.sql.Date(date.getTime());
        }
        return sqlDate;
    }

    /**
     * 按照JVM时区,将紧凑格式的时间字符串转换为java.sql.Timestamp类型
     *
     * @param timeStr
     *            时间字符串,格式为:yyyyMMddHHmmss
     * @return java.sql.Timestamp类型的时间
     */
    public static java.sql.Timestamp compStrToSqlTimestamp(String timeStr)
    {
        java.util.Date date = compStrToDate(timeStr);
        java.sql.Timestamp timestamp = null;
        if (null != date)
        {
            timestamp = new java.sql.Timestamp(date.getTime());
        }
        return timestamp;
    }

    /**
     * 按照JVM时区,将java.util.Date类型转换为时间字符串
     *
     * @param date
     *            java.util.Date类型的时间
     * @param format
     *            时间格式
     * @return 按指定格式格式化后的时间字符串
     */
    public static String dateToString(java.util.Date date, String format)
    {
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

    /**
     *
     * 按照JVM时区,将当前时间后几年时间转换为时间字符串
     *
     * @param countYear
     *            需要往后推迟的年数
     * @param format
     *            时间格式
     * @return String
     */
    public static String dateToString(int countYear, String format)
    {
        Calendar cale = Calendar.getInstance();
        cale.setTime(new Date());
        cale.add(Calendar.YEAR, countYear);

        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(cale.getTime());
    }

    /**
     * 按照接口时区,将java.util.Date类型转换为紧凑格式的时间字符串
     *
     * @param date
     *            java.util.Date类型的时间
     * @return 时间字符串,格式为:yyyyMMddHHmmss
     */
    public static String dateToCompStr(Date date)
    {
        return dateToString(date, COMPACT_DATE_TIME_FORMAT);
    }
}
