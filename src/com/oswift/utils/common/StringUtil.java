package com.oswift.utils.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具类
 *
 * @author zhuou
 * @version C03 Sep 13, 2012
 * @since OSwift GPM V1.0
 */
public final class StringUtil
{
    /**
     * Key:Value字符串最小有效长度
     */
    private static final int MININUM_KEY_VALUE_STRING_LENGTH = 3;

    private StringUtil()
    {
    }

    /**
     * 根据正则表达式从字符串中检索Key:Value字符串中的Value
     *
     * @param element
     *            待检索字符串
     * @param regex
     *            正则表达式
     * @return 检索出的Value
     */
    public static String getValueByRegex(String element, String regex)
    {
        String result = null;
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(element);
        while (matcher.find())
        {
            result = matcher.group();
            if (result.length() > MININUM_KEY_VALUE_STRING_LENGTH)
            {
                int pos = result.indexOf(":");
                return result.substring(pos + 1, result.length());
            }
        }
        return null;
    }

    /**
     * 判断string是否为null，或长度是否为0
     *
     * @param str
     *            待判断的字符串
     *
     * @return 如果字符串为null，或长度为0，则返回true，否则返回false
     */
    public static boolean isEmpty(String str)
    {
        if ((null == str) || (str.length() == 0))
        {
            return true;
        }
        return false;
    }

    /**
     * 检查CharSequence是否是whitespace, empty ("") or null
     *
     * <pre>
     * StringUtils.isBlank(null)      = true
     * StringUtils.isBlank(&quot;&quot;)        = true
     * StringUtils.isBlank(&quot; &quot;)       = true
     * StringUtils.isBlank(&quot;bob&quot;)     = false
     * StringUtils.isBlank(&quot;  bob  &quot;) = false
     * </pre>
     *
     */
    public static boolean isBlank(CharSequence cs)
    {
        int strLen;
        if (cs == null || (strLen = cs.length()) == 0)
        {
            return true;
        }
        for (int i = 0; i < strLen; i++)
        {
            if (Character.isWhitespace(cs.charAt(i)) == false)
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 去除字符串前后的分隔符<br>
     * 使用场景：配置文件中有些配置，支持配置多个参数，每个参数间以某指定的分隔符分隔，支持的分隔符也可能有多种。<br>
     * 比如widbeans.properties中authentication.ip的配置，每个IP间以逗号,分隔。<br>
     * 考虑到配置时，可能因手误而在字符串的前后添加了多余的分隔符，或者仅配置为分隔符而忘记写实际的参数，提供了该方法
     *
     * @author zhuou
     * @param str
     *            待处理的string
     * @param seperator
     *            分隔符，分隔符的种类可能有多个
     * @return 处理后的结果
     */
    public static String trimSeperator(String str, String... seperator)
    {
        if (null == str)
        {
            return str;
        }

        if (null == seperator || seperator.length == 0)
        {
            return str;
        }

        for (String sep : seperator)
        {
            int len = sep.length();

            while (str.startsWith(sep))
            {
                str = str.substring(len);
            }
            while (str.endsWith(sep))
            {
                str = str.substring(0, str.length() - len);
            }
        }
        return str;
    }

    /**
     *
     * 替换字符串中字符，如xxxxxxx{0}xxxxxxx{1}xxxxxx,可以替换{0}....{n}个字符
     *
     * @author zhuou
     * @param source
     *            源字符串
     * @param value
     *            要替换的值
     * @return 字符串
     */
    public static String replace(String source, String... value)
    {
        if (isEmpty(source) || value.length == 0)
        {
            return null;
        }

        StringBuffer sb = new StringBuffer(source);
        for (int i = 0; i < value.length; i++)
        {
            String pattern = "{" + String.valueOf(i) + "}";
            int l = pattern.length();
            int n = sb.indexOf(pattern);
            if (-1 != n)
            {
                sb.replace(n, n + l, value[i]);
            }
        }
        return sb.toString();
    }

    /**
     *
     * 去除字符前后空格，如果字符串为null则返回null
     *
     * @author zhuou
     * @param source
     *            源字符串
     * @return String
     */
    public static String trim(String source)
    {
        if (null == source)
        {
            return null;
        }

        return source.trim();
    }

    /**
     *
     * 去除字符前后空格，如果字符串为null则返回defaultString默认值
     *
     * @author zhuou
     * @param source
     *            源字符串
     * @param defaultString
     *            默认值
     * @return String
     */
    public static String trim(String source, String defaultString)
    {
        if (null == source)
        {
            return defaultString;
        }

        return source.trim();
    }

    /**
     *
     * 判断字符串是否都是数字，如果都是数字，返回true，否则返回false
     *
     * @author zhuou
     * @param string
     *            字符串
     * @return boolean
     */
    public static boolean isNumeric(String string)
    {
        if (string == null || string.length() == 0)
            return false;

        int l = string.length();
        for (int i = 0; i < l; i++)
        {
            if (!Character.isDigit(string.codePointAt(i)))
                return false;
        }
        return true;
    }
}
