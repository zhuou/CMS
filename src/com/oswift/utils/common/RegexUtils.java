package com.oswift.utils.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtils
{
    /**
     * 数字
     */
    public static final String REGEX_NUMBER = "^[0-9]*$";

    /**
     * 大于0的正整数
     */
    public static final String REGEX_POSITIVE_INTEGER = "^+?[1-9][0-9]*$";

    /**
     * Email正则
     */
    public static final String REGEX_EMAIL = "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

    /**
     * 匹配正整数、正浮点数的正则表达式
     */
    public static final String REGEX_POSITIVE_FLOAT = "^[+]?([0-9]*\\.?[0-9]+|[0-9]+\\.?[0-9]*)([eE][+-]?[0-9]+)?$";

    /**
     * 判断string是否是数字
     *
     * @param str
     *            待判断的字符串
     *
     * @return 如果字符串null，或长度为0，或者不是数字，则返回false，否则返回true
     */
    public static boolean isNumber(String str)
    {
        if (isEmpty(str))
        {
            return false;
        }
        return isMatcher(str, REGEX_NUMBER);
    }

    /**
     *
     * 大于0的正整数
     *
     * @author zhuou
     * @param str
     *            待判断的字符串
     * @return 如果字符串null，或长度为0，或者不是大于0的正整数，则返回false，否则返回true
     */
    public static boolean isPositiveInteger(String str)
    {
        if (isEmpty(str))
        {
            return false;
        }
        return isMatcher(str, REGEX_POSITIVE_INTEGER);
    }

    /**
     *
     * 正浮点数
     *
     * @author zhuou
     * @param str
     *            待判断的字符串
     * @return 如果字符串null，或长度为0，或者不是正浮点数，则返回false，否则返回true
     */
    public static boolean isPositiveFloat(String str)
    {
        if (isEmpty(str))
        {
            return false;
        }
        return isMatcher(str, REGEX_POSITIVE_FLOAT);
    }

    /**
     *
     * 判断是否是合法的Email
     *
     * @author zhuou
     * @param str
     *            待判断的字符串
     * @return 如果字符串null，或长度为0，或者不是合法的Email，则返回false，否则返回true
     */
    public static boolean isEmail(String str)
    {
        if (isEmpty(str))
        {
            return false;
        }
        return isMatcher(str, REGEX_EMAIL);
    }

    /**
     *
     * 正则表达式匹配函数
     *
     * @author zhuou
     * @param str
     *            待判断的字符串
     * @param regex
     *            正则表达式
     * @return 如果匹配成功，返回true，否则返回false
     */
    private static boolean isMatcher(String str, String regex)
    {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }

    /**
     * 判断string是否为null，或长度是否为0
     *
     * @param str
     *            待判断的字符串
     *
     * @return 如果字符串为null，或长度为0，则返回true，否则返回false
     */
    private static boolean isEmpty(String str)
    {
        if ((null == str) || (str.trim().length() == 0))
        {
            return true;
        }
        return false;
    }

//    public static void main(String[] arg)
//    {
//        System.out.print(isEmail("qqqq@ssss.dd"));
//    }
}
