/*
 * 文 件 名：HttpUrlUtil.java
 * 版    权：Copyright 2010-2012 OSwift Tech.Co.Ltd.All Rights Reserved.
 * 描    述：HTTP URL 工具类
 * 修 改 人：zhuou
 * 修改时间：2009-8-15
 * 修改内容：新增
 */
package com.oswift.utils.http;

/**
 * HTTP URL 工具类
 * 
 * @author zhuou
 * @version C03 2009-8-15
 * @since OSwift V1.0
 */
public final class HttpUrlUtil
{
    /**
     * 构造函数
     */
    private HttpUrlUtil()
    {
    }

    /**
     * 从URL中获取IP和端口
     * 
     * @param httpUrl
     *            URL
     * @return IP和端口
     */
    public static String[] getIpAndPortFromHttpUrl(String httpUrl)
    {
        if (null == httpUrl || "".equals(httpUrl))
        {
            return null;
        }

        // 获取IP和端口的起始位置
        int start = httpUrl.indexOf("http://");
        if (start >= 0)
        {
            start += "http://".length();
        }
        else
        {
            start = httpUrl.indexOf("https://");
            if (start >= 0)
            {
                start += "https://".length();
            }
        }
        if (start < 0)
        {
            start = 0;
        }

        // 获取IP和端口的结束位置
        int end = httpUrl.indexOf("/", start);
        if (end < 0)
        {
            end = httpUrl.length();
        }
        String ipport = httpUrl.substring(start, end);

        // 解析IP和端口
        int index = ipport.indexOf(':');
        String host = null;
        String port = null;
        if (index < 0)
        {
            host = ipport;
            port = "80";
        }
        else
        {
            host = ipport.substring(0, index);
            port = ipport.substring(index + 1);
        }

        final int resultSize = 2;
        String[] result = new String[resultSize];
        result[0] = host;
        result[1] = port;
        return result;
    }
}
