package com.oswift.utils.exception;

/**
 *
 * 错误码定义
 *
 * @author Administrator
 * @version C03 Sep 13, 2012
 * @since OSwift GPM V1.0
 */
public interface ErrorCode
{
    /**
     * 未知错误
     */
    public static final String COMMON_UNKNOWN_ERROR = "common.unknown.error";

    /**
     * 数据库操作失败
     */
    public static final String COMMON_DB_ERROR = "5001";

    /**
     * 下载文件失败
     */
    public static final String COMMON_DOWNLOAD_REMOTE_FILE_FAIL = "5002";

    /**
     * 发送http请求失败
     */
    public static final String COMMON_SEND_HTTP_REQUEST_FAIL = "5003";

    /**
     * 打开http连接失败
     */
    public static final String COMMON_OPEN_HTTP_CONNECTION_FAIL = "5004";

    /**
     * 流操作失败
     */
    public static final String COMMON_OPERATE_STREAM_FAIL = "5005";

    /**
     * xml解析失败
     */
    public static final String COMMON_PARSE_XML_FAIL = "5006";

    /**
     * 文件删除失败
     */
    public static final String COMMON_FILE_DEL_FAIL = "5007";
}
