package com.oswift.utils.cache;

import com.oswift.utils.exception.PlatException;

/**
 * 
 * 缓存存取异常
 * 
 * @author zhuou
 * @version C03 Oct 27, 2012
 * @since OSwift GPM V1.0
 */
public class CacheAccessException extends PlatException
{
    private static final long serialVersionUID = 1L;

    /**
     * 带异常码的构造函数
     * 
     * @param exceptionCode
     *            异常码
     */
    public CacheAccessException(String exceptionCode)
    {
        super(exceptionCode);
    }

    /**
     * 带异常码、异常描述的构造函数
     * 
     * @param exceptionCode
     *            异常码
     * @param message
     *            异常描述
     */
    public CacheAccessException(String exceptionCode, String message)
    {
        super(exceptionCode, message);
    }

    /**
     * 带异常码的构造函数,封装其它异常
     * 
     * @param message
     *            异常信息
     * @param cause
     *            异常
     */
    public CacheAccessException(String message, Throwable cause)
    {
        super(message, cause);
    }

    /**
     * 封装其他异常为CacheAccessException
     * 
     * @param cause
     *            异常
     */
    public CacheAccessException(Throwable cause)
    {
        super(cause);
    }
}
