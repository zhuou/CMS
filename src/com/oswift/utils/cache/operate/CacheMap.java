package com.oswift.utils.cache.operate;

import java.util.Map;

import com.oswift.utils.cache.CacheAccessException;


/**
 * 
 * 用于存储缓存的Map，负责缓存数据的存储，并提供最底层的缓存存取操作。
 * 
 * @author zhuou
 * @version C03 Oct 27, 2012
 * @since OSwift GPM V1.0
 */
public interface CacheMap
{
    /**
     * 获取指定键所映射的值
     * 
     * @param key
     *            与指定值关联的键
     * @return Object 键所对应的值
     * @throws CacheAccessException
     *             缓存存取异常
     */
    Object get(Object key) throws CacheAccessException;

    /**
     * 返回整个缓存数据对象，该对象中包含该缓存的所有数据
     * 
     * @return 整个缓存对象
     * @throws CacheAccessException
     *             缓存存取异常
     */
    @SuppressWarnings("unchecked")
    Map getAll() throws CacheAccessException;

    /**
     * 将指定的值与此映射中的指定键关联
     * 
     * @param key
     *            与指定值关联的键
     * @param value
     *            与指定键关联的值
     * @throws CacheAccessException
     *             缓存存取异常
     */
    void put(Object key, Object value) throws CacheAccessException;

    /**
     * 批量增加缓存数据，健值已存在的数据将被新值替换。
     * 
     * @param cache
     *            缓存数据
     * @throws CacheAccessException
     *             缓存存取异常
     */
    @SuppressWarnings("unchecked")
    void putAll(Map cache) throws CacheAccessException;

    /**
     * 将指定的健对应的值从此映射中移除
     * 
     * @param key
     *            从映射中移除其映射关系的键
     * @throws CacheAccessException
     *             缓存存取异常
     */
    void remove(Object key) throws CacheAccessException;

    /**
     * 判断缓存中是否存在指定的健对应的值，如果此映射包含指定键的映射关系，则返回 true，否则返回false
     * 
     * @param key
     *            测试是否存在于此映射中的键
     * @return boolean 如果此映射包含指定键的映射关系，则返回 true
     * @throws CacheAccessException
     *             缓存存取异常
     */
    boolean containsKey(Object key) throws CacheAccessException;

    /**
     * 清空缓存数据
     * 
     * @throws CacheAccessException
     *             缓存存取异常
     */
    void clear() throws CacheAccessException;
}
