package com.oswift.utils.cache.operate.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.oswift.utils.cache.CacheAccessException;
import com.oswift.utils.cache.operate.CacheMap;

/**
 * 
 * 本地化缓存存储Map,缓存数据存放在本地内存中。
 * 
 * @author zhuou
 * @version C03 Oct 27, 2012
 * @since OSwift GPM V1.0
 */
public class LocalCacheMap implements CacheMap
{
    /**
     * 缓存名称
     */
    private String cacheName;

    /**
     * 真实存储缓存数据的Map
     */
    private Map<Object, Object> cacheData = Collections
            .synchronizedMap(new HashMap<Object, Object>());

    /**
     * {@inheritDoc}
     */
    public Object get(Object key) throws CacheAccessException
    {
        return cacheData.get(key);
    }

    /**
     * {@inheritDoc}
     */
    public Map<Object, Object> getAll() throws CacheAccessException
    {
        return cacheData;
    }

    /**
     * {@inheritDoc}
     */
    public synchronized void put(Object key, Object value)
        throws CacheAccessException
    {
        cacheData.put(key, value);
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public synchronized void putAll(Map cache) throws CacheAccessException
    {
        cacheData.putAll(cache);
    }

    /**
     * {@inheritDoc}
     */
    public synchronized void remove(Object key) throws CacheAccessException
    {
        cacheData.remove(key);
    }

    /**
     * {@inheritDoc}
     */
    public boolean containsKey(Object key) throws CacheAccessException
    {
        return cacheData.containsKey(key);
    }

    /**
     * {@inheritDoc}
     */
    public synchronized void clear() throws CacheAccessException
    {
        cacheData.clear();
    }

    /**
     * 取得cacheName
     * 
     * @return 返回 cacheName。
     */
    public String getCacheName()
    {
        return cacheName;
    }

    /**
     * 设置cacheName
     * 
     * @param cacheName
     *            要设置的 cacheName。
     */
    public void setCacheName(String cacheName)
    {
        this.cacheName = cacheName;
    }
}
