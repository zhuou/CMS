package com.oswift.utils.cache.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oswift.utils.cache.AbstractBusinessCache;
import com.oswift.utils.cache.CacheAccessException;
import com.oswift.utils.cache.manager.CacheFileds;

/**
 *
 * 系统全局变量缓存,缓存结构如下： <br>
 * Map<类名_变量名，对应值>
 *
 * @author Administrator
 * @version C03 Nov 5, 2012
 * @since OSwift GPM V1.0
 */
public class GlobalVariableCacheImpl extends AbstractBusinessCache
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(GlobalVariableCacheImpl.class);

    /**
     * 实现类启动加载缓存。
     *
     * @throws CacheAccessException
     *             缓存存取异常
     */
    @SuppressWarnings("unchecked")
    public void init() throws CacheAccessException
    {
        // 获取缓存内容
        Map<String, Object> map = getCacheMap().getAll();

        // 为空，则声明缓存空间；初始化静态变量对应值。
        if (null == map)
        {
            map = new HashMap<String, Object>();
        }

        getCacheMap().putAll(map);

        log.info("Cache ->> " + CacheFileds.CacheName.GLOBALVARIABLECACHE
                + " had Loaded.");
    }
}
