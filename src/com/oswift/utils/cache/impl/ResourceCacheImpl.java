package com.oswift.utils.cache.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oswift.utils.cache.AbstractBusinessCache;
import com.oswift.utils.cache.CacheAccessException;
import com.oswift.utils.cache.manager.CacheFileds;
import com.oswift.utils.common.ResourceHelper;

/**
 *
 * 缓存系统properties配置文件数据
 *
 * @author zhuou
 * @version C03 2012-12-22
 * @since OSwift GPM V1.0
 */
public class ResourceCacheImpl extends AbstractBusinessCache
{

    /**
     * 日志对象
     */
    private static Logger log = LoggerFactory.getLogger(ResourceCacheImpl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void init() throws CacheAccessException
    {
        ResourceHelper helper = new ResourceHelper("exception");
        Map<String, String> resourceMap = helper.getAllResource();

        // 先清除缓存
        getCacheMap().clear();

        getCacheMap().putAll(resourceMap);

        log.info("Cache ->> " + CacheFileds.CacheName.RESOURCECACHE
                + " had Loaded. Data:" + getCacheMap().getAll());
    }

}
