package com.oswift.utils.cache;

import java.util.Map;

/**
 * 
 * 业务缓存接口，所有业务缓存都必须实现此接口。
 * 
 * @author zhuou
 * @version C03 Oct 27, 2012
 * @since OSwift GPM V1.0
 */
public interface BusinessCache
{
    /**
     * 初始化缓存
     * 
     * @throws CacheAccessException
     *             缓存存取异常
     */
    void init() throws CacheAccessException;

    /**
     * 销毁缓存
     * 
     * @throws CacheAccessException
     *             缓存存取异常
     */
    void destroy() throws CacheAccessException;

    /**
     * 判断指定缓存中是否存在指定键对应的值
     * 
     * @param key
     *            具体某个缓存中对应key值
     * @return boolean 是否存在该健对应值
     * @throws CacheAccessException
     *             缓存存取异常
     */
    boolean containsKey(Object key) throws CacheAccessException;

    /**
     * 向指定缓存中增加一条数据，若键已存在则覆盖原来的值
     * 
     * @param key
     *            缓存Key值,其类型为必须与实际缓存Key值类型匹配
     * @param value
     *            待保存的值,其类型为必须与实际缓存Value值类型匹配
     * @throws CacheAccessException
     *             缓存存取异常
     */
    void set(Object key, Object value) throws CacheAccessException;

    /**
     * 根据Key值从指定缓存中获取Value。
     * 
     * @param key
     *            缓存Key值
     * @return Object 缓存Value值
     * @throws CacheAccessException
     *             缓存存取异常
     */
    Object get(Object key) throws CacheAccessException;

    /**
     * 返回整个缓存数据对象，该对象中包含该缓存的所有数据
     * 
     * @return Object 整个缓存对象
     * @throws CacheAccessException
     *             缓存存取异常
     */
    @SuppressWarnings("unchecked")
    Map getAllCache() throws CacheAccessException;

    /**
     * 通过Key值，从指定缓存中删除相应内容。
     * 
     * @param key
     *            缓存Key值
     * @throws CacheAccessException
     *             缓存存取异常
     */
    void remove(Object key) throws CacheAccessException;
}
