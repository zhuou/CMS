package com.oswift.utils.cache.manager;

/**
 * 
 * 定义缓存常量名称
 * 
 * @author zhuou
 * @version C03 Oct 27, 2012
 * @since OSwift GPM V1.0
 */
public interface CacheFileds
{
    /**
     * 
     * 缓存名称常量
     * 
     * @author zhuou
     * @version C03 Oct 27, 2012
     * @since OSwift GPM V1.0
     */
    interface CacheName
    {
        /**
         * 系统全局变量缓存
         */
        String GLOBALVARIABLECACHE = "globalVariableCache";
        
        /**
         * 缓存系统properties配置文件数据
         */
        String RESOURCECACHE = "resourceCache";
    }

    /**
     * 
     * 缓存对应全局变量常量名称
     * 
     * @author zhuou
     * @version C03 Oct 27, 2012
     * @since OSwift GPM V1.0
     */
    interface CacheCommonField
    {
        /**
         * 系统所有缓存加载标志 0:未加载 1:加载中 2:加载完成
         */
        String GLOBAL_CACHEFLAG = "cache_flag";

        /**
         * 0:未加载
         */
        String GLOBAL_CACHENOLOAD = "0";

        /**
         * 1:加载中
         */
        String GLOBAL_CACHELOADING = "1";

        /**
         * 2:加载完成
         */
        String GLOBAL_CACHELOADED = "2";
    }
}
