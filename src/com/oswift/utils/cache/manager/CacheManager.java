package com.oswift.utils.cache.manager;

import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oswift.utils.cache.BusinessCache;
import com.oswift.utils.cache.CacheAccessException;

/**
 *
 * 集中管理缓存处理类，主要负责统一加载、销毁、刷新等操作。
 *
 * @author zhuou
 * @version C03 Oct 27, 2012
 * @since OSwift GPM V1.0
 */
public class CacheManager
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(CacheManager.class);

    /**
     * 业务缓存容器
     */
    private static Map<String, BusinessCache> businessCacheMap;

    /**
     * 统一加载全部缓存
     *
     * @throws CacheAccessException
     *             缓存存取异常
     */
    @SuppressWarnings("unchecked")
    public static void init() throws CacheAccessException
    {
        if (businessCacheMap == null)
        {
            log.warn("Cache ->> Cache list is empty.");
            return;
        }

        BusinessCache globalVarCacheInstance = getBusinessCache(CacheFileds.CacheName.GLOBALVARIABLECACHE);

        // 如果从Spring容器中获取实例失败，则抛出异常。
        if (null == globalVarCacheInstance)
        {
            log
                    .error("Fail to get global variable cache instance from spring container.");

            throw new CacheAccessException("Server's cache init error.");
        }

        try
        {
            String flag = (String) globalVarCacheInstance
                    .get(CacheFileds.CacheCommonField.GLOBAL_CACHEFLAG);

            // 如果标识为空或状态为未加载，则加载缓存，其他情况则不加载缓存
            if (null == flag
                    || CacheFileds.CacheCommonField.GLOBAL_CACHENOLOAD
                            .equals(flag))
            {
                // 设置缓存加载标志为加载中
                // 由于加载系统缓存时，全局变量缓存尚未加载，这时从全局变量缓存中获取缓存加载标志，会抛出异常。
                globalVarCacheInstance.set(
                        CacheFileds.CacheCommonField.GLOBAL_CACHEFLAG,
                        CacheFileds.CacheCommonField.GLOBAL_CACHELOADING);

                // 遍历key集合，获取BusinessCache
                Iterator it = businessCacheMap.entrySet().iterator();

                Map.Entry<String, BusinessCache> entry = null;

                while (it.hasNext())
                {
                    entry = (Map.Entry<String, BusinessCache>) it.next();
                    ((BusinessCache) (entry.getValue())).init();
                }

                // 设置缓存加载标志为加载完成
                globalVarCacheInstance.set(
                        CacheFileds.CacheCommonField.GLOBAL_CACHEFLAG,
                        CacheFileds.CacheCommonField.GLOBAL_CACHELOADED);

                log.info("Cache ->> "
                        + CacheFileds.CacheName.GLOBALVARIABLECACHE
                        + " had Loaded. Data:"
                        + globalVarCacheInstance.getAllCache());

            }
            else
            {
                log
                        .info("Cache ->> All cache had been loaded or is being loaded.");
            }
        }
        catch (CacheAccessException cae)
        {
            log.error("Cache ->> Fail to init cache.", cae);

            // 设置缓存加载标志为未加载；
            // 如果尚未初始化全局变量缓存
            globalVarCacheInstance.set(
                    CacheFileds.CacheCommonField.GLOBAL_CACHEFLAG,
                    CacheFileds.CacheCommonField.GLOBAL_CACHENOLOAD);

            throw new CacheAccessException(cae);
        }
    }

    /**
     * 统一销毁全部缓存
     */
    @SuppressWarnings("unchecked")
    public static void destroy()
    {
        if (businessCacheMap == null)
        {
            log.warn("Cache ->> Cache list is empty.");
            return;
        }

        // 遍历key集合，获取BusinessCache
        Iterator it = businessCacheMap.entrySet().iterator();

        Map.Entry<String, BusinessCache> entry = null;

        while (it.hasNext())
        {
            entry = (Map.Entry<String, BusinessCache>) it.next();

            try
            {
                ((BusinessCache) (entry.getValue())).destroy();
            }
            catch (CacheAccessException ce)
            {
                log.error("Cache ->> Fail to destroy cache [" + entry.getKey()
                        + "].", ce);
            }
        }
    }

    /**
     * 通过缓存实例名称，从容器中获取具体缓存实例对象
     *
     * @param cacheName
     *            缓存实例名称
     * @return BusinessCache 具体业务缓存实例
     */
    public static BusinessCache getBusinessCache(final String cacheName)
    {
        if (businessCacheMap == null)
        {
            log.warn("Cache ->> Cache list is empty.");
            return null;
        }

        BusinessCache businessCache = businessCacheMap.get(cacheName);

        if (null == businessCache)
        {
            log.info("Cache ->> Fail to get cache instance of " + cacheName
                    + ",the cache instance is null.");
        }

        return businessCache;
    }

    /**
     * 取得businessCacheMap
     *
     * @return 返回 businessCacheMap。
     */
    public Map<String, BusinessCache> getBusinessCacheMap()
    {
        return businessCacheMap;
    }

    /**
     * 设置businessCacheMap
     *
     * @param businessCacheMap
     *            要设置的 businessCacheMap。
     */
    @SuppressWarnings("static-access")
    public void setBusinessCacheMap(Map<String, BusinessCache> businessCacheMap)
    {
        this.businessCacheMap = businessCacheMap;
    }
}
