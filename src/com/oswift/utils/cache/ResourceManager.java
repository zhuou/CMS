package com.oswift.utils.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oswift.utils.cache.manager.CacheFileds;
import com.oswift.utils.cache.manager.CacheManager;
import com.oswift.utils.common.StringUtil;

/**
 *
 * 操作系统properties配置文件数据
 *
 * @author zhuou
 * @version C03 2012-12-22
 * @since OSwift GPM V1.0
 */
public class ResourceManager
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(ResourceManager.class);

    /**
     * 记录系统properties配置文件缓存实例
     */
    private static BusinessCache systemConfigCacheInstance = CacheManager
            .getBusinessCache(CacheFileds.CacheName.RESOURCECACHE);

    /**
     *
     * 根据key获取系统properties配置文件数据
     *
     * @author zhuou
     * @param key
     * @return properties配置文件value
     */
    public static String getValue(String key)
    {
        String value = null;
        if (StringUtil.isEmpty(key))
        {
            return value;
        }
        try
        {
            value = (String) systemConfigCacheInstance.get(key);
        }
        catch (CacheAccessException e)
        {
            log.error("Fail to get value of properties, key=" + key);
        }
        return value;
    }
}
