package com.oswift.utils.cache;

import java.util.Map;

import com.oswift.utils.cache.operate.CacheMap;


/**
 *
 * 实现缓存基本操作抽象类，主要实现通用的增、删、改、查等基本动作。
 *
 * @author zhuou
 * @version C03 Oct 27, 2012
 * @since OSwift GPM V1.0
 */
public abstract class AbstractBusinessCache implements BusinessCache
{
    /**
     * 缓存存储实体
     */
    private CacheMap cacheMap = null;

    /**
     * {@inheritDoc}
     */
    public synchronized void destroy() throws CacheAccessException
    {
        cacheMap.clear();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public Map getAllCache() throws CacheAccessException
    {
        return cacheMap.getAll();
    }

    /**
     * {@inheritDoc}
     */
    public synchronized Object get(Object key) throws CacheAccessException
    {
        return cacheMap.get(key);
    }

    /**
     * {@inheritDoc}
     */
    public boolean containsKey(Object key) throws CacheAccessException
    {
        return cacheMap.containsKey(key);
    }

    /**
     * {@inheritDoc}
     */
    public synchronized void remove(Object key) throws CacheAccessException
    {
        cacheMap.remove(key);
    }

    /**
     * {@inheritDoc}
     */
    public synchronized void set(Object key, Object value)
        throws CacheAccessException
    {
        cacheMap.put(key, value);
    }

    /**
     * 取得cacheMap
     *
     * @return 返回 cacheMap。
     */
    public CacheMap getCacheMap()
    {
        return cacheMap;
    }

    /**
     * 设置cacheMap
     *
     * @param cacheMap
     *            要设置的 cacheMap。
     */
    public void setCacheMap(CacheMap cacheMap)
    {
        this.cacheMap = cacheMap;
    }
}
