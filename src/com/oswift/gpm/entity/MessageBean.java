package com.oswift.gpm.entity;

/**
 * 
 * 公共返回消息类
 * 
 * @author zhuou
 * @version C03 2012-12-22
 * @since OSwift GPM V1.0
 */
public class MessageBean
{
    /**
     * 返回码
     */
    private String code;

    /**
     * 返回内容
     */
    private String content = "";

    /**
     * obj类型
     */
    private Object objContent;

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public Object getObjContent()
    {
        return objContent;
    }

    public void setObjContent(Object objContent)
    {
        this.objContent = objContent;
    }
}
