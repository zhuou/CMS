package com.oswift.gpm.entity;

public class WhiteListBean
{
    /**
     * 白名单ID
     */
    private int whiteListId;

    /**
     * 请求Url名称
     */
    private String name;

    /**
     * 请求Url
     */
    private String requestURI;

    public int getWhiteListId()
    {
        return whiteListId;
    }

    public void setWhiteListId(int whiteListId)
    {
        this.whiteListId = whiteListId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getRequestURI()
    {
        return requestURI;
    }

    public void setRequestURI(String requestURI)
    {
        this.requestURI = requestURI;
    }
}
