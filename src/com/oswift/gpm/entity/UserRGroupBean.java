package com.oswift.gpm.entity;

public class UserRGroupBean
{
    /**
     * 用户ID
     */
    private int userId;

    /**
     * 系统ID
     */
    private int systemId;

    /**
     * 组ID
     */
    private int groupId;

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public int getSystemId()
    {
        return systemId;
    }

    public void setSystemId(int systemId)
    {
        this.systemId = systemId;
    }

    public int getGroupId()
    {
        return groupId;
    }

    public void setGroupId(int groupId)
    {
        this.groupId = groupId;
    }
}
