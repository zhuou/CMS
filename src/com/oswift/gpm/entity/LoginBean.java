package com.oswift.gpm.entity;


public class LoginBean
{
    /**
     * systemId+userId的hash值
     */
    private long hashId;

    /**
     * sessionId
     */
    private String sessionId;

    /**
     * 系统ID
     */
    private int systemId;

    /**
     * 系统名称
     */
    private String systemName;

    /**
     * 系统IP
     */
    private String systemIP;

    /**
     * 域名或者系统IP+端口，用于拼接请求地址使用
     */
    private String hostAddr;

    /**
     * 通信签证信息，MD5（密钥+系统ID）
     */
    private String Signed;

    /**
     * 用户ID
     */
    private int userId;

    /**
     * 管理员名称
     */
    private String userName;

    public long getHashId()
    {
        return hashId;
    }

    public void setHashId(long hashId)
    {
        this.hashId = hashId;
    }

    public int getSystemId()
    {
        return systemId;
    }

    public void setSystemId(int systemId)
    {
        this.systemId = systemId;
    }

    public String getSystemName()
    {
        return systemName;
    }

    public void setSystemName(String systemName)
    {
        this.systemName = systemName;
    }

    public String getSystemIP()
    {
        return systemIP;
    }

    public void setSystemIP(String systemIP)
    {
        this.systemIP = systemIP;
    }

    public String getSigned()
    {
        return Signed;
    }

    public void setSigned(String signed)
    {
        Signed = signed;
    }

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }

    public String getHostAddr()
    {
        return hostAddr;
    }

    public void setHostAddr(String hostAddr)
    {
        this.hostAddr = hostAddr;
    }
}
