package com.oswift.gpm.entity;

import java.util.Date;

import com.oswift.utils.common.TimeUtil;

/**
 *
 * 系统模块实体类
 *
 * @author zhuou
 * @version C03 Sep 14, 2012
 * @since OSwift GPM V1.0
 */
public class SystemModuleBean
{
    /**
     * 系统ID
     */
    private int systemId;

    /**
     * 系统名称
     */
    private String systemName;

    /**
     * 系统描述
     */
    private String systemDesc;

    /**
     * 系统IP，用于验证使用
     */
    private String systemIP;

    /**
     * 域名或者系统IP+端口，用于拼接请求地址使用
     */
    private String hostAddr;

    /**
     * 密钥
     */
    private String secretKey;

    /**
     * 通信签证信息，MD5（密钥+系统ID）
     */
    private String signed;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 最后编辑时间
     */
    private String lastEditTime;

    /**
     * 最后编辑人
     */
    private String lastEditUser;

    public int getSystemId()
    {
        return systemId;
    }

    public void setSystemId(int systemId)
    {
        this.systemId = systemId;
    }

    public String getSystemName()
    {
        return systemName;
    }

    public void setSystemName(String systemName)
    {
        this.systemName = systemName;
    }

    public String getSystemDesc()
    {
        return systemDesc;
    }

    public void setSystemDesc(String systemDesc)
    {
        this.systemDesc = systemDesc;
    }

    public String getSystemIP()
    {
        return systemIP;
    }

    public void setSystemIP(String systemIP)
    {
        this.systemIP = systemIP;
    }

    public String getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        this.createTime = TimeUtil.dateToString(createTime,
                TimeUtil.ISO_DATE_NOSECOND_FORMAT);
    }

    public String getCreateUser()
    {
        return createUser;
    }

    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }

    public String getLastEditTime()
    {
        return lastEditTime;
    }

    public void setLastEditTime(Date lastEditTime)
    {
        this.lastEditTime = TimeUtil.dateToString(lastEditTime,
                TimeUtil.ISO_DATE_NOSECOND_FORMAT);
    }

    public String getLastEditUser()
    {
        return lastEditUser;
    }

    public void setLastEditUser(String lastEditUser)
    {
        this.lastEditUser = lastEditUser;
    }

    public String getSecretKey()
    {
        return secretKey;
    }

    public void setSecretKey(String secretKey)
    {
        this.secretKey = secretKey;
    }

    public String getSigned()
    {
        return signed;
    }

    public void setSigned(String signed)
    {
        this.signed = signed;
    }

    public String getHostAddr()
    {
        return hostAddr;
    }

    public void setHostAddr(String hostAddr)
    {
        this.hostAddr = hostAddr;
    }
}
