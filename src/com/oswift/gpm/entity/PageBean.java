package com.oswift.gpm.entity;

import java.util.List;

/**
 *
 * 主要用于列表页面分页
 *
 * @author zhuou
 * @version C03 2013-1-8
 * @since OSwift GPM V1.0
 */
public class PageBean
{
    /**
     * 总记录数
     */
    private int totalRecordNum;

    /**
     * 每页记录数
     */
    private int pageRecordNum = 15;

    /**
     * 获取的数据
     */
    private List<?> dataList;

    public int getTotalRecordNum()
    {
        return totalRecordNum;
    }

    public void setTotalRecordNum(int totalRecordNum)
    {
        this.totalRecordNum = totalRecordNum;
    }

    public int getPageRecordNum()
    {
        return pageRecordNum;
    }

    public void setPageRecordNum(int pageRecordNum)
    {
        this.pageRecordNum = pageRecordNum;
    }

    public List<?> getDataList()
    {
        return dataList;
    }

    public void setDataList(List<?> dataList)
    {
        this.dataList = dataList;
    }
}
