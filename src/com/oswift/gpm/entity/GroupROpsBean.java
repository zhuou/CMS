package com.oswift.gpm.entity;

public class GroupROpsBean
{
    /**
     * 组ID
     */
    private int groupId;

    /**
     * 操作ID
     */
    private int opsId;

    /**
     * 是否继承父节点的权限，false不继承 true继承，默认false
     */
    private boolean inherit = false;

    public int getGroupId()
    {
        return groupId;
    }

    public void setGroupId(int groupId)
    {
        this.groupId = groupId;
    }

    public int getOpsId()
    {
        return opsId;
    }

    public void setOpsId(int opsId)
    {
        this.opsId = opsId;
    }

    public boolean isInherit()
    {
        return inherit;
    }

    public void setInherit(boolean inherit)
    {
        this.inherit = inherit;
    }
}
