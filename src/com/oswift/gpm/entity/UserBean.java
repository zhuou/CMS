package com.oswift.gpm.entity;

import java.util.Date;

import com.oswift.utils.common.TimeUtil;

public class UserBean extends UserContacterBean
{
    /**
     * 用户ID
     */
    private int userId;

    /**
     * 管理员名称
     */
    private String userName;

    /**
     * 管理员密码
     */
    private String password;

    /**
     * 允许多人同时使用此帐号登录
     */
    private boolean enableMultiLogin;

    /**
     * 容许登录错误次数，0为不限制
     */
    private int loginErrorTime;

    /**
     * 锁定时间长度，单位分钟。默认为0，当为0时，为无限长时间
     */
    private int lockTime;

    /**
     * 用户状态。0－－正常，1－－超过登录错误次数锁定，2－－被管理员锁定'
     */
    private int status;

    /**
     * 登录次数
     */
    private int loginCount;

    /**
     * 最近登录IP
     */
    private String lastLoginIP;

    /**
     * 最近登录时间
     */
    private String lastLoginTime;

    /**
     * 最近退出管理后台时间
     */
    private String lastLogoutTime;

    /**
     * 最近修改密码时间
     */
    private String lastModifyPasswordTime;

    /**
     * 是否允许修改密码
     */
    private boolean enableModifyPassword;

    /**
     * 账户有效期，如果不填则无限期
     */
    private String accountExpires;

    /**
     * 登录错误次数
     */
    private int loginErrorCount;

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public boolean isEnableMultiLogin()
    {
        return enableMultiLogin;
    }

    public void setEnableMultiLogin(boolean enableMultiLogin)
    {
        this.enableMultiLogin = enableMultiLogin;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public int getLoginCount()
    {
        return loginCount;
    }

    public void setLoginCount(int loginCount)
    {
        this.loginCount = loginCount;
    }

    public String getLastLoginIP()
    {
        return lastLoginIP;
    }

    public void setLastLoginIP(String lastLoginIP)
    {
        this.lastLoginIP = lastLoginIP;
    }

    public String getLastLoginTime()
    {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime)
    {
        if (null == lastLoginTime)
        {
            this.lastLoginTime = null;
        }
        else
        {
            this.lastLoginTime = TimeUtil.dateToString(lastLoginTime,
                    TimeUtil.ISO_DATE_NOSECOND_FORMAT);
        }
    }

    public String getLastLogoutTime()
    {
        return lastLogoutTime;
    }

    public void setLastLogoutTime(Date lastLogoutTime)
    {
        if (null == lastLogoutTime)
        {
            this.lastLogoutTime = null;
        }
        else
        {
            this.lastLogoutTime = TimeUtil.dateToString(lastLogoutTime,
                    TimeUtil.ISO_DATE_NOSECOND_FORMAT);
        }
    }

    public String getLastModifyPasswordTime()
    {
        return lastModifyPasswordTime;
    }

    public void setLastModifyPasswordTime(Date lastModifyPasswordTime)
    {
        if (null == lastModifyPasswordTime)
        {
            this.lastModifyPasswordTime = null;
        }
        else
        {
            this.lastModifyPasswordTime = TimeUtil.dateToString(
                    lastModifyPasswordTime, TimeUtil.ISO_DATE_TIME_FORMAT);
        }
    }

    public boolean isEnableModifyPassword()
    {
        return enableModifyPassword;
    }

    public void setEnableModifyPassword(boolean enableModifyPassword)
    {
        this.enableModifyPassword = enableModifyPassword;
    }

    public String getAccountExpires()
    {
        return accountExpires;
    }

    public void setAccountExpires(Date accountExpires)
    {
        if (null != accountExpires)
        {
            this.accountExpires = TimeUtil.dateToString(accountExpires,
                    TimeUtil.ISO_DATE_FORMAT);
        }
        else
        {
            this.accountExpires = null;
        }
    }

    public int getLoginErrorTime()
    {
        return loginErrorTime;
    }

    public void setLoginErrorTime(int loginErrorTime)
    {
        this.loginErrorTime = loginErrorTime;
    }

    public int getLockTime()
    {
        return lockTime;
    }

    public void setLockTime(int lockTime)
    {
        this.lockTime = lockTime;
    }

    public int getLoginErrorCount()
    {
        return loginErrorCount;
    }

    public void setLoginErrorCount(int loginErrorCount)
    {
        this.loginErrorCount = loginErrorCount;
    }
}
