package com.oswift.gpm.entity;

public class GroupRRoleBean
{
    /**
     * 组ID
     */
    private int groupId;

    /**
     * 角色ID
     */
    private int roleId;

    /**
     * 是否继承父节点的权限，false不继承 true继承，默认false
     */
    private boolean inherit = false;

    public int getGroupId()
    {
        return groupId;
    }

    public void setGroupId(int groupId)
    {
        this.groupId = groupId;
    }

    public int getRoleId()
    {
        return roleId;
    }

    public void setRoleId(int roleId)
    {
        this.roleId = roleId;
    }

    public boolean isInherit()
    {
        return inherit;
    }

    public void setInherit(boolean inherit)
    {
        this.inherit = inherit;
    }
}
