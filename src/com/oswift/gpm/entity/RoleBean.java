package com.oswift.gpm.entity;

import java.util.Date;

import com.oswift.utils.common.TimeUtil;

public class RoleBean
{
    /**
     * 角色ID
     */
    private int roleId;

    /**
     * 系统ID
     */
    private int systemId;

    /**
     * 系统名称
     */
    private String systemName;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色描述
     */
    private String roleDesc;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 最后编辑时间
     */
    private String lastEditTime;

    /**
     * 最后编辑人
     */
    private String lastEditUser;

    /**
     * 是否被选中（此字段主要在用户权限设置时使用）
     */
    private boolean checked = false;

    public int getRoleId()
    {
        return roleId;
    }

    public void setRoleId(int roleId)
    {
        this.roleId = roleId;
    }

    public int getSystemId()
    {
        return systemId;
    }

    public void setSystemId(int systemId)
    {
        this.systemId = systemId;
    }

    public String getRoleName()
    {
        return roleName;
    }

    public void setRoleName(String roleName)
    {
        this.roleName = roleName;
    }

    public String getRoleDesc()
    {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc)
    {
        this.roleDesc = roleDesc;
    }

    public String getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        this.createTime = TimeUtil.dateToString(createTime,
                TimeUtil.ISO_DATE_NOSECOND_FORMAT);
    }

    public String getCreateUser()
    {
        return createUser;
    }

    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }

    public String getLastEditTime()
    {
        return lastEditTime;
    }

    public void setLastEditTime(Date lastEditTime)
    {
        this.lastEditTime = TimeUtil.dateToString(lastEditTime,
                TimeUtil.ISO_DATE_NOSECOND_FORMAT);
    }

    public String getLastEditUser()
    {
        return lastEditUser;
    }

    public void setLastEditUser(String lastEditUser)
    {
        this.lastEditUser = lastEditUser;
    }

    public String getSystemName()
    {
        return systemName;
    }

    public void setSystemName(String systemName)
    {
        this.systemName = systemName;
    }

    public boolean isChecked()
    {
        return checked;
    }

    public void setChecked(boolean checked)
    {
        this.checked = checked;
    }
}
