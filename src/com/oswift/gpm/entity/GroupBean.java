package com.oswift.gpm.entity;

import java.util.Date;

import com.oswift.utils.common.TimeUtil;

public class GroupBean
{
    /**
     * 组ID
     */
    private int groupId;

    /**
     * 系统ID
     */
    private int systemId;

    /**
     * 系统名称
     */
    private String systemName;

    /**
     * 父节点ID 如果没有父节点则为-1
     */
    private int parentId;

    /**
     * 父节点名称
     */
    private String parentName;

    /**
     * 组名称
     */
    private String groupName;

    /**
     * 组描述
     */
    private String groupDesc;

    /**
     * 是否继承父节点的权限，0不继承 1继承，默认0
     */
    private boolean inherit = false;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 最后编辑时间
     */
    private String lastEditTime;

    /**
     * 最后编辑人
     */
    private String lastEditUser;

    /**
     * 是否被选中（此字段主要在用户权限设置时使用）
     */
    private boolean checked;

    public int getGroupId()
    {
        return groupId;
    }

    public void setGroupId(int groupId)
    {
        this.groupId = groupId;
    }

    public int getSystemId()
    {
        return systemId;
    }

    public void setSystemId(int systemId)
    {
        this.systemId = systemId;
    }

    public String getSystemName()
    {
        return systemName;
    }

    public void setSystemName(String systemName)
    {
        this.systemName = systemName;
    }

    public int getParentId()
    {
        return parentId;
    }

    public void setParentId(int parentId)
    {
        this.parentId = parentId;
    }

    public String getParentName()
    {
        return parentName;
    }

    public void setParentName(String parentName)
    {
        this.parentName = parentName;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public boolean getInherit()
    {
        return inherit;
    }

    public void setInherit(boolean inherit)
    {
        this.inherit = inherit;
    }

    public String getGroupDesc()
    {
        return groupDesc;
    }

    public void setGroupDesc(String groupDesc)
    {
        this.groupDesc = groupDesc;
    }

    public String getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        this.createTime = TimeUtil.dateToString(createTime,
                TimeUtil.ISO_DATE_NOSECOND_FORMAT);
    }

    public String getCreateUser()
    {
        return createUser;
    }

    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }

    public String getLastEditTime()
    {
        return lastEditTime;
    }

    public void setLastEditTime(Date lastEditTime)
    {
        this.lastEditTime = TimeUtil.dateToString(lastEditTime,
                TimeUtil.ISO_DATE_NOSECOND_FORMAT);
    }

    public String getLastEditUser()
    {
        return lastEditUser;
    }

    public void setLastEditUser(String lastEditUser)
    {
        this.lastEditUser = lastEditUser;
    }

    public boolean isChecked()
    {
        return checked;
    }

    public void setChecked(boolean checked)
    {
        this.checked = checked;
    }
}
