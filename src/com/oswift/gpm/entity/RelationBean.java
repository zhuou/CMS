package com.oswift.gpm.entity;

public class RelationBean
{
    /**
     * 主表ID
     */
    private int mId;

    /**
     * 附表ID
     */
    private int aId;

    public int getMId()
    {
        return mId;
    }

    public void setMId(int id)
    {
        mId = id;
    }

    public int getAId()
    {
        return aId;
    }

    public void setAId(int id)
    {
        aId = id;
    }
}
