package com.oswift.gpm.entity;

public class UserRRoleBean
{
    /**
     * 用户ID
     */
    private int userId;

    /**
     * 系统ID
     */
    private int systemId;

    /**
     * 角色ID
     */
    private int roleId;

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public int getSystemId()
    {
        return systemId;
    }

    public void setSystemId(int systemId)
    {
        this.systemId = systemId;
    }

    public int getRoleId()
    {
        return roleId;
    }

    public void setRoleId(int roleId)
    {
        this.roleId = roleId;
    }
}
