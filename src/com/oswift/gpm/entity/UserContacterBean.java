package com.oswift.gpm.entity;


public class UserContacterBean
{
    /**
     * 用户ID
     */
    protected int userId;

    /**
     * 真实姓名
     */
    protected String trueName;

    /**
     * 性别
     */
    protected String sex;

    /**
     * 职务
     */
    protected String position;

    /**
     * Email
     */
    protected String email;

    /**
     * QQ
     */
    protected String qq;

    /**
     * 办公电话
     */
    protected String officePhone;

    /**
     * 住宅电话
     */
    protected String homePhone;

    /**
     * 移动电话
     */
    protected String mobile;

    /**
     * 联系地址
     */
    protected String address;

    /**
     * 邮政编码
     */
    protected String zipCode;

    /**
     * 出生日期
     */
    protected String birthday;

    /**
     * 证件号码
     */
    protected String idCard;

    public int getUserId()
    {
        return userId;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public String getTrueName()
    {
        return trueName;
    }

    public void setTrueName(String trueName)
    {
        this.trueName = trueName;
    }

    public String getSex()
    {
        return sex;
    }

    public void setSex(String sex)
    {
        this.sex = sex;
    }

    public String getPosition()
    {
        return position;
    }

    public void setPosition(String position)
    {
        this.position = position;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getQq()
    {
        return qq;
    }

    public void setQq(String qq)
    {
        this.qq = qq;
    }

    public String getOfficePhone()
    {
        return officePhone;
    }

    public void setOfficePhone(String officePhone)
    {
        this.officePhone = officePhone;
    }

    public String getHomePhone()
    {
        return homePhone;
    }

    public void setHomePhone(String homePhone)
    {
        this.homePhone = homePhone;
    }

    public String getMobile()
    {
        return mobile;
    }

    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getZipCode()
    {
        return zipCode;
    }

    public void setZipCode(String zipCode)
    {
        this.zipCode = zipCode;
    }

    public String getBirthday()
    {
        return birthday;
    }

    public void setBirthday(String birthday)
    {
        this.birthday = birthday;
    }

    public String getIdCard()
    {
        return idCard;
    }

    public void setIdCard(String idCard)
    {
        this.idCard = idCard;
    }
}
