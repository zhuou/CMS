package com.oswift.gpm.entity;

import java.util.Date;

import com.oswift.utils.common.TimeUtil;

public class OpsInfoBean
{
    /**
     * 操作ID
     */
    private int opsId;

    /**
     * 系统ID
     */
    private int systemId;

    /**
     * 父节点ID
     */
    private int parentId;

    /**
     * 父节点名称
     */
    private String parentName;

    /**
     * 节点深度 默认为1
     */
    private int noteLevel;

    /**
     * 操作名称
     */
    private String opsName;

    /**
     * 操作类型，定义3种类型。1代表菜单类型 2代表页面类型 3代表按钮操作类型
     */
    private int opsType;

    /**
     * 操作URL
     */
    private String opsUrl;

    /**
     * icon图片路径
     */
    private String opsIcon;

    /**
     * 状态位 1为启用 0为停用
     */
    private int status;

    /**
     * 排序
     */
    private int sort;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 最后编辑时间
     */
    private String lastEditTime;

    /**
     * 最后编辑人
     */
    private String lastEditUser;

    /**
     * 操作描述
     */
    private String opsDesc;

    /**
     * 系统信息bean
     */
    private SystemModuleBean sysBean;

    /**
     * 是否被选中（此字段主要在用户权限设置时使用）
     */
    private boolean checked = false;

    public int getOpsId()
    {
        return opsId;
    }

    public void setOpsId(int opsId)
    {
        this.opsId = opsId;
    }

    public int getSystemId()
    {
        return systemId;
    }

    public void setSystemId(int systemId)
    {
        this.systemId = systemId;
    }

    public int getParentId()
    {
        return parentId;
    }

    public void setParentId(int parentId)
    {
        this.parentId = parentId;
    }

    public int getNoteLevel()
    {
        return noteLevel;
    }

    public void setNoteLevel(int noteLevel)
    {
        this.noteLevel = noteLevel;
    }

    public String getOpsName()
    {
        return opsName;
    }

    public void setOpsName(String opsName)
    {
        this.opsName = opsName;
    }

    public String getOpsUrl()
    {
        return opsUrl;
    }

    public void setOpsUrl(String opsUrl)
    {
        this.opsUrl = opsUrl;
    }

    public String getOpsIcon()
    {
        return opsIcon;
    }

    public void setOpsIcon(String opsIcon)
    {
        this.opsIcon = opsIcon;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public int getSort()
    {
        return sort;
    }

    public void setSort(int sort)
    {
        this.sort = sort;
    }

    public String getCreateTime()
    {
        return createTime;
    }

    public void setCreateTime(Date createTime)
    {
        this.createTime = TimeUtil.dateToString(createTime,
                TimeUtil.ISO_DATE_NOSECOND_FORMAT);
    }

    public String getCreateUser()
    {
        return createUser;
    }

    public void setCreateUser(String createUser)
    {
        this.createUser = createUser;
    }

    public String getLastEditTime()
    {
        return lastEditTime;
    }

    public void setLastEditTime(Date lastEditTime)
    {
        this.lastEditTime = TimeUtil.dateToString(lastEditTime,
                TimeUtil.ISO_DATE_NOSECOND_FORMAT);
    }

    public String getLastEditUser()
    {
        return lastEditUser;
    }

    public void setLastEditUser(String lastEditUser)
    {
        this.lastEditUser = lastEditUser;
    }

    public int getOpsType()
    {
        return opsType;
    }

    public void setOpsType(int opsType)
    {
        this.opsType = opsType;
    }

    public String getOpsDesc()
    {
        return opsDesc;
    }

    public void setOpsDesc(String opsDesc)
    {
        this.opsDesc = opsDesc;
    }

    public SystemModuleBean getSysBean()
    {
        return sysBean;
    }

    public void setSysBean(SystemModuleBean sysBean)
    {
        this.sysBean = sysBean;
    }

    public String getParentName()
    {
        return parentName;
    }

    public void setParentName(String parentName)
    {
        this.parentName = parentName;
    }

    public boolean isChecked()
    {
        return checked;
    }

    public void setChecked(boolean checked)
    {
        this.checked = checked;
    }
}
