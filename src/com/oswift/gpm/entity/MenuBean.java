package com.oswift.gpm.entity;

/**
 *
 * 菜单实体类
 *
 * @author zhuou
 * @version C03 2013-7-22
 * @since OSwift GPM V1.0
 */
public class MenuBean
{
    /**
     * 操作ID
     */
    private int opsId;

    /**
     * 父节点ID
     */
    private int parentId;

    /**
     * 父节点名称
     */
    private String parentName;

    /**
     * 节点深度 默认为1
     */
    private int noteLevel;

    /**
     * 操作名称
     */
    private String opsName;

    /**
     * 操作类型，定义3种类型。1代表菜单类型 2代表页面类型 3代表按钮操作类型
     */
    private int opsType;

    /**
     * 操作URL
     */
    private String opsUrl;

    /**
     * icon图片路径
     */
    private String opsIcon;

    /**
     * 排序
     */
    private int sort;

    /**
     * 操作描述
     */
    private String opsDesc;

    public int getOpsId()
    {
        return opsId;
    }

    public void setOpsId(int opsId)
    {
        this.opsId = opsId;
    }

    public int getParentId()
    {
        return parentId;
    }

    public void setParentId(int parentId)
    {
        this.parentId = parentId;
    }

    public String getParentName()
    {
        return parentName;
    }

    public void setParentName(String parentName)
    {
        this.parentName = parentName;
    }

    public int getNoteLevel()
    {
        return noteLevel;
    }

    public void setNoteLevel(int noteLevel)
    {
        this.noteLevel = noteLevel;
    }

    public String getOpsName()
    {
        return opsName;
    }

    public void setOpsName(String opsName)
    {
        this.opsName = opsName;
    }

    public int getOpsType()
    {
        return opsType;
    }

    public void setOpsType(int opsType)
    {
        this.opsType = opsType;
    }

    public String getOpsUrl()
    {
        return opsUrl;
    }

    public void setOpsUrl(String opsUrl)
    {
        this.opsUrl = opsUrl;
    }

    public String getOpsIcon()
    {
        return opsIcon;
    }

    public void setOpsIcon(String opsIcon)
    {
        this.opsIcon = opsIcon;
    }

    public int getSort()
    {
        return sort;
    }

    public void setSort(int sort)
    {
        this.sort = sort;
    }

    public String getOpsDesc()
    {
        return opsDesc;
    }

    public void setOpsDesc(String opsDesc)
    {
        this.opsDesc = opsDesc;
    }
}
