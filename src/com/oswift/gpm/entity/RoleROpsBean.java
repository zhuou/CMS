package com.oswift.gpm.entity;

public class RoleROpsBean
{
    /**
     * 角色ID
     */
    private int roleId;

    /**
     * 操作ID
     */
    private int opsId;

    public int getRoleId()
    {
        return roleId;
    }

    public void setRoleId(int roleId)
    {
        this.roleId = roleId;
    }

    public int getOpsId()
    {
        return opsId;
    }

    public void setOpsId(int opsId)
    {
        this.opsId = opsId;
    }
}
