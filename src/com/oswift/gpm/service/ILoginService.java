package com.oswift.gpm.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.oswift.gpm.entity.MenuBean;
import com.oswift.gpm.entity.MessageBean;
import com.oswift.gpm.entity.SystemModuleBean;
import com.oswift.gpm.entity.UserBean;
import com.oswift.utils.cache.CacheAccessException;
import com.oswift.utils.exception.PlatException;

public interface ILoginService
{
    /**
     *
     * 登录
     *
     * @author zhuou
     * @param userName
     *            用户名
     * @param password
     *            密码
     * @param lastLoginIP
     *            客户端IP
     * @return MessageBean
     * @throws PlatException
     *             公共异常
     */
    MessageBean login(String userName, String password, String lastLoginIP,
            HttpSession session) throws PlatException;

    /**
     *
     * 设置登录信息缓存
     *
     * @author zhuou
     * @param userBean
     *            用户信息
     * @param sysBean
     *            系统信息
     * @throws CacheAccessException
     *             缓存异常
     */
    void setCacheLoginBean(UserBean userBean, SystemModuleBean sysBean)
        throws CacheAccessException;

    /**
     *
     * 根据userId、systemId和节点深度获取菜单列表
     *
     * @author zhuou
     * @param userId
     *            用户Id
     * @param systemId
     *            系统Id
     * @param noteLevel
     *            节点深度 如1只查节点深度为1 如1,2 只查节点深度为1,2的节点；当为空或者null查询全部
     * @param parentId
     *            父节点ID
     *
     * @return List
     * @throws PlatException
     *             公共异常
     */
    List<MenuBean> getMenu(int userId, int systemId, int parentId,
            String noteLevel) throws PlatException;

    /**
     *
     * 根据userId、systemId和url获取操作，可能有多个
     *
     * @author zhuou
     * @param userId
     *            用户Id
     * @param systemId
     *            系统Id
     * @param opsUrl
     *            操作Url
     * @return List
     * @throws PlatException
     *             公共异常
     */
    List<MenuBean> getMenu(int userId, int systemId, String opsUrl)
        throws PlatException;

    /**
     *
     * 根据userId、systemId和opsId获取某个操作
     *
     * @author zhuou
     * @param userId
     *            用户Id
     * @param systemId
     *            系统Id
     * @param opsId
     *            操作Id
     * @return MenuBean
     * @throws PlatException
     *             公共异常
     */
    MenuBean getMenu(int userId, int systemId, int opsId) throws PlatException;

    /**
     *
     * 修改密码
     *
     * @author zhuou
     * @param userId
     *            后台用户id
     * @param pwd
     *            新密码
     * @return MessageBean
     */
    MessageBean updatePWD(int userId, String oldPwd, String newPwd)
        throws PlatException;
}
