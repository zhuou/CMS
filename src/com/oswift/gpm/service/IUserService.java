package com.oswift.gpm.service;

import java.util.List;
import java.util.Map;

import com.oswift.gpm.entity.PageBean;
import com.oswift.gpm.entity.SystemModuleBean;
import com.oswift.gpm.entity.UserBean;
import com.oswift.utils.exception.PlatException;

public interface IUserService
{
    /**
     *
     * 添加用户信息
     *
     * @author zhuou
     * @param bean
     *            用户bean
     * @return boolean
     * @throws PlatException
     *             公共平台异常
     */
    boolean addUser(UserBean bean, String sysId) throws PlatException;

    /**
     *
     * 获取有分页的用户列表
     *
     * @author zhuou
     * @param pageRecordNum
     * @param pageNum
     * @return PageBean
     * @throws PlatException
     *             平台公共异常
     */
    PageBean getUserList(int pageRecordNum, int pageNum) throws PlatException;

    /**
     *
     * 根据UserId查询用户详情
     *
     * @author zhuou
     * @param userId
     * @return UserBean
     * @throws PlatException
     *             公共平台异常
     */
    UserBean getUserDetail(int userId) throws PlatException;

    /**
     *
     * 根据userId查询和此用户关联的系统
     *
     * @author zhuou
     * @param userId
     *            用户Id
     * @return List<SystemModuleBean>
     * @throws PlatException
     *             公共异常
     */
    List<SystemModuleBean> getSysByUserId(String userId) throws PlatException;

    /**
     *
     * 修改用户
     *
     * @author zhuou
     * @param bean
     *            用户bean
     * @param sysId
     *            系统id字符串集合
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean updateUser(UserBean bean, String sysId) throws PlatException;

    /**
     *
     * 根据UserId删除用户
     *
     * @author zhuou
     * @param userId
     *            用户Id
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean delUser(String userId) throws PlatException;

    /**
     *
     * 根据userId修改用户状态
     *
     * @author zhuou
     * @param userId
     *            用户Id
     * @param status
     *            状态
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean updateUserStatus(int userId, int status) throws PlatException;

    /**
     *
     * 获取初始化权限设置页面数据
     *
     * @author zhuou
     * @param userId
     *            用户Id
     * @param systemId
     *            角色Id
     * @return Map
     * @throws PlatException
     *             公共异常
     */
    Map<String, Object> getSettingsRightData(int userId, int systemId)
        throws PlatException;

    /**
     *
     * 设置用户权限
     *
     * @author zhuou
     * @param userId
     *            用户Id
     * @param systemId
     *            系统Id
     * @param aRoleId
     *            角色id字符串集合
     * @param aGroupId
     *            组id字符串集合
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean settingRight(int userId, int systemId, String aRoleId,
            String aGroupId) throws PlatException;
}
