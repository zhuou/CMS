package com.oswift.gpm.service;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import com.oswift.gpm.entity.GroupBean;
import com.oswift.gpm.entity.OpsInfoBean;
import com.oswift.gpm.entity.RoleBean;
import com.oswift.utils.exception.PlatException;

public interface IGroupService
{
    /**
     *
     * 添加一条分组记录
     *
     * @author zhuou
     * @param group
     *            Group
     * @param roleIds
     *            角色ID字符串，以“,”分隔
     * @param opsIds
     *            操作Id字符串，以“,”分隔
     * @return boolean
     */
    boolean addGroup(GroupBean group, String roleIds, String opsIds)
        throws PlatException;

    /**
     *
     * 加载页面groupList.jsp所需要的页面数据
     *
     * @author zhuou
     * @param pageName
     *            groupList.jsp
     * @return ModelAndView
     * @throws PlatException
     *             平台公共异常
     */
    ModelAndView loadGroupList(String pageName) throws PlatException;

    /**
     *
     * 加载Group新增页面，获取页面需要的数据
     *
     * @author zhuou
     * @param sysId
     *            系统Id
     * @return ModelAndView
     * @throws PlatException
     *             平台公共异常
     */
    ModelAndView loadGroup(String sysId, String groupId) throws PlatException;

    /**
     *
     * 根据sysId获取组/部门的信息
     *
     * @author zhuou
     * @param sysId
     *            系统ID
     * @return List
     * @throws PlatException
     *             公共异常
     */
    List<GroupBean> getGroupList(String sysId) throws PlatException;

    /**
     *
     * 根据groupId查询组详情
     *
     * @author zhuou
     * @param groupId
     *            组id
     * @return GroupBean
     */
    GroupBean getDetailById(String groupId) throws PlatException;

    /**
     *
     * 根据组ID获取组拥有的角色
     *
     * @author zhuou
     * @param groupId
     *            组id
     * @return List<RoleBean>
     */
    List<RoleBean> getRolesByGroupId(String groupId) throws PlatException;

    /**
     *
     * 根据组ID获取组拥有的操作权限
     *
     * @author zhuou
     * @param groupId
     *            组ID
     * @return List<OpsInfoBean>
     */
    List<OpsInfoBean> getOpsByGroupId(String groupId) throws PlatException;

    /**
     *
     * 修改组记录
     *
     * @author zhuou
     * @param group
     *            Group
     * @param roleIds
     *            角色ID字符串，以“,”分隔
     * @param opsIds
     *            操作Id字符串，以“,”分隔
     * @return boolean
     */
    boolean updateGroup(GroupBean group, String roleIds, String opsIds)
        throws PlatException;

    /**
     *
     * 删除组/部门信息
     *
     * @author zhuou
     * @param groupId
     *            组ID
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean delGroup(String groupId) throws PlatException;
}
