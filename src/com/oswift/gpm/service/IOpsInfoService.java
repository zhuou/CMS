package com.oswift.gpm.service;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import com.oswift.gpm.entity.OpsInfoBean;
import com.oswift.utils.exception.PlatException;

public interface IOpsInfoService
{
    /**
     *
     * 加载页面opsInfoList.jsp所需要的页面数据
     *
     * @author zhuou
     * @param pageName
     *            opsInfoList.jsp
     * @return ModelAndView
     * @throws PlatException
     *             平台公共异常
     */
    ModelAndView loadOpsInfo(String pageName) throws PlatException;

    /**
     *
     * 添加操作信息
     *
     * @author zhuou
     * @param bean
     *            操作信息bean
     * @param parentId
     *            父节点ID
     * @return boolean
     * @throws PlatException
     *             平台公共异常
     */
    boolean add(OpsInfoBean bean, int parentId) throws PlatException;

    /**
     *
     * 获取操作列表信息
     *
     * @author zhuou
     * @return list
     */
    List<OpsInfoBean> getOpsList(String systemId, String groupId, String roleId)
        throws PlatException;

    /**
     *
     * 根据ID获取操作详情
     *
     * @author zhuou
     * @param opsId
     * @return OpsInfoBean
     */
    OpsInfoBean getOpsInfo(String opsId) throws PlatException;

    /**
     *
     * 修改操作
     *
     * @author zhuou
     * @param opsInfo
     *            操作Bean
     * @return boolean
     * @throws PlatException
     *             平台公共异常
     */
    boolean updateOpsInfo(OpsInfoBean opsInfo) throws PlatException;

    /**
     *
     * 删除一条操作信息
     *
     * @author zhuou
     * @param opsId
     *            操作Id
     * @return boolean
     * @throws PlatException
     *             平台公共异常
     */
    boolean delOpsInfo(String opsId) throws PlatException;
}
