package com.oswift.gpm.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.gpm.dao.GroupDao;
import com.oswift.gpm.dao.OpsInfoDao;
import com.oswift.gpm.dao.RoleDao;
import com.oswift.gpm.dao.SystemDao;
import com.oswift.gpm.entity.GroupROpsBean;
import com.oswift.gpm.entity.OpsInfoBean;
import com.oswift.gpm.entity.RoleROpsBean;
import com.oswift.gpm.entity.SystemModuleBean;
import com.oswift.gpm.service.IOpsInfoService;
import com.oswift.gpm.utils.Constant;
import com.oswift.gpm.utils.PageCode;
import com.oswift.utils.cache.ResourceManager;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

/**
 *
 * 权限管理操作的业务处理类
 *
 * @author zhuou
 * @version C03 2013-6-16
 * @since OSwift GPM V1.0
 */
public class OpsInfoServiceImpl implements IOpsInfoService
{
    /**
     * SystemDao spring注入实例
     */
    private SystemDao systemDao;

    /**
     * OpsInfoDao spring注入实例
     */
    private OpsInfoDao opsInfoDao;

    /**
     * GroupDao spring注入实例
     */
    private GroupDao groupDao;

    /**
     * RoleDaoo spring注入实例
     */
    private RoleDao roleDao;

    /**
     *
     * 加载页面opsInfoList.jsp所需要的页面数据
     *
     * @author zhuou
     * @param pageName
     *            opsInfoList.jsp
     * @return ModelAndView
     * @throws PlatException
     *             平台公共异常
     */
    public ModelAndView loadOpsInfo(String pageName) throws PlatException
    {
        ModelAndView mv = null;
        try
        {
            List<SystemModuleBean> list = systemDao.getSysList();

            mv = new ModelAndView(pageName);
            mv.addObject("sysList", list);
        }
        catch (Exception e)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
        return mv;
    }

    /**
     *
     * 添加操作信息
     *
     * @author zhuou
     * @param bean
     *            操作信息bean
     * @param parentId
     *            父节点ID
     * @return boolean
     * @throws PlatException
     *             平台公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean add(OpsInfoBean bean, int parentId) throws PlatException
    {
        boolean isSuccess = false;
        try
        {
            // 获取操作ID
            int opsId = 0;
            OpsInfoBean top1Bean = opsInfoDao.getTop1Ops(bean.getSystemId());
            if (null == top1Bean)
            {
                opsId = Integer.valueOf(String.valueOf(bean.getSystemId())
                        + Constant.DEFAULT_OPSID);
            }
            else
            {
                opsId = top1Bean.getOpsId() + 1;
            }
            bean.setOpsId(opsId);

            if (Constant.PARENT_ID == parentId)
            {
                bean.setNoteLevel(Constant.DEFAULT_LEVEL);
                bean.setParentId(Constant.PARENT_ID);

                int rows = opsInfoDao.add(bean);
                if (rows <= 0)
                {
                    // 抛出异常回滚
                    throw new PlatException(PageCode.ADD_OPSINFO_FAIL);
                }
                isSuccess = true;
            }
            else
            {
                // 检查父节点是否被删除
                OpsInfoBean parentBean = opsInfoDao.selectOne(parentId);
                if (null == parentBean)
                {
                    // 抛出异常回滚
                    throw new PlatException(PageCode.GET_PARENT_OPSINFO_FAIL);
                }

                // 获取层深，并计算当前的层深
                bean.setNoteLevel(parentBean.getNoteLevel() + 1);

                int rows = opsInfoDao.add(bean);
                if (rows <= 0)
                {
                    // 抛出异常回滚
                    throw new PlatException(PageCode.ADD_OPSINFO_FAIL);
                }

                isSuccess = true;
            }
        }
        catch (Exception e)
        {
            throw new PlatException(PageCode.ADD_OPSINFO_FAIL, e);
        }
        return isSuccess;
    }

    /**
     *
     * 获取操作列表信息
     *
     * @author zhuou
     * @return list
     */
    public List<OpsInfoBean> getOpsList(String systemId, String groupId,
            String roleId) throws PlatException
    {
        List<OpsInfoBean> opsInfoBean = opsInfoDao.selectList(systemId);

        if (!StringUtil.isEmpty(groupId))
        {
            List<GroupROpsBean> groupROpsBean = groupDao.getGroupROps(groupId);

            if (null != groupROpsBean && !groupROpsBean.isEmpty()
                    && null != opsInfoBean && !opsInfoBean.isEmpty())
            {
                for (OpsInfoBean item1 : opsInfoBean)
                {
                    for (GroupROpsBean item2 : groupROpsBean)
                    {
                        if (item1.getOpsId() == item2.getOpsId())
                        {
                            item1.setChecked(true);
                            break;
                        }
                    }
                }
            }
        }

        if (!StringUtil.isEmpty(roleId))
        {
            List<RoleROpsBean> roleROpsBean = roleDao
                    .getRoleROpsByRoleId(roleId);
            if (null != opsInfoBean && !opsInfoBean.isEmpty()
                    && null != roleROpsBean && !roleROpsBean.isEmpty())
            {
                for (OpsInfoBean item1 : opsInfoBean)
                {
                    for (RoleROpsBean item2 : roleROpsBean)
                    {
                        if (item1.getOpsId() == item2.getOpsId())
                        {
                            item1.setChecked(true);
                        }
                    }
                }
            }
        }

        return opsInfoBean;
    }

    /**
     *
     * 根据ID获取操作详情
     *
     * @author zhuou
     * @param opsId
     * @return OpsInfoBean
     */
    public OpsInfoBean getOpsInfo(String opsId) throws PlatException
    {
        OpsInfoBean bean = opsInfoDao.selectOne(opsId);
        if (bean.getParentId() != Constant.PARENT_ID)
        {
            OpsInfoBean parentBean = opsInfoDao.selectOne(bean.getParentId());
            if (null == parentBean)
            {
                bean.setParentName(ResourceManager
                        .getValue(PageCode.GET_PARENT_NAME_FAIL));
            }
            else
            {
                bean.setParentName(parentBean.getOpsName());
            }
        }
        else
        {
            bean.setParentName(ResourceManager
                    .getValue(PageCode.NO_PARENT_NAME));
        }

        return bean;
    }

    /**
     *
     * 修改操作
     *
     * @author zhuou
     * @param opsInfo
     *            操作Bean
     * @return boolean
     * @throws PlatException
     *             平台公共异常
     */
    public boolean updateOpsInfo(OpsInfoBean opsInfo) throws PlatException
    {
        boolean isSuccess = false;

        OpsInfoBean bean = opsInfoDao.selectOne(opsInfo.getOpsId());
        if (null == bean)
        {
            throw new PlatException(PageCode.OPSINFO_DELETED, ResourceManager
                    .getValue(PageCode.OPSINFO_DELETED));
        }

        isSuccess = opsInfoDao.update(opsInfo) > 0 ? true : false;

        return isSuccess;
    }

    /**
     *
     * 删除一条操作信息
     *
     * @author zhuou
     * @param opsId
     *            操作Id
     * @return boolean
     * @throws PlatException
     *             平台公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean delOpsInfo(String opsId) throws PlatException
    {
        boolean isSuccess = false;

        OpsInfoBean bean = opsInfoDao.selectOne(opsId);
        if (null == bean)
        {
            throw new PlatException(PageCode.OPSINFO_DELETED, ResourceManager
                    .getValue(PageCode.OPSINFO_DELETED));
        }

        // 根据操作ID删除操作和组关联数据
        opsInfoDao.delOpsRGroupByOpsId(opsId);

        // 根据操作ID删除操作和角色关联数据
        opsInfoDao.delOpsRRoleByOpsId(opsId);

        // 根据操作ID删除操作和系统关联数据
        opsInfoDao.delOpsRSysByOpsId(opsId);

        // 修改所有的子节点信息
        OpsInfoBean childBean = new OpsInfoBean();
        childBean.setOpsId(bean.getOpsId());
        childBean.setParentId(bean.getParentId());
        opsInfoDao.updateOpsChild(childBean);

        isSuccess = opsInfoDao.delete(opsId) > 0 ? true : false;

        return isSuccess;
    }

    public void setSystemDao(SystemDao systemDao)
    {
        this.systemDao = systemDao;
    }

    public void setOpsInfoDao(OpsInfoDao opsInfoDao)
    {
        this.opsInfoDao = opsInfoDao;
    }

    public void setGroupDao(GroupDao groupDao)
    {
        this.groupDao = groupDao;
    }

    public void setRoleDao(RoleDao roleDao)
    {
        this.roleDao = roleDao;
    }
}
