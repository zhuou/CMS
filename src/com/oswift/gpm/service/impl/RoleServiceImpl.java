package com.oswift.gpm.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.gpm.dao.RoleDao;
import com.oswift.gpm.dao.SystemDao;
import com.oswift.gpm.entity.OpsInfoBean;
import com.oswift.gpm.entity.PageBean;
import com.oswift.gpm.entity.RelationBean;
import com.oswift.gpm.entity.RoleBean;
import com.oswift.gpm.entity.SystemModuleBean;
import com.oswift.gpm.service.IRoleService;
import com.oswift.gpm.utils.PageCode;
import com.oswift.utils.cache.ResourceManager;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

/**
 *
 * 权限管理角色的业务处理类
 *
 * @author zhuou
 * @version C03 2013-6-16
 * @since OSwift GPM V1.0
 */
public class RoleServiceImpl implements IRoleService
{
    /**
     * SystemDao spring注入实例
     */
    private SystemDao systemDao;

    /**
     * RoleDao spring注入实例
     */
    private RoleDao roleDao;

    /**
     *
     * 获取有分页的角色列表
     *
     * @author zhuou
     * @param pageRecordNum
     * @param pageNum
     * @return PageBean
     * @throws PlatException
     *             平台公共异常
     */
    public PageBean getRoleList(int pageRecordNum, int pageNum)
        throws PlatException
    {
        int rowNum = (pageNum - 1) * pageRecordNum;

        Map<String, Object> param = new HashMap<String, Object>();
        param.put("rowNum", rowNum);
        param.put("pageNum", pageRecordNum);

        try
        {
            List<RoleBean> list = roleDao.selectList(param);
            int totalRecordNum = roleDao.selectCount(null);
            PageBean bean = new PageBean();
            bean.setTotalRecordNum(totalRecordNum);
            bean.setDataList(list);
            return bean;
        }
        catch (Exception e)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
    }

    /**
     *
     * 加载页面roleList.jsp所需要的页面数据
     *
     * @author zhuou
     * @param pageName
     *            页面名称
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    public ModelAndView loadRole(String pageName) throws PlatException
    {
        ModelAndView mv = null;
        try
        {
            List<SystemModuleBean> list = systemDao.getSysList();

            mv = new ModelAndView(pageName);
            mv.addObject("sysList", list);
        }
        catch (Exception e)
        {
            throw new PlatException(ErrorCode.COMMON_DB_ERROR, e);
        }
        return mv;
    }

    /**
     *
     * 新增角色信息
     *
     * @author zhuou
     * @param roleBean
     *            角色bean
     * @param opsId
     *            操作id字符集，以","分隔
     * @return 是否成功
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean add(RoleBean roleBean, String opsId) throws PlatException
    {
        if (StringUtil.isEmpty(opsId))
        {
            throw new PlatException(PageCode.GET_OPSID_FAIL);
        }

        int rows = roleDao.add(roleBean);
        if (rows <= 0)
        {
            return false;
        }

        String[] ids = opsId.split(",");
        List<RelationBean> list = new ArrayList<RelationBean>();
        for (String id : ids)
        {
            RelationBean rBean = new RelationBean();
            rBean.setMId(roleBean.getRoleId());
            rBean.setAId(Integer.valueOf(id));
            list.add(rBean);
        }
        rows = roleDao.addROpsRole(list);
        if (rows <= 0)
        {
            throw new PlatException(PageCode.ADD_R_OPSROLE_FAIL);
        }
        return true;
    }

    /**
     *
     * 根据ID获取角色详情
     *
     * @author zhuou
     * @param id
     *            角色ID
     * @return RoleBean
     * @throws PlatException
     *             公共异常
     */
    public RoleBean getRoleDetail(String id) throws PlatException
    {
        try
        {
            RoleBean bean = roleDao.selectOne(id);
            if (null == bean)
            {
                throw new PlatException(PageCode.ROLEINFO_NO_EXIST);
            }
            return bean;
        }
        catch (Exception e)
        {
            throw new PlatException(PageCode.GET_ROLEINFO_FAIL, e);
        }
    }

    /**
     *
     * 根据角色ID查询角色下拥有的操作
     *
     * @author zhuou
     * @param roleId
     *            角色Id
     * @return List<OpsInfoBean>
     */
    public List<OpsInfoBean> getOpsByRoleId(String roleId) throws PlatException
    {
        try
        {
            List<OpsInfoBean> beans = roleDao.getOpsByRoleId(roleId);
            return beans;
        }
        catch (Exception e)
        {
            throw new PlatException(PageCode.GET_ROLEINFO_FAIL, e);
        }
    }

    /**
     *
     * 修改角色
     *
     * @author zhuo
     * @param roleBean
     *            角色bean
     * @param opsId
     *            操作字符串
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    public boolean updateRole(RoleBean roleBean, String opsId)
        throws PlatException
    {
        if (StringUtil.isEmpty(opsId))
        {
            throw new PlatException(PageCode.GET_OPSID_FAIL);
        }

        // 删除操作和角色关联表数据
        int rows = roleDao.delOpsRRoleByRoleId(String.valueOf(roleBean
                .getRoleId()));
        if (rows <= 0)
        {
            return false;
        }

        rows = roleDao.update(roleBean);
        if (rows <= 0)
        {
            throw new PlatException(PageCode.UPDATE_ROLE_FAIL);
        }

        String[] ids = opsId.split(",");
        List<RelationBean> list = new ArrayList<RelationBean>();
        for (String id : ids)
        {
            RelationBean rBean = new RelationBean();
            rBean.setMId(roleBean.getRoleId());
            rBean.setAId(Integer.valueOf(id));
            list.add(rBean);
        }
        rows = roleDao.addROpsRole(list);
        if (rows <= 0)
        {
            throw new PlatException(PageCode.UPDATE_ROLE_FAIL);
        }

        return true;
    }

    /**
     *
     * 根据角色ID删除一条角色信息
     *
     * @author zhuou
     * @param roleId
     *            角色ID
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    @Transactional(rollbackFor = PlatException.class)
    public boolean delRole(String roleId) throws PlatException
    {
        RoleBean bean = roleDao.selectOne(roleId);
        if (null == bean)
        {
            throw new PlatException(PageCode.OPSINFO_DELETED, ResourceManager
                    .getValue(PageCode.OPSINFO_DELETED));
        }

        // 根据角色ID删除操作和角色关联表数据
        roleDao.delOpsRRoleByRoleId(roleId);

        // 根据角色ID删除组和角色关联表数据
        roleDao.delRoleRGroupByRoleId(roleId);

        // 根据角色ID删除用户和角色关联表数据
        roleDao.delUserRRoleByRoleId(roleId);

        boolean isSuccess = roleDao.delete(roleId) > 0 ? true : false;

        return isSuccess;
    }

    public RoleDao getRoleDao()
    {
        return roleDao;
    }

    public void setRoleDao(RoleDao roleDao)
    {
        this.roleDao = roleDao;
    }

    public SystemDao getSystemDao()
    {
        return systemDao;
    }

    public void setSystemDao(SystemDao systemDao)
    {
        this.systemDao = systemDao;
    }
}
