package com.oswift.gpm.service;

import java.util.List;

import com.oswift.gpm.entity.MessageBean;
import com.oswift.gpm.entity.PageBean;
import com.oswift.gpm.entity.SystemModuleBean;
import com.oswift.gpm.entity.UserBean;
import com.oswift.utils.exception.PlatException;

public interface ISystemModuleService
{
    /**
     *
     * 添加系统信息，如果有userId，则同时插入t_gpm_r_usersystem表
     * 将t_gpm_systemmodule和t_gpm_user关联
     *
     * @author zhuou
     * @param bean
     * @param userId
     * @return boolean
     * @throws PlatException
     *             GPM公共异常
     */
    boolean addSystem(SystemModuleBean bean, List<String> userId)
        throws PlatException;

    /**
     *
     * 获取有分页的系统列表
     *
     * @author zhuou
     * @param pageRecordNum
     * @param pageNum
     * @return PageBean
     * @throws PlatException
     *             平台公共异常
     */
    PageBean getSysList(int pageRecordNum, int pageNum) throws PlatException;

    /**
     *
     * 根据systemId获取系统详情
     *
     * @author zhuou
     * @param systemId
     * @return SystemModuleBean
     * @throws PlatException
     *             平台公共异常
     */
    SystemModuleBean getSysDetail(int systemId) throws PlatException;

    /**
     *
     * 根据sysId查询系统下拥有的所有用户
     *
     * @author zhuou
     * @param systemId
     *            系统ID
     * @return List<UserBean>
     */
    List<UserBean> getUsersBySysId(String systemId) throws PlatException;

    /**
     *
     * 修改sys信息
     *
     * @author zhuou
     * @param bean
     *            SystemModuleBean
     * @param userId
     *            用户id字符串
     * @return boolean
     * @throws PlatException
     *             GPM公共异常
     */
    public boolean updateSystem(SystemModuleBean bean, List<String> userId)
        throws PlatException;

    /**
     *
     * 根据sysId删除系统信息
     *
     * @author zhuou
     * @param systemId
     *            系统Id
     * @return MessageBean
     * @throws PlatException
     *             GPM公共异常
     */
    MessageBean delSys(String systemId) throws PlatException;
}
