package com.oswift.gpm.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.gpm.entity.OpsInfoBean;
import com.oswift.gpm.entity.PageBean;
import com.oswift.gpm.entity.RoleBean;
import com.oswift.utils.exception.PlatException;

public interface IRoleService
{
    /**
     *
     * 获取有分页的角色列表
     *
     * @author zhuou
     * @param pageRecordNum
     * @param pageNum
     * @return PageBean
     * @throws PlatException
     *             平台公共异常
     */
    PageBean getRoleList(int pageRecordNum, int pageNum) throws PlatException;

    /**
     *
     * 加载页面roleList.jsp所需要的页面数据
     *
     * @author zhuou
     * @param pageName
     *            页面名称
     * @return ModelAndView
     * @throws PlatException
     *             公共异常
     */
    ModelAndView loadRole(String pageName) throws PlatException;

    /**
     *
     * 新增角色信息
     *
     * @author zhuou
     * @param roleBean
     *            角色bean
     * @param opsId
     *            操作id字符集，以","分隔
     * @return 是否成功
     */
    @Transactional(rollbackFor = PlatException.class)
    boolean add(RoleBean roleBean, String opsId) throws PlatException;

    /**
     *
     * 根据ID获取角色详情
     *
     * @author zhuou
     * @param id
     *            角色ID
     * @return RoleBean
     * @throws PlatException
     *             公共异常
     */
    RoleBean getRoleDetail(String id) throws PlatException;

    /**
     *
     * 根据角色ID查询角色下拥有的操作
     *
     * @author zhuou
     * @param roleId
     *            角色Id
     * @return List<OpsInfoBean>
     */
    List<OpsInfoBean> getOpsByRoleId(String roleId) throws PlatException;

    /**
     *
     * 修改角色
     *
     * @author zhuo
     * @param roleBean
     *            角色bean
     * @param opsId
     *            操作字符串
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean updateRole(RoleBean roleBean, String opsId) throws PlatException;

    /**
     *
     * 根据角色ID删除一条角色信息
     *
     * @author zhuou
     * @param roleId
     *            角色ID
     * @return boolean
     * @throws PlatException
     *             公共异常
     */
    boolean delRole(String roleId) throws PlatException;
}
