package com.oswift.gpm.system.cache;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oswift.gpm.entity.LoginBean;
import com.oswift.utils.cache.BusinessCache;
import com.oswift.utils.cache.CacheAccessException;
import com.oswift.utils.cache.manager.CacheManager;
import com.oswift.utils.common.StringUtil;

public class LoginManager
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(LoginManager.class);

    /**
     * 记录系统参数缓存实例
     */
    private static BusinessCache loginCacheInstance = CacheManager
            .getBusinessCache(GPMCacheFileds.CacheName.LOGINCACHE);

    /**
     *
     * 根据key获取登录bean信息
     *
     * @author zhuou
     * @param key
     *            key值
     * @return LoginBean
     */
    public static LoginBean getLoginBean(String key)
    {
        LoginBean bane = null;
        if (StringUtil.isEmpty(key))
        {
            return bane;
        }
        try
        {
            bane = (LoginBean) loginCacheInstance.get(key);
        }
        catch (CacheAccessException e)
        {
            log.error("Fail to get Login bean, key=" + key);
        }
        return bane;
    }

    /**
     *
     * 设置登录LoginBean
     *
     * @author zhuou
     * @param bean
     *            登录bean信息
     * @throws CacheAccessException
     *             缓存异常
     */
    public static void setLoginBean(LoginBean bean) throws CacheAccessException
    {
        if (null == bean)
        {
            throw new CacheAccessException("LoginBean is null.");
        }

        loginCacheInstance.set(String.valueOf(bean.getHashId()), bean);
    }

    /**
     *
     * 根据userName查询缓存中是否存在此账户，如果有返回true，没有返回false
     *
     * @author zhuou
     * @param userName
     *            用户名
     * @return boolean
     * @throws CacheAccessException
     *             缓存异常
     */
    @SuppressWarnings("unchecked")
    public static boolean isUserExist(String userName)
        throws CacheAccessException
    {
        if (StringUtil.isEmpty(userName))
        {
            return false;
        }

        Map<String, LoginBean> list = loginCacheInstance.getAllCache();
        if (null == list || list.isEmpty())
        {
            return false;
        }

        for (Map.Entry<String, LoginBean> en : list.entrySet())
        {
            LoginBean valueBean = en.getValue();
            if (userName.equals(valueBean.getUserName()))
            {
                return true;
            }
        }

        return false;
    }

    /**
     *
     * 删除缓存中一条记录
     *
     * @author zhuou
     * @param key
     *            key值
     * @throws CacheAccessException
     *             缓存异常
     */
    public static void remove(String key) throws CacheAccessException
    {
        if (StringUtil.isEmpty(key))
        {
            return;
        }
        loginCacheInstance.remove(key);
    }

    /**
     *
     * 获取所有缓存的登录用户bean
     *
     * @author zhuou
     * @return BusinessCache
     */
    public static BusinessCache getLoginCacheInstance()
    {
        return loginCacheInstance;
    }
}
