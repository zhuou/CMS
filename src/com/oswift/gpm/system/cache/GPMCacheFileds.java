package com.oswift.gpm.system.cache;

/**
 *
 * 定义缓存常量名称
 *
 * @author zhuou
 * @version C03 Oct 27, 2012
 * @since OSwift GPM V1.0
 */
public interface GPMCacheFileds
{

    /**
     *
     * 缓存名称常量
     *
     * @author zhuou
     * @version C03 Oct 27, 2012
     * @since OSwift GPM V1.0
     */
    interface CacheName
    {
        /**
         * 缓存用户登录信息
         */
        String LOGINCACHE = "loginCache";

        /**
         * 缓存URL白名单
         */
        String WHITELISTCACHE = "whiteListCache";
    }

    /**
     *
     * 缓存对应业务变量常量名称
     *
     * @author zhuou
     * @version C03 2013-3-16
     * @since OSwift GPM V1.0
     */
    interface BusinessField
    {

    }
}
