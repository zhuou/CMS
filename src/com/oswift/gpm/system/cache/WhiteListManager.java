package com.oswift.gpm.system.cache;

import com.oswift.utils.cache.BusinessCache;
import com.oswift.utils.cache.CacheAccessException;
import com.oswift.utils.cache.manager.CacheManager;
import com.oswift.utils.common.StringUtil;

public class WhiteListManager
{
    /**
     * 记录系统参数缓存实例
     */
    private static BusinessCache whiteListCacheInstance = CacheManager
            .getBusinessCache(GPMCacheFileds.CacheName.WHITELISTCACHE);

    /**
     *
     * 判断是否在白名单中
     *
     * @author zhuou
     * @param key
     *            requestURI
     */
    public static boolean isExist(String key) throws CacheAccessException
    {
        if (StringUtil.isEmpty(key))
        {
            return false;
        }
        return whiteListCacheInstance.containsKey(key);
    }
}
