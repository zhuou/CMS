package com.oswift.gpm.system.cache.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oswift.gpm.dao.WhiteListDao;
import com.oswift.gpm.entity.WhiteListBean;
import com.oswift.gpm.system.cache.GPMCacheFileds;
import com.oswift.utils.cache.AbstractBusinessCache;
import com.oswift.utils.cache.CacheAccessException;
import com.oswift.utils.cache.operate.CacheMap;
import com.oswift.utils.exception.PlatException;

public class WhiteListCacheImpl extends AbstractBusinessCache
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(WhiteListCacheImpl.class);

    /**
     * spring注入
     */
    private WhiteListDao whiteListDao;

    /**
     * 初始化 {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public void init() throws CacheAccessException
    {
        try
        {
            List<WhiteListBean> list = whiteListDao.selectList(null);
            CacheMap cache = this.getCacheMap();

            // 先清除缓存
            cache.clear();

            if (null != list && !list.isEmpty())
            {
                for (WhiteListBean bean : list)
                {
                    cache.put(bean.getRequestURI().trim(), bean);
                }
            }
        }
        catch (PlatException e)
        {
            log.error("Failure data cache dictionary.", e);
        }

        log.info("Cache ->> " + GPMCacheFileds.CacheName.WHITELISTCACHE
                + " had Loaded.");
    }

    public WhiteListDao getWhiteListDao()
    {
        return whiteListDao;
    }

    public void setWhiteListDao(WhiteListDao whiteListDao)
    {
        this.whiteListDao = whiteListDao;
    }
}
