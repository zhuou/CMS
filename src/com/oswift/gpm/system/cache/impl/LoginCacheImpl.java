package com.oswift.gpm.system.cache.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oswift.gpm.entity.LoginBean;
import com.oswift.gpm.system.cache.GPMCacheFileds;
import com.oswift.utils.cache.AbstractBusinessCache;
import com.oswift.utils.cache.CacheAccessException;

public class LoginCacheImpl extends AbstractBusinessCache
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(LoginCacheImpl.class);

    /**
     * 初始化 {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public void init() throws CacheAccessException
    {
        // 获取缓存内容
        Map<String, LoginBean> map = getCacheMap().getAll();

        // 为空，则声明缓存空间；初始化静态变量对应值。
        if (null == map)
        {
            map = new HashMap<String, LoginBean>();
        }

        getCacheMap().putAll(map);

        log.info("Cache ->> " + GPMCacheFileds.CacheName.LOGINCACHE
                + " had Loaded.");
    }
}
