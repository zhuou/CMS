package com.oswift.gpm.system.listener;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.oswift.gpm.system.cache.LoginManager;
import com.oswift.gpm.utils.Constant;

public class SessionListener implements HttpSessionListener
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(SessionListener.class);

    /**
     * Session创建事件
     */
    @Override
    public void sessionCreated(HttpSessionEvent arg)
    {
        // 无
    }

    /**
     * Session失效事件
     */
    @Override
    public void sessionDestroyed(HttpSessionEvent arg)
    {
        try
        {
            HttpSession session = arg.getSession();
            if (null != session)
            {
                String hashId = String.valueOf(session
                        .getAttribute(Constant.SESSION_KEY));
                if (null != hashId && !"".equals(hashId))
                {
                    LoginManager.remove(hashId);
                    log.info("Remove sessionCache Success.hashId=" + hashId);
                }
            }
        }
        catch (Exception e)
        {
            log.error("Fail to Operation SessionCache. Message="
                    + e.getMessage());
        }
    }
}
