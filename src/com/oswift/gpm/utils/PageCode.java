package com.oswift.gpm.utils;

public interface PageCode
{
    /**
     * 成功
     */
    public static final String COMMON_SUCCESS = "200";

    /**
     * 错误
     */
    public static final String COMMON_ERROR = "500";

    /** **********6000-6020定义页面公共错误提示信息************* */
    /**
     * 获取web页面参数失败
     */
    public static final String GET_WEBPARAM_FAIL = "6000";

    /**
     * 必填项不能为空，请填写
     */
    public static final String TEXTFIELD_ISNOT_EMPTY = "6001";

    /** **********页面错误提示信息************* */
    /**
     * 系统信息添加成功
     */
    public static final String SYS_ADD_SUCCESS = "7001";
    /**
     * 系统信息添加成功
     */
    public static final String SYS_ADD_FAIL = "7002";
    /**
     * 用户添加成功
     */
    public static final String USER_ADD_SUCCESS = "7003";
    /**
     * 用户添加失败
     */
    public static final String USER_ADD_FAIL = "7004";

    /**
     * 获取父节点信息错误
     */
    public static final String GET_PARENT_OPSINFO_FAIL = "7005";

    /**
     * 更新父节点的ArrChildId值失败
     */
    public static final String UPDATE_ARRCHILDID_FAIL = "7006";

    /**
     * 添加操作信息失败
     */
    public static final String ADD_OPSINFO_FAIL = "7007";

    /**
     * 添加操作信息成功
     */
    public static final String ADD_OPSINFO_SUCCESS = "7008";

    /**
     * 获取操作列表错误
     */
    public static final String GET_OPSINFO_LIST_FAIL = "7009";

    /**
     * 没有获取父节点名称
     */
    public static final String GET_PARENT_NAME_FAIL = "7010";

    /**
     * 无父节点
     */
    public static final String NO_PARENT_NAME = "7011";

    /**
     * 操作信息已经被删除，请刷新页面
     */
    public static final String OPSINFO_DELETED = "7012";

    /**
     * 新增角色失败
     */
    public static final String ADD_ROLE_FAIL = "7013";

    /**
     * 获取权限ID失败
     */
    public static final String GET_OPSID_FAIL = "7014";

    /**
     * 新增操作和角色关系表失败
     */
    public static final String ADD_R_OPSROLE_FAIL = "7015";

    /**
     * 获取系统Id失败
     */
    public static final String GET_SYSINFO_FAIL = "7016";

    /**
     * 新增角色成功
     */
    public static final String ADD_ROLE_SUCCESS = "7017";

    /**
     * 获取角色详情失败
     */
    public static final String GET_ROLEINFO_FAIL = "7018";

    /**
     * 角色信息不存在，可能已经被删除
     */
    public static final String ROLEINFO_NO_EXIST = "7019";

    /**
     * 请设置权限，角色权限和操作权限必须设置一项
     */
    public static final String POWER_EMPTY = "7020";

    /**
     * 新增组/部门信息成功
     */
    public static final String ADD_GROUP_SUCCESS = "7021";

    /**
     * 新增组/部门信息失败，请重试
     */
    public static final String ADD_GROUP_FAIL = "7022";

    /**
     * 获取组/部门详情信息失败
     */
    public static final String GET_GROUPDETAIL_FAIL = "7023";

    /**
     * 修改系统信息成功
     */
    public static final String SYS_UPDATE_SUCCESS = "7024";

    /**
     * 修改系统信息失败
     */
    public static final String SYS_UPDATE_FAIL = "7025";

    /**
     * 系统下存在用户信息，系统信息不可以被删除
     */
    public static final String SYS_R_USER_EXIST = "7026";

    /**
     * 系统下存在角色信息，系统信息不可以被删除
     */
    public static final String SYS_R_ROLE_EXIST = "7027";

    /**
     * 系统下存在组/部门信息，系统信息不可以被删除
     */
    public static final String SYS_R_GROUP_EXIST = "7028";

    /**
     * 系统下存在操作信息，系统信息不可以被删除
     */
    public static final String SYS_R_OPS_EXIST = "7029";

    /**
     * 系统信息删除成功
     */
    public static final String SYS_DEL_SUCCESS = "7030";

    /**
     * 系统信息删除失败
     */
    public static final String SYS_DEL_FAIL = "7031";

    /**
     * 系统信息已经被删除，请刷新页面
     */
    public static final String SYS_ALREADY_DELETE = "7032";

    /**
     * 用户修改成功
     */
    public static final String USER_UPDATE_SUCCESS = "7033";

    /**
     * 用户修改失败
     */
    public static final String USER_UPDATE_FAIL = "7034";

    /**
     * 角色修改成功
     */
    public static final String UPDATE_ROLE_SUCCESS = "7035";

    /**
     * 角色修改失败
     */
    public static final String UPDATE_ROLE_FAIL = "7036";

    /**
     * 组/部门已经被删除，请刷新页面
     */
    public static final String GROUP_ALREADY_DELETE = "7037";

    /**
     * 组/部门修改成功
     */
    public static final String UPDATE_GROUP_SUCCESS = "7038";

    /**
     * 组/部门修改失败
     */
    public static final String UPDATE_GROUP_FAIL = "7039";

    /**
     * 修改操作信息成功
     */
    public static final String UPDATE_OPSINFO_SUCCESS = "7040";

    /**
     * 修改操作信息失败
     */
    public static final String UPDATE_OPSINFO_FAIL = "7041";

    /**
     * 删除操作信息成功
     */
    public static final String DEL_OPSINFO_SUCCESS = "7042";

    /**
     * 删除操作信息失败
     */
    public static final String DEL_OPSINFO_FAIL = "7043";

    /**
     * 获取角色Id失败
     */
    public static final String GET_ROLEID_FAIL = "7044";

    /**
     * 删除角色成功
     */
    public static final String DEL_ROLE_SUCCESS = "7045";

    /**
     * 删除角色失败
     */
    public static final String DEL_ROLE_FAIL = "7046";

    /**
     * 删除组/部门成功
     */
    public static final String DEL_GROUP_SUCCESS = "7047";

    /**
     * 删除组/部门失败
     */
    public static final String DEL_GROUP_FAIL = "7048";

    /**
     * 用户删除成功
     */
    public static final String USER_DEL_SUCCESS = "7049";

    /**
     * 用户删除失败
     */
    public static final String USER_DEL_FAIL = "7050";

    /**
     * 用户状态修改成功
     */
    public static final String UPDATE_USER_SUCCESS = "7051";

    /**
     * 用户状态修改失败
     */
    public static final String UPDATE_USER_FAIL = "7052";

    /**
     * 获取权限数据失败
     */
    public static final String GET_RIGHTDATA_FAIL = "7053";

    /**
     * 您没有设置权限，请设置权限
     */
    public static final String NO_SETTINGRIGHT = "7054";

    /**
     * 权限设置成功
     */
    public static final String SETTINGRIGHT_SUCCESS = "7055";

    /**
     * 权限设置失败
     */
    public static final String SETTINGRIGHT_FAIL = "7056";

    /** **********7500-7999定义登录和鉴权错误信息************* */
    /**
     * 用户名或者密码错误
     */
    public static final String USER_NAMEORPASSWORD_ERROR = "7500";

    /**
     * 您的账户已经被锁定，请在{0}分钟之后重新登录
     */
    public static final String USER_OVERTIME_LOCKED = "7501";

    /**
     * 您的账户已经管理员被锁定，请联系管理员
     */
    public static final String USER_ADMIN_LOCKED = "7502";

    /**
     * 您的账户因为登录错误次数大于{}次，已经被永久锁定，请联系管理员解锁
     */
    public static final String USER_OVERTIME_LOCKED_FOREVER = "7503";

    /**
     * 请输入用户名
     */
    public static final String USERNAME_EMPTY = "7504";

    /**
     * 请输入密码
     */
    public static final String PASSWORD_EMPTY = "7505";

    /**
     * 请输入验证码
     */
    public static final String VALIDATECODE_EMPTY = "7506";

    /**
     * 验证码获取超时，请刷新验证码
     */
    public static final String VALIDATECODE_OVERTIME = "7507";

    /**
     * 验证码获错误，请重新输入
     */
    public static final String VALIDATECODE_ERROR = "7508";

    /**
     * 您的账户没有和系统绑定，请联系管理员
     */
    public static final String USER_NO_SYS = "7509";

    /**
     * 用户和多个系统绑定
     */
    public static final String USER_MULTIPLE_SYS = "7510";

    /**
     * 您的账户已经过期，请联系管理员
     */
    public static final String ACCOUNT_OVERTIME = "7511";

    /**
     * 此账户已经被使用，不能同时被多人使用
     */
    public static final String ACCOUNT_ALREADY_LOAD = "7512";

    /**
     * 登录超时，请重新登录
     */
    public static final String SESSION_OVERTIME = "7600";

    /**
     * 您没有此操作权限，详细情况请联系管理员
     */
    public static final String NO_POWER = "7601";

    /**
     * 安全退出成功
     */
    public static final String LOGOUT_SUCCESS = "7602";

    /**
     * 安全退出失败，请联系管理员
     */
    public static final String LOGOUT_FAIL = "7603";

    /**
     * 此用户信息不存在，可能已经被删除，请联系管理员
     */
    public static final String LOGIN_USER_ISNOT_EXIST = "7604";

    /**
     * 此账号不容许修改密码，请联系管理员
     */
    public static final String LOGIN_PASSWORD_NOT_UPDATE = "7605";

    /**
     * 输入的密码不正确，请重新输入
     */
    public static final String LOGIN_PASSWORD_NOT_RIGHT = "7606";

    /**
     * 密码修改成功
     */
    public static final String LOGIN_PASSWORD_UPDATE_SUCCESS = "7607";

    /**
     * 密码修改失败，请重试
     */
    public static final String LOGIN_PASSWORD_UPDATE_FAIL = "7608";

    /**
     * 2次输入放入密码不一致，请重新输入
     */
    public static final String LOGIN_PASSWORD_ERROR = "7609";
}
