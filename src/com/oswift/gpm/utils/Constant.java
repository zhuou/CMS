package com.oswift.gpm.utils;

public interface Constant
{
    /**
     * 默认父节点Id常量
     */
    public static final int PARENT_ID = -1;

    /**
     * 默认节点深度
     */
    public static final int DEFAULT_LEVEL = 1;

    /**
     * 系统默认ID值
     */
    public static final int DEFAULT_SYSID = 10001;

    /**
     * 操作默认ID
     */
    public static final String DEFAULT_OPSID = "0001";

    /**
     * 文本换行符
     */
    public static final String TXT_LINEBREAK = "\r\n";

    /**
     * session key
     */
    public static final String SESSION_KEY = "loginInfo";

    /**
     * ajax请求头key
     */
    public static final String AJAX_HEAD_KEY = "x-requested-with";

    /**
     * ajax请求头值
     */
    public static final String AJAX_HEAD_VALUES = "XMLHttpRequest";

    /**
     * 登录页面
     */
    public static final String LOGIN_PAGE = "/login.html";

    /**
     * 错误页面
     */
    public static final String ERROR_PAGE = "/error.html";

    /**
     * http请求头
     */
    public static final String HTTP_HEAD = "http://";

    /**
     *
     * 操作类型，定义3种类型。1代表菜单类型 2代表页面类型 3代表按钮操作类型
     *
     * @author zhuou
     */
    public interface OpsType
    {
        /**
         * 菜单
         */
        public static final int MENU = 1;

        /**
         * 页面
         */
        public static final int PAGE = 2;

        /**
         * 操作
         */
        public static final int OPERATION = 3;
    }

    /**
     *
     * 用户状态定义
     *
     * @author zhuou
     * @version C03 2013-7-18
     * @since OSwift GPM V1.0
     */
    public interface UserStatus
    {
        /**
         * 正常
         */
        public static final int NORMAL = 0;

        /**
         * 超过登录错误次数锁定
         */
        public static final int OVERTIME_LOCKED = 1;

        /**
         * 被管理员锁定
         */
        public static final int ADMIN_LOCKED = 2;
    }

    /**
     *
     * 页面名称
     *
     * @author zhuou
     * @version C03 2013-7-22
     * @since OSwift GPM V1.0
     */
    public interface PageName
    {
        /**
         * 基础路径
         */
        public static final String BASE_PATH = "admin/gpm/jsp/";

        /**
         * 错误提示页面
         */
        public static final String ERROR_PAGE = BASE_PATH + "errorInfo";

        /**
         * 组/部门管理页面
         */
        public static final String GROUPLIST = BASE_PATH + "groupList";

        /**
         * Group新增页面
         */
        public static final String GROUP = BASE_PATH + "group";

        /**
         * 组详情
         */
        public static final String GROUPDETAIL = BASE_PATH + "groupDetail";

        /**
         * 操作管理页面
         */
        public static final String OPSINFOLIST = BASE_PATH + "opsInfoList";

        /**
         * 添加修改操作页面
         */
        public static final String OPSINFO = BASE_PATH + "opsInfo";

        /**
         * 角色列表
         */
        public static final String ROLELIST = BASE_PATH + "roleList";

        /**
         * 新增修改角色页面
         */
        public static final String ROLE = BASE_PATH + "role";

        /**
         * 角色详情
         */
        public static final String ROLEDETAIL = BASE_PATH + "roleDetail";

        /**
         * system.jsp页面
         */
        public static final String SYSTEM = BASE_PATH + "system";

        /**
         * 系统列表页面
         */
        public static final String SYSTEMLIST = BASE_PATH + "systemList";

        /**
         * 系统信息详情页面
         */
        public static final String SYSTEMDETAIL = BASE_PATH + "systemDetail";

        /**
         * 系统列表选择页面
         */
        public static final String CHECKSYSTEMLIST = BASE_PATH
                + "checkSystemList";

        /**
         * 新增、修改页面
         */
        public static final String USER = BASE_PATH + "user";

        /**
         * user列表页面
         */
        public static final String USERLIST = BASE_PATH + "userList";

        /**
         * 用户选择页面
         */
        public static final String CHECKUSERLIST = BASE_PATH + "checkUserList";

        /**
         * 用户详情页面
         */
        public static final String USERDETAIL = BASE_PATH + "userDetail";

        /**
         * 登录页面
         */
        public static final String LOGIN = "admin/login";

        /**
         * 后台首页
         */
        public static final String INDEX = "admin/main";

        /**
         * 后台欢迎页面
         */
        public static final String WORKSPACE = "admin/workSpace";

        /**
         * 权限赋值页面
         */
        public static final String SETTINGRIGHT = BASE_PATH + "settingRight";

        /**
         * 修改密码页面
         */
        public static final String UPDATEPWD = BASE_PATH + "updatePwd";
    }
}
