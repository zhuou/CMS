package com.oswift.gpm.web.action;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;

import com.oswift.gpm.entity.LoginBean;
import com.oswift.gpm.system.cache.LoginManager;
import com.oswift.gpm.utils.Constant;

public class BaseAction
{
    /**
     * 每页显示记录数
     */
    protected int pageRecordNum = 15;

    /**
     * 页码默认值
     */
    protected static int DEFAULT_PAGENUM = 1;

    /**
     * 方法标志
     */
    protected static String METHOD = "method";

    /**
     * 添加方法标志
     */
    protected static String METHOD_ADD = "add";

    /**
     * 修改方法标志
     */
    protected static String METHOD_UPDATE = "update";

    /**
     *
     * 将查询字符串解码
     *
     * @author zhuou
     * @param keyword
     *            查询字符串
     * @return 解码字符串
     */
    public String decodeKeyword(String keyword)
    {
        // 解决中文乱码的问题
        if (null != keyword)
        {
            try
            {
                keyword = new String(keyword.getBytes("ISO-8859-1"), "UTF-8");
                keyword = URLDecoder.decode(keyword, "UTF-8");
            }
            catch (UnsupportedEncodingException e)
            {
                // 异常抛弃
            }
        }
        return keyword;
    }

    /**
     *
     * 获取缓存登录信息
     *
     * @author zhuou
     * @return LoginBean
     */
    public LoginBean getLoginBean(HttpServletRequest request)
    {
        Object obj = request.getSession().getAttribute(Constant.SESSION_KEY);
        if (null == obj)
        {
            return null;
        }
        return LoginManager.getLoginBean(String.valueOf(obj));
    }

    /**
     *
     * 登录是否失效
     *
     * @author zhuou
     * @return boolean
     */
    public boolean isOverTime(HttpServletRequest request)
    {
        Object obj = request.getSession().getAttribute(Constant.SESSION_KEY);
        if (null == obj)
        {
            return false;
        }

        return true;
    }
}
