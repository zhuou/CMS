package com.oswift.gpm.web.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.gpm.entity.ErrorBean;
import com.oswift.gpm.entity.LoginBean;
import com.oswift.gpm.entity.MessageBean;
import com.oswift.gpm.entity.OpsInfoBean;
import com.oswift.gpm.service.IOpsInfoService;
import com.oswift.gpm.utils.Constant;
import com.oswift.gpm.utils.PageCode;
import com.oswift.utils.cache.ResourceManager;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.PlatException;

@Controller
public class OpsInfoAction extends BaseAction
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(OpsInfoAction.class);

    /**
     * spring 注入
     */
    @Resource
    private IOpsInfoService opsInfoService;

    /**
     *
     * 操作管理页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/loadOpt", method = RequestMethod.GET)
    public ModelAndView loadOpt()
    {
        ModelAndView mv = null;
        try
        {
            mv = opsInfoService.loadOpsInfo(Constant.PageName.OPSINFOLIST);
        }
        catch (PlatException e)
        {
            log.error("LoadOpt info is error.", e);
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }

        return mv;
    }

    /**
     *
     * 添加操作信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/loadAddOpt", method = RequestMethod.GET)
    public ModelAndView loadAddOpt()
    {
        ModelAndView mv = new ModelAndView(Constant.PageName.OPSINFO);
        mv.addObject(METHOD, METHOD_ADD);
        return mv;
    }

    /**
     *
     * 加载修改时页面信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/loadUpdateOpt", method = RequestMethod.GET)
    public ModelAndView loadUpdateOpt(HttpServletRequest request)
    {
        ModelAndView mv = null;
        String id = request.getParameter("id");

        if (StringUtil.isEmpty(id))
        {
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.GET_WEBPARAM_FAIL);
            bean.setMessInfo(ResourceManager
                    .getValue(PageCode.GET_WEBPARAM_FAIL));
            mv.addObject("bean", bean);
            return mv;
        }

        try
        {
            OpsInfoBean bean = opsInfoService.getOpsInfo(id);
            if (null == bean)
            {
                mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
                ErrorBean errorBean = new ErrorBean();
                errorBean.setCode(PageCode.OPSINFO_DELETED);
                errorBean.setMessInfo(ResourceManager
                        .getValue(PageCode.OPSINFO_DELETED));
                mv.addObject("bean", bean);
                return mv;
            }

            mv = new ModelAndView(Constant.PageName.OPSINFO);
            mv.addObject(METHOD, METHOD_UPDATE);
            mv.addObject("bean", bean);
        }
        catch (PlatException e)
        {
            log.error("Fail to loadUpdateOpt error.", e);
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.COMMON_ERROR);
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }

        return mv;
    }

    /**
     *
     * 添加操作信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/addOpt", method = RequestMethod.POST)
    @ResponseBody
    public Object addOpt(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();

        try
        {
            OpsInfoBean bean = getBeanFromPage(request, METHOD_ADD);

            LoginBean loginBean = this.getLoginBean(request);
            bean.setCreateUser(loginBean.getUserName());

            boolean isSuccess = opsInfoService.add(bean, bean.getParentId());
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.ADD_OPSINFO_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.ADD_OPSINFO_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.ADD_OPSINFO_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("Fail to save System info error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    /**
     *
     * 获取页面数据，放入到OpsInfoBean
     *
     * @author zhuou
     */
    private OpsInfoBean getBeanFromPage(HttpServletRequest request,
            String optType) throws PlatException
    {
        int sort = 0;
        String opsUrl = null;

        OpsInfoBean bean = new OpsInfoBean();

        if (METHOD_ADD.equals(optType))
        {
            String systemId = request.getParameter("systemId");
            if (StringUtil.isEmpty(systemId))
            {
                throw new PlatException(PageCode.GET_WEBPARAM_FAIL,
                        ResourceManager.getValue(PageCode.GET_WEBPARAM_FAIL));
            }
            bean.setSystemId(Integer.valueOf(systemId));

            String s_parentId = request.getParameter("parentId");
            if (StringUtil.isEmpty(s_parentId))
            {
                throw new PlatException(PageCode.GET_WEBPARAM_FAIL,
                        ResourceManager.getValue(PageCode.GET_WEBPARAM_FAIL));
            }
            bean.setParentId(Integer.valueOf(s_parentId));
        }
        else
        {
            String opsId = request.getParameter("opsId");
            if (StringUtil.isEmpty(opsId))
            {
                throw new PlatException(PageCode.GET_WEBPARAM_FAIL,
                        ResourceManager.getValue(PageCode.GET_WEBPARAM_FAIL));
            }
            bean.setOpsId(Integer.valueOf(opsId));
        }
        String s_status = request.getParameter("status");
        if (StringUtil.isEmpty(s_status))
        {
            throw new PlatException(PageCode.GET_WEBPARAM_FAIL, ResourceManager
                    .getValue(PageCode.GET_WEBPARAM_FAIL));
        }
        String s_sort = request.getParameter("sort");
        if (!StringUtil.isEmpty(s_sort))
        {
            sort = Integer.valueOf(s_sort);
        }

        String s_opsType = request.getParameter("opsType");
        if (StringUtil.isEmpty(s_opsType))
        {
            throw new PlatException(PageCode.GET_WEBPARAM_FAIL, ResourceManager
                    .getValue(PageCode.GET_WEBPARAM_FAIL));
        }

        int opsType = Integer.valueOf(s_opsType);
        if (Constant.OpsType.MENU != opsType)
        {
            opsUrl = request.getParameter("opsUrl");
            if (StringUtil.isEmpty(opsUrl))
            {
                throw new PlatException(PageCode.TEXTFIELD_ISNOT_EMPTY,
                        ResourceManager
                                .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
            }
        }

        String opsName = request.getParameter("opsName");
        if (StringUtil.isEmpty(opsName))
        {
            throw new PlatException(PageCode.TEXTFIELD_ISNOT_EMPTY,
                    ResourceManager.getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
        }

        String opsIcon = request.getParameter("opsIcon");
        String opsDesc = request.getParameter("opsDesc");

        bean.setStatus(Integer.valueOf(s_status));
        bean.setSort(sort);
        bean.setOpsUrl(opsUrl);
        bean.setOpsType(opsType);
        bean.setOpsName(opsName);
        bean.setOpsIcon(opsIcon);
        bean.setOpsDesc(opsDesc);

        return bean;
    }

    /**
     *
     * 获取操作信息列表
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/getOpsList", method = RequestMethod.GET)
    @ResponseBody
    public Object getOpsList(HttpServletRequest request)
    {
        String sid = request.getParameter("sid");
        String gid = request.getParameter("gid");
        String rid = request.getParameter("rid");

        MessageBean messBean = new MessageBean();
        try
        {
            if (StringUtil.isEmpty(sid))
            {
                messBean.setCode(PageCode.GET_OPSINFO_LIST_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));

                return messBean;
            }

            List<OpsInfoBean> list = opsInfoService.getOpsList(sid, gid, rid);
            if (null != list)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setObjContent(list);
            }
            else
            {
                messBean.setCode(PageCode.GET_OPSINFO_LIST_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_OPSINFO_LIST_FAIL));
            }
        }
        catch (Exception e)
        {
            log.error("Fail to getOpsList error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    /**
     *
     * 获取操作信息详情
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/getOpsInfo", method = RequestMethod.GET)
    @ResponseBody
    public Object getOpsInfo(HttpServletRequest request)
    {
        String id = request.getParameter("id");

        MessageBean messBean = new MessageBean();
        try
        {
            if (StringUtil.isEmpty(id))
            {
                messBean.setCode(PageCode.GET_OPSINFO_LIST_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));

                return messBean;
            }

            OpsInfoBean bean = opsInfoService.getOpsInfo(id);
            if (null != bean)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setObjContent(bean);
            }
            else
            {
                messBean.setCode(PageCode.GET_OPSINFO_LIST_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_OPSINFO_LIST_FAIL));
            }
        }
        catch (Exception e)
        {
            log.error("Fail to getOpsInfo info error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    /**
     *
     * 修改操作信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/updateOpt", method = RequestMethod.POST)
    @ResponseBody
    public Object updateOpt(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();

        try
        {
            OpsInfoBean bean = getBeanFromPage(request, METHOD_UPDATE);

            LoginBean loginBean = this.getLoginBean(request);
            bean.setLastEditUser(loginBean.getUserName());

            boolean isSuccess = opsInfoService.updateOpsInfo(bean);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.UPDATE_OPSINFO_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.UPDATE_OPSINFO_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.UPDATE_OPSINFO_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("Fail to update Ops info error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    /**
     *
     * 删除操作信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/delOps", method = RequestMethod.GET)
    @ResponseBody
    public Object delOps(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();

        String id = request.getParameter("id");
        if (StringUtil.isEmpty(id))
        {
            messBean.setCode(PageCode.GET_OPSINFO_LIST_FAIL);
            messBean.setContent(ResourceManager
                    .getValue(PageCode.GET_WEBPARAM_FAIL));
            return messBean;
        }

        try
        {
            boolean isSuccess = opsInfoService.delOpsInfo(id);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.DEL_OPSINFO_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.DEL_OPSINFO_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.DEL_OPSINFO_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("Fail to delete ops info error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    public void setOpsInfoService(IOpsInfoService opsInfoService)
    {
        this.opsInfoService = opsInfoService;
    }

}
