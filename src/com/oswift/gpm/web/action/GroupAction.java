package com.oswift.gpm.web.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.gpm.entity.ErrorBean;
import com.oswift.gpm.entity.GroupBean;
import com.oswift.gpm.entity.LoginBean;
import com.oswift.gpm.entity.MessageBean;
import com.oswift.gpm.entity.OpsInfoBean;
import com.oswift.gpm.entity.RoleBean;
import com.oswift.gpm.service.IGroupService;
import com.oswift.gpm.utils.Constant;
import com.oswift.gpm.utils.PageCode;
import com.oswift.utils.cache.ResourceManager;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.PlatException;

@Controller
public class GroupAction extends BaseAction
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(GroupAction.class);

    /**
     * spring 注入
     */
    @Resource
    private IGroupService groupService;

    /**
     *
     * 组/部门管理页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/loadGroup", method = RequestMethod.GET)
    public ModelAndView loadGroup()
    {
        ModelAndView mv = null;
        try
        {
            mv = groupService.loadGroupList(Constant.PageName.GROUPLIST);
        }
        catch (PlatException e)
        {
            log.error("loadGroup is error.", e);
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }

        return mv;
    }

    /**
     *
     * 加载新增页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/loadAddGroup", method = RequestMethod.GET)
    public ModelAndView loadAddGroup(HttpServletRequest request)
    {
        ModelAndView mv = null;
        String id = request.getParameter("id");

        if (StringUtil.isEmpty(id))
        {
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.GET_WEBPARAM_FAIL);
            bean.setMessInfo(ResourceManager
                    .getValue(PageCode.GET_WEBPARAM_FAIL));
            mv.addObject("bean", bean);
            return mv;
        }

        try
        {
            mv = groupService.loadGroup(id, null);
            mv.addObject(METHOD, METHOD_ADD);
        }
        catch (PlatException e)
        {
            log.error("loadAddGroup is error.", e);
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 加载组修改页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/loadUpdateGroup", method = RequestMethod.GET)
    public ModelAndView loadUpdateGroup(HttpServletRequest request)
    {
        ModelAndView mv = null;
        String id = request.getParameter("id");

        if (StringUtil.isEmpty(id))
        {
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.GET_WEBPARAM_FAIL);
            bean.setMessInfo(ResourceManager
                    .getValue(PageCode.GET_WEBPARAM_FAIL));
            mv.addObject("bean", bean);
            return mv;
        }

        try
        {
            GroupBean bean = groupService.getDetailById(id);
            if (null == bean)
            {
                mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
                ErrorBean errorBean = new ErrorBean();
                errorBean.setCode(PageCode.GROUP_ALREADY_DELETE);
                errorBean.setMessInfo(ResourceManager
                        .getValue(PageCode.GROUP_ALREADY_DELETE));
                mv.addObject("bean", errorBean);
            }
            else
            {
                mv = groupService.loadGroup(String.valueOf(bean.getSystemId()),
                        id);
                mv.addObject(METHOD, METHOD_UPDATE);
                mv.addObject("bean", bean);
            }
            return mv;
        }
        catch (PlatException e)
        {
            log.error("loadUpdateGroup is error.", e);
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 根据sysId获取组/部门信息，json格式
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/getGroupList", method = RequestMethod.GET)
    @ResponseBody
    public Object getGroupList(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        String id = request.getParameter("id");
        try
        {
            if (StringUtil.isEmpty(id))
            {
                messBean.setCode(PageCode.GET_OPSINFO_LIST_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));

                return messBean;
            }

            List<GroupBean> bean = groupService.getGroupList(id);
            messBean.setCode(PageCode.COMMON_SUCCESS);
            messBean.setObjContent(bean);
        }
        catch (PlatException e)
        {
            log.error("Fail to getGroupList error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    /**
     *
     * 新增组/部门信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/addGroup", method = RequestMethod.POST)
    @ResponseBody
    public Object addGroup(HttpServletRequest request)
    {
        String sSystemId = request.getParameter("systemId");
        String sParentId = request.getParameter("parentId");
        String sGroupName = request.getParameter("groupName");
        String sGroupDesc = request.getParameter("groupDesc");
        String sInherit = request.getParameter("inherit");
        String sOpsId = request.getParameter("opsId");
        String sRoleIds = request.getParameter("roleIds");

        MessageBean messBean = new MessageBean();
        try
        {
            if (StringUtil.isEmpty(sSystemId) || StringUtil.isEmpty(sGroupName))
            {
                messBean.setCode(PageCode.GET_OPSINFO_LIST_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));

                return messBean;
            }

            if (StringUtil.isEmpty(sOpsId) && StringUtil.isEmpty(sRoleIds))
            {
                messBean.setCode(PageCode.POWER_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.POWER_EMPTY));

                return messBean;
            }

            if (StringUtil.isEmpty(sParentId))
            {
                sParentId = String.valueOf(Constant.PARENT_ID);
            }

            GroupBean bean = new GroupBean();
            bean.setSystemId(Integer.valueOf(sSystemId));
            bean.setParentId(Integer.valueOf(sParentId));
            bean.setInherit(Boolean.valueOf(sInherit));
            bean.setGroupName(sGroupName);
            bean.setGroupDesc(sGroupDesc);

            LoginBean loginBean = this.getLoginBean(request);
            bean.setCreateUser(loginBean.getUserName());

            boolean isSuccess = groupService.addGroup(bean, sRoleIds, sOpsId);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.ADD_GROUP_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.ADD_GROUP_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.ADD_GROUP_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("Fail to addGroup error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }
        catch (Exception e)
        {
            log.error("Fail to addGroup error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    /**
     *
     * 获取组详细，展示详情页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/getGroupDetail", method = RequestMethod.GET)
    public ModelAndView getGroupDetail(HttpServletRequest request)
    {
        ModelAndView mv = null;
        String id = request.getParameter("id");

        if (StringUtil.isEmpty(id))
        {
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(PageCode.GET_WEBPARAM_FAIL);
            bean.setMessInfo(ResourceManager
                    .getValue(PageCode.GET_WEBPARAM_FAIL));
            mv.addObject("bean", bean);
            return mv;
        }

        try
        {
            mv = new ModelAndView(Constant.PageName.GROUPDETAIL);
            GroupBean bean = groupService.getDetailById(id);
            mv.addObject("bean", bean);
        }
        catch (Exception e)
        {
            log.error("getGroupDetail is error.", e);
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 根据组ID获取组拥有的角色
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/getRolesByGroupId", method = RequestMethod.GET)
    @ResponseBody
    public Object getRolesByGroupId(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        String id = request.getParameter("id");
        try
        {
            if (StringUtil.isEmpty(id))
            {
                messBean.setCode(PageCode.GET_OPSINFO_LIST_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));

                return messBean;
            }

            List<RoleBean> bean = groupService.getRolesByGroupId(id);
            messBean.setCode(PageCode.COMMON_SUCCESS);
            messBean.setObjContent(bean);
        }
        catch (PlatException e)
        {
            log.error("Fail to getRolesByGroupId error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    /**
     *
     * 根据组ID获取组拥有的操作权限
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/getOpsByGroupId", method = RequestMethod.GET)
    @ResponseBody
    public Object getOpsByGroupId(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        String id = request.getParameter("id");
        try
        {
            if (StringUtil.isEmpty(id))
            {
                messBean.setCode(PageCode.GET_OPSINFO_LIST_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));

                return messBean;
            }

            List<OpsInfoBean> bean = groupService.getOpsByGroupId(id);
            messBean.setCode(PageCode.COMMON_SUCCESS);
            messBean.setObjContent(bean);
        }
        catch (PlatException e)
        {
            log.error("Fail to getOpsByGroupId error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    /**
     *
     * 修改组/部门信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/updateGroup", method = RequestMethod.POST)
    @ResponseBody
    public Object updateGroup(HttpServletRequest request)
    {
        String sGroupId = request.getParameter("groupId");
        String sGroupName = request.getParameter("groupName");
        String sGroupDesc = request.getParameter("groupDesc");
        String sInherit = request.getParameter("inherit");
        String sOpsId = request.getParameter("opsId");
        String sRoleIds = request.getParameter("roleIds");

        MessageBean messBean = new MessageBean();
        try
        {
            if (StringUtil.isEmpty(sGroupId) || StringUtil.isEmpty(sGroupName))
            {
                messBean.setCode(PageCode.GET_OPSINFO_LIST_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));

                return messBean;
            }

            if (StringUtil.isEmpty(sOpsId) && StringUtil.isEmpty(sRoleIds))
            {
                messBean.setCode(PageCode.POWER_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.POWER_EMPTY));

                return messBean;
            }

            GroupBean bean = new GroupBean();
            bean.setGroupId(Integer.valueOf(sGroupId));
            bean.setInherit(Boolean.valueOf(sInherit));
            bean.setGroupName(sGroupName);
            bean.setGroupDesc(sGroupDesc);

            LoginBean loginBean = this.getLoginBean(request);
            bean.setLastEditUser(loginBean.getUserName());

            boolean isSuccess = groupService
                    .updateGroup(bean, sRoleIds, sOpsId);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.UPDATE_GROUP_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.UPDATE_GROUP_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.UPDATE_GROUP_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("Fail to updateGroup error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }
        catch (Exception e)
        {
            log.error("Fail to updateGroup error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    /**
     *
     * 删除改组/部门信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/delGroup", method = RequestMethod.GET)
    @ResponseBody
    public Object delGroup(HttpServletRequest request)
    {
        String groupId = request.getParameter("id");

        MessageBean messBean = new MessageBean();
        try
        {
            if (StringUtil.isEmpty(groupId))
            {
                messBean.setCode(PageCode.GET_WEBPARAM_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));

                return messBean;
            }

            // 删除
            boolean isSuccess = groupService.delGroup(groupId);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.DEL_GROUP_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.DEL_GROUP_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.DEL_GROUP_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("Fail to delGroup error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    public void setGroupService(IGroupService groupService)
    {
        this.groupService = groupService;
    }
}
