package com.oswift.gpm.web.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.gpm.entity.ErrorBean;
import com.oswift.gpm.entity.LoginBean;
import com.oswift.gpm.entity.MessageBean;
import com.oswift.gpm.entity.OpsInfoBean;
import com.oswift.gpm.entity.PageBean;
import com.oswift.gpm.entity.RoleBean;
import com.oswift.gpm.service.IRoleService;
import com.oswift.gpm.utils.Constant;
import com.oswift.gpm.utils.PageCode;
import com.oswift.utils.cache.ResourceManager;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.PlatException;

@Controller
public class RoleAction extends BaseAction
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(RoleAction.class);

    /**
     * spring 注入RoleServiceImpl
     */
    @Resource
    private IRoleService roleService;

    /**
     *
     * 获取角色列表
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/getRoleList", method = RequestMethod.GET)
    public ModelAndView getRoleList(HttpServletRequest request)
    {
        ModelAndView mv = null;
        String s_PageNum = request.getParameter("pageNum");

        int pageNum = DEFAULT_PAGENUM;
        if (!StringUtil.isEmpty(s_PageNum))
        {
            pageNum = Integer.valueOf(s_PageNum);
        }
        try
        {
            PageBean bean = roleService
                    .getRoleList(this.pageRecordNum, pageNum);
            mv = new ModelAndView(Constant.PageName.ROLELIST);
            mv.addObject("pageBean", bean);
        }
        catch (PlatException e)
        {
            log.error("Get getRoleList info is error.", e);
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            bean.setUrl("roleList.html");
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 加载新增角色页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/loadAddRole", method = RequestMethod.GET)
    public ModelAndView loadAddRole()
    {
        ModelAndView mv = null;
        try
        {
            mv = roleService.loadRole(Constant.PageName.ROLE);
            mv.addObject(METHOD, METHOD_ADD);
        }
        catch (PlatException e)
        {
            log.error("Load addRole page is error.", e);
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 加载更新角色页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/loadUpdateRole", method = RequestMethod.GET)
    public ModelAndView loadUpdateRole(HttpServletRequest request)
    {
        ModelAndView mv = null;

        String roleId = request.getParameter("id");
        try
        {
            if (StringUtil.isEmpty(roleId))
            {
                mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setMessInfo(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
            }
            else
            {
                RoleBean bean = roleService.getRoleDetail(roleId);
                mv = new ModelAndView(Constant.PageName.ROLE);
                mv.addObject(METHOD, METHOD_UPDATE);
                mv.addObject("bean", bean);
            }
        }
        catch (PlatException e)
        {
            log.error("Load loadUpdateRole page is error.", e);
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 新增角色
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/addRole", method = RequestMethod.POST)
    @ResponseBody
    public Object addRole(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String systemId = request.getParameter("systemId");
            String roleName = request.getParameter("roleName");
            String roleDesc = request.getParameter("roleDesc");
            if (StringUtil.isEmpty(systemId))
            {
                messBean.setCode(PageCode.GET_SYSINFO_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_SYSINFO_FAIL));
                return messBean;
            }
            if (StringUtil.isEmpty(roleName))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }
            String opsId = request.getParameter("opsId");
            if (StringUtil.isEmpty(opsId))
            {
                messBean.setCode(PageCode.GET_OPSID_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_OPSID_FAIL));
                return messBean;
            }
            RoleBean bean = new RoleBean();
            bean.setSystemId(Integer.valueOf(systemId));
            bean.setRoleName(roleName);
            bean.setRoleDesc(roleDesc);

            LoginBean loginBean = this.getLoginBean(request);
            bean.setCreateUser(loginBean.getUserName());

            boolean isSuccess = roleService.add(bean, opsId);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.ADD_ROLE_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.ADD_ROLE_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.ADD_ROLE_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("addRole is Fail!", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    /**
     *
     * 获取角色详情
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/getRoleDetail", method = RequestMethod.GET)
    public ModelAndView getRoleDetail(HttpServletRequest request)
    {
        ModelAndView mv = null;
        String s_Id = request.getParameter("id");

        try
        {
            if (StringUtil.isEmpty(s_Id))
            {
                mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setMessInfo(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
            }
            else
            {
                mv = new ModelAndView(Constant.PageName.ROLEDETAIL);
                RoleBean bean = roleService.getRoleDetail(s_Id);
                mv.addObject("bean", bean);
            }
        }
        catch (PlatException e)
        {
            log.error("Get getRoleDetail info is error.", e);
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 根据角色ID查询角色下拥有的操作
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/getOpsByRoleId", method = RequestMethod.GET)
    @ResponseBody
    public Object getOpsByRoleId(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String roleId = request.getParameter("id");
            if (StringUtil.isEmpty(roleId))
            {
                messBean.setCode(PageCode.GET_SYSINFO_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_SYSINFO_FAIL));
                return messBean;
            }

            List<OpsInfoBean> beans = roleService.getOpsByRoleId(roleId);
            messBean.setCode(PageCode.COMMON_SUCCESS);
            messBean.setObjContent(beans);
        }
        catch (PlatException e)
        {
            log.error("getOpsByRoleId is Fail!", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    /**
     *
     * 修改角色
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/updateRole", method = RequestMethod.POST)
    @ResponseBody
    public Object updateRole(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String roleName = request.getParameter("roleName");
            String roleDesc = request.getParameter("roleDesc");
            String roleId = request.getParameter("roleId");
            if (StringUtil.isEmpty(roleId))
            {
                messBean.setCode(PageCode.GET_SYSINFO_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_SYSINFO_FAIL));
                return messBean;
            }
            if (StringUtil.isEmpty(roleName))
            {
                messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
                return messBean;
            }
            String opsId = request.getParameter("opsId");
            if (StringUtil.isEmpty(opsId))
            {
                messBean.setCode(PageCode.GET_OPSID_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_OPSID_FAIL));
                return messBean;
            }
            RoleBean bean = new RoleBean();
            bean.setRoleId(Integer.valueOf(roleId));
            bean.setRoleName(roleName);
            bean.setRoleDesc(roleDesc);

            LoginBean loginBean = this.getLoginBean(request);
            bean.setLastEditUser(loginBean.getUserName());

            boolean isSuccess = roleService.updateRole(bean, opsId);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.UPDATE_ROLE_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.UPDATE_ROLE_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.UPDATE_ROLE_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("updateRole is Fail!", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    /**
     *
     * 删除角色
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/delRole", method = RequestMethod.GET)
    @ResponseBody
    public Object delRole(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        try
        {
            String roleId = request.getParameter("id");
            if (StringUtil.isEmpty(roleId))
            {
                messBean.setCode(PageCode.GET_ROLEID_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.GET_ROLEID_FAIL));
                return messBean;
            }

            boolean isSuccess = roleService.delRole(roleId);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.DEL_ROLE_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.DEL_ROLE_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.DEL_ROLE_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("delRole is Fail!", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }

        return messBean;
    }

    public void setRoleService(IRoleService roleService)
    {
        this.roleService = roleService;
    }
}
