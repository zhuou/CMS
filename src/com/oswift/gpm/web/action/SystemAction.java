package com.oswift.gpm.web.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.oswift.gpm.entity.ErrorBean;
import com.oswift.gpm.entity.LoginBean;
import com.oswift.gpm.entity.MessageBean;
import com.oswift.gpm.entity.PageBean;
import com.oswift.gpm.entity.SystemModuleBean;
import com.oswift.gpm.entity.UserBean;
import com.oswift.gpm.service.ISystemModuleService;
import com.oswift.gpm.utils.Constant;
import com.oswift.gpm.utils.PageCode;
import com.oswift.utils.cache.ResourceManager;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.PlatException;

@Controller
public class SystemAction extends BaseAction
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(SystemAction.class);

    /**
     * spring注入CompanyServiceImpl对象
     */
    @Resource
    private ISystemModuleService systemService;

    /**
     *
     * 添加系统信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/addSys", method = RequestMethod.POST)
    @ResponseBody
    public Object addSys(SystemModuleBean bean, HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        if (StringUtil.isEmpty(bean.getSystemName())
                || StringUtil.isEmpty(bean.getSystemIP())
                || StringUtil.isEmpty(bean.getSecretKey())
                || StringUtil.isEmpty(bean.getHostAddr()))
        {
            messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
            messBean.setContent(ResourceManager
                    .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
            return messBean;
        }

        String userId = request.getParameter("userId");
        List<String> list = new ArrayList<String>();
        if (!StringUtil.isEmpty(userId))
        {
            String[] users = userId.split(",");
            for (String s : users)
            {
                list.add(s);
            }
        }

        try
        {
            LoginBean loginBean = this.getLoginBean(request);
            bean.setCreateUser(loginBean.getUserName());

            boolean isSuccess = systemService.addSystem(bean, list);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.SYS_ADD_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.COMMON_ERROR);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.SYS_ADD_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("Fail to save System info error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 初始化system.jsp页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/loadSys", method = RequestMethod.GET)
    public ModelAndView loadSys(HttpServletRequest request)
    {
        ModelAndView mv = new ModelAndView(Constant.PageName.SYSTEM);
        mv.addObject(METHOD, METHOD_ADD);
        return mv;
    }

    /**
     *
     * 初始化system.jsp修改页面
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/loadUpdateSys", method = RequestMethod.GET)
    public ModelAndView loadUpdateSys(HttpServletRequest request)
    {
        ModelAndView mv = null;

        try
        {
            String s_Id = request.getParameter("id");
            if (StringUtil.isEmpty(s_Id))
            {
                mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setCode(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
            }
            else
            {
                SystemModuleBean bean = systemService.getSysDetail(Integer
                        .valueOf(s_Id));
                mv = new ModelAndView(Constant.PageName.SYSTEM);
                mv.addObject("bean", bean);
                mv.addObject(METHOD, METHOD_UPDATE);
            }
        }
        catch (PlatException e)
        {
            log.error("loadUpdateSys info is error.", e);
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 获取系统列表信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/getSysList", method = RequestMethod.GET)
    public ModelAndView getSysList(HttpServletRequest request)
    {
        ModelAndView mv = null;
        String s_PageNum = request.getParameter("pageNum");

        int pageNum = DEFAULT_PAGENUM;
        if (!StringUtil.isEmpty(s_PageNum))
        {
            pageNum = Integer.valueOf(s_PageNum);
        }
        try
        {
            PageBean bean = systemService.getSysList(this.pageRecordNum,
                    pageNum);
            mv = new ModelAndView(Constant.PageName.SYSTEMLIST);
            mv.addObject("pageBean", bean);
        }
        catch (PlatException e)
        {
            log.error("Get getSysList info is error.", e);
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            bean.setUrl("getSysList.html");
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 根据Id获取系统信息详情
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/getSysDetail", method = RequestMethod.GET)
    public ModelAndView getSysDetail(HttpServletRequest request)
    {
        ModelAndView mv = null;
        String s_Id = request.getParameter("id");

        try
        {
            if (StringUtil.isEmpty(s_Id))
            {
                mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
                ErrorBean bean = new ErrorBean();
                bean.setMessInfo(PageCode.GET_WEBPARAM_FAIL);
                bean.setMessInfo(ResourceManager
                        .getValue(PageCode.GET_WEBPARAM_FAIL));
                mv.addObject("bean", bean);
            }
            else
            {
                SystemModuleBean bean = systemService.getSysDetail(Integer
                        .valueOf(s_Id));
                mv = new ModelAndView(Constant.PageName.SYSTEMDETAIL);
                mv.addObject("bean", bean);
            }
        }
        catch (PlatException e)
        {
            log.error("Get getSysDetail info is error.", e);
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            bean.setUrl("getSysList.html");
            mv.addObject("bean", bean);
        }
        return mv;
    }

    /**
     *
     * 根据sysId查询系统下拥有的所有用户
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/getUsersBySysId", method = RequestMethod.GET)
    @ResponseBody
    public Object getUsersBySysId(HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        String systemId = request.getParameter("id");

        if (StringUtil.isEmpty(systemId))
        {
            messBean.setCode(PageCode.GET_WEBPARAM_FAIL);
            messBean.setContent(ResourceManager
                    .getValue(PageCode.GET_WEBPARAM_FAIL));
            return messBean;
        }
        try
        {
            List<UserBean> beans = systemService.getUsersBySysId(systemId);
            messBean.setCode(PageCode.COMMON_SUCCESS);
            messBean.setObjContent(beans);
        }
        catch (PlatException e)
        {
            log.error("Fail to getUsersBySysId info error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 修改系统信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/updateSys", method = RequestMethod.POST)
    @ResponseBody
    public Object updateSys(SystemModuleBean bean, HttpServletRequest request)
    {
        MessageBean messBean = new MessageBean();
        String sysId = request.getParameter("id");

        if (StringUtil.isEmpty(sysId)
                || StringUtil.isEmpty(bean.getSystemName())
                || StringUtil.isEmpty(bean.getSystemIP())
                || StringUtil.isEmpty(bean.getSecretKey())
                || StringUtil.isEmpty(bean.getHostAddr()))
        {
            messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
            messBean.setContent(ResourceManager
                    .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
            return messBean;
        }

        String userId = request.getParameter("userId");
        List<String> list = new ArrayList<String>();
        if (!StringUtil.isEmpty(userId))
        {
            String[] users = userId.split(",");
            for (String s : users)
            {
                list.add(s);
            }
        }

        try
        {
            bean.setSystemId(Integer.valueOf(sysId));

            LoginBean loginBean = this.getLoginBean(request);
            bean.setLastEditUser(loginBean.getUserName());

            boolean isSuccess = systemService.updateSystem(bean, list);
            if (isSuccess)
            {
                messBean.setCode(PageCode.COMMON_SUCCESS);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.SYS_UPDATE_SUCCESS));
            }
            else
            {
                messBean.setCode(PageCode.SYS_UPDATE_FAIL);
                messBean.setContent(ResourceManager
                        .getValue(PageCode.SYS_UPDATE_FAIL));
            }
        }
        catch (PlatException e)
        {
            log.error("Fail to update System info error.", e);
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 根据sysId删除系统信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/delSys", method = RequestMethod.GET)
    @ResponseBody
    public Object delSys(HttpServletRequest request)
    {
        MessageBean messBean = null;
        String sysId = request.getParameter("id");

        if (StringUtil.isEmpty(sysId))
        {
            messBean = new MessageBean();
            messBean.setCode(PageCode.TEXTFIELD_ISNOT_EMPTY);
            messBean.setContent(ResourceManager
                    .getValue(PageCode.TEXTFIELD_ISNOT_EMPTY));
            return messBean;
        }

        try
        {
            messBean = systemService.delSys(sysId);
        }
        catch (PlatException e)
        {
            log.error("Fail to update System info error.", e);

            messBean = new MessageBean();
            messBean.setCode(PageCode.COMMON_ERROR);
            messBean.setContent(e.getMessage());
        }
        return messBean;
    }

    /**
     *
     * 获取系统列表信息
     *
     * @author zhuou
     */
    @RequestMapping(value = "/security/getCheckSysList", method = RequestMethod.GET)
    public ModelAndView getCheckSysList(HttpServletRequest request)
    {
        ModelAndView mv = null;
        String s_PageNum = request.getParameter("pageNum");

        int pageNum = DEFAULT_PAGENUM;
        if (!StringUtil.isEmpty(s_PageNum))
        {
            pageNum = Integer.valueOf(s_PageNum);
        }
        try
        {
            PageBean bean = systemService.getSysList(this.pageRecordNum,
                    pageNum);
            mv = new ModelAndView(Constant.PageName.CHECKSYSTEMLIST);
            mv.addObject("pageBean", bean);
        }
        catch (PlatException e)
        {
            log.error("Get getCheckSysList info is error.", e);
            mv = new ModelAndView(Constant.PageName.ERROR_PAGE);
            ErrorBean bean = new ErrorBean();
            bean.setCode(e.getExceptionCode());
            bean.setMessInfo(e.getMessage());
            mv.addObject("bean", bean);
        }
        return mv;
    }

    public void setSystemService(ISystemModuleService systemService)
    {
        this.systemService = systemService;
    }
}
