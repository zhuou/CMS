package com.oswift.gpm.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.gpm.entity.SystemModuleBean;
import com.oswift.gpm.entity.UserBean;
import com.oswift.gpm.entity.UserRGroupBean;
import com.oswift.gpm.entity.UserRRoleBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("userDao")
public class UserDao extends BaseDao<UserBean>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(UserDao.class);

    /**
     *
     * 构造函数
     */
    public UserDao()
    {
        super(SqlName.User.NAMESPACE);
    }

    /**
     *
     * 根据userId查询和此用户关联的系统
     *
     * @author zhuou
     * @param userId
     *            用户ID
     * @return List<SystemModuleBean>
     */
    public List<SystemModuleBean> getSysByUserId(String userId)
    {
        return this.sqlSession.selectList(SqlName.User.GET_SYSBYUSERID, userId);
    }

    /**
     *
     * 根据userId删除用户和系统关联表数据
     *
     * @author zhuou
     * @param userId
     *            用户ID
     * @return int
     */
    public int delUserRSysByUId(String userId) throws PlatException
    {
        try
        {
            return this.sqlSession.delete(SqlName.User.DEL_USERRSYSBYUID,
                    userId);
        }
        catch (Exception e)
        {
            log.error("Fail to delUserRSysByUId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据userId删除用户和角色关联表数据
     *
     * @author zhuou
     * @param userId
     *            用户ID
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int delUserRRoleByUId(String userId) throws PlatException
    {
        try
        {
            return this.sqlSession.delete(SqlName.User.DEL_USERRROLEBYUID,
                    userId);
        }
        catch (Exception e)
        {
            log.error("Fail to delUserRRoleByUId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据userId删除用户和组关联表数据
     *
     * @author zhuou
     * @param userId
     *            用户ID
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int delUserRGroupByUId(String userId) throws PlatException
    {
        try
        {
            return this.sqlSession.delete(SqlName.User.DEL_USERRGROUPBYUID,
                    userId);
        }
        catch (Exception e)
        {
            log.error("Fail to delUserRGroupByUId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据userId修改用户状态
     *
     * @author zhuou
     * @param userId
     *            用户Id
     * @param status
     *            状态
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updateUserStatus(int userId, int status) throws PlatException
    {
        Map<String, Integer> param = new HashMap<String, Integer>();
        param.put("status", status);
        param.put("userId", userId);

        try
        {
            return this.sqlSession
                    .update(SqlName.User.UPDATE_USERSTATUS, param);
        }
        catch (Exception e)
        {
            log.error("Fail to updateUserStatus info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据userId查询用户和组关联数据
     *
     * @author zhuou
     * @param userId
     *            用户ID
     * @return List
     * @throws PlatException
     *             公共异常
     */
    public List<UserRGroupBean> getUserRGroupByUId(int userId, int systemId)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("userId", userId);
            param.put("systemId", systemId);

            return this.sqlSession.selectList(SqlName.User.GET_USERRGROUPBYUID,
                    param);
        }
        catch (Exception e)
        {
            log.error("Fail to getUserRGroupByUId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据userId查询用户和角色关联数据
     *
     * @author zhuou
     * @param userId
     *            用户ID
     * @return List
     * @throws PlatException
     *             公共异常
     */
    public List<UserRRoleBean> getUserRRoleByUId(int userId, int systemId)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("userId", userId);
            param.put("systemId", systemId);

            return this.sqlSession.selectList(SqlName.User.GET_USERRROLEBYUID,
                    param);
        }
        catch (Exception e)
        {
            log.error("Fail to getUserRRoleByUId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据userId和sysId删除用户和组关联数据
     *
     * @author zhuou
     * @param userId
     *            用户Id
     * @param systemId
     *            系统Id
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int delUserRGroupByUIdAndSysId(int userId, int systemId)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("userId", userId);
            param.put("systemId", systemId);

            return this.sqlSession.delete(
                    SqlName.User.DEL_USERRGROUPBYUIDSYSID, param);
        }
        catch (Exception e)
        {
            log.error("Fail to delUserRGroupByUIdAndSysId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据userId和sysId删除用户和角色关联数据
     *
     * @author zhuou
     * @param userId
     *            用户Id
     * @param systemId
     *            系统Id
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int delUserRRoleByUIdAndSysId(int userId, int systemId)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("userId", userId);
            param.put("systemId", systemId);

            return this.sqlSession.delete(SqlName.User.DEL_USERRROLEBYUIDSYSID,
                    param);
        }
        catch (Exception e)
        {
            log.error("Fail to delUserRRoleByUIdAndSysId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 新增用户和组关联数据
     *
     * @author zhuou
     * @param list
     *            UserRGroupBean list
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int addUserRGroup(List<UserRGroupBean> list) throws PlatException
    {
        try
        {
            return this.sqlSession.insert(SqlName.User.ADD_R_USERGROUP, list);
        }
        catch (Exception e)
        {
            log.error("Fail to addUserRGroup info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 新增用户和角色关联数据
     *
     * @author zhuou
     * @param list
     *            UserRRoleBean list
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int addUserRRole(List<UserRRoleBean> list) throws PlatException
    {
        try
        {
            return this.sqlSession.insert(SqlName.User.ADD_R_USERGROLE, list);
        }
        catch (Exception e)
        {
            log.error("Fail to addUserRRole info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

}
