package com.oswift.gpm.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.gpm.entity.OpsInfoBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("opsInfoDao")
public class OpsInfoDao extends BaseDao<OpsInfoBean>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(OpsInfoDao.class);

    /**
     *
     * 构造函数
     */
    public OpsInfoDao()
    {
        super(SqlName.OpsInfo.NAMESPACE);
    }

    /**
     *
     * 根据操作ID删除操作和组关联数据
     *
     * @author zhuou
     * @param opsId
     *            操作ID
     * @return int
     */
    public int delOpsRGroupByOpsId(String opsId) throws PlatException
    {
        try
        {
            return this.sqlSession.delete(SqlName.OpsInfo.DELETE_R_OPSGROUP,
                    opsId);
        }
        catch (Exception e)
        {
            log.error("Fail to del delOpsRGroupByOpsId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据操作ID删除操作和角色关联数据
     *
     * @author zhuou
     * @param opsId
     *            操作ID
     * @return int
     */
    public int delOpsRRoleByOpsId(String opsId) throws PlatException
    {
        try
        {
            return this.sqlSession.delete(SqlName.OpsInfo.DELETE_R_OPSROLE,
                    opsId);
        }
        catch (Exception e)
        {
            log.error("Fail to del delOpsRRoleByOpsId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据操作ID删除操作和系统关联数据
     *
     * @author zhuou
     * @param opsId
     *            操作ID
     * @return int
     */
    public int delOpsRSysByOpsId(String opsId) throws PlatException
    {
        try
        {
            return this.sqlSession.delete(SqlName.OpsInfo.DELETE_R_OPSSYS,
                    opsId);
        }
        catch (Exception e)
        {
            log.error("Fail to del delOpsRSysByOpsId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据ParentId修改ParentId和NoteLevel
     *
     * @author zhuou
     * @param childBean
     *            要修改的bean信息
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updateOpsChild(OpsInfoBean childBean) throws PlatException
    {
        try
        {
            return this.sqlSession.update(SqlName.OpsInfo.UPDATE_OPSCHILD,
                    childBean);
        }
        catch (Exception e)
        {
            log.error("Fail to del updateOpsChild info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 获取操作ID最大的操作信息
     *
     * @author zhuou
     * @return OpsInfoBean
     */
    public OpsInfoBean getTop1Ops(int systemId)
    {
        return this.sqlSession.selectOne(SqlName.OpsInfo.GET_TOP1OPS, systemId);
    }
}
