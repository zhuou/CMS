package com.oswift.gpm.dao;

import org.springframework.stereotype.Repository;

import com.oswift.gpm.entity.UserBean;

@Repository("contacterDao")
public class ContacterDao extends BaseDao<UserBean>
{
    /**
     *
     * 构造函数
     */
    public ContacterDao()
    {
        super(SqlName.Contacter.NAMESPACE);
    }

}
