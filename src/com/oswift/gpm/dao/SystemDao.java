package com.oswift.gpm.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.oswift.gpm.entity.RelationBean;
import com.oswift.gpm.entity.SystemModuleBean;
import com.oswift.gpm.entity.UserBean;

@Repository("systemDao")
public class SystemDao extends BaseDao<SystemModuleBean>
{
    public SystemDao()
    {
        super(SqlName.System.NAMESPACE);
    }

    /**
     *
     * 插入用户和系统关联表数据
     *
     * @author zhuou
     * @param bean
     *            RelationBean
     * @return int影响行数
     */
    public int addUserRSys(List<RelationBean> bean)
    {
        return this.sqlSession.insert(SqlName.System.ADD_R_USERSYSTEM, bean);
    }

    /**
     *
     * 获取所有系统信息不带分页
     *
     * @author zhuou
     * @return list
     */
    public List<SystemModuleBean> getSysList()
    {
        return this.sqlSession.selectList(SqlName.System.GET_SYS_LIST);
    }

    /**
     *
     * 查询Id号最大的sys
     *
     * @author zhuou
     * @return SystemModuleBean
     */
    public SystemModuleBean getTop1Sys()
    {
        return this.sqlSession.selectOne(SqlName.System.GET_TOP1SYS);
    }

    /**
     *
     * 根据sysId查询系统下拥有的所有用户
     *
     * @author zhuou
     * @param systemId
     *            系统ID
     * @return List<UserBean>
     */
    public List<UserBean> getUsersBySysId(String systemId)
    {
        return this.sqlSession.selectList(SqlName.System.GET_USERSBYSYSID,
                systemId);
    }

    /**
     *
     * 根据sysId删除系统和用户关系表
     *
     * @author zhuou
     * @param systemId
     *            系统ID
     * @return int
     */
    public int delUserRSysBySysId(int systemId)
    {
        return this.sqlSession.delete(SqlName.System.DEL_USERRSYSBYSYSID,
                systemId);
    }

    /**
     *
     * 根据sysId查看此系统下有多少个角色信息
     *
     * @author zhuou
     * @param systemId
     *            系统ID
     * @return int
     */
    public int countRolesBySysId(String systemId)
    {
        return (Integer)this.sqlSession.selectOne(SqlName.System.COUNT_ROLESBYSYSID,
                systemId);
    }

    /**
     *
     * 根据sysId查看此系统下有多少个操作信息
     *
     * @author zhuou
     * @param systemId
     *            系统ID
     * @return int
     */
    public int countOpsBySysId(String systemId)
    {
        return (Integer)this.sqlSession.selectOne(SqlName.System.COUNT_OPSBYSYSID,
                systemId);
    }

    /**
     *
     * 根据sysId查看此系统下有多少个用户信息
     *
     * @author zhuou
     * @param systemId
     *            系统ID
     * @return int
     */
    public int countUsersBySysId(String systemId)
    {
        return (Integer)this.sqlSession.selectOne(SqlName.System.COUNT_USERSBYSYSID,
                systemId);
    }

    /**
     *
     * 根据sysId查看此系统下有多少个组/部门信息
     *
     * @author zhuou
     * @param systemId
     *            系统ID
     * @return int
     */
    public int countGroupsBySysId(String systemId)
    {
        int count = (Integer)this.sqlSession.selectOne(SqlName.System.COUNT_GROUPSBYSYSID,
                systemId);
        return count;
    }
}
