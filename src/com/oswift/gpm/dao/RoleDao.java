package com.oswift.gpm.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.gpm.entity.OpsInfoBean;
import com.oswift.gpm.entity.RelationBean;
import com.oswift.gpm.entity.RoleBean;
import com.oswift.gpm.entity.RoleROpsBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("roleDao")
public class RoleDao extends BaseDao<RoleBean>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(RoleDao.class);

    /**
     *
     * 构造函数
     */
    public RoleDao()
    {
        super(SqlName.Role.NAMESPACE);
    }

    /**
     *
     * 插入T_GPM_R_OpsRole关系表,字段顺序为(RoleId,OpsId)
     *
     * @author zhuou
     * @param bean
     *            List
     * @return int
     */
    public int addROpsRole(List<RelationBean> bean)
    {
        return this.sqlSession.insert(SqlName.Role.ADD_R_OPSROLE, bean);
    }

    /**
     *
     * 根据sysid获取此系统的所有角色
     *
     * @author zhuou
     * @param systemId
     *            系统id
     * @return List
     */
    public List<RoleBean> getRolesBySysId(String systemId)
    {
        return this.sqlSession.selectList(SqlName.Role.GET_ROLESBYSYSID,
                systemId);
    }

    /**
     *
     * 根据角色ID查询角色下拥有的操作
     *
     * @author zhuou
     * @param roleId
     *            角色Id
     * @return List<OpsInfoBean>
     */
    public List<OpsInfoBean> getOpsByRoleId(String roleId)
    {
        return this.sqlSession.selectList(SqlName.Role.GET_OPSBYROLEID, roleId);
    }

    /**
     *
     * 根据角色ID删除操作和角色关联表数据
     *
     * @author zhuou
     * @param roleId
     *            角色ID
     * @return int
     */
    public int delOpsRRoleByRoleId(String roleId) throws PlatException
    {
        try
        {
            return this.sqlSession.delete(SqlName.Role.DEL_OPSRROLEBYROLEID,
                    roleId);
        }
        catch (Exception e)
        {
            log.error("Fail to del delOpsRRoleByRoleId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据角色ID删除组和角色关联表数据
     *
     * @author zhuou
     * @param roleId
     *            角色ID
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int delRoleRGroupByRoleId(String roleId) throws PlatException
    {
        try
        {
            return this.sqlSession.delete(SqlName.Role.DEL_ROLERROLEBYROLEID,
                    roleId);
        }
        catch (Exception e)
        {
            log.error("Fail to del delRoleRGroupByRoleId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据角色ID删除用户和角色关联表数据
     *
     * @author zhuou
     * @param roleId
     *            角色ID
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int delUserRRoleByRoleId(String roleId) throws PlatException
    {
        try
        {
            return this.sqlSession.delete(SqlName.Role.DEL_USERRROLEBYROLEID,
                    roleId);
        }
        catch (Exception e)
        {
            log.error("Fail to del delUserRRoleByRoleId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据角色Id获取角色和操作关联表数据
     *
     * @author zhuou
     * @param roleId
     *            角色ID
     * @return List
     * @throws PlatException
     *             公共异常
     */
    public List<RoleROpsBean> getRoleROpsByRoleId(String roleId)
        throws PlatException
    {
        if (null == roleId || "".equals(roleId))
        {
            return null;
        }
        try
        {
            return this.sqlSession.selectList(
                    SqlName.Role.GET_ROLEROPSBYROLEID, roleId);
        }
        catch (Exception e)
        {
            log.error("Fail to del getRoleROpsByRoleId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }
}
