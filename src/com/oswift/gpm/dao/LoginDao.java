package com.oswift.gpm.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.oswift.gpm.entity.MenuBean;
import com.oswift.gpm.entity.UserBean;
import com.oswift.utils.common.StringUtil;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("loginDao")
public class LoginDao
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(LoginDao.class);

    /**
     * 映射生成SqlSession对象
     */
    @Autowired
    private SqlSession sqlSession;

    /**
     *
     * 根据用户名称获取用户信息
     *
     * @author zhuou
     * @param userName
     *            用户名称
     * @return UserBean
     * @throws PlatException
     *             公共异常
     */
    public UserBean getUserByName(String userName) throws PlatException
    {
        try
        {
            return sqlSession.selectOne(SqlName.Login.GET_USERBYNAME, userName);
        }
        catch (Exception e)
        {
            log.error("Fail to del getUserByName info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据UserID,修改LoginCount,LastLoginIP,LastLoginTime,LoginErrorCount字段
     *
     * @author zhuou
     * @param bean
     *            UserBean
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updateUserLoginInfo(UserBean bean) throws PlatException
    {
        try
        {
            return sqlSession.update(SqlName.Login.UPDATE_USERLOGININFO, bean);
        }
        catch (Exception e)
        {
            log.error("Fail to del updateUserLoginInfo info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 修改用户状态
     *
     * @author zhuou
     * @param status
     *            用户状态
     * @param userId
     *            用户ID
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updateStatus(int status, int userId) throws PlatException
    {
        try
        {
            Map<String, Integer> param = new HashMap<String, Integer>();
            param.put("status", status);
            param.put("userId", userId);

            return sqlSession.update(SqlName.Login.UPDATE_USERLOGININFO, param);
        }
        catch (Exception e)
        {
            log.error("Fail to del updateStatus info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据userId、systemId和节点深度获取菜单列表
     *
     * @author zhuou
     * @param userId
     *            用户Id
     * @param systemId
     *            系统Id
     * @param noteLevel
     *            节点深度 如1只查节点深度为1 如1,2 只查节点深度为1,2的节点；当为空或者null查询全部
     * @param parentId
     *            父节点ID
     *
     * @return List
     * @throws PlatException
     *             公共异常
     */
    public List<MenuBean> getMenu(int userId, int systemId, int parentId,
            String noteLevel) throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("userId", userId);
            param.put("systemId", systemId);

            // 判断节点深度
            if (StringUtil.isEmpty(noteLevel))
            {
                param.put("noteLevel", null);
            }
            else
            {
                param.put("noteLevel", noteLevel);
            }

            // 判断父节点Id
            if (parentId <= 0)
            {
                param.put("parentId", null);
            }
            else
            {
                param.put("parentId", parentId);
            }

            return sqlSession.selectList(SqlName.Login.GET_MENU1, param);
        }
        catch (Exception e)
        {
            log.error("Fail to getMenuList info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据userId、systemId和url获取操作，可能有多个
     *
     * @author zhuou
     * @param userId
     *            用户Id
     * @param systemId
     *            系统Id
     * @param opsUrl
     *            操作Url
     * @return List
     * @throws PlatException
     *             公共异常
     */
    public List<MenuBean> getMenu(int userId, int systemId, String opsUrl)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("userId", userId);
            param.put("systemId", systemId);
            param.put("opsUrl", opsUrl);

            return sqlSession.selectList(SqlName.Login.GET_MENU2, param);
        }
        catch (Exception e)
        {
            log.error("Fail to get one or more Menu info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据userId、systemId和opsId获取某个操作
     *
     * @author zhuou
     * @param userId
     *            用户Id
     * @param systemId
     *            系统Id
     * @param opsId
     *            操作Id
     * @return MenuBean
     * @throws PlatException
     *             公共异常
     */
    public MenuBean getMenu(int userId, int systemId, int opsId)
        throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("userId", userId);
            param.put("systemId", systemId);
            param.put("opsId", opsId);

            return sqlSession.selectOne(SqlName.Login.GET_MENU3, param);
        }
        catch (Exception e)
        {
            log.error("Fail to getOneMenu info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 修改用户密码
     *
     * @author zhuou
     * @param userId
     *            用户id
     * @param password
     *            密码
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updatePassword(int userId, String password) throws PlatException
    {
        try
        {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("userId", userId);
            param.put("password", password);

            return sqlSession.update(SqlName.Login.UPDATE_PASSWORD, param);
        }
        catch (Exception e)
        {
            log.error("Fail to updatePassword info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }
}
