package com.oswift.gpm.dao;

/**
 *
 * 定义sql名称
 *
 * @author zhuou
 * @version C03 Oct 11, 2012
 * @since OSwift GPM V1.0
 */
public interface SqlName
{
    public interface User
    {
        /**
         * uesr.xml的命名空间
         */
        public static final String NAMESPACE = "gpm.user";

        /**
         * 根据userId查询和此用户关联的系统
         */
        public static final String GET_SYSBYUSERID = NAMESPACE + ".getSysByUserId";

        /**
         * 根据userId删除用户和系统关联表数据
         */
        public static final String DEL_USERRSYSBYUID = NAMESPACE + ".delUserRSysByUId";

        /**
         * 根据userId删除用户和角色关联表数据
         */
        public static final String DEL_USERRROLEBYUID = NAMESPACE + ".delUserRRoleByUId";

        /**
         * 根据userId删除用户和组关联表数据
         */
        public static final String DEL_USERRGROUPBYUID = NAMESPACE + ".delUserRGroupByUId";

        /**
         * 根据userId修改用户状态
         */
        public static final String UPDATE_USERSTATUS = NAMESPACE + ".updateUserStatus";

        /**
         * 根据userId查询用户和组关联数据
         */
        public static final String GET_USERRGROUPBYUID = NAMESPACE + ".getUserRGroupByUId";

        /**
         * 根据userId查询用户和角色关联数据
         */
        public static final String GET_USERRROLEBYUID = NAMESPACE + ".getUserRRoleByUId";

        /**
         * 根据userId和sysId删除用户和组关联数据
         */
        public static final String DEL_USERRGROUPBYUIDSYSID = NAMESPACE + ".delUserRGroupByUIdAndSysId";

        /**
         * 根据userId和sysId删除用户和角色关联数据
         */
        public static final String DEL_USERRROLEBYUIDSYSID = NAMESPACE + ".delUserRRoleByUIdAndSysId";

        /**
         * 新增用户和组关联数据
         */
        public static final String ADD_R_USERGROUP = NAMESPACE + ".add_R_UserGroup";

        /**
         * 新增用户和角色关联数据
         */
        public static final String ADD_R_USERGROLE = NAMESPACE + ".add_R_UserRole";
    }

    public interface Contacter
    {
        /**
         * contacter.xml的命名空间
         */
        public static final String NAMESPACE = "gpm.contacter";
    }

    public interface Group
    {
        /**
         * group.xml的命名空间
         */
        public static final String NAMESPACE = "gpm.group";

        /**
         * 添加一条分组记录
         */
        public static final String ADD_GROUPINFO = NAMESPACE + ".addGroup";

        /**
         * 新增组和操作关系表
         */
        public static final String ADD_R_GROUPOPS = NAMESPACE + ".add_R_GroupOps";

        /**
         * 新增组和角色关系表
         */
        public static final String ADD_R_GROUPROLE = NAMESPACE + ".add_R_GroupRole";

        /**
         * 根据groupId获取组和操作关系表数据
         */
        public static final String GET_R_GROUPOPS = NAMESPACE + ".get_R_GroupOps";

        /**
         * 根据groupId获取组和角色关系表数据
         */
        public static final String GET_R_GROUPROLE = NAMESPACE + ".get_R_GroupRole";

        /**
         * 根据组ID获取组拥有的角色
         */
        public static final String GET_ROLE_BYGROUPID = NAMESPACE + ".getRoleByGroupId";

        /**
         * 根据组ID获取组拥有的操作权限
         */
        public static final String GET_OPS_BYGROUPID = NAMESPACE + ".getOpsByGroupId";

        /**
         * 根据组id删除组和角色关联表数据
         */
        public static final String DEL_GROUPRROLEBYGROUPID = NAMESPACE + ".delGroupRRolebyGroupId";

        /**
         * 根据组id删除组和操作关联表数据
         */
        public static final String DEL_GROUPROPSBYGROUPID = NAMESPACE + ".delGroupROpsbyGroupId";

        /**
         * 根据组id删除组和用户关联表数据
         */
        public static final String DEL_GROUPRUSERBYGROUPID = NAMESPACE + ".delGroupRUserbyGroupId";

        /**
         * 修改子节点的ParentId
         */
        public static final String UPDATE_CHILDPARENTID = NAMESPACE + ".updateChildParentId";
    }

    public interface System
    {
        /**
         * system.xml的命名空间
         */
        public static final String NAMESPACE = "gpm.system";

        /**
         * 添加用户和系统关系
         */
        public static final String ADD_R_USERSYSTEM = NAMESPACE + ".add_R_UserSystem";

        /**
         * 获取所有sys信息，不带分页
         */
        public static final String GET_SYS_LIST = NAMESPACE + ".getSysList";

        /**
         * 查询Id号最大的sys
         */
        public static final String GET_TOP1SYS = NAMESPACE + ".getTop1Sys";

        /**
         * 根据sysId查询系统下拥有的所有用户
         */
        public static final String GET_USERSBYSYSID = NAMESPACE + ".getUsersBySysId";

        /**
         * 根据sysId删除系统和用户关系表
         */
        public static final String DEL_USERRSYSBYSYSID = NAMESPACE + ".delUserRSysBySysId";

        /**
         * 根据sysId查看此系统下有多少角色信息
         */
        public static final String COUNT_ROLESBYSYSID = NAMESPACE + ".countRolesBySysId";

        /**
         * 根据sysId查看此系统下有多少个操作信息
         */
        public static final String COUNT_OPSBYSYSID = NAMESPACE + ".countOpsBySysId";

        /**
         * 根据sysId查看此系统下有多少个用户信息
         */
        public static final String COUNT_USERSBYSYSID = NAMESPACE + ".countUsersBySysId";

        /**
         * 根据sysId查看此系统下有多少个组/部门信息
         */
        public static final String COUNT_GROUPSBYSYSID = NAMESPACE + ".countGroupsBySysId";
    }

    public interface OpsInfo
    {
        /**
         * opsInfo.xml的命名空间
         */
        public static final String NAMESPACE = "gpm.opsinfo";

        /**
         * 根据操作ID删除操作和组关联数据
         */
        public static final String DELETE_R_OPSGROUP = NAMESPACE + ".delOpsRGroupByOpsId";

        /**
         * 根据操作ID删除操作和角色关联数据
         */
        public static final String DELETE_R_OPSROLE = NAMESPACE + ".delOpsRRoleByOpsId";

        /**
         * 根据操作ID删除操作和系统关联数据
         */
        public static final String DELETE_R_OPSSYS = NAMESPACE + ".delOpsRSysByOpsId";

        /**
         * 根据ParentId修改ParentId和NoteLevel
         */
        public static final String UPDATE_OPSCHILD = NAMESPACE + ".updateOpsChild";

        /**
         * 获取操作ID最大的操作信息
         */
        public static final String GET_TOP1OPS = NAMESPACE + ".getTop1Ops";
    }

    public interface Role
    {
        /**
         * Role.xml的命名空间
         */
        public static final String NAMESPACE = "gpm.role";

        /**
         * 插入T_GPM_R_OpsRole关系表
         */
        public static final String ADD_R_OPSROLE = NAMESPACE + ".add_R_OpsRole";

        /**
         * 根据sysid获取此系统的所有角色
         */
        public static final String GET_ROLESBYSYSID = NAMESPACE + ".getRolesBySysId";

        /**
         * 根据角色ID查询角色下拥有的操作
         */
        public static final String GET_OPSBYROLEID = NAMESPACE + ".getOpsByRoleId";

        /**
         * 根据角色ID删除操作和角色关联表数据
         */
        public static final String DEL_OPSRROLEBYROLEID = NAMESPACE + ".delOpsRRoleByRoleId";

        /**
         * 根据角色ID删除组和角色关联表数据
         */
        public static final String DEL_ROLERROLEBYROLEID = NAMESPACE + ".delRoleRGroupByRoleId";

        /**
         * 根据角色ID删除用户和角色关联表数据
         */
        public static final String DEL_USERRROLEBYROLEID = NAMESPACE + ".delUserRRoleByRoleId";

        /**
         * 根据角色Id获取角色和操作关联表数据
         */
        public static final String GET_ROLEROPSBYROLEID = NAMESPACE + ".getRoleROpsByRoleId";
    }

    public interface Login
    {
        /**
         * Login.xml的命名空间
         */
        public static final String NAMESPACE = "gpm.login";

        /**
         * 根据用户名称获取用户信息
         */
        public static final String GET_USERBYNAME = NAMESPACE + ".getUserByName";

        /**
         *  根据ID,修改LoginCount,LastLoginIP,LastLoginTime,LoginErrorCount字段
         */
        public static final String UPDATE_USERLOGININFO = NAMESPACE + ".updateUserLoginInfo";

        /**
         * 根据userId、systemId和节点深度获取菜单列表
         */
        public static final String GET_MENU1 = NAMESPACE + ".getMenu1";

        /**
         * 根据userId、systemId和url获取操作，可能有多个
         */
        public static final String GET_MENU2 = NAMESPACE + ".getMenu2";

        /**
         * 根据userId、systemId和opsId获取某个操作
         */
        public static final String GET_MENU3 = NAMESPACE + ".getMenu3";

        /**
         * 修改密码
         */
        public static final String UPDATE_PASSWORD = NAMESPACE + ".updatePassword";
    }

    public interface WhiteList
    {
        /**
         * 命名空间
         */
        public static final String NAMESPACE = "gpm.whiteList";
    }
}
