package com.oswift.gpm.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.oswift.gpm.entity.GroupBean;
import com.oswift.gpm.entity.GroupROpsBean;
import com.oswift.gpm.entity.GroupRRoleBean;
import com.oswift.gpm.entity.OpsInfoBean;
import com.oswift.gpm.entity.RoleBean;
import com.oswift.utils.exception.ErrorCode;
import com.oswift.utils.exception.PlatException;

@Repository("groupDao")
public class GroupDao extends BaseDao<GroupBean>
{
    /**
     * 日志实例
     */
    private static Logger log = LoggerFactory.getLogger(GroupDao.class);

    /**
     *
     * 构造函数
     */
    public GroupDao()
    {
        super(SqlName.Group.NAMESPACE);
    }

    /**
     *
     * 新增组和操作关系表
     *
     * @author zhuou
     * @param beans
     *            List<GroupROpsBean>
     * @return int
     */
    public int addGroupROps(List<GroupROpsBean> beans)
    {
        return this.sqlSession.insert(SqlName.Group.ADD_R_GROUPOPS, beans);
    }

    /**
     *
     * 新增组和角色关系表
     *
     * @author zhuou
     * @param beans
     *            List<GroupRRoleBean>
     * @return int
     */
    public int addGroupRRole(List<GroupRRoleBean> beans)
    {
        return this.sqlSession.insert(SqlName.Group.ADD_R_GROUPROLE, beans);
    }

    /**
     *
     * 根据groupId获取组和操作关系表数据
     *
     * @author zhuou
     * @param groupId
     *            组Id
     * @return List<GroupROpsBean>
     */
    public List<GroupROpsBean> getGroupROps(String groupId)
    {
        return this.sqlSession
                .selectList(SqlName.Group.GET_R_GROUPOPS, groupId);
    }

    /**
     *
     * 根据groupId获取组和角色关系表数据
     *
     * @author zhuou
     * @param groupId
     *            组Id
     * @return List<GroupRRoleBean>
     */
    public List<GroupRRoleBean> getGroupRRole(String groupId)
    {
        return this.sqlSession.selectList(SqlName.Group.GET_R_GROUPROLE,
                groupId);
    }

    /**
     *
     * 根据组ID获取组拥有的角色
     *
     * @author zhuou
     * @param groupId
     *            组id
     * @return List<RoleBean>
     */
    public List<RoleBean> getRolesByGroupId(String groupId)
    {
        return this.sqlSession.selectList(SqlName.Group.GET_ROLE_BYGROUPID,
                groupId);
    }

    /**
     *
     * 根据组ID获取组拥有的操作权限
     *
     * @author zhuou
     * @param groupId
     *            组ID
     * @return List<OpsInfoBean>
     */
    public List<OpsInfoBean> getOpsByGroupId(String groupId)
    {
        return this.sqlSession.selectList(SqlName.Group.GET_OPS_BYGROUPID,
                groupId);
    }

    /**
     *
     * 根据组id删除组和角色关联表数据
     *
     * @author zhuou
     * @param groupId
     *            组ID
     * @return 删除行数
     */
    public int delGroupRRolebyGroupId(String groupId) throws PlatException
    {
        try
        {
            return this.sqlSession.delete(
                    SqlName.Group.DEL_GROUPRROLEBYGROUPID, groupId);
        }
        catch (Exception e)
        {
            log.error("Fail to del delGroupRRolebyGroupId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据组id删除组和操作关联表数据
     *
     * @author zhuou
     * @param groupId
     *            组ID
     * @return 删除行数
     */
    public int delGroupROpsbyGroupId(String groupId) throws PlatException
    {
        try
        {
            return this.sqlSession.delete(SqlName.Group.DEL_GROUPROPSBYGROUPID,
                    groupId);

        }
        catch (Exception e)
        {
            log.error("Fail to del delGroupROpsbyGroupId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 根据组id删除组和用户关联表数据
     *
     * @author zhuou
     * @param groupId
     *            组ID
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int delGroupRUserbyGroupId(String groupId) throws PlatException
    {
        try
        {
            return this.sqlSession.delete(
                    SqlName.Group.DEL_GROUPRUSERBYGROUPID, groupId);

        }
        catch (Exception e)
        {
            log.error("Fail to del delGroupRUserbyGroupId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

    /**
     *
     * 修改子节点的ParentId
     *
     * @author zhuou
     * @param groupInfo
     *            GroupBean
     * @return int
     * @throws PlatException
     *             公共异常
     */
    public int updateChildParentId(GroupBean groupInfo) throws PlatException
    {
        try
        {
            return this.sqlSession.update(SqlName.Group.UPDATE_CHILDPARENTID,
                    groupInfo);

        }
        catch (Exception e)
        {
            log.error("Fail to del updateChildParentId info.", e);
            throw new PlatException(ErrorCode.COMMON_DB_ERROR);
        }
    }

}
