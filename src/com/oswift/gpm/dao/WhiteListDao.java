package com.oswift.gpm.dao;

import org.springframework.stereotype.Repository;

import com.oswift.gpm.entity.WhiteListBean;

@Repository("whiteListDao")
public class WhiteListDao extends BaseDao<WhiteListBean>
{
    /**
     *
     * 构造函数
     */
    public WhiteListDao()
    {
        super(SqlName.WhiteList.NAMESPACE);
    }
}
