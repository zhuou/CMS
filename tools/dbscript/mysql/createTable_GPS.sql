﻿drop table if exists T_GPM_Contacter;
drop table if exists T_GPM_Group;
drop table if exists T_GPM_Ops_Info;
drop table if exists T_GPM_R_GroupUser;
drop table if exists T_GPM_R_OpsGroup;
drop table if exists T_GPM_R_OpsRole;
drop table if exists T_GPM_R_OpsSystem;
drop table if exists T_GPM_R_RoleGroup;
drop table if exists T_GPM_R_UserRole;
drop table if exists T_GPM_R_UserSystem;
drop table if exists T_GPM_Role;
drop table if exists T_GPM_SystemModule;
drop table if exists T_GPM_User;
drop table if exists T_GPM_WhiteList;

/*==============================================================*/
/* Table: T_GPM_Contacter                                       */
/*==============================================================*/
create table T_GPM_Contacter
(
   UserId               int comment '用户ID',
   TrueName             varchar(50) comment '真实姓名',
   Sex                  varchar(8) comment '性别',
   Position             varchar(50) comment '职务',
   Email                varchar(20) comment 'Email',
   QQ                   varchar(20) comment 'QQ',
   OfficePhone          varchar(20) comment '办公电话',
   HomePhone            varchar(20) comment '住宅电话',
   Mobile               varchar(20) comment '移动电话',
   Address              varchar(255) comment '联系地址',
   ZipCode              varchar(10) comment '邮政编码',
   Birthday             timestamp null default null comment '出生日期',
   IDCard               varchar(20) comment '证件号码'
);

/*==============================================================*/
/* Table: T_GPM_Group                                           */
/*==============================================================*/
create table T_GPM_Group
(
   GroupId              int not null AUTO_INCREMENT comment '组ID',
   SystemId             int not null comment '系统ID',
   ParentId             int not null comment '父节点ID 如果没有父节点则为-1',
   Inherit              bool default false comment '是否继承父节点的权限，false不继承 true继承，默认false',
   GroupName            varchar(50) not null comment '组名称',
   GroupDesc            varchar(500) comment '组描述',
   CreateTime           timestamp null default null comment '创建时间',
   CreateUser           varchar(50) comment '创建人',
   LastEditTime         timestamp null default null comment '最后编辑时间',
   LastEditUser         varchar(50) default null comment '最后编辑人',
   primary key(GroupId)
);

/*==============================================================*/
/* Table: T_GPM_Ops_Info                                        */
/*==============================================================*/
create table T_GPM_Ops_Info
(
   OpsId                int not null comment '操作ID',
   SystemId             int not null comment '系统ID',
   ParentId             int not null comment '父节点ID 没有父节点则为-1',
   NoteLevel            int default 1 comment '节点深度 默认为1',
   OpsName              varchar(50) not null comment '操作名称',
   OpsType              int default 1 comment '操作类型，定义3种类型。1代表菜单类型 2代表页面类型 3代表按钮操作类型',
   OpsUrl               varchar(100) comment '操作URL',
   OpsIcon              varchar(100) comment 'icon图片路径',
   Status               int default 1 comment '状态位 1为启用 0为停用',
   Sort                 int default 1 comment '排序',
   CreateTime           timestamp null default null comment '创建时间',
   CreateUser           varchar(50) comment '创建人',
   LastEditTime         timestamp null default null comment '最后编辑时间',
   LastEditUser         varchar(50) comment '最后编辑人',
   OpsDesc              varchar(500) comment '操作描述',
   primary key(OpsId)
);

/*==============================================================*/
/* Table: T_GPM_R_GroupUser                                     */
/*==============================================================*/
create table T_GPM_R_GroupUser
(
   UserId               int not null,
   SystemId             int not null,
   GroupId              int not null
);

/*==============================================================*/
/* Table: T_GPM_R_OpsGroup                                      */
/*==============================================================*/
create table T_GPM_R_OpsGroup
(
   GroupId              int comment '组ID',
   OpsId                int comment '操作ID',
   Inherit              bool default false comment '是否继承父节点的权限，false不继承 true继承，默认false'
);

/*==============================================================*/
/* Table: T_GPM_R_OpsRole                                       */
/*==============================================================*/
create table T_GPM_R_OpsRole
(
   OpsId                int not null,
   RoleId               int not null comment '角色ID'
);

/*==============================================================*/
/* Table: T_GPM_R_OpsSystem                                     */
/*==============================================================*/
create table T_GPM_R_OpsSystem
(
   SystemId             int not null,
   OpsId                int not null
);

/*==============================================================*/
/* Table: T_GPM_R_RoleGroup                                     */
/*==============================================================*/
create table T_GPM_R_RoleGroup
(
   GroupId              int comment '组ID',
   RoleId               int comment '角色ID',
   Inherit              bool default false comment '是否继承父节点的权限，false不继承 true继承，默认false'
);

/*==============================================================*/
/* Table: T_GPM_R_UserRole                                      */
/*==============================================================*/
create table T_GPM_R_UserRole
(
   UserId               int not null,
   SystemId             int not null,
   RoleId               int not null
);

/*==============================================================*/
/* Table: T_GPM_R_UserSystem                                    */
/*==============================================================*/
create table T_GPM_R_UserSystem
(
   UserId               int not null,
   SystemId             int not null
);

/*==============================================================*/
/* Table: T_GPM_Role                                            */
/*==============================================================*/
create table T_GPM_Role
(
   RoleId               int not null AUTO_INCREMENT comment '角色ID',
   SystemId             int comment '系统ID',
   RoleName             varchar(50) not null comment '角色名称',
   RoleDesc             varchar(500) comment '角色描述',
   CreateTime           timestamp null default null comment '创建时间',
   CreateUser           varchar(50) comment '创建人',
   LastEditTime         timestamp null default null comment '最后编辑时间',
   LastEditUser         varchar(20) comment '最后编辑人',
   primary key(RoleId)
);

/*==============================================================*/
/* Table: T_GPM_SystemModule                                    */
/*==============================================================*/
create table T_GPM_SystemModule
(
   SystemId             int not null comment '系统ID',
   SystemName           varchar(50) not null comment '系统名称',
   SystemDesc           varchar(500) comment '系统描述',
   SystemIP             varchar(32) not null comment '系统IP，用于验证使用',
   HostAddr             varchar(64) not null comment '域名或者系统IP+端口，用于拼接请求地址使用',
   SecretKey	        varchar(32) not null comment '密钥',
   Signed               varchar(32) not null comment '通信签证信息，MD5（密钥+系统ID）',
   CreateTime           timestamp null default null comment '创建时间',
   CreateUser           varchar(50) comment '创建人',
   LastEditTime         timestamp NULL default null comment '最后编辑时间',
   LastEditUser         varchar(50) comment '最后编辑人',
   primary key (SystemId)
);
alter table T_GPM_SystemModule add unique AK_key_systemNum (SystemName);

/*==============================================================*/
/* Table: T_GPM_User                                            */
/*==============================================================*/
create table T_GPM_User
(
   UserId               int not null AUTO_INCREMENT comment '用户ID',
   UserName             varchar(64) not null comment '管理员名称',
   Password             varchar(32) not null comment '管理员密码',
   EnableMultiLogin     bool default true comment '允许多人同时使用此帐号登录',
   LoginErrorTime       int default 0 comment '容许登录错误次数，0为不限制',
   LockTime             int default 5 comment '锁定时间长度，单位分钟。默认为5分钟，当为0时，为无限长时间，有管理员解锁',
   Status               int default 0 comment '用户状态。0－－正常，1－－超过登录错误次数锁定，2－－被管理员锁定',
   LoginCount           int default 0 comment '登录次数',
   LastLoginIP          varchar(32) default null comment '最近登录IP',
   LastLoginTime        timestamp null default null comment '最近登录时间',
   LastLogoutTime       timestamp null default null comment '最近退出管理后台时间',
   LastModifyPasswordTime timestamp null default null comment '最近修改密码时间',
   EnableModifyPassword bool default true comment '是否允许修改密码',
   AccountExpires       timestamp null default null comment '账户有效期，如果不填则无限期',
   LoginErrorCount      int default 0 comment '本次登录错误次数',
   primary key (UserId)
);
alter table T_GPM_User add unique AK_key_userName (UserName);

/*==============================================================*/
/* Table: T_GPM_WhiteList                                       */
/*==============================================================*/
create table T_GPM_WhiteList
(
	WhiteListId int not null AUTO_INCREMENT comment '白名单ID',
	Name        varchar(64) not null comment '请求Url名称',
	RequestURI  varchar(256) not null comment '请求Url',
	primary key (WhiteListId)
)