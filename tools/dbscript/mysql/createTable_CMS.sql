﻿drop table if exists T_CMS_Announce;
drop table if exists T_CMS_Article;
drop table if exists T_CMS_Channel;
drop table if exists T_CMS_Comment;
drop table if exists T_CMS_CommonModel;
drop table if exists T_CMS_Company;
drop table if exists T_CMS_CompanyIndustry;
drop table if exists T_CMS_CompanyPropertie;
drop table if exists T_CMS_CompanyScale;
drop table if exists T_CMS_Download;
drop table if exists T_CMS_FriendSite;
drop table if exists T_CMS_GuestBook;
drop table if exists T_CMS_GuestBookReply;
drop table if exists T_CMS_PicGallery;
drop table if exists T_CMS_PicUrl;
drop table if exists T_CMS_MemberGroup;
drop table if exists T_CMS_WebSiteConfig;
drop table if exists T_CMS_Province;
drop table if exists T_CMS_City;
drop table if exists T_CMS_District;
drop table if exists T_CMS_Dictionary;
drop table if exists T_CMS_Member;
drop table if exists T_CMS_MemberContacter;
drop table if exists T_CMS_Files;
drop table if exists T_CMS_AdZone;
drop table if exists T_CMS_Advertisement;
drop table if exists T_CMS_ReceptionPageUrl;
drop table if exists T_CMS_Tag;

/*==============================================================*/
/* Table: T_CMS_Announce                                        */
/*==============================================================*/
create table T_CMS_Announce
(
   AnnounceId           bigint not null AUTO_INCREMENT comment '公告ID',
   Keyword              varchar(512) comment '关键字',
   Author               varchar(64) not null comment '作者',
   OutTime              decimal comment '有效期天数',
   Content              text not null comment '公告内容',
   PublishTime          datetime comment '公告发布时间',
   primary key (AnnounceId)
);

alter table T_CMS_Announce comment '公告表';

/*==============================================================*/
/* Table: T_CMS_Article                                         */
/*==============================================================*/
create table T_CMS_Article
(
   ArticleId            bigint not null AUTO_INCREMENT comment '文章ID',
   TitleIntact          varchar(512) not null comment '完整标题',
   Subheading           varchar(512) comment '副标题',
   Author               varchar(256) comment '作者',
   CopyFrom             varchar(256) comment '来源',
   KeyWord              varchar(512) comment '关键字',
   ContentImgNum        int default 0 comment '内容图片数量',
   FocusImg             varchar(256) comment '焦点图片',
   FocusImgNum          int default 0 comment '0 表示没有焦点图片，1表示有焦点图片',
   BigNewsImg           varchar(256) comment ' 头条图片',
   BigNewsImgNum        int default 0 comment '0 表示没有头条图片，1表示有头条图片',
   MarqueeImg           varchar(256) comment ' 滚动图片',
   MarqueeImgNum        int default 0 comment '0 表示没有滚动图片，1表示有滚动图片',
   Intro                text comment '描述',
   Content              longtext not null comment '内容',
   Praise               int default 0 comment '当前点"赞"数',
   Tread                int default 0 comment '当前点"踩"数',
   primary key (ArticleId)
);

alter table T_CMS_Article comment '文章表';

/*==============================================================*/
/* Table: T_CMS_Channel                                         */
/*==============================================================*/
create table T_CMS_Channel
(
   ChannelId            int not null comment '栏目ID',
   CompanyId            int not null comment '公司ID',
   ChannelName          varchar(512) not null comment '栏目名称',
   NickName             varchar(64) not null comment '栏目别称，英文名，可以在前台url中使用和栏目id作用一致',
   ChannelType          tinyint comment '节点类型。0为容器栏目，1为专题栏目，2为单个页面，3为外部链接',
   ParentId             int comment '父节点,根节点的值为-1',
 --  RootId               int comment '根节点,如果是根节点此值为自己的ID',
 --  ArrRootId            varchar(512) comment '根节点ID集',
   Sort                 int default 1 comment '排序',
  -- NoteLevel            int default 1 comment '节点深度 默认为1',
   IsChild              bool default 0 comment'是否有子节点',
   Tips                 varchar(512) comment '栏目tip',
   Meta_Keywords        varchar(512) comment '栏目关键字',
   Meta_Description     varchar(512) comment '栏目描述',
   Description          text comment '描述',
   ChannelUrl           varchar(512) comment '栏目URL',
   IconUrl              varchar(512) comment '栏目iconURL',
   OpenType             tinyint comment '打开方式',
   Content              mediumtext comment '栏目内容',
   Category             int default 1 comment '栏目类别 0为预置栏目 1为用户创建栏目',
   PurviewType          int default 0 comment '栏目权限。0--开放栏目 1--认证栏目',
   AccessRight          varchar(512) comment '访问权限，和用户组ID相关联，如：1,2,3',
   Forums               varchar(512) comment '包含板块',
   CreateTime           datetime null comment '创建时间',
   CreateUser           varchar(50) comment '创建人',
   LastEditTime         datetime null comment '最后编辑时间',
   LastEditUser         varchar(50) default null comment '最后编辑人',
   primary key (ChannelId)
);

alter table T_CMS_Channel comment '栏目表';

/*==============================================================*/
/* Table: T_CMS_Comment                                         */
/*==============================================================*/
create table T_CMS_Comment
(
   CommentId            bigint not null AUTO_INCREMENT comment '评论ID',
   CommonModelId        bigint not null comment '内容全局ID',
   CompanyId            int not null comment '公司ID',
   UserId               bigint comment '用户ID -1为游客',
   UserName             varchar(256) comment '用户名称',
   CommentTitle         varchar(512) comment '评论标题',
   CommentContent       text not null comment '评论内容',
   CreateTime           datetime comment '创建时间',
   IP                   varchar(128) comment 'IP',
   IsElite              bool default 0 comment '是否精华',
   IsPrivate            bool default 0 comment '该评论是否可以公开（是否只有管理员可以看到）0公开 1不公开',
   Agree                int default 0 comment '当前评论的支持数',
   Oppose               int default 0 comment '当前评论的反对数',
   Neutral              int default 0 comment '当前评论的中立数',
   CommentType          tinyint comment ' 评论类别 1-网站用户2-手机用户',
   primary key (CommentId)
);

alter table T_CMS_Comment comment '评论表';

/*==============================================================*/
/* Table: T_CMS_CommonModel                                     */
/*==============================================================*/
create table T_CMS_CommonModel
(
   CommonModelId        bigint not null AUTO_INCREMENT comment '内容全局ID',
   ChannelId            int not null comment '栏目ID',
   CompanyId            int,
   ItemId               bigint not null comment '相应表的记录ID',
   TableName            varchar(512) not null comment '模型表名',
   Title                varchar(512) not null comment '标题',
   TitlePrefix          varchar(512) comment '标题前缀 1--[图文] 2--[组图] 3--[推荐] 4--[注意]',
   Inputer              varchar(256) comment '录入者',
   Hits                 int comment '点击数',
   DayHits              int comment '日点击数',
   WeekHits             int comment '周点击数',
   MonthHits            int comment '月点击数',
   OpenType             bool comment '链接打开方式 false本窗口打开 true新窗口打开',
   Status               tinyint comment '-3为删除，-2为退稿，-1为草稿，0为待审核，99为终审通过，其它为自定义',
   EliteLevel           int comment '推荐级别',
   Priority             int comment ' 优先级别',
   CommentCount         int comment '评论总数',
   UpdateTime           datetime comment '更新时间',
   CreateTime           datetime,
   PassedTime           datetime comment '审核通过时间',
   LastHitTime          datetime comment '上次点击时间',
   DefaultPicUrl        varchar(512) comment '默认封面图片',
   ShowCommentLink      tinyint comment '列表显示时是否在标题旁显示“评论”链接',
   IsCreateHtml         tinyint comment '是否生成静态页面',
   HtmlUrl              varchar(512) comment '静态页URL',
   primary key (CommonModelId)
);

alter table T_CMS_CommonModel comment '内容公共信息表';

/*==============================================================*/
/* Table: T_CMS_Company                                         */
/*==============================================================*/
create table T_CMS_Company
(
   CompanyId            int not null AUTO_INCREMENT comment 'ID',
   CompanyName          varchar(256) not null comment '公司名称',
   CompanyScaleId       int not null comment '公司规模',
   CompanyPropertieId   int not null comment '公司性质',
   CompanyIndustryId    int not null comment '公司行业',
   Referred             varchar(256) comment '公司简称',
   Country              varchar(256) comment '国家/地区',
   Province             varchar(256) comment '省市/州郡',
   City                 varchar(256) comment '城市',
   District             varchar(256) comment '区/县',
   Address              varchar(512) comment '联系地址',
   ZipCode              varchar(10) comment '邮政编码',
   Contact              varchar(256) comment '联系人',
   Telephone            varchar(20) comment '公司联系电话',
   CompanyIntr          text comment '公司简介',
   Email                varchar(64) comment 'Email',
   Mobile               varchar(20) comment '公司手机',
   CreateTime           datetime comment '创建时间',
   Remark               text,
   primary key (CompanyId)
);

alter table T_CMS_Company comment '公司/单位表';

/*==============================================================*/
/* Table: T_CMS_CompanyIndustry                                 */
/*==============================================================*/
create table T_CMS_CompanyIndustry
(
   CompanyIndustryId    int not null AUTO_INCREMENT comment 'ID',
   CompanyIndustry      varchar(64) not null comment '公司行业信息',
   primary key (CompanyIndustryId)
);

alter table T_CMS_CompanyIndustry comment '公司行业表';

/*==============================================================*/
/* Table: T_CMS_CompanyPropertie                                */
/*==============================================================*/
create table T_CMS_CompanyPropertie
(
   CompanyPropertieId   int not null AUTO_INCREMENT comment 'ID',
   CompanyPropertie     varchar(64) not null comment '公司性质',
   primary key (CompanyPropertieId)
);

alter table T_CMS_CompanyPropertie comment '公司性质表';

/*==============================================================*/
/* Table: T_CMS_CompanyScale                                    */
/*==============================================================*/
create table T_CMS_CompanyScale
(
   CompanyScaleId       int not null AUTO_INCREMENT comment 'ID',
   CompanyScale         varchar(64) comment '公司规模',
   primary key (CompanyScaleId)
);

alter table T_CMS_CompanyScale comment '公司规模表';


/*==============================================================*/
/* Table: T_CMS_Download                                        */
/*==============================================================*/
create table T_CMS_Download
(
   DownloadId           bigint not null AUTO_INCREMENT comment '下载ID',
   FileVersion          varchar(64) comment '软件版本',
   Author               varchar(256) comment '作者',
   CopyFrom             varchar(256) comment '软件来源',
   DemoUrl              varchar(512) comment '示例地址',
   Keyword              varchar(512) comment '关键字',
   OperatingSystem      varchar(128) comment '软件平台',
   FileType             varchar(128) comment '软件类型',
   FileLanguage         varchar(128) comment '软件语言',
   CopyrightType        varchar(128) comment '授权方式',
   RegUrl               varchar(512) comment '注册地址',
   FileIntro            text comment '软件介绍',
   FileSize             varchar(10) comment '软件大小',
   DownloadUrl          text not null comment '下载地址',
   DecompressPassword   varchar(20) comment '解压密码',
   primary key (DownloadId)
);

/*==============================================================*/
/* Table: T_CMS_FriendSite                                      */
/*==============================================================*/
create table T_CMS_FriendSite
(
   FriendSiteId         bigint not null AUTO_INCREMENT comment '友情链接ID',
   SiteUrl              varchar(512) not null comment '友情链接URL',
   SiteIntro            text comment '友情链接描述',
   LogoUrl              varchar(512) comment '友情链接logo地址',
   LinkType             tinyint default 1 comment '链接类型1表示文字类型;2表示图片类型',
   IsElite              tinyint default 0 comment '是否推荐',
   primary key (FriendSiteId)
);

/*==============================================================*/
/* Table: T_CMS_GuestBook                                       */
/*==============================================================*/
create table T_CMS_GuestBook
(
   GuestBookId          bigint not null AUTO_INCREMENT comment '留言ID',
   UserId               bigint not null comment '用户ID',
   UserName             varchar(50) comment '用户名',
   GuestEmail			varchar(100) comment 'Email',
   PhoneNumber			varchar(20) comment '手机号码',
   GuestFace			varchar(255) comment '用户头像',
   GuestOicq            varchar(50) comment 'QQ',
   GuestIsPrivate       tinyint default 1 comment '是否隐藏',
   GuestContent         text not null comment '留言内容',
   ReplyCount           int default 0 comment '回复数',
   primary key (GuestBookId)
);
alter table T_CMS_GuestBook comment '留言表';

/*==============================================================*/
/* Table: T_CMS_GuestBookReply                                  */
/*==============================================================*/
create table T_CMS_GuestBookReply
(
   ReplyId              bigint not null AUTO_INCREMENT comment '留言回复ID',
   GuestBookId          bigint not null comment '留言ID',
   AdminId              int not null comment '管理员ID',
   ReplyName            varchar(50) comment '回复人名称',
   ReplyContent         text comment'管理员回复内容',
   ReplyTime            datetime comment'管理员回复日期',
   CreateTime           datetime comment '创建时间',
   primary key (ReplyId)
);
alter table T_CMS_GuestBookReply comment '留言回复表';

/*==============================================================*/
/* Table: T_CMS_PicGallery                                      */
/*==============================================================*/
create table T_CMS_PicGallery
(
   PicGalleryId         bigint not null AUTO_INCREMENT comment '图片ID',
   PicGalleryAuthor     varchar(128) comment '作者',
   CopyFrom             varchar(512) comment '来源',
   Keyword              varchar(512) comment '关键字',
   PicGalleryIntro      text comment '描述',
   primary key (PicGalleryId)
);

alter table T_CMS_PicGallery comment '图片集表';

/*==============================================================*/
/* Table: T_CMS_PicUrl                                          */
/*==============================================================*/
create table T_CMS_PicUrl
(
   PicGalleryId         bigint not null comment '图片集ID',
   PicName              varchar(128) comment '图片名称',
   PicAuthor            varchar(128) comment '图片作者',
   PicUrl               varchar(512) not null comment '图片URL',
   PicIntro             text comment '图片简介',
   CreateTime           datetime comment '创建时间'
);

/*==============================================================*/
/* Table: T_CMS_MemberGroup                                       */
/*==============================================================*/
create table T_CMS_MemberGroup
(
   GroupID              int not null AUTO_INCREMENT comment '用户组ID',
   CompanyId            int not null comment '公司ID',
   GroupName            varchar(128) not null comment '用户组名',
   GroupType            int default 1 comment '会员组类型 1表示内置会员组(不容许删除) 2表示用户创建会员组',
   Description          tinytext comment '用户组说明',
   UpgradeSetting       text comment '用户升级参数设置',
   CreateTime           datetime comment '创建时间',
   CreateUser           varchar(128) comment '创建者',
   primary key (GroupID)
);

/*==============================================================*/
/* Table: T_CMS_WebSiteConfig                                   */
/*==============================================================*/
create table T_CMS_WebSiteConfig
(
   SiteId               int not null AUTO_INCREMENT comment '网站ID',
   CompanyId            int not null comment '公司ID',
   SiteName             varchar(128) not null comment '网站名称',
   SiteUrl              varchar(512) not null comment '网站网址',
   LogoUrl              varchar(512) comment '网站logo',
   Copyright            varchar(512) comment '版权',
   ErrorPagePath		varchar(512) comment '错误页面路径，如果不填则为系统错误页面',
   MetaKeywords         varchar(512) comment '网站关键字',
   MetaDescription      varchar(512) comment '网站描述',
   UpLoadImgType        varchar(128) comment '容许上传图片的格式，以“,”分隔',
   UploadFileType       varchar(128) comment '容许上传文件的格式，以“,”分隔',
   ImgThumbWidth        int default 100 comment '网站缩略图宽',
   ImgThumbHeight       int default 100 comment '网站缩略图高',
   CreateTime           datetime comment '创建时间',
   CreateUser           varchar(64) comment '创建者',
   UpdateTime           datetime comment '修改时间',
   primary key (SiteId)
);

alter table T_CMS_WebSiteConfig comment '网站配置表';

/*==============================================================*/
/* Table: T_CMS_Province 创建省级表                                   */
/*==============================================================*/
CREATE TABLE T_CMS_Province
(
	ProId INT not null AUTO_INCREMENT comment 'ID',
	ProCode varchar(16) comment'省份编码',
	ProName varchar(50) NOT NULL comment'省份名称',
	ProSort INT default 1 comment'省份排序',
	primary key (ProID)
);
/*==============================================================*/
/* Table: T_CMS_City                                            */
/*==============================================================*/
CREATE TABLE T_CMS_City
(
	CityId INT not null AUTO_INCREMENT comment 'ID',
	CityCode varchar(16) comment'城市编码',
	CityName varchar(50)  NOT NULL comment'城市名称',
	ProCode varchar(16) comment'省份编码',
	CitySort INT default 1 comment'城市排序',
	primary key (CityID)
);
/*==============================================================*/
/* Table: T_CMS_District                                        */
/*==============================================================*/
CREATE TABLE  T_CMS_District
(
	DisId INT not null AUTO_INCREMENT comment 'ID',
	DisCode varchar(16) comment'区县编码',
	DisName	varchar(30) NOT NULL comment'区县名称',
	CityCode varchar(16) comment'城市编码',
	DisSort INT default 1 comment'区县排序',
	primary key (DisId)
);
/*==============================================================*/
/* Table: T_CMS_Dictionary 字典表                                    */
/*==============================================================*/
CREATE TABLE  T_CMS_Dictionary
(
	FieldID INT not null AUTO_INCREMENT comment '字段ID',
	Title   varchar(200) comment'字段标题',
	FieldName varchar(255) comment'字段名',
	FieldValue varchar(255) comment'字段值（选项名|是否启用值|默认值）',
	IsCache bit comment'是否缓存',
	primary key (FieldID)
);
/*==============================================================*/
/* Table: T_CMS_Member 用户表                                     */
/*==============================================================*/
CREATE TABLE  T_CMS_Member
(
	UserId                                bigint not null AUTO_INCREMENT comment'用户Id',
	GroupId                               int not null comment'用户组Id',
	UserName                              varchar(32) not null comment'用户名',
	UserPassword                          varchar(32) not null comment'用户密码',
	Question                              varchar(64) comment'提示问题',
	Answer                                varchar(64) comment'提示答案',
	RegTime                               datetime comment '注册时间',
	LoginTimes                            int default 0 comment'登录次数',
	LastLoginTime                         datetime comment '最后登录时间',
	LastLoginIP                           varchar(32) comment'最后登录IP',
	LastPasswordChangedTime               datetime comment '上次修改密码的时间',
	LastLockoutTime                       datetime comment '上次被锁定的时间',
	FailedPasswordAttemptCount            int default 0 comment'本次使用无效密码登录的次数，正确登录后置为0',
	FailedPasswordAnswerAttempCount       int default 0 comment'本次使用无效答案找回密码的次数，输入正确后置为0',
	Status                                int default 0 comment'用户状态。0-正常，1-锁定',
	EnableResetPassword                   tinyint default 1 comment'是否允许用户修改密码',
	UserFace                              varchar(256) comment'用户头像',
	FaceWidth                             int comment'头像宽度',
	FaceHeight                            int comment'头像高度',
	UserExp                               int default 0 comment'用户积分',
	ConsumeExp                            int default 0 comment'消费的积分数',
	GetPasswordSid                        varchar(32) comment'取回密码随机串',
	GetPasswordTime                       datetime comment '上次取回密码时间',
	primary key (UserId)
);
alter table T_CMS_Member add unique AK_key_userName (UserName);
/*==============================================================*/
/* Table: T_CMS_MemberContacter 用户扩展表                            */
/*==============================================================*/
CREATE TABLE  T_CMS_MemberContacter
(
	ContacterID                           bigint not null AUTO_INCREMENT comment'用户扩展Id',
	UserId                                bigint not null  comment'用户Id',
	TrueName                              varchar(32) comment'真实姓名',
	Email                                 varchar(32) comment'用户Email',
	Sex                                   int comment'性别',
	Company                               varchar(128) comment'单位名称',
	Department                            varchar(64) comment'所属部门',
	Position                              varchar(64) comment'职务',
	CompanyAddress                        varchar(128) comment'单位地址',
	Homepage                              varchar(256) comment'相关网址',
	QQ                                    varchar(16) comment'QQ号码',
	OfficePhone                           varchar(32) comment'办公电话',
	HomePhone                             varchar(32) comment'住宅电话',
	Mobile                                varchar(32) comment'移动电话',
	Province                              varchar(32) comment'省、直辖市',
	City                                  varchar(32) comment'市',
	District                              varchar(32) comment'区、县',
	ZipCode                               varchar(32) comment'邮政编码',
	Address                               varchar(256) comment'联系地址',
	NativePlace                           varchar(32) comment'籍贯',
	Nation                                varchar(32) comment'民族',
	Birthday                              datetime comment'出生日期',
	CardType                              int comment'证件类型',
	IDCard                                varchar(64) comment'证件号',
	Marriage                              int comment'婚姻状况',
	Family                                text comment'家庭情况',
	Income                                varchar(64) comment'月收入',
	Education                             varchar(64) comment'学历',
	GraduateFrom                          varchar(64) comment'毕业学校',
	InterestsOfLife                       varchar(256) comment'生活爱好',
	InterestsOfCulture                    varchar(256) comment'文化爱好',
	InterestsOfAmusement                  varchar(256) comment'娱乐休闲爱好',
	InterestsOfSport                      varchar(256) comment'体育爱好',
	InterestsOfOther                      varchar(256) comment'其他爱好',
	Attachment                            varchar(256) comment'附件路径',
	CreateTime                            datetime comment'创建时间',
	UpdateTime                            datetime comment'更新时间',
	primary key (ContacterID)
);
alter table T_CMS_MemberContacter add unique AK_key_userId (UserId);


CREATE TABLE T_CMS_ReceptionPageUrl
(
	PageUrlId int not null AUTO_INCREMENT comment '站点前台页面Url Id',
	SiteId int comment'站点Id',
	UrlType int not null comment'前台页面url类型',
	NickName varchar(64) default '' comment '站点前台页面名称,英文命名，index表示首页',
	JspPath varchar(512) comment'JSP页面路径',
	Intro varchar(512) comment'描述',
	CreateTime datetime comment'创建时间',
	CreateUser varchar(50) comment '创建人',
	UpdateTime datetime comment'更新时间',
	primary key (PageUrlId)
);


/*==============================================================*/
/* Table: T_CMS_Files 上传文件数据管理                          */
/*==============================================================*/
CREATE TABLE T_CMS_Files
(
  FilesId bigint not null AUTO_INCREMENT comment '文件ID',
  CompanyId int not null comment '公司ID',
  FileName varchar(256) not null comment '文件名称',
  Size int null comment '文件大小 单位KB',
  Path varchar(512) not null comment '文件储存路径',
  FileType varchar(128) not null comment '文件类型',
  Status int default 1 comment'1为图片，2为除图片其他文件',
  CreateTime datetime null comment '创建时间',
  primary key(FilesId)
);

/*==============================================================*/
/* Table: T_CMS_AdZone 广告位                                   */
/*==============================================================*/
CREATE TABLE T_CMS_AdZone
(
 ZoneId int not null AUTO_INCREMENT comment '广告位ID',
 CompanyId int not null comment '公司ID',
 ZoneCode varchar(32) not null comment '广告位编码，用于前台查询广告位使用',
 ZoneName varchar(128) not null comment '广告位名称',
 ZoneIntro varchar(512) not null comment '广告位描述',
 -- ZoneWidth int default 0 comment'广告位宽度',
 -- ZoneHeight int default 0 comment'广告位高度',
 Status bit default 1 comment'广告位状态，0为不显示 1为活动广告位',
 CreateTime datetime null comment '创建时间',
 CreateUser varchar(128) comment '创建者',
 primary key(ZoneID)
);
ALTER TABLE T_CMS_AdZone add index(ZoneCode);

/*==============================================================*/
/* Table: T_CMS_Advertisement 广告                              */
/*==============================================================*/
CREATE TABLE T_CMS_Advertisement
(
  AdId int not null AUTO_INCREMENT comment '广告ID',
  ZoneId int not null comment'广告id',
  AdName varchar(512) not null comment '广告名称',
  AdType int not null comment'广告类型',
  ImgUrl varchar(255) null comment'图片路径',
  ImgWidth int default 0 comment'图片宽',
  ImgHeight int default 0 comment'图片高',
  AdIntro text comment '广告内容描述',
  LinkUrl varchar(255) null comment'链接地址',
  LinkAlt varchar(255) null comment'链接alt提示信息',
  Priority int default 0 comment'广告项目的权重',
  Passed bit default 1 comment'是否通过审核',
  Sort int default 1 comment'排序',
  Clicks int default 0 comment'点击次数',
  OpenTimeLimit bit default 0 comment'是否开启时间限制 0为不开启 1开启',
  StartTime datetime null comment'开始时间',
  EndTime datetime null comment'结束时间',
  CreateTime datetime null comment '创建时间',
  CreateUser varchar(128) comment '创建者',
  primary key(AdId)
);
/*==============================================================*/
/* Table: T_CMS_Tag 标签                                        */
/*==============================================================*/
CREATE TABLE T_CMS_Tag
(
	Tagid int not null AUTO_INCREMENT comment '标签ID',
	CompanyId int not null comment '公司ID',
	TagName varchar(64) not null comment '标签名称',
	UsedNum int default 0 comment'标签被使用次数',
	TagClick int default 0 comment'标签被点击次数',
	CreateTime datetime null comment '创建时间',
	primary key(Tagid)
);