
INSERT into t_cms_companyindustry (CompanyIndustry)VALUES('计算机/互联网/通信/电子'),('会计/金融/银行/保险'),('贸易/消费/制造/营运'),('制药/医疗'),('广告/媒'),('房地产/建筑'),('专业服务/教育/培训'),('服务业'),('物流/运输'),('能源/原材料'),('政府/非赢利机构/其他'),('其他');
INSERT INTO t_cms_companypropertie(CompanyPropertie)VALUES('外资企业'),('合资企业'),('国有企业'),('股份制企业'),('私营企业'),('其他');
INSERT INTO t_cms_companyscale(CompanyScale)VALUES('微型(10人以下)'),('小型(10-50人)'),('中小(50-100人)'),('中型(100-500人)'),('大型(500人以上)');

INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('前台错误页面路径','ErrorPagePath','common/404/404',1);
INSERT INTO t_cms_dictionary (Title,FieldName,FieldValue,IsCache)
VALUES('文章版本','default_article','-1',1),('公告板块','default_announce','-2',1),
('下载版本','default_download','-3',1),('留言版本','default_guestbook','-4',1),
('图片板块','default_picurl','-5',1),('友情链接版本','default_friendsite','-6',1);

-- ------------------------------------------------------------------------------------------------------------
-- CMS字典表初始化数据
-- ------------------------------------------------------------------------------------------------------------
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件类别','FileType','国产软件',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件类别','FileType','国外软件',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件类别','FileType','图片下载',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件类别','FileType','电影下载',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件类别','FileType','其他',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件语言','FileLanguage','简体中文',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件语言','FileLanguage','繁体中文',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件语言','FileLanguage','英文',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件语言','FileLanguage','其他语言',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('授权方式','CopyrightType','免费版',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('授权方式','CopyrightType','共享版',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('授权方式','CopyrightType','试用版',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('授权方式','CopyrightType','演示版',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('授权方式','CopyrightType','注册版',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('授权方式','CopyrightType','破解版',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('授权方式','CopyrightType','零售版',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('授权方式','CopyrightType','OME版',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件平台','OperatingSystem','windows XP',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件平台','OperatingSystem','windows NT',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件平台','OperatingSystem','windows7',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件平台','OperatingSystem','windows8',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件平台','OperatingSystem','windows 2003',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件平台','OperatingSystem','Linux',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件平台','OperatingSystem','Unix',0);
INSERT INTO t_cms_dictionary(Title,FieldName,FieldValue,IsCache)values('软件平台','OperatingSystem','.NET平台',0);


-- --------------------------------------------------------------------------------------------------------------
-- 初始化权限白名单---
-- --------------------------------------------------------------------------------------------------------------
INSERT INTO `t_gpm_whitelist` VALUES ('1', '根据组ID获取组拥有的角色', '/security/getRolesByGroupId.html');
INSERT INTO `t_gpm_whitelist` VALUES ('2', '根据组ID获取组拥有的操作权限', '/security/getOpsByGroupId.html');
INSERT INTO `t_gpm_whitelist` VALUES ('3', '获取操作信息列表', '/security/getOpsList.html');
INSERT INTO `t_gpm_whitelist` VALUES ('4', '根据角色ID查询角色下拥有的操作', '/security/getOpsByRoleId.html');
INSERT INTO `t_gpm_whitelist` VALUES ('5', '根据sysId查询系统下拥有的所有用户', '/security/getUsersBySysId.html');
INSERT INTO `t_gpm_whitelist` VALUES ('6', '获取系统列表信息', '/security/getCheckSysList.html');
INSERT INTO `t_gpm_whitelist` VALUES ('7', '获取有分页的用户列表', '/security/getCheckUserList.html');
INSERT INTO `t_gpm_whitelist` VALUES ('8', '获取角色和组数据', '/security/getRightData.html');
INSERT INTO `t_gpm_whitelist` VALUES ('9', '后台管理首页', '/security/index.html');
INSERT INTO `t_gpm_whitelist` VALUES ('10', '获取菜单', '/security/menu.html');
INSERT INTO `t_gpm_whitelist` VALUES ('11', '根据用户Id获取用户下的系统', '/security/getSysByUId.html');
INSERT INTO `t_gpm_whitelist` VALUES ('12', '获取组数据', '/security/getGroupList.html');
INSERT INTO `t_gpm_whitelist` VALUES ('13', '获取栏目', '/security/getChannel2.html');
INSERT INTO `t_gpm_whitelist` VALUES ('14', '获取上传的图片', '/security/getUploadImg.html');
INSERT INTO `t_gpm_whitelist` VALUES ('15', '初始化默认页面', '/security/loadDefaultImg.html');
INSERT INTO `t_gpm_whitelist` VALUES ('16', '获取栏目', '/security/getChannel.html');
INSERT INTO `t_gpm_whitelist` VALUES ('17', '获取内容默认类型', '/security/getContentType.html');
INSERT INTO `t_gpm_whitelist` VALUES ('18', '获取板块信息', '/security/getForumsByCId.html');
INSERT INTO `t_gpm_whitelist` VALUES ('19', '上传默认图片', '/security/uploadDefaultImg.html');
INSERT INTO `t_gpm_whitelist` VALUES ('20', '获取子节点值', '/security/getChildChannels.html');
INSERT INTO `t_gpm_whitelist` VALUES ('21', '获取栏目', '/security/getChannel3.html');
INSERT INTO `t_gpm_whitelist` VALUES ('22', '上传文件页面', '/security/loadUploadFile.html');
INSERT INTO `t_gpm_whitelist` VALUES ('23', '上传文件操作', '/security/uploadFile.html');
INSERT INTO `t_gpm_whitelist` VALUES ('24', '获取城市', '/security/getCity.html');
INSERT INTO `t_gpm_whitelist` VALUES ('25', '获取区县', '/security/getDistrict.html');
INSERT INTO `t_gpm_whitelist` VALUES ('26', '编辑框上传图片', '/security/editorUploadImg.html');
INSERT INTO `t_gpm_whitelist` VALUES ('27', '上传图片管理', '/security/editorManagerImg.html');


-- -----------------------------------------------------------------------------------------------------------------------
-- 初始化CMS用户数据
-- -----------------------------------------------------------------------------------------------------------------------
INSERT INTO `t_cms_company` VALUES ('-1', '欧迅科技', '1', '1', '1', '欧迅科技', null, '江苏省', '江苏省', '南京市', '', '', '', '', '', '', '', now(), '');
INSERT INTO `t_cms_websiteconfig` VALUES ('1','-1','欧讯科技','127.0.0.1',NULL,NULL,'','','','bmp,jpg,jpeg,png,gif','doc,zip,rar,txt',100,100,NOW(),'zhuou',NOW());

-- 预置栏目数据
INSERT INTO `t_cms_channel` VALUES (1001, -1, '文章中心', 'default-articles', 0, -1, 1, 0, NULL, NULL, NULL, '预置栏目', NULL, NULL, 0, NULL, 0, 0, NULL, '-1', now(), NULL, NULL, NULL);
INSERT INTO `t_cms_channel` VALUES (1002, -1, '图片中心', 'default-pics', 0, -1, 1, 0, NULL, NULL, NULL, '预置栏目', NULL, NULL, 0, NULL, 0, 0, NULL, '-5', now(), NULL, NULL, NULL);
INSERT INTO `t_cms_channel` VALUES (1003, -1, '下载中心', 'default-downloads', 0, -1, 1, 0, NULL, NULL, NULL, '预置栏目', NULL, NULL, 0, NULL, 0, 0, NULL, '-3', now(), NULL, NULL, NULL);
INSERT INTO `t_cms_channel` VALUES (1004, -1, '公告中心', 'default-announces', 0, -1, 1, 0, NULL, NULL, NULL, '预置栏目', NULL, NULL, 0, NULL, 0, 0, NULL, '-2', now(), NULL, NULL, NULL);
INSERT INTO `t_cms_channel` VALUES (1005, -1, '留言中心', 'default-guestBooks', 0, -1, 1, 0, NULL, NULL, NULL, '预置栏目', NULL, NULL, 0, NULL, 0, 0, NULL, '-4', now(), NULL, NULL, NULL);
INSERT INTO `t_cms_channel` VALUES (1006, -1, '友情链接中心', 'default-friendSites', 0, -1, 1, 0, NULL, NULL, NULL, '预置栏目', NULL, NULL, 0, NULL, 0, 0, NULL, '-6', now(), NULL, NULL, NULL);
-- 初始化用户组
INSERT INTO `T_CMS_MemberGroup`(CompanyId,GroupName,Description,UpgradeSetting,CreateTime,CreateUser) VALUES ('-1', '注册会员', '注册会员', null, now(), null);