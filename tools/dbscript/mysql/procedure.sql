﻿DROP PROCEDURE IF EXISTS cms_commentList_noPage;
CREATE PROCEDURE `cms_commentList_noPage`(IN topNum INT,
	IN commonModelId LONG,
	IN strWhere VARCHAR (200),
	IN orderColumn VARCHAR (50),
	IN orderType INT,
  IN companyId INT)
BEGIN
	DECLARE
		strSql VARCHAR (1000);

	DECLARE
		strOrder VARCHAR (200);

	DECLARE
		tempWhere VARCHAR (200);

	IF orderType = 1 THEN
		SET strOrder = concat('Order By ',orderColumn,' DESC');
	ELSE
		SET strOrder = concat('Order By ',orderColumn,' ASC');
	END IF;
	
	IF commonModelId = -1 THEN -- 等于-1是取所有评论数据，如果不等于-1是取此内容的所有评论
		SET tempWhere = concat(' WHERE CompanyId=',companyId,' ', strWhere);
	ELSE
		SET tempWhere = concat(' WHERE CompanyId=',companyId,' AND CommonModelId=',commonModelId,' ',strWhere);
	END IF;

	SET strSql = concat('SELECT * FROM view_commentlist ', tempWhere, ' ', strOrder, ' LIMIT 0,', topNum);

	-- 执行sql
	SET @v_sql = strSql;
	PREPARE stmt FROM @v_sql; -- 预处理需要执行的动态SQL
	EXECUTE stmt;    -- 执行SQL语句
	DEALLOCATE PREPARE stmt; -- 释放掉预处理段

END;

DROP PROCEDURE IF EXISTS cms_commentList_page;
CREATE PROCEDURE `cms_commentList_page`(IN topNum INT,
	IN commonModelId LONG,
	IN strWhere VARCHAR (200),
	IN orderColumn VARCHAR (50),
	IN orderType INT,
	IN companyId INT,
  IN rowNum INT,
  OUT totalRecordNum INT)
BEGIN
	DECLARE
		strSql VARCHAR (1000);

	DECLARE
		sqlCount VARCHAR (1000);

	DECLARE
		strOrder VARCHAR (200);

	DECLARE
		tempWhere VARCHAR (200);

	IF orderType = 1 THEN
		SET strOrder = concat('Order By ',orderColumn,' DESC');
	ELSE
		SET strOrder = concat('Order By ',orderColumn,' ASC');
	END IF;
	
	IF commonModelId = -1 THEN -- 等于-1是取所有评论数据，如果不等于-1是取此内容的所有评论
		SET tempWhere = concat(' WHERE CompanyId=',companyId,' ', strWhere);
	ELSE
		SET tempWhere = concat(' WHERE CompanyId=',companyId,' AND CommonModelId=',commonModelId,' ',strWhere);
	END IF;

	SET strSql = concat('SELECT * FROM view_commentlist ', tempWhere, ' ', strOrder, ' LIMIT ',rowNum,',',topNum);
	SET sqlCount = concat('SELECT COUNT(1) INTO @recordCount FROM view_commentlist ', tempWhere);
	
	-- 执行sql
	SET @v_sql1 = sqlCount;
	PREPARE stmt1 FROM @v_sql1; -- 预处理需要执行的动态SQL
	EXECUTE stmt1;    -- 执行SQL语句
	DEALLOCATE PREPARE stmt1; -- 释放掉预处理段
	SET totalRecordNum = @recordCount;

	-- 执行sql
	SET @v_sql = strSql;
	PREPARE stmt FROM @v_sql; -- 预处理需要执行的动态SQL
	EXECUTE stmt;    -- 执行SQL语句
	DEALLOCATE PREPARE stmt; -- 释放掉预处理段

END;

DROP PROCEDURE IF EXISTS cms_contentDetail;
CREATE PROCEDURE `cms_contentDetail`(IN cmId BIGINT,
	IN tableName VARCHAR (50))
BEGIN
	-- 更新点击率
	UPDATE t_cms_commonmodel SET Hits=Hits+1,DayHits=DayHits+1,MonthHits=MonthHits+1,WeekHits=WeekHits+1,LastHitTime=NOW() WHERE CommonModelId = cmId;
		
	-- 查询文章
	IF strcmp(tableName, 'T_CMS_Article') = 0 THEN
		SELECT * FROM view_articlelist WHERE CommonModelId = cmId;
	-- 查询公告
	ELSEIF strcmp(tableName, 'T_CMS_Announce') = 0 THEN
		SELECT * FROM view_announceList WHERE CommonModelId = cmId;
	-- 查询下载
	ELSEIF strcmp(tableName, 'T_CMS_Download') = 0 THEN
		SELECT * FROM view_downloadList WHERE CommonModelId = cmId;
	-- 查询留言
	ELSEIF strcmp(tableName, 'T_CMS_GuestBook') = 0 THEN
		SELECT * FROM view_guestbookList WHERE CommonModelId = cmId;
	-- 查询图片
	ELSEIF strcmp(tableName, 'T_CMS_PicGallery') = 0 THEN
		SELECT * FROM view_picList WHERE CommonModelId = cmId;
	-- 查询友情链接
	ELSEIF strcmp(tableName, 'T_CMS_FriendSite') = 0 THEN
		SELECT * FROM view_friendsiteList WHERE CommonModelId = cmId;
	END IF;
END;

DROP PROCEDURE IF EXISTS cms_contentList_noPage;
CREATE PROCEDURE `cms_contentList_noPage`(IN topNum INT,
	IN strWhere VARCHAR (200),
	IN orderColumn VARCHAR (50),
	IN orderType INT,
	IN cId INT,
  IN companyId INT,
	IN tableName VARCHAR (50))
BEGIN
	DECLARE
		strSql VARCHAR (1000);

DECLARE
	strOrder VARCHAR (200);

DECLARE
	tempWhere VARCHAR (200);

DECLARE
	channelIds VARCHAR (1000);

IF orderType = 1 THEN
	SET strOrder = concat('Order By ',orderColumn,' DESC');
ELSE
	SET strOrder = concat('Order By ',orderColumn,' ASC');
END IF;

-- 查询文章
IF strcmp(tableName, 'T_CMS_Article') = 0 THEN
	-- 当channelId=-1表示查询所有栏目下的文章；当channelId=栏目Id表示查询此栏目下的文章
	IF cId = -1 THEN
			IF companyId <> -1 THEN  -- 查询公司下记录，因为栏目已经和公司ID关联，所有按照栏目查询时，不需要在根据公司Id进行过滤了
				SET tempWhere = concat(' AND CompanyId=',companyId,' ', strWhere);
			ELSE
				SET tempWhere = strWhere;
			END IF;
			SET strSql = concat('SELECT * FROM view_articlelist ',' WHERE 1=1 ', tempWhere, ' ', strOrder, ' LIMIT 0,', topNum);
	ELSE
			-- 查询该栏目下的所有子栏目的ID
			SELECT group_concat(ChannelId separator ',') INTO @arrChildId FROM t_cms_channel WHERE ParentId = cId;
			IF @arrChildId IS NOT NULL AND @arrChildId <> '' THEN
				SET channelIds = concat(cId,',',@arrChildId);
			ELSE
				SET channelIds = concat(cId);
			END IF;
			-- 拼接sql
			SET strSql = concat('SELECT * FROM view_articlelist ',' WHERE ChannelId in (', channelIds, ') ', strWhere, ' ', strOrder, ' LIMIT 0,', topNum);
  END IF;
-- 查询公告
ELSEIF strcmp(tableName, 'T_CMS_Announce') = 0 THEN
	IF cId = -1 THEN
			IF companyId <> -1 THEN
				SET tempWhere = concat(' AND CompanyId=',companyId,' ', strWhere);
			ELSE
				SET tempWhere = strWhere;
			END IF;
			SET strSql = concat('SELECT * FROM view_announceList ',' WHERE 1=1 ', tempWhere, ' ', strOrder, ' LIMIT 0,', topNum);
	ELSE
			SELECT group_concat(ChannelId separator ',') INTO @arrChildId FROM t_cms_channel WHERE ParentId = cId;
			IF @arrChildId IS NOT NULL AND @arrChildId <> '' THEN
				SET channelIds = concat(cId,',',@arrChildId);
			ELSE
				SET channelIds = concat(cId);
			END IF;
			SET strSql = concat('SELECT * FROM view_announceList ',' WHERE ChannelId in (', channelIds, ') ', strWhere, ' ', strOrder, ' LIMIT 0,', topNum);
  END IF;
-- 查询下载
ELSEIF strcmp(tableName, 'T_CMS_Download') = 0 THEN
	IF cId = -1 THEN
			IF companyId <> -1 THEN
				SET tempWhere = concat(' AND CompanyId=',companyId,' ', strWhere);
			ELSE
				SET tempWhere = strWhere;
			END IF;
			SET strSql = concat('SELECT * FROM view_downloadList ',' WHERE 1=1 ', tempWhere, ' ', strOrder, ' LIMIT 0,', topNum);
	ELSE
			SELECT group_concat(ChannelId separator ',') INTO @arrChildId FROM t_cms_channel WHERE ParentId = cId;
			IF @arrChildId IS NOT NULL AND @arrChildId <> '' THEN
				SET channelIds = concat(cId,',',@arrChildId);
			ELSE
				SET channelIds = concat(cId);
			END IF;
			SET strSql = concat('SELECT * FROM view_downloadList ',' WHERE ChannelId in (', channelIds, ') ', strWhere, ' ', strOrder, ' LIMIT 0,', topNum);
  END IF;
-- 查询留言
ELSEIF strcmp(tableName, 'T_CMS_GuestBook') = 0 THEN
	IF cId = -1 THEN
			IF companyId <> -1 THEN
				SET tempWhere = concat(' AND CompanyId=',companyId,' ', strWhere);
			ELSE
				SET tempWhere = strWhere;
			END IF;
			SET strSql = concat('SELECT * FROM view_guestbookList ',' WHERE 1=1 ', tempWhere, ' ', strOrder, ' LIMIT 0,', topNum);
	ELSE
			SELECT group_concat(ChannelId separator ',') INTO @arrChildId FROM t_cms_channel WHERE ParentId = cId;
			IF @arrChildId IS NOT NULL AND @arrChildId <> '' THEN
				SET channelIds = concat(cId,',',@arrChildId);
			ELSE
				SET channelIds = concat(cId);
			END IF;
			SET strSql = concat('SELECT * FROM view_guestbookList ',' WHERE ChannelId in (', channelIds, ') ', strWhere, ' ', strOrder, ' LIMIT 0,', topNum);
  END IF;
-- 查询图片
ELSEIF strcmp(tableName, 'T_CMS_PicGallery') = 0 THEN
	IF cId = -1 THEN
			IF companyId <> -1 THEN
				SET tempWhere = concat(' AND CompanyId=',companyId,' ', strWhere);
			ELSE
				SET tempWhere = strWhere;
			END IF;
			SET strSql = concat('SELECT * FROM view_picList ',' WHERE 1=1 ', tempWhere, ' ', strOrder, ' LIMIT 0,', topNum);
	ELSE
			SELECT group_concat(ChannelId separator ',') INTO @arrChildId FROM t_cms_channel WHERE ParentId = cId;
			IF @arrChildId IS NOT NULL AND @arrChildId <> '' THEN
				SET channelIds = concat(cId,',',@arrChildId);
			ELSE
				SET channelIds = concat(cId);
			END IF;
			SET strSql = concat('SELECT * FROM view_picList ',' WHERE ChannelId in (', channelIds, ') ', strWhere, ' ', strOrder, ' LIMIT 0,', topNum);
  END IF;
-- 查询友情链接
ELSEIF strcmp(tableName, 'T_CMS_FriendSite') = 0 THEN
	IF cId = -1 THEN
			IF companyId <> -1 THEN
				SET tempWhere = concat(' AND CompanyId=',companyId,' ', strWhere);
			ELSE
				SET tempWhere = strWhere;
			END IF;
			SET strSql = concat('SELECT * FROM view_friendsiteList ',' WHERE 1=1 ', tempWhere, ' ', strOrder, ' LIMIT 0,', topNum);
	ELSE
			SELECT group_concat(ChannelId separator ',') INTO @arrChildId FROM t_cms_channel WHERE ParentId = cId;
			IF @arrChildId IS NOT NULL AND @arrChildId <> '' THEN
				SET channelIds = concat(cId,',',@arrChildId);
			ELSE
				SET channelIds = concat(cId);
			END IF;
			SET strSql = concat('SELECT * FROM view_friendsiteList ',' WHERE ChannelId in (', channelIds, ') ', strWhere, ' ', strOrder, ' LIMIT 0,', topNum);
  END IF;
END IF;

-- 执行sql
SET @v_sql = strSql;
PREPARE stmt FROM @v_sql; -- 预处理需要执行的动态SQL
EXECUTE stmt;    -- 执行SQL语句
DEALLOCATE PREPARE stmt; -- 释放掉预处理段

END;

DROP PROCEDURE IF EXISTS cms_contentList_page;
CREATE PROCEDURE `cms_contentList_page`(IN topNum INT,
	IN strWhere VARCHAR (200),
	IN orderColumn VARCHAR (50),
	IN orderType INT,
	IN cId INT,
	IN companyId INT,
  IN rowNum INT,
  OUT totalRecordNum INT,
	IN tableName VARCHAR (50))
BEGIN
	DECLARE
		strSql VARCHAR (1000);

DECLARE
		sqlCount VARCHAR (1000);

DECLARE
	strOrder VARCHAR (200);

DECLARE
	tempWhere VARCHAR (200);

DECLARE
	channelIds VARCHAR (1000);

IF orderType = 1 THEN
	SET strOrder = concat('Order By ',orderColumn,' DESC');
ELSE
	SET strOrder = concat('Order By ',orderColumn,' ASC');
END IF;

IF strcmp(tableName, 'T_CMS_Article') = 0 THEN
	-- 当channelId=-1表示查询所有栏目下的文章；当channelId=栏目Id表示查询此栏目下的文章
	IF cId = -1 THEN
			IF companyId <> -1 THEN  -- 查询公司下记录，因为栏目已经和公司ID关联，所有按照栏目ID查询时，不需要在根据公司Id进行过滤了
				SET tempWhere = concat(' AND CompanyId=',companyId,' ', strWhere);
			ELSE
				SET tempWhere = strWhere;
			END IF;
			SET strSql = concat('SELECT * FROM view_articlelist ',' WHERE 1=1 ', tempWhere, ' ', strOrder, ' LIMIT ', rowNum, ',', topNum);
			SET sqlCount = concat('SELECT COUNT(1) INTO @recordCount FROM view_articlelist ',' WHERE 1=1 ', tempWhere);
	ELSE
			-- 查询该栏目下的所有子栏目的ID
			SELECT group_concat(ChannelId separator ',') INTO @arrChildId FROM t_cms_channel WHERE ParentId = cId;
			IF @arrChildId IS NOT NULL AND @arrChildId <> '' THEN
				SET channelIds = concat(cId,',',@arrChildId);
			ELSE
				SET channelIds = concat(cId);
			END IF;
			-- 拼接sql
			SET strSql = concat('SELECT * FROM view_articlelist ',' WHERE ChannelId in (', channelIds, ') ', strWhere, ' ', strOrder, ' LIMIT ', rowNum, ',', topNum);
			SET sqlCount = concat('SELECT COUNT(1) INTO @recordCount FROM view_articlelist ',' WHERE ChannelId in (', channelIds, ') ', strWhere);
  END IF;
-- 查询公告
ELSEIF strcmp(tableName, 'T_CMS_Announce') = 0 THEN
	IF cId = -1 THEN
			IF companyId <> -1 THEN  -- 查询公司下记录，因为栏目已经和公司ID关联，所有按照栏目查询时，不需要在根据公司Id进行过滤了
				SET tempWhere = concat(' AND CompanyId=',companyId,' ', strWhere);
			ELSE
				SET tempWhere = strWhere;
			END IF;
			SET strSql = concat('SELECT * FROM view_announceList ',' WHERE 1=1 ', tempWhere, ' ', strOrder, ' LIMIT ', rowNum, ',',  topNum);
			SET sqlCount = concat('SELECT COUNT(1) INTO @recordCount FROM view_announceList ',' WHERE 1=1 ', tempWhere);
	ELSE
			SELECT group_concat(ChannelId separator ',') INTO @arrChildId FROM t_cms_channel WHERE ParentId = cId;
			IF @arrChildId IS NOT NULL AND @arrChildId <> '' THEN
				SET channelIds = concat(cId,',',@arrChildId);
			ELSE
				SET channelIds = concat(cId);
			END IF;
			SET strSql = concat('SELECT * FROM view_announceList ',' WHERE ChannelId in (', channelIds, ') ', strWhere, ' ', strOrder, ' LIMIT ', rowNum, ',',  topNum);
			SET sqlCount = concat('SELECT COUNT(1) INTO @recordCount FROM view_announceList ',' WHERE ChannelId in (', channelIds, ') ', strWhere);
  END IF;
-- 查询下载
ELSEIF strcmp(tableName, 'T_CMS_Download') = 0 THEN
	IF cId = -1 THEN
			IF companyId <> -1 THEN  -- 查询公司下记录，因为栏目已经和公司ID关联，所有按照栏目查询时，不需要在根据公司Id进行过滤了
				SET tempWhere = concat(' AND CompanyId=',companyId,' ', strWhere);
			ELSE
				SET tempWhere = strWhere;
			END IF;
			SET strSql = concat('SELECT * FROM view_downloadList ',' WHERE 1=1 ', tempWhere, ' ', strOrder, ' LIMIT ', rowNum, ',', topNum);
			SET sqlCount = concat('SELECT COUNT(1) INTO @recordCount FROM view_downloadList ',' WHERE 1=1 ', tempWhere);
	ELSE
			SELECT group_concat(ChannelId separator ',') INTO @arrChildId FROM t_cms_channel WHERE ParentId = cId;
			IF @arrChildId IS NOT NULL AND @arrChildId <> '' THEN
				SET channelIds = concat(cId,',',@arrChildId);
			ELSE
				SET channelIds = concat(cId);
			END IF;
			SET strSql = concat('SELECT * FROM view_downloadList ',' WHERE ChannelId in (', channelIds, ') ', strWhere, ' ', strOrder, ' LIMIT ', rowNum, ',', topNum);
			SET sqlCount = concat('SELECT COUNT(1) INTO @recordCount FROM view_downloadList ',' WHERE ChannelId in (', channelIds, ') ', strWhere);
  END IF;
-- 查询留言
ELSEIF strcmp(tableName, 'T_CMS_GuestBook') = 0 THEN
	IF cId = -1 THEN
			IF companyId <> -1 THEN  -- 查询公司下记录，因为栏目已经和公司ID关联，所有按照栏目查询时，不需要在根据公司Id进行过滤了
				SET tempWhere = concat(' AND CompanyId=',companyId,' ', strWhere);
			ELSE
				SET tempWhere = strWhere;
			END IF;
			SET strSql = concat('SELECT * FROM view_guestbookList ',' WHERE 1=1 ', tempWhere, ' ', strOrder, ' LIMIT ', rowNum, ',', topNum);
			SET sqlCount = concat('SELECT COUNT(1) INTO @recordCount FROM view_guestbookList ',' WHERE 1=1 ', tempWhere);
	ELSE
			SELECT group_concat(ChannelId separator ',') INTO @arrChildId FROM t_cms_channel WHERE ParentId = cId;
			IF @arrChildId IS NOT NULL AND @arrChildId <> '' THEN
				SET channelIds = concat(cId,',',@arrChildId);
			ELSE
				SET channelIds = concat(cId);
			END IF;
			SET strSql = concat('SELECT * FROM view_guestbookList ',' WHERE ChannelId in (', channelIds, ') ', strWhere, ' ', strOrder, ' LIMIT ', rowNum, ',', topNum);
			SET sqlCount = concat('SELECT COUNT(1) INTO @recordCount FROM view_guestbookList ',' WHERE ChannelId in (', channelIds, ') ', strWhere);
  END IF;
-- 查询图片
ELSEIF strcmp(tableName, 'T_CMS_PicGallery') = 0 THEN
	IF cId = -1 THEN
			IF companyId <> -1 THEN  -- 查询公司下记录，因为栏目已经和公司ID关联，所有按照栏目查询时，不需要在根据公司Id进行过滤了
				SET tempWhere = concat(' AND CompanyId=',companyId,' ', strWhere);
			ELSE
				SET tempWhere = strWhere;
			END IF;
			SET strSql = concat('SELECT * FROM view_picList ',' WHERE 1=1 ', tempWhere, ' ', strOrder, ' LIMIT ', rowNum, ',', topNum);
			SET sqlCount = concat('SELECT COUNT(1) INTO @recordCount FROM view_picList ',' WHERE 1=1 ', tempWhere);
	ELSE
			SELECT group_concat(ChannelId separator ',') INTO @arrChildId FROM t_cms_channel WHERE ParentId = cId;
			IF @arrChildId IS NOT NULL AND @arrChildId <> '' THEN
				SET channelIds = concat(cId,',',@arrChildId);
			ELSE
				SET channelIds = concat(cId);
			END IF;
			SET strSql = concat('SELECT * FROM view_picList ',' WHERE ChannelId in (', channelIds, ') ', strWhere, ' ', strOrder, ' LIMIT ', rowNum, ',', topNum);
			SET sqlCount = concat('SELECT COUNT(1) INTO @recordCount FROM view_picList ',' WHERE ChannelId in (', channelIds, ') ', strWhere);
  END IF;
-- 查询友情链接
ELSEIF strcmp(tableName, 'T_CMS_FriendSite') = 0 THEN
	IF cId = -1 THEN
			IF companyId <> -1 THEN  -- 查询公司下记录，因为栏目已经和公司ID关联，所有按照栏目查询时，不需要在根据公司Id进行过滤了
				SET tempWhere = concat(' AND CompanyId=',companyId,' ', strWhere);
			ELSE
				SET tempWhere = strWhere;
			END IF;
			SET strSql = concat('SELECT * FROM view_friendsiteList ',' WHERE 1=1 ', tempWhere, ' ', strOrder, ' LIMIT ', rowNum, ',', topNum);
			SET sqlCount = concat('SELECT COUNT(1) INTO @recordCount FROM view_friendsiteList ',' WHERE 1=1 ', tempWhere);
	ELSE
			SELECT group_concat(ChannelId separator ',') INTO @arrChildId FROM t_cms_channel WHERE ParentId = cId;
			IF @arrChildId IS NOT NULL AND @arrChildId <> '' THEN
				SET channelIds = concat(cId,',',@arrChildId);
			ELSE
				SET channelIds = concat(cId);
			END IF;
			SET strSql = concat('SELECT * FROM view_friendsiteList ',' WHERE ChannelId in (', channelIds, ') ', strWhere, ' ', strOrder, ' LIMIT ', rowNum, ',', topNum);
			SET sqlCount = concat('SELECT COUNT(1) INTO @recordCount FROM view_friendsiteList ',' WHERE ChannelId in (', channelIds, ') ', strWhere);
  END IF;
END IF;

-- 执行sql
 SET @v_sql1 = sqlCount;
 PREPARE stmt1 FROM @v_sql1; -- 预处理需要执行的动态SQL
 EXECUTE stmt1;    -- 执行SQL语句
 DEALLOCATE PREPARE stmt1; -- 释放掉预处理段
 SET totalRecordNum = @recordCount;

-- 执行sql
SET @v_sql2 = strSql;
PREPARE stmt2 FROM @v_sql2; -- 预处理需要执行的动态SQL
EXECUTE stmt2;    -- 执行SQL语句
DEALLOCATE PREPARE stmt2; -- 释放掉预处理段
END;


DROP PROCEDURE IF EXISTS cms_advertisementList;
CREATE PROCEDURE `cms_advertisementList`(IN zId INT, IN topNum INT)
BEGIN
  SELECT COUNT(1) INTO @countNum FROM view_advertisementlist WHERE ZoneId=zId;
	IF @countNum<=topNum THEN
		SELECT * FROM view_advertisementlist WHERE ZoneId=zId;
  ELSE
		SELECT MAX(Priority) INTO @maxPriority FROM view_advertisementlist WHERE ZoneId=zId;
		SELECT MIN(Priority) INTO @minPriority FROM view_advertisementlist WHERE ZoneId=zId;
		SELECT * FROM view_advertisementlist 
		WHERE ZoneId=zId AND Priority>=ROUND((RAND()*(@maxPriority- @minPriority) + @minPriority)*(1-topNum/@countNum)) 
		ORDER BY Sort LIMIT topNum;
  END IF;
END;